{% extends "base.tmpl" %}

{% block seo %}

<title>{{entry.Title}}</title>
<link rel="canonical" href="{{site.URL}}/{{entry.Slug}}/">
<meta property="og:url" content="{{site.URL}}/{{entry.Slug}}/"/>
<meta property="og:title" content="{{entry.Title}}"/>
<meta property="og:description" content="{{entry.Excerpt}}"/>
<meta property="og:type" content="article" />
<meta name="description" content="{{entry.Excerpt}}"/>
<meta name="twitter:title" content="{{entry.Title}}" />
<meta name="twitter:description" content="{{entry.Excerpt}}" />
{% if entry.FeaturedImage %}
  <meta name="twitter:image" content="{{ entry.FeaturedImage }}" />
  <meta property="og:image" content="{{ entry.FeaturedImage }}">
{% else %}
  <meta property="og:image" content="https://cdn.olai.in/jjude/jjude.jpg" />
{% endif %}
<script type="application/ld+json">
  {
    "@context": "https://schema.org/",
    "@type": "BlogPosting",
      "headline": "{{entry.Title}}",
      "url": "{{site.URL}}/{{entry.Slug}}",
      "description": "{{entry.Excerpt}}",
      "datePublished": "{{ entry.PublishAt | slice: ":9" }}",
      {% if entry.FeaturedImage %}
        "image": "{{entry.FeaturedImage}}",
      {% else %}
        "image": "https://cdn.olai.in/jjude/jjude.jpg",
      {% endif %}
      "author": {
        "@type": "Person",
        "name": "{{user.Name}}"
      }
  }
</script>
{% endblock %}

{% block content %}
  <article class="pt3 pb3 bb b--moon-gray">
    <div>
      <div class="tc">        
        <h1 class="mb1 mt1 f3 dark-gray">{{ entry.Title }}</h1>
        <h3 class="ph3 ph5-ns fw3 georgia i">{{ entry.Excerpt }}</h3>
      </div>
      <div class="f4">
         {{ entry.Content | safe }}

         {% if entry.Type == "post" && entry.BSkyID %}
          <footer>
          <h3>Comments</h3>
          <bsky-comments post="at://did:plc:4hn2ld3q5zelho23lx372hx5/app.bsky.feed.post/{{entry.BSkyID}}"></bsky-comments>
          </footer>
         {% endif %}
      </div>
    </div>
    <small class="silver">Published On:
      <time datetime="{{entry.PublishAt | slice: ":9"}}">
      {{ entry.PublishAt | slice: ":9" }}
      </time>
    </small>
    <br/>
    {% if entry.Tags %}
      <small class="silver">Under:
        {% for tag in entry.Tags %}
          <a href="{{site.URL}}/tags/{{tag}}/" class="link" target="_blank">#{{tag}}</a>
          {% if not forloop.Last %}, {% endif %}
        {% endfor %}
      </small>
    {% endif %}
  </article>

{% endblock %}
