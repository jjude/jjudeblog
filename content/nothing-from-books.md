---
title="You don't gain anything by reading books"
slug="nothing-from-books"
excerpt="Take one idea. Make it your own. Better than speed-reading 52 books a year."
tags=["coach"]
type="post"
publish_at="01 Apr 23 12:06 IST"
featured_image=""
---

<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless src="https://share.transistor.fm/e/3f505377"></iframe>

There is no end to knowledge. Every day there are more books released than you will be able to read even in a decade. The point is not to read 52 books a year, or even 12. You don't gain anything from speed-reading books other than just showing-off.
  
Instead, **take one idea from a book. Make it your own**. Work on it like a gardener - water it, remove weeds, and harvest it. Chisel the life of your dreams out of hard rock like a sculptor. Weave the idea into your life like a master craftsman.

I am going to share with you three such ideas from books that changed my life.
  
A long time ago, I read the book "The Artist's Way" by Julia Cameron. I don't remember everything I read in it, except for the idea of **morning pages**. I wrote one page every day for almost a year. Even now, I write. Writing daily has helped me to think better, communicate better, and even earn better. The idea of morning pages, when implemented, changed my life, and it still does.

The second idea that changed my life is from the popular book, "The seven habits of highly effective people." The author Steven Covey talks about **writing your obituary**. When I read that as a twenty-something-old, it read as a creepy idea. But after I got married and my wife was admitted for our first baby-birth, I took time to do that. Oh man, that was such a powerful exercise. What I wrote became my north-star and my guiding principle. Ever since I wrote it, I have tried to live to earn that obituary.

The final idea is from a simple tweet by Naval Ravikanth. He tweeted, **media + code = wealth**. As a CTO, I was already doing quite good at the code part. I was weak at the media part. Since I read that tweet, I have started a podcast and a youtube channel. And I publish on LinkedIn regularly. If you believe in the popular saying, "your network is your net-worth," I've surely become wealthy.

The main point of this episode is that, you don't need to read hundreds of books to change your life. What will change your life is taking one idea and implementing it.

What idea are you going to implement?

## Continue Reading

* [No hobby is an island](/hobby-no-island/)
* [Do you have a board of advisors?](/board-of-advisors/)
* [Wrong way to assemble the best car](/wrong-way-for-best-car/)