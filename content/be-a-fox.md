---
title="In an Uncertain World, Be a Fox to Innovate and Thrive"
slug="be-a-fox"
excerpt="If you want to succeed as an executive, be like a fox -  a multi-disciplinary thinker"
tags=["wins","systems","insights"]
type="post"
publish_at="31 Oct 22 14:58 IST"
featured_image="https://cdn.olai.in/jjude/be-a-fox-gen.jpg"
---

"The fox knows many things, but the hedgehog knows one big thing," wrote the Greek poet Archilochus. The philosopher Isaiah Berlin expanded on Archilochus in his often-quoted essay “The Hedgehog and the Fox.”

According to Berlin, hedgehogs "relate everything to a single central vision". They only think, understand, and act according to that one big thing.

The **fox-like thinkers** "pursue many ends, often unrelated and even contradictory" so that they can draw from the "essence of a vast variety of experiences and objects".

Hedgehogs are fixated on one big thing;  foxes know many little things. Hedgehogs have focused worldview and an ideological leaning; foxes are cautious and pragmatic. Hedgehogs are interested in knowledge; foxes focus on learning.

A fox-like engineer, Eiji Nakatsu, when faced with the challenge of improving a bullet train, asks, "**What would nature do?**" and designs the nose of the train like that of a kingfisher. 

You might have used a shoe with velcro (and if you are old enough, you might have used a watch with a velcro). A Swiss engineer George de Mestral came up with the idea of hook and loop fastener after he removed burrs from his clothes and dog's fur. 

As you might notice, the classification of thinkers into hedgehogs and foxes originates in nature.

Biomimicry is just one way of fox-like learning. Another method is cross-industry learning.

When Ford visited a slaughterhouse, he came up with the idea for assembly line.

If you want to succeed as a product manager, business unit head, or even as an executive, be like a fox - roam freely, listen carefully, and consume omnivorously. If you want to develop fox-like skills, I encourage you to follow "**multi-disciplinary thinking**" as advocated by Peter Kauffman. Peter recommends learning big ideas from three primary domains:

- non-living (physics, geology, chemistry); 
- biology (living things); 
- recorded human history

As a true fox, Peter draws from many domains instead of one. He draws from each bucket, so to speak, and builds useful mental models.

Consider physics. Newton's third law of motion says, "for every force, there is an equal and opposite force." The world echoes your action.

Take an example from biology. A dog will run around and wag its tail if you treat it with love and care. If you act in an unpleasant manner, your dog will mirror your behavior. It is the same with people.

In life and at work, you cannot solve challenges only with ideas from one field. To be able to solve problems in today's interconnected world, you need to learn from multiple domains.

## Continue Reading

* [You need to know very little to get ahead in life](/little-to-get-ahead/)
* [Wrong way to assemble the best car](/wrong-way-for-best-car/)
* [How to get lucky?](/luck/)