---
title="Why I Uber"
slug="why-i-uber"
excerpt="I take Uber cabs, because it benefits me."
tags=["startup"]
type="post"
publish_at="06 Oct 15 21:35 IST"
featured_image="https://cdn.olai.in/jjude/why-i-uber-gen.jpg"
---

I type this as I ride a Uber cab. I used to drive, not anymore. For the past 6 months, I take a Uber cab, for any journey that is more than 15 kms.

As an entrepreneur, I admire Uber. They have a great business model and they are executing it with a ruthless focus on a global scale. I wish I have both.

But that is not why I continue to use it. I take it because it benefits me.

<div class="getty embed image" style="background-color:#fff;display:inline-block;font-family:'Helvetica Neue',Helvetica,Arial,sans-serif;color:#a7a7a7;font-size:11px;width:100%;max-width:594px;"><div style="overflow:hidden;position:relative;height:0;padding:66.666667% 0 0 0;width:100%;"><iframe src="//embed.gettyimages.com/embed/457199178?et=1sB-ZjSGQs9NTdDSRKWEXg&viewMoreLink=off&sig=NLXBCBKocRrAJXLvfj5w28eTSWQHfVbSAaLbzGe7GPs=" width="594" height="396" scrolling="no" frameborder="0" style="display:inline-block;position:absolute;top:0;left:0;width:100%;height:100%;"></iframe></div><p style="margin:0;"></p><div style="padding:0;margin:0 0 0 10px;text-align:left;"><a href="http://www.gettyimages.com/detail/457199178" target="_blank" style="color:#a7a7a7;text-decoration:none;font-weight:normal !important;border:none;display:inline-block;">View image</a> | <a href="http://www.gettyimages.com" target="_blank" style="color:#a7a7a7;text-decoration:none;font-weight:normal !important;border:none;display:inline-block;">gettyimages.com</a></div></div>

1. **Avoid unnecessary stress**
	I have lived in Chennai, Bangalore, and Delhi. Now I live in Panchkula. There is one factor that is common to all these cities. Driving is stressful. It's always a drive through mad crowd. As if that isn't enough, many junctions don't have traffic signals and are manned by policemen. In India, most traffic policemen don't think of aiding citizens to follow the law. They stand there to watch you fail so that they could earn extra money on the side. I hate bribing, and I hate arguing with a lower-rank policemen whose only motive is to get some extra money. Most often they are not interested to issue a challan either. They make you wait with a hope that you will give in and pay them.

    There is one more factor. If you migrate from one state to another, you need to re-register the car and pay state road tax. These are not cheap. And this process isn't easy either. I bought my vehicle in Chennai and I haven't changed the registration. When all else fails, the traffic policeman will point to the registration plate to hold you. (this has happened in Bangalore, not so much in Delhi & Panchkula)

    As it is, I have difficulty in getting things done in my [ventures][1]. If I spend my cognitive energies on unnecessary activities and carry stress from those, then my output will grind to a halt. I avoid such stress by taking a cab.

2. **Buy time**
Like everyone else, I have known the equation: time = money. It was an epiphany when I realized that the reverse is true as well. Taking a cab costs, but I make time. I listen to [podcasts][2], or write a blog post, or code or take a nap in that made-up time. I don't keep myself engaged all the time though. There are days I free myself to think—new ideas, or ideas to solve an issue bothering me.

3. **It's my window into the world around**
I work from home most of the days. I watch limited TV (max half hour per week), and I read only editorials in newspapers. I get most of my news in the form of tweets.

   When I take cabs, I strike a conversation with drivers, in the Hindi, to know what is going on. The government PR machine churns out only the best news. The newspapers are full of TRP-driving headlines. The reality is somewhere in the middle. I get to know the reality from these drivers.

You might say these are true for any cab aggregator. True in theory, not true in practice. I was a regular user of Meru & Ola before switching to Uber.

Meru faced the typical scaling problem that haunts any startup. They couldn't scale with quality. Most of the time, the drivers would say the credit card reader doesn't work and I have to pay in cash. When I paid in cash, they would say they don't have proper change for return, forcing me to pay more than I had to pay. When I insist on getting the change, they would make me wait as they go from shop to shop to get change. Once, their cab broke down and the replacement cab came after half hour. I stood in the hot summer sun sweating and cursing myself for taking a Meru cab. I used to take Meru daily up and down for a year. I don't think Meru appreciated that fact.

Given this, I switched to Ola when it came in. The cars were cleaner than the Meru cabs. I liked their service. I continued to take Ola cabs even when I moved to Panchkula. Now I need to travel to Mohali and Ola doesn't have their services in Mohali. So I was looking for an alternative. That's when Uber introduced it's services in the tri-city. Uber costs less too.

Will Uber succeed in India? I hope so. Because I'm growing more and more dependent on it.


[1]: /my-itches-my-ventures/
[2]: /2015-02-podcasts/

