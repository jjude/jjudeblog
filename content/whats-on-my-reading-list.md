---
title="What's on my reading list"
slug="whats-on-my-reading-list"
excerpt="What I'm reading currently."
tags=["sdl","consume"]
type="post"
publish_at="27 Apr 08 16:30 IST"
featured_image="https://cdn.olai.in/jjude/whats-on-my-reading-list-gen.jpg"
---

I'm a voracious reader. I spend a lot (both money and time) on books. Blogs bring in a lot of flexibility and casualness to such a habit. I keep modifying my reading list according to perceived benefits.

Here are some of them:

##### Management Thoughts

[HBS Working Knowledge](http://hbswk.hbs.edu/): From Harvard Business School. Academic discussions on management.

Raven's Brain: She has good information on project management. Not only that she collates pretty good articles on project management too.

[Scottberkun.com](http://www.scottberkun.com): Scott Berkun's [The Art of Project Management](http://www.amazon.com/Project-Management-Theory-Practice-OReilly/dp/0596007868) is a must-read for all project managers. His web page is a collection of information on his books and essays.

##### Technical

Grey Sparling PeopleSoft Expert's Corner: I'm from Vantive world who moved on to Peoplesoft world. I don't develop in Peoplesoft but manage two projects (with some excellent team leads) in Peoplesoft. Tips from Grey Sparling helps me understand Peoplesoft little better.

[The Django community aggregator](http://www.djangoproject.com/community/): I've recently started with Django, a web framework in Python. I find Django easy to learn and Python easy to code. Hopefully I can develop some decent, useful applications in Django. I already developed SOL, a Twitter clone for our department use.

[Joel on Software](http://www.joelonsoftware.com): I'm not sure if this should be under Technical or under Management, because Joel speaks authoritatively on both topics. Whatever be the topic, his articles are very informative of the subject.

[programming: what's new online](http://reddit.com/r/programming/): Reddit's collection of articles of interest on Programming. These articles are rated socially and hence generally worth reading.

[Scott Hanselman's Computer Zen](http://www.hanselman.com/blog/): I stumbled on Scott Hanselman's blog while looking for useful tools. He not only blogs about useful utilities but about .NET as well.

[LinkedIn Answers: Technology](http://www.linkedin.com/answers/browse/technology/TCH): Related to LinkedIn Management Answers. I don't find this discussion forum that useful like the other one. Yet, time to time I get some valuable tips from this forum.

##### Indian blogs

[Just a little something](http://www.anitabora.com/blog/):I envy Anita's photography skills and admire her nomadic spirit. Every time I see one of her photos, I wish to shoot like that. I visited her Photography Exhibition too.

