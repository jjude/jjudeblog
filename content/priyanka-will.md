---
title="Protect your wealth with will' with Priyanka Sud"
slug="priyanka-will"
excerpt="How to ensure your hard-earned wealth gets passed to the next generation in the way you would like it to be?"
tags=["gwradio","wealth"]
type="post"
publish_at="12 Mar 24 14:35 IST"
featured_image="https://cdn.olai.in/jjude/priyanka-will-yt.jpg"
---
When it comes to wealth, there are lot of chatter about making wealth and growth wealth. But what about protecting it? How to ensure it gets passed to the next generation in the way you would like it to be?

One of the ways of protecting wealth is with will. To discuss how to protect your wealth with will, I have a good friend of mine and a leading lawyer, Priyanka here. We discuss everything about creating will.

<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless="" src="https://share.transistor.fm/e/32d991ef"></iframe>

## What you'll hear

- Who should create a will?
- Are there any restrictions for creating a will?
- Common mistakes in drafting a will
- Involvement of a lawyer
- How often to review a will?
- Handling joint properties
- Handling properties in multiple countries
- Potential problems in executing a will
- Kindest thing anyone has done for you
- Best leadership quality
- Definition of living a good life

## Watch on YouTube
<iframe width="560" height="315" src="https://www.youtube.com/embed/Obqk3YMsgN8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

## Major Points
1. Importance of Making a Will
   - Definition and Purpose of a Will: Understanding what a will is and why it is crucial to have one.
   - Eligibility and Guidelines: Who can create a will, the requirements, and the legal aspects involved.
   - Avoiding Disputes: Ensuring the will is free from coercion, clear in intent, and properly executed.
2. Common Mistakes in Drafting a Will
   - Selection of Witnesses: Choosing witnesses wisely who are independent and not beneficiaries.
   - Appointment of Executor: Designating an executor to ensure proper implementation of the will.
   - Registering the Will: The benefits of registering a will and the legal implications.
3. Involvement of a Lawyer in Will-Making
   - Complex Transactions: Consulting a lawyer for complex wealth or property matters.
   - Self-Drafting vs. Legal Assistance: Considering legal expertise for clear and error-free will drafting.
   - Updates and Amendments: Seeking legal advice for reviewing and updating the will as needed.
4. Handling Properties in Multiple Countries
   - Multiple Wills: Creating separate wills for properties in different countries.
   - Probate Process: Understanding probate in different jurisdictions and its implications.
   - Challenges in International Wills: Dealing with legal complexities and potential contests in multi-country wills.
5. Challenges in Executing a Will
   - Registration and Documentation: Importance of proper documentation and registration to avoid disputes.
   - Clarity and Reasoning: Providing clear reasons for decisions in the will to prevent misinterpretation.
   - Future Planning: Considering unforeseen circumstances and updating the will regularly for accuracy and relevance.

## Connect with Priyanka
- LinkedIn: https://www.linkedin.com/in/priyanka-sud-654254186/

## Connect with me
- Twitter: https://twitter.com/jjude
- LinkedIn: https://www.linkedin.com/in/jjude/
- Website: https://jjude.com/
- Newsletter: https://jjude.com/subscribe/
- Youtube: https://youtube.com/gravitaswins
- Email: podcast@jjude.com
- Executive Coaching Program: https://gravitaswins.com

## Your feedback counts

Thank you for listening. If you enjoy the podcast, would you please leave a short review on Apple podcast or on YouTube? It takes less than 60 seconds, and it really makes a difference in finding this podcast. And it boosts my spirits. 😀

I would like to hear what resonated the most with you in this episode. Either email me or share it on social media and tag me. Thank you for your support.

Have a life of WINS.

_Gravitas WINS Radio is produced by [Joseph Jude](https://jjude.com/). Be sure to subscribe on [Apple Podcast](https://podcasts.apple.com/in/podcast/gravitas-wins-radio/id1562802449), [Spotify](https://open.spotify.com/show/7LOBjAyjBkechMC0xMIj9E),[Google Podcast](https://podcasts.google.com/feed/aHR0cHM6Ly9mZWVkcy50cmFuc2lzdG9yLmZtL2dyYXZpdGFzLXdpbnMtcmFkaW8=),[YouTube](https://www.youtube.com/gravitaswins), or the podcasting app of your choice._

Please check out other episodes of [Gravitas WINS Radio](/gwr-themes/). For easy consumption, they are categorized by themes. If you are a fellow podcaster, you might be interested in [How I Podcast](/how-i-podcast/) or [Tools I Use](/i-use/).

## Continue Reading

- [Regi Thomas on 'Investing for short-term goals'](/regi-short-invest/)
- [Rishi Jiwan Gupta on 'Financial Planning For A Meaningful Retirement'](/rishi-retire/)
- [Building family culture](/family-culture/)