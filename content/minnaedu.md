---
title="Here is a blog engine for Google AppEngine"
slug="minnaedu"
excerpt="Simple blog engine that can run on Google AppEngine and anywhere else Python can run."
tags=["myapps"]
type="post"
publish_at="28 Dec 11 16:30 IST"
featured_image="https://cdn.olai.in/jjude/minnaedu-gen.jpg"
---


How about finishing the year with an achievement?

Wouldn't that be a great feel-good motivator for the new year? Well it is.

As part of learning [web2py](http://www.web2py.com), [Python](http://www.python.org) and [Google AppEngine](http://code.google.com/appengine/), I have been creating few applications. Earlier I created a [checklist](http://123-check.appspot.com/) application, which I'm happy about. Since beginning of December, I worked on a simple blog engine. Requirements were based on my itch and they are :

1. Should support writing in [Markdown](http://daringfireball.net/projects/markdown/). I have been using [Markdown](http://daringfireball.net/projects/markdown/) for all my writing. An additional benefit of writing in a pure text format is that you got a local copy of all your writings.

2. Just a single author system.

3. Commenting is non- essential. Ratio of quality comment that adds to the discussion to time spent on spam is low. I don't mind taking the discussion to [Twitter](http://www.twitter.com/jjude) or [G+](http://gplus.to/jjude), if required.

4. Have a minimal design. Yes, I want to learn CSS but I also like to limit the learning to few topics at one moment so that I can learn them well. Also I have found that minimalistic blogs are pleasant to the eyes :-)

Having laid out these simple requirements, I went about developing it. Due to workload and family commitments, I could spend only an hour or two at max a day, some days even that was not possible. But the desire to create something of my own kept my enthusiasm high. The intuitiveness of web2py design should be commended since I didn't spend any time in picking up from where I left. There was no time lost.

I set myself a deadline - I should complete the blog engine before Christmas.

When I started, I wasn't sure I will be able to complete. But am happy that I was able to meet the deadlines I set to myself. In fact, I did better. My biz blog was running on [wordpress](http://www.wordpress.org) and I was even able to import [wordpress](http://www.wordpress.org) entries (though I would consider it was only about 90% success).

On 24th, I had the running code and launched the application on [Google AppEngine](http://code.google.com/appengine/). I called it Minnaedu - a [Tamil](https://en.wikipedia.org/wiki/Tamil_language) word for online journal. I played with it few more times before announcing it in [web2py forum](http://groups.google.com/d/topic/web2py/TJ2Ww8Fk2EE/discussion) on Dec 25th.

Soon after I worked on making it work outside of [Google AppEngine](http://code.google.com/appengine/). I was hosting my blog on webfaction servers (my webhost at that time). Though webfaction doesn't provide quick-install method for [web2py](http://www.web2py.com) as they provide for [django](https://www.djangoproject.com/), they do provide a script. Few clicks here and there and voila, I got my blog in my own blog engine.

That is when I understood the power of [web2py](http://www.web2py.com) - I could run the same codebase on [Google AppEngine](http://code.google.com/appengine/) and webfaction (only change was database connection string).

This blog is still running on [wordpress](http://www.wordpress.org). Migrating this to minnaedu is less priority than getting minnaedu well tested and having the deployment taken care by [fabric](http://docs.fabfile.org/en/1.3.3/index.html).

Overall it is a great way to finish the year.

Wishing you create something useful for all - you and the tribe you chose to be with.

**Happy New Year**

_update: I'm not using this blog engine anymore._

