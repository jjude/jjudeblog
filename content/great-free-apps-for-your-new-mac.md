---
title="Great Free Apps For Your New Mac"
slug="great-free-apps-for-your-new-mac"
excerpt="Great free applications for your Mac"
tags=["tech"]
type="post"
publish_at="02 Aug 12 16:30 IST"
featured_image="https://cdn.olai.in/jjude/great-free-apps-for-your-new-mac-gen.jpg"
---

There is a perception that Mac OS is very expensive. It is true that the hardware is expensive than other available PCs; but it is also true that the stability of the system compensates for the price over time.

On the software side, there are some great paid apps, like [Omnigraffle](http://www.omnigroup.com/products/omnigraffle/), but Mac OS developers have also produced some great free applications. Here are the free applications that I use regularly:

[Netnewswire](http://netnewswireapp.com/): A comprehensive offline RSS feed reader with [Google Reader](http://reader.google.com) integration. Does one thing and does it well.

[Chrome](https://www.google.com/intl/en/chrome/browser/): A great, cool browser from Google.

[Notational Velocity](http://brettterpstra.com/project/nvalt/): [nvAlt](http://brettterpstra.com/project/nvalt/) is my default note taking application, containing hundreds of notes synced with [Simple Note](http://simplenoteapp.com/), which in turn syncs with my iPhone, making all of the notes readily available all the time.

[Dropbox](https://www.dropbox.com/): The service that made cloud sync as a fashion.

[Skydrive](https://skydrive.live.com/): As a user of [MS Office](http://www.microsoft.com/mac/products) for Mac, [Skydrive](https://skydrive.live.com/) becomes default choice to store and share [MS Office] docs.

[Skitch](http://skitch.com/): When I have to annotate images and screenshots of web-pages, I turn to [Skitch](http://skitch.com/).

[Evernote](http://evernote.com/): This used to be my choice tool for saving notes, but now that I use [Notational Velocity](http://brettterpstra.com/project/nvalt/), [Evernote](http://evernote.com/) is only used for rich text notes (the ones with images and tables).

[Mindnode Lite](http://mindnode.com/): This lite version is sufficient enough for my needs of mind mapping. My needs are simple like the one, I made for book review about [The McKinsey Way](/mckinsey-way/).

[Unarchiever](http://wakaba.c3.cx/s/apps/unarchiver.html): This little tool understands every compression format including .7z.

[Yemu Zip](http://www.yellowmug.com/yemuzip/) : When Finder compresses a file or a folder, it includes quite a lot of hidden files. Obviously, when it is unzipped by your Windows colleagues, they get confused with all these hidden files. [Yemu Zip](http://www.yellowmug.com/yemuzip/) zips without these hidden files, keeping sharing of files clean.

[Skype](http://www.skype.com/intl/en/home): Video conferencing from desktop. Seems to work even on lower bandwidth.

[Skim](http://skim-app.sourceforge.net/): In my profession, I need to read lots of documents, mostly in pdf format. [Skim](http://skim-app.sourceforge.net/) makes it easy to annotate as I read through. It has the feature to create a pdf file with annotations or export annotations separately as text file.

[Appcleaner](http://www.freemacsoft.net/appcleaner/): Uninstalling in Mac is as easy as dragging the app to the Trash. But the configuration and other support files are left behind. [Appcleaner](http://www.freemacsoft.net/appcleaner/) ensures all files are deleted.

[VLC](http://www.videolan.org/vlc/index.html): Player of almost all  video formats available today.

[Kindle](http://www.amazon.com/gp/feature.html/ref=sv_kinh_1?ie=UTF8&docId=1000493771): e-Book reader from Amazon.

[Flux](http://stereopsis.com/flux/): Tool to automatically adjust lighting of your Mac so that your eyes aren't hurt when working late nights.

[Ommwriter](http://www.ommwriter.com/en/free-download-mac.html): A minimalist full screen writing app if you want to write without any distraction.

[Carbon Copy Cloner](http://www.bombich.com/): Though _Time Machine_ is the default backup utility for Mac, I somehow still stick to [Carbon Copy Cloner](http://www.bombich.com/). I've never tried to restore back from the backups but I do browse through the backup folders and I've found them to be okay.

[Sync Two Folders](http://throb.pagesperso-orange.fr/site/ind_JS.html?Prg_S.html&Prg_AutresRB.html#SyncTwoFolders): [Carbon Copy Cloner](http://www.bombich.com/) is used for full backup, but [Sync Two Folders](http://throb.pagesperso-orange.fr/site/ind_JS.html?Prg_S.html&Prg_AutresRB.html#SyncTwoFolders) is used for syncing photos & videos to another set of backup drives. It has features to simulate before real syncing which is helpful when you sync after a prolonged delay.

As web & desktop developer, I also use quite a lot of development tools. Some of the best free tools for software development under Mac are:

[Source Tree](http://www.sourcetreeapp.com/): Source control client for both [Git](http://git-scm.com/) & [Mercurial](http://mercurial.selenic.com/).

[Xcode](https://developer.apple.com/xcode/): Apple's IDE.

[Filezilla](http://filezilla-project.org/): File transfer tool.

What are the free tools that are not in this list, that you would recommend?

