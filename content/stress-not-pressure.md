---
title="Stress is not pressure"
slug="stress-not-pressure"
excerpt="Pressure is good. Stress is not. You grow with pressure; You break with stress."
tags=["gwradio"]
type="post"
publish_at="15 Mar 23 06:05 IST"
featured_image=""
---
<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless src="https://share.transistor.fm/e/5def0924"></iframe>

Stress is not pressure. Pressure is good, stress is not.  
  
**Pressure makes you look for resources** that will help you do more than what you thought you can. Let me give you couple of scenarios.
  
You are asked to deliver a presentation at the last minute because one who has to deliver fell sick; or you have to speak up against a bully in office; or you have to fire an employee who isn't performing even after all the training and coaching.  
  
All of these situations put you under pressure. Even though you might think you are unqualified to handle them, you eventually rise up to the challenge and even meet them better than you thought could do.

Just last week, a client team visited us. It was a jam-packed week. It started on Monday morning. Every day we worked long hours. And we said our good-byes on Friday night at about 11.30. Were we under pressure? Absolutely. But were we under stress? No. Because we had assembled a **highly competent team and we had prepared well** before their visit. Most importantly, every day, we accomplished more than what we thought we could do. Everyday we had a sense of satisfaction and looked forward to the next day.

Stress on the other hand, is **a sustained expectation someone places on you beyond your capabilities and available resources**. When your capabilities and available resources can't meet the challenge, you panic and that leads to stress. When that situation continues, you break.

Pressure is good. Stress is not.
  
You grow with pressure; You break with stress.

What do you think? I would love to hear your comments of this episode. Feel free to send me your comments via email or or via any social media platform.

Thank you for listening. Have a life of WINS.

Thank you for listening. Thank you for your support. Have a life of WINS.

## Continue Reading

* [Wrong way to assemble the best car](/wrong-way-for-best-car/)
* [Who are you chasing?](/who-chase/)
* [Do you have a board of advisors?](/board-of-advisors/)