---
title="Sathyanand on 'The Visual Solopreneur'"
slug="sathya-visuals"
excerpt="Learn how to build an online business in the creator economy with this masterclass"
tags=["biz","gwradio"]
type="post"
publish_at="11 Jul 23 14:54 IST"
featured_image="https://cdn.olai.in/jjude/sathya-yt.jpg"
---
Whether you are an executive or an entrepreneur, you face two challenges all the time: how to solve the problems at hand or capture new opportunities. You need all the leverage that you can gain to beat the competition. Visual thinking and communication is one such leverage. 

My guest today Sathya has made it his mission to help executives and entrepreneurs in using visuals as a leverage. We discuss tools, success stories, and ways you can get started. Along the conversation, we also discuss running a solo-business.

Hope the conversation is helpful to you.

<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless src="https://share.transistor.fm/e/a98ae77b"></iframe>


## Quotes from the podcast

> You can be a good fan of many things, but great fan only few things. I'm a good fan of Sketchnoting. I'm a good fan of writing. But I'm a great fan of this visual form of communication.

> When you are a good fan, you enjoy. When you are a great fan, you want to recreate.

> Everybody is a derivative. I wanted to be a poor version of Jack Butcher. I became a better version of myself.

> When you can sleep calmly in the night, you've lived a good life.

## Edited transcript

### How Sathya got started with business?

Many people can relate to this because it all happened because of Covid. Previously, I had my own consulting agency. I was working with a business partner, but unfortunately, our business flopped due to Covid and various reasons. I didn't have anything else to do, so the only opportunity I had was to join Twitter and figure things out. 

That's when I discovered this amazing, vibrant creative community on Twitter. Maybe it's not as prominent today as it was before. During that time, I came across someone named Jack Butcher. He had a wonderful handle and a brand called Visualize Value. I became fascinated by what he was doing. He takes these incredible concepts, like philosophy or particularly, he gained a lot of fame by visualizing Naval Ravikant's ideas.

I was so captivated by his visualizations that I also wanted to create something similar. It started as a simple passion, and then I began making them. People started liking what I created, and eventually, they started asking me if I could do it for them. They even paid me for it. That's when I realized, "Hey, there's something here." And that's how we ended up where we are today.

### Good fan & Great fan theory

I have a sort of theory, so I call it "good fan" and "great fan." You can be a good fan of many things, right? You can enjoy music, songs, and so on. The analogy I like to use is going to a hotel. You eat a delicious dish, let's say a tasty biryani, and you really like it. You love it, and you appreciate the chef by giving a five-star review. That's being a good fan.

On the other hand, a great fan is like my mom. She would ask about the ingredients, the spices, and the recipe for that dish. She would want to recreate it at home. 

You have to differentiate between being a good fan of a particular art form or craft and being a great fan. Being a great fan means you can't help but recreate it yourself. This is how you can discover your niche or a passion that goes beyond being just a fan, and you want to build a career out of it.

In my case, I became a great fan of the micro-visual, the atomic visual that people are talking about, and I wanted to recreate it. I'm also a good fan of many things. I enjoy sketchnoting, and I'm a good fan of writing. But when it comes to this visual form of communication, I am a great fan.

### How Sathya found first clients?

It primarily came through Twitter, and it continues to come through Twitter. Fortunately, I have the gift of receiving a lot of inbound queries.

People often underestimate the luxury we have on platforms like Twitter or LinkedIn. Let me explain. I used to work in sales, not directly but as a manager and a training coordinator, accompanying salespeople. The traditional sales cycle is quite long. You're asked to wait and go through various steps. However, people fail to realize the luxury we have because of the internet.

When you're on Twitter, for example, you're actually placed on a pedestal. Just by sharing some random words, you don't have to talk to people or have charisma. By putting your work out there, you have the opportunity to be seen by millions of people. Of course, this depends on timing and algorithms. Increased visibility enhances your credibility and authority, allowing you to showcase your skills to a vast audience.

If they're interested, they may reach out to you or vice versa. You can approach others and say, "Hey, I do this kind of work. Would you be interested?" You can even provide samples like, "Hey, I did something like this." That's how I started initially.

Let me take the example of Jack Butcher. He was nobody on Twitter until he took Naval Ravikant's concept, created visuals for it, and tagged Naval Ravikant. Naval liked it and retweeted it. The network effect blew up Jack's account, and now he's one of the top creators in the entire economy. I tried to emulate that. I tagged a few people and even did something for Rogan. Although he didn't retweet it, there were others who liked and retweeted it. They appreciated my style of work, and this increased my credibility and visibility on the platform.

Eventually, some people became interested and asked, "Can you do it for me?" Alternatively, I reached out to them and said, "I can do it for you. How much are you willing to pay?" We handled the payment through platforms like PayPal, eliminating the need for Upwork or other freelancing platforms. I've never been to those platforms or used them.

It has been a gradual process. I started with offering a minimal tier-one service, like creating five visuals. Then, I expanded my services beyond visuals to include scheduling those visuals on clients' social media platforms. I even became a ghost scheduler, helping clients plan their social media calendars. This allowed me to upsell and offer higher-level services. It transformed from a $100 service to a $1,000 consulting gig, and eventually evolved into a retainer fee model. That's what I'm currently aiming for.

Last year, it worked well, and it's still working this year. However, now I'm focusing on offering packages instead of individual $100 services. I say, "I can only do a $1,000 service. If you're interested, you can join." Each person needs to figure out their own path. We're all learning. Eventually, you reach a point where everything starts moving in the right direction.

### Initial challenges in business

In a very practical sense, you won't have an immediate cash flow when starting an initiative. Unfortunately, that's the reality every entrepreneur or solopreneur has to deal with. In 2021, I told myself that my previous consulting business wasn't working, and it was time to explore my creative side.

In 2021, it seemed like everyone was on a break. So I gave myself the freedom to explore this avenue, knowing that I had a buffer. I had some savings, so I wasn't running around in desperation. I had a minimal buffer. Moreover, my family had a minimalist lifestyle, so our requirements were not excessive. I didn't have a huge mortgage or loan payments to worry about. These factors need to be carefully considered. Understand your own cash flow requirements and build that buffer, if possible. I'm using the term "luxury" in a specific context here.

The second important aspect is actually mastering the craft itself. There are thousands of people trying to establish themselves on Twitter and social media, attempting to turn it into a business. Not everyone succeeds. Some end up going back to applying for jobs and returning to the corporate world. There's nothing wrong with that. However, you need to be prepared externally with a buffer and internally with enough grit to persevere. These two factors, in my opinion, are crucial.

### Mastering the art

I didn't start with a theory, but later on, I realized that my approach aligned with a validated theory. Essentially, it involves collecting, copying, and eventually creating. So the first step is to collect your own mini library of crafts that you're a great fan of. That's the key. You don't have to do this for everything. Focus on the things that truly inspire you and that you want to master. 

Initially, you gather those great pieces that resonate with you. Then, in the next step, you start copying and reworking them. Personally, I was trying to recreate Jack Butcher's style using PowerPoint, as that was the medium I was familiar with. In fact, I went as far as listening to Jack's earlier podcast interviews. I was truly engrossed, studying not only the business aspect but also the craft itself.

One specific interview that stuck with me was his conversation with David Perell. David, in his characteristic inquisitive manner, asked Jack about creating a particular visual. Jack actually gave a demonstration, breaking down the process and sharing insights. I took notes and saved them in my Evernote. I observed his techniques and compared different elements, learning how he approached it. It's worth noting that he didn't rely on fancy software or tools, despite having a degree in graphic design. 

The next step is to imitate and copy them. Everyone starts as a derivative, but through this process, I aimed to become a poor version of Jack Butcher and eventually evolved into a better version of myself.

### Mistakes people do while starting

First and foremost, I think it's important to avoid overthinking and getting overwhelmed. In the visual thinking and learning industry, there is a certain expectation for beautiful, aesthetically pleasing illustrations, particularly from those with an artistic background. However, you don't have to be an amazing illustrator to embrace visual thinking. You don't need an iPad or advanced drawing skills. Let me give you an example. I can't draw well, and that's why I appreciate people like Dan who purposefully create what some may consider "ugly" drawings. It's not about the image itself, but rather the idea conveyed. The concept behind the drawing holds more importance.

Don't let yourself become overwhelmed by the abundance of stunning sketch notes and visuals you see. That's why I admire Jack Butcher. He primarily uses minimalistic geometric shapes. There may be only ten types of shapes involved, and you can mix and match them. It's about the thinking behind the design, not the design itself. In fact, you can recreate almost everything Jack or I have created using PowerPoint. It's not about the design part, as I mentioned earlier. It's about how you interpret and reinterpret the concepts.

With that in mind, avoid over-embellishing your work. Don't feel the need to beautify or add excessive colors. Stick with a white background and black color. You can introduce color later if needed. Don't overthink your choice of font either. I personally appreciate dual chrome or monochrome art styles. That's what I understand and resonate with. It's important to note that I'm not a graphic designer. I'm an information communicator who utilizes visuals to convey information. The visuals serve as a vehicle for communication, not the focal point.

During my workshops, someone once expressed gratitude when I clarified that they don't have to draw. They had assumed that the workshop would teach them drawing skills. I want to emphasize that I don't possess strong drawing abilities either. Instead, we can utilize existing visual libraries and organize them in a way that makes sense to both you and your audience. 

Our focus is on making an impact, not merely entertaining.

### When are visuals effective?

Visuals are most effective during the awareness phase. They help elaborate and create a sense-making space, whether it's for yourself or for an audience. However, there are instances where a more elaborate and long-form approach is necessary, such as technical documents that require preservation for future reference. In such cases, visuals can still be incorporated, but the content should primarily be in long-form.

Here's where we encounter another challenge within the visual thinking community. It's not a matter of visual versus verbal. Instead, it's about combining both elements. Our goal is to enhance understanding, not create an artificial conflict between visual and verbal forms. It's best to embrace both in tandem, avoiding unnecessary dichotomies.

During the awareness phase, visuals can be utilized effectively. Then, as you progress, textual information can be integrated seamlessly. The key is finding the right balance between visuals and text to convey your message clearly and comprehensively.

### Tools for visual thinking and communication

Certainly, the first tool I recommend is a notebook. I'm a big advocate for taking notes in a bullet format. It provides a different perspective on organizing your content compared to traditional paragraph-style writing.

The second tool or process I suggest is mind mapping. It's a highly accessible form of visual thinking where you don't need to draw extensively. Instead, you place existing phrases and keywords on a radial structure. Mind mapping, in my view, seems to be the most accessible option. 

However, if mind mapping doesn't resonate with you, there's another technique called cluster mapping. It's a step before mind mapping and is often used in English writing classes as a pre-writing exercise. These exercises, including listing, help capture the non-linear way of looking at things. You can even use them as index cards, jotting down key points. Nowadays, there are digital versions available as well. For example, there's a tool called X Mind, of which I'm an ambassador. 

Another option is using a whiteboard format on Canva app to quickly capture ideas. Digital sticky notes are also available. Miro is a great collaborative platform that supports group thinking, and I personally use it for creating apps when communicating with external audiences. Additionally, I utilize Notion for organizing documents.

These are just a few examples of the wide variety of tools available. However, I want to emphasize that you shouldn't get stuck on the tool itself. Ultimately, it's crucial to focus on the process and, more importantly, the problem you're trying to solve. Choose the process that works best for you and be committed to it. That's my suggestion—start somewhere and stay connected to the problem you're addressing rather than being overly attached to the tool itself.

### How to  stand out in the crowd?

Let's actually reverse the perspective. Ask yourself, why do you want to stand out? Do you want to stand out for a specific audience? Knowing this can provide more clarity. However, it's important to recognize that by taking a stand, you might end up offending some people. It's something we have to live with. There have been enough criticisms directed at me, questioning my approach. Some say, "This is not even visual. You're not using multiple colors. You're not drawing; you're simply taking from an existing library. Why are you using Canva? You should use Figma and create your own design system. But here's the thing: I don't want to use Figma and create my own system. The Canva app already has a library, and I'm utilizing it while adding my creativity through placement.

All of us will face criticism as long as we appeal to a specific target audience we want to address, engage, and communicate with. Initially, I had aspirations of becoming an influencer with millions of followers. Then I thought, "Okay, maybe I can aim to become a micro-influencer with 10,000 followers." But now, my perspective has shifted. I don't want to pursue those goals. Instead, I focus on leveraging my creativity to generate income. It's not about followership, standing out, or chasing likes.

It seems that I resonate with a particular type of audience. As long as there's a positive cash flow and I can solve problems for myself and my clients using this vehicle, this tool, I find contentment. That's what matters to me.

### Having multiple revenue streams

To begin with, I must say that it wasn't all planned from day one. 

It was more of an experimental process. However, I eventually realized that there is a method to the madness. 

I remember you introducing me to the concept of a value ladder, where you have products at different price points: $10, $100, and $1,000. I visualized it as a pyramid and it stuck with me. 

You also mentioned the barbell strategy, which involves having both a $10 product and a high-value $1,000 product. The sales cycle for the higher-priced product would be longer, requiring a more personalized approach. In reality, you don't need a hundred or even a thousand customers. Just a couple of clients investing in a $1,000 or $2,000 service per year can suffice. Repeat the process the following year, and so on. 

The $100 product could serve as a lead generation tool, while the $10 product could be an entry-level version. And, of course, there can be free offerings to lead your audience in. I remember your metaphor of creating multiple gateways, like entrances to a stadium. This visual metaphor resonated with me. You helped me think in this strategic manner. 

So, by putting your work out there through social media, LinkedIn, newsletters, and other channels, you guide your audience towards purchasing the $10 product, then progressing to a $200 workshop, and ultimately leading to $1000 consulting. This seems to be the path that works. 

However, a mistake I made earlier was transitioning from a $10 product to a $1,000 product. Now, I realize it makes more sense to start with a higher-priced offering and then offer a downgraded $10 product. It's a more intelligent approach. This way, several things will happen. 

Firstly, with a high-ticket product, you can generate revenue more quickly and face less competition. You don't necessarily need to establish extensive online credibility. Instead, you can rely on personal connections to find clients with specific problems you can solve. 

Secondly, you'll gain valuable experience by working directly with clients, diving deep into their problems rather than just reading ebooks and understanding surface-level issues. This will increase your authority. 

Thirdly, you'll begin to identify patterns within the problems you encounter. Working with five clients, for example, can provide insights into the common challenges they face, allowing you to refine and develop your $100 and $10 offerings accordingly. While I acknowledge that it won't be an easy task, I believe it's a smarter way to approach it. 

Of course, we all dream of having a $100 product that sells to millions of people, but we should recognize that there's only one James Clear or Justin Welsh. Such success stories don't apply to everyone. If you choose to pursue that route, keep in mind that James Clear has been writing for 20 years before achieving his tremendous success in the past four years. Let's be mindful of these factors.

### Kindest thing anyone has done for Sathya

I could think of my father and mother. The kindest thing is accepting your mistake. 

### Manifestation of best leadership quality

I had a team leader named Krishna Murthy who played a significant role in my career trajectory. There was a time when I found myself in a job profile that was completely unsuitable for me, despite being talented and bright. It reached a point where I had to leave that job behind. Fortunately, within the same organization, I was transferred to another role. It was during the first week of this transition that I had a conversation with Krishna, and he shared with me his own experience of facing a similar challenge. He told me that he found his niche by mastering what comes naturally to you, rather than trying to master everything that comes your way. It resonated with me deeply.

I realized that I excel more in intellectual challenges rather than in managerial tasks or sales-related negotiations. I discovered that my strength lies in desktop work, to put it broadly. Krishna's ability to empathize with me and openly share his own journey of starting from a place of failure to leading an entire unit was incredibly inspiring. It instilled in me a sense of leadership and the belief that I, too, can find my path and thrive.

### Definition of living a good life

Perhaps a potential definition is when you can peacefully sleep at night. It's not about waiting for sleep to arrive, but rather reaching a tired state where sleep naturally embraces you. 

There was a time when I experienced a mini-crisis, struggling to fall asleep and having to force myself to do so. I would stay awake until one or two in the morning. However, things have changed recently. I no longer have to force myself to sleep; sleep comes to me. In my current season, this seems to be a significant aspect of how I define living a good life.

## Your feedback counts

Thank you for listening. If you enjoy the podcast, would you please leave a short review on Apple podcast or on YouTube? It takes less than 60 seconds, and it really makes a difference in finding this podcast. And it boosts my spirits. 😀

I would like to hear what resonated the most with you in this episode. Either you can email me or share it on social media and tag me. Thank you for your support.

Have a life of WINS.

_Gravitas WINS Radio is produced by [Joseph Jude](https://jjude.com/). Be sure to subscribe on [Apple Podcast](https://podcasts.apple.com/in/podcast/gravitas-wins-radio/id1562802449), [Spotify](https://open.spotify.com/show/7LOBjAyjBkechMC0xMIj9E),[Google Podcast](https://podcasts.google.com/feed/aHR0cHM6Ly9mZWVkcy50cmFuc2lzdG9yLmZtL2dyYXZpdGFzLXdpbnMtcmFkaW8=),[YouTube](https://www.youtube.com/gravitaswins), or the podcasting app of your choice._

Please check out other episodes of [Gravitas WINS Radio](/gwr-themes/). For easy consumption, they are categorized by themes. If you are a fellow podcaster, you might be interested in [How I Podcast](/how-i-podcast/) or [Tools I Use](/i-use/).

## Connect with Sathya
- LinkedIn: https://www.linkedin.com/in/sathyanands/
- Twitter: https://twitter.com/SathyaHQ/

## Connect with me
- Twitter: https://twitter.com/jjude
- LinkedIn: https://www.linkedin.com/in/jjude/
- Website: https://jjude.com/
- Newsletter: https://jjude.com/subscribe/
- Youtube: https://youtube.com/gravitaswins
- Email: podcast@jjude.com
- Executive Coaching Program: https://gravitaswins.com

## Continue Reading
* [Tanmay Vora on 'Mindset for Creator Economy'](/tanmay-creator-economy/)
* [Sampark A. Sachdeva On 'Linkedin Success Mantras'](/sampark-linkedin/)
* [How to amortize your efforts in producing content?](/amortize-content/)
