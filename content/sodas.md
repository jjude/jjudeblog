---
title="SODAS - A Simple Problem Solving Methodology"
slug="sodas"
excerpt="Introducing a simple problem solving methodology."
tags=["problem-solving","coach","visuals"]
type="post"
publish_at="07 Feb 11 16:30 IST"
featured_image="https://cdn.olai.in/jjude/build-wealth.png"

---

Fundamental requirement for solving business problems is structured thinking. SODAS - which stands for Situation, Options, Disadvantages, Advantages and Solution, is a simple methodology facilitating such structured thinking for problem solving.

![SODAS problem solving](https://cdn.olai.in/jjude/build-wealth.png)

Different components in the methodology are:

- **Situation** : Define the situation. It can be a crisis that needs a clean-up (like a stalled project launch) or a future desired state (increased market share).

- **Options** : Generate as many options as possible. Your options could be [MECE](/mckinsey-way/) (mutually exclusive, completely exhaustive) or independent & complete. You need to generate as many options as possible for a better solution.

- **Disadvantages & Advantages** : For each of these options generate both disadvantages and advantages. It is essential to generate at least three of these. Why at least three? It is possible that as we generate the options, we come to like or dislike some options. If we like an option, we will tend to generate only advantages and fail to see any disadvantage. Similarly for options that we dislike. If we don't have the positives and negatives of these options, we may end up with an ineffective solution. So it is necessary to think through the advantage and disadvantage of each option.

- **Solution** : You can't [copy paste](/beware-of-cloning-best-practices/) a best practice. **Context** matters. Information from the above steps should help you in choosing a workable solution for the situation.

Keep in mind that SODAS does not arm you with techniques to generate options or methodology for implementation. It simply provides a structure to go from a situation to a solution.

Another factor to keep in mind is that it is not a single left-to-right journey. As you generate the disadvantages and advantages you might get creative and think of more options. So it is an iterative process.

Go forth and start applying SODAS.

_This post is part of ‘[Be a Problem Solver](/be-a-problem-solver/)‘ series._

## Continue Reading

* [Structured Thinking](/structured-thinking/)
* [Structured Communication](/structured-communication/)
* [Smartcuts](/smartcuts/)