---
title="I used Android for ten months, and I don't like it"
slug="android"
excerpt="When it comes to privacy and usability, Android is no match to iOS"
tags=["tools"]
type="post"
publish_at="28 Oct 19 08:30 IST"
featured_image="https://cdn.olai.in/jjude/android-gen.jpg"
---


I used the iPhone for six years. Then this year (2019), I switched to Samsung S9. Not as an additional phone, but as the only phone. Because, as a technologist, I wanted to have the first-hand experience of the Android platform. It is hypocritical to diss a platform without using it.

Now that I have used it for ten months, here are my notes:

- The best thing is the "work profile" feature. I can isolate the apps I use at work. No worry about emailing a client or colleague from a personal email id.
- Next best thing is the camera. S9 camera produces terrific pictures.

Other than these two positives, I have not had any 'aha' moments with Android. It is the opposite. I have had many frustrating moments using Android. Here are the things I don't like in Android / S9.

1. There are not many well-crafted apps in Android.  Take example the banal stuff like Twitter. I was even ready to pay money. But there is no decent app like Tweetbot for Android. Or take a simple note-taking app that integrates with dropbox. None. Even iAWriter fails. Technically it integrates with Dropbox, but the user experience is not as smooth as iOS.
2. I tried removing stock apps. I am not even talking about Google services. I am talking about third-party apps like Facebook. I can disable it, but can't uninstall. If it is sitting there, who knows if it is leaking some info about my usage?
3. There is always an uncomfortable feeling that every app is stealing some data from you. I have installed the Bouncer app to control permissions given to applications. Still not sure if during the usage of the app, they do something nasty.
4. Maybe it has to do with the underlying Android OS permission system. I can't figure out why a banking app needs permissions to access 'contacts.'
5. I have disabled "Auto Update" in the settings. Still, I see certain apps automatically updated. Some not. Don't know why.
6. There is a tap to top action in iOS. Such a feature is not available in Android. It's a pain when you are reading a long article in pocket or reading streams of tweets.
7. Even when internet connectivity is present (like I can browse websites), some apps like (journaling app, journey) will complain there is no internet connectivity.
8. Accessing local servers on the iPhone is easy. Connect to wifi, and you are done. You can access the local dev server on your iPhone. It is not so easy in Android. Refer: https://developers.google.com/web/tools/chrome-devtools/remote-debugging/local-server
9. Sometimes location gets turned on automatically. I read that if I connect to wifi, it turns on. It looks as if Google wants to track location, so it turns it on, so it can track unsuspecting victims.

The biggest downer with iPhones is the price. It's outrageous, a daylight robbery. But when it comes to privacy and usability, Apple still reigns.

Have you used both platforms? Why do you choose one for the other?

### Also Read

- [What Is A Tool?](/what-is-a-tool/)
- [An User's Perspective on OSes: Horrible, Bearable, and Adorable](/oses-pov/)
- [How I Came To Own An iPad?](/how-i-came-to-own-an-ipad/)

