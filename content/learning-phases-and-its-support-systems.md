---
title="Learning Phases And Its Support Systems"
slug="learning-phases-and-its-support-systems"
excerpt="Do you have a disciplined and structured routine to become an expert?"
tags=["sdl","systems","wins","insights"]
type="page"
publish_at="12 Jan 13 16:30 IST"
featured_image="https://cdn.olai.in/jjude/learning.png"
---
We live in an era where information on any topic—literature, science, project management, programming—is just a click away. But, reading through pages and pages of information doesn't build your expertise. You need a disciplined and structured routine to become an expert.

A structure that I've been practicing to learn new fields is 'consume-produce-engage'.

In the consume phase, you learn concepts, ideas, and other fundamentals of the domain. These may be learned through classroom sessions, workshops, books, podcasts and videos. When learning fundamentals it is better to learn from the best, as they usually have a knack of simplifying even the complicated concepts.

Consumption makes you a storehouse of ideas; but it is when you take time to synthesize what you gained to produce something of value, these ideas crystallize in you. As you create, you may realize a gap in your understanding and go back to the original reference for further learning. Only, this time you'll study with reference to the context, thus crystallizing the learning. There is no better measurement of learning than producing successful products.

By consuming you learn and by producing you crystallize. But by engaging others—in conversation, discussion and debate—you invite other's perspectives on what you learn. Others' perspectives will either augment or challenge your idea of the topic. Either way, you'll enrich your learning.

This routine has to be repeated continuously until...well, there is no end to learning. Right?

For this routine to continue, it has to rest on three essential supporting elements: the learner, environment, and tools.

![Learning Phases](https://cdn.olai.in/jjude/learning.png)

Fundamental to your learning is, your **beliefs and discipline**. A belief that a topic isn't too high for you will get you started; and, discipline will keep you on the course through highs and lows of everyday life. You need to back your beliefs with intense, sustained efforts.

We don't survive as an island. We live in groups and we need each other to achieve our goals. So it is essential to build an **environment** that is **encouraging** and **forgiving**; alternatively, you can move to such an environment. Whether you build your own environment or move to one, you need an environment that gives you ample space to make mistakes and correct them.

Finally, you need to tap **readily available and facilitative tools** to meet your learning goals. Tools help you in every stage of the learning path—they help you discover the best sources in a topic, synthesize different sources to build your own product, take your product to the right set of people and then to engage them in debates and discussions. Best of the tools hide their complexity and let you do whatever you do with ease and speed. Go after those tools.

When all components of this system work in their proper propositions, you learn well.
