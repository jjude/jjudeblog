---
title="How I Use Twitter As A Learning Tool?"
slug="learn-via-twitter"
excerpt="Of all the social media platforms, Twitter stands-out as a great place to discover and discuss ideas. Use it."
tags=["sdl","wins","insights","consume"]
type="post"
publish_at="14 Feb 14 16:30 IST"
featured_image="https://cdn.olai.in/jjude/learn-via-twitter.png"

---

I've designed consume-produce-engage as a framework for self-learning. I have learned & improved skills in product-development, investment and blogging using this framework.

Twitter, as a tool, fits well into each of these learning segments.

First recap of the [consume-produce-engage][11] learning framework.

In the 'consume' phase, I learn concepts and fundamentals of the domain I'm learning.

In the 'produce' phase, I make sense of whatever I have learned in the consume phase and produce something of value. Generally value generation is possible only by synthesising ideas from multiple domains, not just from the domain I'm interested in at that time.

By consuming I learn and by producing I crystallise. In the 'engage' phase, I seek out other's perspectives. Other's perspective will either augment or challenge my idea of the topic. Both enriches my learning.

Now to the topic of this post — how I use twitter for learning.

!["Learn Via Twitter"](https://cdn.olai.in/jjude/learn-via-twitter.png)

### Consume

With millions of twitter users, chances of finding an expert in any domain is high. Twitter bio plays an immediate role in identifying experts but today everyone is an expert. I browse through their tweets, their interactions and their blog(s) to confirm if I should follow them on twitter. If their tweets are only of 'Thank you for following me,' and links to their blog posts (like Seth Godin), I don't follow them. Twitter isn't and shouldn't be a one-way street.

Consider [Vala Afshar][15], [Umair Haque][2] or [Alan Weiss][3]. All of them are prolific twitter users, having embraced the constraints of twitter. They express stimulating thoughts within those constrains. Vala is exceptional. He can express his views as a list in twitter!

Only few can express themselves comprehensively within 140 characters. But when they do, it is educational.

When I like a tweet, I favourite it. I have connected [IFTTT][13] with twitter and [Evernote][10]. Any favourite tweet gets stored in an Evernote note automatically.

Then there are interesting links shared via twitter. Following my productivity hack, I usually limit accessing twitter to [Ecofon][7] in my iPhone. Ecofon has an integration with Pocket. If the tweet convinces me, I save the link to [Pocket][8] for delayed reading.

When I read articles in Pocket, I take one of three actions:

- Read & Delete
- Archive well written articles, if I think it would improve my writing
- Highlight, make notes, and store thought-provoking, productivity-enabling articles for future references

I use Google Chrome with [Evernote Clearly extension][9] for highlighting and [Evernote][10] for storage. I revisit these articles stored in Evernote regularly as [spaced repetition][14] increases retention of knowledge.

### Produce

When I started with Twitter, I thought, what could possibly be said in 140 characters; only gibberish, I supposed. It turns out, if you place a constraint, on your mind, it can quickly adapt and work within these limits well enough. Twitter forces you to think of shorter words; which in itself is good for writing.

Yet, I'm still learning to express myself succinctly within the constraints of Twitter.

### Engage

By nature, I'm inclined to debate ideas[^1] and also open to change my mind, if convincing arguments are put forth. Twitter, by its design, is the best platform for engaging with others and bounce off ideas.

Twitter is one big global town hall with discussions on range of topics going on simultaneously. You can walk around, listen to discussions that you like, drop off if discussion doesn't interest you or leave a quick, short, async message for others to leave you quick, short, async messages.

The lovable aspect of Twitter is that strangers are willing to engage with strangers on the other side of the globe on any topic. When I look at my recent interactions, I am amazed that I have not met any of them face-to-face (except Sastry).

As in any tool, there are dangers too. You could waste hours simply browsing or getting sucked by trolls. I usually [tie][12], my twitter activities to my mobile devices to avoid these dangers.

Of all the social media platforms, I identify myself well with Twitter. Its a great place to discover and discuss ideas.

[2]: http://twitter.com/umairh
[3]: http://twitter.com/BentleyGTCSpeed
[4]: http://bufferapp.com/
[5]: http://twitter.com/joelgascoigne
[6]: /we-dont-need-more-we-need-better/
[7]: http://www.echofon.com/
[8]: http://getpocket.com/en/
[9]: https://chrome.google.com/webstore/detail/clearly/iooicodkiihhpojmmeghjclgihfjdjhj?hl=en
[10]: http://evernote.com/
[11]: http://jjude.in/learn
[12]: /productivity-hacks-that-work/#tie
[13]: http://ifttt.com/
[14]: https://en.wikipedia.org/wiki/Spaced_repetition
[15]: http://twitter.com/ValaAfshar

[^1]: Even in physical world, I'm weary of gossips. I have transferred this habit to Twitter too.
