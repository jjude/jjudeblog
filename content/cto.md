---
title="What I learned as a CTO"
slug="cto"
excerpt="A self-reflective post about what each letter of the job title CTO means to me."
tags=["guest-post","coach","tech","sketch"]
type="post"
publish_at="08 Feb 17 13:58 IST"
featured_image="https://cdn.olai.in/jjude/hnocto.jpg"
---
[Hashnode](https://hashnode.com) is a digital meeting place for software developers. I have been following them since their first avatar as devmag. Recently, Sandeep, their co-founder, requested me to write for their **Hashnode Originals** section.

I chose to write about my experience as a chief technology officer. After trying in few different ways to write the article, I chose to write the article in a self-reflective manner. I choose to write what each word of the title means to me and how I performed against that definition.

![CTO](https://cdn.olai.in/jjude/hnocto.jpg "CTO")

I liked how it turned out. Sandeep and team helped in editing it even better. The article is live now. [Read](https://hashnode.com/post/what-i-learned-as-a-cto-ciyvn6tjp00083h53frb6gp12) it and spread it.
