---
title="Jayaram Easwaran on 'Secrets of A Master Storyteller'"
slug="jayarame"
excerpt="When we think of corporate communication we think only in terms of power points and bullet list. But slides don't make you memorable. To stand out you need to master story telling skills."
tags=["wins","gwradio"]
type="post"
publish_at="14 Dec 21 06:00 IST"
featured_image="https://cdn.olai.in/jjude/jayarame.jpg"
---

<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless src="https://share.transistor.fm/e/3f846915"></iframe>


- Why did you choose a fiction format rather than the standard non-fiction format? 
- How do you source these stories?
- How do you go about writing? How many times do you re-write?
- Did you study creative writing? How did you get into this?
- How can anyone learn this skill? What courses/workshops helped you?


![Secrets of A Master Storyteller](https://cdn.olai.in/jjude/jayarame.jpg)

### Connect with Jayaram:

- LinkedIn: https://www.linkedin.com/in/jayarame/
- Jayaram's Business World Columns: https://www.businessworld.in/author/Guest-Author/Jayaram-Easwaran-90122/

## Connect with me
- Twitter: https://twitter.com/jjude
- LinkedIn: https://www.linkedin.com/in/jjude/
- Website: https://jjude.com/
- Newsletter: https://jjude.com/subscribe/
- Youtube: https://youtube.com/gravitaswins
- Email: podcast@jjude.com
- Executive Coaching Program: https://gravitaswins.com

## Your feedback counts

Thank you for listening. If you enjoy the podcast, would you please leave a short review on Apple podcast or on YouTube? It takes less than 60 seconds, and it really makes a difference in finding this podcast. And it boosts my spirits. 😀

I would like to hear what resonated the most with you in this episode. Either you can email me or share it on social media and tag me. Thank you for your support.

Have a life of WINS.

## Amazing tools I use:
- I record all the remote interview with [Riverside.fm](https://riverside.fm/?via=joseph-jude)
- I use [Transistor.fm](https://transistor.fm/?via=jjude) for hosting these podcast episodes

_Gravitas WINS Radio is produced by [Joseph Jude](https://jjude.com/). Be sure to subscribe on [Apple Podcast](https://podcasts.apple.com/in/podcast/gravitas-wins-radio/id1562802449), [Spotify](https://open.spotify.com/show/7LOBjAyjBkechMC0xMIj9E),[Google Podcast](https://podcasts.google.com/feed/aHR0cHM6Ly9mZWVkcy50cmFuc2lzdG9yLmZtL2dyYXZpdGFzLXdpbnMtcmFkaW8=),[YouTube](https://www.youtube.com/gravitaswins), or the podcasting app of your choice._

Please check out other episodes of [Gravitas WINS Radio](/gwr-themes/). For easy consumption, they are categorized by themes. If you are a fellow podcaster, you might be interested in [How I Podcast](/how-i-podcast/) or [Tools I Use](/i-use/).

## Continue reading
* [Tanmay Vora on 'Mindset for Creator Economy'](/tanmay-creator-economy/)
* [Krishna Kumar on 'Power of storytelling in business'](/kk-sotry/)
* [Sampark A. Sachdeva on 'LinkedIn Success Mantras'](/sampark-linkedin/)