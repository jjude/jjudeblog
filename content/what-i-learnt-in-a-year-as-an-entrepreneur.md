---
title="What I learned in a year as an entrepreneur"
slug="what-i-learnt-in-a-year-as-an-entrepreneur"
excerpt="Three lessons I learned in a year as an entrepreneur"
tags=["startup","coach"]
type="post"
publish_at="02 Sep 15 20:54 IST"
featured_image="https://cdn.olai.in/jjude/what-i-learnt-in-a-year-as-an-entrepreneur-gen.jpg"
---


Last year, I quit a consulting job and started a [venture][2]. The venture didn't succeed. Since [92% of startups fail within 3 years][1], it is not at all surprising, although it hurts to see my venture fail.

It is one thing to read about startups as a wantrepreneur, but it is an exhilarating experience to try your hand at building a venture, even if it fails. I have no regrets. As I look back, here are the three major lessons I learned in the last one year.

**Hobbyists are enthused with technology innovation, but entrepreneurs should solve a problem**. We got fascinated with the latest development in technology and built a product that fascinated us. Such products are good to talk about in geeks cocktail parties. To build a business, though, you need to solve a problem with that latest technology. That problem should also be acute enough that customers will write a cheque for a solution.

**Security business is a tough business**. We built a firewall which can be monitored via a mobile app. The mobile app had a dashboard to show the number of attacks that the firewall prevented. When we showed all the attacks, beta users got scared that there were many attacks on their network; when we showed less, they couldn't appreciate the need for such a product. I realized then, the plight of police force. If the crime rate is high, we blame the police for their ineffectiveness; if the rate is low, we question the need for police force because there is no crime. We quickly forget that the rate is low, because the police did their job well.

**David can't fight with the same armor as Goliath**. There is an insightful [verse][3] in the Bible, within the David and Goliath story. David first clothes himself with warrior's attire, then throws them away and goes to war with just stones. As I said earlier, our product was a firewall. Whenever we approached prospective channel partners, they would take out brochures of existing firewalls and demand every feature. We knew that if we built every feature in every other firewall, we will be just a 'me too' product. We didn't want to be a cloned product. We continued to search for partners who will understand the necessity of our solution. One day, our own sales team came and said, "build all these features, or we can't sell". That day, we declared "Game over".

I am not (yet) going back into the job market. I am planning to iterate as many times as needed to succeed as an entrepreneur. Currently, I'm building an audience engagement tool called [WiseCrowd][4]. I'm following the 50/50 rule mentioned in the [Traction Book][5][^6]. I'm making and marketing at the same time. The initial response has been encouraging. I hope to come back to this page next year and say I got a hit.

[1]: https://www.quora.com/What-percentage-of-startups-fail
[2]: /biggest-bet/
[3]: https://www.biblegateway.com/passage/?search=1+Samuel+17%3A38-39&version=NKJV
[4]: http://wisecrowd.in
[5]: http://amzn.to/1NWNFGs

[^6]: _Disclaimer: Link to Amazon book is an affiliate link_

