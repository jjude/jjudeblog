---
title="Airtel + Twitter = an easy, effective travel blog"
slug="airtel-twitter-an-easy-effective-travel-blog"
excerpt="If you want to keep notes while you travel, twitter can help."
tags=[""]
type="post"
publish_at="02 Mar 08 16:30 IST"
featured_image="https://cdn.olai.in/jjude/airtel-twitter-an-easy-effective-travel-blog-gen.jpg"
---

I like to travel - smelling new land, tasting native food, seeing new faces, visiting monuments, cuddling with nature and loosing oneself excites me too much to stay comfortable within four walls.

And when I travel, I like to write down what I see and how I feel when I see them. I tried to write at the end of day; didn't work. I tried carrying a notebook and write down taking short breaks during the trip; didn't work either. May be if I was BBC reporter or a [BootsnAll](http://www.bootsnall.com/) contributor, it would force me to write; but being a 'hobby' traveler, I find it difficult to log when I travel.

I've tried to write once back home. I could make some notes, but most often, I forget the 'little' details, which means that what I end-up writing is no different from the thousands of other 'informatory' articles on the net. So I've lived with the guilt so far.

Recently I heard of a popular micro-blogging site, [Twitter](http://twitter.com/jjude) and immediately registered there. Twitter provides an option to update via mobile - as it is a micro blog, it makes sense to update via mobile - one tend to keep it short.

Soon after that we, as a family, planned to go to [Wayanad](http://www.raincountryresort.com/) in Kerala. We were excited about the trip - it is to the beautiful 'God's-own-country'; a family trip in years; a needed break for all of us from our hectic work schedule.

Being a family trip, I wanted to spend as much time as possible, with the family rather than sitting and writing travel notes. Thanks to mobility provided by Twitter, I can send short logs to Twitter by way of SMS! So I carried my mobile, though I wasn't sure if there will be signal - despite Airtel's ads claiming, the coverage over sea, villages, mountains and forests. Its time to check that claim.

It came as a pleasant surprise that Airtel's claim was true - except for the short area between Karnataka & Kerala border, there was signal all through our trip - during trekking through forest near the resort, on crossing many rivulets in Kuruva island or when spending hours of quality time with the family in the resort. I kept sending short messages to Twitter without ever knowing if they reach the site - there is no Internet connection in the resort.

I checked on Twitter as soon as I came home. Wow! every single log was in. With Airtel and Twitter, I've captured all the 'little' details of our trip without spending too much time away from the family. Thank you, Airtel & Twitter.

_I don't know the feasibility of this option for international travelers and also for hard-core back-bag travelers. I am just writing what worked for me. And believe me, I am paid neither by Airtel or Twitter for this._

