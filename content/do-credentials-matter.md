---
title="Do Credentials Matter?"
slug="do-credentials-matter"
excerpt="One's credentials do not guarantee success; it's their attitude that makes or breaks a venture."
tags=["coach","sdl","wins","insights"]
type="post"
publish_at="18 Jan 14 16:30 IST"
featured_image="https://cdn.olai.in/jjude/credentials.jpg"
---
When I wanted to apply for PMP certification, I asked my boss at that time, if I should take up PMP. He said,

>"If your project fails, no one would bother you are a PMP; and if your project succeeds, even then no one would care you are a PMP."

I didn't take up PMP.

<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless src="https://share.transistor.fm/e/b2a9125e"></iframe>

Consider these:

1. [Michael Porter][6], a Harvard Professor and author of the famous [Porter Framework][7], co-founded Monitor Group. Monitor group advised large companies and governments on strategy and being competitive. But they themselves had to file for bankruptcy.

2. [Long-Term Capital Management][3] had Nobel Price winners for economics in its board. Yet, the fund had to be liquidated due to heavy losses.


Again consider these:

1. Group of illiterate men, called, [Dabbawalas][2] have been picking up lunches and delivering them on time for decades. Their on-time delivery with minimal error rate would put any six-sigma practitioner to shame.

2. [Kamaraj][5], the illiterate chief minister of Tamil Nadu, laid the strong  foundation for mass-education that made Tamils from rural areas reach epic heights.

3. Do you know that the world's largest burger restaurant, [McDonald's][8] in its present form was built by [Ray Kroc][9], a school dropout? He is not alone. There is plenty [more][10]—Steve Jobs, Bill Gates, Larry Ellison and the list goes on.

What am I saying? Am I saying, schools don't matter? Am I saying, education doesn't matter? Absolutely not.

I'm saying, the **belief that having a degree or a certificate is a sure path to success is wrong**. It's flawed.

![Do credentials matter?](https://cdn.olai.in/jjude/credentials.jpg)

Let me narrate an incident from my life.

For one of the project that I was managing, I needed a tester. I told the HR department to send me those with certification in testing. I interviewed few sent by the HR team. They had a "certificate," what I was looking for. But their knowledge was narrow, shallow and useless. I rejected them all.

Then my boss, sent Ashbel Roy. Ashbel was a Peoplesoft consultant. I was running a Java project and looking for a tester; my boss sends me a Peoplesoft consultant. I thought he lost his mind. When I complained, he said, "He is a good resource. Use him."

Ashbel turned out to be the best tester I have worked with. Not because of his credentials, but because of his attitude.

I have seen this repeat in my career again again. I have come to realise that, **one's credentials do not guarantee success; it's their attitude that makes or breaks a venture**.



[1]: /tags/qwa/
[2]: https://en.wikipedia.org/wiki/Dabbawala
[3]: https://en.wikipedia.org/wiki/Long-Term_Capital_Management
[4]: https://en.wikipedia.org/wiki/Licence_Raj
[5]: https://en.wikipedia.org/wiki/K._Kamaraj
[6]: https://en.wikipedia.org/wiki/Michael_Porter
[7]: https://en.wikipedia.org/wiki/Porter_five_forces_analysis
[8]: https://en.wikipedia.org/wiki/McDonald's
[9]: http://www.entrepreneur.com/article/197544
[10]: http://www.mytopbusinessideas.com/school-drop-out-billionaires-successful-entrepreneurs/
