---
title="How to choose technology for your business growth"
slug="tech-for-biz-growth"
excerpt="Your business will follow crawl, walk, run stages. Each stage will use different technologies."
tags=["talks","coach"]
type= "post"
publish_at= "24 Jan 20 10:30 IST"
featured_image="https://cdn.olai.in/jjude/tech-stack-idea-stage.jpg"
---

_CMR is a market research and event management agency. They invited me to speak at an HP launch event. This blog post is the edited version of the talk._

Technology is a great lever. You can launch a new product or reach new markets by leveraging technology. 

Traditionally, **software development takes months, if not years**. The process goes something like this: sales or marketing team identifies a market need, an analyst writes detailed requirements (which in itself could take months), hand over the requirements to the development team, and then wait months for the product to come up for testing. Sadly, when the product is ready for the launch, either the market dynamics or the key product feature would have changed, **making the product dead on arrival**.

![GTM Feedback](https://cdn.olai.in/jjude/gtm-feedback.jpg)

We now have a **better way of product development** for the last few years. Here the product is developed in an incremental and iterative cycle. At the start of the project, even before any development starts, you create a one-page document that describes the market need, how the product will satisfy that need. You circulate this document with the executives and a few confidant prospects. In this way, you can fine-tune product features and the pitch even before spending a penny on developing the product. This stage is what I call **"30% feedback" stage**. Your focus at this stage is on understanding the problem and the solution, not the beauty of the solution.

Once you have a clear understanding of the market need and product features, you can **start duct taping the solution** to get the initial version of the product into the hands of prospects to get quick feedback. You can iterate the product development in this fashion until the final product is ready for a bigger launch.

You might think that if I follow this process, it might take years before the final launch. It is true. It is also true that the popular Google product that we all use, **Gmail, was in the "beta" version for five years**[^1]. The advantage here is that the product is already in the hands of the early adopters, and you are getting feedback from them, which you can use to fine-tune the application.

So what technology can you use to build your product? Your product development will fall into three broad stages, which I am calling **crawl, walk, and run**. Each stage will use different technologies.

![Tech Stacks for each idea stage](https://cdn.olai.in/jjude/tech-stack-idea-stage.jpg)

### Stage 1: Crawl 

In the "crawl" stage, you are trying to validate that you have the right solution for the problem worth solving. You don't have to necessarily have "code" at this stage. If you read stories of successful products, you will find one theme repeated enough. They **composed their solution on top of existing products**. 

Find some time to read [Reverse Engineering A Successful Lifestyle Business](https://www.toomas.net/2017/07/18/reverse-engineering-a-successful-lifestyle-business-heres-everything-ive-learned-from-reading-indiehackers-com/). I'm quoting two of the points here.

Here is one who **duct-taped his product** instead of coding the product.

> I launched with zero custom software. I used Gumroad, Zapier, Memberful, and Campaign Monitor to basically duct-tape my own product together.

Some even go further. They don't even have a website:

>I wanted to start really simple, so I didn't even have a website when we launched. I just sent a PayPal subscription link to potential customers over email and then started sending candy to their address periodically.

Product Hunt, the popular product discovery portal, [began as an email list](http://ryanhoover.me/post/69599262875/product-hunt-began-as-an-email-list) in 2014. In 2016, it was sold for $20 million.

The options are getting better. You can browse through [Maker Pad](https://www.makerpad.co/) for solutions using no-code / low-code solutions. Whether it is a content management system, membership portal with payment, or even e-commerce, they have a duct tape solution for you.

### Stage 2: Walk

When you come to the "walk" stage, you already have enough customers paying for your solution, and more and more customers are signing up.

By now, you know all the product features and importantly business model that set you apart from your competition. It is time to move away from the duct tape solution and develop a solution that you own.

Even on the infrastructure side, you should **start small and scale as needed**. Long back, we have to configure whole servers to serve the solution. Now, you can begin with minimal infrastructure on a cloud provider like AWS and scale as the traffic builds up.

You should invest more in **monitoring, analyzing, and alerting** tools. Say you have an e-commerce application. Then invest in tools that tell you about the customer journey, pages where customers drop-off, and so on. Such monitoring will help you learn, iterate, and improve as quickly as possible.

### Stage 3: Scale

When you reach this stage, you'll **serve multiple customer segments with price-points relevant to these segments**. 

To support such a business, you'll need multiple technologies and layers of technology. At a minimum, you will need the product technology, sales and marketing technology, and support technology.


### Conclusion

The technology used to overwhelm business people. The improvements in technology have made it easy for anyone with an idea to launch and test their ideas. You don't have to be afraid of technology anymore.

I will conclude with this quote from Bill Gates: 

> We overestimate what we can do in a year and underestimate what we can do in a decade.

You can do a lot more in the coming decade using technology. Go and make the world a better place. Good luck.

_You can find the slides in [slideshare](https://www.slideshare.net/JosephJude4/technology-for-business-growth)._

### You might also like these

* [Three stages of startups and how to chose tech-stack for each of them](/stacks-for-startups/)
* [What small IT businesses can learn from Mary Meeker's internet trends report](/2017-trends/)
* [Agile in the C-Suite](/csuite-agile/)

[^1]: https://slate.com/news-and-politics/2009/07/why-google-kept-gmail-in-beta-for-so-many-years.html