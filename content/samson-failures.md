---
title="'How not to operate a startup' with  Samson Selladurai"
slug="samson-failures"
excerpt="A discussion with a serial entrepreneur on what we should be careful about and what we should avoid in the path of entrepreneurship."
tags=["gwradio","startup"]
type="post"
publish_at="05 Mar 24 20:08 IST"
featured_image="https://cdn.olai.in/jjude/samson-failures.jpg"
---
While we talk about entrepreneurship, most of the discussions are about success, growth, and all the glories associated with it.

It is said that we learn the best from our success and from other's failures. So here I'm talking to my friend Samson who can be rightly called as a serial entrepreneur to discuss what we should be careful about and what we should avoid in the path of entrepreneurship.

<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless="" src="https://share.transistor.fm/e/14eece77"></iframe>

## What you'll hear

-  Samson's ventures
-  Lessons from failed & successful ventures
-  Balancing passion and execution
-  Identifying problems to solve
-  Thinking about exits
-  "I'm safe" app
-  Kindest thing anyone has done
-  Best leadership quality
-  Definition of living a good life

## Main Points
Here are the main points discussed in the episode:

1. Entrepreneurship journey and challenges
    - Discussing the entrepreneurship journey of Samson, who started his first venture at 21. 
    - He shared insights about starting an energy drink company at an early age, the audacity and passion it required.
    - Emphasized that entrepreneurship is about waging a war against an injustice or vacuum, often formed from personal experiences and observations. 

2. Role of mentorship and motivation in entrepreneurship
    - Samson emphasized the importance of having a mentor in the course of entrepreneurship, as they help avoid pitfalls.
    - He stated having people who support your ideas and are personally committed to you as a significant factor of success.
    - The vital role of motivation and maintaining a positive outlook even during challenges was discussed.

3. Managing startups and lessons from failures
    - Samson provided insights on managing multiple ventures and the importance of clearly identifying the problem one is trying to solve.
    - According to him, vision and commitment to a goal are vital, but entrepreneurs must not forget about the practicality and execution part of their ideas.
    - He shared valuable lessons learned from his failures, including the difference between cash flow and receivables, and the need to secure legal structure before partnering up.

4. I'm Safe App – A childhood dream actualized
    - Samson talked about his app, I'm Safe, which was created to address women's safety. He said that helping women and working against trafficking has been his childhood dream.
    - While discussing the importance of remaining passionate, he noted that it becomes easier to stick to your vision when it's something that you deeply care about.
    - He expressed his long-term commitment to this project and explained why he doesn't see an exit for the app despite the possibility of future expansions.

5. Humility, gratefulness, and living a good life
    - Samson shared his insights on leadership qualities, particularly humility. He reflected on the lives of various historical figures who exhibited this trait.
    - He defined living a good life as being grateful and maintaining peace. According to him, gratefulness for what one has is imperative for success.
    - He wrapped up mentioning that everyone has their own mystery and understanding life’s injustices is a part of the journey; it's crucial to remain humble and maintain peace through it all.

Note: This summary captures the essence of the conversation and may not include all the details discussed. It is advised to listen to the full podcast episode for a comprehensive understanding.

## Edited Transcript
_coming soon_

## Watch on YouTube
<iframe width="560" height="315" src="https://www.youtube.com/embed/OJ0YJziggXA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

## Connect with Samson
- LinkedIn: https://www.linkedin.com/in/sselladurai/

## Connect with me
- Twitter: https://twitter.com/jjude
- LinkedIn: https://www.linkedin.com/in/jjude/
- Website: https://jjude.com/
- Newsletter: https://jjude.com/subscribe/
- Youtube: https://youtube.com/gravitaswins
- Email: podcast@jjude.com
- Executive Coaching Program: https://gravitaswins.com

## Your feedback counts

Thank you for listening. If you enjoy the podcast, would you please leave a short review on Apple podcast or on YouTube? It takes less than 60 seconds, and it really makes a difference in finding this podcast. And it boosts my spirits. 😀

I would like to hear what resonated the most with you in this episode. Either email me or share it on social media and tag me. Thank you for your support.

Have a life of WINS.

_Gravitas WINS Radio is produced by [Joseph Jude](https://jjude.com/). Be sure to subscribe on [Apple Podcast](https://podcasts.apple.com/in/podcast/gravitas-wins-radio/id1562802449), [Spotify](https://open.spotify.com/show/7LOBjAyjBkechMC0xMIj9E),[Google Podcast](https://podcasts.google.com/feed/aHR0cHM6Ly9mZWVkcy50cmFuc2lzdG9yLmZtL2dyYXZpdGFzLXdpbnMtcmFkaW8=),[YouTube](https://www.youtube.com/gravitaswins), or the podcasting app of your choice._

Please check out other episodes of [Gravitas WINS Radio](/gwr-themes/). For easy consumption, they are categorized by themes. If you are a fellow podcaster, you might be interested in [How I Podcast](/how-i-podcast/) or [Tools I Use](/i-use/).

## Continue Reading

- [Sohail Khan on 'Why startups fail'](/sohail-tbc/)
- [Manish Verma on 'How Angel Investors Think?'](/manish-angelblue/)
- [Arvindh Sundar on 'Gamification of marketing for small businesses'](/arvind-gamify/)