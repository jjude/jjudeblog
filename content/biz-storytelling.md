---
title="Power Of Storytelling In Business"
slug="biz-storytelling"
excerpt="Move your audience with simple, memorable, and relatable stories."
tags=["branding","networking","wins","gwshow"]
type="post"
publish_at="23 Mar 21 20:57 IST"
featured_image="https://cdn.olai.in/jjude/biz-stories.jpg"
---
If John F. Kenndy had announced, "We will establish ourselves as the aerospace leader beating our competitors with strategic rocket launches," would America be as innovative as it is now?

JFK galvanized the nation with simple and memorable language evoking emotion. He demonstrated the power of storytelling with the phrase, "landing a man on the moon and returning him safely to the Earth."

Our messages are competing for attention from our prospects and customers as they are in-between swiping one app after another.  Unless our messages are simple, relatable, and memorable, they are going to be lost.

Corporate storytelling is delivering business facts in simple, memorable, and relatable language, so it moves our audience to act in the way we want them to act. 

![Storytelling in business](https://cdn.olai.in/jjude/biz-stories.jpg)

### Examples of corporate storytelling 

When NGOs call us for funding, they narrate a personal story to move us to donate. They could say, "we support cancer patients. Would you donate?" Instead, they always rely on a story of a suffering patient. 

"Laxmi is suffering from cancer. Your donation of 100 rupees can make a difference in her life."

How can you use that in your presentation and demo?

Say you are talking about the illiteracy rate in Africa. You could represent it as a bar chart comparing the illiteracy rate in developed nations and African nations. Alternatively, you can say an Europe is dropping out of school in Africa every year. That is a powerful language that the audience can relate to. When they can relate, they will take the action you request them to do. 

Another example. Say you have to present a web optimization tool to a client. Traditionally we will talk about all the features of the tool.

You can follow a different approach. If you take the time to study the loading time of their website and their competitor's websites, you can lead the discussion with a question: 

How many sales are you losing because your customers are waiting for 2 minutes for the page to load?

When you wrap facts with relatable and simple language, your prospect will engage with you in a lively conversation. That's a win for you.

### Getting better at storytelling 

Can everyone get better at storytelling? Of course, yes. We are born storytellers.

You can use the [LION framework][1] to get better at storytelling. LION stands for:

- **Learn**: You have to be curious to be a good storyteller. Collect stories from books, videos, and podcasts. Observe how others tell stories. Make notes.
- **Innovate**: In the startup world, there is a concept called bundling and unbundling. You can use that framework to improve your storytelling. Use the stories you have collected and bundle them into your demos and presentations. 
- **Outwork**: You have to practice to get better. If you want to be a successful storyteller, you need to outwork everyone else. Practice it everywhere, so it becomes second nature to you.
- **Network**: All our success is social. Use stories to network with others. While talking to others, listen to their stories. If you hear exciting stories, note them. Use them wherever you find an opportunity.

### It is your turn now

Storytelling is a powerful way to build your brand. Your first step is to observe how others use this technique in their conversations. As you listen, take notes. And then practice. Practice. And practice even more. As you practice, you'll become a fantastic storyteller.

_These are notes from conversation with [Krishna Kumar][2] in [Gravitas WINS show][3]._


[1]: /writing-lion/
[2]: https://www.linkedin.com/in/krishna-kumar-n-kk
[3]: https://www.youtube.com/watch?v=x7aJsQxlS9o

## Continue Reading

* [Power in your hands](/power-in-hands/)
* [Build on the rock for the storms are surely coming](/build-on-rock/)
* [Authors who shaped my thinking before I turned 18](/authors-impact/)