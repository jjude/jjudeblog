---
title="Thanks & Sorry. Say them before it is too late."
slug="thanks-and-sorry"
excerpt="We have moving eulogies. Why can’t we convey our thanks (and sorry) to our family, friends, and mentors while they are alive?"
tags=["opinion"]
type="post"
publish_at="10 Dec 19 09:30 IST"
featured_image="https://cdn.olai.in/jjude/thanks-and-sorry-gen.jpg"
---


When I got into Engineering college, I struggled to speak in English. In the first year, our English teacher would conduct debate sessions and compelled us to speak in English.

The themes of these sessions were interesting. So I wanted to participate. But I was constrained by my lack of fluency in English. 

My bench mate was a handsome and helpful boy called Basha. He offered to help me. I would tell him what I wanted to say in Tamil, he would scribble that in English, and then I would say that out loud. It went on for months until I got good to manage those debates myself.

We became good friends. We both chose to study electronics engineering, so that helped to cement our friendship.

In the later years, a coordinator by name Jebaraj Samuel joined our college. He upped the game and compelled me to speak in our daily assemblies.

These two gents sowed the seeds of public speaking in me.

When we graduated, there were no Facebook or WhatsApp. We didn’t even have emails back then. I went to work in Chennai, and he went to work in Saudi Arabia. We lost touch. 

Though I improved my ability to communicate in English, I never attempted public speaking after college. In the last five years, these opportunities opened up again. I spoke in schools, colleges, corporates, and churches. When I restarted my public speaking, I remembered the little scribbles of Basha. I wanted to connect and tell him that his little bits of help elevated me beyond my imagination. I was sure he would be proud.

Every time I got an opportunity to speak, I would think of events that happened on that desk in college. In the later days, Basha refused to translate and scribble. He forced me to think and tell him in English. He would only correct if there was a need, and there was always a need. In the busyness of life, I went on smiling, thinking of those events, but never really thanked him.

Then I heard the news. He passed away in cancer. He didn’t tell anyone about his suffering. He prepared his family and planned everything for them. But didn’t tell any of us. It came as a shock to all of us who knew him.

Today, as I write this, I choke. 

Many of my college mates entered college like me - lacking fluency in English. Most of them left the same. I graduated better because of Basha. But I missed the opportunity to tell him how much he helped me.

We have moving eulogies. Why can’t we convey our thanks (and sorry) to our family, friends, and mentors while they are alive? Wouldn’t we make our lives a whole lot better?

You are what you are, because of so many. But few played a pivotal role. Don’t wait until after the last minute to convey your thanks. As we end the year, make a list of people who helped you become who you are. Walk up to them and talk to them. Next best thing is to call them to tell them.

Whatever way you choose to say is ok. The critical point is to say it. Say it today.

P.S: I met Mr. Jebaraj Samuel this year and spent an hour with him. Among many other things, I expressed my thanks. I have learned my lesson.
