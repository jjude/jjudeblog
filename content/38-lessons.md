---
title="38 Lessons"
slug="38-lessons"
excerpt="I look back at what I learned, one lesson for each year I have spent in the journey."
tags=["insights"]
type="post"
publish_at="15 May 11 16:30 IST"
featured_image="https://cdn.olai.in/jjude/38-lessons-gen.jpg"
---

Today I turn 38. From a village boy to advisor to government, it has been a fantastic journey. I have been lucky to have traveled this journey. Without a doubt, this journey was made enjoyable by my friends, family and foes too. Here I look back at what I have learned, one lesson for each year I have spent in the journey:

1.  **Success in life depends on relations you build**: Even if you are smart, success in life comes only through ‘who you know’ and ‘who knows you’. No one makes in life alone. Go network, even if you are an introvert.

2.  **Life is what happens to you when you plan other things**: I wanted to be an expert in robotics engineering but I got an opportunity to work in health-care informatics; I went to the US thinking, as like so many other Indian guys, that I will have a green card, marry an American and be a faithful Indian in filling the earth with kids but I got transferred to Belgium; as I was in the midst of setting up a company in Belgium, events beyond my control, forced me to come back to India; while I refused to come to Delhi in my previous job (as an employee) because of politics in that unit and Delhi’s weather, I got an opportunity to be an advisor to Government of India and I’m in Delhi for past two and half years. This has become a pattern.

3.  **Make the best of what happens to you** : Despite the unexpected and many times unpleasant turn of events, I have learned to live ‘here and now’. It wasn’t easy and I still struggle when life leads me through a surprise turn. This doesn’t mean taking heaps of rubbish, though.

4.  **Common-sense is nonsense** : By commonsense, earth is flat and sun moves around the earth. So are, MBAs are smart, beautiful people are happy, rich people have an easy life and you need to pass out from top institutions to be successful. I have witnessed all of these to be utter non-sense. If you don't question what the crowd believes, you can never be ahead of that crowd.

5.  **If you help, you will be helped** : Not necessarily from the same person or in the same context. But help comes from strangers when you expect the least. Help whenever you can, in whatever way you can. Assuredly you will be helped.

6.  **There is no substitute for hard work** : Like many of the lessons mentioned in this list, I learned this from my dad and it was reinforced over the years. Even if you are smart, there is no alternative to succeed in life, in whatever factor you measure success.

7.  **Your failures are apparent** : There was a period in my life which was filled with all kinds of failures. I was distressed by these academic, financial, professional and relationship failures. But in the larger scheme of life, these failures didn't matter. For example, while in Engineering college, I failed in two papers in seventh semester. Until then, I never failed. At 20, I didn't know how to handle failure. I locked myself for two days; I thought life was over; I told myself, no one will respect me and so on. But none of those turned to be true. In the past two decades, it was discussed zero times. This is true of so many other failures in which it seemed I was against a wall. Some of these failures were because of my stupidity but mostly it was because I chose to do things different.

8.  **Respect your parents** : Like many, I too went through adolescent phase when I disliked my dad for his control on me. But as I stepped out of college and through these many years, my admiration for him grew multi-fold. I observed him closely and learned from him in real life. By his temperament, he doesn't talk his walk. So I don't get to discuss with him much but what a great way to learn by emulating his walk. And my mother? I always loved her for the hard work she put in to take care of the family in addition to working as a teacher. Now that I have become a father, my respect is only increasing for both of them.

9.  **Money is essential but not critical** : It took me some time to realize the difference and when I realized, it was a big relief.

10. **Pursuit of happiness is vanity** : Happiness is a byproduct, a byproduct of pursuing and achieving your heart's desire. Set your heart on good things in life and pursue them without an excuse. You will be happy everyday of that pursuit.

11. **Be a synthesis of dove &amp; serpent** : Life is a paradox and we are too often caught between opposing concepts - sometimes we need to hold on and pursue and some other times, we need to let go; sometimes we need to burn bridges and some other times we need to build bridges. I am still learning the ironies of life.

12. **You are superior to none and inferior to none** : My friend Arun uttered these words almost two decades ago and my life has never been the same. These words also helped me to escape categorization by people and live my life in my own terms.

13. **Honestly speaking, honesty isn't the best policy** : You may be surprised to find this in here. Let me ask you a simple question : when was the last time you were honest the whole day - to yourself, to your friends, to your siblings, to your parents, to your boss, and not forgetting the government. When we know everyone else is not honest why are we hanging these phrases everywhere in our office? Are we wishing, others were little more honest?

14. **Even men cry** : I grew up in an environment where it was implied that crying was a sign of weakness. I carried this unwanted burden for way too long. At one phase in my life, I had troubles beyond what I could endure and I broke down. Not just once but lot of times. That is when I realized the power of crying. It eases your heart.

15. **If you don't believe in my God, I have the right not to believe in your God** : Yes, I believe in almighty, but not in a traditional sense. But I'm not preaching my spiritual beliefs here. I'm talking about professional life. I have witnessed consultants fight over methodologies and frameworks with the same vigorous as of religious fanatics. I tend to measure engagements by outcome, so I don't hide behind shiny pillars of these frameworks.

16. **If you practice wasting, you will get perfect at it** : "Practice makes perfect", isn't true just for positive things. If you let yourself distracted - by internet, social media, games - you get addicted and your productivity is lost. Same goes true for feeding your mind with wasteful thoughts and ideas. Nip them early before they destroy you.

17. **Life doesn't compartmentalize so you shouldn't too** : Does life wait for you to return home to throw up a trouble? When life teaches you not to compartmentalize your life, learn it. If you have too much work, don't feel guilty to work on a Sunday morning; but at the same time, if you prefer to go for a swim at three in the evening, go. Live here and now.

18. **Inspiration doesn't put dinner on the table** : There is no dearth for inspirational songs, stories and quotes. Surely we need inspiration. But these messages become just dead bytes if not translated into actions towards your goal. Listen and read but also roll your sleeves and do your work.

19. **Travel light** : No, I'm not talking about traveling the world in a back-bag. But I'm talking about throwing away the dead weight of guilt and regrets. I used to be a person who carried too much dead weight. For me the revelation was gradual but I practice them as much as possible.

20. **Live a life you will be proud of** : In the grind of daily life, we may forget the big picture and commit blunders. For example, you may be ignoring your family under the pretext of working hard to provide a comfortable living for them. So pause, evaluate and proceed. Surely you don't want to be busy for years only to find that you have been going away from your goals.

21. **Learn but ignore critics** : If you do any action - good or bad, you are sure to attract criticism. Whenever, you are criticized evaluate if there is any truth in it, or is the comment for the benefit of the giver. If there is any thing you can learn, learn. Else ignore without any peril. Also, never let the fear of being criticized stop you from taking decisions and moving forward.

22. **Love should be nurtured** : Love at first sight is for Hollywood heroes; for the rest of us, love need to be nurtured before and after marriage. If you ignore your wife, she will return the favor. I say, if you want to be treated as a king, you better treat your wife as a queen. There is no second thought on that. Similarly, if you want your son to grow up responsibly and respect others, first respect him and give him responsibility and allow him to handle it.

23. **Don't be indispensable** : Many think that making themselves indispensable is a sure way for job security. Some of the tactics used by them are hiding critical information and unwillingness to help juniors. In a knowledge industry, this works for a short term, but in the long term it has an opposite effect. Rather, help generously and share information as much as possible. That will make every one happy to work with you. That's a sure way to move up in career

24. **Don't be measured by how they measure you** : I have always resisted the temptation to measure people by the degrees they posses, or the amount they earn or the car they drive. I have applied the same principle to myself too - I refused to be measured by any of these parameters. None of these define me. And in the age of social media, the general yardsticks are number of hits, followers and likes. None of these are useful. What's the use of all of these measures if hits don't turn to comments, likes don't convert to buys and followers don't evangelize your product or service? Know what measures are useful and pursue only that.
25. **If you buy things you don't want, soon you will sell things you need** : There is one thing to enjoy the pleasures of life and it's another to fill your house with unwanted gadgets. It's not about being stingy but being responsible towards money at your hand.
26. **Little bit of fear is good** : I entertain a decent amount of fear in me about what I do. That fear forces me to introspect and take corrective actions, if necessary.

27. **This too shall pass** : My favorite phrase. Good and bad passes. Nothing stays the same for long. So when life gets tough, tell yourself this too shall pass, so endure with hope; and when life is full of laughter, tell yourself this too shall pass, so enjoy every moment.

28. **Forgive &amp; seek forgiveness for your own benefit**: He who erred would have moved on with his life, but you hold a grudge against him and spoil your life. So forgiving is a favor you do it to yourselves. Similarly, if you feel guilty, go ahead and seek forgiveness. It doesn't matter she doesn't forgive you. In all earnestness if you seek forgiveness, peace will surround you. It is her problem, if she is still holding a grudge against you.

29. **Get out of comfort zone before life throws you out of it** : When you are in a place too long doing the same thing over and over again, you rot. Instead seek change. Change before you are forced to change. Life will be relatively easier. If you become complacent and someone throws you out, it's going to be messy.

30. **Life is a game, but it is not a zero-sum game** : There're enough opportunities in this world for all - may not be in the same region, domain and with the same resource. But the undeniable truth is you don't have to loose for me to win. In the long run, all of us can be winners.

31. **It doesn't matter how much you know; what matters is how much you put to use** : I am a voracious reader. Even after selling boxes of books, I am still left with hundreds of books. But what's the use of reading all these books, if I am stupid in my endeavors or I can't convert the knowledge into revenue stream. Consume, but create too.
32. **Be faithful in small things before great things can be handed to you** : Yes, it's a Biblical phrase, but I have witnessed it comes true in life, not just for me but for many others too. There is no other way to move up in life.

33. **If you aren't dead, you got a second chance** : If not for second chances, I wouldn't be where I am. I am a slow learner and so life has been gracious to give me second and sometimes third chances. You may have to change tactic or sometimes re-define the target. In all through, keep your desire alive. I wanted to be an independent consultant in Europe. It didn't happen then, but after eight years, I got an opportunity in India. I grabbed it and so far I am enjoying it.

34. **Its a package deal** : Like many, I too have my own 'if only' desires. But life comes as a package; you can't pick and choose. You got to learn to enjoy as a whole package. Given a choice, I would like to earn American salary in India with a climate as that of Bavarian hills with a beach at a drivable distance. When that ideal life isn't possible, I am okay to sweat out in this hellish hot Delhi and commute via it's metro to work with Government officials six days a week. Why? Not many civilians get an opportunity to serve their country.

35. **Failure and success aren't overnight events** : It might be that final action that elevates you but that's not to say the years of work leading up to that final action can be ignored. Same is true of failure. Divorces don't happen for a single fight. Years of ignoring the family breaks them; similarly continuously ignoring your health leads to disastrous results.

36. **Learn to balance** : Balancing isn't compromise. My wife wanted to go to her mother's house, which is at the other tip of India, for the delivery of our first baby. So, I had to balance between her desire, my desire to be with her as well as the possibility that I may not get there on time for the birth of our first baby. We discussed and agreed that she will go. Most situations in life are like that - all options are valid and correct and there are emotions attached to each options. You will have to take a balanced decision. By the way, I am still learning to balance.

37. **Choose your fights** : Not all fights are worth fighting and not all fights are fought to win. So pick and fight a good battle. You should be proud of your fighting, despite the results.

38. **No regrets** : In my youth, I use to regret over so many mistakes. But I painfully learned, it's useless to feed regrets. I do contemplate my actions and learn ways to improve, but I don't regret. If you have done your best with all of your heart towards a worthy goal, what else can be asked of you?

Its long, but I enjoyed - both my life and this post. I hope you enjoyed reading this. And thank you for reading.

