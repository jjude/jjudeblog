---
title="Best Of Jan Reads"
slug="bestof-0114"
excerpt="Thinking in ratios and doing things together may turn out to be the best tips of this year."
tags=["insights"]
type="post"
publish_at="31 Jan 14 16:30 IST"
featured_image="https://cdn.olai.in/jjude/think-in-ratios.png"

---

I am sharing two articles that stood out from all the articles that I read in Jan.

<a name="one"></a>First one is by [Joel Gascoigne][7], founder of Buffer[^1]. In an article titled, [The Habits of Successful People: Thinking in Ratios][3], he explains how the 'law of average' helped him in marketing, fund-raising and hiring.

Quoting Jim Rohn,

> "If you do something often enough, you’ll get a ratio of results. Anyone can create this ratio."

![Thinking in ratio](https://cdn.olai.in/jjude/think-in-ratios.png)

I know about 80/20 principle[^2] and I do practice to the extent that is possible. What hit me when I read Joel's post is that you need to do even that 20% of the tasks often enough to take advantage of the law of average.

I'm not a founder; only a blogger and consultant. So what this means for me is:

- the more I write, better my blog-posts will be;
- if I improve the number of solutions I'm able to think for a problem, I improve the chance of hitting upon a better solution;

Once you realise that the world works in ratios, it becomes easier to send that next marketing email, it becomes easier to sit down to write and so on. You know that this particular email or post may not be get you the result but you are increasing the chances of getting there.

**Thinking in ratios and working to make that ratio better may turn out to be the lever that shifts this year into a productive, successful year**. I'm excited to see how this turns out to be.

<a name="two"></a>The second one is by [Jeff Goins][4]. I subscribed to his newsletter to improve my writing. Little did I know I will learn more than just writing. He is now in Uganda as part of [Compassion Bloggers][5] group. He has been writing about his experience interacting with the families there.

Those African families may not be well developed by standards of materialistic world. But they know how to live, which the developed materialistic world is forgetting.

Jeff [asks][6] Sam, a poor family man, what they do for fun.

Now pause for a moment and think what you, as a family, do for fun. We leave the kids at home at the care of babysitters and go shopping, movies or parties. Even if we take kids, we go to Disneys of the world.

The reply Sam gave, tells how different and meaningful their lives are. He says,

> **We do everything together. And to us, it is fun.**

Fun is being together as a family. Isn't it? Even if it is just listening to stories that your kids say in broken sentences.

This year I am going to have fun. We are going to do things together.

[3]: http://blog.bufferapp.com/the-habits-of-successful-people-thinking-in-ratios
[4]: http://twitter.com/jeffgoins
[5]: http://compassionbloggers.com/trips/uganda-2014/
[6]: http://goinswriter.com/hope-looks-like/
[7]: http://twitter.com/joelgascoigne
[8]: http://bufferapp.com/

[^1]: For those who don't know, [Buffer][8] is a social-media tool to schedule your updates to Twitter, Linkedin and Facebook.
[^2]: Technically known as **law of the vital few**. It states that for many events, roughly 80% of the effects come from 20% of the causes
