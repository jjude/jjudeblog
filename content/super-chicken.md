---
title="What can super-chickens and system thinking tell us about building a hyper-performing team?"
slug="super-chickens"
excerpt="Superstars don't bring success."
tags=["systems","wins","coach"]
type="post"
publish_at="13 Jul 21 20:37 IST"
featured_image="https://cdn.olai.in/jjude/super-chickens-gen.jpg"
---


### An Experiment At Purdue

Evolutionary biologist William Muir conducted an interesting experiment with chickens while at Purdue University. He was curious to find out how to make chickens lay more eggs so that they would be more productive.

He picked two flocks of chickens. The first flock consisted of average chickens. He left them alone for six generations. The second flock consisted of super-chickens, which were individually the most productive. He selected only the most productive chickens for breeding in each generation.

After six generations, what did he find? Not only were the average chickens doing well, but their egg production also increased dramatically.

As for the second group, there were only three survivors. The rest were pecked to death. The productive chickens achieved their success by undermining the productivity of the rest.

As Margaret Heffernan said in her [TED talk](https://www.ted.com/talks/margaret_heffernan_forget_the_pecking_order_at_work), "We've thought that success is achieved by picking the superstars. The result has been aggression, dysfunction, and waste."

### Systems Thinking Concurs

Let us say we want to assemble the best car in the world. The way we are going about doing it, we find the best individual parts. Our consulting engineer prepares a list like this:

• best engine: Rolls-Roys
• best transmission: Mercedes
• best battery: BMW

and so on.

If we buy these parts and assemble them, we might have a car with the best parts but we will not have the best car.

Russell Ackoff, my favorite systems thinker, gave [this example](https://youtu.be/OqEeIG8aPPk) and said, "performance of a system depends on how the parts fit, not how they act taken separately."

### Clarity, Communication, Cooperation

What do the super-chicken experiment and systems thinking teach us about building hyper-performance teams?

Like how a car with the best parts won't be the best car, a company full of 10x employees won't be the most productive company. A team with "super-chickens" will be driven by anxiety, dishonesty, and jealousy leading to dysfunction and failure.

Only when members co-operate between themselves, the team will be hyper-productive.

Collaboration is not our default behavior. Leaders have to nurture it in a team. To evoke co-operation, all team members should know the purpose for which they have come together. When there is clarity of purpose, the team will move cohesively towards the purpose.

Making the purpose clear is not a one-time activity. Leaders have to reinforce the purpose again and again because existing members will easily forget it and new members will join the team.

If you've assembled a competent team and yet are frustrated with their unproductivity, ask yourself these questions:

• Do I know the purpose well enough?
• Do I communicated the purpose often to the team?
• Do I promote a superstar syndrome in the team?

### Quote To Ponder

A system is not the sum of the behavior of its parts; it's a  product of their interactions - Russell Ackoff

_I got the idea for this post from my conversations with Abhinav Goel. You can [watch](https://youtu.be/6vDEDsuLZDM) our discussions on Gravitas WINS Channel or [listen](https://open.spotify.com/episode/55zgg4MloygZHMT6PuVYRz) on Gravitas WINS podcast._

## Continue Reading

* [System for success](https://jjude.com/system-for-success)
* [How to get lucky?](https://jjude.com/luck)
* [How to improve project delivery in an IT services company](https://jjude.com/it-delivery-system)
