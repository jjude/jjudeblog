---
title="Is life a battle or a blessing?"
slug="battle-blessings"
excerpt="I'm still learning to adjust to pain and growth happening at once."
tags=["wins","self"]
type="post"
publish_at="30 Mar 21 21:35 IST"
featured_image="https://cdn.olai.in/jjude/gw-course-testimonials.jpg"
---
I used to think of life as cycles of battles and blessings. As I age, I am coming to think of life as battle and blessing happening together. It is like a train track with one track full of worries, sadness, loss; and another track with wins, excitement, and joy.

The covid season is only cementing that belief.

I lost a friend two weeks back to covid. We sang and prayed together in church every week for three years. He was elder to me but didn't suffer from any serious illness. As a government officer, he was transferred to another city after his deputation was over here. We bid farewell, promising to keep in touch. As soon as he landed in the new town, he was hospitalized. In twenty days, he was gone. Just like that. Man is but a flower.

He left so soon is not the only news weighing on me. With no comforting family or friends, he suffered alone, and then he died alone. Because he died of Covid in a far-away city, his family and friends couldn't be with him on his last journey. He was buried alone too. I'm still struggling to close his chapter in my heart.

Earlier in February, I suffered backache while playing with my boys. Whenever I had backaches earlier, my wife will massage the area applying medicine. After a day of rest, I will be back to normal. This time, the pain went on for weeks. The pain was sharp too. Most days, I stayed lying in bed for hours.

It is a season of pain and anguish. It is also a season of growth.

I started the [Gravitas WINS course](https://gravitaswins.com/) as an experiment in July of 2020. I wanted to grow the venture but wasn't sure if I knew how to. When I completed the second cohort, I told my mentor, " I have trained everyone I know." Apparently not. I am in the fourth cohort and going to start another one next month. Most students have given me great feedback.

![Testimonials for Gravitas WINS course](https://cdn.olai.in/jjude/gw-course-testimonials.jpg)

> I was looking for a course that will help me make better decisions. In this course, I learned the concepts of structured thinking, flywheel, and how to take small conscious steps to achieve larger objectives.
> [Nitin Mukhija](https://www.linkedin.com/in/nitinmukhija/), Chief Digital Officer, Edelweiss

>The best part about the course is the personal mentorship, comprehensive course material, and peer-to-peer learning
>[Sejal Sud](https://www.linkedin.com/in/sejal-sud-762664162/), Youtuber

>Gravitas WINS course brims with savvy advice and overflows with hands-on exercises. Joseph focuses on the matters that matter.
>[PK Khurana](https://www.linkedin.com/in/pk-khurana-81313a23/), WoW happiness and The Growth School

In support of the course, I started a weekly webinar called "[Gravitas WINS show](https://www.youtube.com/channel/UCPDIeR7Lzvt9uATvMZSZX-Q)." I started it even when I had the backache. I expected 5 - 10 people to join. Oh, boy, I was in for a surprise. Twenty-five people joined. So far, I have taken six sessions, and each of these sessions had at least twenty people staying for the entire session. Most of them appreciated the shows.

> You took up cases and solved the riddle live, which added immense value to the lecture you delivered

> You should take more sessions for us...It is very hard to find books which you teach in these sessions...You are the best.

> The idea of thinking about projected, plausible, possible, and preposterous future states for goal setting is profound. It allows me to go into a comforting yet ambitious mindset.

It is truly the best of times and worst of times. Until you live through such times, you don't understand the impact of those words. I'm still learning.

Are you living through pain or growth or both at once?

## Continue Reading

* [Numb is dumb](/numb-is-dumb/)
* [Are you spending enough on recovery](/recovery/)
* [Writing my obituary](/my-obituary/)
