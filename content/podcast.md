---
title="Gravitas WINS Radio"
slug="podcast"
excerpt="If you want to build gravitas and attract luck, this show is for you."
tags=["gwradio","produce","wins"]
type="page"
publish_at="25 Apr 21 21:06 IST"
featured_image="https://cdn.olai.in/jjude/podcast.jpg"
---

The key to success is the ability to influence others - bosses, peers, and team members. But how to be a person of influence? Chief Technology Officer and coach Joseph Jude unpacks key elements of gravitas every Tuesday. If you want to build gravitas and attract luck, this show is for you.

Listen on [Apple Podcasts](https://podcasts.apple.com/in/podcast/gravitas-wins-radio/id1562802449) / [Spotify](https://open.spotify.com/show/7LOBjAyjBkechMC0xMIj9E) / [Google Podcasts](https://podcasts.google.com/feed/aHR0cHM6Ly9mZWVkcy50cmFuc2lzdG9yLmZtL2dyYXZpdGFzLXdpbnMtcmFkaW8=)


<iframe width="100%" height="390" frameborder="no" scrolling="no" seamless src="https://share.transistor.fm/e/gravitas-wins-radio/playlist"></iframe>

_You can read about [individual episodes](/tags/gwradio/) or view episodes [categorized by themes](/gwr-themes/)._


### What Others Are Saying?

![Reviews of Gravitas WINS Radio on Apple Podcast](https://cdn.olai.in/jjude/gwr-reviews-apple.jpg "Reviews of Gravitas WINS Radio on Apple Podcast")

_You can read all the [reviews](https://podcasts.apple.com/in/podcast/gravitas-wins-radio/id1562802449?see-all=reviews) on Apple podcast page._

In my world, Gravitas does not only win - but makes you win! I have known JJ closely for some years now, and the simplicity of his thought process is mind-blowing !!! A voracious reader, he knows the art of converting information to knowledge… and he makes it all look so simple. Endearing with a fantastic sense of humour, JJ’s perspectives are original, unique and very refreshing. Huge fan :-) - *Ritika Bhatia Singh*, Founder, Kontent Factory

***

Gravitas wins radio is one of my favorite podcasts. Joseph has got the knack of fetching maximum insights from the other person in the conversation. He doesn’t leave anything on the table and went a really deep dive into the why and how of things. He doesn’t only cover the work-related aspect of life but also touches upon other things which matter a lot in life. - *Rishabh Garg*, Indie Developer

