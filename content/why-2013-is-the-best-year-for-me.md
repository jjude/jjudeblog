---
title="Why 2013 Is The Best Year For Me?"
slug="why-2013-is-the-best-year-for-me"
excerpt="Because I broke my mental barriers."
tags=["retro"]
type="post"
publish_at="01 Jan 14 16:30 IST"
featured_image="https://cdn.olai.in/jjude/why-2013-is-the-best-year-for-me-gen.jpg"
---

Because I broke my mental barriers.

It's both **a challenge and a satisfaction to get over your prejudices**, to do something that you thought you can't do earlier.

This year I got many situations to do just that and that makes this year special for me.

Let me first talk about travel.

Though I have been in Delhi for five years, I didn't travel anywhere outside Delhi. I had assumed travelling in North India would be unpleasant.

This year we, as a family, visited [Agra](https://en.wikipedia.org/wiki/Agra), [Kasauli](https://en.wikipedia.org/wiki/Kasauli), and [Patna](https://en.wikipedia.org/wiki/Patna). Some parts of these travels were uncomfortable but these trips turned out to be fantastic.

When I told my wife, in 2008, that I got an assignment in Delhi and we have to move there, there was only one place she wanted to see—Taj Mahal. I took her there after four years. She was happy and seeing her happy made me happy too.

Moving on to the business side.

This year I developed two mobile apps - BlogEasy and Memorise Bible. Memorise Bible was free but I charged for BlogEasy. [^1]

Being a programmer, I have developed apps earlier but gave them away for free. I did so, because I had reservation about my skills in marketing, sales and everything that is needed to make a product a successful business.

Did I become an expert in marketing?

I can't say I became an expert at marketing, but I became better than whatever I was. I sold more than 100 copies. For me that's a success.

This year I was also able to expand my mind by reading [eight books](/books-read-in-2013/). They all belong to different genre (I read a novel and enjoyed it; I mention it because I despise novels and I think they are a time-hole).

Challenging mental limitation is a challenge; how about challenging both mental and physical?

Earlier years, I tried learning skipping, but failed. I could never jump and swing the rope in rhythm. This year I took that as a challenge, looked for instructions, stumbled a [YouTube video](http://www.youtube.com/watch?v=quoyW7FZqdI) and boom, I started skipping. Now I could do 100 skips continuously. Sweating doing something that frustrated me endlessly is a satisfaction without words.

<object width="420" height="315"><param name="movie" value="//www.youtube.com/v/quoyW7FZqdI?version=3&amp;hl=en_US"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="//www.youtube.com/v/quoyW7FZqdI?version=3&amp;hl=en_US" type="application/x-shockwave-flash" width="420" height="315" allowscriptaccess="always" allowfullscreen="true"></embed></object>

Paraphrasing Paulo Coelho, when you do your best the world conspires to aid you.

So came Delhi election. We Indians are good at whining about state of India without doing our bit to improve it. Election is an important tool that democracy provides to improve the state. So we were happy to go to voting booth with our kids (our kids are aged 4 &amp; 2; without help at home we can't leave them alone at home) and vote. It turned out we were part of a revolution. We proved that India does have a vibrant democracy.

In the last hours of this year, I look back and feel so happy about the way the year went. Getting over the mental challenges weren't simple. There were days of doubts, pain and stress. But the results made all of that worth.

2013 has been the best year. But I believe in the adage, **the best is yet to come**.

### Retros of the earlier years

I've been publishing annual reviews almost every year. You can [read them](/annual-reviews/) to understand the journey I've come through.

[^1]: I stared a company [DSD Infosec](http://dsdinfosec.com) by the end of 2014. I discontinued these apps to focus on building the company

