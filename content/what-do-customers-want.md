---
title="What do customers want?"
slug="what-do-customers-want"
excerpt="Companies should spend their resources on improving customer's experience at every point of contact  - be it marketing, sales, delivery or post-sales support."
tags=["coach"]
type="post"
publish_at="29 Jan 09 16:30 IST"
featured_image="https://cdn.olai.in/jjude/what-do-customers-want-gen.jpg"
---

Companies spend big share of their budget trying to 'differentiate' their product and services from those of their peer-group companies. Usually these differentiations are add-ons to the core category benefits which are expected from their product or service. But do such differentiations work?

Would you return to a restaurant that has an impressive interior and calming music but serves absolutely tasteless food?

This is the theme of '[Simply Better](http://www.simply-better.biz/)', a book that, Gary Silverman calls, "... a book about marketing for people who have read too many books about marketing".  Drawing upon experience of Tesco, Toyota and similar companies, Patrick Barwise and Sean Meehan, authors of the book, argues that, **customers expect a predictable and reliable delivery of category benefits**, every time; all the time. Differentiation does not matter to customers when category benefits fail.

The authors also emphasize the marketing principle put forwarded by [Peter Drucker](https://en.wikipedia.org/wiki/Peter_Drucker), 'Marketing is not a specialized business activity…it is **the whole enterprise seen from the customer’s point of view**'. Companies should spend their resources on improving customer's experience at every point of contact  - be it marketing, sales, delivery or post-sales support.

These are simple yet fundamental concepts for any company's success. Fact is, simple concepts are easy to be missed.

