---
title="As life gets by"
slug="as-life-gets-by"
excerpt="From being just-a-lump in the womb to where I'm"
tags=[""]
type="post"
publish_at="10 Sep 05 16:30 IST"
featured_image="https://cdn.olai.in/jjude/as-life-gets-by-gen.jpg"
---

I started this article as an 'About the author'. Interestingly it developed into this one. My life is filled with parallel journeys, as like in many people's life. This is in no way inclusive of all, just bits that I thought would be worth sharing.
1973 May 15 20h04: After being dismissed as just-a-lump and then diagnosed as a 'dead child', finally I got into this world. Years later an astrologer would insist that if it was 20h04, then it should be a girl. So here I am, a typical taurian male with a tender and a gentle heart.

Until 1978: It might be hard to believe for those who know me, but it is true: I wouldn't utter a word in these years giving an impression that I was dumb. I would go on, 'lol lol' for dogs, 'meow' for cats and so on. But ever since I started speaking, it has been a 'non-stop non-sense'.

1982: First loss! Lost being the first rank holder to a girl. Damn! I felt terrible. She was from Aruppukottai and every time I cross that town, I think of her (of course fondly).

1983 - 1988: Joined Caldwell school, an English medium school in Tuticorin. A great and an exciting news for a, nearly, village boy. Traveling every day in school bus and playing kabadi after school was fun. We also played 'tree jumping' - a game that we developed and played in the near-by groove. I am not clear why we stopped playing - either teachers came to know and punished or one of us got injured. I wouldn't call this time as a period of innocence, because I wasn't, but it was definitely a period void of responsibility, though I studied hard.

1989-1990: Joined St. Xavier's, a Jesuit run catholic school. I would tease a priest so much, that my dad had to visit the school and apologies to the head master. This is also the time I watched my first English movie - Armour of God - with schoolmates. Though I didn't understand a word of Jackie Chan's English, his actions pumped adrenaline up.

1990: Went to St. Xavier's College, Palayankottai. Took up Physics with a passion of becoming a scientist. I did well both in curricular and co-curricular/extra-curricular activities. Co-curricular activity was quiz programs and extra-curricular activities were, coming up with innovative excuses for not attending daily mass, roaming the town, watching lots of movies! First impression of hostel life - simply superb.

After around two months, I went to Karunya Institute, an Engineering college. Took up Electronics & Communication Engineering (ECE), but most of us mistook it as Enjoy-College-Everyday. I enjoyed by dreaming all about computers; I would be the reference point for all things computers. But I did very badly in other subjects. On reflection, I regret for spending all the time on computers and spirituality; I should've yielded to temptation every now and then and enjoyed with gals!

1994: Graduated out of Engineering college and out in the world. Damn! No one taught us, this world would be so tough. Finding a job was tough, getting through life was tough, everything was tough. I managed to get a job as a video editor in a studio. Though this would create a passionate hobby in me, it was a totally different field.

1995: Probably, I would've ended up as a editor for one of Russell Crowe's movies. But I couldn't withstand the filth in the industry and so, I quit and become a tutor. I worked 12 hours everyday for a meager pay.

1996 - 1998: Became an independent software consultant for a leading Cardiac surgeon in India. This job would open my eyes to so many things, especially to money. I saw ABBA's words to be true: 'It is always sunny in the rich man's world'. They were earning my annual salary in a month! I learned the software development life cycle and associated skills during this time period.

1998: Joined a 'real' software firm. First six months were really hard. One of the team members would make my life hell and would do everything to release me from the project. But it was a blessing in disguise. I was spotted by a dynamic project manager and then later referred for a Vantive project. Ended up going to LA, USA for two months. First glimpse of the dreamland.

1999: Landed in Belgium. First two years sucked, more so because of personal life. Second part was great. I would have everything in excess - food, wine, fun, pleasure, travel and so on. Of those years in my memory, these two years are the period that I can claim that I lived.

2003: Trip to Belgium ended the same way it started - unexpectedly. Back to India. Misery continued. Horrible period until 2005 March. Even job was tough, but I got 'high' by kicking few bosses and bossing around people. Am I too conservative? May be I should read more of Osho and enjoy-every-day!

2005 March: Light at the end of the tunnel. (And I am sure it is not the light of the on coming train).

