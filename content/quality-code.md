---
title="Why Indian developers struggle to write quality code and what can be done about it?"
slug="quality-code"
excerpt="Let us go beyond the surface-level symptoms to identify fundamental problems."
tags=["coach"]
type= "post"
publish_at= "20 Oct 19 06:00 IST"
featured_image="https://cdn.olai.in/jjude/moon-crater-bng.jpg"

---

_People differ about quality, not because the quality is different, but because people are different in terms of experience. - Robert M. Pirsig, Zen and the Art of Motorcycle Maintenance: An Inquiry Into Values_

It has long been argued in the west, that Indian developers are [incompetent to write quality code][1]. Now it is becoming a fashionable concern among Indian executives too. The other day, I was in a gathering of top executives of the IT companies in the city, and they started debating a similar question. The answers were entirely predictable — schools don't teach relevant skills; developers are not enthusiastic about upgrading themselves; everybody is looking for shortcuts, and so on. I was guilty of thinking similar lazy, and easy cop-out answers.

> If everyone is thinking alike, then somebody isn't thinking. - George S. Patton

As a coder for more than two decades, I worried only about improving my craft. As an introvert coder, I shunned from poking my nose into anyone else's code unless specifically asked to assist. After I became a CTO, mediocre code produced by others became my burden. Initially, I subscribed to the same reasons that everyone quoted. The more I discussed with developers and peers, and I saw the problem differently. The easy answers are just the symptoms. The trouble runs deeper.

### Is there quality anywhere in India?

![Moon crater in Bangalore](https://cdn.olai.in/jjude/moon-crater-bng.jpg)

Indian roads are the epitome of Indian coders. Millions of rupees are spent on Indian roads. Yet, even a slight monsoon rain causes potholes resembling moon-craters. This is the case even in the so-called silicon valley of India, Bangalore. When Nanjundaswamy, an artist, [moon-walked][4] a damaged road in Bangalore to highlight the plight of that road, [twitter users][5] in other parts of India requested him to moon-walk their streets. Such are the conditions of roads across the length and breadth of India.

It is not just the roads that are abysmal. Take airports. [Leaky airport-roofs][2] are common occurrences. They don't shock us anymore. We just get on with life by placing plastic trays to harvest rain-water within airports. Every year when floods disrupt normal life in Mumbai and Chennai, politicians and bureaucrats will invoke the [spirit of the city][3] and hide their failures. Residents, which include coders, will suffer but go on. You would think the swanky malls and office buildings are a paragon of quality. Even the best of them are islands of quality, but most of them are just lipstick-on-a-pig. I can go on, but you get the drift.

**Experiences shape our notion of quality. It is impossible for someone who spends his everyday life in this shiny-looking-but-broken-underneath environment to develop a higher sense of quality.** He can wobble his head vigorously for the quality pep-talk, but as experience has proven, that wobble doesn't translate into action.

Skilling and even re-skilling is not an issue. Developers can register on Udemy or other sites to learn newer technology and skills. But unless developers can soak in quality, the quality of code developed by them isn't going to improve.

This is why I recommend young developers to take up onsite assignments, so they can experience the quality of infrastructure first-hand and develop their sense of quality.

### Hiring 100 chickens to plow fields

In the early days of IT in India, IT companies were headed by executives who came from traditional industries like manufacturing. They did not understand that IT is a knowledge industry, where the number of bodies in the project and the number of hours on the seat doesn't correlate with productivity.

Unfortunately, even the recent executives perpetuate the same old flawed thinking borrowed from the industrial age. I bet none of them read 'Mythical Man-Month,' or even after reading it, they still **believe in the myth of interchangeability of men and months**.

When the focus is on increased billing, and not quality, the concomitant effect is to focus on adding headcounts, not building competence. The industry still operates on "rate-cards," which implies we are not dealing with a team of developers, but a bag of potatoes.

**When greed kicks in on billing**, the experienced developers are replaced with more number of freshers. When you replace oxen with chickens, the executive is awarded with bonus for increased headcount and reducing cost, but guess what? The field is now full of chicken-shit. What's the point then of lamenting, "oh, the developers are incompetent."

There is nothing wrong with adding freshers into projects. But is it done to develop competent developers by shadowing experienced developers? Or recent graduates are placed only to replace expensive but skilled developers. Sadly, the reason is mostly later.

### No celebration of freedom of thought

One of the consistent complaint of the western clients is that "Indian developers are just extended hands." What is the reason behind this behavior?

Hinduism, the native theology of the land, celebrates variety. Asceticism, as well as eroticism, are part of the religious text. Vegetarianism, as well as animal sacrifice, are part of the practice. Hinduism is the only religion that accommodates theist, agnostic, and even atheist.

Yet, the society that practices Hindusim, **chokes freedom of thought and forces uniformity** in eating, reading, and watching habits. A child growing in such a home learn to obey rather than expressing their thoughts out loud. When getting into the workforce, this child is not going to morph into an independent thinker suddenly. Even within organizations, developers are controlled to stay within bounds.

When the mind is conditioned, at home, society, and office, to stay within bounds, how is it possible to freely discuss ideas only with clients?

### There is hope

Are there ways to solve this problem?

For a starter, let developers experience quality. Let quality exhibit in the office, in the seats they sit, and in the office infrastructure. Work with the city administration and improve the quality of the area near the offices.

Understand this is the knowledge industry, and managing people in such an industry is not the same as in legacy industries. Read "Mythical Man-Month," twice. Treat developers as people rather than as billable bodies. Call them engineers rather than resources.

Design workplaces so new ideas are discussed and tested. Let the developers surprise you with their ideas, even the crazy ones.

The more developers develop a sense of quality, they will be able to produce quality code.

### You may also like

- [How to think structurally? A framework for developers](/structured-thinking/)
- [Mastering sales as a new CTO](/cto-sales/)
- [I have seen the future of jobs and it is Hollywood model](/future-of-jobs/)

[1]: https://www.theatlantic.com/international/archive/2013/10/behind-the-bad-indian-coder/280636/
[2]: https://www.thenewsminute.com/article/roof-turned-showers-dmk-s-kanimozhi-slams-chennai-airport-leaky-roof-109248
[3]: https://www.indiatoday.in/india/story/mumbai-rain-fury-waterlogging-bmc-shiv-sena-bjp-1034261-2017-08-30
[4]: https://theprint.in/india/chandrayaan-moonwalk-not-his-first-bengaluru-man-is-known-for-fixing-potholes-with-art/
[5]: https://twitter.com/baadalvirus/status/1168768799408967682
