---
title="How we learn to be a father?"
slug="learn-to-be-father"
excerpt="We are not taught to be a father. In this episode KK and I discuss how we learn to be a father by observing those around us."
tags=["gwradio","parenting"]
type="post"
publish_at="16 Jan 24 07:52 IST"
featured_image="https://cdn.olai.in/jjude/learning-to-be-a-father.jpg"
---
In this series on "Being a dad", KK and I talk about everything about fatherhood. On today's episode we talk about our role-models as fathers, how we learn about being a dad, and how society influences fatherhood. I hope you find this helpful in being a dad.

<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless="" src="https://share.transistor.fm/e/7c3ddd1c"></iframe>

## What you'll hear
- Introduction
- Rolemodels for us
- Peer pressure on fathers
- Is community living good or bad
- Challenges of fatherhood
- Seeking advice or complaining
- Bonding with kids

## Edited Transcript

_coming soon_

## Watch on YouTube

<iframe width="560" height="315" src="https://www.youtube.com/embed/CYDyIVYr6jE?si=NVgtiG89qbkSojCw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

## Connect with KK
- LinkedIn: https://www.linkedin.com/in/storytellerkrishna/

## Connect with me
- Twitter: https://twitter.com/jjude
- LinkedIn: https://www.linkedin.com/in/jjude/
- Website: https://jjude.com/
- Newsletter: https://jjude.com/subscribe/
- Youtube: https://youtube.com/gravitaswins
- Email: podcast@jjude.com
- Executive Coaching Program: https://gravitaswins.com

## Your feedback counts

Thank you for listening. If you enjoy the podcast, would you please leave a short review on Apple podcast or on YouTube? It takes less than 60 seconds, and it really makes a difference in finding this podcast. And it boosts my spirits. 😀

I would like to hear what resonated the most with you in this episode. Either email me or share it on social media and tag me. Thank you for your support.

Have a life of WINS.

_Gravitas WINS Radio is produced by [Joseph Jude](https://jjude.com/). Be sure to subscribe on [Apple Podcast](https://podcasts.apple.com/in/podcast/gravitas-wins-radio/id1562802449), [Spotify](https://open.spotify.com/show/7LOBjAyjBkechMC0xMIj9E),[Google Podcast](https://podcasts.google.com/feed/aHR0cHM6Ly9mZWVkcy50cmFuc2lzdG9yLmZtL2dyYXZpdGFzLXdpbnMtcmFkaW8=),[YouTube](https://www.youtube.com/gravitaswins), or the podcasting app of your choice._

Please check out other episodes of [Gravitas WINS Radio](/gwr-themes/). For easy consumption, they are categorized by themes. If you are a fellow podcaster, you might be interested in [How I Podcast](/how-i-podcast/) or [Tools I Use](/i-use/).

## Continue Reading

- [Mahendran Kathiresan on 'Homeschooling In India'](/mahendrank/)
- [Sweet home is a safe home, even digitally](/sweet-home-safe-home/)
- [How our fathers, society, and peers shape our fatherhood](/fatherhood-influences/)