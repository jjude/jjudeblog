---
title="Topics that interest me now"
slug="topics1920"
excerpt="Deriving inspiration from a tweet by Jeff Morris Jr., here are the topics that interest me now."
tags=["insights","sdl"]
type="post"
publish_at="01 Sep 19 06:30 IST"
featured_image="https://cdn.olai.in/jjude/topics1920-gen.jpg"
---


When the world changes, I change my focus.

As I [blogged last week](/wins/), I am focusing on WINS - **wealth, insights, network, and self-control**. In a multi-dimensional world, you can't generate valuable insights if you focus only on one field. You combine two or more areas and create something new at the intersection. Thus, I'm interested in many topics, but the thread that combines all of them is, "**future of business, jobs, and skills**."

As a father of two boys, I am also interested in **effective parenting**.

Combining all of these, here is the list of topics I'm interested now.

**Wealth**

- Ways to build wealth
  - Linear (Exchanging time for money. Examples: Salary, overtime, commission, consulting, professional services)
  - Leverage (Leveraging other people to make money. Examples: sub-contracting, salaried staff, strategic alliances, running a company)
  - Passive (Income while not working. Example: renewals, interests, dividend, books)
  - Windfall (Sudden large income. Example: winning lottery, appreciation of stocks and real estate)

**Insights**

- Ways to build insights
  - Consume (Books, TED Talks, Podcasts, White-papers)
  - Produce (Blog posts, Talks, Books, Podcasts)
  - Engage (On blogs, social media, networking events)

**Networking**

- Visibility (become visible to your potential network and actively maintain it)
- Credibility (keep your appointments and promises, render services; let your results speak louder than words)
- Profitability (both benefit from the relation)

**Self-control**

- Wellness
  - Mind
  - Body
  - Spirit
  - Social

**Family / Parenting**

- Teaching Gratitude
- Disciplining

**Topics to build Insights**

- Gaining Wisdom
  - Faith / Ancient wisdom
  - Alternate educational models
  - Framework thinking
  - Mental Models
- Gaining wealth
  - Financial Independence
  - Personal finance & literacy
  - Media + Code = wealth
- Preparing for future
  - Scenario planning
  - Role of software in society
  - Role of entertainment in society
  - Future of Job
    - Remote working
    - Group hiring (hire a senior engineer + the designer + engineers they know as a whole package)
    - Return of gurukuls
    - Productivity
    - Tools for distributed teams
  - Future of Business
    - Disruptions in Agriculture (farming, water)
    - 3D printing
    - Living a digital life
    - Future of consumer
    - Convergence of B2B & B2C
    - Luxury software
    - Monetization
    - Bluetooth based social
    - Machine Learning & General AI
    - Crypto
      - Easy to play games
      - Privacy
      - Decentralized finance
      - Decentralized governance
    - Avatar based streaming
    - Hard to find data marketplace
    - Cashless payments
    - Instant personalized experience
    - Anti fragile/ disaster resistance
  - Future Skills Needed
    - Storytelling
    - New Media
    - Persuasive communication (write, talk, video, new media)
    - Dealing with fake news
    - Reputation scores & trust

_This post was triggered by a [tweet](https://twitter.com/jmj/status/1162486224570867714) by Jeff Morris Jr. I have generously borrowed ideas from that twitter thread._

I will blog about these here. If you are interested in these factors, please subscribe using the form below.

