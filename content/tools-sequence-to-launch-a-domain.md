---
title="Tools to launch your domain"
slug="tools-sequence-to-launch-a-domain"
excerpt="If you write down the sequence and follow it, you won't miss a step and will save ton of time."
tags=["martech"]
type="post"
publish_at="11 Nov 14 06:46 IST"
featured_image="https://cdn.olai.in/jjude/tools-sequence-to-launch-a-domain-gen.jpg"
---

I have been registering and launching domains regularly. The first one I registered was my personal domain [jjude.com][1], the latest one is for the company I started, [DSD Infosec][2]. With the experience of launching multiple domains, I have formulated a sequence in launching a site.

In this post, I detail these steps along with the tools I use:

1. **Domain Name Search**: Search for the domain name at [Instantname][3]. Instantname provides all the available names categorised by top level domains, domain hacks, regional level domains along with  registration prices at different registrars. It is a great time saver since I can view all the required information in a single screen. If you are particular about short domain names, you can also use, domai.nr, though I have not used them.

2. **Domain Registration**: I use [Godaddy][4] for all domain registration. For one site, I tried [hover][5], but I transferred the site back to Godaddy, since I would like to have all sites with the same registrar. It makes it easy to monitor and renew sites.

3. **Website Hosting**: I use Webfaction to host all my domains. Their hosting plan includes web hosting, db hosting and email servers. I have been their customer close to ten years now. Their hosting prices are competitive; but their customer service is beyond any comparison. It is the best.

4. **Email Ids**: I create at least two email ids for all the domains. First one is the primary email id and the other one is exclusively for mailing lists. If I’m creating a website for an app, I may create a support mail id too. Webfaction has a concept of mapping multiple email ids to a single mailbox. So I may publish multiple email ids for a domain, but I have to check only one mail id, from within the mail client (Gmail, Apple mail, iOS mail).

5. **Cloudflare Optimisation**: I use [cloudflare][7] to protect and accelerate all my sites. Once I include the domain in Cloudflare, I change the name servers in [Godaddy][4].

6. **Social Media Handles**: I use only [twitter][8] for social media presence. Sometimes, I don’t get a twitter handle of the exact name. Then I try appending ‘app’ to the name (suited if the handle is for an app, if the domain is for persons, you can try ’says’). While you are at twitter, create a [twitter card][9] too.

    I created a [Facebook page][10] only for this domain.

7. **Mailing List**: Email is still the best marketing channel. I use [mailchimp][11] for managing the mailing list. Even for a blogger, mailing lists are useful. I get to know subscribers and then when I write a post, I visualise having a conversation with them. It helps me to get into writing flow.

8. **Web Analytics**: I don’t obsess over analytics. [I focus on what matters][15] and ignore other parameters. I use [Google Analytics][14]; I used to use [Statcounter][12], but recently I started using [Clicky][13]. I configure the analytics solution to email the reports. It saves me a lot of time.

9. **Landing page**:  I use [bootstrap][16] for creating landing pages. There are plenty of resources (templates, tips, code-snippets) available for bootstrap, so I can create a landing page quickly.

10. **Source control**: Every code I write, including HTML & CSS, are pushed to source control. I have separate repository for each domains. Having site code in source-control has three benefits: I can clone an existing repository for a new site; I can track changes; and if I make a mistake, I can revert to an earlier working version. I use [Bitbucket][17] as a source control tool.

I don’t launch domains every day. So having a sequence and writing that sequence helps me not to miss any step. It is also a saves me time, since I don’t have to keep revisiting the steps.

Was this helpful to you?

_Disclaimer: Links to Clicky and Hover are affiliate links_

[1]: https://jjude.com
[2]: http://www.dsdinfosec.com
[3]: http://instantname.me/
[4]: http://godaddy.com
[5]: https://hover.com/bdRwW8qx
[7]: https://www.cloudflare.com
[8]: https://twitter.com
[9]: https://dev.twitter.com/cards/overview
[10]: https://www.facebook.com/Certainities.Serendipities
[11]: http://mailchimp.com
[12]: http://statcounter.com
[13]: http://clicky.com/100782648
[14]: http://www.google.com/analytics/
[15]: /focus-on-what-matters-or-sleep-alone/
[16]: http://getbootstrap.com
[17]: https://bitbucket.org

