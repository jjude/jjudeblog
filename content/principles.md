---
title="My General Operating Principles"
slug="principles"
excerpt="Guidelines for effective life"
tags=["self","wins"]
type="page"
publish_at="30 Jan 16 07:49 IST"
featured_image="https://cdn.olai.in/jjude/principles-gen.jpg"
---

Whenever we face tough situations, we seek answers to solve that particular situation. What if, we sought guiding principles rather than specific answers?

With many trials, I've discovered the principles that bring greater satisfaction to me, and those that bring the best in me. In turn, these principles guide me navigate through new situations with ease.

* I believe that **life is a non-zero sum game**. I don't have to lose for others to win. We can find a deal that works for both parties in the game.
* **Life is fun if you don't compare**. I am happy if I am better than what I was. I don't compare myself with others and feel neither elated nor dejected.
* I believe that **integrated life is a joyful life**. There can never be a balance between work and life. Integrate life into work and work into life.
* I believe in a **growth-mindset**. I'm always learning something new. I even designed a [framework][1] to learn.
* I nurture the newly learned principles through **disciplined practice**. Use it or lose it.
* I like to **work with people whom I trust**. That way, I can channelise my focus on what I'm creating rather than worrying if I will get my part of the deal. No amount of contractual clauses can assure you confidence, if you don't trust other parties in the game.
* Because I work with people I trust, I seldom **negotiate**. If I get a fair deal, I accept. When I negotiate hard I get more, but I become less.
* When troubles come, go to a **mountain and weep**, if you have to; but come back and **face them**. It is a prevailing prejudice that men shouldn't cry. If I feel helpless or feel overwhelmed, I cry. After weeping enough, I always find the courage to face the situation.
* **If I don't have to think, I don't think.** I try to make as much of life as routine and focus my energies on where I can have more impact. My mornings are on auto-pilot. I use Mac, primarily because it lets me do what I want to do. I don't spend time on unproductive activities.
* It’s **no humility to live a lesser life** than the one you are capable of living. In fact it’s a crime to live such a life. Ever since I read these lines in 'Blue Sweater' by Jacqueline Novogratz, I have tried my best to live up to this principle.
* **Deliberate practice beats intelligence**. I'm not the most intelligent in any crowd. But I practice longer than the most.

I keep revisiting these principles and tweak them as my understanding evolves. I'm sure, I will keep revising them until I die.

I encourage you to think deeply and craft your operating principles. That will help you live a fulfilling life.

_Last updated: Jan, 29 2016_

[1]: /learning-phases-and-its-support-systems/
[2]: http://taylorpearson.me/

