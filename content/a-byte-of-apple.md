---
title="A byte of apple"
slug="a-byte-of-apple"
excerpt="experience of a recent convert to Mac world"
tags=["others"]
type="post"
publish_at="11 Mar 09 16:30 IST"
featured_image="https://cdn.olai.in/jjude/mac-apps.png"

---

I wanted to own an [Apple MacBook](https://en.wikipedia.org/wiki/MacBook#Aluminum_Unibody_MacBook) for long. Very long. Since I turned an [independent consultant](/new-year-brings-a-new-challenge/), I thought why not go for a MacBook?

Coming from 'Microsoft Windows' platform, the first impression wasn't that impressive - after all even Windows has GUI, dock & task bar. But when I started using it, I realized why Mac is superior. In less than two weeks, I've come to like MacBook. There are at least three features that make Mac to stand out (I'm not sure even in a decade such features can come to Windows based laptop; same holds for Linux too):

- **Install & uninstall experience** - Drag and drop into 'Application Folder' for install; drag & drop into 'Trash Can' for uninstall. Just as simple as that. No registry; no clean-up of messy folders. Mac OS folks have done really good design and it is simply superb.
- **No shutdown** - Okay, not necessary to shut down. Close the lid and its off; Open and start working. Cool, isn't it?
- **Spotlight** - something like desktop search. It is quick and precise (only once it didn't return what I was looking for).

#### Mac Applications

A concern that I had was that I may have to spend quite a lot on applications. Not true. There are lots of open source and free applications for Mac too. And most of them are elegant in UI and efficient in what they are supposed to do. Here are some of the applications that I'm using:

![Mac Applications](https://cdn.olai.in/jjude/mac-apps.png "Mac Applications")

**Web Browsing** : Mac comes pre-installed with Safari, which is a fast browser. I am also using Firefox.

**Email** : Mail, the pre-installed email program doesn't have a provision to download only the headers. I'm using Gmail with offline feature and it is more than sufficient for me.

**RSS Feedreader**: Again I depend on Google for it. I'm using Google Reader with offline feature.

**Office Suite** : I'm using the Apple iWork. Might give a try to NeoOffice.

**Games** : I'm surprised that Mac doesn't come with pre-installed games pack. There is only chess. I'm searching for some good free games.

**Chat**: My chat buddies have grown old (no I'm still young ;-) ). Everyone in the current friends circle use Gmail chat and so I go with it too.

**Programming** : I'm using [TextWrangler](https://en.wikipedia.org/wiki/TextWrangler) for coding. But couldn't find a decent SVN client. I tried SCPlugin, but for some reason it didn't work.

**Photography**: I've been using Adobe Photoshop Elements and it worked in Mac too. Still to edit any photo in Mac but I'm sure the experience is going to be good.

**Note Taking** : [Voodoopad Lite](http://flyingmeat.com/voodoopad/voodoopadlite.html) is a desktop wiki for Mac and it makes it easy to takes notes and organize within a single physical file.

**Personal Finances** : I tried [mmex](http://www.codelathe.com/mmex/). But it kept crashing, though it works fine under Windows.

**VirtualBox** : I couldn't find a decent Desktop Blog writer than Microsoft Live Writer; also I already bought [SmartDraw](https://www.smartdraw.com/). So went with VirtualBox to run Windows inside Mac. I am also using MMex for personal finances under Windows.

**Others** : iSync for syncing addresses & calender from Nokia E71; TweetDeck for tweets; [SLife](https://www.slifelabs.com/) for tracking hours spent on applications; [Books](https://books.aetherial.net/wordpress/) for cataloguing; Grab for taking screenshots.

Are there any other interesting Mac applications (preferably free) that I should try?
