---
title="Delightful Customer Experience in a Highway Restaurant"
slug="cx-in-highway"
excerpt="The more you interact with your customers, you will understand them more."
tags=["coach","sales","experience"]
type= "post"
publish_at= "14 Jan 20 08:00 IST"
featured_image="https://cdn.olai.in/jjude/kyc.jpg"
---

![Hotel Akshaya Bhavan Owners](https://cdn.olai.in/jjude/hotel-akshaya-bhavan-owners.jpg)

We drove to Tuticorin for our annual holidays. On our way back, we were hungry and wanted to eat breakfast. Eating at highway restaurants is risky, especially when you are with kids. My sister, the smartest in any crowd, decided to search for a decent restaurant on the highway. She stumbled on a restaurant named “Akshaya Bhavan,” which had 4.6 ratings in Google.

Akshaya Bhavan is behind a petrol station and unsophisticated. No fancy arches or shining interiors. While sitting on the wooden benches, we whispered to each other, is this the same place that had a high rating? Did we stop at the wrong place? My sister checked on Google Maps, and it confirmed we are in the right place.

In a few minutes, a waiter came with the traditional banana leaf. After we cleaned the leaf with sprinkling water, we asked the waiter the same question that everyone asks: what is here?

We ordered Pongal, Idli, and Dosa. Each came with a sambar and three chutneys - mint, coconut, and tomato. I started with Pongal. I remember telling my sister, wow, this is fantastic. As we were chatting and enjoying the food, an old man walked towards us, pointed to me and said, “thambi, don’t eat Pongal with a spoon. Use your hand, and don’t forget to mix chutney with it.”

He is the owner of the place. As he was inquiring about us and our travel, his wife joined with a bowl of vadas. **Bowled over by delicious food and their unmatched hospitality, we ate more than we had planned**. I am not sure if the owner knew the concept of “management by walking around,” but clearly, he was practicing it. We were so impressed that we took a photograph with them. I have never done that. Never.

They had a delicious product and an excellent service. No wonder the earlier customers rated the restaurant highly on Google.

![Know your customers](https://cdn.olai.in/jjude/kyc.jpg "Know your customers")

**He knows something most CEOs don’t**. The more he interacts with customers, the more he understands his customers. Since he gets feedback directly and not via some proxies like survey form or help-desk, he will get to know customer tastes and changing trends immediately.

If you ever drive near Tuticorin, stop by his restaurant. More importantly, practice what he does - go and talk to your customers.

## Continue Reading

* [Should You Run That Extra Mile to Make a Sale?](/extra-mile-for-sale/)
* [Customer Service Is Sales](/customer-service-is-sales/)
* [System for success](/system-for-success/)