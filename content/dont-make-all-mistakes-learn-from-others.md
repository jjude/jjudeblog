---
title="Don't Make All Mistakes; Learn From Other's"
slug="dont-make-all-mistakes-learn-from-others"
excerpt="We can't learn from our mistakes because we, as humans in general, are averse dissecting them. So let's learn from other's mistakes."
tags=["problem-solving","insights"]
type="post"
publish_at="14 Apr 11 16:30 IST"
featured_image="https://cdn.olai.in/jjude/dont-make-all-mistakes-learn-from-others-gen.jpg"
---

With plethora of PMP certified professionals and management graduates in IT industry, it is surprising that 40% of IT projects still fail.

Part of the problem is learning to make project plans only for happy-path scenarios. We don't learn what factors could derail the project and what we can do about these factors. "We learn from our mistakes", remains often repeated but quickly forgotten phrase. We can't learn from our mistakes because we, as humans in general, are averse dissecting them.

But Michael Krigsman, CEO of [Asuret, Inc.](http://asuret.com/), is doing just that. He runs a blog, called [IT Projects Failure](http://www.zdnet.com/blog/projectfailures?tag=mantle_skin;content), where he analyzes failures of various IT projects, majorly ERP as it is falls as a bigger pie of IT project implementation. Latest one being Marin County’s [lawsuit](http://www.zdnet.com/blog/projectfailures/marin-county-claims-racketeering-against-deloitte-and-sap-part-one/12749) against Deloitte Consulting and SAP.

He not only provides analysis of such failures but distills these learnings into [early warning signs of failure](http://www.zdnet.com/blog/projectfailures/twelve-early-warning-signs-of-it-project-failure/10561?tag=mantle_skin;content) and [how to prevent doom](http://www.zdnet.com/blog/projectfailures/13-warning-signs-to-prevent-erp-doom/10826?tag=mantle_skin;content) among other lessons. He also bunks the myth that [only large projects fail](http://www.zdnet.com/blog/projectfailures/do-large-projects-really-fail-more-often/10522?tag=mantle_skin;content).

If you are a technology project manager, go read his [blog posts](http://www.zdnet.com/blog/projectfailures?tag=mantle_skin;content). You will avoid repeating mistakes of many.

