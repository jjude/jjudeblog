---
title="Striking a balance"
slug="balance"
excerpt="Is striking a balance always right?"
tags=["insights"]
type="post"
publish_at="24 Feb 14 16:30 IST"
featured_image="https://cdn.olai.in/jjude/balance-gen.jpg"
---

This world is full of people with strong opinions. The people who think that they are on the 'absolutely' right side and all others, who don't agree with them, are on the 'wrong' side. For them it is, 'either you are with us or you are against us' - a ridiculously immature attitude.

Not even for a moment, they can entertain the idea that there is tiny teeny possibility that they could be wrong (borrowing on the words from the character Phoebe in Friends series).

Take a simple case of, enjoying the present to worrying about the future. Should I spend all my money (or effort or whatever) enjoying the present or save for the future?

Another scenario that I quite often find myself is whether to leave things to 'fate' or 'to take control of life'.

Same applies to 'religion' and 'science'. Or 'Christianity' vs 'Islam' vs 'Hinduism' vs any other numerous regions on the face of the earth.

I have been on both sides of the argument and have come to realize that there is no absolute position.

I need to live at the present; yet be concerned about the future.

I need to plan and do my best; yet understand that there will be events beyond my control.

I won't even get in to the religious topics. That is where the balance is needed the most.

I believe it is a paradox of life that one needs to be balanced.

Of course I agree that, that line of balance changes to one to the other; or as we age.

Point is, do you consider that you are at the right side and all others are just plain wrong?

So is striking a balance always right? And all others wrong?

