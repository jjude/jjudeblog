---
title="Safari - A digital library for your career growth"
slug="safari"
excerpt="Safari Online is a digital library for CXOs that has e-books, case studies, learning paths, and much more."
tags=["sdl","coach","consume"]
type="post"
publish_at="18 Jun 18 10:30 IST"
featured_image="https://cdn.olai.in/jjude/safari.jpg"

---

If you are not updated, you will be outdated. That's the unwritten rule in professional lives.

How can you keep yourself updated?

You can attend conferences to know the latest trends. Or attend hands-on workshops to learn new skills. They are effective, but also take a lot of time. You also have to travel to these places of conferences and workshops.

**Reading books and attending webinars** are other ways of improving your skills.

I used to buy and read a ton of books. When I shifted from Chennai, a decade back, I had more boxes of books than boxes of clothes.

When **Kindle** came out, I shifted to e-books. Kindle library and apps were a gift for lovers of books. I bought many books on technology and business.

Then I became a CTO. The **rate at which I had to learn multiplied** when I became a CTO.

I had to understand newer technologies and their impact on business, update myself on changes in existing programming languages, and know changes in business trends. I didn't want to buy a mountain of books and read them cover-to-cover. I had to find a way to become familiar with these concepts as quickly as possible. That's when I stumbled on Safari Online. Since then it has become my digital library.

![Safari Online Homepage](https://cdn.olai.in/jjude/safari.jpg "Safari Online Homepage")

**Safari Online has books from a variety of publishers**. O'Reilly, John Wiley, Packt, and Apress are some of the publishers in the service.

I read some books cover to cover. Usually, I skim most books focusing only on certain paras and chapters. I highlight those interesting paras. Later I re-visit **only those highlighted paras**.

I can read on the web and on mobile devices. I browse through on the web and add books to my queue. Then I download on [iPad](https://itunes.apple.com/us/app/safari-queue/id881697395?mt=8&at=1000lMsn) or [iPhone](https://itunes.apple.com/us/app/safari-queue/id881697395?mt=8&at=1000lMsn) and read on the mobile. I'm listing some of the books I have read in the last year.

- [Soft Skills: The software developer's life manual](https://www.amazon.com/dp/1617292397?tag=jjude-20) by John Z. Sonmez
- [Seven Steps to Mastering Business Analysis](https://www.amazon.com/dp/1604270071?tag=jjude-20) by Barbara Carkenord
- [Mythical Man-Month, The: Essays on Software Engineering](https://www.amazon.com/dp/0201835959?tag=jjude-20) by Frederick P. Brooks Jr.
- [Driving Digital](https://www.amazon.com/dp/0814438601?tag=jjude-20) by Isaac Sacolick
- [Getting Started with React Native](https://www.amazon.com/dp/1785885189?tag=jjude-20) by Tom Bray and Ethan Holmes
- [Value-Based Fees: How to Charge—and Get—What You're Worth: A Guide for Consultants](https://www.amazon.com/dp/0470275847?tag=jjude-20) by Alan Weiss

These are the books I read at least 70% and more. There are many other books I read only parts of the book.

### Not just digital books

Safari Online recently added two cool features — case studies and learning paths.

**Case studies** are an amazing list of how companies implemented a certain concept. I have listened to case studies videos for DevOps, micro-services, data analytics and so on. One example is [Data analytics for product quality at QuintilesIMS](https://www.safaribooksonline.com/case-studies/analytics/data-analytics-for-product-qua/9781491991336-video307592/).

**Learning path** is a curated curriculum of video courses on a particular subject. I have taken learning path on d3.js and react-native so far.

They also have **webinars**. I haven't attended many as they are at midnight for Indian time.

### Needed improvements

Even though the web application and mobile applications are well done, there are certain improvements needed in them.

1. "Download video" feature for iOS apps: You can download the books into the mobile apps, so you can read offline. A similar feature is missing for videos.
2. Export highlights for a single book: It is strange that you can't download highlights for individual books. You can only download all the highlights. I raised this as a support issue. Apparently, they know about this bug.

### Price

Now to the most important piece — price. It is **expensive**. It costs $399 per year. I got it at lifetime discount of 50% (every year I pay $199). Even at a discount, the price is high, but I find so much of value that I have this subscription for the 2nd year in a row now. They run regular promotions during the year. You can register and they will let you know.

There are few other educational subscriptions, a popular one is Pluralsight. I subscribed to it for six months. PluralSight is limited to IT skills. So I jumped back to Safari Online.

### It is a great choice

If you want to improve your skill-set across business domains, Safari Online is a great choice.

_Disclaimer: Links to Amazon books and iOS apps are affiliate links. That means, if you purchase through these links, I get a commission._

### Related Links

- [How I Use Twitter As A Learning Tool?](/learn-via-twitter/)
- [Are You Learning A New Domain? Visit Its Zoo](/are-you-learning-a-new-domain-visit-its-zoo/)
- [Learning Phases And Its Support Systems](/learning-phases-and-its-support-systems/)
