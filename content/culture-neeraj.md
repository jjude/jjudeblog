---
title="'Building the winning culture' with Maj Gen Neeraj Bali (Retd)"
slug="culture-neeraj"
excerpt=""
tags=["gwradio","coach"]
type="post"
publish_at="09 Jan 24 06:28 IST"
featured_image="https://cdn.olai.in/jjude/winning-culture.jpg"
---
In this episode of Gravitas WINS Conversations, Joseph Jude interviews Major General Neeraj Bali, a SENA medal recipient and author of '[The Winning Culture](https://amzn.to/4aRYrN9).' 

General Bali talks about the importance of culture in organizations, using examples from the Indian Army and Indigo airlines to underscore his points. He emphasizes the power of culture to guide behaviors and lead to extraordinary outcomes. The discussion sheds light on how to develop a robust culture, the role of leadership in shaping culture, and the types of errors common in organizations. 

The discussion includes fascinating accounts from General Bali's military career and how these experiences shaped his approach to leadership and culture-building. The interview concludes with a discussion on the definition of living a good life, focusing on the importance of finding purpose and meaning in one's endeavors.

<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless="" src="https://share.transistor.fm/e/972629af"></iframe>

## What you'll hear
- What is culture and why it matters?
- Culture is a force multiplier of strategy
- How to build culture?
- Role of members in buiding culture
- Errors of judgement, omission, and intention
- Dealing with suggestions and feedback
- Kindest thing someone has done for you
- What is the best leadership quality
- Definition of living a good life

## Edited Transcript

_coming soon_

## Watch on YouTube
<iframe width="560" height="315" src="https://www.youtube.com/embed/7z0fOVJL16I?si=Zcc4sY8YWBDknmRy" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

## Connect with Maj Gen Neeraj Bali (Retd)
- LinkedIn: https://www.linkedin.com/in/neerajbali7/

## Connect with me
- Twitter: https://twitter.com/jjude
- LinkedIn: https://www.linkedin.com/in/jjude/
- Website: https://jjude.com/
- Newsletter: https://jjude.com/subscribe/
- Youtube: https://youtube.com/gravitaswins
- Email: podcast@jjude.com
- Executive Coaching Program: https://gravitaswins.com

## Your feedback counts

Thank you for listening. If you enjoy the podcast, would you please leave a short review on Apple podcast or on YouTube? It takes less than 60 seconds, and it really makes a difference in finding this podcast. And it boosts my spirits. 😀

I would like to hear what resonated the most with you in this episode. Either email me or share it on social media and tag me. Thank you for your support.

Have a life of WINS.

_Gravitas WINS Radio is produced by [Joseph Jude](https://jjude.com/). Be sure to subscribe on [Apple Podcast](https://podcasts.apple.com/in/podcast/gravitas-wins-radio/id1562802449), [Spotify](https://open.spotify.com/show/7LOBjAyjBkechMC0xMIj9E),[Google Podcast](https://podcasts.google.com/feed/aHR0cHM6Ly9mZWVkcy50cmFuc2lzdG9yLmZtL2dyYXZpdGFzLXdpbnMtcmFkaW8=),[YouTube](https://www.youtube.com/gravitaswins), or the podcasting app of your choice._

Please check out other episodes of [Gravitas WINS Radio](/gwr-themes/). For easy consumption, they are categorized by themes. If you are a fellow podcaster, you might be interested in [How I Podcast](/how-i-podcast/) or [Tools I Use](/i-use/).

## Continue Reading

* [Culture is what you do, not what you believe](/culture-is-doing/)
* [Krishna Kumar N on 'Power of storytelling in business'](/kk-story/)
* [Ian Heaton on 'Listening leaders are effective leaders'](/ian-listening/)