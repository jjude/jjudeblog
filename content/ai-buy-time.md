---
title="How will you use the extra time AI will give you?"
slug="ai-buy-time"
excerpt="AI tools will add more hours to you. Will you use it to relax? If earlier innovation cycles are any indication, we will work more."
tags=["tech","aieconomy","gwradio"]
type="post"
publish_at="24 May 23 16:20 IST"
featured_image=""
---

<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless src="https://share.transistor.fm/e/a8644d02"></iframe>

Although we all have 24 hours a day, everyone's hours are not equal. When the poor must walk or cycle to a place, the rich can take a chauffeur driven car, traveling in luxury, conducting business while traveling and arriving early. The same applies to long distance travel. While the masses travel in cattle class, the rich fly in business class. The richer still take private jets.

Artificial intelligence will allow a large portion of the population to enjoy this privilege. I noticed such a privilege in my recent trip. In Bangalore airport, there is a separate lane called "The DigiYatra lane" which allows you to board using facial recognition. This facility is available in Delhi, and Mumbai in addition to Bangalore. Despite privacy concerns, this reduces time spent standing in queues. We will see more solutions like this that add "hours" to our days.

How will we spend those extra hours? Will we relax and spend time with our family and friends? Maybe explore the arts and literature? Immerse ourselves in music? Learn another hobby like painting? Or serve the underprivileged?

If earlier technology innovation cycles are any indication, we will work more. In the days before laptops and smartphones, we stopped working after a certain amount of time. Today, it is common for people to work weekends and public holidays.

So I won't be surprised that all the hours added by AI will tie us into a much bigger rat race. What do you think? Share your comments on LinkedIn or Twitter.

_This is part of [Short notes on AI Economy](/ai-economy-notes/)_