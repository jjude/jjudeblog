---
title="An overview of Helpdesk functionality"
slug="an-overview-of-helpdesk-functionality"
excerpt="What features should a helpdesk tool have?"
tags=["myapps"]
type="post"
publish_at="18 Jul 08 16:30 IST"
featured_image="https://cdn.olai.in/jjude/helpdesk-process.png"

---

In this post, I describe here my understanding of Helpdesk functionality.

Let us say, you bought a HP Printer. It was working fine. You were happy about the product. Suddenly it is not printing. You want that to be fixed (some might want to know what happened; but majority of us are happy if it was fixed and worked).

Consider another situation. You started with Vodafone services. You joined their amazing 'Family & Friend's offer', because you were convinced by a cute girl in the store that you can form your own 'club' and it is almost free if you call someone within that 'club'. When you received the monthly bill, you notice that it doesn't reflect what you thought you understood. You want someone to explain that to you.

I'm sure all of has such experience.

![Level of support from helpdesk](https://cdn.olai.in/jjude/support-levels.png "Level of support from helpdesk")

Every product manufacturer (service provider) sets up a Helpdesk (service desk/call center) to listen to these customer complaints and probably resolve them in the first call.

However many-a-times the issue can't be resolved in the first call - some one more qualified need to troubleshoot the USB connection or billing team need to provide details of the billing. This will constitute next level in Helpdesk system. The number of levels that an issue traverses depend on the complexity of the issue.

In general, level indicates the increased level of expertise. But it could also be just another department in the company, as indicated in the billing issue above.

All is well if the issue can be resolved at the first call. Like the example that we considered above, there will be instances where a deeper analysis is needed. In such instances, as a customer, you want to know how long it will take for them to get back or to resolve the issue. That is when Service Levels come in. Service Levels define the time that a servicing company (not necessarily that particular agent) will take to resolve the reported issue. That amount of time depends on the criticality of the issue - you would demand a quicker resolution if you are not able to print anything; but would be okay to wait for few days if the printer became slower.

Every firm wants to maximize the benefit of helpdesk implementation by  resolving the issue on the first call - called First Call Resolution. Customer satisfaction drops heavily if they've to call back (or wait for agents to call them back). One of the ways is to implement a resolution database. This is something similar to Frequently Asked/Answered Questions. Building such a DB is an ongoing process and it should be regularly revised. This will increase the number of times, an agent is able to resolve issues on the first call itself.

![helpdesk process](https://cdn.olai.in/jjude/helpdesk-process.png "helpdesk process")

Most of the helpdesk tools additionally provide a 'Diagnostic Scripts' based resolution database. Using such an implementation, agents can search for a keyword and then the tool will guide them through 'Yes/No' type of wizards to arrive at a proper resolution.

![helpdesk state diagram](https://cdn.olai.in/jjude/helpdesk-states.png "helpdesk state diagram")

If the issue is not resolved in the first call and when you call again, you would like to know the status of the issue - is someone working on it; or it is on hold. These are represented by state transitions (or Workflow). State transitions can be a simple one as depicted on the left. However complex transition requirements are not uncommon.

These workflow gets complicated if many departments (or levels) are involved. Some customers will demand a parallel workflow (to indicate the life cycle in the respective department) and merging of the workflow status before providing resolution.

SLAs are calculated in tune with the statuses. What I've shown here is a very simple algorithm. In reality, this gets complicated as well - should the SLA be on hold if the agent is waiting for an answer from another department? Thinking as a Level 1 agent, it should be; but if you take a customer perspective, it shouldn't. Customer is least interested to know that a ticket went through fifty other departments; they are interested only if the ticket was resolved in the stipulated time.

Consolidating all of this, here are the requirements that a helpdesk tool should support.  This is not an exhaustive list; but it lists most of them.

![helpdesk activities](https://cdn.olai.in/jjude/helpdesk-activities.png "helpdesk activities")

Did it give a fair idea of how a Helpdesk function? Do you've any comments? Tell me on [Twitter](http://twitter.com/jjude).