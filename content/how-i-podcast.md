---
title="How I Podcast"
slug="how-i-podcast"
excerpt="From preparation to promotion, here is how I produce my podcast - Gravitas WINS. Updated on August, 2023"
tags=["wins","sdl","produce"]
type="page"
publish_at="28 Aug 23 17:57 IST"
featured_image="https://cdn.olai.in/jjude/podcast-planning.jpg"
---

I started [Gravitas WINS](/podcast/) in April 2021. From the first episode, I've iterated on almost everything - recording techniques, tools, time of release ... As of August 2023, every Tuesday, I release an episode - one is a monologue, another is a 30-minute interview with an industry expert.

In this post, I list all the tools I use for podcasting (as of August 2023).

## What you'll read
- Planning
- Recording
- Editing
- Hosting
- Creating cover art
- Promote

## Planning In Obsidian

![Planning a podcast in obsidian](https://cdn.olai.in/jjude/podcast-planning.jpg)

[Obsidian](https://obsidian.md/) is my digital brain. Everything goes in there. 

- Topics to create
- Episode script
- Sample questions
- References to the topic
- Podcast checklist
- The mistakes I made while recording an interview

I don't use [Obsidian Sync](https://obsidian.md/sync), rather I use [Working Copy](https://workingcopy.app/) to sync desktop and mobile notes. To set one up for yourself, you can read these guides.
- [Sync your Obsidian Vault on iOS with GitHub, Working Copy, and Apple Shortcuts](https://meganesulli.com/blog/sync-obsidian-vault-iphone-ipad/)
- [Setting up iOS git-based syncing with mobile app](https://forum.obsidian.md/t/mobile-setting-up-ios-git-based-syncing-with-mobile-app-using-working-copy/16499/1)

If I'm doing a monologue, I read the content out loud once or twice before recording to check:

- Are there "big" words that I could mispronounce?
- Do I sound like talking to a friend or as corporate mumbo jumbo?
- Does the speaking flow naturally from start to end?

When recording an interview, I practice the questions. Often, the questions are impromptu depending on the guests' answers. But this rehearsal helped me get into the mood.

I am now ready to record the episode.

## Recording

Just before I record, I do some facial and vocal exercises to make sure I sound OK on the podcast.

I repeat phrases like,

- laba laba laba
- peeli pali poli

This is the part where my sons and wife flip a coin to see if I'm still sane. 😱 😂

I use [ATR 2100 USB](https://geni.us/atr2100-mic) with a pop filter to record the podcast. All monologues are recorded via the [Screenflow](https://www.telestream.net/screenflow/) app.

I use [Riverside.fm](https://riverside.fm/) for recording an interview with guests. Why not use Zoom? Most of my guests will have intermittent internet problems. When an interview is recorded using Zoom, my side will be clear, but the other side will be cut off whenever there's a problem with the internet. Riverside records locally, then uploads to their cloud server, so the small interruptions in internet connections don't matter.

## Hosting With Transistor.fm

![Transistor.fm statistics dashboard](https://cdn.olai.in/jjude/podcast-hosting.jpg)

I host with [Transistor FM](https://transistor.fm/) .

I've been following [Justin](https://justinjackson.ca/) ever since his crazy experiements of "100 things to do in a year".

For me creating a podcast is a crazy experiment. So hosting on his platform seemed just about right.

Also I'm part of his [Megamaker](https://megamaker.co/) club 😜

## Cover Art With Sketch app

![Podcast episode media creation with Sketch App](https://cdn.olai.in/jjude/podcast-sketch.jpg)

I use [Sketch](https://www.sketch.com/) to create podcast cover and episode promotional images.

Why not something like Canva or Figma? I live in India and 4 hour power-cuts are common. I need something that I can work even offline.

Also sketch has a great "pay once and use forever" pricing model.

## Post Promotion

![Promoting podcast episode on social media](https://cdn.olai.in/jjude/podcast-promotion.jpg)

How do I promote my podcasts?

As like my blog, only friends and family listen to them (more friends than my family). 

I share as whatsapp status, LinkedIn story, LinkedIn post, Twitter story, Tweet...


## Continue Reading

* [Gravitas WINS podcast](/podcast/)
* [Build An Ecosystem For Learning](/build-an-ecosystem-for-learning/)
* [Documenting Your Decisions](/documenting-your-decisions/)