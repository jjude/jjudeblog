---
title="Don’t throw away your opportunity"
slug="dont-throw-away-your-opportunity"
excerpt="Call it 'unfair', but life never presents an opportunity in a golden plate."
tags=["coach"]
type="post"
publish_at="09 Oct 08 16:30 IST"
featured_image="https://cdn.olai.in/jjude/dont-throw-away-your-opportunity-gen.jpg"
---

In an [earlier post](/so-do-we-throw-the-scriptures-away/), I mentioned about interpreting scriptures for practical application in our lives. In this post, I'm sharing with you a story in a Hindu epic that inspired me greatly. It's a pity that I forgot the names of the character and reference but the moral of the story inspired me all through my life.

In the ancient days, according to Hindu epics, it is a common practice for men to undertake penance to invoke blessing of gods. One such story goes like this:
  > _A king undertakes penance with a desire to taste God's food. Days go by; months go by; and years go by. After many many years of penance, the king is tired and thirsty. Then a lowly man walks by and enquires the tired king, if he needs something. When the king requests for water, the stranger pees in his 'thiruvodu' (earthen pot used by beggars for collecting food) and place it before the king. Annoyed by this, the king throws away the pot in anger. As the king throws away the pot, the lowly stranger transforms into God saying, &quot;You threw away what you've been longing for - It was God's food'._

![Failure or Success](https://cdn.olai.in/jjude/success-failure.png)

We all long for that one 'thing' in our lives - be it the job that will get us out of our&#160; debts or the gal of our dreams, or the article that will take us to the pinnacle of fame. Call it 'unfair', but life never gives that in a golden plate. It comes in as a 'pee in the earthen pot'.

In software industry, it always takes the form of 'the risky project that none wants to handle'. Taking it could spoil your reputation. But there lies &quot;God's food&quot;, that one thing that you've been waiting for.

Those who know me, know that I grab such projects with all enthusiasm. (Sometimes it had been just plain 'pee in the earthen pot'. When that happens, the emotional pain is too high. Still you learn something valuable about handling risks). But most often I end up as a winner.

Is this true only for professional life? No. I've found it to be true even in personal life.

Taking risks is a difficult decision. During the decision making phase, it drains you emotionally and during implementation it drains you physically. But there lies 'God's food'.

