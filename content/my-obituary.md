---
title="Writing my obituary"
slug="my-obituary"
excerpt="When we have an end in mind, we can live a potent life rather than a palatable one"
tags=["wins","self","coach"]
type="post"
publish_at="16 Dec 20 08:30 IST"
featured_image="https://cdn.olai.in/jjude/obituary.jpg"
---

A French newspaper published a scathing obituary of a merchant. He read it, changed his life, and left a lasting legacy. That merchant was Alfred Nobel.

The newspaper mistook his brother's death as his and [wrote](https://en.wikipedia.org/wiki/Alfred_Nobel#Nobel_Prize):

> The merchant of death is dead
> Dr. Alfred Nobel, who became rich by finding ways to kill more people faster than ever before, died yesterday.

He was stung by the "merchant of death" epithet. He set aside large part of his wealth to establish five annual prizes bearing his name. **He was fortunate to read his obituary**.

Steve Jobs introduced a series of revolutionary products after he was diagnosed with cancer. His products brought fortunes to Apple. But his thinking had a ripple effect far beyond the borders of Apple. **When he numbered his days, he gained a heart of wisdom**.

He said to the [Standford students](https://news.stanford.edu/2005/06/14/jobs-061505/): 

> Remembering that I'll be dead soon is the most important tool I've ever encountered to help me make the big choices in life.

![Potent life vs palatable life quote](https://cdn.olai.in/jjude/obituary.jpg "Potent life vs palatable life quote")

I think of my death often. I've been thinking about my death ever since I read Steven Covey's phenomenal book. In "[Seven habits](https://amzn.to/33X6mbq)," he encouraged his readers to **visualize their funeral**. He set the scene vividly: 

> In your mind's eye, see yourself going to the funeral parlor, parking the car, and getting out.  As you walk inside the building, you notice the flowers, the soft organ music.  You see faces of friends and family you pass along the way.  You feel the shared sorrow of losing, the joy of having known, that radiates from the hearts of the people there.

Here is your funeral. Your family, friends, and colleagues have come to honor you.

> Now think deeply.  What would you like each of these speakers to say about you and your life?  What kind of husband, wife, father, or mother would you like their words to reflect?  What kind of son or daughter or cousin?  What kind of friend?  What kind of working associate?  What character would you like them to have seen in you?  What contributions?  What achievements?

When I read these words for the first time as a twenty-seven-year-old, they felt creepy, eerie, and futile. I ignored the exercise. Even though I ignored it, the eulogy exercise clang to my unconscious mind for years after I finished the book.

A month before my marriage, the exercise seemed relevant. I sat in a quiet place and wrote what I would want my future wife to say as her obituary.

> We built our home together, not with gold, silver, and dollars but with laughter and love. He never compelled me to do anything which I didn't want but always encouraged me to explore worlds beyond my immediate comprehension. When I felt tired by such exploration, he stood as a pillar of strength and encouragement on which I can lean to rest and refresh. I can't say if he was a husband or friend or father or son because he was all that to me. On the day of our engagement, I asked permission for only one thing. He ensured I was able to do that until his last laughter. He always said, "if I lived a life worth honoring, don't cry that I died, but celebrate that I lived a life of worth." So, though my heart is heavy, eyes are full, and I am lonely for the first time in decades, I am going to celebrate the life we lived.

When my wife was admitted for the delivery of our first child, which happened to be a son, I augmented the "obituary" with his words. 

_She was in the hospital for 15 days due to pregnancy complications. So I wrote this with ample time in hand but mostly anxiety in heart_

> Today, I lost a cistern from which I drank wisdom, discipline, and courage. Appa led by example with two Bible verses. The first one was from the book of Proverbs. It says, "The horse is prepared for the battle, but the victory comes from the Lord." He prepared us for life. We read diverse books, kept ourselves fit, and pursued exciting hobbies. We ate with kids in slums as well as kids in bungalows. He taught us never to feel haughty in slums nor inferior in bungalows. He encouraged us to have God-sized dreams but trained us to accept situations as God-sent.
The other verse was from Saint Paul. Dad was a Paul-fanatic. Not only he devoured everything about Paul, but he also tried to live as much as possible like Paul. We had a daily study of Paul's letters. Paul's letters shaped our worldview. In his letters to Romans, Paul writes, "let not the patterns of the world entangled you. Rather transform yourself by the constant renewal of the mind." Daddy insisted that we never follow a line of thought or a path because that's the way of the masses. He trained us to think for ourselves and chose a path of our selection. When we found ourselves in predicaments, he would ask us if biases constrain us.
He led by example, much like Paul. If he wanted us to wake up at 6, he was up at 5; if he wanted us to read for 10 min, he read for an hour; if he wanted us to respect amma, he treated her like a queen, true to her name. From the time we were kids, he treated us as equals. He didn't do anything at home without discussing it with us. He made us feel as if we advised him. Whenever we faced a loss, he comforted us, saying not to weep because we lost but to rejoice because we had the privilege of experiencing it. Today, I am lost if I should weep or rejoice. I looked into the cistern that gave me answers. But it is dry. 

My professional obituary became my [operating principles](/principles/).

I read these words once a year to remind me of the destiny I chose to shape. The obituaries help me to live a potent life rather than a palatable one.

Now a question to you: **What do you want your dear ones to say at your funeral**?

## Continue Reading

* [A eulogy for my mother](/eulogy-for-mother/)
* [Numb is dumb](/numb-is-dumb/)
* [Build on the rock for the storms are surely coming](/build-on-rock/)