---
title="Ubellah Maria on 'Transitioning from developer to manager'"
slug="ubellah-maria-becoming-a-manager"
excerpt="Mindset shift needed to go from maker to multiplier and other important points for first-time managers"
tags=["coach","gwradio"]
type="post"
publish_at="01 Feb 22 06:00 IST"
featured_image="https://cdn.olai.in/jjude/ubellah-mgr-mindshift.jpg"
---

Every developer faces a choice at least once in their lifetime: Should I stay as a developer or become a manager? In this episode Ubellah shares the questions you should ask when considering the decision, the help others can provide, and finally books that can help in the switch. Please enjoy the conversation.

<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless src="https://share.transistor.fm/e/38c90aec"></iframe>

## How did you make the switch from developer to manager?

Six months into a project, my project manager said, I want to move to another project in this organization. So will you be okay to replace me in this role? 

At that point of time, I was not ready for that. I said can you give me some time I'll come back to you.

**I asked these two questions**: 

- will this role help me get excited every Monday morning or rather. 
- And is this going to help someone 

I asked myself these two questions and the answer was yes. 

Through my school and college, I was able to collaborate with people. I was able to work without authority. I was able to see people grow and then be happy about it. So I went back to my project manager and agreed to take the project manager role.


![Mindshift Needed To Become A Manager](https://cdn.olai.in/jjude/ubellah-mgr-mindshift.jpg "Mindshift Needed To Become A Manager")

## How was the initial days of becoming a manager? 

I am a staunch believer of **be, do and have**. Most of us fail because we go in the reverse.

Most of us think that leadership is a role. It is a title. It is an authority. It is a privilege. But **leadership is your character**. It is your mindset.

I had the opportunity to be a leader right from my school. So, I've always thought I am a leader. Right. So it is in being one and it became a little easy for me to do. 

But it doesn't work in the reverse. Most of us take onto the role and we think that we have to do something to prove ourselves as  a manager and then we will become one someday. As James Clear says in his atomic habits book, you have to have identity based habits that, so that you are that person. So it's going to be a little more easier.

Since I had the mindset of a leader and I've to only exhibit my character and my qualities, it became easier to be a manager.

## If I never thought I was a leader, what are the questions I should ask before taking up the role

When somebody asks you, can you become a manager, you become really excited because we think it is a promotion.

But if you are a rock-star developer and then if you have to go into a managerial role, you must understand that success to you is going to be very different. Your role is going to be different. Your responsibilities are going to be different.

**Ironically, whatever made you the rock-star developer that you are, is not going to take you a long way when you become a manager**. For example, as an individual contributor, you focused on your productivity, your knowledge, and your skill set. Success meant that doing your work well and getting appreciation from your boss and client.

When you become a manager, probably it is going to have a great shift. The first and foremost thing that you have to question yourself is, can I remove that beloved pronoun I from my mind?

> Success for you is not the code that you're going to ship; success for you is not the solution, the design or the architecture that you're going to propose. Success for you is taking that responsibility of enabling somebody else to grow.

That is the first mindset that you should develop.

Delegating things when you know so much about a topic is a difficult choice. The developer mindset is like can you just move? Let me show you how it is done. 

But **you have to become a multiplier**. 

The second mindset that you should have is that you should become a multiplier. If you think that you have all the expertise on any technology or if you have domain expertise, then you should step up as a coach, a guide, a mentor to your team.

You should have the mindset of a Sherpa? What does the Sherpa do? Sherpa doesn't carry you to the peak. Sherpa guides you; helps you walk, helps you reach the peak. He's not going to carry you along. Others don't even know who the Sherpa was. The world knows only the person who climbed the peak.

Likewise, you should have a mindset to coach, guide and mentor people to achieve success. 

You have to accept that you have to do less of a maker's work.

You are going to shine if your team members shine. 

No one in your team wants to, uh, do pair programming with their manager. That's the last thing they want to do. Right. So you embrace the fact that you are no longer a developer. Yes, you will have fears like, will I lose touch with my hands-on skills. But you have broader responsibilities then when you were a developer.

Third thing: You have to **listen to your team**. So far, you were giving opinions, but when you become a manager, you should empower your team to make those decisions. That is where your successes.

Most of us think that a manager has to be a know-it all. You have a team which you have to groom and grow those people to be the one that you are. If you are a rock star developer, make 10 rock-star developers. That is what your role is. 

In the story of management you play the role of a guide, a coach or a mentor. You are no more hero. Your team, your team member is going to be a hero.

You have to ask yourself whether you will be able to take up that responsibility for enabling others to succeed. Now you will have a team, so you have to shoulder the responsibility of enabling others to succeed. And taking that lightly is going to set yourself and all those people for failure.

## When I'm starting as a manager, what are the kind of help that I would need, or rather the ecosystem can provide?

This is something that every senior leaders should think about. You cannot become a manager just because the organizational roles says that you are a manager. It is a process. 

Any transformation, any change cannot be momentary. Even if a person is 60% or 70% or 80% ready, we promote that person. As soon as the person takes the role, he wants to improve project management skill, estimation skills and things like that. But even before that, they should have a shadowing period, at least for six months.

After the shadowing period, ask questions again to yourself. If you feel you're right, then take on the role. In India, we should give the space for developers to go back to technical role. We, in India, feel that going back to technical role is a demotion.

Nobody takes up the manager's role fully baked. Even after decades into this role, I'm still learning. Leaders should help the new managers to slip into the right mindset during the shadowing period.

Another thing that leaders should be doing is talking to the team. Many times when somebody grows as a manager from the same team there are a lot of conflicts. People have questions like why not me?

If you are a leader and you are planning to bring somebody to that role, you have to be transparent enough. There is nothing to hide there. You have to seek help from your team members. There should be an open conversation with the team and tell them that this is why you think that this person is the right person to get onto that role.

## What are the books that helped you to have that kind of a mindset and also to perform as a manager initially? 

I am not a voracious reader. I always believe learning from people and that's how I have learned. People help you better than books and courses. I've always been very observant of what my leader did. 

The moment you decide that you want to get into a leadership role you will have to come out of that corner that you were sitting with heads into your laptop and coding, not even seeing left or right. There is so much to observe and learn from your environment. 

Whatever is presented in a book is a personal experience of somebody. The author is going to say, I did it this way. It worked. It is survivorship bias. Many of the developers who become first-time managers they want to follow every word in a book. That is where their failure starts. You should understand that it is a personal experience of somebody. Not everything that the author explained is going to be how you are going to handle the situation. 

We are all unique and we all have our own style of leadership. So take things that will work for you.

Still some of the books I would recommend:

- [Making of a Manager](https://amzn.to/32JJ3nR) by Julie Zhuo
- [Radical Candor](https://amzn.to/3KXiKfh) by Kim Scott
- [The First 90 Days](https://amzn.to/3G8MKkM) by Watkins

I would always recommend you to have your eyes and yours open wide to see the world. You will have more experiences, more learnings from people than what is available in the books. 

## Are you writing a book?

I did not even know that I can write. It was my mentee, Ajay, who actually helped me understand that I can also write. From there was no turning back.

I have plans to compile a book from whatever I've been writing on LinkedIn. It should come out in 2022.

## Watch on YouTube

<iframe width="560" height="315" src="https://www.youtube.com/embed/O7d9-L7V8lY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Your feedback counts

Thank you for listening. If you enjoy the podcast, would you please leave a short review on Apple podcast or on YouTube? It takes less than 60 seconds, and it really makes a difference in finding this podcast. And it boosts my spirits. 😀

I would like to hear what resonated the most with you in this episode. Either you can email me or share it on social media and tag me. Thank you for your support.

Have a life of WINS.

_Gravitas WINS Radio is produced by [Joseph Jude](https://jjude.com/). Be sure to subscribe on [Apple Podcast](https://podcasts.apple.com/in/podcast/gravitas-wins-radio/id1562802449), [Spotify](https://open.spotify.com/show/7LOBjAyjBkechMC0xMIj9E),[Google Podcast](https://podcasts.google.com/feed/aHR0cHM6Ly9mZWVkcy50cmFuc2lzdG9yLmZtL2dyYXZpdGFzLXdpbnMtcmFkaW8=),[YouTube](https://www.youtube.com/gravitaswins), or the podcasting app of your choice._

Please check out other episodes of [Gravitas WINS Radio](/gwr-themes/). For easy consumption, they are categorized by themes. If you are a fellow podcaster, you might be interested in [How I Podcast](/how-i-podcast/) or [Tools I Use](/i-use/).

## Connect with Ubellah Maria:

- LinkedIn: https://www.linkedin.com/in/ubellah-maria-2a562214/

## Connect with me
- Twitter: https://twitter.com/jjude
- LinkedIn: https://www.linkedin.com/in/jjude/
- Website: https://jjude.com/
- Newsletter: https://jjude.com/subscribe/
- Youtube: https://youtube.com/gravitaswins
- Email: podcast@jjude.com
- Executive Coaching Program: https://gravitaswins.com

## Continue Reading

* [Three Types Of Goals You Should Set](/three-goals/)
* [Meenakshi on 'Carving a unique career path'](/meenakshi-career/)
* ['I'd far rather be lucky than smart' with Emmy Sobieski](/emmys/)