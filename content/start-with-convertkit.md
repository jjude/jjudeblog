---
title="Using ConvertKit For Email Marketing"
slug="start-with-convertkit"
excerpt="ConvertKit is a well designed email marketing software. I'm starting to use it for this blog."
tags=["tech","martech","blogging"]
type="post"
publish_at="11 Dec 16 23:31 IST"
featured_image="https://cdn.olai.in/jjude/convertkit.png"
---
There is abundance of social engagement channels—Twitter, Facebook, Slack, and so on. Yet, companies after companies, bloggers after bloggers are finding out that **email is the best marketing tool**.

![ConvertKit for email marketing](https://cdn.olai.in/jjude/convertkit.png)

### From The Beginning

I tried Zoho marketing campaign for a while for this blog. I even wrote a [guide](/zoho-campaign-101/). Zoho has great-set of features. But it's UI is still stuck in old ways.

After Zoho, I used Mailchimp. You can use it for free to send emails up to 2000 subscribers. I don't have any particular grievance with Mailchimp service. **Mailchimp is great if your need is limited to collecting email ids and sending them emails**. This was my need until recently.

### Looking Forward & Models

Going forward, I want to make this blog a hub of my [portfolio](https://www.prudentdevs.club/dev-portfolio). Part of that hub is teaching.

I researched on bloggers who have done something similar—transitioned from blogging to teaching (and moved onto even higher aspiration like entrepreneurship)

One of the product people I follow is [Justin](https://justinjackson.ca/). His ideas are simple but useful. One of his idea is to include the twitter handle in the `<title>` tag, so that whenever someone shared the post on twitter, you will come to know automatically. I have implemented it in this blog. Go ahead and [share this post](https://twitter.com/share?text=Using%20ConvertKit%20For%20Email%20Marketing%20by%20@jjude:) on twitter and you will see it in action.

He has a beautiful sign-up form for his newsletters powered by ConvertKit. I have tried in vain to design such a sign-up form for Mailchimp. ConvertKit is run by Nathan Barry.

I have followed [Nathan](http://nathanbarry.com/) for sometime now. I liked how he transitioned from a designer to an author to an entrepreneur. I have read his blog about his journey in launching and building ConvertKit.

**Inspired by Justin, I decided to move to ConvertKit** for email marketing.

### Conversion

Though I decided to use it, I thought I will go ahead after I created enough content. And I went about creating.

Then I got an email for a **webinar** about ConvertKit. I signed up for it. But I couldn't attend the live webinar, as it fell at midnight, Indian time. I knew they would record the webinar.

There was recording, but there was a little surprise attached to it. There was a free one month trial. That was not all. They gave away Nathan Barry's excellent [Authority Book](http://nathanbarry.com/authority/), and access to [Product to Profit masterclass](https://the-product-to-profit-masterclass-nathan-barry.teachery.co/) if I created an account and used it within 24 hours.

It was **too good a deal** to let it go. So I created an account.

### Experience so far

ConvertKit is designed so well. I signed up, created a sign-up form for my blog, added it to my blog, imported the subscribers from Mailchimp, and sent the first email, all within 30 minutes. That was one of the highly productive half hour.

The entire user-experience (UX) is pleasant and satisfying. As I went from one activity to another, **I was feeling smarter and smarter**. It increased my emotional connect with it. ConvertKit is definitely a candidate for a case-study for a well designed web application.

I told you I sent the first broadcast to all my subscribers within the first 30 minutes. If that wasn't enough, the **open rate for that broadcast was whopping 67.6%**. I never had such a high open rate for any of my campaigns.

I don't know if it was because of the time (I sent at Thursday 4 pm IST) or ConvertKit managed to deliver the email to the inbox and avoided the 'promotions' tab of gmail. In any case, the initial success cemented the emotional bond.

I'm yet to explore all other options—tagging, automation, and drip campaign. I'm sure, ConvertKit will make me feel even smarter as I go along.

### Suggested Improvements

ConvertKit is younger than other long running solutions in this domain. They have selected features essential for their niche audience (professional bloggers and authors). As I mentioned earlier, they have implemented these features in a well thought out manner.

There is always room for improvement. So here are some of the improvements I would like to see:

#### 1. A mobile app
I like see a report after I send out a campaign. Not only that, I like to keep my commitment to my subscribers. If I commit to sending them a tip every Friday, I like to keep it, no matter I'm at home and have access to my laptop or I'm traveling and all that I have is my mobile. Having a mobile app will ensure that I can keep that promise to my subscribers.

#### 2. Un-opened Report

ConvertKit has a feature to resend the campaign to subscribers who didn't open it the first time. It is a fantastic feature (which Zoho has but not Mailchimp). I would like to see who all didn't open so that I can decide to send.

### 3. Create a campaign via email

Mailchimp calls it [Email Beamer](https://mailchimp.com/features/email-beamer/). Why I need it? Same reason as the first one.

### 4. Subscribers' insight

ConvertKit has a page for each subscriber. It lists the campaigns sent to them and their actions (opened, clicked). It is great. But I want more.

[FullContact](https://www.fullcontact.com/) is a contact management software with APIs. ConvertKit can sync to FullContact to provide more insights about subscribers. [Betaout](http://demo.betaout.com/e-com/users/user_profile.html), an e-Commerce automation software does this so well.

![Betaout Customer Insight](https://cdn.olai.in/jjude/betaout-insight.png)

If it can list out their website, twitter id, and other details, I will know more about my subscribers. More I know my subscribers, more I can personalize the campaigns, thus making them more effective.

### 5. Campaigns in subscriber's timezone

More than half of my subscribers are in India, but some of them are in the US and some in Europe. I would like to send a campaign at a time suitable for them to improve the open rate. Can ConvertKit pickup the timezone of the subscriber? They can. Either via a service like FullContact or via an option in the app itself.

Though it is only a week since I signed up, I'm excited to start with ConvertKit.
