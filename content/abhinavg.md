---
title="Abhinav Goel on 'Product Manager In A VUCA World'"
slug="abhinavg"
excerpt="We talk about super-chicken experiment, thriving in a VUCA world, how to get into product management, and support systems we need in life to thrive in our career."
tags=["gwradio"]
type="post"
publish_at="14 Jul 21 07:09 IST"
featured_image="https://cdn.olai.in/jjude/abhinavg-gen.jpg"
---

<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless src="https://share.transistor.fm/e/f8eabfc9"></iframe>

_Click on the ▶️ button above to listen to the episode_

## Watch on YouTube

<iframe width="560" height="315" src="https://www.youtube.com/embed/6vDEDsuLZDM" title="Product Manager In A VUCA World" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Introduction

Section Overview: Abhinav introduces himself as a product manager and talks about his role in defining problems, researching solutions, and improving existing products. He also shares his journey into the field of product management.

### Abhinav's Role as a Product Manager

- [00:45](https://youtu.be/6vDEDsuLZDM?t=45) Abhinav defines his role as a product manager, which involves defining problems from customers, researching possible solutions, improving existing products for customers and beating competition.
- [01:05](https://youtu.be/6vDEDsuLZDM?t=65) He talks about how he plans tasks for the team and ensures quality delivery while minimizing issues in the future.

### Journey to Product Management

- [01:46](https://youtu.be/6vDEDsuLZDM?t=106) Abhinav shares that he comes from a business family but chose to diverge into a different professional field.
- [02:08](https://youtu.be/6vDEDsuLZDM?t=128) He talks about how he realized that coding was not his strong suit during his Bachelor of Technology studies but got lucky with an opportunity at a startup where he learned to define problems well and research solutions even better.
- [03:30](https://youtu.be/6vDEDsuLZDM?t=210) Abhinav mentions that being technical is an advantage in product management because it gives you a strong hand with your team.

## [0:05:03] Moving Up the Ranks

Section Overview: Joseph asks Abhinav about his journey from being a business analyst to becoming a product owner and then eventually moving up to become a product manager.

### Climbing Up the Ladder

- [05:26](https://youtu.be/6vDEDsuLZDM?t=326) Abhinav explains that these are all different roles that one can climb on their way to becoming a product manager or whatever they aspire to be.
- He mentions that each role has its own set of responsibilities and skills required.
## [06:09](https://youtu.be/6vDEDsuLZDM?t=369) Building Effective Processes for Hyper Productivity

Section Overview: In this section, Abhinav talks about the importance of building effective processes to achieve hyper productivity. He shares his experience of working in a startup with no processes and then moving on to a bigger organization where he had to document everything.

### Importance of Effective Processes

- [06:09](https://youtu.be/6vDEDsuLZDM?t=369) Building effective processes is crucial for achieving hyper productivity.
- In a startup, there were no processes, and whatever they were doing was considered the process.
- In a bigger organization, he had to document everything as part of level three processes.

### Agile Transformation

- [06:28](https://youtu.be/6vDEDsuLZDM?t=388) The speaker got involved in an agile transformation when agile was gaining popularity in India.
- He learned about roles such as Scrum Master, Product Owner, and Scrum Team.
- The Product Owner is similar to a Business Analyst but acts as the proxy customer for the team.

## [08:11](https://youtu.be/6vDEDsuLZDM?t=491) Building Hyper Performance Teams in VUCA World

Section Overview: In this section, Abhinav talks about building hyper performance teams in a VUCA world. He defines VUCA as volatile, uncertain, complex, and ambiguous.

### Understanding VUCA World

- [08:11](https://youtu.be/6vDEDsuLZDM?t=491) VUCA stands for volatile, uncertain, complex, and ambiguous.
- We have been living in a VUCA world since the start of workplaces.
- COVID has made things more volatile than ever before.

### Challenges Faced

- [08:34](https://youtu.be/6vDEDsuLZDM?t=514) The volatility and uncertainty make it tough to work with product teams remotely.
- Complexity arises when deciding which products to work on or whether to focus on new features or current customers first.
- Ambiguity arises due to unclear situations that are difficult to interpret.

### Three Key Factors for Success

- [10:27](https://youtu.be/6vDEDsuLZDM?t=627) Communication, transparency, and trust are the three key factors for building hyper performance teams.
- If these factors are present in a team, they will perform well even in a VUCA world.
## [11:45](https://youtu.be/6vDEDsuLZDM?t=705) High Performance Teams

Section Overview: In this section, Abhinav talks about how to create high-performance teams by including technical team members in the project plan and making the product roadmap transparent.

### Importance of Inclusion

- [12:02](https://youtu.be/6vDEDsuLZDM?t=722) Team members should be included in the whole project plan and product plan.
- [12:22](https://youtu.be/6vDEDsuLZDM?t=742) Developers can think smartly about fixing problems and saving time, so their views should be considered.
- [12:39](https://youtu.be/6vDEDsuLZDM?t=759) Transparency helps to get insights from team members that can make the product better.

### Cohesive Unit

- [13:17](https://youtu.be/6vDEDsuLZDM?t=797) The team needs to work as a cohesive unit with a shared vision.
- [13:34](https://youtu.be/6vDEDsuLZDM?t=814) Working in silos leads to misunderstandings and not delivering what customers want.

## [14:46](https://youtu.be/6vDEDsuLZDM?t=886) Hyper-performing Individuals vs. High Performing Teams

Section Overview: In this section, Abhinav discusses whether hyper-performing individuals can form high performing teams.

### Super Chicken Model

- [15:11](https://youtu.be/6vDEDsuLZDM?t=911) Resource allocation based on individual performance does not always work.
- [15:30](https://youtu.be/6vDEDsuLZDM?t=930) The "Super Chicken Model" experiment showed that hyper-performing individuals may compete with each other instead of working together.
- [15:50](https://youtu.be/6vDEDsuLZDM?t=950) Average chickens performed better as a group because they learned from their mistakes and worked together.
## [17:35](https://youtu.be/6vDEDsuLZDM?t=1055) Building a High-Performance Team

Section Overview: In this section, Abhinavs discuss the importance of having a balanced team and cohesiveness in building high-performance teams. They also talk about how COVID-19 has affected remote working and its impact on hyper-performance.

### Importance of Cohesive Teams

- [17:55](https://youtu.be/6vDEDsuLZDM?t=1075) A balanced team is preferred to have cohesiveness and cancel out negativities from each other.
- [18:18](https://youtu.be/6vDEDsuLZDM?t=1098) The cohesiveness between the team matters more than individual capabilities.
- [18:38](https://youtu.be/6vDEDsuLZDM?t=1118) A cohesive team can create something larger than individual capabilities.

### Impact of COVID-19 on Remote Working

- [19:03](https://youtu.be/6vDEDsuLZDM?t=1143) People have learned to work from home due to COVID-19.
- [19:28](https://youtu.be/6vDEDsuLZDM?t=1168) Work from home brings challenges to communication, transparency, and trust.
- [20:02](https://youtu.be/6vDEDsuLZDM?t=1202) Companies and employees are brought to the same platform to understand that it's all about performance rather than being in or out of the office.
- [20:38](https://youtu.be/6vDEDsuLZDM?t=1238) It may take up to two to three years more for us to get out of COVID mindset.

### Habits for High Performance

- [21:41](https://youtu.be/6vDEDsuLZDM?t=1301) Open channels of communication build transparency among everybody in the team.
- [22:02](https://youtu.be/6vDEDsuLZDM?t=1322) Setting goals helps maintain a solution-focused approach.
## [23:40](https://youtu.be/6vDEDsuLZDM?t=1420) Importance of Being a Mentor and Earning the Right to be One

Section Overview: In this section, Abhinav talks about how being a mentor to your children is not an automatic right that comes with being a parent. Instead, it is something that needs to be earned through setting a good example and being a role model.

### Earning the Right to Be a Mentor

- As parents, we cannot assume that we have the right to demand things from our children just because we are their parents.
- We need to earn the right to be mentors by conducting ourselves in a way that sets a good example for our children.
- When one teaches, two learn - as mentors, we can also learn new things from our children.

## The Power of Processes in Business

Section Overview: In this section, Abhinav discusses how having effective processes in place is crucial for success in business. He shares his experience reading "The Phoenix Project" and how it taught him about the importance of having processes.

### The Importance of Processes

- "The Phoenix Project" taught Abhinav about why having processes is important in business.
- Having competent people on your team is not enough if there are no processes in place to manage their work effectively.
- A person who comes from a manufacturing background can bring valuable insights into establishing effective processes in an IT organization.
- Establishing effective processes leads to increased productivity and better delivery.

## Overcoming Procrastination and Taking Action

Section Overview: In this section, Abhinav talks about his struggles with procrastination and how he overcame them through taking action. He shares his experience with Gravitas Bench course and how it helped him overcome procrastination.

### Overcoming Procrastination

- The speaker struggled with procrastination, even when it came to things he was confident about.
- Gravitas Bench course helped Abhinav overcome his procrastination by teaching him how to take action and not just rely on confidence.
- Taking action is crucial for success, even if it means starting small and building momentum over time.
## [30:00](https://youtu.be/6vDEDsuLZDM?t=1800) The Flywheel Effect and Accountability

Section Overview: In this section, Abhinav discusses how he overcame procrastination by implementing the flywheel effect and accountability. He explains how he consumes, produces, and engages with information to build an insight platform for himself.

### The Flywheel Effect

- [30:21](https://youtu.be/6vDEDsuLZDM?t=1821) The speaker realizes that he needs to build a flywheel that will churn value for him both socially and monetarily in his career.
- [30:32](https://youtu.be/6vDEDsuLZDM?t=1832) He consumes from books, blogs, and other sources of information.
- [30:42](https://youtu.be/6vDEDsuLZDM?t=1842) He writes about what he learns and shares it on LinkedIn or his blog.
- [30:57](https://youtu.be/6vDEDsuLZDM?t=1857) He engages in conversations with others to learn more.

### Accountability

- [31:17](https://youtu.be/6vDEDsuLZDM?t=1877) The speaker emphasizes the importance of accountability in achieving his goals.
- [31:36](https://youtu.be/6vDEDsuLZDM?t=1896) Having an accountability team has become routine for him.
- [31:56](https://youtu.be/6vDEDsuLZDM?t=1916) Being part of a support group provides social push to stay on track.

### Micro Improvements

- [32:12](https://youtu.be/6vDEDsuLZDM?t=1932) The speaker sets short goals such as 15 minutes of reading or writing per day.
- [32:31](https://youtu.be/6vDEDsuLZDM?t=1951) These micro-improvements are easy to achieve and difficult to fail at.
- [32:53](https://youtu.be/6vDEDsuLZDM?t=1973) Gravitas WINS has given him direction in working towards these goals.

## Building a Hyper-performant Community

Section Overview: In this section, Abhinav talks about building a hyper-performant community where individuals hold each other accountable and push each other forward. He also shares two instances where people have been kind to him.

### Building a Hyper-performant Community

- [33:17](https://youtu.be/6vDEDsuLZDM?t=1997) The speaker's aim is to create a community where individuals are hyper-performant and hold each other accountable.
- [33:37](https://youtu.be/6vDEDsuLZDM?t=2017) Individuals can share information from their own practices to help others move forward.

### Kindness of Others

- [33:55](https://youtu.be/6vDEDsuLZDM?t=2035) The speaker's father-in-law and family have been supportive of him in his personal and professional life.
- [34:18](https://youtu.be/6vDEDsuLZDM?t=2058) Mr. Joseph Jude, the person in the next window of the video screen, has had small conversations with Abhinav that have helped him learn new things.

## The Importance of Personal Growth

Section Overview: In this section, Abhinav talks about the importance of personal growth and how it can help individuals become a valuable product in their career.

### Personal Growth and Building Value

- [35:56](https://youtu.be/6vDEDsuLZDM?t=2156) Personal growth is important for individuals who want to grow in their careers.
- [36:16](https://youtu.be/6vDEDsuLZDM?t=2176) Individuals should treat themselves as a product and build value around it.
- [36:48](https://youtu.be/6vDEDsuLZDM?t=2208) Abhinav credits the person he's talking to for playing a big role in his organic growth by pushing him to think about himself as a product and not just a resource for some team.

## Defining A Good Life

Section Overview: In this section, Abhinav defines what he considers to be a good life.

### Definition of A Good Life

- [37:30](https://youtu.be/6vDEDsuLZDM?t=2250) For Abhinav, a good life is where he can sleep well at night knowing that he did some good work during the day.
- [37:49](https://youtu.be/6vDEDsuLZDM?t=2269) Helping someone do something good or positively impacting somebody's life is what makes his day fulfilling.

## Connecting with Abhinav Online

Section Overview: In this section, Abhinav shares how people can connect with him online if they want to learn more about product ownership.

### How to Connect with Abhinav Online

- [38:28](https://youtu.be/6vDEDsuLZDM?t=2308) The best way to connect with Abhinav is through LinkedIn or Twitter.
- [38:45](https://youtu.be/6vDEDsuLZDM?t=2325) People can also follow his blog on abhinavji.com or send him an email.

_[Generated with Video Highlight](https://videohighlight.com/video/summary/6vDEDsuLZDM)_

## Connect with me
- Twitter: https://twitter.com/jjude
- LinkedIn: https://www.linkedin.com/in/jjude/
- Website: https://jjude.com/
- Newsletter: https://jjude.com/subscribe/
- Youtube: https://youtube.com/gravitaswins
- Email: podcast@jjude.com
- Executive Coaching Program: https://gravitaswins.com

## Your feedback counts

Thank you for listening. If you enjoy the podcast, would you please leave a short review on Apple podcast or on YouTube? It takes less than 60 seconds, and it really makes a difference in finding this podcast. And it boosts my spirits. 😀

I would like to hear what resonated the most with you in this episode. Either you can email me or share it on social media and tag me. Thank you for your support.

Have a life of WINS.

_Gravitas WINS Radio is produced by [Joseph Jude](https://jjude.com/). Be sure to subscribe on [Apple Podcast](https://podcasts.apple.com/in/podcast/gravitas-wins-radio/id1562802449), [Spotify](https://open.spotify.com/show/7LOBjAyjBkechMC0xMIj9E),[Google Podcast](https://podcasts.google.com/feed/aHR0cHM6Ly9mZWVkcy50cmFuc2lzdG9yLmZtL2dyYXZpdGFzLXdpbnMtcmFkaW8=),[YouTube](https://www.youtube.com/gravitaswins), or the podcasting app of your choice._

Please check out other episodes of [Gravitas WINS Radio](/gwr-themes/). For easy consumption, they are categorized by themes. If you are a fellow podcaster, you might be interested in [How I Podcast](/how-i-podcast/) or [Tools I Use](/i-use/).
