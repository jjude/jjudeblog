---
title="People Aren't Potatoes"
slug="no-potatoe"
excerpt="Treating people as replaceable commodities is an antiquated idea. It is time to bury the idea."
tags=["coach","startup"]
type="post"
publish_at="22 Feb 14 16:30 IST"
featured_image="https://cdn.olai.in/jjude/potatoes.jpg"
---
There is a heinous practice perpetuated by HR in most of the companies. That is treating people as replaceable commodities.

The on going belief is that any company should be process oriented and not people oriented. Nonsense.

![People aren't potatoes](https://cdn.olai.in/jjude/potatoes.jpg "People aren't potatoes")

**Processes by themselves are lifeless; it is people who impute life to these processes.** A committed, skilled, and intelligent person will work with a process (sometimes within and sometimes without) to achieve phenomenal success. An idiot will get killed by the same process.

Processes don't supersede people. Processes aid people not to overlook key factors in decision-making. Processes are the pathway to success. But processes themselves can't and won't carry you to success.

If people are replaceable commodities and process can take care of all gaps, then John Scully should've taken Apple to 70B$ in cash reserve, not Steve Jobs.

But what is the root of this thinking? Is there a validity in this idea?

From what I read, I have understood that it is an industrial-era thinking. For repetitive tasks, as in most of industrial age tasks, a Tom is as good or bad as a Henry. In the current era, this line of thinking is valid only for flipping burgers in McDonalds.

But the linear-thinking-suits in HR couldn't upgrade their thinking and [carried this antiquated idea][1] into knowledge enterprises. Nobody questions them and this practice continues.

Am I saying people are indispensable? Oh, no. Steve Jobs is gone and Apple is still standing tall. **While the 'people-are-replacable' thinking says anyone can do a task; 'I-am-indispensable' thinking says only I can do this task.** Both are wrong. Let us not correct one wrong with another wrong.

'People are replaceable commodities' is an industrial era concept carried over to knowledge economy. Its time to bury this idea. Let us respect people for their skills and compensate them for their contribution.


[1]: /beware-of-cloning-best-practices/

