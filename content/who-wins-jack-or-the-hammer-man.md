---
title="Who Wins - Jack or The Hammer Man?"
slug="who-wins-jack-or-the-hammer-man"
excerpt="Widen your knowledge rather than develop a narrow vision."
tags=["insights"]
type="post"
publish_at="14 Jan 14 16:30 IST"
featured_image="https://cdn.olai.in/jjude/who-wins-jack-or-the-hammer-man-gen.jpg"
---


Whenever I learn a new topic I dive deep into it. I learn as much as possible about the topic.

I followed this when I learned computer graphics at college, medical informatics at my first job and Visual Basic at my first overseas assignment. I follow this even in my personal life. From parenting to travel, I read wide and deep about the topic.

On each of these topics, I aim to be a master. But life changes, with it roles change. I embrace a new passion and fix my eye to be a master of another topic. The cycle goes on.

While some of my friends have built decades of experience in SAP or in Java, I have moved around picking up gems in data structures, application development, people management, and consulting. I have become a [Jack](https://en.wikipedia.org/wiki/Jack_of_all_trades,_master_of_none) instead of a master.

Do I regret becoming a Jack? No. On the contrary, I find it extremely valuable.

Why do I find it valuable to be a Jack?

- I can **thrive** in many environments, because of the broad knowledge. I can work on green-field as well as maintenance projects; a government client is as easy to handle as a corporate client; process design is as satisfying as executing it. You might say, it is because of the confidence. That confidence is because of the broad knowledge.

- I don't have affinity to [CMMi](https://en.wikipedia.org/wiki/Capability_Maturity_Model_Integration) or [Prince2](https://en.wikipedia.org/wiki/PRINCE2) or [TOGAF](https://en.wikipedia.org/wiki/The_Open_Group_Architecture_Framework) or any other acronyms. So I am able to **avoid Procrustean beds**. I choose whatever technique, tool, methodology that gets the results. Every project is different and every person in a team is different. They need to be treated differently. Success can't be achieved by confirming to someone's standards.

- For any project to be successful, a manager needs to **integrate ideas** from different domains - ideas from psychology to technology, to win in a competitive environment. A hammer-man[^1] fails miserably in such a competitive environment.

Success comes only to those who build knowledge on different domains and [integrate](/no-polarization/) that knowledge for a contextual solution.

If I were to give advice to graduates starting their job, it would be to widen their knowledge rather than develop a narrow vision.

