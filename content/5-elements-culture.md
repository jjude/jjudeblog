---
title="5 elements of magnetic company culture"
slug="5-elements-culture"
excerpt="Everyone talks about building a great company culture; but what are the ingredients of such a culture?"
tags=["biz","coach","visuals"]
type="post"
publish_at="23 Mar 22 06:00 IST"
featured_image="https://cdn.olai.in/jjude/magentic-culture.jpg"
---

![5 elements of magnetic company culture](https://cdn.olai.in/jjude/magentic-culture.jpg "5 elements of magnetic company culture")

Culture is how employees act when no one is looking; it is how decisions are made when the CEO is not present; it is what holds everyone together through all phases. 

Culture is hard to build. It can't be copied; it can't be built with pep talks; it can't be drafted in a weekend culture retreat.

While the academic world is busy researching how to build culture, we can look to institutions that have had a strong culture for many centuries. Can you guess which institutions they are?

Religions teach us much about building culture. Consider two of the major religions - Christianity and Hinduism. While they are fundamentally different, they have a lot in common when it comes to building a long-lasting culture.

Borrowing from these two religions, there are five elements to building a magnetic culture. They are:
- Grand idea
- Rituals
- Stories
- Celebration
- Evangelists

Let us look at each of them.

### Grand Idea

A **grand idea** draws people, and people, in turn, inject life into it. A grand idea is not like a vision statement. A vision statement of a company can be substituted with another company's vision statement and employees won't notice the difference. A grand idea **distinguishes the company**. 

A grand idea is not a contract but **a covenant**. A contract comes in handy when you know exactly where you are going. The covenant idea implies that when we band together, we can create a better future for each of us.

### Rituals

Without **rituals**, people will forget the idea, however grand it is. It could be a communal ritual, such as a mass, or an individual one, such as a prayer; it could be tied to a place, such as dipping in Ganga, or time, such as the phase of the moon. Or it could be a combination of many. Rituals build the company around the idea and keep the idea alive in people's minds.

What could be some of the company rituals?
- daily stand-ups
- weekly reviews
- quarterly town halls
- annual retreats

### Stories

Numbers and analytics have their place in building a successful company. But **stories** are needed to create cultures that transcend tough times. Corporate stories could revolve around **three ideas**:
- personal success stories, such as a designer cracking a difficult customer experience workflow to land a prestigious client
- tales of achieving challenging milestones like signing up the first Fortune 100 client
- collective acts like how a group helped a colleague arrange blood for a relative

Stories will not spread by themselves.  That's where rituals play a role. Stories should be embedded in the rituals. 

### Celebrations

Though stories celebrate, there must be a **celebration** in the form of an event. A party. A gala event. It could be: 
- annual events like founder's day & sales retreats; 
- a celebration of a milestone

The greater the frequency of celebrations, the greater the bonding. Make the most of every occasion to spread stories.

### Evangelists

Though the founder's personality is reflected in the culture, the founder can't shoulder the responsibility of building the culture alone. He or she needs both hearts and hands, better yet if hands come with the commitment of the heart. **Evangelists** need to be nurtured. When they are nurtured with dignity, they in turn will go out and nurture the vision, stories, and heroes. They birth further evangelists. Only through them, culture spreads to every corner of the company, and often outside too.

Though I have listed them individually they all reinforce each other. As an example, an annual celebration of founder's day could be a ritual, where stories are retold. 

When a company has a delightful culture, the employees are motivated to do the best work of their lives. Clients are likely to do business with a company if its employees find meaning in their work. A work culture like this reduces operational expenses and thus increases profits. As long as that profit is reinvested in employee benefits, the company will continue to grow in size, profit, and customer base.
## Continue Reading

* [Three types of mistakes leaders keep making](/three-types-of-mistakes/)
* [Great Leaders Are Invisible](/great-leader/)
* [Culture is what you do, not what you believe](/culture-is-doing/)