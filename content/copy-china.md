---
title="What can we learn from China?"
slug="copy-china"
excerpt="Imitate masters. Discover Yourself. Innovate to become a master."
tags=["wins","coach","branding","gwshow"]
type="post"
publish_at="06 Apr 21 20:58 IST"
featured_image="https://cdn.olai.in/jjude/china.jpg"
---


In the early 90s, China was derided as a [copycat country](https://en.wikipedia.org/wiki/Copy_to_China). China produced a close imitation of every fancy product on the earth. 

- Gucci bag
- Rolex watches
- Viagra medicine

Everything was copied and manufactured in bulk.

While we were looking at the imitation game, the Chinese silently built enormous infrastructure to become the manufacturing hub of the world. They were no more despised. Executive of every industrial house queued up in China, to get their products manufactured in mass and cheap.

As a result, wealth flowed into China.

They invested all that wealth in R & D. Fast forward to today. China [files more patents](https://www.wipo.int/edocs/pubdocs/en/wipo_pub_941_2017.pdf) than the US or Germany. 

Now the US companies are copying products and business models from China. Don't forget that China has more data now than any other country on the planet. They will lead the innovation cycles for decades to come.

There are lessons we can learn from how China evolved from an imitation factory to an innovation leader.

![Copy as China](https://cdn.olai.in/jjude/china.jpg)

### Copy, copy, copy

Are you a developer? Study open source applications. Understand the architecture. Then write the same application in your way. Don't copy and paste, but code the application in your style.

Are you a content marketer? Read the popular posts from the top authors. Not just once. Read them multiple times. Learn the rhythm in their sentences. Then write the same article in your words.

Do you aspire to become a public speaker? Binge-listen to the top twenty-five speeches. Pay attention to the choice of words, rhythmic delivery, pauses between phrases. Replicate the same speech, in the same manner, standing in front of the mirror. 

### Build your production muscles

When you mimic great speeches, rewrite great copy, and reverse engineer applications you'll know what works. Use that knowledge to produce something of your own.

Go on a production spree. 

- 30 short articles in 30 days
- 30 applications in 30 days
- 30 speeches in 30 days

At this stage, speed matters. [Focus on quantity, not quality](/quantity-vs-quality).

When you bind yourself into a box of constraints, you'll gain production velocity, because you will leave behind every embellishment.

### Start innovating

When you mimic you gain speed. When you can produce in volume, it is time to pause and think of how you can differentiate yourself. Break your field of expertise into parts and experiment with bundling and unbundling to carve a unique identity.

There is no better example than [Pomplamoose](https://www.youtube.com/watch?v=2CCNswShJRc&list=PLiJey_76CkNRBzI2b-D5Jt_zaqPBu5cQC), whose music I'm playing on loop as I write this article.

They copy popular songs and mash them up with humor. Each song is unbelievably enticing. 

### What will you copy?

China is not alone in imitating to innovate. The same principle applies in every domain. Great writers, actors, and companies copy all the time. So don't feel guilty for copying. Start to think about what you'll copy as a first step to become a master.

## Continue Reading

* [Quantity trumps quality](/quantity-vs-quality)
* [Beware of Cloning Best Practices](/beware-of-cloning-best-practices)
* [Building In Public, Jeff Bezos Style](/build-in-public-bezos)