---
title="Principles trump processes"
slug="principles-trumps"
excerpt="If you want to last longer in a field with respect, focus on fundamental principles governing it"
tags=["wins","insights","coach"]
type="post"
publish_at="26 Aug 20 10:30 IST"
featured_image="https://cdn.olai.in/jjude/principles-trumps.png"
---

Photography is a hobby for me. As a novice photographer, I believed the camera makes the photo. Whenever I met a fellow hobby photographer, I would discuss things related to photography, but mostly would gravitate to cameras. That was until I stumbled on [Sephi](https://www.sephibergerson.com/). He shoots [exquisite photos on an iPhone](https://www.youtube.com/watch?v=gQByQCKPENw), debunking the myth that the "camera makes the photo."

Camera, lenses, bags, and filters are the visible parts of photography, and they are easy to debate, discuss, and dissect. Learning the fundamentals of lighting, composition, and creativity is the hardest, so many photographers are not interested in them.

If you think you are unaffected by the pretentious veneer in your industry, you may be unaware of your biases, like I was.

* Do you focus on fonts and colors on your website instead of content on the pages?
* Are you rolling out Jira (or Basecamp) without first training your team on basics of project management?
* Is your dress-shelf full of turtlenecks because Steve Jobs wore them?
* As a startup founder, are you focused on tuning your logo instead of finding product-market fit?
* Are you developing your application in Microservices architecture because Dropbox is moving to that architecture?
* Did you buy Apple stock because Warren Buffet bought it?

If you are guilty of any of the above or the likes, you **copied the visible at the cost of the principles**.

![Principles trumps processes](https://cdn.olai.in/jjude/principles-trumps.png)

Allow me to entertain you with a comedy scene from a Tamil movie that brings out this point even better.

> There is a pair of comedians in Tamil movies, who are like Laurel and Hardy. They together have produced many hilarious scenes, some of which evoke laughter even today. The below scene appears in one such comical scene.

> In this movie, Goundamani is head of a village, and Senthil is his assistant. Goundamani is smart, knows what to say in any situation, and the villagers respect him. Goundamani is haughty and never misses a chance to deride Senthil. Senthil envies him and wants to prove he is smart too. He accompanies Goundamani everywhere and observes what he says in every situation.

> In one such situation, an older woman passes away. Goundamani is invited, and Senthil tags along. "She wasn't just a mother to you," he comforts the family that lost their mother, "she was a mother to all of us." He goes on with his eulogy, but Senthil notes down this point.

> At the climactic scene, another lady, a wife this time, dies. Goundamani is sick and can't leave the bed. Senthil recollects the earlier scene and offers to go to the funeral.

> You guessed what he would have said, didn't you? "She wasn't just a wife to you," he cuts & pastes the words, "she was a wife to all of us."

The latest trends, tools, and practices are visible. They are easy to copy. They are the seed for fun talks in the cocktail parties. But when the context changes, you are handicapped. 

Principles are the opposite. Nobody enjoys squeezing their brain, challenging it's cemented beliefs. Yet **principles determine whether you thrive or fade away**.

David Ogilvy, the father of direct marketing, shared this [anecdote](https://christophercopywriter.wordpress.com/2012/05/17/a-letter-from-david-ogilvy/) in his _Confessions_.

> A man walked into Ogilvy's London agency wanting to advertise the opening of his hotel. Since he had just $500, he was turned to the novice, David Ogilvy. Ogilvy bought $500 worth of postcards and sent invitations to everybody he found in the local telephone directory. The hotel opened with a full house. 

As David Ogilvy proves, if you understand the principles well, you could use any tool to achieve the results. If you know the principles well, you would be further than those who use the tool without an understanding of the underlying tenets.

What is on the surface, like tools and processes, churn faster. If you want to last longer in a field with respect, focus on fundamental principles governing it.

## Continue Reading

* [Build on the rock for the storms are surely coming](/build-on-rock/)
* [If you see everything, you'll get nothing 🎯](/focus-to-win/)
* [Three types of books as per Elon Musk](/book-types)