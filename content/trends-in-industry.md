---
title="How to find trends in your industry"
slug="trends-in-industry"
excerpt="Internet is an open gold mine if you know how to use it."
tags=["coach","startup","wins","insights"]
type= "post"
publish_at= "24 Jun 20 10:30 IST"
featured_image="https://cdn.olai.in/jjude/sonata-platform.jpg"
---

As a Chief Technology Officer of an IT services company, I tend to look for trends in the IT industry often. Identifying trends help me to build useful and profitable skills. In this article, I explain how I go about identifying trends.

### Surveys

Every industry has its surveys. Software developers have [Stackoverflow surveys](https://insights.stackoverflow.com/survey/). Stackoverflow has become an integral part of developers, and they have been conducting developer surveys for nearly a decade. This year's report is based on a [study](https://insights.stackoverflow.com/survey/2020#methodology) of 65,000 software developers from 186 countries around the world. It is a large and diverse sample. So it is reliable.

There are many interesting facts in this [year's survey](https://insights.stackoverflow.com/survey/2020). I am listing just two here.

![Developer's Salary - 2020](https://cdn.olai.in/jjude/dev-salary-2020.png)

The first insight comes from [Salary and Experience by Developer Type](https://insights.stackoverflow.com/survey/2020#work-salary-and-experience-by-developer-type) report. For reasonably similar years of experience, **full-stack developers earn more than backend and front-end developers**. But, if you want to make more than most others, you should **become a data engineer**.

![Language Salary - 2020](https://cdn.olai.in/jjude/language-salary-2020.png)

The second insight comes from [Salary and Experience by Language](https://insights.stackoverflow.com/survey/2020#work-salary-and-experience-by-language) report. **Python developers earn more** than the median salary. Is it because Python is the second-best language for developing cloud applications, or is it the primary language of data engineers? Maybe both. This report also indicates that **as you gain experience, you should learn a compiled language** like Go or Rust.

### Industry Trend Reports

[Mary Meeker](https://en.wikipedia.org/wiki/Mary_Meeker) is a partner in a venture capital firm, Bond Capital. She has been releasing **annual report on internet trends** since 2001. Each report is packed with insights for both technologists and business executives.

I study her report with a lens of a technologist in a small IT business. I published a detailed analysis of [2017 report](/2017-trends/) with this summary:

> Smartphones and other edge devices are becoming heavy computing machines. Combine this computing power with APIs and Functions as services (aka serverless model), and businesses will build powerful applications on the edge devices.

I also designed **a sample business architecture** that small IT services companies can embrace to win.

![Architecture for small IT companies](https://cdn.olai.in/jjude/2017-trends-architecture.png)

I used a similar architecture to drive changes in our company.

### Annual reports and Investor Presentations of companies

Surveys and analyst's reports are second-hand information. You **get first-hand information from annual reports** published by companies listed in stock exchanges. I routinely read annual reports and investor presentations by Indian IT companies to understand the trends that have become profitable businesses.

IT services companies suffer from a significant problem. **Increase in revenue is tightly coupled with the number of employees**. Companies try to overcome this challenge by reusing code and asking employees to work on multiple projects. Both practices help only a little. In a growing company, every project is different. Hence developers prefer to code from scratch rather than modifying "template code." When employees work on multiple projects, employees stress out quickly, increasing the risk for the company.

[NIIT Technologies](https://www.niit-tech.com/) identified the challenge of tight coupling of revenue with headcounts in an [investor presentation](https://www.niit-tech.com/sites/default/files/NIIT%20Tech%20Investor%20Meet%2009.pdf). Their solution was to move stage by stage **from "Industry Focus" to "Managed Services" to "Platform-based solutions," and then to "SAAS products"**.

![NIIT Trend](https://cdn.olai.in/jjude/niit-trend.jpg)

Almost all Indian IT services companies followed a similar approach. Except for Infosys, which developed  [Finacle](https://www.edgeverve.com/finacle/), no other IT services company has been successful in developing a successful SAAS product. But **most of them are successful in creating a "platform."**

If many large IT companies are developing platforms, then the question is, "**what is a platform**." Ben Thompson defines platforms in his article titled, [The Bill Gates Line](https://stratechery.com/2018/the-bill-gates-line/):

> A platform is when the economic value of everybody that uses it, exceeds the value of the company that creates it. 

Though Ben Thompson writes concerning Shopify and the likes, the definition fits what Indian IT companies do.

As the NIIT Technologies team indicated, a platform helps the owning services company to decouple revenue from the number of employees, which means after the initial development cost **more revenue can be extracted from the platform just by operating it efficiently**. Customers of the platform also reap benefits by going to market faster and cheaper.

Once we understand the benefits of developing platforms, we can try to understand the ingredients of a platform. Again an investor presentation comes to the rescue. [Sonata Software](https://www.sonata-software.com/) calls itself a "Platformation company," since they build platforms for their customers. In their [presentation to investors in March 2020](https://www.sonata-software.com/sites/default/files/financial-reports/2020-06/sonata-investor-deck-latest_0.pdf), they talk about the **characteristics of a successful platform**. They are open, scalable, connected, and intelligent. 

![Sonata Platform](https://cdn.olai.in/jjude/sonata-platform.jpg)

Are there sample architectures available of platforms? Yes. [TCS](https://www.tcs.com/), the second-largest Indian company, published the architecture of their [TCS Connected Intelligence Platform](https://dss.tcs.com/wp-content/uploads/2019/10/tcs-connected-intelligence-platform-solution-brief-31-10-2019.pdf). What is the need of the CIP? TCS [says](https://dss.tcs.com/connected-intelligence-platform/):

> As the volume, variety, and velocity of data continue to increase, businesses must find new and better ways to harness the insights hiding in their data to **deliver more value to their customers** and respond effectively to continuous disruptions to their industries.

![TCS CIP](https://cdn.olai.in/jjude/tcs-cip.jpg)

If you study the architecture, you will understand that it is connected and intelligent (they are in the name of the platform itself ;-) ), and hopefully scalable as defined in the Sonata's slide deck.

_Disclaimer: I own stocks in Sonata, but not in other companies mentioned here._

### Latest Trends Report

Surveys, Industry Reports, and Annual Reports are lagging indicators of trends. They inform what worked. If you are open to taking risks to reap outsized rewards, you need to look at trends as it is evolving.

![Trends.VC](https://cdn.olai.in/jjude/trends-vc.jpg)

[Dru Riley](https://druriley.com/) is running such a [trend report](https://trends.vc/). If you read his reports, you will see a pattern emerging among the latest trends. The underlying pattern among many of the latest products is "**memberships**" - be it paid newsletters, podcasts, or communities. Probably it is time to start a "membership platform," like [Substack](https://substack.com/). You might soon see "**substack for x**."


### Study, Create, Profit

By the definition of Bill Gates, the Internet is the most useful and profitable platform ever created. If you know how to use it, you can find trends and take advantage of them to create a fulfilling life. Go.

Do you have any questions? Feel free to send me an [email](mailto:fromblog@jjude.com). Or [tweet](https://twitter.com/jjude) me.

If you are interested in followup articles, don't forget to subscribe using the below form.