---
title="Why And How I'm Homeschooling My Kids"
slug="why-how-homeschooling"
excerpt="What are the best techniques to prepare kids for an uncertain future?"
tags=["parenting","homeschool"]
type="post"
publish_at="25 Mar 22 18:04 IST"
featured_image="https://cdn.olai.in/jjude/kids-calendar-22.png"
---

We started homeschooling our two kids in October, 2021 (when they were studying 7th & 4th standards). Here I document the reasons for my decision. Additionally, I describe how we are doing it. Read on if you're interested. Leave a comment below if you have questions.

## Why

What are the objectives of education?

- To print a degree in wedding invitation (status symbol)
- To earn a living

Since I don't care about the status symbol, if I can get the kids to earn a living, that's enough for me. Additionally, if I can teach them how to be good humans, that would be a bonus. A good human life is not part of the curriculum of schools; it is an extra-curricular activity!

Schools care only about creating obedient students. There is little concern for developing students holistically. They are looking for students who will bring in more students in the future. For this, they will conduct programs (annual day functions, other celebratory functions) and leave it to parents to prepare their kids for those programs. The comic cycle of parents outsourcing to schools and schools delegating to parents continues. Children are affected by both parties' power play.

**Cost**

In India, school costs at least 1 lakh per year until 12th standard, which means the cost of my elder son's education would be about 4 lakhs (at the time of decision he was in 7th std). A typical engineering college education costs around 2 lakhs per year in fees alone. With travel, lodging, and incidentals, the cost reaches 10 lakhs for an engineering degree. This would amount to 40 lakhs for two boys.

Can I provide a better education to an average student than what the average college provides? 

My answer is YES.

## How

I do not see homeschooling as teaching school curriculums at home, rather as a way to train my children to earn a sustainable living. Neither quick money nor as much money, but sustainable wealth. My training is based on the [WINS framework](/wins/). It goes as follows:

- **Wealth**: Define what wealth is. Absolute riches don't matter; what matters is to live life fully.
- **Insight**: Learning to learn is a fundamental skill. Seeing profitable, non-obvious ideas and holding on to them for a long time. It is important that they are able to research topics, synthesize ideas, present ideas, and organize a community around them.
- **Network**: They should create a diverse and dense network. Engaging conversations, selling their ideas, and building long-term relationships are skills they should be able to develop and display.
- **Self-Care & Control:** This is the foundation on which they can build their other three skills. They should have a healthy self-esteem, and they can only have a positive self-esteem if they take care of themselves and are in control of their lives.

All curriculum and activities will revolve around those four pillars.

I am starting with these:
- Stock market investing (value investing)
- Music
- YouTube video creation
- Art

**Value Investing**

Stock market investing seems like an odd subject to teach to children 10 and 12. I see it as an introduction to a variety of topics. Learning value investing involves studying human psychology, anthropology, economics, pricing, and value, as well as different industries, finance, and most importantly, patience.

They have already made a few videos on these topics:

- [How to Value A stock](https://www.youtube.com/watch?v=uQuDd383xnE)
- [Should you Invest in Sonata?](https://www.youtube.com/watch?v=9jC30UWzitU)
- [Should I Invest In Graphite Limited](https://www.youtube.com/watch?v=EpcoQV6TmY4)
- [My Infosys Analysis!](https://www.youtube.com/watch?v=NPmGzqCeEXA)
- [My NMDC Analysis](https://www.youtube.com/watch?v=YrzAcBloa7I)
- [Coal India Analysis!](https://www.youtube.com/watch?v=nCipWHhp_Ww)

They are learning about researching, different industries, and how to speak confidently about their research. Even I am surprised at their growth.

After the age of 18, Indians are allowed to invest independently in stocks (till then, their income is included in the earnings of their parents). From now until the age of 18, I will teach them how to invest. I'll give them 5 lakhs from their education fund for them to make all the mistakes on the stock market for few years. After they have gained confidence, I will give them more money to invest and let them be on their own.

**Music**

The elder one is learning guitar, while the younger one is learning cajon. They both like their respective lessons. They both have private tutors. I have requested their tutors to accelerate their learning (much like [what Kimo did for Derek Sivers](/smartcuts/)).

**Industry Visits**

Every quarter, they visit companies and factories run by my friends. I encourage them to talk to the founders. They have already visited three companies, and I am surprised by the questions they ask and the manner in which they behave with the founders.

Here are some of the videos they created out of their industry visits:

- [Their first industry visit - a tooling factory in the defense industry](https://www.youtube.com/watch?v=XfWVgThySbU)
- [Visit to The Furniture Factory](https://www.youtube.com/watch?v=uUkpTCpwxrQ)

**Weekly Schedule**

![Homeschooling Weekly Schedule](https://cdn.olai.in/jjude/kids-calendar-22.png "Homeschooling Weekly Schedule")

In the mornings, they exercise three times a week and go for a walk with their mother in the nearby park two days a week. We always eat breakfast, lunch, and dinner together. We talk a lot at the dinner table. Our discussions cover their progress, any needed adjustments, stock market investing, philosophy, etc.

Then they go about their morning routines. They read five chapters from the Bible, write their understanding in two pages, and sometimes draw a visual out of what they read. Also, they must read the editorial in the newspaper and explain it in their own words. It serves as a practice for reading, understanding, and writing.

They read a lot of fiction. They read Geronimo Stilton, Shakespeare, Paulo Coelho, and like authors. In addition to increasing their imagination, these stories give them knowledge about history, psychology, and geography.

After lunch, they begin their productive time. They create videos about stock market analysis, unboxings of new purchases, historical figures, etc. Visit their YouTube channel to see what they produce.

They attend music class in the evening, two days a week. After that, their mother teaches them Tamil.

Twice a month, they spend a day in their friend's house. 

As you can see, there are lot of activities. The activities revolve around what they like, and in preparation for the trends that I anticipate to come in the future.

**Retros & Revisions**

This will, of course, evolve as I learn and progress. We review each week / month / quarter and revise as needed. 

## Frequently Asked Questions

### How will they get into college?

I do not plan to get them into college. As I mentioned earlier, they'll continue to do homeschooling and be able to earn a living on their own. In my prediction, they may not go in as employees but as job creators; or even if they are employees, they will begin earning on their own before they turn 21 years old, which is the usual age at which Indians enter the workforce.

If something were to happen to me, there are options for them to write exams to get back into regular educational streams. Refer the podcast I did with [Mahendran](/mahendrank/) for those options.

### What about social skills?

It is called homeschooling. Not cave-schooling. 

Kids are naturally attracted to talking and making friends. You have to force them to keep quiet. So unless you hide in isolation, no, they don't miss out on social skills. My kids mingle with other kids of the same age in the family; they move with kids in church; they also get to know kids of my colleagues. When time comes, I will send them to sports coaching, where they will meet people and move with them. All along, they also mingle with people of other age.

Another fact I observed. They don't hesitate to talk to CEOs when take them on industry tours, unlike the school going kids who are conditioned not to talk to elders.

They also are freely talking to kids of all age and classes (upper class, lower class) since I take them everywhere. In school they are exposed only to one set of people.

### How will you know if they are learning?

In homeschooling, there are no tests. They won't compete with others. We set certain goals every quarter and they work daily to achieve those goals. I train them to create a vision for the future, plan how to realize that vision, and organize their days accordingly. I'm happy as long as they improve every quarter. Plus, they will be able to prove their skills by being paid for their work or making money by investing in the stock market.

### Should I homeschool my kids?

I can't answer for you. Everyone's situation is different. I'm freely sharing my thinking behind this experiment and how I'm going about it. I'm open to talk to you in detail, if you are really interested. You can get my contact details from [about](/about/) page.

_If you have any further questions, please leave them in comments. Will try to answer them._

## Continue Reading

* ['Homeschooling In India' With Mahendran Kathiresan](/mahendrank/)
* [How We Started With Homechooling...](/how-we-started-with-homeschooling/)
* [Teaching Storytelling To Kids](/storytelling-to-kids/)