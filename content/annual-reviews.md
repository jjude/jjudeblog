---
title="Annual Reviews"
slug="annual-reviews"
excerpt="There is no looking forward, if you don't look back to introspect"
tags=["retro","flywheel","frameworks"]
type="page"
publish_at="01 Jan 22 10:22 IST"
featured_image="https://cdn.olai.in/jjude/annual-reviews-gen.jpg"
---

People are fond of making plans. I am fond of evalauting what worked in the days gone by. With that, I’m able to stack strengths towards success. Here are the published annual reviews.

- [2023](/2023/): Turning Fifty on a high note
- [2022](/2022/): Chasm of vanity metrics and wealth
- [2021](/2021/): Between dream and miracle
- [2020](/2020/): Pushing the WINS flywheel
- [2019](/2019/): Building a Personal Flywheel
- [2018](/2018/): Productive & Satisfied
- [2016](/2016/): A Productive and Joyful year
- [2013](/why-2013-is-the-best-year-for-me/): Breaking mental barriers
- [2012](/an-award-to-end-the-year/): An Award To End The Year
- [2011](/looking-back-at-2011-with-pride-and-satisfaction/): Looking Back At 2011 With Pride And Satisfaction
