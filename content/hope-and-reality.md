---
title="Tension between hope and reality"
slug="hope-and-reality"
excerpt="A template for going through trying times, like COVID"
tags=["coach","frameworks","luck"]
type="post"
publish_at="03 Jun 20 18:48 IST"
featured_image="https://cdn.olai.in/jjude/hope-and-reality-gen.jpg"
---


When Prime Minister Modi suddenly announced a one-day lockdown for COVID, I felt it would be easy to get through a one-day lockdown. When that one-day lockdown was extended to twenty-one days, the grim reality gripped me. When the lockdown got extended even further, fear of the uncertainty filled me.

Whenever you go through uncertain times, **you can respond in one of three ways**.

The first is **pessimism**:

"This is going to destroy me."
"The world is going to end."

When you take a pessimistic view, it becomes a self-fulfilling prophecy. 

The typical way people respond to uncertainty is with **unbridled optimism**.

"This will be over soon."
"Smart people are solving this problem so that it will be fine."

The optimists act as though things will get back to normal soon. With that misplaced optimism, they continue to behave as they always behaved. When the situation doesn't improve, or it deteriorates continuously, they lose heart and give up.

The best way to respond to uncertain situations, especially when you don't know how long it is going to last, is what I call as **determinant optimism**. A determinant optimist is hopeful that they will get through the troubled times, but they also look at reality and ask themselves, "what will get me to the other side of this trouble or what will reduce the intensity of the effect." Then they focus their efforts on implementing the answer.

During COVID kind of uncertainty, an unbridled optimist might spend money as they have always spent because they believe the situation will turn back to normal soon. When they run out of money, they panic and lose hope. On the contrary, determinant optimist entrepreneur conserves their cashflow to have enough runway. They might negotiate with their customers for micro-payments; they might renegotiate rent; they might postpone any pay hikes; at the same time, accelerating their marketing and sales efforts.

Likewise, a determinant optimistic employee will stop mindless binge-watching, learn new skills, and connect with potential mentors to increase their "**who luck**."

Acting as though the future will worsen, but firmly believing that you will make it out of the troubled season is a cognitive paradox that many can't handle. Even the trained military professionals can't handle this paradox as [recollected](https://www.youtube.com/watch?v=GvWWO7F9kQY) by a Vietnam war veteran, James Scottsdale. He narrated the following to Jim Collins, the author of '*Good to Great*' book:

> You must never confuse faith that you will prevail in the end—which you can never afford to lose—with the discipline to confront the most brutal facts of your current reality, whatever they might be.

When you are going through trying times, you should embrace the seemingly contradictory ideas of "confronting the reality" and "unwavering faith." There is no other way.

## Continue Reading

* [Approximately correct](/approximate/)
* [Life is series of lost, found, and lucky breaks](/life-is-lost-and-found/)
* [Don't Make All Mistakes; Learn From Other's](/dont-make-all-mistakes-learn-from-others/)
