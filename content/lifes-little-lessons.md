---
title="Life’s little lessons"
slug="lifes-little-lessons"
excerpt="Three of the best life lessons"
tags=["insights"]
type="post"
publish_at="26 Mar 08 16:30 IST"
featured_image="https://cdn.olai.in/jjude/lifes-little-lessons-gen.jpg"
---

As I go through the ups and downs of life, these words keep me going with sanity.

##### This too shall pass

I've come across many difficulties in life - disappointments, physical ailments, academic failures, broken relationships and financial difficulties. As I went through each of the tough phase in my life, I'd tell myself - 'This too shall pass' and so did each of them.

There was a time when I used to think that, &quot;I don't need to do anything; with time, this situation will go away or get better&quot;. I've realized that it is a lazy attitude.

Some situations may go away or get better as time goes. In other cases, you have to change your perspective or take a solid action before the situation get any better. However, most often, it is, 'take-enough-action-and-then-wait-with-hope'.

It has guided me not only during my bad days. These words have guided me during my 'feeling-damn-good-buddy' days as well.

In essence, these words have kept me going with hope one one side and humility on the another.

##### Life is not a zero-sum game

I've not understood game theory well. All that I know is, &quot;I don't have to necessarily loose for others to win; and others don't have to necessarily fail for me to win&quot;. Both can win. Or in corporate parlance, it is a 'win-win' situation.

In one of the mail forwards, I read the below and it challenged me to think with this 'win-win' perspective.

_A shoe salesman is sent to an island. He returns back saying, &quot;None wears shoes; No opportunity&quot;. The company sends another. He returns saying, &quot;None wears shoes; great opportunity&quot;._

I've been in many disastrous situations - in personal and professional lives - sometimes when others have explicitly given up hope. I don't mean to say I can solve the issue. Not always. But something better can come out of it, if someone takes the pain to think.

##### Life is a bitch and at the end you have to die

I bet you did not expect this after all this optimistic talk. Right? Well, let me say one thing: I'm somewhere in between being-optimistic and being-pessimistic, which I call 'being-realistic'.

When I get attached to life (when the going is good) and try to be a good person myself, I start to expect that life will be fair to me. Let us get this straight: life can be beautiful; life can be fun; life can be anything but fair. Life is unfair: sometimes in favor of you; most often ruthlessly against you.

I accept the brutal fact that life is unfair (rather a bitch). That doesn't depress me; rather it challenges me to be stronger to take action (life is not a zero-sum game; think differently to win) with a hope (this too shall pass).

I keep repeating this (AA) prayer quite often:

_Grant me the strength
To change the things that I can
Grant me serenity
To accept the things that I can't change
And wisdom
To know the difference_

Life is a paradox. Beauty lies in knowing to sail through that.

