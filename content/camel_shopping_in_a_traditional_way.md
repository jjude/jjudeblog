---
title="Camel shopping in a traditional way"
slug="camel_shopping_in_a_traditional_way"
excerpt="A memorable day, partly living in the ancient Egyptian way."
tags=["travel"]
type="post"
publish_at="10 Sep 05 16:30 IST"
featured_image="https://cdn.olai.in/jjude/camel_shopping_in_a_traditional_way-gen.jpg"
---

After having spent good amount of time visiting Luxor, Aswan & Abu Simbel in southern Egypt, it was time for what I love to do during travel - going off-beaten path. With some search and enquiry, I decided to go to Daraw, a village near Aswan, famous for its camel market. I learned that camels are brought from as far as Sudan and sold here and the market is in existence for many years. Enquiring at the reception and the hotel travel desk, I came to know that it is better to take a bus there; no cabs come to Daraw.

So dawned my day with an excitement. I took a luxurious highway bus which passes through the village. I had to walk for a long distance from the bus stop; don't remember if it was 2 or 3 km; not that it mattered. I was in a place where time has come to a stand still. I was walking on a dirt road with old mud houses on both sides, with wild bushes here and there. A Chevrolet Jeep carrying a man with a mobile phone zipped past to announce the existence of modern life in the otherwise ancient village. Along the way, I watched in awe, men traveling on donkeys, camels tied and carried in Jeeps and fertile fields.

I would have walked for couple of hours, and there it was, a huge ground with countless number of camels. May be 500 or may be 1000! Kiddy ones, healthy ones and lean ones (I am sure there were female and male too, but I don't know how to differentiate). I have never seen so many camels in one place. Some of them were lying down, others standing on three legs (for the fourth one was tied so that they wouldn't run away), and few others were grazing whatever was left on the ground. My eyes were twinkling in surprise.

At one end of the market, there was a thatched roof and men were sitting and smoking water-pipes and others drinking black tea. There was an old man who has been a guide for the camel market for a period that he can't remember. He took me around explaining how to identify a race camel from a camel brought for just plain meat. There was a big butcher shop within the market. He even said the approximate age of each of camel. Those men were amazed with the digital pictures I took of them. So with the video camera, I shooted them and entertained them with the video. They were happy and I was much more than happy. For few hours I witnessed life lived in an ancient way.

On my way back, I stopped by a road side shop. There was a big pan and the guy was making something that looked delicious. I asked, 'what is it?'. His answer seemed like a shout! Then someone emerged out of nowhere to explain, in rather broken English, that it is a made of spinach and..., again something that I didn't understand. I asked him a simple question: Is it good and he shook his head up & down to imply yes. I ended up eating few and I found them very delicious.

Thus went the day; a memorable one, partly living in the ancient Egyptian way.

