---
title="My First Book - Ionic 2 - Definite Guide"
slug="ionicbook"
excerpt="I'm happy to tell you about my first book, Ionic 2: Definite Guide, which I co-authored with my sister."
tags=["books"]
type="post"
publish_at="19 Dec 16 12:55 IST"
featured_image="https://cdn.olai.in/jjude/ionic-book.jpg"
---
I'm happy to tell you about my first book, 'Ionic 2: Definite Guide', which I co-authored with my sister. It is announced to the market for [pre-order](http://jjude.in/ionicbook).

![Ionic Book by Joseph Jude and Joyce Justin](https://cdn.olai.in/jjude/ionic-book.jpg "Ionic Book")

Here are the chapters and their summary:

### Chapter 1 : Welcome to the world of Ionic
There are 2.5 billion smart phones in the world and that number is growing at an exponential rate. These smart phones are not homogeneous and the screen sizes of these phones are fragmented.

Coding a user interface that works well across these multitude of phones is a huge challenge for mobile application developers. Ionic comes as a great relief to these challenges.

Why Ionic, compared to other cross-platform tools? Because it has great documentation, supportive community, and a thriving market place.

### Chapter 2: Build Your First Ionic App

Install Ionic 2 and learn to create the first Ionic app. Learn about quick-start application templates. Build a simple counter application with "**blank starter**" template. The app has just two buttons: one to increment the counter, and another to reset the counter.

### Chapter 3: Building blocks of Ionic

Ionic 2 is built on Angular 2, and Angular 2 is built on Typescript. So learn these two building blocks before mastering Ionic 2.

### Chapter 4: Those Famous Quotes

Learn to build multi-page app and navigation between them.

![Ionic Book by Joseph Jude and Joyce Justin](https://cdn.olai.in/jjude/ionic-book-quotes.png "Fetch via web-service and display in multi-page Ionic app")

Build a quotes app by fetching quotes as a json file through web-service from a server.  Display all these quotes in a list and when a quote is selected, display that quote in a detail page.

### Chapter 5: What will be the weather like?

Learn to display data in a chart.

![Ionic Book by Joseph Jude and Joyce Justin](https://cdn.olai.in/jjude/ionic-book-chart.png "Display weather forecast as a chart in an Ionic app")

Build a "Weather App" that displays current weather and weather forecast in two tabs. Learn to capture data in a form and validating values in a field. Use charts to display forecast for the location.

### Chapter 6: Saving Memories

Learn to use native hardware features like Camera. Take a picture and store it in the device.

### Chapter 7: Gather Analytics

When you have built a mobile app, you want to know how many are using it. Learn to integrate Google Analytics with your mobile app.

### Chapter 8: Go offline

Build an offline mobile app that stores data locally and sync with actual database when the connectivity is available.

### Chapter 9: Where to go from here

Learn the rich Ionic ecosystem.

There is [Ionic creator](https://creator.ionic.io/), a drag-and-drop prototyping tool; [Ionic View](http://view.ionic.io/), a stock app to distribute your app for testing; Ionic Cloud, a powerful mobile backend as a service; and [Ionic Marketplace](https://market.ionic.io/), where you can buy and sell Ionic related assets like themes and plugins.

In addition, Ionic has strong, supportive community to help you along the way. They are everywhere! There is a traditional [forum](https://forum.ionicframework.com); there is [Stackoverflow community](http://stackoverflow.com/questions/tagged/ionic2); there is even a [Slack Community](http://ionicworldwide.herokuapp.com/).

You can use all of these to develop your Ionic expertise.

***

If all this sounds exciting, go and pre-order the book from [Amazon](http://jjude.in/ionicbook). If all goes as per plan, you should have the book by early 2017.
