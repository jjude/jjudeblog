---
title="Siddhartha Ghosh on 'What we can learn from nature'"
slug="siddharthag"
excerpt="Nature is our home. It was here before us. There are lot we can observe and learn from nature."
tags=["wins","gwradio"]
type="post"
publish_at="07 Dec 21 06:00 IST"
featured_image="https://cdn.olai.in/jjude/siddhartha.jpg"
---

<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless src="https://share.transistor.fm/e/0a97aef4"></iframe>

- How did you get into wild life photography?
- How do you plan any photography?
- Is it a matter of tool or patience and skill?
- Has any animal posed for you?
- Any near death experience?
- Have you ported any of the skills you learned in the wild-life into your professional life?
- What drew you towards nature conservation?
- How can we practice it in our normal daily lives?
- How do we balance between progress and preserving our nature?

![What can we learn from nature with Siddhartha Gosh](https://cdn.olai.in/jjude/siddhartha.jpg)

## Connect with Siddhartha:

- LinkedIn: https://www.linkedin.com/in/sidghosh-analytics/
- Instagram: https://www.instagram.com/siddphoto/

## Connect with me
- Twitter: https://twitter.com/jjude
- LinkedIn: https://www.linkedin.com/in/jjude/
- Website: https://jjude.com/
- Newsletter: https://jjude.com/subscribe/
- Youtube: https://youtube.com/gravitaswins
- Email: podcast@jjude.com
- Executive Coaching Program: https://gravitaswins.com

## Your feedback counts

Thank you for listening. If you enjoy the podcast, would you please leave a short review on Apple podcast or on YouTube? It takes less than 60 seconds, and it really makes a difference in finding this podcast. And it boosts my spirits. 😀

I would like to hear what resonated the most with you in this episode. Either you can email me or share it on social media and tag me. Thank you for your support.

Have a life of WINS.

## Amazing tools I use:
- I record all the remote interview with [Riverside.fm](https://riverside.fm/?via=joseph-jude)
- I use [Transistor.fm](https://transistor.fm/?via=jjude) for hosting these podcast episodes

_Gravitas WINS Radio is produced by [Joseph Jude](https://jjude.com/). Be sure to subscribe on [Apple Podcast](https://podcasts.apple.com/in/podcast/gravitas-wins-radio/id1562802449), [Spotify](https://open.spotify.com/show/7LOBjAyjBkechMC0xMIj9E),[Google Podcast](https://podcasts.google.com/feed/aHR0cHM6Ly9mZWVkcy50cmFuc2lzdG9yLmZtL2dyYXZpdGFzLXdpbnMtcmFkaW8=),[YouTube](https://www.youtube.com/gravitaswins), or the podcasting app of your choice._

Please check out other episodes of [Gravitas WINS Radio](/gwr-themes/). For easy consumption, they are categorized by themes. If you are a fellow podcaster, you might be interested in [How I Podcast](/how-i-podcast/) or [tools I use](/i-use/).