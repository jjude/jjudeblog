---
title="Serving The Nation With A Delight"
slug="serving-the-nation-with-a-delight"
excerpt="As part of Government machinery, I participate in e-Governance initiatives that directly affect the service delivery of public services to citizens. That is my service to this great nation."
tags=["retro"]
type="post"
publish_at="22 Jan 12 16:30 IST"
featured_image="https://cdn.olai.in/jjude/serving-the-nation-with-a-delight-gen.jpg"
---

It started with a chat with my [mentor](http://in.linkedin.com/in/sastry). I had a job offer as Head of Application Development Unit from an infrastructure company and I wanted to check with him, if I was making a good choice. He confirmed that it was a good choice. Then he asked me a question that would change the course of my career.

"Government of India is looking for a project manager.  I think you are a good fit for it and it would be good for you too. Would you consider joining?"

I have greatest respect for him. If he would've asked me to join a failing company, I would have gladly joined. So I dropped the grandiose plans of being a business unit head and decided to follow where the wind led.

At that point, I had no idea what I was getting into. My idea of government was formed largely by media reports and my limited interactions with government offices for obtaining driving license, registering flats and the likes. I would be lying if I said it was anything but positive.

I had to make quite a lot of changes too. I had to become a solo consultant without backing of any company; and I had to move to Delhi without knowing the [language](https://en.wikipedia.org/wiki/Standard_Hindi). They were major life changes. Was it all worth?

Three years later, the answer is, it is. Any my prejudice of government machinery changed too.

May be because I got to work with the best [e-governance project](http://www.mca.gov.in/) in India. It is one of best conceived, architected and monitored project, winning the most coveted Prime Minister's award, in addition to host of other awards.

My experience is limited<sup id="fnref:1">[1](#fn:1)</sup> to this Ministry and Project. But I could say, most of the bureaucrats I interact are brilliant. Some of them surpass private players. Take for example, Mr D K Mittal, ex-secretary of MCA and current Banking Secretary - he was more restless than a CEO of a startup. He did his homework well and demanded that we did ours too; he forced everyone to think of what is needed for the country in 2030 and start building the foundation now. He took bold steps. To quote just one, in a single stroke of order, he saved a forest by eliminating posting of physical copies of annual report to shareholders; they are to be sent electronically. What an idea, sir ji!

Also, I found many middle level public servants committed and passionate. I work on a daily basis with two such gentlemen - Sh Rao in NIC and Sh Sridharan in MCA. Sh Sridharan's passion to MCA21 project is dangerously contagious. They don't dream of sales commissions, bonuses and retirement in Bahamas. They get to work every day without much fuss. Many, like these gentlemen, work throughout India to make our lives little bit better.
> "Patriotism is not short, frenzied outbursts of emotion, but the tranquil  and steady dedication of a lifetime."  (Adlai Stevenson)

Some one said, you are average of five people you interact with. If so, then I have become a lot more intelligent and patriotic in the past three years, working with many such people.

If I'm happy about what I've learnt, I'm proud of my contributions. As a corporate employee, one's contribution to nation is limited to paying taxes, voting and social service (many don't do that is out of context for this blog entry).  But as part of Government machinery, I participate in e-Governance initiatives that directly affect the service delivery of public services to citizens. I manage few of these initiatives, monitor many and I architect some. Thats a big deal!

If I have kindled your interest enough, know that avenues to work in Government projects are opening up unlike before. [NISG](http://www.nisg.org) is pioneering capacity building for the Government of India and they monitor and manage many of these e-Governance projects. You could join them. Of course, the conventional avenues of joining [NIC](http://www.nic.in) and related governmental organizations has been open. You could also be part of private organizations that undertake these e-Governance projects under [Public Private Partnership](https://en.wikipedia.org/wiki/Public%E2%80%93private_partnership) mode.

More than ever, chances to bring a change in India are many. Instead of sitting on the fence and raise a finger, come, be a part of the change.
<div class="footnote">

* * *

1.  Though I didn't work on other projects or ministries, I interact with various other ministries and consultants working on other projects. So I guess I do have a comprehensive view.&#160;[&#8617;](#fnref:1 "Jump back to footnote 1 in the text")</div>


