---
title="Why It Hurts To Be An Indian?"
slug="hurts2bindian"
excerpt="Going from an emerging economy to a submerging economy hurts."
tags=["opinion"]
type="post"
publish_at="06 Feb 14 16:30 IST"
featured_image="https://cdn.olai.in/jjude/hurts-as-indian.png"
---
Alan, who had a positive influence in my consulting career, [noted][4] few days back[^3].

>Did you see where Brazil and India, once part of the three “emerging economies” (with China) that were going to alter the world stage are now part of the “fragile five” which can’t really sustain their own growth without unreliable foreign investment?

This wouldn't hurt so much if it is not true.

I was in college when [Narashima Rao][5] sowed seeds for India's growth in early 90's. We, as young students, cherished those stories that were told to us that India will outgrow developed nations. We were euphoric that our future, would be fantastic, unlike our parents' who had to suffer low rate of growth (somewhere in the range of 4-5%) and red-tapism.

But then Advani appeared on the scene.

His rath yatras culminated in destruction of Babrij Masid ending focus on growth and split the country on communal lines[^1].

Next, BJP came to power. They were not so bad as we feared, since Vajpaee was sensible in putting country first unlike some of his party colleagues.  BJP under Vajpaee didn't dash our hopes. India continued to grow. Investments poured into infrastructure and IT. Hydrabad and Bangalore grew as IT hubs promoted by visionary chief ministers. Heads of state thronged to visit these cities and CEOs of IT companies. There was euphoria everywhere. India was unstoppable.

![Hurts To Be An Indian](https://cdn.olai.in/jjude/hurts-as-indian.png)

Then "Manmohan Singh" happened.

Businessmen, markets and everyone cheered as he was hailed as the architect of Indian growth under Narashimha Rao. There were silly predictions that India will be an economic super-power, since it had an economist as a PM.

MMS proved [Peter principle][6].

Under him, India's growth tanked.

If there were structural reforms going on, it would be okay for a lower growth rate. But in the last decade, there has not been any major reforms in any field (except RTI may be).

It is not just the lack of reforms. India is back to its bureaucratic process that throws bottlenecks on growth. India scores low in any index that matters to business - be it in [transparency][8] or [doing business][7], India is at the lowest bottom.

So, it has been a double loss for India. One we lost a decade without a reform. The other is we deteriorated to 90's in terms of processes. This hurts.

The disappointment that we didn't tap our potential to grow higher than we could've, hurts; the frustration that we didn't put in place any structural reforms that will boost growth in the future hurts; the humiliation to go from emerging economy to submerging economy hurts.

It hurts to be an Indian[^2].

[4]: http://www.contrarianconsulting.com/alans-thought-for-today-23/
[5]: https://en.wikipedia.org/wiki/P._V._Narasimha_Rao
[6]: https://en.wikipedia.org/wiki/Peter_Principle
[7]: http://www.doingbusiness.org/data/exploreeconomies/india
[8]: http://www.transparency.org/country#IND

[^1]: He came into prominence by division, which is unfortunate in the land of Gandhi, who worked for unity.

[^2]: I should add that being an Indian in India is way different from one abroad. As the US has shown, Indians will thrive in an efficient structure.

[^3]: I drafted this post after reading Alan, but delayed posting because of edits and appropriate illustration. MSFT selecting an Indian CEO turned out to be a coincidence.
