---
title="Sukhwinder Singh on 'Mantras for going from ₹1200 to ₹1 Crore of sale'"
slug="sukhwinders"
excerpt="Going from the first order of ₹1200 to orders worth ₹1 crore by being truthful to oneself and combining being fast and fortunate."
tags=["gwradio"]
type="post"
publish_at="09 Nov 21 06:08 IST"
featured_image="https://cdn.olai.in/jjude/sukhwinder.jpg"
---

<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless src="https://share.transistor.fm/e/1297ac5a"></iframe>

I interviewed Sukhwinder Singh Uppal for [Gravitas WINS Radio](/podcast/). After working for a large corporate, he started his manufacturing plant in Mohali, Punjab. His first order was ₹1200. Now, sixteen years later, he serves orders worth ₹1 crore. 

We discussed his guiding mantra, lessons he learned along the way, and books and courses that shifted his thinking.

Here is the edited version of our conversation. I was liberal in editing the content. You should listen to the podcast.

![Inspirations Don't Die](https://cdn.olai.in/jjude/sukhwinder.jpg "Inspirations Don't Die")

## What you'll hear
- Early internship selling Diwali crackers
- The story behind the company name - Priority Solutions
- Early days of starting up
- Core mantra for going from ₹1200 to ₹1 Crore of sale
- Training employees to meet the mantra
- Be faithful to yourself
- Dealing with setbacks
- Building a team that holds itself accountable
- Building India's first bullet proof testing lab
- Balancing between fast and fortunate
- Disengage from basic activities to think
- Leaders you admire
- Books that shifted the thinking
- Optimism as a habit
- Gravitas WINS course and community
- Kindest thing anybody has done for you 
- Definition of living a good life
- Finding one's purpose

## Edited Transcript

### Early internship selling Diwali crackers

My cousin used to put up a shop for Diwali crackers. My dad suggested me that I should join him and help him. I used to go by cycle to his shop only during Diwali days and I used to manage that cracker shop.

I was handling customers. They always wanted to negotiate. That helped me build confidence in handling customers.

I did it for 4 - 5 years. In the last year he decided not to put up the stall. I and my brother decided that we'll put up the same. He helped us. It was a big learning handling everything from buying crackers to selling.

### The story behind the company name - Priority Solutions

Every customer wanted us to keep them on the top priority and they expected the best solutions and we ended up naming Priority solutions. And the tagline is your priority, our solutions.

I have kept this close to my heart that we must be known for our spontaneous response. We should be the fastest. We knew that we are small, but I wanted to be the sharpest. Even today I claim that in and around this region, no one can do things faster than us. And we are proud of our team on this. 

### Training employees to meet that mantra

In the initial days we used to drive things from a start to end by our own. And we had that passion. We had the vision. We were clear on the basic things.

We try to understand the purpose for which a customer is with us and we try to give a holistic approach rather than only focusing on the specific need. What is expected of us or what is defined? We used to try to cater to the undefined things also. That has brought us here 16 years down the line.

### Early days of starting up

In my last assignment in Volvo, I was into supplier audits and vendor development. I traveled the length and breadth of India. What I identified was that **companies were not investing into research and development**. They were making good business, but they were not fast enough for the new developments and which was critical for their future. 

When I started this company, I knew the crux. I knew where the bullseye is. So I targeted those companies, which were doing good in production or large volumes, but wanted to grow bigger. I extended my expertise to lot of companies in the Baddi area. We started designing assembly lines for them.

When we started, we targeted their pain area and from there we moved on. **We have learned from the toughest teacher and that is market**.

### Core mantra for going from ₹1200 to ₹1 Crore of sale

The first sales is very close to my heart. It was for ₹1200. 

We have been fortunate and blessed to have stability in the business. We have already crossed a single order of ₹1 crore. I am seeing huge potential in the future.

We have never, ever been unfaithful to ourselves. We have always identified the areas, the weakness, where we need to improve, and we have worked on those things. **We have been sincere to ourselves**. 

### Be faithful to yourself

It starts with knowing and accepting yourself - being faithful and factual of the truth about yourself and never, ever, excuse yourself.

We all humans tend to blame situations or other people, rather than accepting the failure, what we have already incurred.

Start owning things. And during my 16 years of entrepreneurial journey, I've seen people failing and majority of the people have this thing in common that they weren't faithful to their self. 

### Dealing with setbacks

There is no shame in sharing setbacks. We've had more setbacks than the successes and that only has taught us. And failure is very important in life, we must accept failure with grace and we have always accepted it. We have analyzed the reasons behind it, wherever we failed.

There are two type of things which work in this world. First are the areas where you can play some role. And there are other things which are not in our hands. Rather than cursing those things which are not in our hand, we should just focus on things which are in our hands.

We should try to improve ourselves.

### Building a team that holds itself accountable

Preach what you practice. Because if I preach something else than what I'm practicing, they won't take it that way. It has to be in my blood. Only then they will accept it. If I am accepting my own mistakes on the table, obviously they will also come up with theirs.

It is reciprocal.

### Building India's first bullet proof testing lab

It happened after the Mumbai attack. When Hemant Karkare and others were killed by the attackers from Pakistan. Our government decided to test the bulletproof material and validate it before using it.

The Commonwealth games was coming in. It was in 2009 and MHA decided, and they gave this assignment to central forensic science laboratory, Chandigarh. We were referred to CFSL, Chandigarh. We decided to build it since it was matter of national importance. We have evolved around it and we have developed various other new things on the same platform.

### Impact of Covid

It has affected the industry hard. Market is still not back to normal to the pre COVID levels and the inflation and less demand have shut many shops. 

We have been fortunate enough, being agile and faster to change. We built the UV sanitizer chambers and other sanitizer units.

We have been very fast and very adaptive so we could bounce back early.

### Balancing between fast and fortunate

Being lucky is something that we should be prepared for the opportunity. Opportunities knock at our doors. We should be prepared to grab them. And we have always tried to prepare ourselves for the future.

And we do strategic thinking on 10 years down the lane - how market will be and all those things. We always tried to be optimistic of the capabilities of the team and everything around.

### Disengage from basic activities to think

During COVID, we were all free to do something new. I started this practice of positive self-talk to cultivate the right mindset and planning for the future.

**We must be capable of analyzing variety of inputs, like market conditions, emerging business trends and internal resource allocation.** 

We should have the ability to communicate complex ideas to internal and external stakeholders. We have to collaborate with them. We have to engage them. We bring them onboard and building consensus and ensuring that everyone is aligned towards the shared goal.

I dedicate 2 - 3 hours in the second half of every day to plan and implement these solutions. I'm getting very good results and I'm disengaging myself from those basic activities now. 

I'm a big fan of a Pareto principle of 80/20, and I applied in every aspect of my life. So what I tried to do is I have identified those activities where I'm involved for 80% of the time and the value which is coming up is about 20%. I delegate those activities and disengage to spare more time for myself and the company so that I can do the strategic thinking and planning.

Identify those areas where people can be deputed. And they'll make some mistakes. If we keep on focusing on that perfection, then it won't happen. Things will go wrong. That's the cost we are paying to build those teams.

> Letting go of perfection is the cost we are paying to build teams to which we can delegate.

### Leaders you admire

**Leaders who birth to future leaders** are those whom I admire. We must build the second line of leaders. The courageous, honest, forward looking and those having the ability to inspire are the leaders who I admire.

I don't Google for those leaders. I look around myself, I look around my area, my vicinity, those people whom I might physically know, so that I can extract the learning, I can converse with them and I can learn from them.

### Books that shifted the thinking

- [Magic Of Thinking Big](https://amzn.to/3qeABXh)
- [Rich Dad Poor Dad: What the Rich Teach Their Kids About Money That the Poor and Middle Class Do Not](https://amzn.to/3mPgY5V)

### Optimism as a habit

You have to be optimistic to handle newer and newer challenges. We should look at problems as opportunities.

Take my case. Now there is COVID and supply chains are disturbed. The defense companies that are centered abroad are not possible to deliver things faster. So I can pitch in those areas and those territories. And we have already started **working very aggressively** for developing most of the equipment related to the ballistic industry.

Work on things which you can handle. Don't look for excuses. Economy's not working. Inflation is very high. There is no demand. This all is there and we can't change it.

There are only 20% of the things which are in our control. So obviously work on those and leave those 80% to the almighty.

### Gravitas WINS course and community

Firstly, I want to be very much thankful to you for introducing them to us. 

I belong to pragmatism school of thinking which says that test of a good theory is that they must have some problem solving path.

Whatever ideas you have shared, they all had energy. Through the course, I got very good practical frameworks for translating those ideas into action. 

And the community has been so special to me. And it's pushing me to grow in the right areas. I'm indebted to the community. So it's, it's bringing a lot of positivity around. 

### Kindest thing anybody has done for you 

After almighty, it is our dear parents, who have brought us up in such a beautiful way with limited sources at their disposal. They have built us independent and we cannot repay. 

### Definition of living a good life

Those who believe in themselves live a holistic life. 

Know who you are. Know your purpose, and then be true to that purpose. 

**Fill your life with fulfilling relationships, dreams, goals that move you forward**.

The aim is to move, make progress, to move forward, to adapt and keep evolving.

### Finding one's purpose

We should understand ourselves first. What are our strengths and weaknesses? Knowing oneself  will solve half of the battles. Once you know yourself and you are factual about your strengths and weaknesses, then you can find life. It cannot be injected. It has to be brought from within.

We should work on ourselves. We should know ourselves and be truthful to ourselves.

### Role of parents

I'm what I'm because of my upbringing. Our father has always been very transparent with us.

We started having those conversations and that has brought the transparency about being truthful and he has already admitted whatever mistakes even if he makes. This taught us to be truthful.

## Watch on YouTube
<iframe width="560" height="315" src="https://www.youtube.com/embed/qtYCoz-2Re4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

## Connect with Sukhwinder

- LinkedIn: https://www.linkedin.com/in/sukhwinder-singh-uppal-05525a8/
- Twitter: https://twitter.com/singhtechnical
- Instagram: https://www.instagram.com/singhtechnical/

## References

- [Baddi Area](https://en.wikipedia.org/wiki/Baddi)
- [Hemant Karkare](https://en.wikipedia.org/wiki/Hemant_Karkare)
- [2008 Mumbai Attacks](https://en.wikipedia.org/wiki/2008_Mumbai_attacks)
- [Gravitas WINS course](/gwcourse/)
- [Gravitas WINS weekly shows](/gwshow/)

## Connect with me
- Twitter: https://twitter.com/jjude
- LinkedIn: https://www.linkedin.com/in/jjude/
- Website: https://jjude.com/
- Newsletter: https://jjude.com/subscribe/
- Youtube: https://youtube.com/gravitaswins
- Email: podcast@jjude.com
- Executive Coaching Program: https://gravitaswins.com

## Your feedback counts

Thank you for listening. If you enjoy the podcast, would you please leave a short review on Apple podcast or on YouTube? It takes less than 60 seconds, and it really makes a difference in finding this podcast. And it boosts my spirits. 😀

I would like to hear what resonated the most with you in this episode. Either you can email me or share it on social media and tag me. Thank you for your support.

Have a life of WINS.

## Amazing tools I use:
- I record all the remote interview with [Riverside.fm](https://riverside.fm/?via=joseph-jude)
- I use [Transistor.fm](https://transistor.fm/?via=jjude) for hosting these podcast episodes

_Gravitas WINS Radio is produced by [Joseph Jude](https://jjude.com/). Be sure to subscribe on [Apple Podcast](https://podcasts.apple.com/in/podcast/gravitas-wins-radio/id1562802449), [Spotify](https://open.spotify.com/show/7LOBjAyjBkechMC0xMIj9E),[Google Podcast](https://podcasts.google.com/feed/aHR0cHM6Ly9mZWVkcy50cmFuc2lzdG9yLmZtL2dyYXZpdGFzLXdpbnMtcmFkaW8=),[YouTube](https://www.youtube.com/gravitaswins), or the podcasting app of your choice._

Please check out other episodes of [Gravitas WINS Radio](/gwr-themes/). For easy consumption, they are categorized by themes. If you are a fellow podcaster, you might be interested in [How I Podcast](/how-i-podcast/) or [tools I use](/i-use/).

## Continue reading
- ['Words Change Lives' with Ritika Singh](/ritikas/)
- [Culture is what you do, not what you believe](/culture-is-doing/)
- [Book Notes - Smartcuts](/smartcuts/)