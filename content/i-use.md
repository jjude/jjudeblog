---
title="Tools I use"
slug="i-use"
excerpt="Hardware, Software Apps, and SAAS Apps I use to do whatever I do"
tags=["tech"]
type="page"
publish_at="20 Aug 23 20:02 IST"
featured_image=""
---

## Hardware

- [Mac Book Air 13" with M2 chip](https://geni.us/macboo-air-13-inc)
- [LG 22 inch IPS Monitor](https://geni.us/lg-22-monitor)
- iPhone 11
- [Apple Airpods 3rd Generation](https://geni.us/airpod-3rd-gen)
- [Qizlar Extended Gaming Mouse Pad](https://geni.us/Qizlar-MousePad)
- [Logitech Ergo M575 Wireless Trackball Mouse](https://geni.us/logitechm575)
- [Logitech K380 Wireless Keyboard](https://geni.us/logitk380keyboard)
- [PALO Ergonomic Footrests](https://geni.us/PALO-EFootrests)
- [Audio-Technica ATR2100](https://geni.us/atr2100-mic)
- [Maono AU-B01 Microphone Suspension Boom](https://geni.us/maono-aub01)

## Mac Apps
- 1Password 7
- Alfred 4
- Apple Mail
- Basecamp
- Descript
- Drafts
- Eagle
- Firefox
- Fork
- Goodnotes
- iTerm
- Itsycal
- [Obsidian](https://obsidian.md/)
- ReadKit
- Safari
- [Screenflow](https://www.telestream.net/screenflow/)
- Sketch
- Skitch
- Soulver 2
- TextSniper
- Visual Studio Code
- Vivaldi Browser
- Zoom

## iPhone / iPad Apps

- 1Password 7
- Audio Pen
- Basecamp
- Bible
- Buffer
- Drafts
- Dropbox
- Gmail / Google Calendar / Google Docs / Google Maps / Google Translate
- Kindle
- [Obsidian](https://obsidian.md/)
- Overcast
- Journey
- LinkedIn
- Reeder
- Reader
- Scribd
- Slack
- Spotify
- Todoist
- [Working Copy](https://workingcopy.app/)
- Whatsapp

## SAAS Services

- Audio Pen
- Basecamp
- Dropbox
- Feedly
- Riverside.fm
