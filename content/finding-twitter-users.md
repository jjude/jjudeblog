---
title="How To Find Awesome Twitter Users To Follow"
slug="finding-twitter-users"
excerpt="Step by step guide for finding curious twitter users from whom you can learn"
tags=["wins","insights","sdl"]
type= "post"
publish_at= "15 Jul 20 10:30 IST"
featured_image="https://cdn.olai.in/jjude/find-twitter-users.jpg"

---

> For the men of Athens, it was not a question of going there occasionally, hoping to see someone they knew; they went there all the time, expecting to see everybody. A stranger stood out. All other faces were familiar. News and gossip were exchanged side by side with goods and currencies. Here was the public forum of a free society where men could gather to discuss the issues of the day. It was also a place for political shenanigans, including an occasional idea.
> _[Greek realities; life and thought in ancient Greece](https://amzn.to/3fm5Ffv), by Finley Hooper_

Here, Finley Hooper is talking of _agora_, the downtown in Athens. If he were writing today, he would say the same thing about social media in general and Twitter in particular.

I have been an early adopter of most of the social media. I have used the popular ones like Facebook, Twitter, Instagram, and even the lesser-known ones like Ello. Of all these sites, I gained the most from Twitter, and hence, it has become a crucial part of my [learning routine](/sdl/).

![Obama Follows Me](https://cdn.olai.in/jjude/obama-follows.jpg)

_A trivia. Where else would President Obama follow a nobody?_

I already wrote about how I [use Twitter to learn](/learn-via-twitter/). As I mentioned in that post, learning from Twitter goes like this:

- Collect high-quality links from Twitter
- Save the link in Pocket
- Read articles in batches
- Save best articles in Evernote
- Distill the learning into a blog post
- Share on Twitter and discuss with others

Learning via Twitter starts with getting high-quality links. How do you get high-quality links? That is the topic of this post. 

You can get high-quality links only if you follow *curious* Twitter users. I say curious and not experts or thought leaders. The so-called experts and thought leaders broadcast their one-dimensional views of topics. On the other hand, curious users read widely, curate the best links, and distill the ideas. Power users do a lot of groundwork for you.

Here is the step by step process of finding such curious, power users who teach me the most.

![Finding Twitter Users](https://cdn.olai.in/jjude/find-twitter-users.jpg)

### Search for a topic

Everything starts with my curiosity on a topic. For this post, I'm going to consider my latest interest - sketch noting.

When I search for a topic, Twitter shows relevant users as search results. I open the Twitter profile of every user appearing in the result.

### Look at user's profile

The first thing I read is the user profile. Do they boast about their affiliations, or do they say how they help others? Is there a social proof? Do they mention what their primary interests are? 

When I search for "Sketching," one of the interesting profile I end up is [Presto Sketching](https://twitter.com/prestosketching). Why? It ticks all the boxes that tickle the interests.

* The profile bio says, "…help you draw better and think better." ✅
* That account is followed by [Dave Gray](https://twitter.com/davegray), whom I follow and like. ✅
* They mention "visualthinking, sketchnoting and sketching" as their primary focus. ✅

Once profile information is interesting, I dig further.

### Look at user's tweets and replies

Some users are just broadcasters. They go on talking about their view of the topic, and nothing else. They neither answer questions nor interact with others. You don't learn anything from such bores.

Engaging users are lovely, and they keep their feed lively. They share their creations but also retweet others. They "talk" to others. They answer questions in their field.

Presto doesn't seem to answer questions or comment on other's tweets. But the feed is neither a boring monologue nor senseless retweets of all sketches. Another ✅.

### Look at user's favorites

Next, I look at what they like. Are they narcissistic? Do they like only those tweets that praise them or talk about them? Have they liked tweets by others to check out later? Are these favorites from a single person or a diverse group?

Presto seems to favor the democratic party in the US. They have like many of tweets promoting Joe Biden's candidacy for president. Even though it is an agency, it might be a one-person company, and (s)he is bringing their whole personality to Twitter. I am OK with it. In fact, I like it. Another ✅.

### Follow practitioners rather than observers (consultants)

Twitter attracts people who walk around swimming pools making comments on everyone's actions inside the water but never really jumping into the pool themselves. I would instead follow people who ski before me, fall, get up, and ski again while providing a real-time commentary so that others can learn.

### Next Actions

* Read [How I Use Twitter As A Learning Tool?](/learn-via-twitter/)
* Follow me on [Twitter](https://twitter.com/jjude/)
* Comment about this article on Twitter
