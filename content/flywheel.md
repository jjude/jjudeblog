---
title="Building a personal flywheel"
slug="flywheel"
excerpt="Understanding the power of the flywheel is essential for building a successful career or business."
tags=["biz","wins","coach","flywheel","frameworks"]
type="post"
publish_at="30 Jun 21 06:00 IST"
featured_image="https://cdn.olai.in/jjude/flywheel.png"
---
<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless src="https://share.transistor.fm/e/5196d29f"></iframe>

## Flywheel

Have you seen a flywheel? Like me, if you come from a village, most likely to have seen it in the temple there. When they pull the temple rath during the annual festival, flywheel comes into play. 

Flywheels are gigantic wheels mounted horizontally on an axle. 

They are difficult to rotate at first. As the momentum builds up, the momentum will sustain the rotation of the flywheel. 

## Flywheels in Business

Jim Collins wrote much about the usage of the flywheel in business. Amazon executives embraced the concept and put it into practice. They used it to build a formidable business. Their flywheel goes like this:

They created an online market with a large number of low-priced products. Low prices attracted more customers. Amazon used this huge traffic to attract many merchants to offer low-priced products on its website. 

What happens when you have an online marketplace with a variety of products at low prices? The traffic increases.

Amazon's flywheel is made of low-priced products, traffic, and high-volume transactions.

A successful business is like a flywheel in motion. By increasing 'x', they increase 'y', which in turn increases 'z', which flows back to increasing 'x' again. Once the flywheel starts rotating, it becomes significantly easier to generate sustainable, profitable businesses. The challenge is in identifying the related factors and weave them into one wheel.

## Personal Flywheel

Is it possible to build a flywheel that integrates your personal and professional life? Yes, of course.

Here is how my personal flywheel looks like:

- I develop insights by reading a variety of books
- I then share those insights for free on social media platforms, attracting more people to me
- As more people come in with a variety of questions, I gain more insights by answering these questions
- When more people come to me, I have more opportunities to coach them, which leads to greater wealth
- As I gain wealth, I gain more insight into what works and what doesn't. 
- Finally, self-control is the axle that unites them all.

![Gravitas WINS flywheel](https://cdn.olai.in/jjude/flywheel.png)

I've gone a step further. I have built flywheels for all of these things - like insights building, wealth building, and network-building.

## One Small Step

Understanding the power of the flywheel is essential for building a successful career or business. If you understand flywheels and their impact, you can build one for yourself.

Gravitas WINS, my coaching program, is based on the concept of the flywheel. WINS, as you know, stands for wealth, insights, network, and self-control. As I mentioned earlier, more insights lead to a richer and more diverse network, which in turn opens up more opportunities and hence wealth. I can spin this wheel in a profitable way for a long time if I maintain these factors with self-control as an axle.

If you are interested in building your career or business in a systematic way, you should consider taking the course. Young entrepreneurs as well as a chief digital officer from large financial companies have taken the course and benefited from it. All their testimonials are available on [the website](https://www.youtube.com/watch?v=bFke3nY1XPA&list=PL538B_xQcbX6mS-6TyXsefj0fVaWBdDv7). 

I start a new batch on July 11th. If you are interested to join, send me an [email](mailto:joseph@jjude.com).

## Quote To Ponder

The flywheel, when properly conceived and executed, creates both continuity and change. - Jim Collins

## Continue Reading

* [Building In Public, Jeff Bezos Style](/build-in-public-bezos/)
* [What Studying For Exams Taught Me About Decision Making](/decision-process/)
* [Building Your Career - Apple Or Amazon Way](/apple-amazon-career/)