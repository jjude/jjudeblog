---
title="Quotable quotes from 'A Game Plan For Life' by John Wooden"
slug="quotes-from-a-game-plan-for-life"
excerpt="Quotes I liked from A Game Plan For Life."
tags=["books","wins","coach","parenting"]
type="post"
publish_at="06 Aug 10 16:30 IST"
featured_image="https://cdn.olai.in/jjude/quotes-from-a-game-plan-for-life-gen.jpg"
---


Quotes I liked from A Game Plan For Life.

- Success is peace of mind, which is a direct result of self-satisfaction in knowing you made the effort to become the best of which you are capable.
- There are tremendous lessons to learn from the great people of the world, but there are just as many to be learned from the quiet people around us.
- If there's a secret to success, it just might be little things done well.
- Practice simplicity with constant repetition.
- Your not-doing can speak as loudly as your doing.
- Some lessons are learned more subconsciously than consciously.
- Humiliation is not the same thing as correction: One attacks the person; the other attacks the problem.
- ...mentoring often involves telling people what they need to hear, rather than what they want to hear.
- ..'mentor' is both a verb and a noun. It is simultaneously something you do and something you are.
- What matters is what you do with the lessons those mentors teach you.
- It is the principles you should study, not the specific situation.
- ...if your principles are solid, you can approach any opportunity with confidence.
- ... patriotism should challenge ideas, not disrespect people.
- ...fame, fortune, and power are not success, that the four things mankind craves the most are freedom, happiness, peace, and love, none of which can be obtained without first giving it to someone else.
- ...a man of his word will be more effective leader than a man who derives his power from fear, empty promises, or inconsistent policies.
- My father used the immediate to prepare us for the future.

## Continue Reading

- [Build on the rock for the storms are surely coming](/build-on-rock/)
- [Book Review – A Game Plan For Life by John Wooden](/game-plan-for-life/)
- [Writing my obituary](/my-obituary/)
