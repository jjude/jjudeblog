---
title="Podcasting In 2021"
slug="podcasting-2021"
excerpt="All the tools you need to get started with podcasting in 2021."
tags=["wins","sdl","produce"]
type="post"
publish_at="09 Jun 21 18:04 IST"
featured_image="https://cdn.olai.in/jjude/podcast-recording.jpg"
---

I started a podcast called [Gravitas WINS](/podcast/) recently. So far released 12 episodes.

From the first episode, I've iterated on almost everything - recording techniques, tools, time of release ...

In this post, I list all the tools I use for podcasting.

### Planning In Obsidian

![Planning a podcast in obsidian](https://cdn.olai.in/jjude/podcast-planning.jpg)

All planning happens in [Obsidian](https://obsidian.md/).

- topics to create
- episode script
- podcast checklist

Obsidian is my digital brain. Everything goes there.

If I've to edit on mobile, I use [1Writer](https://1writerapp.com/).

Once content is ready, I read it aloud once or twice.

- Are there "big" words that I could mispronounce?
- Do I sound like talking to a friend or as corporate mumbo jumbo?
- Is the speaking flow naturally from start to end?

I am now ready to record the episode.

### Recording & Edit In GarageBand

![Recording a podcast in Garage Band](https://cdn.olai.in/jjude/podcast-recording.jpg)

Just before I record, I do some facial and voice exercises just to make sure I sound ok on the podcast.

I repeat phrases like,

- laba laba laba
- peeli pali poli

This is the part where my sons and wife tosses a coin if I'm still sane. 😱 😂

I record using [GarageBand](https://www.apple.com/in/mac/garageband/). I don't use any special settings. "Narrative Vocal" works wonders for me.

I use ATR 2100 USB with a pop filter to record the podcast.

### Hosting With Transistor.fm

![Transistor.fm statistics dashboard](https://cdn.olai.in/jjude/podcast-hosting.jpg)

I host with [Transistor FM](https://transistor.fm/) .

I've been following [Justin](https://justinjackson.ca/) ever since his crazy experiements of "100 things to do in a year".

For me creating a podcast is a crazy experiment. So hosting on his platform seemed just about right.

Also I'm part of his [Megamaker](https://megamaker.co/) club 😜

### Cover Art With Sketch app

![Podcast episode media creation with Sketch App](https://cdn.olai.in/jjude/podcast-sketch.jpg)

I use [Sketch](https://www.sketch.com/) to create podcast cover and episode promotional images.

I live in India and 4 hour power-cuts are common. I need something that I can work even offline. 

Also sketch has a great "pay once and use forever" pricing model.

### Post Promotion

![Promoting podcast episode on social media](https://cdn.olai.in/jjude/podcast-promotion.jpg)

How do I promote my podcasts?

As like my blog, only friends and family listen to them (more friends than my family). 

I share as whatsapp status, LinkedIn story, LinkedIn post, Twitter story, Tweet...

Still early days to get good traffic.


## Continue Reading

* [Gravitas WINS podcast](/podcast/)
* [Build An Ecosystem For Learning](/build-an-ecosystem-for-learning/)
* [Documenting Your Decisions](/documenting-your-decisions/)