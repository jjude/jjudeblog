---
title="How to find your element?"
slug="finding-your-element"
excerpt="Finding your Element is a quest to explore what lies within you; and opportunities in the world around you"
tags=["wins","insights","video"]
type="post"
publish_at="27 Jan 21 09:00 IST"
featured_image="https://cdn.olai.in/jjude/finding-your-element-gen.jpg"
---


Some people love what they do and couldn't imagine doing anything else. They identify with what they do because they so love it. It feels most natural, and they are at their authentic self when they do it. Then there are others who get on with their day and wait for the weekend. They don't enjoy what they do, and life is a drag.

Finding your Element is about discovering your talents and passions. Sir Ken Robinson [explains][1] how we can find our Element and the difference it makes in our lives.

What follows are my notes from this talk.

### Natural Talents

We all have natural talents, but just like the natural resources, they are hidden, and we need to discover them. Once found, you have to refine and cultivate those talents, much like the resources. When people don't find out what lies inside them, they conclude that they are not talented.

### Reasons to find your Element

There are three reasons to find your Element.

- **Personal**: Finding your Element is essential to personal fulfillment. If you love what you do and you are good at it, you never feel bored. If you do what resonates with your Element, you might be physically exhausted after a long day of work but feel you're still surfing on positive energy. On the other hand, if you do things you don't care about, you might be physically fine but depressed.
- **Social**: Many people are disengaged from what they do. When they discover what gives their life a purpose and meaning, they attain a different quality to their lives. When everyone finds their Element, our collective lives reach higher quality.
- **Economic**: We spend most of our lives at work, and hence we should do work that is fulfilling. The Element gives us a sense of direction. Once we have the direction, we stop hopping mindlessly from one job to another, trying to find satisfaction.

### Principles for finding our Element

- **Your life is unique**: You may be like other people, but you are unique at this moment in human history. You may look like your father and inherit many of his mental abilities. Still, because of cultural differences, you also differ from him. Nobody has ever lived your life, and nobody else ever will.
- **You create and recreate your life**: We have the ability to imagine things that aren't present; we can visit the past; we can anticipate the future, and we can enter the world of others and empathize with them. With our imagination, we can create and re-create our lives.
- **Your life is organic, not linear**:  Your life is a constant process of improvisation. You compose your life as you go. No one's life works out exactly as planned. You create your life as you move through it and recreate it according to the opportunities you see around.

### How to find your Element?

- **Passion**: The word passion has changed its meaning. What it originally meant was suffering and endurance, as used in "Passion of Christ." Now it means the things you love to do, things that fulfill you, the things that give you a sense of deep reward. Finding your Element is a journey of suffering and endurance to a sense of fulfillment. If you avoid suffering, you avoid finding your Element.
- **Aptitude**: Our culture has a narrow conception of aptitude, and that idea is promulgated through our educational systems. We confuse all forms of intellectual ability with IQ or academic ability. A lot of people have marginalized talents. Since society doesn't appreciate these marginalized talents, they conclude they are not talented. To find your Element, you need a bigger conception of aptitude.
- **Behaviour**: People who take action are the most fulfilled are the ones. They apply their aptitude with a passion to the opportunities they find. Thus they create newer opportunities that were not available to them earlier. They take the first step, without knowing the entire journey, and respond to what emerges in front of them. 

### Quest, not a journey

> We don't see the world as it is, we see it as we are - Anais Nin

**Finding your Element is a quest, not a journey**. Going from New York to San Francisco is a journey, an activity with a known outcome. A quest is an act of discovery. In this quest, you may sink or end up on a shore you hadn't anticipated, which turns out to be a better option than the one you had in mind.

A quest to find your Element is **travel on two roads at once**: a peek into your inner world and an exploration of the outer world.

We live in a world of terrible noise, and that noise distracts us from knowing ourselves. When we don't know ourselves, we can't find talents hidden deep inside us.

If you live a closed life with repetitive experiences, it is unlikely you'll have new experiences that may trigger a discovery of yourself. When you discover yourself and be bold enough to try new things, doors open that you didn't even realize were present.

If we make a determined effort to find our Element, new possibilities will open up which will enrich not just our own lives but the lives of all those around us.

> Instead of standing on the shore and proving to ourselves that the ocean cannot carry us, let us venture on its waters to see. - Pierre Teilhard de Chardin


[1]: https://www.youtube.com/watch?v=17fbxRQgMlU 

## Continue Reading

* [Why Some of us Don't Have One True Calling](/video-one-true-calling)
* [3 Daily Needs](/video-3-daily-needs)
* [Approximately correct](/approximate)
