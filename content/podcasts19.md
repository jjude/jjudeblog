---
title="Podcasts I listen"
slug="podcasts19"
excerpt="Listen to challenging words. It will energize your day."
tags=["sdl","consume"]
type= "post"
publish_at= "29 Sep 19 06:00 IST"
featured_image="https://cdn.olai.in/jjude/podcasts-i-listen.jpg"

---

"Reading is more efficient when at rest. Audio is more efficient when in motion." - [naval](https://twitter.com/NavalBot/status/1175499995761577984)

![Podcasts I listen](https://cdn.olai.in/jjude/podcasts-i-listen.jpg)

I listen to podcasts when I drive. Inspiring and challenging words on the way to work energize me to face the day. When I return, I usually listen to music, but sometimes listen to a captivating long episode that I didn't complete in the morning.

My interests vary, hence the list of podcasts I listen vary too. Here is my current list:

- [Akimbo](https://www.akimbo.me/) by Seth Godin - Having read his books and blogs, I wondered if there would be anything different in his podcasts. I was wrong. He manages to challenge my thinking in every episode.

- [The Uncomfortable Truth](https://overcast.fm/itunes1208575899/alan-weiss-the-uncomfortable-truth) by Alan Weiss - Nobody talks the truth. Everyone lays layers and layers of words to be politically correct. Not Alan. In 5 min episodes, Alan tels you the uncomfortable truth in almost all spheres of life.

- [Tim Ferris Podcast](https://tim.blog/podcast/) - Tim brings exciting personalities to his podcast and has an insightful conversation with them. Each episode is at least an hour-long, but worth every second. There are many favorites, but if I have to pick, I will choose the ones with [Derek Sivers](https://tim.blog/2015/12/14/derek-sivers-on-developing-confidence-finding-happiness-and-saying-no-to-millions/) and [Jim Collins](https://tim.blog/2019/02/18/jim-collins/).

- [The seen and the unseen](https://seenunseen.in/) by Amit Varma - Recently I got interested in public policy. Amit is the man to listen to when it comes to public policy, especially in India. Every public policy has two kinds of effects: intended and visible; and unintended and invisible. He examines these seen and unseen effects in this podcast.

- [Design Tomorrow](https://designtomorrow.co/) by Christopher Butler - Chris doesn't talk about what is trendy, but how we can and should create a better tomorrow via design, technology, and being human.

- [Born to Impact](https://overcast.fm/itunes1437958494/born-to-impact) by Joel Marion- A life of purpose… A life where you're making a difference in your family, in your relationships, in your career, and in the world around you. Listen to the first two episodes; and the [Networking masterclass](https://overcast.fm/+O9I_KioqA) with Travis Chappell. All three are amazing episodes. If you don't like those episodes, you won't like the podcast.

- [Ray Edwards Show](https://overcast.fm/itunes263185014/ray-edwards-show) - Ray is a fantastic copywriter. In this podcast, he discusses the business of copywriting and copywriting for a business. Mind you; he mixes his faith in Christ into every episode. That might put off some.

- [Modern CTO](https://overcast.fm/itunes1325942361/modern-cto-with-joel-beasley) by Joel Beasley - ModernCTO is the place where CTOs hang out. Joel brings CTOs of startups and established companies to discuss their learning methods, challenges they face, and the metrics they track.

- [North Start Podcast](https://www.perell.com/podcast/) by David Perell. Like Tim Ferriss, David brings a variety of interesting personalities and have a long conversation with them.

- [Elevation](https://overcast.fm/itunes216015753/elevation-with-steven-furtick) with Steven Furtick. I have been reading the Bible for twenty years now. But Steven comes up with amazing practical insights even from the familiar Biblical passages. I listen to this podcast, whenever I'm feel discouraged.

- [The Knowledge Project](https://overcast.fm/itunes990149481/the-knowledge-project-with-shane-parrish) with Shane Parrish. Another podcast in the likes of Tim Ferriss and David Perell. Shane worked with Canadian spy agency before embarking on a journey of urging curious minds to "upgrade yourself." He focuses on mental models that are useful for life and work.

- [a16z](https://overcast.fm/itunes842818711/a16z)- A16Z is a private equity firm founded by the legendary entrepreneurs Marc Andreessen and Ben Horowitz. They invest in e-commerce, education, enterprise IT, and gaming ventures. In these episodes, A16Z team discusses trends in the areas they invest in.

- [Naval](https://overcast.fm/itunes1454097755/naval) - Naval is an entrepreneur and a philosopher. He [tweetstormed](https://twitter.com/naval/status/1002103360646823936) recently on "How to Get Rich (without getting lucky)." This podcast is an extension of the tweetstorm.

- [Predictable Revenue Podcast](https://overcast.fm/itunes1233372004/predictable-revenue-podcast) with Aaron Ross & Collin Stewart - As a chief technology officer of an IT services company, I get into sales calls often. Aaron and Collin talk about selling services and creating sales playbooks.

- [Anecdotally Speaking](https://overcast.fm/itunes1336755633/anecdotally-speaking) with Shawn Callahan. This podcast is an extension of the book he wrote - Putting stories to work. Shawn and his guests discuss nuances of stories at workplaces.

I use [Pocket Casts](https://www.pocketcasts.com/) app to listen to podcasts. If you are on iOS, [Overcast](https://overcast.fm/) is a fantastic choice. Both podcast clients allow importing podcast feed as an OPML file. [Here](https://www.dropbox.com/s/htk310xgqvq2x0w/podcasts_opml.xml?dl=0) is the OPML file of the podcasts I listen. Feel free to use it.

What are the interesting podcasts you listen? Tell me on [twitter](https://twitter.com/jjude).

_If you are looking for another fantastic list of business related podcasts, head over to [13 of The Best Business Podcasts You Should Listen To](https://biz30.timedoctor.com/best-business-podcasts/) by Time Doctor._

_I got the idea for this post because [Laxman Karthik](https://twitter.com/iamlaxmanK) asked for [favorite podcasts](https://twitter.com/iamlaxmanK/status/1168022093100769281)_
## Continue Reading

- [Earlier list of Podcasts I listened](/2015-02-podcasts)
- [Self-Directed Learning](/sdl/)
- [Build An Ecosystem For Learning](/build-an-ecosystem-for-learning/)
