---
title="What Are The Top Leadership Challenges?"
slug="leader-challenges"
excerpt="All leadership challenges can be boiled down to just two"
tags=["coach","wins"]
type="post"
publish_at="25 May 21 07:46 IST"
featured_image="https://cdn.olai.in/jjude/leaders-qualities.png"
---

<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless src="https://share.transistor.fm/e/cc4a7e34"></iframe>

If I asked you to think of top leadership challenges, what are the challenges that come to your mind?

May be,

- Framing an inspiring vision? Or
- Hiring & firing?
- Building a culture where employees enjoy doing their best work?

I agree these are all leadership challenges. But if you analyze these and countless other challenges that leaders face, you will arrive at two primary challenges:

They are **competence** and **confidence**.

![What are leader's qualities](https://cdn.olai.in/jjude/leaders-qualities.png)

Let me talk about competence first. 

### Competence

As leaders, we go through different stages in our careers. 

Say you start a company with two employees. You become successful and gain customers. You start to hire. Every time your employees’ count doubles you have a different company. You need different competencies to lead that company. 

Or say you are an employee in the software industry. You start as a developer. In some years you become a project manager and then after few more years, you become a business unit head. 

**At every stage, you need a different set of skills**. As a developer, you need competencies to manage only your work. As a project manager, you need people competency. But as a business unit head, you probably need financial skills. 

That’s why Marshall Goldsmith wrote a book called, **what got you here won’t get you there**. 

Now let’s talk about the next challenge - confidence.

### Confidence

You might have every competence to play your role. Still, if you are not bold enough then you’ll fail as a leader.

If you are promoted, and now have to manage your peers, you might feel you can’t get work done from them. 

Or the peer you hired from another company might intimidate you with their experience or knowledge.

Or you might get overwhelmed with all the responsibilities that come with the new role. 

In every case, your teammates will spot your lack of confidence and will play their part to make you confused and more insecure. 

To be a great leader and to continue to grow as a leader, you need both competence and confidence.

The greatest disaster for a community, company, and country is to be led by a **sheep in lions cloth**. Such a leader is generally incompetent but hides their incompetency behind pumped-up confidence. That never ends well. 

Do you agree with these challenges? Let me know your comments via [email](mailto:website@jjude.com) or social media.

### Quote To Ponder

_"The problem with the world is that the intelligent people are full of doubts, while the stupid ones are full of confidence." ― Charles Bukowski_

## Continue Reading

* [Dichotomy of contentment and ambition](https://jjude.com/contentment-and-ambition)
* [Future Of Jobs Is Partnership Of Experts](https://jjude.com/future-of-jobs)
* [How To Be Effective As A New Manager?](https://jjude.com/how-to-be-effective-as-a-new-manager)