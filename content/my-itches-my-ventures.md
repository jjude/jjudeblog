---
title="My Itches, My Ventures"
slug="my-itches-my-ventures"
excerpt="Is Paul Graham right? Is scratching your own itch the best way to build a product? I'm sure to find the answer soon."
tags=["startup","coach"]
type="post"
publish_at="16 Sep 15 23:57 IST"
featured_image="https://cdn.olai.in/jjude/2015-traction-tools.png"
---
When I started on the entrepreneurial journey, I built a product from a borrowed idea. No wonder it [flopped][5].

For a while, I was searching for a brilliant idea for a product. Then I happened to read [Paul Graham][2] and [Drew Houston][1] on this subject. They say, **scratching your own itch is the best way to create the best product**. Drew Houston proved it by building a billion dollar business by scratching his own itch.

I started looking inward. I wrote down some of the annoying problems I face. From that list, I left out problems I face once in a while. I focused only on problems I face often and those I thought I can solve with technology. I did this exercise many times. Thus was born my first product.

I love to write. I remember sending my pocket-money to a writing course when I was in 5th class. But the application returned. I don't recollect the reason for rejection, but my tiny mind couldn't fathom the rejection. I tore that application. Yet, I continued to write. I still write.

Sometimes I like to share what I write. So I blog. I tried most of the blog engines - Drupal, Nucleus, Mamboo, and of course the king of blogging tools, Wordpress. Though I am trained as a software engineer, like many of the Indian kids of the 70's, I don't enjoy fumbling around the technical side of blog engines. Worst is blog going down, when one of the post become even slightly popular.

**I love to write. I like to share. But I hate to fiddle with blog systems.**

I scratched my itch. I created a blogging tool that brings the best of dynamic blog engines like [Wordpress][7] and the static engines like [Jekyll][8]. [Olai][6], the blogging tool that I created, supports XMLRPC API, so I can continue to blog using blog editors; it also generates static pages and transfers them to [Amazon S3][9]. Now I focus on writing and sharing.

![Focus on blogging with Olai](https://cdn.olai.in/jjude/2015-09-olai.png)

Have you ever noticed the funny thing with scratching an itch? When you scratch one, you get another.

I'm introvert and shy. I hate speaking in public, in general. My nightmare, though, is asking a question in public. At times, I would gather courage and raise a question. I would rehearse, mentally, framing the question and it would look just fine. As soon as I would open my mouth, my tongue would become dry, my heart would beat faster, and my voice would become squeaky. **Can I engage with the speaker without making myself a spectacle?** When [Vijay Anand][10] posted an idea, in his Facebook group, about crowdsourcing questions at events, it resonated with me. It is a product I want and it is a product I could build quickly. I built it and called it [WiseCrowd][11].

![Crowdsource questions at events with WiseCrowd](https://cdn.olai.in/jjude/2015-09-wisecrowd.png)

Building a product is okay. It comes naturally for a software engineer. The tougher question is, how to market it?

The best book I found in this topic is by Gabriel Weinberg & Justin Mares, titled [Traction][12]. This book and other blog posts helped me understand the available marketing channels to promote [Olai][6] and [WiseCrowd][11]. But how do I start with them? What tools should I use?

Google came to the rescue. I spent lot of time on Google and [Product Hunt][13] looking for marketing tools. Googling takes you only so far. **Not every tool has a how-to guide. I was wasting time.**

Do you spot the itch again?

Whenever I start using a new tool, I take as many screenshots as possible with copious notes for each of them. I did the same with these tools. I'm putting them together as an e-book. But first, I will post these guides on my [blog][15]. You can track the progress from [Traction Tools For Startup][14] page.

![Traction Tools For Startups](https://cdn.olai.in/jjude/2015-traction-tools.png)

Is Paul Graham right? Is scratching your own itch the best way to build a product? I can't answer right now. I'm sure to find the answer soon.

[1]: https://news.ycombinator.com/item?id=9247
[2]: http://www.paulgraham.com/organic.html
[3]: http://startupquote.com/post/2497463236
[4]: https://gettingreal.37signals.com/ch02_Whats_Your_Problem.php
[5]: /what-i-learnt-in-a-year-as-an-entrepreneur/
[6]: http://olai.in
[7]: https://wordpress.com
[8]: http://jekyllrb.com
[9]: https://aws.amazon.com/s3/
[10]: http://www.vijayanand.name
[11]: http://wisecrowd.in
[12]: https://amzn.to/2JU2GQg
[13]: http://www.producthunt.com
[14]: /traction-tools-for-startups/
[15]: /
