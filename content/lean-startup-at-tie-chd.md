---
title="Notes from Lean Startup Session by TiE, Chandigarh"
slug="lean-startup-at-tie-chd"
excerpt="Key component of a startup is the feedback loop"
tags=["coach","startup","tiechd"]
type="post"
publish_at="19 Jul 15 09:17 IST"
featured_image="https://cdn.olai.in/jjude/2015-07-winshuttle.jpg"
---
TiE Chandigarh has been organizing discussion meets regularly under the banner of 'Startup Series'. Learn Startup was the topic for discussion yesterday (17th). It was organized at [Winshuttle](http://www.winshuttle.com) office in Chandigarh. Vishal Chalana, co-founder of Winshuttle talked about the principles behind Lean Startup and how he applies them at Winshuttle.

![Vishal at Winshuttle](https://cdn.olai.in/jjude/2015-07-winshuttle.jpg)

Here are my notes:

* What is a startup? A human venture where conditions on which you build a product is uncertain. Unlike a client project, you don't have specs for the product; you don't know if anyone would even buy your product.
* Validated learning is a key component of lean startup. We all say we learn. What is the empirical evidence to show that you learned? Without data, your learning is questionable. You progress when you can empirically show what you learned.
* How do you collect data as you develop? Do it as a proper experiment. Write down your hypothesis. Collect data to prove or disprove your hypothesis.
* With data you either pivot or persevere.
* Learning is the unit of progress in a startup. If you can measure your learning then you are making progress.
* Management and entrepreneurship seems opposite. Management is seen as boring, detailed process that one should follow; and entrepreneurship is generally seen as acting from gut. But if you don't manage your startup (learning) process, you will descend into chaos.
* What is waste in a startup process? Any effort that doesn't lead to a validated learning is a waste.
* If you write a line of code that customer doesn't use then it is a waste. This means, you should have in-built mechanism to measure usage of every feature in the product. [Vishal explained how Winshuttle has in-built mechanisms to measure usage of features in their product].
* Product roadmap is not important. Data collection is. The important factor in product development is the built-in feedback loop.
* You can perfect something all your life. But throw it before the customer and get feedback.
* Vision drives strategy; strategy drives products. Product continues to evolve (or change). Strategy changes when you pivot. Vision changes only when you change business.
* Pivot is change of hypothesis. You can pivot only when you can show with data that your hypothesis is wrong.

![Vishal on Lean Startup](https://cdn.olai.in/jjude/2015-07-vishal-quote.jpg)

As a [startup founder][1], I have read articles, watched videos, and listened to podcasts on lean-startup. So I was aware of the lean-startup process and I was following it in building [Olai][1], the blogging platform that I'm building. Like many startup founders, I'm also keen on learning. Yet, as I was listening to Vishal, I realized that I have not given enough importance to "feedback loop". Thanks Vishal for reminding about the importance of "feedback loop".

[1]: http://olai.in
