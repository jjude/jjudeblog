---
title="How I wrote 'Luck Loves The Paranoid'"
slug="my-writing-method"
excerpt="From an idea to a published article"
tags=["writing","wins"]
type="post"
publish_at="20 Apr 22 16:24 IST"
featured_image="https://cdn.olai.in/jjude/my-writing-method-gen.jpg"
---


For over a decade, I've been writing and publishing. In the beginning, there was only one person in the audience: me. I wrote to improve both my thinking and communication skills.

People now recognize me and want to read what I write. Still, I only have one audience - I imagine I'm speaking to a friend. That imagined friend changes depending on the topic I'm writing about, but I always imagine talking to a friend. 

Here is the method I use to write week after week. I'm explaining the method about my most recent post–[Luck Loves The Paranoid](/luck-loves-paranoid/).

I usually begin in one of two ways: I begin with a title that conveys what I want the readers to take away or I get a flash of an idea and develop it, in which case I defer writing the title until the end.

The first draft and outline is almost always written on a mobile walking around my home-office or nearby park. I use [1Writer](https://1writerapp.com/) on iOS and [Obsidian](https://obsidian.md/) on Android. I use Obsidian on Mac to expand from the outline.

In this instance, I began with a title. The title was "_Luck loves the pessimist_". I wanted to convey that pessimism is good; what I wanted to convey was that you can attract luck only if you are pessimistic about the future. Only a pessimist can imagine all the possible outcomes. Once you've compiled a list of potential pitfalls, you can plan to fill those gaps. At least, that was my idea when I began. 

Once I had a heading, I began quickly filling in the page. Here's how it looked after I finished the first draft.

>- "What can go wrong?" is the first question I ask
>- CM quote, tell me where I am going to die so I won't go there
>- richard branson leasing flights - [[How Richard Branson started his virgin airlines]]
>- I am paranoid of losing
>- it comes from stock market investing
>- a pinch of pessimism makes your luck go around little longer
>- https://twitter.com/rishabh_grg/status/1513336158897733634

Each line indicates ideas I want to convey. Then each of these lines becomes a para. For example:

> "What can go wrong?" is the first question I ask whenever I am involved in any project.  The idea is to unearth any blindside to the enthusiasm I show for the project. Once I know what can go wrong, then I can put in enough guardrails and guiding lights so the team can go fast without bumping on the sides.

As I completed the first para, I realized "pessimist" is not the right word to convey what I wanted to say. 

So I took a pause, got up from my seat, and walk around the working table. I got a better word - paranoid - Luck loves the paranoid. I liked it.

Just a simple word makes a hell of a difference in what you want to say. A correct word is the difference between a coffee cup and a cup of coffee.

In the case of this article, I was able to complete it in a single sitting. However, it usually takes several sittings. The usual reason is that I became distracted while searching for information on the Internet. 

Once I have a rough draft of the entire article, time to edit.

When I write the first draft, I write in short sentences and there is no music in the writing. To bring rhythm to what I write, I rely on three apps:

- [Wordtune](https://www.wordtune.com/)
- [QuillBot](https://quillbot.com/)
- [Grammarly](https://www.grammarly.com/)

The AI engines powering these tools astound me. Take a look at what Wordtune and Quillbot can do. They act as an editor for what I write because they keep the context from previous sentences. 

I may not use all three apps all the time. I choose Quillbot or Wordtune. I use Grammarly for the final edit.

After all of the edits, I take another look at the heading. I want a heading that says everything I want to say. I'm not working on a novel. Because my readers are busy, I want them to understand what they will get as soon as they glance at the headline. I don't want anyone to be duped into reading what I write.

The blog runs on a custom engine I developed in Golang. It generates static HTML files which are hosted on Gitlab.

To recap:
- start with a theme
- write broad outlines
- expand each outline into one or more paragraphs
- edit each para with Quillbot or Wordtune
- edit the entire article with Grammarly
- publish

I hope my writing process helped you. If so, please leave them in the comments section below. 

## Continue Reading

* [How To Become A Writing Lion?](/writing-lion/)
* [Language Shapes Your World](/words-shape-world/)
* [Fame Or Fortune](/fame-fortune/)
