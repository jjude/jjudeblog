---
title="'Good Sales Skills Are Same As Good Life Skills' With Gaganpal Singh"
slug="gaganpals"
excerpt="The essential skills needed for thriving in sales and in life."
tags=["sales","gwradio"]
type="post"
publish_at="21 Jul 21 07:13 IST"
featured_image="https://cdn.olai.in/jjude/gaganpals-gen.jpg"
---

My friend Gaganpal joins me for this episode of [Gravitas WINS conversations](/podcast/). We talk about how his college professor shaped early part of his career; three essential skills needed to thrive in sales; and difference between sales in for-profit companies and social sector organizations. Enjoy the conversation.

<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless src="https://share.transistor.fm/e/0c07e9ee"></iframe>

What you'll learn:

- How college professors shape early part of our career
- Three essential skills needed to thrive in sales
- How the same skills are useful for a meaningful life
- The need for daily reading for sales
- Difference between sales in for-profit companies and social sector organizations

Hope you enjoy the conversation.  

### Watch on YouTube

<iframe width="560" height="315" src="https://www.youtube.com/embed/brJ8IZSwxtU" title="Good Sales Skills Are Same As Good Life Skills" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Connect with Gaganpal

- Twitter: https://twitter.com/orangeturban
- LinkedIn: https://www.linkedin.com/in/gaganpal-singh/


## Connect with me
- Twitter: https://twitter.com/jjude
- LinkedIn: https://www.linkedin.com/in/jjude/
- Website: https://jjude.com/
- Newsletter: https://jjude.com/subscribe/
- Youtube: https://youtube.com/gravitaswins
- Email: podcast@jjude.com
- Executive Coaching Program: https://gravitaswins.com

## Your feedback counts

Thank you for listening. If you enjoy the podcast, would you please leave a short review on Apple podcast or on YouTube? It takes less than 60 seconds, and it really makes a difference in finding this podcast. And it boosts my spirits. 😀

I would like to hear what resonated the most with you in this episode. Either you can email me or share it on social media and tag me. Thank you for your support.

Have a life of WINS.

## Amazing tools I use:
- I record all the remote interview with [Riverside.fm](https://riverside.fm/?via=joseph-jude)
- I use [Transistor.fm](https://transistor.fm/?via=jjude) for hosting these podcast episodes

_Gravitas WINS Radio is produced by [Joseph Jude](https://jjude.com/). Be sure to subscribe on [Apple Podcast](https://podcasts.apple.com/in/podcast/gravitas-wins-radio/id1562802449), [Spotify](https://open.spotify.com/show/7LOBjAyjBkechMC0xMIj9E),[Google Podcast](https://podcasts.google.com/feed/aHR0cHM6Ly9mZWVkcy50cmFuc2lzdG9yLmZtL2dyYXZpdGFzLXdpbnMtcmFkaW8=),[YouTube](https://www.youtube.com/gravitaswins), or the podcasting app of your choice._

Please check out other episodes of [Gravitas WINS Radio](/gwr-themes/). For easy consumption, they are categorized by themes. If you are a fellow podcaster, you might be interested in [How I Podcast](/how-i-podcast/) or [tools I use](/i-use/).

## Continue Reading

* [Mastering sales as a new CTO](/cto-sales/)
* ['Digital Sales For Startups & SMEs' with Roshni Baronia](/roshni-digital-sales/)
* [Should You Run That Extra Mile to Make a Sale?](/extra-mile-for-sale/)
