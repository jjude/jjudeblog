---
title="Creativity - Alchemy or Science?"
slug="creativity-alchemy-or-science"
excerpt="Interview of Craig Wynett, Chief Creative Officer, Procter and Gamble"
tags=["video"]
type="post"
publish_at="14 Feb 12 16:30 IST"
featured_image="https://cdn.olai.in/jjude/creativity-alchemy-or-science-gen.jpg"
---

Interview with Craig Wynett, Chief Creative Officer, Procter and Gamble

<iframe width="480" height="360" src="http://www.youtube.com/embed/ZLBJ9pda7TA" frameborder="0" allowfullscreen></iframe>

**Insights from the interview**:
1. If a pilot would announce, "I'm the most creative pilot, you would grab your bags and leave";
2. Thinking is expensive metabolically, which means most of the decisions are automatic. What is interesting is what drives that automatic decisions;
3. P&G has a strong corporate culture with strong individuals. In most corporates, either one dominates.

