---
title="Dealing with Dad Anxieties "
slug="dad-anxieties"
excerpt="Anxiety is part of life. How can we manage it as dads?"
tags=["gwradio","parenting"]
type="post"
publish_at="27 Feb 24 06:00 IST"
featured_image="https://cdn.olai.in/jjude/dad-anxity.jpg"
---
Today in "Being a dad" series, KK and I are going to talk about parental anxiety from Dad's point of view.

Anxiety is part of life. We can't avoid it. But we should learn to manage it, otherwise it can damage our families. KK and I are going to talk about trigger points for anxiety and our coping mechanisms of anxiety.

Let me set some expectations here: I've two boys. KK has a son and a daughter. They are still young. Our kids are still in school going ages. They have not appeared for board exams, we have not stood in line for hours for college admissions, we have not handled the tension of marriage, and so on. 

And we are not counsellors. We are not telling you how to handle anxiety in all its different forms.

Then why are we doing it? To paraphrase KK, this is an exercise for our younger and older selves. We wish someone taught us what we learnt; or we want to look back through the journey. In a way this is a documentary of our journey of being a dad.

Hope you find it interesting.

If you have any questions or comments, feel free to send them to either of us.

<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless="" src="https://share.transistor.fm/e/d2a3f3d7"></iframe>

## What you'll hear

- Trigger points for anxiety
- Impact of society
- Anxiety & Homeschooling
- Scenario Planning
- Living in Metro vs Tier-II cities
- Ambition vs Contentment
- On being a perfect dad
- Summary

## Edited Transcript
_coming soon_

## Watch on YouTube
<iframe width="560" height="315" src="https://www.youtube.com/watch?v=aXmCYD7cWJE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

## Connect with KK
- LinkedIn: https://www.linkedin.com/in/storytellerkrishna/

## Connect with me
- Twitter: https://twitter.com/jjude
- LinkedIn: https://www.linkedin.com/in/jjude/
- Website: https://jjude.com/
- Newsletter: https://jjude.com/subscribe/
- Youtube: https://youtube.com/gravitaswins
- Email: podcast@jjude.com
- Executive Coaching Program: https://gravitaswins.com

## Your feedback counts

Thank you for listening. If you enjoy the podcast, would you please leave a short review on Apple podcast or on YouTube? It takes less than 60 seconds, and it really makes a difference in finding this podcast. And it boosts my spirits. 😀

I would like to hear what resonated the most with you in this episode. Either email me or share it on social media and tag me. Thank you for your support.

Have a life of WINS.

_Gravitas WINS Radio is produced by [Joseph Jude](https://jjude.com/). Be sure to subscribe on [Apple Podcast](https://podcasts.apple.com/in/podcast/gravitas-wins-radio/id1562802449), [Spotify](https://open.spotify.com/show/7LOBjAyjBkechMC0xMIj9E),[Google Podcast](https://podcasts.google.com/feed/aHR0cHM6Ly9mZWVkcy50cmFuc2lzdG9yLmZtL2dyYXZpdGFzLXdpbnMtcmFkaW8=),[YouTube](https://www.youtube.com/gravitaswins), or the podcasting app of your choice._

Please check out other episodes of [Gravitas WINS Radio](/gwr-themes/). For easy consumption, they are categorized by themes. If you are a fellow podcaster, you might be interested in [How I Podcast](/how-i-podcast/) or [Tools I Use](/i-use/).

## Continue Reading

- [How our fathers, society, and peers shape our fatherhood](/fatherhood-influences/)
- [How we learn about fatherhood?](/learn-to-be-father/)
- [Building family culture](/family-culture/)