---
title="Book Review - Making India Work"
slug="making-india-work"
excerpt="William Bissell, CEO of FabIndia, diagnoses reasons for current state of affairs in India and prescribes solutions."
tags=["books"]
type="post"
publish_at="15 Aug 10 16:30 IST"
featured_image="https://cdn.olai.in/jjude/making-india-work-gen.jpg"
---

Happy Independence Day!

When Indians get frustrated with the corrupt government system, lucky ones migrate to the West, unlucky ones resign to their fates, courageous ones attempt at least a delta change, and the rich produce Bolloywood movies. But the CEO of [FabIndia](http://www.fabindia.com/), William Nanda Bissell, has written a book.

In his book, _Making India Work_, Bissell is both diagnostic and prescriptive. His diagnosis comes from years of interaction with government officials as well as citizens of all classes. In his diagnoses, the current state of India is:

1) a large government with its citizens perceived as inherently criminals and dissidents;
2) a bureaucracy full of corrupt and incompetent officials;
3) where productive effort is heavily taxed and tax-payer's money squandered in inefficient developmental programs;
4) where abstract rights exists in letters with no tangible claim on any of the ideals; and
5) where crony and phoney capitalism thrives.

He identifies poverty, extremism and lack of entrepreneurism (because "the minimum return left after taxes would not justify the effort and headaches in running a business") as side-effects of the above root causes. He warns that if India continues in this path of profound mismanagement, deterioration as that of Zimbabwe is inevitable.

His diagnosis is pragmatic but his prescription is intellectual. His prescriptions are influenced by the [_Federalist Papers_](http://www.flipkart.com/federalist-papers-alexander-hamilton-james-book-0743487710?affid=INJoseph "Federalist Papers"), Bruce F. Johnston and Peter Kilby's _Agricultural and Structural Transformation_, James Scott's [_The Moral Economy of the Peasant_](http://www.flipkart.com/moral-economy-peasant-james-scott-book-0300021909?affid=INJoseph "The Moral Economy of the Peasant"), Hernando de Soto's [_The Mystery of Capital_](http://www.flipkart.com/mystery-capital-hernando-de-soto-book-0465016154?affid=INJoseph "The Mystery of Capital"), and [_Saving Capitalism From the Capitalists_](http://www.flipkart.com/saving-capitalism-capitalists-raghuram-rajan-book-0691121281?affid=INJoseph "Saving Capitalism From the Capitalists") by Raghuram Rajan (currently economic advisor to PM) and Luigi Zingales.

The framework he proposes is built around three principles:
1) Fair markets;
2) Right Sizing of the government; and
3) Operational Transparency.

He then goes on to elaborate these principles and in so doing proposes reformation of every branch of Indian administration. They include, among others, abolition of post of president and governor; restricting government to be an umpire and not a player. He admits that such extensive reform is possible only with rewriting of Indian constitution and to do so, bureaucrats, politicians and president have to "bite the hand that feeds them".

Bissell's flawless diagnosis and well thought-out prescriptions should be commended. But I don't agree with him on approach and method of his prescriptions.

It is common to attempt change through processes and frameworks. Because change-agents believe such high-level changes bring benefits to their constituents. But I have witnessed many change programs fail when approached with this mindset. It should be noted that change, however noble, can not be forced from top; it should come from within. To rephrase president Obama's words to Indian context, real results will not come from Delhi - they will come from the people.

Secondly such a revolutionary change, when it comes, will not come through peaceful way of winning electoral mandate. In India, freedom from colonial oppressors was largely bloodless. But freedom from post-colonial oppressive government system will not be bloodless.

Despite these differences, the book is an inspiring read. Give it a read.
<div style="color: #999999; font-size: 10px; line-height: 15px;">Disclosure of Material Connection: Some of the links in the post above are “affiliate links.” This means if you click on the link and purchase the item, I will receive an affiliate commission. Regardless, I only recommend products or services I use personally and believe will add value to my readers.</div>

