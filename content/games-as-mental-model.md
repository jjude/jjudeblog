---
title="Gaming as a mental model for successful business"
slug="games-as-mental-model"
excerpt="All mental models are wrong, but some are more useful than others. Looking at a company/product/service via a gaming lens may not be a comprehensive model, but it is a useful model."
tags=["coach","frameworks","startup"]
type="post"
publish_at="02 May 21 11:09 IST"
featured_image="https://cdn.olai.in/jjude/gaming-mental-model.jpg"
---

_These are my notes from the podcast, [Identifying Legendary Start-ups](https://www.joincolossus.com/episodes/15214417/buckley-identifying-legendary-start-ups) with [Josh Buckley](https://twitter.com/joshbuckley). It is a masterclass on gaming as a mental-model for investing in startups. Josh talks about other aspects of investing in the podcast, but for this article, I have limited to "Gaming mental model."_


![Identifying Legendary Startups](https://cdn.olai.in/jjude/gaming-mental-model.jpg)

### Gaming as a mental model
- gaming is one lens through which you can view products and companies
- all models are wrong, but some are useful
- This is not a complete model of the world, but certainly a useful one
- Launching a game is 1% of the work
- 99% of work happens post-launch

### Layers of successful games
- explore and optimize these three ideas to the extreme
- the game itself
    - only 30% of the game ecosystem
- community
    - chat room
    - a social network
    - a city within the internet
- shop
    - players spend hundreds, thousands, even millions of dollars on their lifetime

### Flow
- concept from psychology where it creates **a sense of immersion** where time, ego, and the world fades away
- best games bring you into the flow
- flow is synonymous with fun
- Flow is like ecstasy. It's exhilarating. Athletes call it the zone
- best games **create controlled spaces** that bring you into that sense of flow
- Twitter, Facebook, and Instagram are games, in that sense; 3 billion people are playing them actively

### Elements that make games/apps tick
- Frequency
    - without frequency player get apathetic or bored and fall out of the flow
    - there has to be frequent feedback within the game
- variable outcomes
    - People adapt very quickly to a predictable reward schedule. 
    - The term "maybe" is addictive, like nothing else out there.
- sense of control
    - People like to work. People like to work for reward. 
    - To stay in the flow, you need some sense of challenge. 
    - It's rewarding to build a piece of Ikea furniture

### Meta-game
- the abstraction layer above the game
- provides meaning to the game
- answers the question: why should you stay in that loop?
- it comes down to core human motivation.
- a hard problem to solve since context changes as the player goes through the game; you need to keep the player in flow even as the context changes
- people like to "belong" to a group
- they help their group progress, hit their goals to receive a sense of status from their peers
- meta-game for social media (Facebook/Twitter/Instagram)
    - they have a meta-game around identity, around social status.
    - follower count, high retweets, and perceived influence are examples of meta-game

### Shop
- free-to-play games don't charge a single price rather calibrate the payment according to the level of player's intensity
- only a small percentage of the players buy anything
- a subset (called whales) spend a large amount
- Power Law at work
- making a compelling game with built-in distribution is hard
- best games integrate the shop flow throughout the product. The game and the shop are one.
- players exhibit different characteristics as the context changes
- create different segments of the players and create different offers based on how they engage and spend
- players may hop through segments over time

### Portable idea from game design to business
- understand Power Laws
- understand customers in different segments and how much of your business is driven by the biggest segments; 
- you probably don't understand these segments as well as you think
- you're probably underpricing that segment because you don't value the product nearly as they do.
- you probably don't understand how to build effectively for them, because either you don't love the product as much as they do or you don't derive as much value from it as they do
- you can't build a one-size-fits-all product
- There are so many different jobs to be done for a product
- Your product may be a Swiss army knife because you have 10 different segments or 5 different segments.
- each of those segments may both have different things they want from the product and they may have different importance to your own business.
- world is increasingly shifting to a more variable monetization model
- segments that may drive more than half your revenue, but maybe a small base of your customers or users are really important to understand.
- Twilio, where the pricing is usage-based, perfectly captures the power law

## Continue Reading

* [Think clearly about your venture with P.A.S.T.O.R. Framework](/pastor/)
* [Book Notes - Smartcuts](/smartcuts/)
* [Big Ideas From Peter Theil](/big-ideas-from-peter-theil/)