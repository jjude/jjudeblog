---
title="Testimonials"
slug="testimonials"
excerpt="What people, who worked with me, tell about me."
tags=["problem-solving"]
type="page"
publish_at="20 Jun 10 16:30 IST"
featured_image="https://cdn.olai.in/jjude/testimonials-gen.jpg"
---

"Joseph is one of those rare persons, who **despite all their knowledge are full of humility**. An innovator who understands the strength of **process** flow and thinks one level ahead. Moreover, his willingness to explain (and teach) the concepts to his team till the cows come home, shows his perseverance. He is willing to **own responsibility**, in this age where consultants only want credit and shy away from ownership, makes him an **invaluable ally**. His contribution to Government of India's first successful mission mode project MCA21 is immense. Whether it was the basic architecture or payment settlement or integration with other portal or process flow for charge registration, he was always there to contribute with his innovative ideas. It was a joy to work with him for almost three years and make MCA21 such a wonderful e-Governance project."
[Anil Kumar Bhardwaj](https://in.linkedin.com/in/anil-kumar-bhardwaj-089321a5), Director, Ministry of Corporate Affairs, Government of India

***

"I had the opportunity to interact with Joseph who was working with Ministry of Corporate Affairs ( e-governance divn). Joseph is very **focussed** and Joseph is creative in looking for **options from technology and processes front**. Capitalizing on inter-personal and technology skills, he has influenced development created team in the department for the first of its kind e-goverance project from Govt of India. I wish Joseph the very best in his future endeavours !" - [Vikas Singh](https://in.linkedin.com/in/vikas-singh-b370746), General Manager- Public Sector Sales, IBM India

***

"Joseph is a **multi-faceted person**, working with him is great fun and learning too. He is master of many arts. He is one of very few talented persons who are **ready to work outside their comfort zone**. He is very technical and **result oriented person** and once he takes up any job or challenge, be rest assured that the job is done. I myself has witnessed it many times during my stint at iGATE. I would love to work with him at any time and will not hesitate to recommend him."
- [Guru Murty](https://www.linkedin.com/in/gurumurty "Guru Murty"), Vice President, iGATE Global Solutions managed Joseph at iGATE Global Solutions

***

"I have had the privilege of working with Joseph on a very critical tool roll out and institutionalization. He has **excellent project management skills**. He is a **goal oriented manager** who can work against odds to achieve a goal. I enjoyed working with Joseph"
-  [Elizabeth Koshy](http://in.linkedin.com/pub/elizabeth-koshy/5/291/480 "Liz"), Divisional Head, iGATE Global Solutions Ltd worked with Joseph at iGATE Global Solutions

***

Joseph is an **atypical IT consultant**. He is not satisfied with giving you the solution you want; he will want you to have the best solution possible. That is a quality rarely found because it is then contingent on the solution provider to go beyond the ordinary to the cutting edge and **evangelize for adoption**. As a client and colleague, I appreciate that non conformance as it helps to improve my delivery and performance."
- [Jairaj Nair](http://in.linkedin.com/pub/jairaj-nair/3/aa9/b17 "Jairaj"), Director &amp; Head, Banking BPO, iGATE Global Solutions Ltd managed Joseph indirectly at iGATE Global Solutions

***

"Joseph was the first boss that I ever worked for. And I must say that one considers oneself to be **lucky to start one’s career under his expert tutelage**. Perfectionist, demanding, technically sound, **ahead of the curve** are some of the adjectives that I can think of to describe him. He is someone who will constantly push you and make you challenge yourself. I have imbibed some of his work ethics and I feel they have served me well in my career." - [Siddhartha Ghosh](https://in.linkedin.com/in/siddhartha-ghosh-30a74616), Senior Consultant at Deloitte

***

"Joseph is someone that really **listens and tries to understand the real problems** in a situation. Once these are identified he actively searches for solutions and is capable to come up with alternatives where necessary. And on top of that: great guy, and great guy to work with!!"
-  [Nico Claes](http://be.linkedin.com/pub/nico-claes/1/2b8/224 "Nico"), Senior CRM Engineer, Belgacom Mobile worked directly with Joseph at Covansys

***

"Joseph and I worked together early in his career when his **excellent technical skills, can-do attitude and great work ethic were a hard-to-beat combination**. Based on his contribution, I had no hesitation in recommending him for bigger roles. Being a quick learner, he made excellent use of the opportunity and has grown into a **leader who inspires not only with his friendly and infectiously positive disposition, but also with his analytical and technical abilities**. He is one of the people who is constantly looking to improve. With his **voracious reading habits and do-it-to-learn-it attitude, he is a great asset wherever he gets involved**"
-  [Sastry Tumuluri](http://in.linkedin.com/in/sastry "Sas3"), Head - Middleware Dev Center, Covansys Limited
managed Joseph at Covansys

***

"I was one of the privilged ones to have Jude as my trainer. He was very **motivating and used to appreciate and encourage** my programming skills. I wouldnt have understood the concept of Pointers &amp; Linked List except for his brilliant explanation. Having interacted with him personally I am simply amazed looking at his remarkable journey through life."
-  [Gerard Jayasingh](http://in.linkedin.com/pub/gerard-jayasingh/1/381/b91 "Jai"), Student, BDPS Chennai (Bureau of Data Processing Systems) worked indirectly for Joseph at BDPS

***

