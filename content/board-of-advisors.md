---
title="Do you have a board of advisors?"
slug="board-of-advisors"
excerpt="You should have a board of advisors for the same reasons that a CEO has one."
tags=["gwradio","coach"]
type="post"
publish_at="06 Feb 23 17:26 IST"
featured_image=""
---
You might assume only a company should have board of advisors. Not necessarily.

First let us see why a company has a board of advisors. There are three primary reasons.

<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless src="https://share.transistor.fm/e/e7385ede"></iframe>

The CEO might lack experience in certain areas. That could be in scaling the company, expanding in a particular market, or in negotiations. So a CEO forms an advisory board to help her or mentor her.

The second reason could be to guard against personal biases. As they become successful, CEOs form their own mental models of what works and what doesn't work. The trouble is this model has to continually evolve as the dynamics of the market changes. The advisors help a CEO to challenge the existing mental model and form an appropriate model.

The last reason is that nobody within the organization is going to challenge the CEO when he is wrong. So the board of advisors help the CEO in challenging his ideas.

If you think about all of these reasons, you'll realize you also need a board of advisors as you grow in your career.

I don't know where I heard that idea, but I have followed this advice and have a personal advisory board. I treat exactly as a CEO would handle the board of advisors. 
  
I meet them at least once a quarter, usually over lunch or dinner. I share with them my pursuits and results. I ask specific questions that I'm struggling to find answers. These practices have helped me to remove any mental knots I may have at those times.

If you don't have such personal board of advisors, I encourage you to have one.

Here is how you can do that.
  
Look around your network and find people whom you admire. Ask them if you could meet them and ask for advice on how they would think about certain issues. When you meet next time, you should share what you've done with their idea. 

It is as simple as that.

Until we meet next time, have a life of WINS.

## Continue Reading

* [How To Strengthen Your Career Prospects At Mid-Career?](/midcareer-plan/)
* [Is There Dope In Your Network](/network-dope/)
* [What Game Are You Playing?](/game-you-play/)