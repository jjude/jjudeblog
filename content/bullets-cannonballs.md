---
title="First bullets. Then Cannonballs"
slug="bullets-cannonballs"
excerpt="Conduct low-cost, low-risk, low-distraction experiments before concentrating all resources into a big bet"
tags=["wins","luck","coach"]
type="post"
publish_at="04 May 22 06:00 IST"
featured_image="https://cdn.olai.in/jjude/bullets-canonball.jpg"
---

![First bullets. Then Cannonballs](https://cdn.olai.in/jjude/bullets-canonball.jpg "First bullets. Then Cannonballs")

CNN fired a $300 million cannonball at a streaming service & it failed to hit the target. CNN+, as it was called, shut down just after a month of its launch.

In 1985, Coke fired a cannonball. It tried changing its 99-year taste & it failed. It went back to the old formula calling it "Coke Classic"

The idea of “fire bullets, then cannonballs” was developed by Jim Collins in his book Great by Choice. Cannonball can be defined as,

> concentrating resources into a big bet without validating the underlying need

Is there a better way? Yes.

Fire bullets.

Bullets are:

> low-cost, low-risk, low-distraction experiments

If your experiment doesn't hit the target but you still believe in the idea, calibrate and hit again. If the line of sight is clear, then and only then fire a cannonball.

If the experiment fails, move on to a different target.

Comedians are a great example of this approach. Comedians like Chris Rock test their jokes rigorously before getting onto HBO special or David Letterman show or even hosting Oscar.

Best-selling authors follow this technique nowadays. James Clear, the author of the best-selling book, Atomic Habits, tested his ideas first as a series of tweets, then as blog posts, before writing his popular book.

When the founders of Buffer, the SAAS company, had the idea for their product, they put it out as a greeting page. Only when they had enough enrolment, they invested their time and effort to build the product. A classic bullets first, then cannonball strategy. 

How you can follow this strategy:
- Planning to move to a new city? Visit the city for a few days
- Want to buy a new car? Rent it out and drive for a few days
- Want to write a new book? Put out the main idea as a blog post and distribute it on Twitter, LinkedIn, and newsletter
- Thinking of building a new product? Build a version with no-code platforms.

What are the other ideas for bullets first & then cannonball?

## Quote To Ponder

It is better to take many small steps in the right direction than to make a great leap forward only to stumble backward. - Old Proverb

## Continue Reading

* [Embrace The Unknown](/embrace-the-unknown/)
* [Luck Loves The Paranoid](/luck-loves-paranoid/)
* [Approximately Correct](/approximate/)