---
title="What's In A Name? Everything"
slug="name-is-everything"
excerpt="When I finally understood that great quote by Dale Carnegie on the significance of name..."
tags=["coach","networking","wins"]
type="post"
publish_at="17 Nov 21 06:00 IST"
featured_image="https://cdn.olai.in/jjude/name-is-everything.jpg"
---

I was excited and nervous as I boarded the late-night flight from Bangalore to Canada. I was excited because I'll be working with the CFO of one of the largest banks in Canada; nervous because my boss told me a little something earlier that day as we said goodbye.

"We have several multimillion-dollar projects with them, but none of them are as significant as the project you will be working on. If you do this project diligently, he will think we do all projects that way. If you do it sloppily, he will think we do all projects that way."

It was an action-packed first day in Canada. I met the project manager and the IT director. Both of them explained the need and the scope of the project in detail. 

I have to gather requirements to build a digital dashboard for the CFO. It will involve gathering details from almost every department within IT. My project manager warned that since I am from an outsourcing vendor, everyone will view me as a threat to their jobs. 

hmm...what a good start.

A key person in the department is Xavier, who is a much-celebrated person in the department. He wasn't happy the project went to an outsourcing vendor. For fifteen days, he rejected every meeting organized by the project manager. As an alternative, she set up meetings with other deputies. No one was willing to give information without his approval. I was running out of options and time.

When I brought this up in our review meetings, both the IT director and the project manager washed their hands off it.

"You have to get hold of information via documents or in some other way."

Basically, it meant "we don't care, you are on your own."

There was more.

"The project will be canceled if we don't show any progress at the month-end review."

As if I needed more pressure.

The bosses back in Bangalore were not very helpful either. I gave up and started searching for return flights. 

In the meantime, a colleague invited me to a weekly beer meet-up. Many colleagues showed up. Everyone was kind and gracious. I decided to forget about the pain back at work and socialize with them.

One more week has passed. Then another.

The only thing I looked forward to were these beer-Thursdays. I became part of their gang. We would drink beer, play pool, hop to another bar, play pool, and repeat the routine until late at night. Even though they tried to teach me many times, I couldn't catch on to the pool game.

After mingling with more and more teammates, one of them pitied me and told me all about Xavier. 

He is from the French-speaking side of Canada. Despite being smart, he is not arrogant. Everyone in the team respects him very much for his achievements and for his personality. Nobody will his cross his line.

The team-mate suggested that perhaps you should just approach him directly rather than waiting for an official meeting.

What do I have to lose? At least, I could return home knowing that I gave it my all.

I decided to walk over to his desk and catch him without an appointment. As I walked, I rehearsed what I would say and went through a decision tree of options and how I should approach each option.

I was at his desk.

"Excuse me, Xavier. I was wondering if you could spare me a minute."

He looked up surprised.

"What did you say?" he said with a heavy French accent.
"I was wondering if you could ..."
"No, how did you pronounce my name?"
"zahv-YAY"
"How did you know that?"
"I worked with some French earlier when I was in Belgium"

He got up.

"Why is he getting up?" I wondered

"Hey guys," he yelled to his teammates.
"I am working with you all for ten years. None of you pronounced my name correctly. Someone has to come all the way from India to call me correctly. You should be all ashamed of yourself."

![One's name is the sweetest sound in any language](https://cdn.olai.in/jjude/name-is-everything.jpg)

He turned to me, "what can I do for you?"

Reality is stranger than fiction. 

Everything changed at that moment. He directed his teammates to give me every detail I needed. He would also drop at my desk every day after lunch and ask me if I needed any clarification.

All went well with the project. The CFO was happy with the outcome. The project manager and IT director were happy because the CFO was happy.

It didn't end there. Xavier convinced everyone to arrange me a farewell lunch and a ticket for a NBA baseball game. Everyone chipped in for lunch and the ticket.

As I boarded the plane back to India, I finally grasped what Dale Carnegie meant when he said, "A person's name is to that person, the sweetest, most important sound in any language."

## Continue Reading

* [Did you compliment your friend recently?](https://jjude.com/compliment)
* [Is There DOPE In Your Network](https://jjude.com/network-dope)
* [Book Notes - Networking like a Pro](https://jjude.com/network-like-pro)