---
title="How To Be Effective As A New Manager?"
slug="how-to-be-effective-as-a-new-manager"
excerpt="Five tips to get you started as a new manager."
tags=["coach"]
type="post"
publish_at="14 Jul 10 16:30 IST"
featured_image="https://cdn.olai.in/jjude/how-to-be-effective-as-a-new-manager-gen.jpg"
---

Those who are promoted to management often are overwhelmed by the responsibilities. They find themselves in new territory and get disoriented and eventually discouraged. It need not be that way. Asking themselves few questions regularly will enable them to steer through the management maze.

**1) What is expected of me?**: To be effective, first and foremost, one needs to be clear of what is expected of them - said otherwise, the output of the task; not the activities but the result. It could be: Mock Screens, Detailed Project Plan, Working Demo, or Presentation on Sales Process

Since the downstream activities and evaluation will depend on this 'expectation' it is essential to get this clear and precise. Else one will be working hard to produce a wrong output - a disappointment to themselves and to their boss (or the one who requested it).

**2) What is the priority?**: Knowing the priority of the expectation will facilitate re-arranging the existing tasks to fit the current one. Actually knowing the priority serves two purposes.

First one is the obvious one. Given the list of tasks that one is already doing, where does this fit in? Oftentimes, new managers can not differentiate between urgent tasks and important tasks.

Second one is a little tricky one: where does this fit in the grand scheme - within the project, within the department, within the organization. Knowing the answer for this question, will help understand the importance of the assigned task.

**3) What is the timeline?** Sometimes timeline is given and other times it is requested. In both cases, it should be examined and confirmed that the timeline is sufficient for the task assigned.

In reality, situation will always demand to squeeze timeline. Yet, do not confuse effectiveness (delivering what is requested) with efficiency (delivering by using less resources). Until effectiveness is mastered, one can not be efficient.

**4) What is needed?** Be specific about the resources needed to complete the task - a technical architect, a video conference link, 2 GB RAM or whatever. Do not assume that the higher management will not provide. Only when it is asked, it is given.

**5) What can go wrong?** Not just the new managers but even the experienced ones skip this question but an answer to this question is essential to be effective and successful. The most trivial step can go wrong and derail the progress.

How much ever one is prepared, things will still go wrong; but that is no excuse for un-preparedness. Though it is practically impossible to think of 'all' that can go wrong, one should have a 'Plan B' for the obvious ones. Effective managers are able to correct course mid-way because they think through different ways to get to the end and that is the differentiating factor. Go forth and be effective.

