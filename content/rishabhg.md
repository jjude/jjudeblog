---
title="Rishabh Garg on 'Power of Twitter, Side-hustle, and Community'"
slug="rishabhg"
excerpt="What can happen if you mix curiosity and the Internet? Rishabh Garg manifests the answer."
tags=["gwradio"]
type="post"
publish_at="03 Aug 21 09:32 IST"
featured_image="https://cdn.olai.in/jjude/rishabhg-gen.jpg"
---
Rishabh used Twitter to find a partner for his side-hustle. Then because of his side-hustle, he joined an indie-maker community. He eventually joined a startup in Malaysia. He tapped the power of the Internet to improve his career and life.

<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless src="https://share.transistor.fm/e/91560e4c"></iframe>

## Topics Discussed
- Should knowledge workers fill timesheet?
- The best advice his father gave him
- How the movie Tamasha shifted his mindset

## Watch on YouTube

<iframe width="560" height="315" src="https://www.youtube.com/embed/RCfmyNOcSTo" title="Power of Twitter, Side-hustle, and Community" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Connect with Rishabh

- Website: https://rishabhgarg.bloggi.co
- Twitter: https://twitter.com/rishabh_grg
- LinkedIn: https://www.linkedin.com/in/rishabhgarg7/

## Links to resources discussed in the episode

- Ladder blog post: https://rishabhgarg.bloggi.co/ladder
- Fajar Siddiq: https://fajarsiddiq.com
- Md Shadab Alam(partner in developing Tweetflick): https://www.mohdshadab.com/
- Communities that helped Rishabh: https://www.ramadanmakers.com/ & https://www.jamstackmakers.com/

## Connect with me
- Twitter: https://twitter.com/jjude
- LinkedIn: https://www.linkedin.com/in/jjude/
- Website: https://jjude.com/
- Newsletter: https://jjude.com/subscribe/
- Youtube: https://youtube.com/gravitaswins
- Email: podcast@jjude.com
- Executive Coaching Program: https://gravitaswins.com

## Your feedback counts

Thank you for listening. If you enjoy the podcast, would you please leave a short review on Apple podcast or on YouTube? It takes less than 60 seconds, and it really makes a difference in finding this podcast. And it boosts my spirits. 😀

I would like to hear what resonated the most with you in this episode. Either you can email me or share it on social media and tag me. Thank you for your support.

Have a life of WINS.

## Amazing tools I use:
- I record all the remote interview with [Riverside.fm](https://riverside.fm/?via=joseph-jude)
- I use [Transistor.fm](https://transistor.fm/?via=jjude) for hosting these podcast episodes

_Gravitas WINS Radio is produced by [Joseph Jude](https://jjude.com/). Be sure to subscribe on [Apple Podcast](https://podcasts.apple.com/in/podcast/gravitas-wins-radio/id1562802449), [Spotify](https://open.spotify.com/show/7LOBjAyjBkechMC0xMIj9E),[Google Podcast](https://podcasts.google.com/feed/aHR0cHM6Ly9mZWVkcy50cmFuc2lzdG9yLmZtL2dyYXZpdGFzLXdpbnMtcmFkaW8=),[YouTube](https://www.youtube.com/gravitaswins), or the podcasting app of your choice._

Please check out other episodes of [Gravitas WINS Radio](/gwr-themes/). For easy consumption, they are categorized by themes. If you are a fellow podcaster, you might be interested in [How I Podcast](/how-i-podcast/) or [tools I use](/i-use/).

## Continue Reading

- [P. K. Khu₹ana on 'Happiness, Joy and Bliss'](/khuranapk/)
- [Rishabh Garg on 'Power of Twitter, Side-hustle, and Community'](/rishabhg/)
- [Mohan Mathew on 'Management Consulting'](/mohanm/)