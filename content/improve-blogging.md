---
title="Seven tips to writing fantastic blog posts regularly"
slug="improve-blogging"
excerpt="Lessons I learned on this topic from a decade of blogging."
tags=["coach","sdl","blogging"]
type="post"
publish_at="23 Jul 18 15:30 IST"
featured_image="https://cdn.olai.in/jjude/7-tips-for-blogging.jpg"
---
Amit Subodh[^1] asked a question that most bloggers struggle with — how to write good articles regularly.

I have been blogging for more than a decade now. Here are the lessons I learned.

![7 tips for blogging regularly](https://cdn.olai.in/jjude/7-tips-for-blogging.jpg "7 tips for blogging regularly")

_Photo by [Emma Matthews](https://unsplash.com/@emmamatthews) on Unsplash_

### 1. You don't have to publish everything you write

In the initial days, I always wrote with the aim of posting. I spent a lot of time thinking about a topic. Then as I started writing, I spent a lot of time on each sentence. Obviously, I missed many days of posting. It was an unnecessary chain I laid on myself.

Now, I have unshackled myself.

### 2. Write daily

Nowadays, I write daily. The aim is not to publish, but to write something interesting. Every day, I sit down in the same place, almost at the same time and I write for half an hour.

I may write on apps I use, lessons learned from Twitter, the dilemmas I face in professional life, and so on.

There are no rules on topics or structure. There is only one rule — write for a an half hour.

Obviously, some of these writings get into my blog or [newsletter](/subscribe/).

### 3. Write for one

When I write, I imagine having a conversation with a friend. Not an imaginary friend, but a real flesh-and-blood one. Because I have been writing for years, there are folks who regularly contact me with appreciation and questions. So it becomes easier and easier to imagine having a conversation with them. This helps me to write in an easy to understand language rather than hard prose.

I wrote this post itself as a conversation with Amit.

### 4. Read widely

I read widely - not just on topics I write. I read psychology, theology, biography and so on. I prefer to read books than vacuous articles.

As the saying goes, great artists steal. If you read well, it becomes easier to steal — ideas, sentence structure, and quotes. They bring life to your otherwise banal writing.
I have a file with catchy sentences from these books. I read them often and steal something or the other from these sentences.

There is another exercise that I do often. I rewrite paragraphs from those books in my own style. The idea is to improve my thinking and writing style.

### 5. Get feedback

I post some of what I write in this blog and few others in the newsletter. Sometimes, snippets of what I write ends up in emails I send to friends and colleagues. Without shame, I ask for [feedback](/how-to-get-the-feedback-that-you-deserve/).

### 6. Separate activities

I separate the activities involved in blogging. Generally speaking, these are the actives involved in blogging.

- thinking & researching
- writing
- editing
- publishing

Because I separate these activities, I am able to block time in a day and do them. My mind is not jumping from one to the other.

### 7. Quantity is the path to quality

Usual debate on quantity & quality is wrong. We all have heard that "it is quality that matters, not the quantity." As a consumer, I expect quality. But as a maker, I have to focus on quantity to achieve quality.

Makers suffer from two nightmares: fear of starting, and fear of finishing. How do you get rid of these fears? By practicing. Practice builds muscle-memory for makers.

_Read more of my thoughts on this question: [When quantity trumps quality?](/quantity-vs-quality/)_

Since I don’t publish everything I write, I focus on the quantity of words I write. That way, I build a muscle memory for writing. Then when I sit down to write an article, it becomes a lot easier.

### Conclusion

"I write for the unlearned about things in which I am unlearned myself," wrote C.S. Lewis. This is the core idea behind my blogging. Blogging is one of the ways I [learn](/sdl/).

I encourage everyone to blog. It is a sure way to improve your career.

## Continue Reading

[After 300 blog posts...](/300-posts-later)
[Seven Insights From Two Months of Blogging](/seven-insights-from-two-months-of-blogging)

[^1]: He asked in [Blogging As Business](https://www.facebook.com/groups/bloggingasbusiness/) Facebook group
