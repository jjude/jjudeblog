---
title="You need to know very little to get ahead in life"
slug="little-to-get-ahead"
excerpt="You need to only master three domains and you'll be far ahead of others"
tags=["wins","systems"]
type="post"
publish_at="19 Oct 22 14:49 IST"
featured_image="https://cdn.olai.in/jjude/pyramid-comm.jpg"
---
You don't need a lot of knowledge to succeed in life. What matters most is how well you understand what you know. Based on my experience, here's what you need to know:

• Multi-disciplinary learning 
• Structured thinking 
• Structured communication

## Multi-disciplinary learning

What is multi-disciplinary learning?

Knowing "big" ideas from different disciplines. 

Why it is needed? 

Most breakthrough innovation comes because of cross-domain knowledge & application.

Example: Henry Ford brought assembly line to his workshop from slaughterhouses.

What are the different disciplines from which you should build your knowledge? Peter Kaufman suggests three buckets:

• non-living (physics, geology, and chemistry), 
• living (biology), and 
• recorded human history.

You can pick and choose your own domains. Here are mine:

• Spiritual 
• Personal finance 
• Math 
• Anthropology

Big ideas in "Spiritual":
• Pursuing goals larger than here & now 
• Loving oneself 
• Loving others 

As a Christian, I derive them from the Bible. You can read any spiritual book. Atheists may find [Religion for Atheists](https://www.alaindebotton.com/religion/) helpful.

No matter how we feel about it, we're all spiritual in some way. When we are up against the wall, we turn to the divine and hope against all odds.

Even Elon Musk prayed before the first Spacex flight. He shared this in his [conversation with Lex Friedman](https://overcast.fm/+eZyDKqtgI)

Big ideas in "Personal finance"
• Make money
• Grow money
• Protect money

"[Let's talk money](https://amzn.to/3gfgllh)" by Monika Halan is an excellent book on personal finance. It is written in easy-to-understand language that anyone can understand without losing the essence of what it is about.

Big ideas in "Anthropology"

• how we behave in groups (almost always foolish in someway)
• Mimetic behaviour & its implication 

"[Wanting](https://amzn.to/3CHuhvI)" by Luke Burgis explains mimetic behavior and how to minimize its impact (we can't escape it).

Big ideas in "Math"

• How to collect data
• How to analyse data
• How to extrapolate from data on hand
• Decision making by data

## Structured thinking

Why is it needed?

Life is chaotic. A lot of times, you're forced to make a decision with sparse or conflicting data. The opportunities pass by quickly. Structured thinking allows you to move quickly to grab those opportunities.

Structured thinking enables us to create (or at least search for) structures for everything we care about.

• Learning
• Investing
• Fit body

And so on

Example of structure for learning: 
• Consume
• Produce
• Engage

![Learning Framework](https://cdn.olai.in/jjude/learning.png "Structured Learning Framework")

You can read about this here:

Example of structure for investing: 4M structure

• Meaning
• Management
• Moat
• Margin of safety 

You can read about the [entire framework here](https://jjude.com/4m-stocks/). 


## Structured communication

Why communicate?

Prof. Feynman said it [well](https://twitter.com/ProfFeynman/status/1579123122980425731):

> The ultimate test of your knowledge is your ability to convey it to another.

What is structured communication & How to go about learning it?

We live in a world that is full of noise and bias. For your words to be of any use, they must cut through this smog.

Then we all have cognitive limitations. We can communicate and absorb information only sequentially.

When you communicate in a structured manner, your listener can take what you said, and do the awesome work they have to do. Instead, when you throw a pile of sentences at them, they’ve to work harder to decipher what you said.

Pyramid style of communication is one of the structure. In a pyramid style of communication:

• You state the key point first  
• Then logical sub-points  
• Support each sub-point with arguments and data

![Pyramid communication](https://cdn.olai.in/jjude/pyramid-comm.jpg "Pyramid communication")


You can also learn other structures in visual narratives from Gurman Bhatia. She has documented all the [different structures](https://gurmanbhatia.com/talk/2021/03/09/stories-structure.html) on her site.

Master these skills, you'll get ahead of most

• Multi-disciplinary learning
• Structured thinking
• Structured communication

## Continue Reading

* [What can super-chickens and system thinking tell us about building a hyper-performing team?](/super-chickens/)
* [Are You Learning A New Domain? Visit Its Zoo](/are-you-learning-a-new-domain-visit-its-zoo/)
* [Go faster with a structure to success](/structure-to-success/)