---
title="Don't Replace Backbone With A Wishbone"
slug="dont-replace-backbone-with-a-wishbone"
excerpt="It is easy to wish for our situations to be transformed magically. It is easy because every one does so. But only those who step out of their wishing zone create magic."
tags=["problem-solving"]
type="post"
publish_at="25 Feb 11 16:30 IST"
featured_image="https://cdn.olai.in/jjude/dont-replace-backbone-with-a-wishbone-gen.jpg"
---

I, as an independent consultant, face two perennial problems: generating enough leads and sustaining cash flow. I wish I could do what [Dr Alan Weiss](http://www.contrarianconsulting.com/about-alan-weiss/) has done. He generates million dollars in independent consultancy.

I bought his books to learn what he did. I wish to implement all his ideas so I too can generate million dollars.

If not Dr Alan, I wish to be like [Sephi Bergerson](http://www.flickr.com/people/sephi_b/). Since 2002, he has made India his home and photographed many aspects of Indian life, specifically Indian weddings. He has also published a book on Street Foods of India. I wish I could travel as much like him and click snaps as beautifully as him.

I wish, I wish, I wish...

If you thought these are rambles of an insignificant individual, think again. Those occupying the executive chairs are no different.

Recently (On 16th Feb, 2011), Prime Minister of India held a press conference and what did he say? I wish our party had a majority mandate, I wish I had 'clean' colleagues, I wish we had a better opposition party. He was full of wishes.

It is true that wishes are bridges to the future. But **translating wishes to reality requires action** - action of deciding a course and staying the course to the completion. Action differentiates the accomplishers from dreamers.

Great problem solvers act. They act despite having partial information; despite the risk of being burnt; despite being looking stupid. They are willing to adjust their implementation as they go along.

It is easy to wish for our situations to be transformed magically. It is easy because every one does so. But only those who step out of their wishing zone create magic.

Are you wishing for a magic or are you creating a magic?

_This post is part of ‘[Be a Problem Solver](/be-a-problem-solver/)‘ series._

