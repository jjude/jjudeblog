---
title="Should Books Be Backed By Scientific Research?"
slug="should-books-be-backed-by-scientific-research"
excerpt="Books that had profound impact on me and those improved my life were not based on scientific research, but commonsense. Would you read books by field-workers or only by researchers?"
tags=["books"]
type="post"
publish_at="25 Aug 10 16:30 IST"
featured_image="https://cdn.olai.in/jjude/should-books-be-backed-by-scientific-research-gen.jpg"
---

Two books that had a profound impact on me are [Seven Habits of Highly Effective People](https://www.amazon.com/dp/B01069X4H0?tag=jjude-20) by [Steven Covey](https://en.wikipedia.org/wiki/Stephen_Covey) and [Tough Minded Optimist](https://www.amazon.com/dp/B0014IZ5SY?tag=jjude-20) by [Norman Vincent Peale](https://en.wikipedia.org/wiki/Norman_Vincent_Peale). But they are not a result of rigorous scientific research.

Similarly, [The Monk Who Sold His Ferrari](https://www.amazon.com/dp/B009FTAH3W?tag=jjude-20) and
[Tuesdays With Morrie](https://www.amazon.com/dp/B002TXZRNQ?tag=jjude-20) had positive impact in
many lives. These two are not based on scientific research either.

I'm sure there are countless other books, which are not based on scientific research but are inspiring millions around the world.

But the academics routinely dismiss such self-help books which are not based on scientific research. Recently I listened to a
motivational talk as part of leadership workshop. The speaker, a Ph.D. in Psychology, dismissed these books because they are not based
on scientific research. One might argue that this argument is limited to self-help genre of books publishing industry. Not quite true.

Let us consider that one of the [Dabbawalas](https://en.wikipedia.org/wiki/Dabbawala) publish their success story (contrast to a study by Harvard Business School).

Would you read it even though it is not based on scientific research?


_Disclosure: Some of the links in the post above are “affiliate links.” This means if you click on the link and purchase the item, I will receive an affiliate commission. Regardless, I only recommend products or services I use personally and believe will add value to my readers._


