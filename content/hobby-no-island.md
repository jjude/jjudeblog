---
title="No hobby is an island"
slug="hobby-no-island"
excerpt="There are no boundaries between hobbies and work, no doors between personal and professional lives."
tags=["gwradio","coach"]
type="post"
publish_at="14 Feb 23 06:00 IST"
featured_image="https://cdn.olai.in/jjude/hobby-projects.jpg"
---

<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless src="https://share.transistor.fm/e/d147c451"></iframe>

This is the 50th episode of Gravitas WINS radio. I started the podcast as a monologue. Then I interviewed people. Now it is a mix of both.

There are no ads in this podcast. There is no direct income stream from this podcast. It might seem that I'm wasting my time on this podcast. I thought so too. That is why I dropped it last year.

But there are always 2nd order benefits.

Last year, we were hiring heavily and I had to interview lot of people. All via zoom.

Many people told me it was their best interview they attended. I felt good hearing that. But when I heard it from more and more people, I started to think what is going on here. When the next interviewee told me the same thing, I asked why she said so.

She said, you asked questions from what I had done. You didn't try to put me down. You let me explain in my own words. 

That is when it clicked. This is something I learned for interviewing guests on the podcast. When I started to interview guests for podcast, I asked for tips from others who ran successful podcast interviews. 

They said,
- be curious about their work   
- ask questions and shut up   
- make them feel good   

It looks like I applied those skills to hiring interviews.

There are no boundaries between hobbies and work, no doors between personal and professional lives. What we do at home echoes at the office and vice versa.

It is true of writing too. When I graduated from college, I couldn't speak or write a single sentence in English properly. To improve my communication, I wrote in private. I started publishing essays when I got internet access. My writing and speaking improved so much over the last decade. Along with it, my thinking improved too. After all, clear writing requires clear thinking.

And it shows in office communications. In conducting meetings. In sales calls.

Here is what I've learned: No hobby is an island.

If you want to improve your career prospects I highly encourage taking up a hobby - it could be cooking, dancing, theatre...doesn't matter. Bring your best to it. Try to excel in it with whatever resources you have. You'll not only excel in that hobby, but there will be 2nd order benefits coming into your career too.

Thank you for listening. Thank you for your support. Have a life of WINS.

## Continue Reading

* [Wrong way to assemble the best car](/wrong-way-for-best-car/)
* [Who are you chasing?](/who-chase/)
* [Do you have a board of advisors?](/board-of-advisors/)