---
title="Emmy Sobieski on 'I'd far rather be lucky than smart'"
slug="emmys"
excerpt="Life and career lessons from Emmy Sobieski who worked with four billionaires."
tags=["wins","coach","gwradio","luck"]
type="post"
publish_at="26 Oct 21 06:14 IST"
featured_image="https://cdn.olai.in/jjude/emmys.jpg"
---
I interviewed Emmy Sobieski for [Gravitas WINS Conversations](/podcast/). She started her career as a horse trainer before going on to lead the world's number one performing fund. She also worked for four billionaires.

<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless src="https://share.transistor.fm/e/e7d85e73"></iframe>

## What you'll listen

I learned something new every minute of the 45 minutes that I talked to her. Here are the major lessons:
- Don't let peer pressure, guilt, and shame weigh on you and drain your energy
- Offset all of the emotion that's happening in markets and in business by sticking to simple concepts
- The beginner's mind leads you to one core thing. But it's the years of experience that help you define what that core thing is
- Most of AI is about heading towards an objective, trying to minimize errors towards that objective. This is the way most of us live. It's not truly being who we really could be
- Find out what you need to do and who can help you get that
- Amortize your efforts among the areas where there are 85% overlap
- Becoming number one doesn't happen by consensus
- Build a base, build fundamental understanding of what you're going to do, and then warm up. Otherwise you'll hurt yourself
- The best leaders question themselves. They have incredible listening skills. The listening skills give you connection to people with power and give you influence
- Whenever you ask a question, try to ask it with the word, "what" at the front of it, instead of "why"
- Our metabolic rate remains stable from 20 to 60 years of age. If you mentally slow down, you'll slow down
- Have you ever seen cats? They see a string. And they start pulling on the string. Then the whole sweater is unraveling. That's the way to learn. Start pulling strings that interest you.

![I'd far rather be lucky than smart](https://cdn.olai.in/jjude/emmys.jpg)

Here is the edited version of our conversation. I was liberal in editing the content. You should really listen to the podcast.

### How to be full of energy?

The secret of my energy is being free, feeling free. That's like waking up every day with a beginner's mind and starting fresh and not having things weigh on me, like, peer pressure, guilt, and shame, as much as I can. I try to get rid of those and start the day fresh. And think of the possibilities ahead of me. 

Energy goes down because of the feeling that I've got all these pressures or shame or I've to do something for someone.

**I do things for people, because I want to; not because they are guilting me**. Just remove a lot of things that tend to weigh us down.

### Peer pressure

You learn peer pressure initially from your parents and then from kids at school.

### Copying to improve

I was very reactionary to things people would say. With beginner mindset I said, this isn't the way I want to be. I had some really good friends and I noticed how people were interacting with other people. **I wanted to be like that. So I'll just copy that**. 

I saw my one friend just was like super chill about stuff. She had really good parents. And so I said to her, will you observe me? And if you notice me doing things that don't seem like normal interaction, can you let me know so I can train myself. 

### Keeping beginner's mind but drawing on years of experience

I was working at a hedge fund and responsible for billions of dollars. We were long, we were short, just very high pressure situation. A perfect situation to like rest on all of your past experience.

And there was a company and they came in and they said to me, these other companies have higher valuations because they're saying they have this types of software and we make those too. It's just people aren't aware of them.

So we're going to break out that division into a separate unit and then it'll help our valuation. I said, look, I think this is a huge mistake. Just stick to your knitting, do what you've always done. 

They were like, what do you know? We just came through New York to tell you what we're going to do. So then they go off and start reporting the second business unit. 

So I thought, it's brand new type of software. I should see what I think demand is going to be like. I'm going to go talk to some consultants, talk to some customers, I couldn't find any consultants that had ever seen the software; Couldn't find any customers. 

So there's the beginner mind again - I don't understand how you have revenue when you don't have customers.

And people are like, oh, Emmy, it's very complicated. You don't understand. And I'm like, no, it's revenue is P times Q like, it's that simple? And that's the beginner mind, right? 

Fast forward, four months. The CFO and CEO admitted that they had making up the revenue numbers and they both went to jail.

A lot of times **you can offset all of the emotion that's happening in markets in business by just sticking to very simple concepts.**

**It's the beginner's mind that takes you to that one core thing. But it's the years of experience that help you define what that one core thing is.**

### Having an unconventional career path

The childhood could be a negative or it can be a positive. And so what happened with my childhood is that, I came out of it overly sensitive and that's very handy if you're in the stock market and you're trying to read the tea leaves of management and see if they're losing confidence or seeing if they're getting more anxious and I had antennas up all the time. It turned out to be super helpful.

It's the same with horses. I can get on horses and I can sense how they're feeling, how they're doing. When I was in college, I rode in the junior Olympics and was a team gold medalist and wanted to keep pursuing that.

I moved to Seattle. I worked actually as a litigation paralegal during the day, and then rode horses at night and on the weekends, trained with a guy that had trained two basically half the U S team and half the Canadian team were his students. He was such a good trainer. I was training with him and then my mother passed away. And I thought I'll just be closer to family and I'll go to San Diego and become a horse trainer. So that's, that's what I did. 

Then my horse trainer, when I was 25, basically said, you're in your mid twenties and you won't have the same opportunities in your mid thirties. He had already asked me years before if he could be my life guide because he felt my parents weren't super interested. He said, I want you to go to graduate school. Face all your opportunities. And if you still want to be a horse trainer, then I'll support you. But not by default. So then I applied to all these schools. And I'm the only one that let me in was USC. 

So then I got into USC MBA and I told them that I'm going to business school.

He was, oh, that's horrible. That's not what I meant. I wanted you to do philosophy or something, to better the world. We don't need another business person. So that was really funny. 

I had made a bunch of money in the stock market in college, but then the balance effectively went down for various reasons. So I entered USC $30,000 in debt with a  with a horse. And so I sold the horse for 30,000. I went to the head of the brokerage firm and I said, can I just run this?

I know that your margin requirements at 30%, but that the exchange is 50. So can I run this at a 50% margin? The 30 from the horses, my equity, and then your 30 of debt. So effectively 60,000. And I, and I invested it while I was going to school. They had a Bloomberg there at school. It took the 60, turned it into 120 and six months.

It was just the beginning of up cycle. After a recession, I was investing in small cap, which runs the most. So I kind of had the wind at my back and then I paid off the 30, had 90 and then went on from there. So my girlfriend that I had just met at grad school, said there's people that would pay you to do that. I had never really even considered a career in investing. I said, really, this was like playing Pac-Man. I would love to do that. That's what got me into the industry. Completely random. 

### Believing in luck

I'd far rather be lucky than smart.

### Writing your own future

Let's look at this from the direction of AI. AI is super interesting because it's trying to copy what our brains do. 

**Most of AI is heading towards an objective, trying to minimize errors towards that objective. And that's how a lot of us live our lives. And that's not truly being who we really could be**. 

The head of open AI, Dr. Kenneth O. Stanley, invented novelty search when he was in a university in Florida. Most of the algorithms for these AI have been around forever. They've been around 20 years, 25 years, but novelty search is newer and different. What he basically told the computer to do is that every step you take should be something new.

If you search unlimited area, the computer will never have an answer because it's always something new. But if you, have some borders on the search area, it can complete a maze four times as fast. It can learn to walk four times as fast.

And if you think about it, when you're just learning to walk, one of the most important things is to be able to do is just oscillate. The way you learn to oscillate is by falling over. So if you're trying to minimize your errors, you punish the robot for falling over and the robot never learns to walk.

So I try to live my life like that novelty search algorithm - **not an unlimited expanse, but trying new things, being open, having this beginner mind**. 

And so with that, then everything that comes along, I have a general idea. So for novelty search this. You are trying to get to the moon. So you give a big thing that you want not explore the whole universe.

So with anything that I'm thinking about with a career, I'm thinking I generally want to be doing this type of work with these types of people, and I'm open to how that looks; I'm open to what the different paths might be to that goal. 

Instead of trying to minimize your errors all the time, **don't worry so much about making mistakes**. Worry more about being open-minded and trying new things, especially early in your career. 

**Looking at an objective and, and minimize errors are good at incremental improvements.** This algorithm, mirrors, innovation and evolution. Well, **if you want to do something interesting and big and different, and you want to change something, you don't want to be living your life, objective focused, minimizing errors**.

### Showing up for success

I'm going to back up for a second because a lot of it has to do with this beginner mindset. I started the year and I was planning on doing a marathon in May. I had already invited my step step-daughter, but, then my girlfriend was like, hey, you did a 50 K for your 50th last year. I want to do one for my 50th this year. Will you come do the 50 K? They were a week apart. So I thought, I don't want to let my stepdaughter down. So I thought, okay, this could be interesting experiment. And this is like turning things around instead of saying, oh, I'm pressured because I got to do a 50 K one weekend and then fly to Hawaii and run America.

I thought "this is a great experiment. It's staying in my peak state for an entire week. I've read about that. I'd love to see if I can do it." That went really well. So then I thought, well, what other 50 Ks are out there? And I signed up for all that.

The only thing with doing that (I was 51 years old) is that you have to stay pretty disciplined about your training schedule. A little regimented for my style, but that's what I needed and my sprint start going really well. So I thought, well, it's not like I'm going to get any younger, so I should see if there's some kind of sprint competition I should enter.

So I looked it up and there's not a lot of people that are in their mid fifties, decided to go try track and field. So you don't even have to qualify. You can just go to the US masters championship for the United States.

It happened to be in San Diego. So, and it was about five weeks away. So I go on the internet and sign up for it and then go find a coach to teach me how to run off. I look at the different pieces of **what I got to do and who can help me** learn how to do it.

So I'm thinking I'm going to this US masters championship anyway. I wonder what else they have. And I see they got bodybuilding. Not that I really ever wanted to do it, but I thought, **it's probably 85% overlap with the workouts I'm going to be doing**. So I'm going to try to make my legs big.

**I signed up for everything. So this way, I'm amortizing**. I showed up and they basically said nobody had showed up for women's physique. So I ended up winning the north American champion just by showing up.

### Becoming number one doesn't happen from consensus

Most of the time I don't really know what I'm doing, I try to figure, okay, **somebody brought me up there, so I'm just going to give it my best**.

I was running internal money for an insurance company. This mutual fund company hired, the tech analyst for a mid cap portfolio manager. And then the chief investment officer said, well, now that we have you, and you're so good at tech, we should make a tech fund. And what we'll do is we'll take all the people in the whole firm that are focused on tech and we'll all get you guys into a room and you'll pick the best stocks.

That's management by consensus. And so then about a month in, we were top 10% in the nation and she calls a meeting and she's like, what in the world is wrong? And I'm like, I think it's good. Top 10%. Awesome.

She's like, no, I hired you because you're the smartest tech analyst in the country. You should be number one.

Marsha, well, **number one doesn't happen from consensus**. It just doesn't. So she said fine, everybody else out, you run the fund. That's how I got the fund, but I wasn't in there trying to get the fund.

### Be careful to build your base

I've been running for so long. I kind of have a base. Some days I'll just have good energy and I'll run maybe eight miles a day. And someday I'll come out and I'll have a free day and I'll just start running and I'll run 30 miles. I won't plan it. I just start running. And then if I need more water, I stop at a gas station. Get some water. Some days I'll just wander around.

I've one mentee. He goes, he gets some brand new tennis shoes and went out to run like 15 miles. He goes, I couldn't walk. I had to go to the doctor, the podiatrist. 

So there's some **combination of building a base and building some fundamental understanding of what you're going to do and then warm up**, just like having a beginner mindset for sure.

### Leadership quality

**The best leaders question themselves**. Could I be wrong? They question themselves with this beginner mindset

I wouldn't say it's all of them, but some of them **have incredible listening skills**. The listening skills gives you **connection to people with power and gives you influence**, which is a common trait among leaders and billionaires.

### Listening skills

**Whenever you ask a question, try to ask it with the word, "what" at the front of it, instead of "why"**.

Often **when we ask someone "why" they go into explaining mode and very quickly they'll go into a defensive posture**. So if the goal of listening is to build a connection with another person, "what" is a much better.

Second is pause 15 to 30 seconds and really count one, 1000, two 1000. So someone's talking, you think they're done? Just let there be a moment of silence. When I was talking to management teams, that would be the moment where they would share the real nugget. Or you're showing someone you're creating space for them.

And the third is something that I call almost like hopscotch two down one across. 

So you ask, what's your favorite vacation. And then I say St. John, and then you say,  what is it about St. John that you really love? Oh, I love swimming in St. John. Now, if you keep going down one more. Well, who did you swim with right now? It's a little personal. So what you do is you've gone down, we've gone down to what's your favorite vacation? What do you like about St. John's? Now I've said swimming. Now you go across, you go adjacent and you say, have you always swam often when people go down they think, oh, now I'm feeling like this is too personal and they exit, they the switch. 

They go, oh, what are you doing next week? And then the person feels dropped or unheard, but if they go down one more deeper, the person feels like, you know, start. And so if you're trying to build that bridge of connection, thinking about two down one across is really helpful to feel like you care.

### Retirement is in mind

There was a new study out this year that said that our metabolism is the same from age 20 to age 60. **If you mentally slow down and you physically slowed, you'll slow down**.

They had a different study where they took a group of people that were in their eighties that were on walkers and they put them in a closed residence where everyone moved into houses. They lived there for three months and they newspapers were from 20 years before and TV shows from 20 years before, and people were moving around, they stopped using their walkers.

### Learning from cats

**Have you ever seen cats? They see a string. And they start pulling in the string. Then the whole sweater is unraveling**, but they're just pulling on the string. And that is the energy. I have this energy of curiosity. I just keep pulling on the string. 

For instance, the whole bodybuilding thing happened because I was pulling on a string. I was just training for, I was training for ultra marathons. I happened to be sprinting on Tuesdays and then I was like, wow, that went really well. What else should I do? Like, it's like literally a curiosity of a cat and it's just going all of the. I like a really interesting podcast or I see somebody do something really well, but I don't know about, I think, well, how could I do that?

### Kindest thing anyone done

I have had so many people do so many kind things. I feel incredibly blessed. 

My number one is that person I met in college. Her name was Lori. She helped me reprogram my mind and my interactions with people in college.

That completely changed my life because that meant that I could learn to connect with people and have a more normal, connected life instead of an isolated one. It's a lot of work. It was two years of watching me and saying, okay, Emmy, you just won this. You should call your parents.

Just super patient and it was a long haul for her and it was incredible. 

### Definition of living a good life

Freedom. Freedom of choice.

### Where can you find Emmy online

- Twitter: https://twitter.com/EmmySobieski
- LinkedIn: https://www.linkedin.com/in/emmy-sobieski-cfa-cnc-wellness-self-care-health-workshops-coaching/
- Website: https://www.emmysobieski.com/
- Youtube: https://www.youtube.com/channel/UCmjPCmjDar5aQRde7QAX0Vg

### References

- [Metabolic rate remains stable all through adult life, from age 20 to 60 years old](https://www.science.org/doi/10.1126/science.abe5017)
- [Kenneth O. Stanley - Novelty Search](https://www.cs.ucf.edu/~kstanley/)

## Connect with me
- Twitter: https://twitter.com/jjude
- LinkedIn: https://www.linkedin.com/in/jjude/
- Website: https://jjude.com/
- Newsletter: https://jjude.com/subscribe/
- Youtube: https://youtube.com/gravitaswins
- Email: podcast@jjude.com
- Executive Coaching Program: https://gravitaswins.com

## Your feedback counts

Thank you for listening. If you enjoy the podcast, would you please leave a short review on Apple podcast or on YouTube? It takes less than 60 seconds, and it really makes a difference in finding this podcast. And it boosts my spirits. 😀

I would like to hear what resonated the most with you in this episode. Either you can email me or share it on social media and tag me. Thank you for your support.

Have a life of WINS.

_Gravitas WINS Radio is produced by [Joseph Jude](https://jjude.com/). Be sure to subscribe on [Apple Podcast](https://podcasts.apple.com/in/podcast/gravitas-wins-radio/id1562802449), [Spotify](https://open.spotify.com/show/7LOBjAyjBkechMC0xMIj9E),[Google Podcast](https://podcasts.google.com/feed/aHR0cHM6Ly9mZWVkcy50cmFuc2lzdG9yLmZtL2dyYXZpdGFzLXdpbnMtcmFkaW8=),[YouTube](https://www.youtube.com/gravitaswins), or the podcasting app of your choice._

Please check out other episodes of [Gravitas WINS Radio](/gwr-themes/). For easy consumption, they are categorized by themes. If you are a fellow podcaster, you might be interested in [How I Podcast](/how-i-podcast/) or [tools I use](/i-use/).

## Continue Reading

* [Life is series of lost, found, and lucky breaks](/life-is-lost-and-found/)
* [Approximately correct](/approximate/)
* [How to get lucky?](/luck/)