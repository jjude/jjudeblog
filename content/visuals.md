---
title="Visuals"
slug="visuals"
excerpt=""
tags=["",""]
type="page"
publish_at="30 Jun 22 17:37 IST"
featured_image="https://cdn.olai.in/jjude/visuals-gen.jpg"
---

<article>
  <div class="cf ph2-ns">
    <div class="fl w-100 w-third-ns pa2">
        <a href="/work-family-fun-flywheel/" class="db link dim tc nolinka">
            <img src="https://cdn.olai.in/jjude/work-family-fun-flywheel.jpg" alt="Work, Family, and Fun is a flywheel" class="w-100 db black-10"/>
            <dl class="mt2 f6 lh-copy">
            <dt class="clip">Artist</dt>
            <dd class="ml0 gray truncate w-100">Work, Family, and Fun is a flywheel</dd>
            </dl>
        </a>
    </div>
    <div class="fl w-100 w-third-ns pa2">
        <a href="/gap-between-cup-and-lip/" class="db link dim tc nolinka">
            <img src="https://cdn.olai.in/jjude/dreams-to-reality.jpg" alt="Turning dreams into reality" class="w-100 db black-10"/>
            <dl class="mt2 f6 lh-copy">
            <dt class="clip">Artist</dt>
            <dd class="ml0 gray truncate w-100">Turning dreams into reality</dd>
            </dl>
        </a>
    </div>
    <div class="fl w-100 w-third-ns pa2">
        <a href="/5-elements-culture/" class="db link dim tc">
            <img src="https://cdn.olai.in/jjude/magentic-culture.jpg" alt="Elements of magnetic company culture" class="w-100 db black-10"/>
            <dl class="mt2 f6 lh-copy">
            <dt class="clip">Artist</dt>
            <dd class="ml0 gray truncate w-100">Elements of magnetic company culture</dd>
            </dl>
        </a>
    </div>
  </div>

<div class="cf ph2-ns">
    <div class="fl w-100 w-third-ns pa2">
        <a href="/achieve-your-goals/" class="db link dim tc">
            <img src="https://cdn.olai.in/jjude/goal.png" alt="5 Steps To Achieve Your Goal" class="w-100 db black-10"/>
            <dl class="mt2 f6 lh-copy">
            <dt class="clip">Artist</dt>
            <dd class="ml0 gray truncate w-100">5 Steps To Achieve Your Goal</dd>
            </dl>
        </a>
    </div>
    <div class="fl w-100 w-third-ns pa2">
        <a href="/lean-canvas-tie/" class="db link dim tc">
            <img src="https://cdn.olai.in/jjude/lean-canvas.jpg" alt="Lean Canvas" class="w-100 db black-10"/>
            <dl class="mt2 f6 lh-copy">
            <dt class="clip">Artist</dt>
            <dd class="ml0 gray truncate w-100">Lean Canvas</dd>
            </dl>
        </a>
    </div>
    <div class="fl w-100 w-third-ns pa2">
        <a href="/leader-challenges/" class="db link dim tc">
            <img src="https://cdn.olai.in/jjude/leaders-qualities.png" alt="TTop Leadership Challenges" class="w-100 db black-10"/>
            <dl class="mt2 f6 lh-copy">
            <dt class="clip">Artist</dt>
            <dd class="ml0 gray truncate w-100">Top Leadership Challenges</dd>
            </dl>
        </a>
    </div>
  </div>

  <div class="cf ph2-ns">
    <div class="fl w-100 w-third-ns pa2">
        <a href="/kn/" class="db link dim tc">
            <img src="https://cdn.olai.in/jjude/network.png" alt="Knowledge Networks" class="w-100 db black-10"/>
            <dl class="mt2 f6 lh-copy">
            <dt class="clip">Artist</dt>
            <dd class="ml0 gray truncate w-100">Knowledge Networks</dd>
            </dl>
        </a>
    </div>
    <div class="fl w-100 w-third-ns pa2">
        <a href="/structured-thinking/" class="db link dim tc">
            <img src="https://cdn.olai.in/jjude/build-wealth.png" alt="Options for generating wealth" class="w-100 db black-10"/>
            <dl class="mt2 f6 lh-copy">
            <dt class="clip">Artist</dt>
            <dd class="ml0 gray truncate w-100">Options for generating wealth</dd>
            </dl>
        </a>
    </div>
    <div class="fl w-100 w-third-ns pa2">
        <a href="/mckinsey-way/" class="db link dim tc">
            <img src="https://cdn.olai.in/jjude/mckinsey-way.png" alt="The McKinsey Way" class="w-100 db black-10"/>
            <dl class="mt2 f6 lh-copy">
            <dt class="clip">Artist</dt>
            <dd class="ml0 gray truncate w-100">The McKinsey Way</dd>
            </dl>
        </a>
    </div>
  </div>

</article>

