---
title="Looking Back At 2011 With Pride And Satisfaction"
slug="looking-back-at-2011-with-pride-and-satisfaction"
excerpt="I didn't achieve what I set to do. But I'm happy and satisfied."
tags=["retro"]
type="post"
publish_at="29 Dec 11 16:30 IST"
featured_image="https://cdn.olai.in/jjude/looking-back-at-2011-with-pride-and-satisfaction-gen.jpg"
---

I started 2011 with a long list of to-dos. As the year is coming to a completion, its clear that I haven't done most of them. Yet, I'm finishing the year with pride and satisfaction.

Serendipity is the answer for that paradox.

Before I go on further, let me put some context in place.

I have been an independent consultant for the past three years in the domain of e-governance.  I am lucky to get into the [best e-governance project](http://mca.gov.in) in India. I interact with the best of minds. The professional exposure I get is both wide and deep. Surely there are days of frustrations but I strongly believe that this is a one time opportunity to give back to the country and that feeling alone carries me through bad days.

But I realized, at the beginning of the year, that I have put all my eggs into a single basket. Oh hell, there was just one egg!

I am fine with uncertainties[^1]. But having been a father recently I wanted to ensure that I could put food on the table continually. So I wanted to either get out of this project and get into multiple engagements or generate additional revenue by creating products.

Thus, my goals for the year fell into two categories:
 1. Increased Branding: I set myself a goal of posting an entry in this blog twice a week. I hoped that persistence and minor improvements over long period will improve my writing and then I will be able to pitch to and write for magazines;
 2. Increased Revenue: As I said above, either by getting more engagements or by creating products.

It looked simple and for a while seemed as if I am on the way to achieve them.

But things changed; changed for better.

There was new executive (secretary) in the Ministry and he turned the tables upside down. He forced everyone out of their cocoons and most of us were enthused about new ideas and challenges. Needless to say we were deluged with work.

Though I was working odd and long hours, I enjoyed. I was either framing a new process or supervising the implementation of a process; sometimes improvising a process as we went ahead implementing one. I am proud of all these processes. I can't list all of these processes here[^2] but suffice to say I forgot all about my goals and I didn't have time to regret it.

When normality resumed at work, it was already November. Is there anything I could do about the time gone by and goals not achieved?

Yet there was one thing I could do. I could try and create a blog engine. Yes there are [popular blog engines](http://wordpress.org) and I was running this blog on the same.

But writing a blog engine will take me closer home - to programming; and further, I would have created something this year.

With that I went about programming the blog engine and on Dec 25th, I launched it on [Google AppEngine](http://code.google.com/appengine) and named it [minnaedu](http://minnaedu.appspot.com).

Then I did something many consultants avoid - eat my own dog food. I migrated this blog which was earlier running on [wordpress](http://wordpress.org) into my own blog engine.

It felt good.

But there was one more surprise waiting.

Someone with an id [macco](http://news.ycombinator.com/user?id=macco) [published](http://news.ycombinator.com/item?id=3403233) about my blog engine on [hacker news](http://news.ycombinator.com)[^3]. And it reached the front page of hacker news today (29th Dec). Of course it brought my site down because of huge traffice, but it is a pleasurable pain, which I'm willing to endure.

So with two days to go, I'm already happy about this year. It is a good feeling to step into new year with pride and satisfaction.

Happy New Year.

### Retros of the earlier years

I've been publishing annual reviews almost every year. You can [read them](/annual-reviews/) to understand the journey I've come through.

[^1]: I wouldn't have become an independent consultant that too in government project, if I pursued certainty.

[^2]:  May be in the coming months, I will write about my learnings in e-gov space.

[^3]:  It is a community news aggregator for startups and programming geeks.

