---
title="Be a Problem Solver"
slug="be-a-problem-solver"
excerpt="All blog posts on problem solving"
tags=["problem-solving"]
type="post"
publish_at="22 Jan 11 16:30 IST"
featured_image="https://cdn.olai.in/jjude/be-a-problem-solver-gen.jpg"
---

This page lists all the blog posts on the 'Be a Problem Solver' series categorized under different headings.

#### Blog Posts

*   [Ten Commandments of a Problem Solver](/ten-commandments-of-a-problem-solver/) : If you want to have a high success rate in solving problems, live by simple rules.
*   [Don’t frown upon problems](/dont-frown-upon-problems/) :The basic tenet of knowledge industry is, problems aren’t one of the things to deal with; it is the only thing to deal with; so learn to deal with it .
*   [Don't Fail to Ask 'What is The Problem?'](/dont-fail-to-ask-what-is-the-problem/) : How can a problem be solved effectively if the problem is not fully understood? So go on asking questions until problem is clear.
*   [How Ideologies Impact Problem Solving?](/how-ideologies-impact-problem-solving/ "How Ideologies Impact Problem Solving?") : Ideologies of team members greatly impact problem solving. In fact it can impact the way a problem is perceived.
*   [Aim For Success; Not For Perfection](/aim-for-success/) : Hunt for a workable solution within the existing configuration of the system.
*   [Don't Polarize; Maintain a Balance](/dont-polarize/) : Business world is fond of polarization. Especially so when solving problems. Let us see why we tend to polarize.
*   [Is Problem Solving Tying Us To The Past?](/is-problem-solving-tying-us-to-the-past/) : The phrase problem solving may sound like being tied to the past. But if you want a better future, be a better problem solver.
*   [Don’t Replace Backbone With A Wishbone](/dont-replace-backbone-with-a-wishbone/) : Are you wishing for your situations to be transformed magically? Magic only happens to those who step out of their wishing zone.
*   [Talk Their Language](/talk-their-language/) : Are you communicating your solution in a way your client can understand?
*   [Is Problem Solving an Art or Science?](/is-problem-solving-an-art-or-science/): Can you analyze a situation objectively and arrive at a solution statistically? What about handling associated people issues?
*   [Don’t Torture Context To Fit Your Solution](/dont-torture-context-to-fit-your-solution/): Study the problem within its context before applying your favorite solution.

#### Tools, Techniques, Frameworks etc.

*   [An Overview of SODAS Problem Solving Methodology](/sodas/) : An overview of SODAS (situation, options, disadvantages, advantages, solution), a simple methodology to solve business problems.

#### Resources

#### Books

*   [The McKinsey Way](/mckinsey-way/) : Learn how experts handle business problems.

#### Blogs

*   [Rich Repository of Business Cases](/rich-repository-of-business-cases/): It is impossible to shadow executives in various industries to know every challenges they face. Thats where business cases come in handy.

