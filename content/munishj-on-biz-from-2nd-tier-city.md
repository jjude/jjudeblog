---
title="Munish Jauhar on 'Building a business from a tier-2 city'"
slug="munishj-on-biz-from-2nd-tier-city"
excerpt="What would we need to build a successful business from every city in India?"
tags=["gwradio","biz", "coach"]
type="post"
publish_at="15 Feb 22 06:00 IST"
featured_image="https://cdn.olai.in/jjude/munishj.jpg"
---
India has many successful IT companies. But most of them are built from the metro cities like Chennai, Bangalore, and Delhi. With the possibilities of remote work, now entrepreneurs are starting companies in 2nd tier cities. In today’s conversation, I am talking to Munish Jauhar who has built a successful company from Chandigarh. He has experience both as an operator and an investor. I hope this conversation will help the founders thinking of starting their ventures from a 2nd tier city.

<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless src="https://share.transistor.fm/e/db5ca50f"></iframe>

### Topics Discussed
- What's the story behind your company? 
- How did he get the idea to start a company?
- Who was your first client and how did you get that particular client?
- How did you grow from there?
- Did you compete with bigger companies?
- What should Chandigarh do to build a global brand?
- What should founders in 2nd tier city consider before starting their ventures?
- What is the role of mentors in shaping founders?
- What improvement can Government bring in?
- What does living a good life mean to you?

![Podcast with Munish Jauhar](https://cdn.olai.in/jjude/munishj.jpg)

### Connect with Munish:

- LinkedIn: https://www.linkedin.com/in/mjauhar/
- Twitter: https://twitter.com/munishjauhar
- GrayCell Technologies: https://www.graycelltech.com/

## Connect with me
- Twitter: https://twitter.com/jjude
- LinkedIn: https://www.linkedin.com/in/jjude/
- Website: https://jjude.com/
- Newsletter: https://jjude.com/subscribe/
- Youtube: https://youtube.com/gravitaswins
- Email: podcast@jjude.com
- Executive Coaching Program: https://gravitaswins.com

## Your feedback counts

Thank you for listening. If you enjoy the podcast, would you please leave a short review on Apple podcast or on YouTube? It takes less than 60 seconds, and it really makes a difference in finding this podcast. And it boosts my spirits. 😀

I would like to hear what resonated the most with you in this episode. Either you can email me or share it on social media and tag me. Thank you for your support.

Have a life of WINS.

## Amazing tools I use:
- I record all the remote interview with [Riverside.fm](https://riverside.fm/?via=joseph-jude)
- I use [Transistor.fm](https://transistor.fm/?via=jjude) for hosting these podcast episodes

_Gravitas WINS Radio is produced by [Joseph Jude](https://jjude.com/). Be sure to subscribe on [Apple Podcast](https://podcasts.apple.com/in/podcast/gravitas-wins-radio/id1562802449), [Spotify](https://open.spotify.com/show/7LOBjAyjBkechMC0xMIj9E),[Google Podcast](https://podcasts.google.com/feed/aHR0cHM6Ly9mZWVkcy50cmFuc2lzdG9yLmZtL2dyYXZpdGFzLXdpbnMtcmFkaW8=),[YouTube](https://www.youtube.com/gravitaswins), or the podcasting app of your choice._

Please check out other episodes of [Gravitas WINS Radio](/gwr-themes/). For easy consumption, they are categorized by themes. If you are a fellow podcaster, you might be interested in [How I Podcast](/how-i-podcast/) or [tools I use](/i-use/).

## Continue Reading

* ['Inspirations Don't Die' with Sukhwinder Singh](/sukhwinders/)
* [Future Doesn't Happen To Us, Future Happens Because Of Us](/shape-the-future/)
* [If you don't blow your trumpet, how can you expect music?](/blow-your-trumpet/)