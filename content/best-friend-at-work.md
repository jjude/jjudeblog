---
title="Do you have a best friend at work?"
slug="best-friend-at-work"
excerpt="Shoudn't we have best friends at work if we spend most of our waking time there?"
tags=["wins","coach"]
type="post"
publish_at="25 May 22 16:57 IST"
featured_image="https://cdn.olai.in/jjude/best-friend-at-work-gen.jpg"
---


Do you have a best friend at work?

The question made me cringe.

😳 What? Best friend? At work?

Isn't the office supposed to be a place where you go, get things done, and go home to spend time with friends and family?

My very idea of having best friends at work is offensive to me.

I am not in the majority though.

Gallop says

> "Those who [have a best friend at work] are seven times as likely to be engaged in their jobs, are better at engaging customers, produce higher quality work, have higher well-being, and are less likely to get injured on the job.

If we spend most of our waking time at work, and our identity comes from our jobs, shouldn't we then have friends as colleagues and colleagues as friends?

I have a friend who works for a large MNC. She has many friends in the office. She also has a best friend. I have heard many stories about their friendship.

She got an idea for a patent while driving to her office with her husband. After she reached the office, she told her friend about the idea. By the end of the day, he had collected all the information he needed. After six months of working together, they had a patent in their names.
Another time, when she moved house, he helped her transfer her gas connection from one vendor to another.

Having friends at work is beneficial both personally and professionally. The days are more enjoyable and productive when you have best friends at work.

No wonder she has worked in the company for more than 15 years.

Do you have a best friend at work?

## Continue Reading

* [5 Elements Of Magnetic Company Culture](/5-elements-culture/)
* [Have You Changed Your Mind Lately?](/changed-mind/)
* [Leaders Should Read Literature](/read-literature/)
