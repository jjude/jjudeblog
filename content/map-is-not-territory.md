---
title="Map is not the territory"
slug="map-is-not-territory"
excerpt="Maps are imperfect representation of reality but serves a useful function"
tags=["wins","coach","frameworks"]
type="post"
publish_at="08 Jun 22 18:12 IST"
featured_image="https://cdn.olai.in/jjude/map-is-not-territory-gen.jpg"
---


Recently, we flew to Bangalore. Sitting next to me, my son threw a riddle at me:  
  
> I have cities, but no buildings  
> I have forests, but no trees  
> I have rivers yet not water  
> Who am I?  
  
He enjoys stumping me with puzzles. After a few minutes, I gave up. He answered:  
  
> Maps  
  
Of course! The map shows cities, forests, and rivers but no buildings, trees, or water. I ruffled his hair and said, "how clever" and pondered the profound truth in the riddle.  
  
Maps are imperfect but helpful representations of reality that direct us to our destination.  
  
Maps are imperfect because they display only the details required for the function they must perform. They do this by removing all the non-essential details so they can guide us effectively.  
  
We can extend the concept to all models we rely on.
  
For the projects I launch, I put together project plans. My plans are built on Excel. The plans look great with rows of tasks and expected completion dates. Using my years of experience, I add as many risks as possible. Yet, reality beats the Excel model to the ground and beats it to bleeding.  
  
There were no project models that predicted covid and the after-effects of attrition and mental illnesses.  
  
Models are not entirely off the table for me. They are needed. In the words of Eisenhower, plans are useless, but planning is useful. Planning provides an overall perspective. We realize the significance of the map when we get lost. When you have a map, you can reorient yourself and restart the journey even when you are lost.

When I tell you that my brother-in-law was taller than all of his classmates, it helps you visualize everyone looking up to talk to him. But he can't write, "taller than my classmates", in his medical records. Models are useful in certain situations. They become obsolete when circumstances change.  
  
Adapting a good model to a given situation is a difficult cognitive exercise. We stick to a model because it is comfortable or that is the only model we know. Either way, we fail.

Amidst all these thoughts, we started our descent. As the plane descended, I glanced out the window at the trees and buildings below me and then looked at the map of Bangalore in the in-flight magazine. They were alike, yet so different.

### Quote to ponder

“All models are wrong but some are useful.” — George Box

## Continue Reading

* [Gaming As A Mental Model For Successful Business](/games-as-mental-model/)
* [How To Avoid Failures With Thirty Percent Feedback?](/30-percent/)
* [How To Think Structurally?](/structured-thinking/)

