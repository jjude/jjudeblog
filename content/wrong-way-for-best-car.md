---
title="Wrong way to assemble the best car"
slug="wrong-way-for-best-car"
excerpt="Don’t look for best parts. Optimize the interactions between the parts."
tags=["coach","systems"]
type="post"
publish_at="13 Oct 22 16:28 IST"
featured_image="https://cdn.olai.in/jjude/best-car.jpg"
---

There is a wrong way to assemble a car.

Let us say you want to assemble the best car in the world. You decide to get the best parts and assemble your dream car yourself. 

You search for the best engine. Let's say you discover that Rolls Royce's engines are the finest. You pick Rolls Royce for engines. You then search for a gear box. Because all car aficionados agree BMW is a great gearbox, you choose it.

Likewise, you choose the best individual components.

It's time to build your dream car.

Is it possible to assemble a car with these parts? Each part is the highest rated in its category, but it's possible you still cannot assemble a car; and even if you do, it may not be the most efficient.

![Wrong way to assemble the best car](https://cdn.olai.in/jjude/best-car.jpg "Wrong way to assemble the best car")

The vitality of a car comes from the interaction between its parts, rather than from their individual parts. Sum is always greater than the parts.

You might think this mental exercise is stupid. You know what? In our professional and personal lives, we do the same thing as in this thought exercise.

In India, parents look for "best girl" (or boy) rather than compatible spouse (Even better would be to let the kids choose their partners, but that is a post for another day).

For a job, we seek the best candidate, not the one who matches our culture. A company may have the best sales officer, delivery officer, manager, and even members, but will not make much profit because the interaction, not the parts, is what matters.

In the same way, candidates look for the best companies, not the most suitable ones.

You get the point.

Don’t look for best parts. Optimize the interactions between the parts.

## Continue Reading

* [What can super-chickens and system thinking tell us about building a hyper-performing team?](/super-chickens/)
* [How to improve project delivery in an IT services company?](/it-delivery-system/)
* [Are You Learning A New Domain? Visit Its Zoo](/are-you-learning-a-new-domain-visit-its-zoo/)