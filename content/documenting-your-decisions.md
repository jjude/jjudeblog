---
title="Documenting Your Decisions"
slug="documenting-your-decisions"
excerpt="Always document the context of a decision. You'll be surprised how this one activity can improve your decision making capability in the long term."
tags=["coach","sdl","problem-solving","wins","insights","decisionmaking"]
type="post"
publish_at="28 Jan 12 16:30 IST"
featured_image="https://cdn.olai.in/jjude/document-your-decision.jpg"

---

It may come in as a surprise, but there is a peripheral benefit of [working](/serving-the-nation-with-a-delight/) in a government setup, which is that you encounter lot of intelligent and useful ideas[^1].

One such idea was to document the context of a decision. It came from Mr R. Bandyopadhyay, ex-secretary of Ministry of Corporate Affairs.

When you encounter a situation where you need to decide, you may discuss with your buddies, analyze (mostly mentally), decide and move on. If you have a habit of writing a journal, you may note the decision you made. 

But you don't write down the details of the situation, choices considered, point of view of these choices, assumptions considered in favor of a decision[^2]. These details of the decision are lost.

You may ask, why does it matter?

![Decisions](https://cdn.olai.in/jjude/document-your-decision.jpg)

Well, a **decision is meaningful only within the constraints in which it was made**. When those constraints change, the decision certainly appear useless, sometimes even foolish. If you have documented the context then it is just an adjustment of the decision; otherwise you have to go through the process of decision making all over again[^3].

Another long-term benefit is your decision making process improves considerably, thus **increasing chances of better decisions in the future**.

Let me tell you some of the ways in which I have been using this:

I'm a [retail](http://www.investopedia.com/terms/r/retailinvestor.asp) and an occasional investor. I do as much through job as I could do to check details of the company before I invest[^4]. However, the stock market is such that many-times price of the stock go lower than my purchasing price. Before I started noting down the context of the purchasing decision, I use to get nervous about the loss and would struggle to decide if I should sell and cut my loss. The decision was solely based on the current market price. But after I started practicing this idea, I evaluate the situation based on the assumptions I had made and if any of these assumptions and expectations were wrong I adjust the position accordingly.

Additionally, because I could go back and compare notes, my investing decisions have improved too.

Having seen the benefit of this idea, I am trying to implement this in other areas too. Try it and let me [know](https://twitter.com/jjude) if you find it helpful.

[^1]: Ideas abound in government setup. They fail in execution.
[^2]: Refer [SODAS](/sodas/) method of problem solving.
[^3]: In some cases, it is better to have a fresh look at the situation.
[^4]: I may be called by some as value investor.
