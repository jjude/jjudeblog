---
title="What's Wrong With Our Educational System?"
slug="whats-wrong-with-our-educational-system"
excerpt="Five factors that are wrong in our educational system."
tags=["sdl","opinion"]
type="post"
publish_at="04 Apr 11 16:30 IST"
featured_image="https://cdn.olai.in/jjude/school-boys.jpg"

---

Napoleon was my hero in school. I read with passion his meteoric rise from obscurity to French Emperor. I could list historic details of all his battles. In my childish mind, I hated the British for killing 'my hero'. (I read about Napoleon before I learned of British rule of India and when I learned that, I hated them even more).

I was also fascinated with Hindu mythology. I read both Mahabharata and Ramayana much before I read a chapter in Bible. I could recite even ancillary stories in these epics. I believe that the lessons taught through these stories are important in life.

And I liked talking. Though I was (and still) an introvert, I loved standing in front of people and talk. Whatever the topic was, I won a prize.

All of these until I was 15. I was told, as like many of my classmates, that I should concentrate on Math and Science, because only these are important (I didn't understand what that importance was and I still don't understand how they are more important than the ones I liked). I did excel in Math & Science, but my heart was not in it.

Continuing on the same principle, in college too, I ignored Accountancy and Economics (though it was only one paper). I concentrated only on engineering subjects.

![At School in India](https://cdn.olai.in/jjude/school-boys.jpg "At School in India")

Now that I have been in the competitive business market for 16 years including the past two years as an independent consultant, my perspectives on education have changed.

I have realized that existing education model is good for mass production of laborers. But when business world prefers to automate most of the routine tasks, does it still need laborers?

If the current education system is sufficient enough, why should new recruits undergo corporate training programs? Why can't they transition as fully trained professionals from college to organizations?

Based on my experience, here are some of the gaps in the existing educational system:

1.  Business is built on relationships; relationships are built on conversations; and conversations are fueled by language. Thus command of **language is essential to build and survive in business**. Without proper language skills, engineers fail to articulate their side of arguments in conversations. But is literature given required importance in schools? It doesn't even appear in professional curriculum.
2.  Do you walk into a car show-room and ask for a well engineered car, even though you are an engineer? **People don't prefer products with just good-engineering but they choose those with appealing design**. Engineering is given. But labs in engineering schools concentrate on circuits working on boards, not on building attractive products (same is true for software too).
3.  School curriculum promotes a culture of consumption. Students read text books, listen to lectures and attend workshops – all designed towards consumption based learning. Even the exam system is designed to test the ability of students to consume rather than their ability to produce. But the business world thrives on production. **Students should be encouraged to synthesize their learning and create something new**. It shouldn't be a co-curricular or out-of-campus activity. Rather evaluation itself should be based on what they create.
4.  **Students are taught 'how-to' but not 'why'**. This is again a reflection of 'mass-production-of-laborers' attitude. Now-a-days, every student passing out of an software engineering course know how to program in Java. But do they know the reason why companies recruit Java programmers? Do they know it is to provide the best experience to their customers so that they will become repeat customers. In fact CEOs don't care if you program in Java or C++ or even in VB as long as the business expand.
5.  **School ranking system is out of sync with real life**. School ranking system is individualistic. But professional life awards the ability to co-operate with others to produce results. This is seldom taught to students. That is why the top ranked students have difficulty in organizational settings and they go back to academics and research.

Indian market place has become highly competitive and will be more so in the coming years. If our students have to succeed in such an environment, our educational system should be changed, and it should be changed now.

## Continue Reading

* ['Homeschooling In India' With Mahendran Kathiresan](/mahendrank/)
* [Why And How I'm Homeschooling My Kids](/why-how-homeschooling/)
* [Teaching Storytelling To Kids](/storytelling-to-kids/)