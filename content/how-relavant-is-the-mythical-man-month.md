---
title="How relevant is ‘The Mythical Man-Month’?"
slug="how-relavant-is-the-mythical-man-month"
excerpt="One book that every software developer should read to avoid the agony of software develoment."
tags=["books"]
type="post"
publish_at="19 Oct 08 16:30 IST"
featured_image="https://cdn.olai.in/jjude/how-relavant-is-the-mythical-man-month-gen.jpg"
---

Few books grab your attention on the first page and keep it that way until the last. It is surprising that a technical book written by a technical person would be such a one. As I read every page of ['The mythical Man-Month](https://en.wikipedia.org/wiki/The_Mythical_Man-Month)' (MMM), I was thinking to myself, "Damn, I should've read this book in college; or at least when I started with my career". The author, Frederick Brooks, is dealing about those invaluable lessons that I learned hard way over these many years in this field.

Every problem that the author talks about is so relevant today. Fundamental to the book is the 'fallacious and dangerous myth about **interchangeability of men and months**'. Even today when I present effort estimates to stakeholders, the immediate question is, "So if we double the team size, we should half the time to build this application?". Brooks derives his response from a restaurant menu in New Orleans - 'Good cooking takes time!'. Some tasks can't be hurried without spoiling the result.

Or take the problem of **communication**. Though every one talks the same business language (English), their interpretations vary. Often one hears, "Oh! you meant that? I thought it was something else!". Despite plenty of modeling techniques, understanding between parties involved remains a paramount problem in running a software project. (This is compounded in offshore projects).

Those of us who have handled large systems with different functional owners for different modules would have dealt with the issue of '**conceptual integrity**'. I have managed such projects and it is not only difficult to integrate these modules; such systems throw enormous amount of confusion to the end users.

Building **prototypes** and releasing alpha & beta software are a common practices today. Open Source Software Practice advocates 'release often; release early'. Yet, I've witnessed large projects with multi man-year effort and high complexity being developed in isolation after gather user requirements. When the project is released after years of development, the 'actual need and the user's perception of that need' changed; and the project is a colossal failure bringing frustration and humiliation to the technical team.

These are just few of the problems and solutions discussed by Brooks. As I said in the beginning of the article, all the issues discussed by Brooks are surprisingly relevant today. If you are in software stream - as a developer or as a functional analyst or as a manager - you should read this book. It will avoid you going though the path of agony.

