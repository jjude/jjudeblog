---
title="Seven Insights From Two Months of Blogging"
slug="seven-insights-from-two-months-of-blogging"
excerpt="What I have learned blogging regularly."
tags=["sdl","blogging"]
type="post"
publish_at="04 Mar 11 16:30 IST"
featured_image="https://cdn.olai.in/jjude/seven-insights-from-two-months-of-blogging-gen.jpg"
---

Introspection is an essential element in self-learning. Without it, you don't know what works and what doesn't work and why so. So here are the insights from past two months of blogging.

1.  <a name="lesson1"></a>**For better blogging experience, adopt themed blogging** : Reading Guy Kawasaki's [ The 120 Day Wonder: How to Evangelize a Blog](http://www.evancarmichael.com/Entrepreneur-Advice/352/The-120-Day-Wonder-How-to-Evangelize-a-Blog.html), might well be the moment of nirvana for my blogging experience. Because after reading it, I started to blog on '[Problem Solving](/be-a-problem-solver/)' and I find blogging enjoyable since then. In fact, once I limited myself to a theme, topics flow freely. Narrow is the road to success

2.  <a name="lesson2"></a>**To learn a concept, blog about it** : Blogging puts me at once a learner and a teacher. When I started blogging on problem solving, I had reasonable knowledge about it (or I thought so). But as I researched for each topic, my understanding of the topic got better. And when I put words to that understanding, I would search for a better way to communicate it which in turn enhanced my understanding. If you are learning something new - be it developing android applications, project management, investing in equities - I encourage you to start blogging. You can be guaranteed that your learning will leap.

3.  <a name="lesson3"></a>**SEO is for robots; Social media is for people**: I spend minimum hours on optimization for search engines. But I concentrate on improving the quality of the posts. When a post is tweeted by an influencer, you get more traffic which in turn improves ranking with the search engines, which is what SEO aims to achieve. So it is better to write quality contents that people find useful and be willing to recommend to their social group than spend the same hours tweaking your blog to be found by search engine spider.

4.  <a name="lesson4"></a>**Traffic doesn't matter without resulting conversation**: Social bookmarking, social media and content marketing do drive traffic, but what matters is the ensuing conversation with the readers - on the blog or off the blog (via email or twitter). Conversation with readers informs me if the posts are indeed [useful](http://twitter.com/srinivasvivek/status/35563381981184000) and sometimes, gives me an alternative perspective on the topic. That is why you should leave your feedback on these posts.

5.  <a name="lesson5"></a>**Build an ecosystem for learning**: I follow consume-produce-engage model for learning and understandably there is no single tool for the entire workflow. I consume information using tools like [NetNewsWire](http://netnewswireapp.com/), [Google Alerts](http://www.google.com/alerts), [Mendeley](http://www.mendeley.com/); I use [Pencil](http://www.pencil-animation.org/), [MindNode](http://www.mindnode.com/), [SmartDraw](http://www.smartdraw.com/), [Jing](http://www.techsmith.com/jing/) and [Evernote](http://www.evernote.com/) for producing sketches, screenshots, diagrams and drafts; and I engage with readers via [Wordpress](/) and [Twitter](http://www.twitter.com/jjude). Of late, I am doing substantial amount of these activities on [Android mobile](http://www.htc.com/). I am hooked to Google Reader, [Evernote](https://market.android.com/details?id=com.evernote), [ThinkingSpace](https://market.android.com/details?id=net.thinkingspace) and [Seesmic](https://market.android.com/details?id=com.seesmic) on mobile. Thankfully, all of these applications integrate well with each other and so I am able to concentrate on what I want to do rather fighting with these tools.

6.  <a name="lesson6"></a>**Want to promote? Reach out to prospects**: Traffic to this blog comes primarily from search and social media mentions. But here is another way I found to build traffic : pitch to prospective readers by way email marketing. Pitching helped me to evaluate the audience targeted which in turn enhanced the content.

7.  <a name="lesson7"></a>**Let the readers surprise you**: I write every post with a zeal. Yet, I will like a post little more than others because I attempted something [new](/how-ideologies-impact-problem-solving/) like drawing or it was a new [learning](/sodas/) for me. In all these cases, I would expect that readers would go agog too. You know the reality that it is not so. Readers are fascinated by an [interview](/dont-set-a-goal-without-knowing-your-passion/), [book review](/making-breakthrough-innovation-happen/) or [something I didn't spend too much time](/quotes-from-a-game-plan-for-life/) on. Then again, that is the experience of all creative artists.

Blogging is exciting and I plan to continue learning through it. What about you? What model do you follow for learning? What are the tools in your ecosystem? Let me know.

