---
title="My video log"
slug="vlog"
excerpt="List of videos"
tags=["wins","video"]
type="page"
publish_at="14 Nov 20 14:27 IST"
featured_image="https://cdn.olai.in/jjude/vlog-gen.jpg"
---


I started posting videos to [Youtube](https://www.youtube.com/channel/UCPDIeR7Lzvt9uATvMZSZX-Q) & [LinkedIn](https://www.linkedin.com/in/jjude/) on a regular basis. Here are the links to those videos:

1. [Don't Shave That Yak](https://youtu.be/sJKRtfPNX10): Yak shaving refers to a task that leads you to related tasks, distracting from achieving the original goal. [Post](/shave-the-yak/).
1. [Make one decision not hundred](https://youtu.be/Ebqz6tq-nWg): Peter Drucker is a proponent of "making a lead decision that removes hundreds of other decisions." How can you apply lead decisions in your life? [Post](/lead-decisions/)
1. [What is your ideal job?](https://youtu.be/wnNMmtP2ckY): Whenever we search for a job, we compare salary as the only factor to decide. But money is only a small part of job we enjoy. What are the other factors you can consider? [Post](/ideal-job/)
1. [What is your personal flywheel?](https://youtu.be/dniPZXiB4cU): Flywheels are difficult to move at first. Once the momentum builds up, the momentum itself will sustain the rotation of flywheel. My personal flywheel is Insights, Network, Wealth, and Self-control.
1. [Four types of luck](https://youtu.be/x827KDs13PQ): Blind luck, Active luck, Discerned luck, and Personality luck. [Post](/luck/)
1. [Types of books to read](https://youtu.be/IEu7gCf1USI): we can ready three types of books. Trunk books lay the foundation for a field, branch books explains a particular area of the field, and leaf books teaches one skill in the field. [Post](/book-types/)
1. [Throwing stones locally and crowns globally](https://youtu.be/JGxpfq1THvQ): When you see the other garden green, it is time to water your garden. Only then you can create a space for you to thrive, excel, and celebrate. 
2. [See Everything, Get Nothing](https://www.youtube.com/watch?v=hdU8ISnJsxE): If you want to achieve something in life, you need to control your monkey mind so you can keep your goals at the front of your mind. Only then you can think of many ways to achieve it or think of partners who can help you achieve it. 
3. [Ideas don't matter](https://www.youtube.com/watch?v=57lza7qqgwQ): Everyday we read, watch, listen to some of the greatest ideas floating around the Internet. We might even comment on it, share it, or even save it. None of that matters. The only thing that matters is, what are you going to do about that idea? When are you going to show up? How are you going to make that idea yours?
4. [Techniques To Build Career, Not Jobs](https://www.youtube.com/watch?v=CdERSJKYQqM): To thrive in “career, not jobs” model, you need three techniques. What are they? Read more [here](/future-of-jobs/). 
5. [Future of Jobs](https://www.youtube.com/watch?v=9EHyKEB7h1E): We will collaborate to create and capture value. Do you agree? Read more [here](/future-of-jobs/). 
6. [What skill will be in demand because of remote work?](https://www.youtube.com/watch?v=q4XMxhcU3Pk): Remote work is becoming a norm. What skill is needed to thrive in this remote work world? Watch the video. 
7. [Becoming a full-stack employee](https://www.youtube.com/watch?v=xrCwnk2aZP0): If you want to become a valuable employee, there are four skills you should learn. What are they? Watch this video to find out. 
8. [Tension of hope and reality](https://www.youtube.com/watch?v=dZcdpRGJA7k): If you think you respond to uncertain times with bubbling optimism, you should watch this video to learn why that is not the best response. 
9. [How to build a cyber-safe home](https://www.youtube.com/watch?v=mMqi7sCkdws): You can build as many technology controls as you want. Kids are going to break it if you don't have this one thing in your home.
10. [How to prepare for next level](https://www.youtube.com/watch?v=kNG6dCsxVQ8): First step is preparing for the level. Ask your managers and mentors to find out what is required to prepare for that level. Watch the video for the other two steps.
11. [How to manage anger](https://www.youtube.com/watch?v=79px9Gc4vQY): Control the Monkey Mind. 
12. [I made mistakes](https://www.youtube.com/watch?v=w-Es6tRYCkU): Three mistakes I made in life. 
13. [How to approach appraisal?](https://www.youtube.com/watch?v=sst-1al4S_o): A three-part framework to approach your appraisals. 
14. [Should business owners learn to code?](https://www.youtube.com/watch?v=jMerIndbTis): After this Corona pandemic, everything is going to move to digital. Does that mean small business owners learn to code? I answer that question in this video. 2020
15. [11 lives to build a rich career](https://www.youtube.com/watch?v=swHmwqJCzHg): You take about 7 years to master a skill. You can continue to learn new skills to build a rich and valuable career. 
16. [The curse of everything and now](https://youtu.be/gFpzLMuT_3c): Successful business-men advice you to learn as many business functions as you can learn. This gives you a strong foundation to build your career. But you should be aware of a mental trap that comes with it. 
17. [Specialist or Generalist?](https://youtu.be/dcpLmoXnSUI): There was one question that occupied my mind in the early part of my career. Should I specialize in a single technology or learn as many business functions as I could learn? In this video, I talk about the answer. 


