---
title="NEFT/RTGS as a payment option for governmental transactions"
slug="neftrtgs"
excerpt="Process flow diagram for introducing electronic transfers for delivering public services."
tags=["tech","payment"]
type="post"
publish_at="21 Sep 12 16:30 IST"
featured_image="https://cdn.olai.in/jjude/neft.jpg"
---
Many public service transactions are delivered after a service fee, and usual mode of payments is paper based challans, which are paid at the bank's counter. With most departments becoming e-savvy, they are introducing payments through credit card and net banking.

Reserve Bank of India (RBI) has introduced National Electronic Fund Transfer (NEFT) and Real Time Gross Settlement (RTGS)[^1] as modes of domestic money transfer. These two modes can also be utilized as payment modes for government service delivery.

NEFT/RTGS - called henceforth as Electronic Fund Transfer(EFT)- is a simple money transfer mechanism where a payee transfers funds to a recipient's bank account. Payee is given a unique transfer number (UTN). This UTN can be then used to verify the payment for service delivery.

To introduce EFF based payments, the department has to have a bank account. It is preferable to have a dedicated account for this purpose. Service seekers should be instructed to transfer funds into this account. And the bank should report to the department (system to system reporting) with the transaction details, including UTN. Once transaction details are reported, then the user can seek the service quoting the UTN.

A flow diagram might explain this better:

![NEFT Process](https://cdn.olai.in/jjude/neft.jpg)

By introducing EFT based payment mode, the department can accept payment from any domestic bank. Otherwise department has to either integrate with Internet banking system of each bank, and thus deal with complexities of reconciliation with that many banks or integrate with a payment aggregator like [BillDesk][2].

EFT based payment mode provides a greater flexibility for users too, especially where there are intermediaries. In any other online payment modes credentials should be shared with the intermediary, but in EFT mode of payment, it isn't necessary.

NEFT is not without hassles though. Some of the concerns to consider are:
1. If the service is a time-bound regulatory function, department should verify that the payment was indeed made prior to due date;
2. For NEFT payments few banks generate both a UTN & a reference id. Often users mix these ids. This is not the case in RTGS based payments;
3. If there are other forms of payment besides EFT mode of payment like challan-based payments, users will mix these-as an example, they may choose challan-based payment in the portal but effect a EFT transfer or vice versa. The system should be capable of handling such scenarios;
4. There should be a defined process to reverse or refund payments, for unfulfilled service requests.

NEFT/RTGS provide flexibility and convenience to departments and its stakeholders and it should definitely be one of the payment options. But care should be taken to tweak it to meet department's process.


[2]: http://www.billdesk.com/

[^1]: http://www.rbi.org.in/scripts/FAQView.aspx?Id=65
