---
title="Xavier Roy on 'Personal knowledge management for career growth'"
slug="xavier-pkm"
excerpt="We are in a knowledge economy. Why are we not talking about managing knowledge?"
tags=["gwradio","coach"]
type="post"
publish_at="04 Apr 23 06:01 IST"
featured_image="https://cdn.olai.in/jjude/xavier-yt.jpg"
---

Whether you are a developer or a project manager or even a CEO, managing knowledge is a key part of your daily routine. But we don't get any formal training in personal knowledge management. In this episode, I discuss with Xavier Roy, a content engineer about personal knowledge management, tools, and even how you can get started with it.

Hope this episode is inspirational and educational too. Enjoy the conversation.

<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless src="https://share.transistor.fm/e/88706bba"></iframe>

## Topics discussed

- Why we need PKM?
- Xavier's PKM Process
- PKM Tools
- What information to store in PKM wrt career?
- How to store articles found on the web?
- How do you find what you need?
- How to keep the PKM details up to date?
- Sharing notes
- Getting started with PKM
- Mistakes people commit when they start with pkm
- Good habits for PKM
- Kindest thing anyone has done for you
- Definition of living a good life

## Connect with Xavier Roy

- Twitter: https://twitter.com/xavierroy
- LinkedIn: https://www.linkedin.com/in/xavierroy/

## Connect with me
- Twitter: https://twitter.com/jjude
- LinkedIn: https://www.linkedin.com/in/jjude/
- Website: https://jjude.com/
- Newsletter: https://jjude.com/subscribe/
- Youtube: https://youtube.com/gravitaswins
- Email: podcast@jjude.com
- Executive Coaching Program: https://gravitaswins.com

## Your feedback counts

Thank you for listening. If you enjoy the podcast, would you please leave a short review on Apple podcast or on YouTube? It takes less than 60 seconds, and it really makes a difference in finding this podcast. And it boosts my spirits. 😀

I would like to hear what resonated the most with you in this episode. Either you can email me or share it on social media and tag me. Thank you for your support.

Have a life of WINS.

_Gravitas WINS Radio is produced by [Joseph Jude](https://jjude.com/). Be sure to subscribe on [Apple Podcast](https://podcasts.apple.com/in/podcast/gravitas-wins-radio/id1562802449), [Spotify](https://open.spotify.com/show/7LOBjAyjBkechMC0xMIj9E),[Google Podcast](https://podcasts.google.com/feed/aHR0cHM6Ly9mZWVkcy50cmFuc2lzdG9yLmZtL2dyYXZpdGFzLXdpbnMtcmFkaW8=),[YouTube](https://www.youtube.com/gravitaswins), or the podcasting app of your choice._

Please check out other episodes of [Gravitas WINS Radio](/gwr-themes/). For easy consumption, they are categorized by themes. If you are a fellow podcaster, you might be interested in [How I Podcast](/how-i-podcast/) or [Tools I Use](/i-use/).

## Continue Reading

- [P. K. Khu₹ana on 'Happiness, Joy and Bliss'](/khuranapk/)
- [Rishabh Garg on 'Power of Twitter, Side-hustle, and Community'](/rishabhg/)
- [Mohan Mathew on 'Management Consulting'](/mohanm/)