---
title="Video - Truth About Passive Income & Financial Freedom"
slug="pasive-income-truths"
excerpt="Focus on building financial confidence, not financial freedom. When you have financial confidence, you can generate 4 types of income."
tags=["coach","wealth","video"]
type="post"
publish_at="02 Jul 18 10:30 IST"
featured_image="https://cdn.olai.in/jjude/passive-income-truths.jpg"
---
[![Truth about passive income](https://cdn.olai.in/jjude/passive-income-truths.jpg "Truth about passive income")](https://www.youtube.com/watch?v=jmNdho2svuc)

One of my newest colleague, Adhyuth, can't stop talking about [Dan Lok](http://danlok.com/). I wanted to find out for myself who this Dan Lok guy is and what is so special about him.

I started watching some of his videos and man, he is damn good. Here is one video that popped out for me. This an hour-long video, but worth it.

Here are my notes from this video. It is not a transcript, but my notes — what I learned from the video.

- It is not how much you make, it is how you make the money that matters
- The truth will set you free but first, it will piss you off

### 4 types of income

There are **4 types of income**:

- **Linear** income: You exchange time for money. Examples: Salary, overtime, commission, consulting, professional services
- **Leverage** income: Leveraging other people to make money. Examples: sub-contracting, salaried staff, strategic alliances
- **Passive** income: Income while not working. Example: renewals, interests, dividend
- **Windfall** income: Sudden large income. Example: winning lottery, appreciation of stocks and real estate

### 3 Myths blocking you from earning massive passive income

### Myth #1. Passive income is permanent

- Traditional definition (by Robert Kiyosaki in [Rich Dad, Poor Dad](https://amzn.to/3ot7DyB)): Financial freedom occurs when your passive income exceeds your expenses.
- If you make money from Adsense or affiliate marketing, the other party (Google, Amazon) can change their algorithm. Then you lose all the money or at least passive income goes down.

**Don't aim for financial freedom. Aim for financial confidence.**

Knowing you have the skills to make money anywhere, anytime is the true freedom. Invest in yourself and **learn high-income skills**.

### Myth #2: Passive income is effortless

- In school, we learn skills for linear income. School system was invented to create employees. We are conditioned to get a cheque every month.
- We are not equipped to generate other types of incomes
- Successful business people like Warren Buffet or Mark Zuckerberg, they don't use the word financial freedom or passive income. They got into business to do what they like to do. Every one of them has enough money for financial freedom, but they work long hours. Why?
- It is not because they are greedy. If you are already healthy, would you eat more healthy food and exercise? If you already have a loving relationship, would you continue to be loving?
- It is not greed, it is growth
- **If you are capable of creating value and don't do it, it is a crime against society**. Poor minds think it is greed because they can't create value.
- **There is nothing called passive relation or passive health. Why should there be passive income?** How healthy would you be if your focus is passive health? How passionate your love life would be if you wanted passive sex?
- **You can't create meaning, purpose, or passion passively.**
- Don't buy magic pills that promise to solve all your money problem. Buy courses that will generate your leverage income. Ask yourself **if this course will enhance whatever I'm doing to make money**.

### Myth #3: What you do determines what you earn

- Needy is creepy. People don't want to deal with needy, desperate salespeople. Create value first. Then profit will follow.
- **Money earned is a byproduct of value created**.
- **It is not what you do, but how you do it that makes the difference**. There are people making millions of dollars in marketing and there are people who make no money in marketing. There are people who make millions in real-estate and there are people who make no money in real-estate.

### What it does it take to feel wealthy

Wealth is gratitude.

No amount of money will make you wealthy. Being wealthy is an attitude of mind.

Your nightmare is a fantasy for someone in a developing country. So whatever problem you face is not a problem at all. You are already wealthy (if you can read blogs or watch videos on youtube).

**Only from the place of abundance and generosity, you can create value**.

It is a long video but worth the time. Watch it.

_Disclaimer: Link to Amazon book is an affiliate link. That means, if you buy through that link, I get a commission._
