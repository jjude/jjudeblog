---
title="Sri Lanka Trip"
slug="srilanka"
excerpt="Though Sri Lanka is tiny, it has variety. In our year-end vacation, we visited temples, churches, beaches, national parks, lakes, tea factory, and a spice garden."
tags=["travel"]
type="post"
publish_at="26 Jan 18 15:48 IST"
featured_image="https://cdn.olai.in/jjude/yala.jpg"

---

We went to Sri Lanka in the last week of December for year-end vacation. Though **Sri Lanka is tiny, it has variety**. In the short span, we visited temples, churches, beaches, national parks, lakes, tea factory, and a spice garden.

I **cherish three unforgettable moments** in Sri Lanka:

- riding a Jet Ski in the Gregory Lake,
- watching peacock dance for a peahen in the Yala national park
- swimming in the sea at the Hikkaduwa beach.

We drove about 1500 kms, covering Anuradhapura in the North, Kandy and Nuwara Eliya in the Center, Yala and Hikkaduwa in the South, and Colombo in the West. Throughout our journey, we were **impressed with the fertility of the land and well-laid roads**. Our driver said, Sri Lanka has carpet-roads, which is true. We couldn't see any patches in roads. In India, you can't see a stretch of 50 km road without a patch.

**Indian brands dominate Sri Lankan auto-industry**. Most local buses were Ashok Leyland buses. Tata, Mahindra, and TVS Tyres are other ubiquitous Indian brands.

As a predominately Buddhist country, Sri Lanka is full of Buddhist temples. We visited **cave temple in Dambulla**, which houses sculpture of reclining Buddha. We climbed 300 steps to visit the temple. Climbing that many steep steps was exhausting. But the view from top of the mountain makes it worth the effort.

![Golden Temple, Dambulla](https://cdn.olai.in/jjude/dambulla.jpg)

Sri Lanka has many national parks. We visited the ones in Minneriya and Yala. Minneriya National Park was disappointing. There were more jeeps than the total animals we saw in the park. Traveling in an open jeep was the only consolation in Minneriya.

Yala was opposite. It is **the best national park** I have ever seen. We spotted many birds and animals — green bee-eaters, jungle fowls (which is Sri Lankan national bird), peacocks and hens, painted storks, deers, water buffalos, crocodiles, and elephants. The park ends on a beach, which is a bonus.

![Lucky Dance](https://cdn.olai.in/jjude/yala.jpg)

From Yala, we went to Hikkaduwa. I **swam in sea at Hikkaduwa beach**. It was an amazing experience. When I was a teenager, I had a near-drowning experience. Because of this experience, I had developed a fear of water. To be out of that paralyzing fear and swim in the sea is an amazing experience.

Hikkaduwa and near-by places were **worst hit by Tsunami of 2004**. Approximately 50,000 people died in Tsunami. Tsunami derailed a passenger train near Hikkaduwa, causing the world's worst train disaster. 1,700 people died in this accident. The government has erected a monument in Hikkaduwa to commemorate the losses. It is a solemn moment to stand there to recall the natural disaster.

![Tsunami Memorial, Hikkaduwa](https://cdn.olai.in/jjude/hikkaduwa.jpg)

As you might know, Sri Lanka also battered **three-decade civil war**. As I traveled through Sri Lanka, I often thought of the causes of this civil war and its impact on the Lankan economy. Nationalistic voices are raising in India and Indian politicians are feeding such voices. As nationalistic movements are based on hatred, such movements never result in growth — economic or cultural. I hope Indians learn from Sri Lanka and stay away from destructive nationalistic ideas. (**Patriotism is different from nationalism**. Sydney J. Harris, the journalist defined it well: The difference between patriotism and nationalism is that the patriot is proud of his country for what it does, and the nationalist is proud of his country no matter what it does; the first attitude creates a feeling of responsibility while the second a feeling of blind arrogance that leads to a war).

Sri Lanka is raising from the depths of destruction caused by natural-disaster and civil-war. I hope it doesn't sink again by any one of these.

At the end of the week, I was tired and ready to go home. This is how all journeys have to be.

I posted lot of pictures on Instagram. If you are interested, you can [view](https://www.instagram.com/jxjude/) them there.
