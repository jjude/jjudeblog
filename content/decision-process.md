---
title="What Studying For Exams Taught Me About Decision Making"
slug="decision-process"
excerpt="If we take process-driven decisions, in the long term, we get better results"
tags=["wins","coach","decisionmaking"]
type="post"
publish_at="16 Jun 21 19:37 IST"
featured_image="https://cdn.olai.in/jjude/process-outcome-matrix.png"
---

### Studying for exams

When I was in college, there were times I went to the exam hall without much preparation. It so happened, I cleared those exams.

Can I extrapolate that situation and decide to go to all exams without studying? Would you recommend that?

On the other side, there were times, I studied so hard but I failed the exams.

Again, can I conclude that there is no use in studying?

Today if your son or daughter walked up to you and said, pappa, I studied for one exam and I failed. I didn't study for another exam and I passed. So I am not going to study for exams anymore.

What would you advise your kid?

![Decision Making Process](https://cdn.olai.in/jjude/process-outcome-matrix.png "Decision Making Process")

### Focus on the input side

Most often we focus only on the output side of the decision (results), not on the input side - a process that precedes the output.

Say you want to buy an iPad. Do you buy it on impulse or do you take a thoughtful decision after considering factors like:
- do you need an iPad?
- can you afford an iPad?
- what will you use the iPad for?

and so on...

Say you have a job offer. Do you take the job that pays you the most? Or do you consider your [ideal job](/ideal-job/) and then take a decision?

### Worst Decisions

From this point of view, the worst decisions are not the ones that didn't give us the expected results. The worst decisions are the ones we didn't follow a thoughtful decision process. 

If the decisions didn't get us the result we thought we would get, it could be because either our process was flawed or it was just a bad break.

### Success comes from good process

If we take process-driven decisions, in the long term, we get better results than those that are primarily driven by outputs.

If we learn continuously, we will improve our wealth
If we lead an active lifestyle, we will have life and joy in our everyday life
If we let our kids do what they enjoy, they will excel in what they chose to do

### Action Item

Do you have a process for taking decisions? If so, how often do you follow the process?

If you don't have a process, you can probably start with [SODAS](/sodas/).

## Continue Reading

* [One decision instead of hundred decisions](/lead-decisions/)
* [Documenting Your Decisions](/documenting-your-decisions/)
* [Principles trump processes](/principles-trumps/)