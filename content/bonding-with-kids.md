---
title="Bonding with kids"
slug="bonding-with-kids"
excerpt="How to balance between demands of jobs and the desire to build lasting connections with our children?"
tags=["gwradio","parenting"]
type="post"
publish_at="30 Jan 24 06:07 IST"
featured_image="https://cdn.olai.in/jjude/bonding-with-kids.jpg"
---
Hello and welcome to Gravitas WINS conversations. This is the 3rd episode in fatherhood series that both KK and I are doing. Today we are discussing about bonding with kids.

Bonding with kids is not an easy task, especially when we live in a materialistic world. There is an ongoing demand to work more, earn more, and bring home more. We can easily excuse ourselves for working more saying we are only doing that for kids.

Amidst this, today we want to talk about how we both are taking steps to bond with our kids. I hope you find this actionable and insightful. If you have any questions or comments, feel free to send them to either of us.


<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless="" src="https://share.transistor.fm/e/424428d3"></iframe>

## What you'll hear

- How KK is bonding with his son and daughter
- Three factors to bond with kids
- When telling stories go into details
- Difference between daughters and sons
- Kid's biases develop much younger
- Working in software industry & bonding with kids
- How parents help us to bond with our kids

## Edited Transcript

_coming soon_

## Watch on YouTube
<iframe width="560" height="315" src="https://www.youtube.com/embed/DDbgaO3ZUCo" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

## Connect with KK
- LinkedIn: https://www.linkedin.com/in/storytellerkrishna/

## Connect with me
- Twitter: https://twitter.com/jjude
- LinkedIn: https://www.linkedin.com/in/jjude/
- Website: https://jjude.com/
- Newsletter: https://jjude.com/subscribe/
- Youtube: https://youtube.com/gravitaswins
- Email: podcast@jjude.com
- Executive Coaching Program: https://gravitaswins.com

## Your feedback counts

Thank you for listening. If you enjoy the podcast, would you please leave a short review on Apple podcast or on YouTube? It takes less than 60 seconds, and it really makes a difference in finding this podcast. And it boosts my spirits. 😀

I would like to hear what resonated the most with you in this episode. Either email me or share it on social media and tag me. Thank you for your support.

Have a life of WINS.

_Gravitas WINS Radio is produced by [Joseph Jude](https://jjude.com/). Be sure to subscribe on [Apple Podcast](https://podcasts.apple.com/in/podcast/gravitas-wins-radio/id1562802449), [Spotify](https://open.spotify.com/show/7LOBjAyjBkechMC0xMIj9E),[Google Podcast](https://podcasts.google.com/feed/aHR0cHM6Ly9mZWVkcy50cmFuc2lzdG9yLmZtL2dyYXZpdGFzLXdpbnMtcmFkaW8=),[YouTube](https://www.youtube.com/gravitaswins), or the podcasting app of your choice._

Please check out other episodes of [Gravitas WINS Radio](/gwr-themes/). For easy consumption, they are categorized by themes. If you are a fellow podcaster, you might be interested in [How I Podcast](/how-i-podcast/) or [Tools I Use](/i-use/).

## Continue Reading

- [Mahendran Kathiresan on 'Homeschooling In India'](/mahendrank/)
- [How our fathers, society, and peers shape our fatherhood](/fatherhood-influences/)
- [How we learn about fatherhood?](/learn-to-be-father/)