---
title="How The Damn Google Spoiled My Career"
slug="how-the-damn-google-spoiled-my-career"
excerpt="Degradation of brain power was gradual; but its impact after years later is telling."
tags=["coach"]
type="post"
publish_at="11 Feb 12 16:30 IST"
featured_image="https://cdn.olai.in/jjude/how-the-damn-google-spoiled-my-career-gen.jpg"
---

Usually I'm not an egotist. So please bear with me as you read the next para. There is a reason for it.

My math score was consistently above 95% through school. I even scored 100% in Engineering Mathematics in undergraduate Engineering. In the final year of Engineering college, I wrote computer programs, both in C & Basic, for seven projects, including mine. When I started my career, I knew most of the API commands of Visual Basic by heart that I could program without referring to a manual. When I moved to Windows Programming, I had to refer to manuals but the instances were minimal. Even as late as 2003, I could program without referring to manuals.

Then Google came in[^1].

There were, of course, search engines before Google. But search results weren't precise. Add to it, the Internet wasn't ubiquitous. But by 2003, Internet was available everywhere, all the time, even in India. And Google was God's gift! It gave you the **exact** answer for the question in your mind. Amidst zillions of web-pages, the possibility that you could locate the page that had your answer, was nothing less than a miracle. That miracle was the beginning of rotting of my career.

I was advised by my friends that remembering all the commands is a waste of memory. The prevailing idea, then, was that you knew it or you knew who knew it. That 'who-knew-it' was Google. She[^2] knew everything. In the beginning, I searched even those commands and algorithms I knew well, just to test Google's capabilities. As confidence in Google's results grew, I chose to forget inessentials.

Seven year later, I'm shocked by the consequence of that choice. I can't commit to memory even simple programming concepts like list comprehension in [Python][3]. It is as if my memory is rejecting it saying, 'I'll not store it; go google it."

The effect got aggravated by ever increasing computing power in handheld devices. As a kid, I used to go along with my father for grocery shopping. I was not as good as my father in doing fractions but I could do it. Even as adult I was good at math(I already boasted about that, didn't I?). As more and more computing power was cramped into mobile devices, I used them in the beginning for calculations that involved multiple steps, remembering intermediate results. Slowly, similar to how my brain got complacent towards programming skills, it showed the same signs towards math. It continually said, 'go use your mobile, leave me alone." The metamorphosis was gradual, but after seven years the impact is telling.

Recently, I have become an independent consultant. One of the tasks expected of me is to evaluate proposals and other estimates. The IT parts are piece of cake, but I just simply can't bring my mind to evaluate estimations and quotes. I am not able to do even simple math (like fractions and sum of large numbers). Since I can't evaluate the important part of the proposal - financials, I become mute spectator at the crucial part of the business strategies.

On one side, my programming productivity is down because I have to look-up most of the commands, for which I have to be connected to Internet. Combining that with the fact that I'm loosing my ability to do math, the future is scary.

So my advice to the next generation is, don't sacrifice your memory at the alter of computing devices and internet. I'm trying to take back from the altar-fire whatever I could.

[3]: http://python.org/

[^1]: I mean Google came into my frame of reference only by 2003.
[^2]: Don't ask me why Google is female. That is a story for another day.

