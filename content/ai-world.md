---
title="The World That AI Will Make"
slug="ai-world"
excerpt="What awaits us when AI shapes our world"
tags=["books","insights"]
type="post"
publish_at="28 Oct 20 09:30 IST"
featured_image="https://cdn.olai.in/jjude/ai-world-gen.jpg"
---

Artificial intelligence is a hot topic now. Every startup claims to use AI in their product. They even purchase .ai domains to signal they are ai driven. Whenever there is the madness of the crowd, you can be sure of overflowing delusional thinking.

The cacophony arising out of the euphoria might force many of us to ignore the power of technology. We need an objective assessment of the technology and plausible future scenarios so policymakers, companies, and common men can prepare for the future.  In recent days, I read two books on AI that fit these criteria.

The first one is [The Big Nine](https://geni.us/big-9) by Prof. Amy Webb, and the other one is [2062: The World that AI Made](https://geni.us/2062-ai-year) by Prof. Toby Walsh. I enjoyed Prof. Toby's narration as it was nuanced and thus enjoyable. 

I synthesized the ideas from the two books mixed with my thoughts to imagine what awaits us. If it is any good, read those two books. If it is terrible, blame it on my sloppy thinking and lazy writing.

## AI will permeate everyday lives

Can you think of a week without electricity? Not only for the rich but even for the poor, electricity is weaved into daily lives.  Due to Covid, even software and the internet has become an essential element throughout the day. AI will, likewise, permeate into our daily lives.

AI is used today as recommendation algorithms in the e-commerce and Youtube-like services to distract and extract our attention, all for the companies' benefit. Our lives have not become extraordinarily better because of AI. 

Gmail is one example of how AI can improve the productivity of users. Gmail suggests helpful phrases to speed up the email. The more you use it, the better it gets.

## Digital colonialism is a reality

For AI to function well, it needs data. Every product company on the planet is collecting data about its users. Large companies like Facebook buy data from everywhere to build a rich profile of every interested person in the world.  Not only private companies, but countries like China are gobbling up data too. China is not only collecting data about its citizens but on non-Chinese too. 

All the collected data will lead to political, economic, and social interferences. We will hear of digitally occupied territories soon.

## AI Jobs will reshape jobs

Until Steve Jobs came back to Apple, software geeks dominated corporate discussions. Technologists expected everyone in a meeting to understand the jargons they used. Jobs lifted user-centered design and industrial design to the top of the product building pyramid. The debate shifted from the power of raw technology to how technology can aid users in achieving their needs and desires. Product owners started to look for "customer journey specialists" and "interactive designers" rather than software developers.

Regarding AI, the debate today is around machine learning models, neural networks, and GPT-3s, much like the days when geeks ruled the corporate discussions. Someone like Jobs will come along and make the AI disappear so everyone can use AI for their benefit rather than being forced to fit into the way AI works (or not works). Such a shift will alter many current jobs but will also create more new jobs. 

## Co-learning will alter education, and then life

I can learn anything I desire with Google as the starting point. The information is out there, and I have to search it out. If the information changed, I have no idea about the changes. What if the information updates automatically over the air, like the software updates on the phones and computers? Then I never have to search for updated information. Not just the app updates, the content updates are also automated.

AI takes this concept even further. When one Tesla car learns of a road condition, all Tesla cars learn of that condition simultaneously. This is called a co-learning. Every person in the group learns everything that anyone else in the group learns. 

## We will adapt

There is a great deal of fear about the uncertain future that AI can shape. The greatest strength of the human mind is its adaptability. We adapted to changes brought by telecommunication, air travel, mobile communication, and smartphones. We, as a species, will adapt to AI too. There is nothing to fear there.

## Continue Reading

* [How to find trends in your industry](/trends-in-industry/)    
* [Sweet home is a safe home, even digitally](/sweet-home-safe-home/)    
* [How to deliver value in the digital age?](/value-in-digital-age/)

