---
title="The dichotomy of contentment and ambition"
slug="contentment-and-ambition"
excerpt="The answer is not or, but a curious combination of both."
tags=["idea","coach","wins"]
type= "post"
publish_at= "12 Aug 20 10:00 IST"
featured_image="https://cdn.olai.in/jjude/ambition-contentment.jpg"
---

Can you be ambitious and yet feel fulfilled? 

I have wrestled with this question for two decades. The answer to life's most troubling philosophical choices is not "or" but a curious combination of both. If you are interested in the journey as much as my resolution, read on. Else, skip to the end.

![dichotomy of contentment and ambition](https://cdn.olai.in/jjude/ambition-contentment.jpg "dichotomy of contentment and ambition")

I am from a [small town](https://en.wikipedia.org/wiki/Thoothukudi) in South India. My parents and extended family are school teachers. Naturally, I wanted to be a teacher when I grew up. But I wanted to "be ambitious," so I aspired to become a lecturer in a college. That was the extent of my ambition as a young boy.

Then I got into an engineering college. My "ambition" became "a lecturer in an engineering college," because I thought big!

When in college, I started reading the Bible, which led me to cognitive dissonance. St. Paul says, godliness with contentment is good. He goes on to say, "I have learned to live in abundance and in need." But this is the man who single-handedly took Christianity to all known parts of the world of his time. He writes about his ambition to spread that idea everywhere. **Should I follow what he said or how he walked?**

Should I be content, which leads to lethargy, or should I be ambitious, which seems to lead to pride and disappointment? Is this a paradox to explore or a dissonance to solve?

I read as many books on this topic and listened to philosophers and yogis. I oscillated between experimenting with contentment and ambition. I didn't get a clear answer. The answer was always right there in front of me, but I had to pursue persistently to show that I was desperate for the answer.

Recently, while thinking more about the topic, everything came together. There are three aspects to life in this context: being, doing, and having.

* Being - what you are; who you are; what characters you exhibit etc
* Doing - what you do for yourself, family, and others.
* Having - what you accumulate in this life - not just tangible things like money, car, and house but also intangible things like fame, network.

I realized that Paul was **ambitious in being and doing but content in having**. 

I should be ambitious in being and becoming a better person; I should be ambitious in doing good for others and myself; I should be ambitious in making this world a better place; I should be ambitious in fulfilling my purpose in this life. **It's no humility to live a lesser life than the one you are capable of living.** At the same time, I should be **content with what I have achieved, acquired, and accumulated.**

As the Chinese say, this is yin and yang. You need to have this duality at the same time. Only then you feel fulfilled. Just because I stumbled on this doesn't mean I am practicing it, but there is great peace in resolving it.

## Continue Reading

* [Tension between hope and reality](/hope-and-reality/)
* [Approximately correct](/approximate/)
* [Life is series of lost, found, and lucky breaks](/life-is-lost-and-found/)