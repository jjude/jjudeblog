---
title="I’m a proud Indian"
slug="im-a-proud-indian"
excerpt="Can we be really proud?"
tags=[""]
type="post"
publish_at="15 Aug 08 16:30 IST"
featured_image="https://cdn.olai.in/jjude/im-a-proud-indian-gen.jpg"
---

On this lovely morning of August 15th, every Indian should feel proud to be an Indian.

We are establishing our footprints everywhere - sports, space, art, literature - and we are successful too.

Everyday there is a news to be proud of being an Indian - some Indian company is buying a British or an American company; someone is winning a gold medal in Olympics; armies of super-powers want to practice with Indian armies; and so on and so forth.

We hit big and we win big.

Or are we?

From the glories of achievement, let us come down to the every day life of common man.

As a proud Indian, I would like to live anywhere in India - safely. Isn't it my right as an Indian? A Tamil in Karnataka or may be in Maharastra. While this is true on paper (constitutionally), how much it is true on reality?

If a Kanada actor is kidnapped, Tamils are targeted; if he dies, Tamil shows are banned. Thousands flee Maharastra in a day because they are not natives.

Within the same country there is a feeling of 'four squares of my native land'. We are not British occupying your land; we are fellow Indians coming for living.

As much as we identify as Malayali Nair or as Tamil Nadar, we fail to identify as an Indian. Being Indian is not in our blood - being a thevar is; being a Tamilian is; being a Christian is. There is an identity crisis for Indians.

Let us talk about succeeding in India. Do you think, despite paying the hefty taxes, there is an encouraging climate for success - in sports, in business, in art? None could've said it better than the coach of [Bindra](https://en.wikipedia.org/wiki/Abhinav_Bindra), the golden boy of India - 'do you know why he won? He bypassed the system; that is why he could win'. He hit the bulls eye!

What about politics?

Politicians moan that they don't have enough authority to reform the country. I don't think authority is an issue - Hypocrisy and dishonesty is.

You might counter me with a question, &quot;Come on, it is in politics of every country; they take bribes too&quot;. I might agree with you on them taking bribes. But there is a difference. As [Kamal](https://en.wikipedia.org/wiki/Kamal_Haasan) says in one of his [movies](https://en.wikipedia.org/wiki/Indian_(film)),&#160; &quot;In other countries, they bribe in order to stop the official from doing their job; only in India you have to bribe to do their job&quot;.

Bribe, as a cancer, has gone deep into our system.

I'll tell you a true incident. I know it is true because it happened to my father. When my father retired, he went to district office to collect his dues. The amount was what was due for having worked for years as a teacher. The peon told him, &quot;You are getting so much; give me 500 rupees; only then I'll dispatch the cheque&quot;

All that the peon had to do was to give that cheque to my dad. He was demanding &quot;something&quot; for doing his job. (My dad being a patient and principled man, he refused. So he was forced to go back again and again. He went until he got his cheque. You could argue that he could've lodged a complaint. Tell me honestly, will that work?)

Bribery is now part of Indian government system, every damn system, including defense ministry. I was so ashamed reading that it was there even in ordering coffins for those who died in Kargil war! That is the extent of bribery.

These are not all. There are many.

But I'll hide my frustrations, insecurity, shame and salute 'Mother India' on this great day, as a proud Indian.

Jai Hind.

