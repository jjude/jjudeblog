---
title="Guest lecture on entrepreneurship at Christ College, Bangalore"
slug="entrepreneurship-talk-at-christ-college-bangalore"
excerpt="I love talking to college students about entrepreneurship. Recently, I spoke to MCA students of Christ College, Bangalore."
tags=["talks","startup","coach"]
type="post"
publish_at="05 Jun 17 20:37 IST"
featured_image="https://cdn.olai.in/jjude/2017-06-03-christ-college.jpg"
---
If you want to build a better world, you have to start with college students. They are not too old to sink in their own ideas and they are not too young to be thoughtless.

That is why I love to talk to college students.  I have lectured at [Baddi University](http://www.baddiuniv.ac.in/) and [Maharaja Agarsen University](http://www.mau.ac.in/) earlier.

I usually talk about entrepreneurship. Entrepreneurship is a sure-shot way to build wealth, and that wealth can and will transform many lives.

I spoke to MCA students of Christ College, Bangalore about entrepreneurship on 3rd June.  From the get-go, the session was interactive. Two of the students are working a startup idea in the area of fishing. I loved discussing that idea in detail.

![In Christ College, Bangalore](https://cdn.olai.in/jjude/2017-06-03-christ-college.jpg)
_Thanks Ankur for taking this photo_

This post is a summary of what I spoke at Christ College.

I divided the talk into three section:

* Why entrepreneurship
* Types of entrepreneurship
* Ways to start with entrepreneurship

### Why entrepreneurship?
Let us start with the obvious one: **building wealth**. If you succeed, you create wealth for yourself and your early employees. This is where entrepreneurship differs from traditional businesses. The entrepreneurship generally leads to uncharted territories. There is a high chance of failure, but when you succeed, you succeed big.

Wealth isn't the reason, I evangelize entrepreneurship. Wealth is just one of the factor that attracts me to  entrepreneurship.

[Hindol Sengupta](https://twitter.com/HindolSengupta) articulated this well in an Young Indians forum. **Entrepreneurship transforms societies**. India has suffered because of caste discrimination. Today when a delivery guy delivers a pizzza from Dominos Pizza or a book from Amazon, we don't check if the guy is from a reputable caste. Or we don't check the caste of the driver before getting into an Uber cab.

Coupled with urban migration, entrepreneurship is changing the society for better.

### Types of entrepreneurship
Well known type of entrepreneurship is **VC funded one**. In this form, a someone with money, called venture capitalist, gives you money to build your idea into a successful business. In exchange, you give them a share of your company. The funding can start early, when you have just an idea. Generally, investors like to make sure that the idea has a possibility of success. So they will invest when you have few paying customers.

Many of these investors, don't just bring money into the company. They are usually experienced and well connected. They usually bring along with them early customers and operational help.

Second is a **bootstrapped model** to build your idea. You fund your idea, either with your own money or with borrowed money from your friends and family. In this model, you retain most of the control, but you may grow slower than the previous model. [Basecamp](https://basecamp.com/) is a well known bootstrapped company.

Last of the model is **freelancing** or **life-style business**. Here in this model, your objective is not to be become a rich man in the world but to support your lifestyle. You realize that someone else will always have a bigger boat. With that realization, you define the lifestyle you want to live and operate a business to support that particular life style. Others might think you are not ambitious enough; that doesn't bother you, because you don't live as per other's definition. The best lifestyle businessman I know is [Alan Weiss](https://www.alanweiss.com/). He makes millions of dollars every year. If that is not ambitious enough, I don't know what else is.

### Ways to start with entrepreneurship

Every final year student has to undertake a project. You can use that opportunity to develop an idea into a project. Later you can develop that project itself into full blown idea for entrepreneurship. (This is what one of the student, Vincent, is planning to do. His idea is to **connect fishermen and distributors through an app**. As someone coming from the family of fishermen, he understands the entire ecosystem well. He is developing an app with a limited scope for his college project. He plans to develop into a full-blown service after the college. Good luck, Vincent).

Easier way to enter into entrepreneurship is to **start teaching**. There are students who can't understand a subject in English. They can grasp it well, if the subject is taught in their native language. [Sangram Singh](https://www.youtube.com/channel/UCGeivitY_27EA0RqWjtspPw) makes educational videos in Hindi to help students pass exams. All you need is a smartphone and internet connection. You will learn many aspects of running a business, especially online business. This is the easiest for students to get on to the entrepreneurship.

Some of the students have already decided on following entrepreneurship route. So we had healthy interaction about their ideas. I'm sure some of them will turn into respected business leaders of Karnataka. Good luck guys.
