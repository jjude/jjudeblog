---
title="Ian Heaton on 'Listening leaders are effective leaders'"
slug="ian-listening"
excerpt="Three core pillars of listening, tools, being assertive while a great listener, and much more..."
tags=["coach","gwradio"]
type="post"
publish_at="13 Jun 23 17:40 IST"
featured_image="https://cdn.olai.in/jjude/ian-yt.jpg"
---

When you think of a leader, any leader, what images come to your mind? A great orator? Whether it is JFK or Obama or Steve Jobs, that is what we think. Today’s guest says, a great leader is someone who listens. Hmmm…let’s find out more.

<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless src="https://share.transistor.fm/e/749c0743"></iframe>

## What you'll listen

- How Ian transitioned from finance to listening
- Three core parts of listening
- Tools for listening
- How can we listen while being assertive?
- Having tough conversations
- How to practice listening well?
- Role of listening in virtual workplaces
- Kindest thing anyone has done for Ian
- What it means to live a good life


## Your feedback counts

Thank you for listening. If you enjoy the podcast, would you please leave a short review on Apple podcast or on YouTube? It takes less than 60 seconds, and it really makes a difference in finding this podcast. And it boosts my spirits. 😀

I would like to hear what resonated the most with you in this episode. Either you can email me or share it on social media and tag me. Thank you for your support.

Have a life of WINS.

_Gravitas WINS Radio is produced by [Joseph Jude](https://jjude.com/). Be sure to subscribe on [Apple Podcast](https://podcasts.apple.com/in/podcast/gravitas-wins-radio/id1562802449), [Spotify](https://open.spotify.com/show/7LOBjAyjBkechMC0xMIj9E),[Google Podcast](https://podcasts.google.com/feed/aHR0cHM6Ly9mZWVkcy50cmFuc2lzdG9yLmZtL2dyYXZpdGFzLXdpbnMtcmFkaW8=),[YouTube](https://www.youtube.com/gravitaswins), or the podcasting app of your choice._

Please check out other episodes of [Gravitas WINS Radio](/gwr-themes/). For easy consumption, they are categorized by themes. If you are a fellow podcaster, you might be interested in [How I Podcast](/how-i-podcast/) or [tools I use](/i-use/).

## Connect with Ian
- LinkedIn: https://www.linkedin.com/in/ian-heaton-coaching/

## Connect with me
- Twitter: https://twitter.com/jjude
- LinkedIn: https://www.linkedin.com/in/jjude/
- Website: https://jjude.com/
- Newsletter: https://jjude.com/subscribe/
- Youtube: https://youtube.com/gravitaswins
- Email: podcast@jjude.com
- Executive Coaching Program: https://gravitaswins.com

## Continue Reading

* [Ranjitha Jeurkar on 'Nonviolent Communication'](/ranjithaj/)
* [Meenakshi on 'Carving a unique career path'](/meenakshi-career/)
* [Ubellah Maria on 'Transitioning from developer to manager'](/ubellah-maria-becoming-a-manager/)