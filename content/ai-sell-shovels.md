---
title="Selling shovels in Generative AI gold rush"
slug="ai-sell-shovels"
excerpt="Should you invest in building an AI model? If you are smart, you would stay away from it."
tags=["tech","aieconomy","gwradio"]
type="post"
publish_at="01 Aug 23 11:25 IST"
featured_image=""
---

_This is the 75th episode of Gravitas WINS Radio. Thank you for all your support in listening, commenting, and spreading the word about the podcast._

<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless src="https://share.transistor.fm/e/6c5e6a2c"></iframe>

Twitter and LinkedIn are full of generative AI news. Both mainstream media and bloggers are discussing chatGPT, stable diffusion, and other versions of Generative AI. There are billions of dollars getting invested in AI. So should you develop a product in this space?

The metaphor "**sell shovels during gold rush**" originated during California's gold rush. The saying means that when you recognize a gold rush is happening, don't try to be the 1,000,001st person to mine for gold, because you will probably fail. Instead sell something that all the millions of gold miners need, like shovels and get rich from selling them.

That is exactly what Levi Strauss did. He realized all these miners need a reliable trousers and he started his "blue jeans" company. Shopify did the same during e-commerce rush. Instead of selling online themselves, they created a platform that other e-Commerce "gold miners" can use.

The idea is **not to chase the obvious prize** everyone is chasing, but **servicing ancillary opportunities** that surround the primary opportunity.

So how are people using this mental model for AI era?

Instead of going after building LLM models or building data infrastructure, smart people are going after the ancillary opportunities like,
- creating tutorials
- coaching on prompt engineering
- issuing certifications
- writing books
- curating newsletters
- curating jobs

and so on. These are the smart people who are selling shovels.

What are the other "shovels" you've come across? [Reply back](https://twitter.com/jjude) with your comments.

Thank you. Have a life of WINS.

_This is part of [Short notes on AI Economy](/ai-economy-notes/)_