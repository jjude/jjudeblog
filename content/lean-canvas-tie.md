---
title="Workshop on Lean Canvas for TiE Young Entrepreneurs, Chandigarh"
slug="lean-canvas-tie"
excerpt="I took a session on 'Lean Canvas' for student entrepreneurs as part of 'TiE Young Entrepreneurs'. Here is the edited version of the session."
tags=["startup","talks"]
type= "post"
publish_at= "04 Nov 19 18:00 IST"
featured_image="https://cdn.olai.in/jjude/lean-canvas.jpg"

---

Only 20% of startups succeed. Do you know why the rest of the 80 percent fail? The reason might surprise you. They fail to solve a problem for a big enough market. Write down two phrases in your note in big, bold letters -- PROBLEM and BIG ENOUGH MARKET. These are so important that you will hear these phrases again and again.

How can you avoid making this cardinal mistake? By thinking clearly and deeply.

As a founder, you are going to repeat two activities all the time -- thinking and communicating. I rely on frameworks for these two activities. Frameworks provide you contours, and then you can play within that broad place.

First, I'm going to give you a framework for thinking, and then we will dive into another framework that will help you communicate the results of that thinking.

Ready?

**PASTOR**

That's the framework for thinking. It is an acronym. Let us see what it stands for.

### P is for persons

You need to identify a large enough section of people who have a problem. It could be South Indian students, working mothers, or artists, and so on.

### A is for ache or pain

The cohort that you identified should experience pain. The impact of pain is so much that they are ready to pay. In a large and diverse country like India, there is no shortage of such pain. You have to go out, talk to people, and observe them and find this pain. You can't google this. It could be that South Indian students can't find tasty restaurants serving their favorite food, or parents are looking for reliable tutors for their kids.

### S is for scenarios

You have to understand the scenarios under which they experience the pain. Is it multiple times a day, is it when they get ready to go to the office, is it when they want to purchase a product, and so on. Such an exercise should tell you the frequency and the severity of the pain. These two factors -- frequency and severity, will guide you to decide how much you can charge them for your solution.

### T is for transformation

Your solution should not just improve their life but transform them. Your customers should shout aloud, saying, "I wasn't able to follow my passion for jogging because of wheezing, but now after using this inhaler, I can jog daily. I am so happy."

### O is for offer

Offer is your answer to the pain your potential customers suffer. Traditionally it was either a product or a service. But when you launch your startups, you will have to add "experience" to the type of offerings. You can offer an instant coffee powder, or a filter coffee or an exquisite dining ambiance created especially for coffee serving. If you add experience to your offering, you can charge a higher premium. When I think of experience, I admire [Auto Anna of Chennai](http://www.amazingauto.in/). He has transformed a banal, daily auto rides in Chennai into a memorable experience.

### R is for reward

All the above points are about creating value for the set of customers you identified. This step is about capturing part of that value. What is your reward for doing all that for your customers? What kind of revenue are you going to have?

### Need for Lean Canvas

Once you have thought deeply about each factor and found answers, you need a mechanism to communicate your ideas to others in the team — your team members, investors, prospects, and so on. That is where "Lean Canvas" comes into play.

Lean Canvas captures all points essential to a startup on a single page. The canvas has nine blocks and arranges them in such a way that the left side describes the product, and the right side depicts the market.

![Lean Canvas](https://cdn.olai.in/jjude/lean-canvas.jpg)

Let us now look at the summary of these blocks.

### Customer Segments

It is the set of people, cohorts, who are facing the problems that you want to solve. Identifying your customer segments is a crucial step. Without identifying the customer segment, you can't talk about their problems.

It is perfectly okay to have multiple customer segments and also sub-segments within a particular segment.

### Problem

The customer segments and their problems are interconnected. You need to think of them together. You will iterate over these two sections many many times to finally say you have a good grasp of the target customer and their problems. Don't limit to just one problem; look for at least 4 to 5 associated problems.

When you are scouting for problems, look for the ones that require pain-killers rather than vitamins. What do I mean, and why? Say you have certain non-threatening vitamin deficiencies, like most of us have. Doctors might advise you to take multi-vitamin tablets. You'll take the medicines for a few days and then forget about them. On the contrary, when you have an ache—headache or stomach ache, you will take the prescribed medicines faithfully. If you solve the aching problems, you can be rest assured your potential customers will use your offer without any additional followup from you.

### Solution

How are you solving the identified problem for the cohort? You have to understand that you are not going to stumble on the solution in a single sitting. It is going to be an iterative process. The best way to test if your solution resonates with your potential customers is to go out, meet them, and talk to them. Don't commit the mistake of conceiving a solution looking at a screen. That never works.

Your solution could be a combination of a product and a service. That is okay. But keep the solution as simple as possible so the customers can understand it quickly. You should also ensure that there is as little friction as possible in getting started with your solution.

### Unique Value Proposition

What makes your solution different from competitors? If you can't differentiate your solution, then you will be forced to compete on price, which is not a suitable pricing strategy for a startup.

There is Starbucks, The coffee beans and tea leaf, and Chayyos in Elante Mall. What sets each of them apart? If they can't articulate it, they are not going to attract business. The same is the case with you.

### Channels

This block is about how you will reach your potential customers. You could use email, SMS, WhatsApp message, Facebook ads, and so on to spread the news about your startup. If it is suitable, you could also use physical channels like schools and studios. The decision on channels becomes easier if you have a precise customer segment.

### Revenue Streams

How are you going to make money? Will customers pay a one-time fee or a monthly fee? Is it a $10 product or a $100 product? Can you offer any other service, like training, in addition to your product so you can build additional revenue streams?

Whatever is your solution, try to build recurring monthly revenue. If you build a product that is purchased only once, then you are constraining yourself to chase after new customers always. Instead, build a service or modify the product in such a way there is recurring monthly revenue.

### Cost Structure

What are the expenses you will incur in creating and running your business? The common ones are employee cost, product development cost, and operational costs. But there are going to be additional costs depending on your business model, like R & D, marketing and advertising, and administration.

There will always be a surprising cost popping up here and there. If you can budget your expenses well, that will help you to plan your funds better, so you stay in business longer.

### Key Metrics

As a founder, you have to commit yourself to make decisions based on data. Otherwise, you will end up driven by hubris.

Few metrics apply to all types of startups: number of orders, the value of each order, frequency of repeat orders, and so on. But there is one key metric for a startup, especially at an early stage, and it is cash in the bank. That's the fuel in your car. You could have a fancy car, a great destination in mind, and fantastic members in the car. But if you don't have fuel in the tank, you are tanked.

Another common mistake that new founders commit is that they look at existing data rather than the data that they needed. Make your own set of the necessary data to assess the health of the company and seek this data.

Ensure the definition of data is the same across the people dealing with it — the team collecting data and the team providing these data to you. As an example, if you are running Zomato, you might want to look at "orders placed." You might think of it as total orders placed even unpaid ones so as to understand the capability of the system. In contrast, those who provide the data might define it as orders successfully placed, meaning total orders that were paid. Such confusions are common, and people will not unearth this difference quickly. So before you make large bets based on data, make sure everyone understands the terms correctly.

### Unfair Advantage

In the investment circles, this is called moat—the ditch surrounding a castle. Without a moat, anyone can attack the castle. Likewise, to starve-off competition, you need to build an unfair advantage.

Say if someone steals your code and establishes a competitive product, will your startup thrive or shutdown? This is the hardest block to fill. You may not have an unfair advantage in the beginning, but you need to keep this in mind and build moats as you build your startup.

Good luck with your startups.

****

After the session, we debated on the ideas the students had and tried filling the lean canvas. I enjoyed engaging with the students because they came prepared with fantastic ideas. One of them wanted to take vertical farming to houses to provide fresh vegetables. Another wished to remove intermediaries in comic book authoring. Each of these ideas was fun to discuss. It was a fun event.

The event was recorded. You can watch it on [YouTube](https://www.youtube.com/watch?v=lsHRFxmSKm4)

## Continue Reading

- [Think clearly about your venture with P.A.S.T.O.R. Framework](/pastor/)
- [Three stages of startups and how to chose tech-stack for each of them](/stacks-for-startups)
- [Guest lecture on entrepreneurship at Christ College, Bangalore](/entrepreneurship-talk-at-christ-college-bangalore)

[1]: https://tie.org/programs/tye-global-program/
