---
title="Books I’ve Read"
slug="books-read"
excerpt="I have read over 100 books. I'm starting to summarize them here."
tags=["books"]
type="page"
publish_at="22 Dec 16 17:20 IST"
featured_image="https://cdn.olai.in/jjude/books-read-gen.jpg"
---

If I'm any smarter, its by reading. That is my way of standing on giants.

In this page, I write summary of the books I read. I posted detailed notes for some of them in this blog. I'll update this page as I read more, so bookmark it if you want to check back in a few months.

## The 7 Habits of Highly Effective People

by Stephen R. Covey / [Amazon Link](https://geni.us/7-habits-bk)

This is one of the two books that changed my life completely. I read this book 20 years ago; yet, the concepts in the book are as valid today as then. The 7 habits are:

1. **Be Proactive**: Always expand your circle of influence; don't get bogged down with circle of concern.
2. **Begin with the End in Mind**: Visualize your future and work backwards to achieve it.
3. **Put First Things First**: Work on important matters prior to they become urgent.
4. **Think Win-Win**: Don't view others are competitors; view them as complementary for your success.
5. **Seek First to Understand, Then to be Understood**: Empathize with others to influence them.
6. **Synergize**: Combine strengths of everyone for a positive outcome.
7. **Sharpen the Saw**: Keep growing.

***

## The McKinsey Way

by Ethan Rasiel / [Amazon Link](https://geni.us/McKinsey-Way) / [Detailed Notes](/mckinsey-way/)

There are three factors to problem solving: **thinking comprehensively about a problem, solving the problem, and selling the solution**. If you want to be an effective problem solver, you need to practice all the three factors.

***

## Steve Jobs

by Walter Isaacson / [Amazon Link](https://www.amazon.com/dp/1501127624?tag=jjude-20) / [Detailed Notes](/steve-jobs/)

Want to change the world? **Develop a deep understanding of these factors: empathy, focus, and impute**.

***

## Zen and the Art of Motorcycle Maintenance

by Robert M. Pirsig / [Amazon Link](https://www.amazon.com/dp/0060839872?tag=jjude-20)

There are **two types of understanding**: classical & romantic. Romantic understanding concerns itself with the immediate appearance; while classical gets into the underlying mechanics. Lines and numbers of a blueprint is dull and boring for a person of romantic understanding but they are fascinating for a person of classical understanding.

***

## The industries of the future

by Alec Ross / [Amazon Link](https://www.amazon.com/dp/1476753660?tag=jjude-20) / [Detailed Notes](/future-industries/)

The future industries are:

1. Robotics
1. Genomics
1. Digital Economy
1. Cyber-Security
1. Big-Data

He finishes with a poignant advice to world leaders: "there is great shame for society and its leaders when a life is made less than what it could be because of a lack of opportunity. **The obligation of those in positions of power and privilege is to shape our policies to extend the opportunities that will come with the industries of the future to as many people as possible**".

***

## The mythical Man-Month

by Fred Brooks / [Amazon Link](https://www.amazon.com/dp/0201835959?tag=jjude-20) / [Detailed Notes](/how-relavant-is-the-mythical-man-month)

Frederick Brooks wrote this book in 1975, with observations from managing development of IBM OS/360, an operating system for a mainframe computer. Even though it is four decades old, all the issues discussed by Brooks are surprisingly relevant today. If you are in software stream - as a developer or as a functional analyst or as a manager - you should read this book. It will avoid you going though the path of agony.

***

## Books still to sort

1. Everything I Know, Paul Jarvis
1. How Will You Measure Your Life, Clay Christensen
1. Warfighting, US Marines Guidebook
1. Stop Stealing Dreams, Seth Godin
1. THE BLUE SWEATER, Jacqueline Novogratz
1. Time Management 2.0: 15 Secrets of a Self-Made Millionaire for Getting Things Done (Coffee With A Millionaire Series), H. Reardon, C. Kane
1. On Writing, Stephen King
1. Bank For The Buck, Tamal Bandyopadhyay
1. Future Shock, Alvin Toffler
1. E-Myth Revisited - Why Most Small Business Don't Work and What to Do About It
1. Seven Steps to Mastering Business Analysis
1. Outliers, Malcom Galdwell
1. A Game Plan for Life: The Power of Mentoring by John Wooden, Don Yeager, Don Yaeger
1. Alchemist, The by Paulo Coelho
1. Alexander the Great's Art of Strategy: The Timeless Leadership Lessons of History's Greatest Empire Builder by Partha Bose
1. All Marketers Are Liars: The Power of Telling Authentic Stories in a Low-Trust World by Seth Godin
1. Antifragile: Things That Gain from Disorder by Nassim Nicholas Taleb
1. Artist's Way by Julia Cameron
1. Bittersweet by Danielle Steel
1. Blue Ocean Strategy: How to Create Uncontested Market Space and Make Competition Irrelevant by W. Chan Kim, Renee Mauborgne
1. Crucial Confrontations: Tools for Resolving Broken Promises, Violated Expectations, and Bad Behavior by Kerry Patterson, Joseph Grenny, Ron McMillan, Al Switzler
1. Deep Survival: Who Lives, Who Dies, and Why by Laurence Gonzales
1. Discovering the Laws of Life by John Marks Templeton
1. Emotional Intelligence by Daniel Goleman
1. Every Business Is a Growth Business: How Your Company Can Prosper Year After Year by Ram Charan, Noel Tichy
1. [Execution: The Discipline of Getting Things Done](https://www.amazon.com/dp/1847940684?tag=jjude-20) by Larry Bossidy and Ram Charan Execution
1. Fail-Safe Investing: Lifelong Financial Security in 30 Minutes by Harry Browne
1. Fooled by Randomness: The Hidden Role of Chance in Life and in the Markets by Nassim Nicholas Taleb
1. FREAKONOMICS: A Rogue Economist Explores the Hidden Side of Everything by Steven D. & Dubner, Stephen J. Levitt
1. FREE: THE FUTURE OF A RADICAL PRICE: THE ECONOMICS OF ABUNDANCE AND WHY ZERO PRICING IS CHANGING THE FACE OF BUSINESS by CHRIS ANDERSON
1. Getting Started in Consulting, Second Edition by Alan Weiss
1. Getting Things Done: The Art of Stress-Free Productivity by David Allen
1. Good to Great: Why Some Companies Make the Leap... and Others Don't by Jim Collins
1. Halo Effect by Phil Rosenzweig
1. Hindu Dharma by M.K. Gandhi
1. How to Lie with Statistics by Darrell Huff
1. How to Win Friends and Influence People by Dale Carnegie
1. How Will You Measure Your Life? by Clay Christensen
1. In an Uncertain World: Tough Choices from Wall Street to Washington by Robert Rubin, Jacob Weisberg
1. Indira: The Life of Indira Nenru Gandhi by Katherine Frank
1. It's Not About the Money: A Financial Game Plan for Staying Safe, Sane, and Calm in Any Economy by Brent Kessel
1. LEADERSHIP. by RUDOLPH W. GIULIANI
1. Learned Optimism: How to Change Your Mind and Your Life by Martin E. P. Seligman
1. Making Breakthrough Innovation Happen: How 11 Indians Pulled Off The Impossible by Porus Munshi
1. Making India Work by William Nanda Bissell
1. Making Of A Champion by SUMRALL LESTER
1. Man Who Invented the Twentieth Century by Robert Lomas
1. Marketing Metrics: The Definitive Guide to Measuring Marketing Performance by Paul W. Farris, Neil T. Bendle, Phillip E. Pfeifer, David J. Reibstein
1. Million Dollar Consulting by Alan Weiss
1. Money Talks: How to Make a Million As A Speaker by Alan Weiss
1. More Than You Know: Finding Financial Wisdom in Unconventional Places by Michael J. Mauboussin
1. Now, Discover Your Strengths by Marcus Buckingham, Donald O. Clifton
1. On Writing Well, 30th Anniversary Edition: The Classic Guide to Writing Nonfiction by William Zinsser
1. Opposable Mind: Winning Through Integrative Thinking by Roger L. Martin
1. Our Films Their Films by Satyajit Ray
1. People Are Never the Problem Pb by Robert Watts
1. Problem Solving 101: A Simple Book for Smart People by Ken Watanabe
1. Project Management Disasters & How to Survive Them by David Nickson Sir
1. Project Management Success Stories: Lessons of Project Leadership by Alexander Laufer, Edward J. Hoffman
1. Rules of Thumb: How to Stay Productive and Inspired Even in the Most Turbulent Times by Alan M. Webber
1. Seven Basic Plots: Why We Tell Stories by Christopher Booker
1. Seven Steps to Mastering Business Analysis by Barbara A. Carkenord
1. Seven-Day Weekend by Ricardo Semler
1. Sex, Science & Profits by Terence Kealey
1. Simply Fly by Gopinath, Captain G.R.
1. Statecraft by Margaret Thatcher
1. Stay Hungry Stay Foolish by Rashmi Bansal
1. Strange Places, Questionable People: Updated With a New Chapter on Kosovo by John Simpson
1. Taking Hold of Tomorrow by Jack W. Hayford
1. Tell Me Your Dreams by Sidney Sheldon
1. That Last Mountain by Terence Strong
1. The Art of the Start: The Time-Tested, Battle-Hardened Guide for Anyone Starting Anything by Guy Kawasaki
1. The Back of the Napkin: Solving Problems and Selling Ideas with Pictures by Dan Roam
1. The Black Swan: The Impact of the Highly Improbable by Nassim Nicholas Taleb
1. The Checklist Manifesto by Atul Gowande
1. The Consolations of Philosophy by Alain de Botton
1. The Design of Everyday Things by Donald A. Norman
1. The E-Myth Revisited: Why Most Small Businesses Don't Work and What to Do About It by Michael E. Gerber
1. The Gnostic Gospels by Elaine Pagels
1. THE JONI STORY by JONI EARECKSON
1. The Knack: How Street-Smart Entrepreneurs Learn to Handle Whatever Comes Up by Norm Brodsky, Bo Burlingham
1. The Kneeling Christian by Unknown Christian
1. The Life and Times of the Thunderbolt Kid by Bill Bryson
1. The Long Tail: How Endless Choice Is Creating Unlimited Demand by Chris Anderson
1. The Millionaire Next Door: The Surprising Secrets of America's Wealthy by Thomas J., Ph.D. Stanley, William D., Ph.D Danko
1. The Minto Pyramid Principle: Logic in Writing, Thinking, & Problem Solving by Barbara Minto
1. The Monk Who Sold His Ferrari by Robin S. Sharma
1. The Personal Beliefs of Jimmy Carter: Winner of the 2002 Nobel Peace Prize by Jimmy Carter
1. The Road Less Traveled Set: A New Psychology of Love, Traditional Values, and Spritual Growth by Michael Scott Peck
1. The Tough-Minded Optimist by Dr. Norman Vincent Peale
1. The Two Minute Rule by Robert Crais
1. Thinking in Systems: A Primer by Donella H. Meadows
1. Tuesdays with Morrie: An Old Man, a Young Man, and Life's Greatest Lesson by Mitch Albom
1. Walk in the Woods by Bill Bryson
1. What Got You Here Won't Get You There by Marshall Goldsmith, Mark Reiter
1. What Smart People Do When Dumb Things Happen At Work by Charles E. Watson
1. What the CEO Wants You to Know : How Your Company Really Works by Ram Charan
1. WHERE IS GOD WHEN IT HURTS?: BEYOND SUFFERING by PHILIP YANCEY
1. Who Will Cry When You Die? by Robin S. Sharma
1. Winning: The Ultimate Business How-To Book by Jack Welch and Suzy Welch
1. Writing That Works by Kenneth Roman, Joel Raphaelson
1. [Algorithms to Live By: The Computer Science of Human Decisions](https://www.amazon.com/dp/1250118360?tag=jjude-20) by Brian Christian & Tom Griffiths
1. [25 Need-to-Know Strategy Tools](https://www.amazon.com/dp/1292016434?tag=jjude-20) by Vaughan Evans
1. [The McKinsey Edge: Success Principles from the World’s Most Powerful Consulting Firm](https://www.amazon.com/dp/1259588688?tag=jjude-20) by Shu Hattori
1. [Predictive Analytics For Dummies](https://www.amazon.com/dp/1119267005?tag=jjude-20) by Tommy Jung, Mohamed Chaouchi, Anasse Bari
1. [Antifragile Systems and Teams](https://www.amazon.com/dp/B00KQVXTL0?tag=jjude-20) by Dave Zwieback
1. [Everybody Writes: Your Go-To Guide to Creating Ridiculously Good Content](https://www.amazon.com/dp/1118905555?tag=jjude-20) by Ann Handley
1. [Soft Skills: The Software Developer's Life Manual](https://www.amazon.com/dp/1617292397?tag=jjude-20) by John Sonmez
1. [The Best Software Writing I: Selected and Introduced by Joel Spolsky](https://www.amazon.com/dp/1590595009?tag=jjude-20) by Joel Spolsky
1. [Smart and Gets Things Done:Joel Spolsky's Concise Guide to Finding the Best Technical Talent](https://www.amazon.com/dp/1590598385?tag=jjude-20) by Joel Spolsky
1. [Measuring and Managing Performance in Organizations](https://www.amazon.com/dp/0932633366?tag=jjude-20) by Robert D. Austin
1. How to study? by _George Fillmore Swain_
1. Rough Draft by _Michael Robertson Jr._
1. Everything is miscellaneous by _David Weinberger_
1. The end of jobs by _Taylor Pearson_
1. Traction by _Gabriel Weinberg & Justin Mares_
1. The power of imagination by _Kerry Kirkwood_
1. The Messiah Matrix by _Kenneth John Atchity_
1. Do Improvise by _Robert Poynton_. Notice more, Let go, Use everything. Everything is an offer.
1. The 4-hour work week by _Tim Ferriss_. **D**efine your priorities, **E**liminate waste, **A**utomate your income, **L**iberate yourself from geography & time

_Disclaimer: As an Amazon Associate I earn from qualifying purchases. Most links in this description are affiliate links. If you buy from my link, I will get a small commission at no extra cost to you. Thank you for supporting me!_

