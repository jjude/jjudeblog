---
title="Will A SOPA Like Protest Succeed In India?"
slug="will-a-sopa-like-protest-succeed-in-india"
excerpt="Can mass online movements change democracy?"
tags=["opinion"]
type="post"
publish_at="26 Jan 12 16:30 IST"
featured_image="https://cdn.olai.in/jjude/will-a-sopa-like-protest-succeed-in-india-gen.jpg"
---


[SOPA](https://en.wikipedia.org/wiki/Stop_Online_Piracy_Act) is dead, at least for now. Thanks to nudging by popular internet figures like [Tim O'Reilly][7], [Paul Graham][8], many constituents called or faxed their representatives. Ultimately the protest forced the bill to be withdrawn. Unlike the [Occupy Wall Street](https://en.wikipedia.org/wiki/Occupy_Wall_Street), [SOPA](https://en.wikipedia.org/wiki/Stop_Online_Piracy_Act) protest was largely online, yet it achieved it's objective.

But can this success be replicated elsewhere, especially in India[^3]? After all she is the largest democracy in the world and the number of [Internet users][6][^2] is increasing rapidly.

Despite the optimism of many, I'm not so sure that such protests will be a success in India.

I can't think of a single mass movement, in the recent past, that actually changed the stand of the government. Talk about two recent such mass movements: [Telangana](https://en.wikipedia.org/wiki/Telangana_movement) and [Anti-Corruption Movement](https://en.wikipedia.org/wiki/2011_Indian_anti-corruption_movement). Mind you, these people didn't stop with their tweets, likes and blogs; they took their protests to streets. Still, did they achieve their objective?[^1]

If such street protests didn't succeed what chance does an online protest has?

I put forth below arguments in favor of my hypothesis:

1. It is not easy to reach representatives as like in the US. They stay behind many layers of human defenses like secretaries and personal aides that it is simply not possible to reach to the representative. It is of no use explaining your protests to these aides. They will simply cut you off after sometime. All of these representatives do have an official email but again, in this world of email overload, it will be a miracle if they even read these emails. Sending a fax, though, might work. But if it comes in plenty, the machine will be safely switched off.

2. Let's say you managed to get through all of these walls and reached the representative. And he[^4] is probably convinced too. But there is no democracy within Indian political parties. So, it doesn't matter what he as an individual representative believe; he will have to abate his position in favor of the party line and many do. And the party line is whatever that will get them votes. If that happens to be the right choice for the country, good for citizens; if not, they are not even sorry.

3. Our politicians are pretty good at diffusing tensions. If they perceive that there could be a bargain that could dilute the protest they will do so. The participants will disperse after a compromise and the real goal will not be achieved. (One has to only look at the state of the above two protests that I talked about earlier).

So am I saying there is no hope for a change? Absolutely not.

In a complex system like democracy, [lollapalooza effect][12] - more than one factor leading to an outcome - is an inevitable component. That lollapalooza effect takes place in one of two situations: either the mass movement reach an avalanche momentum to throw out the political smugness or a do or die crisis erupts that forces radical changes[^5]. (When both happens that is a lollapalooza too).

But its anyone's guess which protests will stimulate a lollapalooza effect.

[6]: http://www.internetworldstats.com/top20.htm
[7]: https://en.wikipedia.org/wiki/Tim_O'Reilly
[8]: http://www.paulgraham.com/bio.html
[9]: http://www.facebook.com/SupportUID
[10]: http://twitter.com/SupportUID
[11]: http://uidai.gov.in/
[12]: https://en.wikipedia.org/wiki/Charlie_Munger#Lollapalooza_Effect
[13]: https://en.wikipedia.org/wiki/Social_proof

[^1]: I'm not debating the worthiness of their cause. Given the fact that they were not political gatherings and there were thousands behind these causes prove their need.
[^2]: In absolute numbers, India is growing in internet users; but penetration is abysmally < 10%
[^3]: This is of interest as support for [UID][11] is now gathering momentum in [social][9] [media][10]
[^4]: English isn't gender neutral, so these are just linguistic conveniences. Forgive me for it.
[^5]: Like what forced India to abandon its license raj and open up its economy.

