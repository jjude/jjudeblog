---
title="Year in review - 2023"
slug="2023"
excerpt="Turning Fifty on a high note"
tags=["retro"]
type="post"
publish_at="17 Jan 24 07:15 IST"
featured_image=""
---

## Highlights of the year
- Celebrated 50th birthday with family at luxurious [Kumarakom Lake Resort](https://www.kumarakomlakeresort.in/)
- Won an AWS Award
- Got fit
- Elder son volunteered a group of blind people
- Celebrated 75th birthday of Mom
- Traveled to south and north of India

## Celebrating 50th birthday

I turned 50 this year. My birth was a miracle. When my mom was pregnant, doctors said I might be dead or just a stone. The doctors wanted to operate to remove whatever was there, but my mom being a woman of faith and a fighter, refused. She walked 10 km to a church every day, praying for a safe delivery.

I was born, but challenges kept coming. My family was poor. My younger brother had a medical condition that required constant care. For a minimal amount, a village doctor treated him. At one point, the situation got worse and they had to take him to a better hospital. My parents didn't have 50 rupees at the time, so we lost him that night. Even I had karrapan (eczema) on my leg. I got healed with treatment, though. Wheezing followed. With all that background, it fills me with gratitude to have reached 50 in good health.

I celebrated my birthday with my family - my wife, kids, sister, brother-in-law, and niece. Having them beside me for this milestone was another blessing. We stayed in Kumarakom backwater resort in Kerala, enjoyed a private dinner and a backwater cruise. Having a rich and generous younger sister is surely a blessing.

I feel so blessed to be turning 50. From a challenging past and poor beginnings to being healthy and surrounded by loved ones today - I am grateful for it all.

## Winning AWS Award

An initiative I spearheaded within the company received the prestigious AWS Market Disruptor Award this year.

In the past, I initiated internal improvements like engineering processes, DevOps procedures, and exploring nascent technologies like Flutter. Our attention this year was on generative AI, a hot topic among industry leaders.

The first thing we did was use Amazon Code Whisperer to boost developer productivity. Our initial focus was on internal applications for employee efficiency and process optimization, much like previous years. Our AWS account manager and CEO convinced us to consider how generative AI could benefit our clients.

In a stroke of luck, one of our retail clients asked about using AI to identify objects in photos. For retailers and brands, this was a big deal. We used Amazon's AWS image recognition models, trained on popular products. AWS awarded us a Market Disruptor Award for our innovative solution.

Success is often a lagging indicator of skills and processes. Our recent win is not just a reflection of what we've done recently, but also what we've done all along.

Happy to end the year with an award.

## Getting fit

As I turned 50 this year, I embarked on a fat-to-fit journey. With 86 kilos, I was obese for my height and age. A combination of diet and exercise got me healthier.

First, I set an eating window between 9:30 am and 7 pm. I drank only water from 7 pm to 9:30 am the next day. A glass of milk is my go-to if I'm hungry. Otherwise, I ate like I always do during my eating window. Eating until you're 80% full is a good rule of thumb. My breakfast was usually idli, dosa, pongal, or oats. Lunch was rice or chapatis, while dinner was usually bread and omelets. Along with avoiding aerated drinks, I also replaced sweets with natural sources like dates to reduce my sugar intake. While I tried to curb my snacking, I occasionally indulged in treats like murukkus. On Fridays, I skip lunch and eat early dinner around 5:30-6 pm.

Getting fit started with 30 minutes of jumping jacks and skipping at home. After a while, I switched to walking and then jogging for six kilometers. Eventually, I jogged three times a week at an average pace of around nine minutes per kilometer for a total of six kilometers. Whenever I wasn't jogging, I did at-home workouts.

Thankfully, these efforts paid off. I dropped from 86 kilos to 77.4 kilos. I used to wear jeans with a 36-inch waist, but now I fit into a size 32.

## Celebrated 75th birthday of Mom

As the daughter of a village head, my mom grew up in privilege. Even though girls weren't allowed to attend school then, she went to school because of her privilege. Because she excelled at school, she even got a job in Madras, the state capital. It seemed her fortune was on the rise.

However, a series of misfortunes soon changed everything. Her father insisted she marry near their village to care for him in his old age. So she married my dad. On the day of marriage, she lost all her jewels gifted by my grandpa. Suddenly thrust into poverty, she and my father lived in a hut where I was born.

Despite the challenges, my parents struggled to make ends meet by working as substitute teachers. My dad sold groceries to put food on the table. Then, fortune finally smiled on our family when my sister was born. A relative helped secure a permanent job for my mother at a primary school, allowing us to move from the village.

Although our financial situation improved over time, my mother's health remained a challenge. Asthma was one of her many ailments. Nevertheless, she continued to study and contribute to the family through various means - offering tutoring sessions, assisting my father in his sari business, and exploring other avenues to enhance our lives. Eventually, we were better off; both my sister and I pursued engineering degrees. 

After my parents retired, they moved to Bangalore to live with my sister. Now 75 years old, my mother remains active and engaged in life. She had her share of challenges but also reaped rewards. She's been to faraway places like Jerusalem and Sri Lanka. While in Sri Lanka, she visited the church her mom attended. She's seen her grandchildren through both of her kids. Even at 75, she's got all her senses. She celebrated her birthday with both of her kids and our families, which is a treat.

## Travel
Looking back I am surprised by all the travel we did. 

During April, we went to Gudalur and Kothagiri in Tamil Nadu for a friend's wedding. In May, my sister surprised me with a trip to Kumarakom Lake Resort for my birthday. In June, we went to Cochin for our college reunion. We went to Delhi in August so my older son could volunteer with a group of blind people going to Uttarakhand. We went to Kashmir for year-end vacation as a climax.

Overall we traveled a lot this year.

## Retros of the earlier years

I’ve been publishing annual reviews for more than a decade. You can [read them](/annual-reviews/) to understand the journey I’ve come through.