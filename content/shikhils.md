---
title="Shikhil Sharma on 'Talk With Me Security'"
slug="shikhils"
excerpt="A masterclass on digital security, starting a startup, and marketing to scale a company."
tags=["security","gwradio"]
type="post"
publish_at="19 Oct 21 07:31 IST"
featured_image="https://cdn.olai.in/jjude/shikhils-gen.jpg"
---

<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless src="https://share.transistor.fm/e/52d3b3b5"></iframe>

- You won an award from Indian PM and got a grant from French president. How did this magic happen?
- Most security companies advertise fear. How do you think about marketing security?
- Small business owners has not much to lose. Why should they invest in security?
- Isn't it the responsibility of the packages like Magento to keep their code clean and secure?
- What makes a great leader?
- What is the kindest thing anyone has done for you?
- What is your definition of a good life?

### Watch on YouTube

<iframe width="560" height="315" src="https://www.youtube.com/embed/HKgUiPBQr7A" title="Talk With Me Security" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Connect with Shikhil 

- LinkedIn: https://www.linkedin.com/in/shikhil-sharma/

## Connect with me
- Twitter: https://twitter.com/jjude
- LinkedIn: https://www.linkedin.com/in/jjude/
- Website: https://jjude.com/
- Newsletter: https://jjude.com/subscribe/
- Youtube: https://youtube.com/gravitaswins
- Email: podcast@jjude.com
- Executive Coaching Program: https://gravitaswins.com

## Your feedback counts

Thank you for listening. If you enjoy the podcast, would you please leave a short review on Apple podcast or on YouTube? It takes less than 60 seconds, and it really makes a difference in finding this podcast. And it boosts my spirits. 😀

I would like to hear what resonated the most with you in this episode. Either you can email me or share it on social media and tag me. Thank you for your support.

Have a life of WINS.

## Amazing tools I use:
- I record all the remote interview with [Riverside.fm](https://riverside.fm/?via=joseph-jude)
- I use [Transistor.fm](https://transistor.fm/?via=jjude) for hosting these podcast episodes

_Gravitas WINS Radio is produced by [Joseph Jude](https://jjude.com/). Be sure to subscribe on [Apple Podcast](https://podcasts.apple.com/in/podcast/gravitas-wins-radio/id1562802449), [Spotify](https://open.spotify.com/show/7LOBjAyjBkechMC0xMIj9E),[Google Podcast](https://podcasts.google.com/feed/aHR0cHM6Ly9mZWVkcy50cmFuc2lzdG9yLmZtL2dyYXZpdGFzLXdpbnMtcmFkaW8=),[YouTube](https://www.youtube.com/gravitaswins), or the podcasting app of your choice._

Please check out other episodes of [Gravitas WINS Radio](/gwr-themes/). For easy consumption, they are categorized by themes. If you are a fellow podcaster, you might be interested in [How I Podcast](/how-i-podcast/) or [tools I use](/i-use/).

## Continue Reading
- [Startups And Security](/startups-and-security/)
- [Cyber Security for Women](/cyber-security-for-women/)
- ['Building An Enterprise Data Strategy' with Ganesan Ramaswamy](/ganesanr/)
