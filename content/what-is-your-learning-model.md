---
title="What is your learning model?"
slug="what-is-your-learning-model"
excerpt="My learning model is of two layers - a core layer, domains in which I want to be an expert and a support layer, domains which will help me to practice my expertise effectively."
tags=["coach","sdl","coach"]
type="post"
publish_at="10 Mar 09 16:30 IST"
featured_image="https://cdn.olai.in/jjude/what-is-your-learning-model-gen.jpg"
---

In the competitive software industry, if you don’t keep yourself updated, soon you'll be outdated. Yet when swamped with hundreds of concepts, methods and techniques, it is impossible to be updated continually. There is also a risk of, being updated in a soon-to-be outdated area.

Whether lucky or smart, I've always worked on challenging, well-paying and emerging concepts & tools. Having reached a certain stage in my career, I'm preparing for the next stage. I've followed 'learn-do-refine' model which has worked pretty well so far. With that foundation, here is my learning model for the next stage.

My learning model is of two layers - a core layer, domains in which I want to be an expert and a support layer, domains which will help me to practice my expertise effectively.

![Learning Model](https://cdn.olai.in/jjude/learning-model.png "Learning Model")

#### Enterprise Architecture

Many-a-times, those in software industry asks, 'I have an answer. Do you have a relevant question?'. But successful (read profit making) companies are those that solve a business problem by engaging technology. EA brings this success story as a practice. I'm excited to learn about EA and possibly apply those concepts in the coming years.

#### Customer Relationship Management

CRM covers topics such as finding new customers, selling more to existing customers, and retaining customers. Now-a-days, CRM is considered relevant even for government organizations. I've been in CRM domain for the past 8 years and I've enjoyed both functional and implementation sides. I would like to continue to learn how CRM concepts help companies (and even governments) to be profitable.

#### People Skills

Executive educator, [Marshall Goldsmith](https://en.wikipedia.org/wiki/Marshall_Goldsmith), says in '[What got you here won't get you there](http://www.amazon.com/What-Got-Here-Wont-There/dp/1401301304)', that most of executive's challenges are 'people related'. To achieve anything significant, there needs to be a team (composed of seniors, peers, and juniors) and it is important to develop people skills to work with and with-in teams. Having been a asocial introvert, I find that this part of the pie is the most frustrating and emotionally draining; yet a critical part to be successful.

#### Project Management

I would broadly term this part as 'Getting things done' - the execution part of the deal. I've worked with some of the brilliant minds who are able to place best ideas on the table, but lack what it takes to get it done. Though I like to conceptualize a solution and communicate it with enthusiasm to others, I find it satisfying to get it executed - not really bothering about who gets the credit. You know what? When you get into that attitude, you almost always get the credit!

#### Financial Management

This is one area where I need to start from scratch. Executives translate every action in the organization into numbers - either profit or loss. It is poor financial management that has gotten the erstwhile famous companies into bankruptcy.

#### ...and some fun

Another critical piece to continue enjoying other pieces. Though I don't expect to be [Ansel Adams](https://en.wikipedia.org/wiki/Ansel_Adams) or [Mark Twain](https://en.wikipedia.org/wiki/Mark_Twain) or [Michael Phelps](https://en.wikipedia.org/wiki/Michael_Phelps), I like to pursue photography, travel, writing and swimming as they joyfully refresh the soul.

Do you have a learning model? Do you think it is worth having one and pursuing it? Feel free to share your comments.

