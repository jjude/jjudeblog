---
title="What Ails India?"
slug="what-ails-india"
excerpt="If you think corruption is the fundamental cause, it is not."
tags=["opinion"]
type="post"
publish_at="08 Dec 12 16:30 IST"
featured_image="https://cdn.olai.in/jjude/what-ails-india-gen.jpg"
---

I used to think corruption is the fundamental cause of all that's wrong with India. But, having sat through countless policy discussions and observed many financial, societal and governmental transactions, I have come to believe corruption[^1] is just a symptom and not a fundamental cause.

If corruption is the culprit, then countries where corruption exists should battle similar socio-economic issues: illiteracy, poverty, inadequate infrastructure, religious violence, crony capitalism, failing welfare schemes, and so on. But the developed countries, where corruption exists in one form or the other, don't face similar ailments. So the cause should be something else.

In a system as complex as a country, any outcome—both positive and negative—can not be pinned to a single or even to few reasons, since driving factors co-evolve blurring the primary causality. But if we study various events, schemes and outcomes—not only in India but in other countries too, we can be reasonably sure of the primary casual factor(s).

The fundamental causes I observed are two. They are: a) lack of uniform law enforcement and b) lack of swift justice. Every other problem that India faces is a derivative of these issues or directly because of these.

The temptation to manipulate the system through bestowed power or acquired money, isn't unique only to Indians. Every human have it. But their chances of manipulation increases, if they are convinced that they won't be caught or punished when they do so. Once they start playing the game and reaping benefits, there is no stopping.

As confidence in getting away unpunished increases, manipulation becomes a norm. This in-turn leads everyone—politicians, bureaucrats, capitalists and ordinary citizens—to get into the playing field to try their hand at manipulation. That is precisely what is happening in India.

[^1]: By corruption I mean, bribes rather than decay of moral and cultural values.

