---
title="Are you principled or just plain stupid?"
slug="pors"
excerpt="We are all hypocrites. Only degree varies."
tags=["coach","wins","self","coach"]
type="post"
publish_at="15 Mar 21 17:52 IST"
featured_image="https://cdn.olai.in/jjude/linkedin-pors-poll.jpg"
---
I posted a hypothetical situation on [LinkedIn][1] and asked people to vote:

> Suppose you're driving across a long stretch of desert and notice that your gas tank is nearly empty. A sign says, "Last gas for 100 miles." Unfortunately, the brand of gas is associated with an oil company that you consider to be unethical weasels. You have vowed to boycott their products. On principle, you drive past the gas station, run out of gas, and eventually die in the desert.  
> Question: Were you principled or stupid?

![LinkedIn Poll](https://cdn.olai.in/jjude/linkedin-pors-poll.jpg)

Not surprisingly, 82% of people sided with compromising their principle and getting gas from the unethical company that they earlier vowed to boycott. 

I say "not surprisingly" because most of us treat principles as punchlines to hang on the office walls. "Honesty is the best policy" looks fabulous on a plaque. Still, everyone who worked in an accounts department knows there are 50 shades of honesty.

The Joker summed us well:

> Their morals, their code; it's a bad joke. Dropped at the first sign of trouble. They're only as good as the world allows them to be.

### Personal

After growing up in a conservative Tamil society, I had the opportunity to work in Belgium for a while. In one of the after-office conversations in a bar, the topic of dating, sex, and marriage came up. My Belgian friends were intrigued by the concept of arranged marriage and asked questions after questions to understand it. 

At one point, the discussion turned to sex before marriage. I said it was despised in our culture, and it is not prevalent, yet I was sure such incidents happened widely. One friend pointed to me and asked if I had done it. For which I gave a boastful and an emphatic no. 

I have not forgotten the next question, "**are you a virgin because of principles or lack of opportunities**?"

I didn't have an answer. By then, I had lived away from my parents for more than a decade, so technically, I had a lot of opportunities. But was I strong or just right temptation didn't trip me?

I have thought of that question hundreds of times since that banter in a Belgian bar. 

- I have not stolen because I don't envy or I have not been subjected to the harsh reality of witnessing a baby brother dying out of hunger?
- I don't lie because of moral reasons, or I have not been in a situation where a lie would save my life and those of my loved ones?
- I am not communal because I believe everyone is equal or nobody challenged my religion?

I have tested these hypotheses. My conclusion: If there is an [indecent proposal][7], I would silently stray from my [principles][2].

### Business

Google started with "Do no evil" as their motto. They changed the motto to "You can make money without doing evil," which is not an imperative principle as the original one. Most entrepreneurs begin their journey like Google, with an aspirational motto for the larger good of society. As they grow larger, their underlying principle shrinks to "don't do things that will put us in jail."

![Amazon's apology for Tandav](https://cdn.olai.in/jjude/amazon-prime-apology.jpg)

Closer home, In India, Amazon [apologied][3] for an Amazon original drama series. Though entertainment is certainly an objective of the movie industry, it is not the only one. Theatre and movies used to address social issues and speak truth to power. Now an apology is seen as [business prudence](https://twitter.com/madversity/status/1366790963356569602) than slipping from principles.

When software companies enter into other domains, they twist the underlying principle of the target domain. There is a running [joke in the software industry](https://news.ycombinator.com/item?id=4025705):

> No ethically-trained software engineer would ever consent to write a DestroyBaghdad procedure. Basic professional ethics would instead require him to write a DestroyCity procedure, to which Baghdad could be given as a parameter.

I'm convinced that software engineers in Facebook create their versions of "DestroyCity" procedure to not feel responsible for amplifying authoritarian regimes and breaking down the social fabric of many cultures.

When questioned, CEOs of large companies can follow the template of Reed Hastings, who said [Netflix is not in speaking truth to the powers][4] (apparently that only applies to [Saudi Arabia][5]). His justification: **when we have to comply with different regulations, we have to have different standards**. 

Joker is right, after all.

### Politics

Politics means compromise. Politicians count on spin-doctors' ability to either wipe their compromises or weave them into a positive image.

The politicians in the US and the UK project themselves as defenders of democracy. They anchor every invasion on freedom, freedom, and freedom. They keep it that way. But all the regional turmoils in the world are created and kept alive by them. The world would be a better place if the US and UK stopped preaching the globe and looked inward.

We remember Obama as the charismatic leader who advocated peace. After all, he was awarded the Nobel peace prize. But how many of us know that he [dropped three bombs every hour](https://www.theguardian.com/commentisfree/2017/jan/09/america-dropped-26171-bombs-2016-obama-legacy), 24 hours a day in just 2016? That is all wrapped in a shiny-looking package of "war for peace."

While blasting through the democracy megaphone from their rooftop, they will also keep their lines connected with the dictators and tyrants. Don't forget that the UK was [close to knighting][6] the same Assad whom they are fighting now.

When a dictator dances to their tune, he is a great friend; and when he grows a spine, he is a threat to democracy. 

### Lessons to learn

> I think on-stage nudity is disgusting, shameful, and damaging to all things American. But if I were 22 with a great body, it would be an artistic, tasteful, patriotic, and progressive religious experience. - Shelley Winters

We all like to project an image of someone having a high moral standard. The truth is we oscillate between stupidity and convenient platitudes. It is prudent to remember that whenever you get a nagging feeling to moral police someone else. We are all hypocrites. Only degree varies.

Does that mean you should throw away living up to your principles? No. They are your aspiration. You should strive to live up to them. When you fail to live up to them, don't beat yourself. Forgive and continue to strive to live up to them. As you do, remember everyone else is fighting a battle you have absolutely no idea of.


[1]: https://www.linkedin.com/posts/jjude_suppose-youre-driving-across-a-long-stretch-activity-6773420958339383296-2j_z
[2]: /principles/
[3]: https://techcrunch.com/2021/03/02/amazon-issues-rare-apology-in-india-over-drama-series/
[4]: https://www.theverge.com/2019/11/7/20953210/netflix-reed-hastings-patriot-act-hasan-minhaj-saudi-arabia-episode-remove
[5]: https://www.vox.com/recode/2019/11/14/20965037/netflix-reed-hastings-ted-sarandos-censorship-saudi-arabia-truth-power-hasan-minhaj
[6]: https://en.wikipedia.org/wiki/Tony_Blair#Syria_and_Libya
[7]: https://en.wikipedia.org/wiki/Indecent_Proposal

## Continue Reading

* [Writing my obituary](/my-obituary/)
* [The dichotomy of contentment and ambition](/contentment-and-ambition/)
* [I made mistakes](/i-made-mistakes/)