---
title="Don't Frown Upon Problems"
slug="dont-frown-upon-problems"
excerpt="Don't complain when problems overwhelm you with their forms and dimensions. Be happy. If not for them, you don't have a job in knowledge industry."
tags=["problem-solving"]
type="post"
publish_at="16 Jan 11 16:30 IST"
featured_image="https://cdn.olai.in/jjude/dont-frown-upon-problems-gen.jpg"
---

"You should be happy they have problems," my boss Mahindra chided me when I complained how dysfunctional the client's team is, "if they don't have problems, we don't have business". Mahindra, a seasoned problem solver, understood the crux of Knowledge Industry.

Knowledge Industry thrives on solving business problems - how to get raw materials on time to production sites, how to conduct business globally, how to reduce waste, how to increase profit and so on and so forth.

Business problems come in **variety of forms**. Some problems require only intellectual firepower to solve - get basic facts, lock yourself, crunch the data and arrive at a solution; there are other problems that demand field-visits and hours, if not days, of brainstorming (and negotiation) with stakeholders to arrive at a solution.

There are **generic problems** that are common to all businesses even though they are separated in time, geography, culture and industry. Here, solution of one business can be tailored and applied to others. More often, though, businesses face unique set of stumbling blocks and cookie-cutter solutions are not the answers for them.

There is also dimensionality to business problems. Business problems are seldom single dimensional. Project overrun may be because of wrong estimates, or scope change, or changes in priorities, or lack of skilled team members or play of politics between executives or an interplay of all of these causes. Same goes for declining profit, failure in clinching deals and employee attrition. Executives, typically, wish for a single, supreme reason so that they can tackle that one problem. Unfortunately, there are **multi-dimensions** to these problems.

Indeed the problems faced by businesses in today's globalized world can overwhelm the beginners in the field of problem solving.  Novice problem-solvers proceed unidirectionally guided by textbook case studies. They can not handle the tension of multi-dimensionality of problems or distinguish conditions that make a situation unique. They panic and complain to their bosses when unexpected dimensions emerge. On the other-hand, competent problem-solvers analyze multi-dimensionality of business problems within their context and arrive at an workable solution. They can easily navigate between breadth and depth of a problem.

The basic tenet of knowledge industry is, problems aren't one of the things to deal with; it is the only thing to deal with.

So don't complain when problems overwhelm you with their forms and dimensions. Be happy. If not for them, you don't have a job.

_This post is part of '[Be a Problem Solver](/be-a-problem-solver/)' series._

