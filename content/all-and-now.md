---
title="The curse of EVERYTHING and NOW on building your career"
slug="all-and-now"
excerpt="Successful business-men advice you to learn as many business functions as you can learn. But you should be aware of a mental trap that comes with it."
tags=["coach","vlog","sdl"]
type="post"
publish_at="15 May 17 22:47 IST"
featured_image="https://cdn.olai.in/jjude/curse-all-and-now.png"
---
Successful business-men advice you to learn as many business functions as you can learn. This gives you a strong foundation to build your career. We saw this in the [previous episode](/specialize-or-generalize/).

This is a good framework to build your career. But you should be aware of a **mental trap** that comes with it. I have fallen into this trap again and again.

Learning as many business functions as possible can quickly morph into, you need to learn EVERYTHING, and learn them all NOW.

[![Everything and Now](https://cdn.olai.in/jjude/curse-all-and-now.png "Everything and Now")](https://youtu.be/gFpzLMuT_3c)

Let me narrate an example from my own experience.

Early in my career, I got an opportunity to work on visual basic to build an application that connected to database and fetched results. As was my practice, I started learning everything about visual basic. In few days, I got fascinated with Oracle database which the application was connecting. I started to learn about codd rules and other database related concepts. Few days into this fascinating journey, someone in my team said, “you know what, you will really enjoy learning unix”. You guess it right. In a matter of few weeks, I switched from visual basic to oracle to unix. I learned nothing deep.

This is an easy trap to fall into for those who want to learn as much as possible. We think, we need to learn everything there is to know and we need to learn them all now.

This is like the donkey in the fable we heard in our school days. It stands in-between a pile of hay and a bucket of water. It looks at the hay and thinks, I am hungry. I should eat this. Then it looks at the water and says, oh, if I eat the hay, I will lose the water. So I better drink the water. Now it can't decide. It keeps looking left and right, left and right, but can't decide. Eventually, it dies of hunger and thirst.

Is there a remedy?

As Alan Weiss, a successful consultant says, **move a thing forward by a mile, not hundred things an inch**.

I was a donkey for long. Last year, I took the advice from Alan Weiss. I focused on writing a book. It took me a whole year, but I kept at it. I didn't switch between water and hay. I focused on moving one thing a mile. Last month, it was published. Now I can claim, I am a published author.

Let me summarize. You need to learn as much as possible. But you don't have to learn everything and not at the same time. Focus on one thing at a time and move it forward as much as possible. It may take 6 months, or a year, or even more. Keep at it. Once you have take it a mile, start with the next one. If you follow this, you will be farther than many. Good luck.


### Similar Posts

* [11 Lives to Build a Rich Career](/11-lives/)     
* [An awesome tip from Jack Ma & Derek Sivers to build your career](/specialize-or-generalize/)     
* [How Your Attitude Impacts Your Career Growth?](/how-your-attitude-impacts-your-career-growth/)     
