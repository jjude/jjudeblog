---
title="Lessons In Consulting"
slug="lessons-in-consulting"
excerpt="Consulting is the only profession where you are paid to learn on the job. It is exciting as well as frightening. But if you got a right mind-set, there is no other profession like consulting."
tags=["coach","insights"]
type="post"
publish_at="10 Aug 10 16:30 IST"
featured_image="https://cdn.olai.in/jjude/lessons-in-consulting-gen.jpg"
---

I entered consulting with a technical - to be precise software - background. I was surprised to learn that technical competence alone does not guarantee success in consulting assignments. I quickly learned to augment technical skills with soft-skills and witnessed increased success rate.

Here are the soft-skills that I learned over the years:

* **Being comfortable with uncertainty** : Certainty isn't certain in consulting. A cloud of uncertainty is ever present in all stages - from sealing contracts, obtaining data, implementing requisite change and so on. Many-a-times, I have heard other consultants complain about chaos in client's organization. Truth to be told, if client’s are certain then there is no need for consultants! So I say, get on with it.

* **Perception is Reality** : As a consultant, you are dealing with the perception of the stakeholders. The stakeholders perceived employees are missing an essential skills and signed you to train them; or he perceives that the new system will bring the much needed efficiency and you are the change agent or whatever else might be the perception for reaching out to you. You are there to match reality with the perception. Its not only about the state of affairs. The perception is about you too. He is either happy that he got a competent consultant or regret getting an incompetent idiot. Its up to you to manage that perception. In any case, know that perception eclipses reality.

* **Be a self-starter** : Assignment, motivation and monitoring are for employees; measurement of outcomes are for consultants. So you should be proactive in getting to the outcome as quickly as possible. If you are not proactive, you won't last long in consulting business.

* **Relationship Building** : As the Million Dollar Consultant, [Alan Weiss](http://www.contrarianconsulting.com), says you are not in consulting business; you are in relationship business. Many consultants are happy with building relationship with gate-keepers and middle-men. My efforts are always concentrated with economic decision-makers. Because if the economic decision maker perceives you as an incompetent idiot, no other help can force him to sign the cheque.

* **Learning on the fly** : Consulting profession demands broad range of skills - interviewing, negotiation, decision making, information technology and so on. So you need to constantly update yourself. If you are not updated, you are outdated. And in consulting, being outdated means you are on the way out. You should view every challenge as an opportunity to learn something new. To quote Alan Weiss again, "I'm constantly surprised how stupid I was two weeks ago."

Consulting is the only profession where you are paid to learn on the job. It is exciting as well as frightening. But if you got a right mind-set, there is no other profession like consulting.

