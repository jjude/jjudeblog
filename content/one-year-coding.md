---
title="I coded for a year. Here are ten lessons"
slug="one-year-coding"
excerpt="Streaks are good, especially if you love it."
tags=["produce","sdl","insights"]
type= "post"
publish_at= "11 Aug 19 06:14 IST"
featured_image="https://cdn.olai.in/jjude/one-year-coding.jpg"
---

![One year of coding](https://cdn.olai.in/jjude/one-year-coding.jpg)


I coded daily for a year. I skipped when we went on holidays and Sundays. Even when I skipped, I got back to the streak as quickly as possible.

Here are ten lessons I learned in this year of coding.

### 1. I am privileged

As I sat down to code day after day, I realized how lucky I'm to do what I love daily.

I work only three days a week; I reach office in about 20 minutes; I live in a city where there are no traffic jams; I'm healthy enough through the year to focus on coding; I have a supportive family that lets me do this. I can go on, but each one of these blew my mind off. I shouldn't whine at all.

It is not that I don't have unpleasant days or I don't have any struggles. But they are minuscule compared to the privileges I have.

### 2. Make it a morning routine

If you want to do something daily, do it as soon as you get up.

Initially, I tried to find some free time in the day to code. Most days, I failed, and I coded a line or two before I went to bed.

Once I made it into a morning routine, my questions and philosophy changed. It was no more, should I code today. It was what should I code today.

Because it was part of the morning routine, I had plenty of energy to complete whatever I planned. This, in turn, created a positive cycle. More I finished, more I wanted to return to coding the next day.

### 3. Build something you need

I started my coding streak with learning Vue.js and Golang. I subscribed to the popular tutorials and followed their roadmap. That kept me going for about a month. Then I got bored.

I could've kept learning one thing or the other. There are plenty of tools and technologies to learn. I realized that without an underlying thread connecting everything I learn, I would keep getting bored.

So I started developing digital products I wanted.

I was using [Buffer](https://buffer.com/) to post to Twitter. I created a Google sheet interface to Buffer, called [Sheet2Buffer](https://app.sheet2buffer.com/) so I could play around with the familiar Google sheet interface.

I also created a blog engine, called [Olai](https://www.olai.in/), that powers this site.

In the course of developing these products, I learned CSS design with [Tachyons](http://tachyons.io/), front-end development with [Mithril](https://mithril.js.org/), hosting with [Heroku](https://www.heroku.com/), monitoring with [Rollbar](https://rollbar.com/), sending emails with [Mailgun](https://www.mailgun.com/), and serving with [Caddy Server](https://caddyserver.com/). All because I needed to do so.

### 4. Default choices make you productive

I didn't want to fight with options daily. So I created as many default choices as possible. Here are some ways I chose to be productive:

- I sat in the same place
- I sat almost at the same time
- I sat for nearly the same duration
- I used the same IDE — Visual Studio Code, and tool-chains — Golang, Mithriljs, Gitlab, and custom coded deployment script.
- I left my Mac laptop on so I can start without waiting for booting time
- I listened to the same playlists

In short, except for what I was developing in that one hour, everything else remained the same.

### 5. Keep a swipe file (backlog)

I use [todoist](https://todoist.com/) for tracking tasks. Todoist has a "project" feature. I used it to note down every feature I wanted to create. Todoist has a comments section for every to-do item. I used this comments section to note down any reference or how to implement that item.

I also used a paper-book. I drew architecture diagrams or data flow diagrams, or sometimes scribbled any insights that came along while I was coding.

With these two, I avoided context switching to a large extent. It helped.

### 6. Separate thinking and doing

Sometime when you are coding, you may have to search for an answer. If you are not careful, you will get into a rabbit chase. If it takes longer than 15 min, get up and walk. I have had answers while outside of the working seat.

### 7. Dope vs. Discipline

When I started, I was full of energy, excitement, and enthusiasm. That dope went away in a few days. When dope faded, discipline kept me going.

I was able to create a positive feedback loop because of everything I mentioned in the above points.

Discipline got me to the seat, rhythm got me to start coding quickly, finishing a feature became a dope to raise expectation about the next day, and the cycle went like a well-oiled machine.

### 8. Everything takes more time than planned

I would start coding a feature thinking it will take an hour or two max. Most often I will take double of that time. Even when I took this into account while planning, it still took more time.

Since I was working in pieces, it took me more time to fit everything as a whole unit.

### 9. Can't do without the internet

I tried disconnecting from the internet to focus on work. It didn't work.

I needed help in getting the syntax correct, figuring out a logic, or implementing that logic.

Almost every time I was stuck, I got the answer from the net — either in some support group or in Stack Overflow. Special thanks to folks hanging out in the [Mithril support group](https://gitter.im/mithriljs/mithril.js). They are always helpful in answering questions. Never once I got RTFM as an answer.

### 10. Obsess over fundamentals; not techniques and tools

I have coded in C++, Visual Basic, and Node.js. Now I code in Golang. Tools change, fundamentals don't.

If I understand how token-based login work, then it is only a matter of finding out how to do that in the selected language or framework. Same goes for everything in software design.

Got a comment? [Tweet](https://twitter.com/jjude) me.