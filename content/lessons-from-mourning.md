---
title="Lessons from mourning"
slug="lessons-from-mourning"
excerpt="A cord of multi-strands is not quickly broken and other lessons from mourning for my mother."
tags=["self","parenting"]
type="post"
publish_at="31 Mar 24 17:35 IST"
featured_image=""
bsky_id="3lbuhyczkq52x"
---

My mom [passed away two weeks ago](/eulogy-for-mother/). I'm trying to capture my grief journey, mainly for my reflection. Many of my friends helped me get through these days, and I realized many passes through the same valley of sorrow. So if these posts help anyone else, that's a bonus. Here are some of the lessons I've learned in these two weeks.

**A cord of multi-strands is not quickly broken**, which is my biggest lesson. We were shattered when we heard about her passing. She had asthma her whole life, but no other crippling diseases. She wasn't bedridden. She was still walking days before she died. At 75, her death was both expected and shocking. We coped because of many people's support.

Our maid rushed to our home immediately after my mom died. She called an ambulance to take mother to the hospital. She and her husband even came to Tuticorin with us when we transported my mother's body from Bangalore. We felt comforted by their presence at the funeral.

My cousins helped us plan everything for the grave and the church. They picked the right spot for the grave and prepared it. They handled all the church arrangements, like speaking with the priest and arranging the mass.

Every arrangement at our house was handled by a family friend. Before the body arrived, he cleaned the house and ordered meals, tea, and water bottles - making sure we were taken care of in our time of sorrow.

After I returned to work, friends and colleagues still check up on me often and listen. I talked about her and what she meant to me more than once, and they listened intently. Their listening has helped me deal with this sorrowful situation calmly. It's like a multi-strand thread withstanding stress and tension. These friends and family members keep us from getting overwhelmed by emotions.

**You can't develop these strands overnight**. Our maid has been with us for three years. She became part of our family during festivals and events, even leading some traditions we had forgotten. Her closeness to my mother made her feel comfortable and compelled to attend the funeral. Friendships that have supported me didn't happen overnight. They took time and effort to develop before being needed.

Another lesson I've learned is to **do everything to reduce future regrets**. Jeff Bezos has a regret minimization framework that he talks about from an entrepreneurship and startup perspective. I applied that framework to my life. You don't have to wait for a eulogy to say nice things or express gratitude. Over the last three years, we've celebrated a lot together as a family. Although it cost me a lot, I flew my family there. Looking back, I don't regret anything. I'm glad I did all those things. Derek Sivers has a post called "[stay in touch with hundreds of people](https://sive.rs/hundreds)." I've been doing that. I keep in touch with my friends via phone calls, WhatsApp messages, Zoom chats, or when possible, personal visits.

Fourth, **to mourn well, mourn deeply and privately**. As soon as I heard my mom passed away, I deleted Twitter and LinkedIn from my phone. For almost 10 days, I didn't post anything on Whatsapp (we were coordinating via Whatsapp, so I had to use it). It let me grieve without doom-scrolling as a proxy. I grieved alone and with family, which helped me accept her absence from our lives. Because I grieved deeply and privately, I was able to handle everything emotionally without breaking down.

**Tradition is powerful**. I've always been anti-tradition. I dropped cast from my name, got married in a registrar's office (even though it was an arranged marriage), named my boys after their mother, and homeschool them. Even so, I saw the power of tradition during my mother's last journey. Her ex-colleagues, former students, and neighbors attended a church mass to share their memories. They stayed until we buried her. We also had traditional third and fifth-day gatherings with close family members. I'm glad I participated in these traditions; they provided a graceful and dignified farewell.

**The Bible comforts us during difficult times**. Every day we had family prayer time and never missed Sunday mass or special church events. Even then, I never read the Bible, until I was 18. In the last decade, I've delved deep into its pages, finding comfort and guidance. Whenever I'm having a problem, verses drift into my mind like a gentle whisper, giving me the right perspective. As I knelt and prayed when I heard about my mother's passing, tears welled up in my eyes. As I opened my Bible, I found Psalm 75:2, which talked about an appointed time. I was crying out to God - why now? Isn't 75 still a young age? As tears bubbled up, I had more questions. Through this verse, a peace came over me - a peace I can't understand or explain.

As I've gone through this difficult time, **I've realized how lucky I am**. I know it sounds weird, but it's true. I've been able to visit my family frequently for the past few years. It wasn't cheap, but we managed. My wife and brother-in-law have woven themselves seamlessly into our family fabric, creating a strong support system. We lift each other when we're feeling low, and for that, I'm immensely grateful.

**It's not over; it'll never be over**. Two weeks after she passed, I miss her more than ever. Some mornings, I wake up thinking about all the sweet memories with her—walking with her as a five-year-old amidst railway tracks to catch a bus to go to her school on time, reciting class lessons like usage of water as I walked with her, taking her to Goa where she exclaimed about getting all kinds of dosa from a live counter and she not having to cook for five days of our stay in the resort, holding my first son in her hand with a triumphant smile only an Indian mother can have, towards the end of her life asking me again and again did I get a promotion or a pay raise saying she is praying for one, and so many other memories. She was a remarkable role model. Her memory will live on in my heart as I continue to think of her and miss her dearly. There is no closure – only acceptance of this new reality.

It's never easy to close a chapter. It's hard to deal with loss. It's even harder when it's your mom, who poured her heart, soul, and sweat into you. I cope by writing morning pages and sharing a few from those pages. It's my way of healing.

## Continue Reading
- [Happy 75th Birthday Ma](/happy-75th-birthday-mom/)
- [Eulogy for my mother](/eulogy-for-mother/)
- [Writing my obituary](/my-obituary/)
