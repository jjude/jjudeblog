---
title="Challenges In 'Consume' Phase Of Learning"
slug="challenges-in-consume-phase-of-learning"
excerpt="To learn successfully, you need to learn from authentic sources with a structure. Challenge is in discovering these sources and framing a structure."
tags=["systems","sdl","wins","insights"]
type="post"
publish_at="23 Feb 13 16:30 IST"
featured_image="https://cdn.olai.in/jjude/consume.png"
---
The curious learn, always.

When you are curious, you may learn about human heart valves, telecom networks, mobile programming, or equity investments. It becomes all the more fun, if you get paid for that learning, as in consulting profession.

At an early stage of learning about a domain, you begin with building an understanding of that domain. At this [consume stage](/learning-phases-and-its-support-systems/), you face three types of challenges:

* Discovery
* Comprehension; and
* Sense-making

When you are learning a new field, you need to learn the **fundamentals**. The challenge is to identify the fundamentals. For example, when I got into investing in equity, lot of folks talked about technical indicators and so I learned them. But I quickly realised, by loosing almost all the money I had invested, that technical indicators are good only to pump adrenaline through your body. If you are looking for a) not to loose money; and b) profit from your investments, then you need to stay away from those indicators. Once I learned the fundamental factors to measure, I could find sources that could guide me to achieve those outcomes.

Another challenge in discovering authentic sources is filtering through **noise**. Today, every one is an expert and they are everywhere — Quora, Twitter, LinkedIn and other god forsaken platforms, and they spew out so much that every search is polluted with their noise. Many of these noises are converted into books providing an aura of expertise around them. Crossing these noises and getting to authentic thinkers in the field is indeed a challenge.

![Challenges in learning](https://cdn.olai.in/jjude/consume.png)

Once you have reached authentic thinkers in a field, the next challenge is of comprehension. **Language** plays a major role in comprehension. When I say language, I am not just talking about words and grammar, though they could be a challenge too; much more important is the **context**. Within an existing group of people, context is almost always assumed. So when you get into that group, you need to learn the context to comprehend the language used.

Let me explain it with an anecdote.

I grew up in [Tuticorin](https://en.wikipedia.org/wiki/Thoothukudi), a town in southern India. We have just two seasons — sweaty days and rainy days. Growing up in such an environment, I never understood the meaning of "[white-christmas](<https://en.wikipedia.org/wiki/White_Christmas_(weather)>)" when we went singing carols; there was no internet in those days to google and none of my relatives or friends witnessed snow to tell me about "white-christmas". So I assumed white-Christmas meant, you know, Christmas by the white men, the ones who brought Christianity to India.

I have had similar animating discussions with senior government officers when they learn of "cloud computing". "What do you mean, servers are in the cloud? What happens when it rains?" Mind you, these are highly intelligent officers. Once the concept was explained they understood what that "cloud" means.

The concepts that you learn have to hang together to make sense. For hanging together and to make sense, a **structure** is important; that is why books are still the best way to learn. Without a structure, you will form only a partial understanding like those blind-men trying to analyze the elephant.

To summarize, to learn successfully, you need to learn from authentic sources with a structure.
