---
title="Trends As A Guide To The Future"
slug="trends21"
excerpt="Trends are early indicators of the future. Embracing a trend is embracing the future."
tags=["coach","wins"]
type="post"
publish_at="20 Jul 21 12:31 IST"
featured_image="https://cdn.olai.in/jjude/trends2021.png"
---

In the last quarter of every year, I look at the trends that are forming and identify which ones may be interesting to me. Some of them could have a negative impact on me. But I may be able to use some of the others to gain an advantage in life and career.

I'm not trying to be exact, just [approximately correct](/approximate/) in this exercise. I am not attempting to predict a single future, but to propose several scenarios that may come to pass in the future.

This year too, I looked at the trends. I have selected the trends I find relevant. You should pick up on the trends that will help or impact you based on your context.

The next question you will ask is: how do you know these trends? To learn about these [trends](/trends-in-industry/), I study industry reports and annual reports of public companies.

![Emerging Trends](https://cdn.olai.in/jjude/trends2021.png "Emerging Trends")

In the end, I will explain how to use these trends. Now let's look at some trends that I find interesting. For my analysis, I use the PESTEL framework - politics, economics, social, technological, environmental, and legal. I might not know trends in all of these buckets; even if I did, not all of them might impact me.

### Politics
- Joe Biden is old. The US presidency is a demanding job. During the first term, Obama looked young, but when he became president the second time, he looked old. That means Joe Biden might not seek a second term. Therefore, it is very likely that we will see a Republican president in the next US election
- Brexit might lead to other countries moving away from the EU. Or countries like Scotland might want to leave the UK
- India does not have a vibrant opposition. In that case, BJP is likely to win the next parliamentary election. Politicians, government officials, and even the judiciary are more likely to lean towards right-wing politics in order to gain power and positions. 

### Economics
- Mauritius opened its lands to remote working. Increasing numbers of countries will adopt this model. They will invest heavily in digital infrastructure and attract top talent. As more talents move to these countries tax laws and other laws may have to be rewritten. As the Chinese say, if you want to wealth build roads. More wealth will flow into these countries as investments flow into digital infrastructure. Such economic changes will reshape political and military power.

_(There is a precedence here. When India got independence, the majority of the development investment went to north India. The software boom brought south India new opportunities and changed power equations. The same thing happened in Belgium as well.)_

### Social
- Covid and its multiple variants will become a part of our lives. It may mean that we will have annual vaccines and vacations in private farmhouses and beaches.
As COVID hits harder on the older generation, there will be palpable fear of moving freely among them 
- With a growing elderly population and with more money in their pockets, technology companies will find a new cohort for their offerings. We might see Tinder for elders, LinkedIn services tailored to elders, etc.
- Industries based on digital technology grew during COVID. COVID affected labor-intensive industries the most. The economy will recover, although digital-based industries will prosper while others languish, causing wealth inequality.

### Technology
 - Blockchain will become a mainstream technology. We will see many uses of blockchain in our daily lives. Non-fungible tokens are probably the first to be widely adopted
- Some countries are beginning to accept bitcoin as an official currency. We will see more countries following this.
 - The use of 3D printing will reduce the costs of developing hardware prototypes, which will lead to interesting hardware startups. 
- Drones will be used for military and surveillance purposes. There is a high likelihood that drones will be used to deliver household items due to multiple mutations of the COVID virus and the need for physical distance. 

### Environment

- Groundwater levels are decreasing every year. Some parts of Punjab have seen a reduction of more than 50% in the last decade or two. To get groundwater, you have to dig deeper and deeper. This will make farming more expensive. It will also affect regular household activities. If this situation continues, next wars will be fought over water, as portrayed in Quantum of Solace.
- How often do you change your phone? Maybe once a year or once every two years? Like many of us, you contribute to e-waste. Disposing of piles and piles of e-waste will be a health and environmental challenge for several countries.

### Legal
- More and more countries will insist on the ownership of data generated within their borders


### How To Use These Trends?

- Say you agree with me that more countries will allow remote working. To take advantage of that trend, you'll start building your brand and skills so that you can work anywhere.
- Or you believe that Indian stock markets will continue growing despite Covid and other economic mismanagement. Then now is the right time to invest or put more money into the stock market.
- Or you say vacations will be spent on private farms. Then you could buy farmhouses and turn them into family and corporate retreats.

You get the point. 

I used to think of the future as a single point. Nowadays, I consider multiple scenarios for the future, and I prepare myself for them. When I prepare like that, I am well prepared to spot multiple opportunities, and then I can focus on the best one.

If you liked the post, hit the subscribe button below. Also, share with at least one friend.

Have a life of WINS.

### Quote To Remember

The outside world can push you into Day 2 if you won’t or can’t embrace powerful trends quickly. If you fight them, you’re probably fighting the future. Embrace them and you have a tailwind. - Jeff Bezos

## Continue Reading

* [How to find trends in your industry](/trends-in-industry/)
* [What small IT businesses can learn from Mary Meeker's internet trends report](/2017-trends/)
* [Seven Business Trends to Watch in India](/seven-business-trends-to-watch-in-india/)