---
title="Kavitha Ashtakala On 'Championing Customer Success'"
slug="kavita-customer-success"
excerpt="Hear expert strategies, best-practices, and real-life success stories to supercharge your customer relationships."
tags=["gwradio","biz"]
type="post"
publish_at="17 Oct 23 16:48 IST"
featured_image="https://cdn.olai.in/jjude/kavita-yt.jpg"
---

Getting a customer is a challenge. Relatively speaking, delighting a customer and converting them into a repeat customer is easier. That is the theme of today's conversation. I have Kavitha, a seasoned customer success manager. We are going to discuss examples, metrics, and challenges of a customer success manager. Hope you find this interview useful.

<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless src="https://share.transistor.fm/e/83e8bd9c"></iframe>

## What you'll hear
- What is customer success?
- Where do the customer success champions fit in?
- Metrics to track as a customer success manager
- What is the ongoing process we need to delight customers?
- Who does a customer success manager report to?
- Examples of customer success
- Skillset of a customer success manager
- How empathy helps
- Customer success manager in a services company
- What is the kindest thing someone has done for you?
- Who has manifested the best leadership quality in your life?
- What is the definition of living a good life?

## Social Media Shares
- LinkedIn: https://www.linkedin.com/posts/jjude_kavitha-ashtakala-on-championing-customer-activity-7119899930248327168--J3I
- Twitter: https://twitter.com/jjude/status/1714141096526127274

## Edited Transcript
### What is customer success?
Customer success can mean different things to different people, but at its core, it's about how you use your product or service to assist the customer in achieving their business objectives. 

In more technical terms, it's a standard business-related definition. But if we're looking at it in a more straightforward manner, there are two ways to approach it. Firstly, customer success is when you make the customer both look good and feel good. 

"Looking good" involves helping the customer achieve their end business objectives so they can shine in front of their management and customers. It's all about supporting them in reaching that goal. 

On the other hand, "feeling good" relates to the customer's overall experience. Are they genuinely happy and delighted to work with you? 

In a nutshell, I'd simplify it by saying that customer success boils down to **making your customer look good and feel good**.

### Where do the customer success champions fit in?

You see, if we go back about 20 years, to the days when products, like those shrink-wrapped boxes from Microsoft, were common, you might remember them. Back then, you had salespeople who would sell these products to customers.

Marketing has also evolved significantly. They were the ones responsible for your advertising and making sure that customers were aware of your products. They'd organize events, among other things. And of course, you had your product delivery processes and so on.

However, it's worth noting that customer success is a relatively recent development, particularly in the last 10 years, especially in India. Customer success acts as a common thread that runs through all these functions. In simpler terms, you can think of it as the "salt" in your food, connecting and enhancing all these functions.

### Metrics to track as a customer success manager

There are two primary metrics to consider. First, we have the **Net Promoter Score** (NPS), which is essentially a scale from zero to 10. Customers are asked whether they would recommend your product or service to someone else.

On this scale, ratings from zero to six represent detractors, those who aren't particularly satisfied with their experience, product, or service. Ratings of seven and eight fall into the passive category, indicating that these customers like your offering but aren't fervent advocates of your brand. The highest ratings, nine and 10, identify promoters. These customers are truly happy with your product and service. They're achieving their business goals and reaping significant value from it.

The Net Promoter Score is calculated by taking the percentage of promoters minus the percentage of detractors. It provides an objective measure of whether your customers are experiencing success and whether they're likely to recommend your product or service to others.

On the other hand, "Feel good" is measured through **Customer Satisfaction (CSAT) scores**, which is more focused on shorter-term, specific interactions. For instance, if a customer has a problem, raises a support ticket, and the issue is resolved effectively, their satisfaction is assessed on a scale of one to five. It gauges whether the customer was extremely dissatisfied or pleased with the resolution.

These two metrics, particularly NPS for the long-term perspective, play a crucial role in the responsibilities of a Customer Success Manager.

### How do you get top scores, and what tools to use.

Typically, the tools people use for tracking NPS (Net Promoter Score) or CSAT (Customer Satisfaction) scores are **standard surveys**. These surveys can be automated, and you have a couple of options for sending them to your customers. You can either send them through your regular email channels, or you can use a CRM (Customer Relationship Management) system that automates NPS surveys.

These surveys are quite straightforward, featuring questions you want to ask. At the end of the day, they provide you with a quantifiable score. As you rightly pointed out, feelings are something that needs to be quantified to make sense in a business context. So using these survey-based tools is a great way to measure how the customer is feeling and how well things are going for them.

### What is the ongoing process we need to delight customers?

You see, customer success isn't a one-time event. It's an ongoing process that extends throughout the year, starting from the moment you onboard the customer. During this **onboarding** phase, you're introducing the customer to your product or service and educating them on how to use it effectively. This initial step is crucial because without the customer understanding what your product can do, they can't proceed effectively.

The second step involves assessing whether the customer is **actively using and adopting** your product. This is essential because if they're not making the most of the product's capabilities, they won't achieve their desired outcomes or meet their business goals. It's important to ensure that all users are properly trained and well-informed about the product's features and capabilities. Regular usage is key, not sporadic interactions every few months.

Thirdly, **regular reviews** are a significant part of a customer success manager's role. They need to be in constant communication with the customer, both external and internal. These reviews aim to understand the customer's challenges, how well you're assisting them, where they currently stand in their journey, and whether their utilization has decreased. It's also an opportunity to suggest best practices that can help them progress. Offering additional training materials and collateral can enhance their understanding of the product. Communicating product roadmaps, such as upcoming features, is also important. This helps align the product with the customer's business needs and goals.

The fourth phase involves the **renewal** of the customer's contract. If they've been successful and have extensively used your product, they're more likely to renew. In cases where there's extensive usage and a strong business case, there might even be opportunities for expansion.

Another key aspect is **turning the customer into an advocate** or reference for your product. Customer referrals are invaluable in today's high-cost customer acquisition environment. Word-of-mouth recommendations from satisfied customers often hold more weight than traditional advertising.

The Net Promoter Score (NPS) is typically conducted once a year to gauge customer satisfaction. This is an essential aspect with its specific timeframe.

Moreover, executive business reviews are highly significant. They provide a strategic opportunity to engage with the company at a higher level. It's not just about user-level utilization; it's about the company's overall vision, goals, and milestones. Customer Success Managers also play a vital role in conveying the customer's voice internally, which can lead to changes and improvements, such as adding training programs based on customer requests.

In summary, the role of a Customer Success Manager is akin to a farmer, continually nurturing and nourishing the customer account, rather than a salesperson who is focused on acquiring new customers. These ongoing activities help in retaining existing customers and even turning them into advocates for your product or service. 
They've actually gone ahead and created a training program for that. 

### Who does a customer success manager report to?

Alright, so let's clarify something. Customer success is sometimes mistakenly equated with either customer service or sales, but in a well-established customer success organization, the ideal structure involves having a Customer Success Manager at the senior level, possibly even within the C-suite, reporting to a Chief Customer Officer, much like you have a Chief Revenue Officer and a Chief Sales Officer for the sales team.

This structure is crucial to differentiate the roles and responsibilities. Customer success should not be attached to sales. Sales has a distinct purpose, which is to acquire new business and focus on bringing in new clients. It plays a vital role but is quite separate. On the other hand, the function of a Customer Success Manager is to maintain and nurture existing customers, ensuring they achieve their desired business outcomes and also exploring opportunities for expansion.


### Examples of customer success

Let's discuss companies, initiatives, and programs related to customer success.

Okay, so, Salesforce is the company that played a pivotal role in initiating the concept of customer success. They recognized, at a certain point, that a substantial portion of their business came from existing customers. This realization led them to establish a dedicated function for customer success, with a clear focus on retaining and nurturing their existing customer base. They invested significant efforts in this direction.

Salesforce is a prime example in this domain, but there's another company that holds a special place, and that's Gainsight. Gainsight is a U.S.-based company with a presence in India as well. They have made significant contributions to the field of customer success, serving as pioneers and guiding the industry on best practices. They've helped define the roles within customer success, including the role of a Chief Customer Officer. They've also explored important questions, such as whether it's acceptable to not pursue a customer if there isn't a good alignment. Additionally, Gainsight has played a crucial role in shaping the fundamental metrics and principles for effective customer success. While many SaaS companies have adopted these models, Salesforce remains a leader in the field.

### Skillset of a customer success manager

One of my personal favorites is the notion that, in the role of a customer success manager, you need to be a collaborator. Being **a people person** is essential.

Now, although the title suggests "Customer Success," it's important to remember that there are two types of customers you must engage with. One is the obvious external customer, but there are also internal customers, such as the product team, the data analytics team, and even teams like finance and marketing. For example, you might need to address issues with invoices that involve the finance team. Collaboration is key, and you must possess strong interpersonal skills.

Additionally, it's essential to have data analytical skills to interpret data, even if you're not responsible for creating dashboards. You'll need to understand the data and effectively communicate it to clients.

Furthermore, your ability to make people feel at ease is crucial. Empathy plays a significant role, even though it may be slightly underestimated in the customer success field. When both your internal teams and customers are facing challenges, you need to bridge the gap and find solutions. These are the essential skills required for success in this role.

### How empathy helps

Empathy involves two key elements. Firstly, it's about **self-awareness**, understanding your own feelings and emotions. Secondly, it's about **being socially aware**, perceiving and comprehending the feelings of others within your network.

I've taken some courses in empathy and emotional intelligence, which have improved my understanding. The goal is to be aware of what you're feeling at any given moment. For instance, if a customer is upset due to an issue with the product, you need to recognize their frustration. As a customer success manager, you often find yourself at the receiving end of both praise and criticism, especially when things go awry.

In such situations, it's crucial to grasp someone else's feelings without reacting negatively. You put yourself in their shoes, in simple terms. Why is the person acting this way? Simultaneously, you need to understand your own emotions. For example, you might feel upset, but you must acknowledge that this stems from the fact that the customer is upset.

The key is to comprehend the reasons behind their emotions and effectively communicate this to your internal teams. It's not about merely allowing the customer to vent and then taking that frustration out on your colleagues. That approach won't yield productive results. There are also instances when your internal teams are under significant pressure, and it's crucial to empathize with them too. In essence, both self-awareness and social skills are vital components of empathy.

### Customer success manager in a services company

In my experience, I've mostly worked with companies that are either SaaS-based or those that offer training services, though not exclusively SaaS.

When it comes to today's IT companies, you might not always find a role with the exact title of "customer success manager." Instead, they might be handling various aspects of this role in parts. In large organizations, especially when dealing with major clients, they often have individuals known as business relationship managers. These professionals oversee not just the delivery or program management but also manage the overall project or a set of projects while nurturing the client relationship.

Does this perfectly align with the concept of customer success? Probably not entirely, but it certainly contributes to it. The core of customer success is helping the client achieve their business objectives with your product or service. With the current shift towards cloud-based solutions and the emergence of productized services, there's a growing need for dedicated customer success roles. As more companies embrace these trends, they will find that customer success becomes increasingly important, serving as a bridge to higher-level services and optimizing the use of the product itself.

### Rapid-fire Questions
#### What is the kindest thing someone has done for you?

If you're asking about an act of kindness someone has done for me, I can certainly share something meaningful. I often visit a government school on Saturdays where I interact with the children. A few months ago, on my birthday, I distributed sweets to them. I wasn't able to return the following week, but when I did, the children surprised me by expressing their wish to buy me a gift. This gesture from these kids, who attend a government school, touched my heart. It's the kind of thing that truly warms my heart. I realize the response may not have been quick, but it's heartfelt.

#### Who has manifested the best leadership quality in your life?

Certainly, empathy is a critical leadership skill in today's context. It's gaining more attention, and I want to acknowledge one of my former colleagues, who also happened to be my manager at a previous company. While I won't mention his name, he truly exemplifies what it means to be a leader.

Being a leader is no longer solely about a position or authority, as traditionally perceived. It's not confined to titles like CEO or VP. Today, leadership is about your actions as an individual. It's about taking steps to lead, influence, and demonstrate leadership qualities. You don't require a specific title to be a leader.

#### What is the definition of living a good life?

I believe the definition of a good life varies from person to person. For me, it's about doing what you love, not necessarily something that pays well. A good life, in my view, involves the ability to **positively impact the lives of others** and make a meaningful difference in their lives.

## Watch on YouTube
<iframe width="560" height="315" src="https://www.youtube.com/embed/tcBPvYGxhuA?si=-4e58w02fTQ-eVXc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

## Connect with Kavitha
- LinkedIn: https://www.linkedin.com/in/kavithaashtakala/

## Connect with me
- Twitter: https://twitter.com/jjude
- LinkedIn: https://www.linkedin.com/in/jjude/
- Website: https://jjude.com/
- Newsletter: https://jjude.com/subscribe/
- Youtube: https://youtube.com/gravitaswins
- Email: podcast@jjude.com
- Executive Coaching Program: https://gravitaswins.com

## Your feedback counts

Thank you for listening. If you enjoy the podcast, would you please leave a short review on Apple podcast or on YouTube? It takes less than 60 seconds, and it really makes a difference in finding this podcast. And it boosts my spirits. 😀

I would like to hear what resonated the most with you in this episode. Either email me or share it on social media and tag me. Thank you for your support.

Have a life of WINS.

_Gravitas WINS Radio is produced by [Joseph Jude](https://jjude.com/). Be sure to subscribe on [Apple Podcast](https://podcasts.apple.com/in/podcast/gravitas-wins-radio/id1562802449), [Spotify](https://open.spotify.com/show/7LOBjAyjBkechMC0xMIj9E),[Google Podcast](https://podcasts.google.com/feed/aHR0cHM6Ly9mZWVkcy50cmFuc2lzdG9yLmZtL2dyYXZpdGFzLXdpbnMtcmFkaW8=),[YouTube](https://www.youtube.com/gravitaswins), or the podcasting app of your choice._

Please check out other episodes of [Gravitas WINS Radio](/gwr-themes/). For easy consumption, they are categorized by themes. If you are a fellow podcaster, you might be interested in [How I Podcast](/how-i-podcast/) or [Tools I Use](/i-use/).

## Continue Reading

* [Ian Heaton on ‘Listening leaders are effective leaders’](/ian-listening/)
* [Roshni Baronia On ‘Digital Sales For Startups & SMEs’](/roshni-digital-sales/)
* [Ubellah Maria on ‘Transitioning From Developer To Manager’](/ubellah-maria-becoming-a-manager/)