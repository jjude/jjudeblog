---
title="What I Have Been Reading"
slug="what-i-have-been-reading"
excerpt="The best of books, blogs and newsletters (of 2013)"
tags=["sdl","consume"]
type="post"
publish_at="31 Dec 13 16:30 IST"
featured_image="https://cdn.olai.in/jjude/what-i-have-been-reading-gen.jpg"
---

If you are a regular reader of this blog, you know that I'm obsessed with reading. Reading is paramount to expand the horizon of your mind, as it's through reading that I get exposed to interesting ideas. I read books, blogs, and newsletters; basically anything that I can get hold of, which can improve my knowledge.

Here I list the best of what I've been reading this year (2013).

### <a name="blogs"></a>Blogs

I'm subscribed to more than 100 blogs on topics like technology, investing, and personal-development. Here are some of the best.

* <a name="alanblog"></a>[Alan Weiss](https://alanweiss.com): I have been reading Alan in books, blogs and newsletters for few years now. His prime focus is on **consulting** but what I've read have been useful in other areas in life too. If you are in consulting, his books and blog is a mandatory read.

* <a name="sethblog"></a>[Seth](http://sethgodin.typepad.com/): Seth is known for his marketing insights but like Alan, his ideas transcends marketing. His posts are short and quick to read, more like a daily inspirational posts. But there are days, I will have to re-read the post many times to assimilate the thoughts contained in the post.

  Another interesting aspect of his prolific blogging is that he breaks almost every rule of SEO experts. He ranks higher because of his ideas rather than idiotic SEO tricks. That is something to note for the aspiring bloggers.

* <a name="haroldblog"></a>[Harold Jarche](http://www.jarche.com/): I like to learn, but hate classroom-based learning. So I formulated [consume-produce-engage](/learning-phases-and-its-support-systems/) as a learning framework. As I searched through net to improve on the framework, I stumbled on Harold's posts on 'learning in a networked world.' His framework is seek-sense-share, which, as you can see, is similar to mine.

* <a name="farnamblog"></a>[Farnam Street](http://www.farnamstreetblog.com/): Shane Parrish distills wisdom from thinkers of the present and past and writes in a readable form. His goal, is to go to bed each night smarter than he woke up. That is a worthy goal and in the process he makes his readers smarter too.

* <a name="mondaynoteblog"></a>[Monday Note](http://www.mondaynote.com/): Frédéric Filloux &amp; Jean-Louis Gassée comment on the business model of intersection of journalism and technology. I believe that technology on its own isn't a miracle; but only when businesses tap the power of technology, miracles happen. Though I have no interest in media business, their comments on technology's impact on the media business are interesting.

* <a name="dilbertblog"></a>[Dilbert](http://dilbert.com/blog): Scott Adams begins most of his blog posts with a disclaimer: “This blog is written for a rational audience that likes to have fun wrestling with unique or controversial points of view.” That is precisely what makes his blog worth reading. He writes on anything that interests him—some day it is about kitchens and some days it's about goals vs systems. None of his posts are a bore.
* <a name="fredblog"></a>[Fred Wilson](http://www.avc.com/): Fred is a VC who invested in lot of successful internet businesses—twitter, tumblr, disqus, del.icio.us and so on. I use most of these sites, but that is not why I read Fred Wilson’s blog. He conducts MBA Mondays, in which he shares the practical knowledge he built investing in these successful companies and those lessons are wonderful and valuable.

* <a name="derekblog"></a>[Derek Sivers](http://sivers.org/blog): I came to know of Derek from his TED talk, [How to start a movement](https://www.ted.com/talks/derek_sivers_how_to_start_a_movement). As he describes himself, he is an 'avid student of life.' He freely shares, what he learns, without any hesitation. Every one of his blog posts (and TED talks) are amazing.

* <a name="radarblog"></a>[Oreilly Radar](http://radar.oreilly.com/) : Though the radar contains interesting perspectives about newer technologies or their application, I look forward to the curated short links that comes daily. I have discovered new tools and read articles with amazing perspectives on technology through these curated links.

Few years back, I started investing in stocks. For sometime, I invested based on tips from here and there. Not surprisingly, I lost money. Then I learned about value investing and started practicing it. Advantage of value investing is that the principles I learn in it, are useful in all areas of life and not just in investing. Below are two of the blogs that I regularly read to keep up with value investing principles.

* <a name="sanjayblog"></a>[Prof. Sanjay Bakshi](http://fundooprofessor.wordpress.com/): Prof Sanjay teaches at MDI, Gurgaon. At his blog he shares his lectures, his interviews and other insights on applying value investing principles in Indian stock market. If you are interested in value investing from an Indian context, there is no better person than Prof Sanjay to learn it from.

* <a name="safalblog"></a>[Safal Niveshak](http://www.safalniveshak.com/): by Vishal Khandelwal, an MBA(Finance) graduate who is pursuing an early retirement from the rat-race and into value investing. I am subscribed to his value investing course. I keenly observe how he has detached himself from the traditional ways of "earning," and builds his earnings purely through internet. I have heard of such ventures by folks from the US and elsewhere, not from India. He is an inspiration both on value investing and on the "retirement-from-rat-race" life-style.

### <a name="books"></a>Books

I [read 8 books](/books-read-in-2013/) this year:

1.  [Zen & Art of Motorcycle Maintenance](/books-read-in-2013/#zen) by Robert Pirsig
2.  [A Bank for the buck](/books-read-in-2013/#hdfc) by Tamal Bandyopadhyay
3.  [On Writing](/books-read-in-2013/#writing) by Stephen King
4.  [Responsive Web Design with HTML5](/books-read-in-2013/#rwd) by Ben Frain
5.  [Breaking Barriers](/books-read-in-2013/#barriers) by Janaki Krishnan
6.  [The Blue Sweater](/books-read-in-2013/#bluesweater) by Jacqueline Novogratz
7.  [Finding Favor](/books-read-in-2013/#findingfavor) by Lana Long
8.  [Free as in freedom](/books-read-in-2013/#free) by Sam Williams

(I posted short summary of these books earlier. You can read them by clicking [here](/books-read-in-2013/).

Keeping up with the technology, I am switching to eBook readers. Out of the 8 books I read, only [Zen & Art of Motorcycle Maintenance](/books-read-in-2013/#zen), was in physical book format. Ebook-readers are definitely convenient but taking notes with them is not as comprehensive as it is in physical book format.

### <a name="newsletters"></a>Newsletters

This year (2013), I started subscribing to newsletters. I generally prefer to read through a RSS feed reader (I use [Feedly](http://feedly.com/) on the cloud, [Reeder](http://reederapp.com/ios/) on the iPhone &amp; [ReadKit](http://readkitapp.com/) on the Mac). But not all content is available via RSS and I found these newsletters to be valuable to subscribe.

* <a name="bennl"></a>[Ben Evans](http://ben-evans.com/newsletter): Ben offers deep insights on mobile business through his weekly newsletter. In a short time he has grown his newsletter to more than 6000 subscribers with data and analysis. His newsletter doesn't have a fancy, flashy design, it is just a plain HTML format. But it is the content that makes it worth to read.

* <a name="alannl"></a>[Alan](https://alanweiss.com/): As I mentioned earlier, [Alan](https://alanweiss.com/) is an amazing thought leader when it comes to consulting and turning your expertise into profitable business. He runs three newsletters and I'm subscribed to all of them. Even if you are not into consulting business but an internal consultant, you will learn tons of tips from him. Go subscribe and be benefited. You can thank me later.

* <a name="hnnl"></a>[HackerNews](http://www.hackernewsletter.com/): [Hackernews](http://news.ycombinator.com) is a social news site for programmer-who-turned-entrepreneurs. When I developed BlogEasy, I used to spend a lot of time on the site. It took me some time to realise, I was spening too much time on the site. Since content on HN is beneficial, I looked for a curated page and came across this newsletter, in which [Kale Davis](http://www.kaledavis.com/) curates best links from [HackerNews](http://news.ycombinator.com) on programming and startups.

* <a name="senl"></a>[Stack Exchange](http://stackexchange.com/newsletters): Stack-Exchange started as a Q&A site only for programmers. Now it has grown to other areas, like English language, Politics, and Christianity. All sites in the stack-exchange network have weekly newsletters that curates the best questions & answers in that site. I'm subscribed to two weekly newsletters from stack-exchange network. Learning through other's questions and answers is a fantastic way to feed your mind.

My reading list is biased towards learning the topics that I'm interested in. Do you have a reading list? If you don't have one, create one. If you already have one, share it.

