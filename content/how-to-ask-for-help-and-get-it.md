---
title="How to Ask For Help and Get It?"
slug="how-to-ask-for-help-and-get-it"
excerpt="We recognize that with little help we can make a head-way. But out of inhibition, we do not ask for help. If you are suffering because you do not ask for help, here are five insights to help you."
tags=["problem-solving","insights"]
type="post"
publish_at="13 Jul 10 16:30 IST"
featured_image="https://cdn.olai.in/jjude/how-to-ask-for-help-and-get-it-gen.jpg"
---

'Ask and it shall be given to you', says the Holy Bible. But its not only the gods who listen and grant the desires of the mortals, but fellow mortals also listen and grant.

Though many of us recognize that with little help we can make a head-way, out of inhibition, we do not ask for help. If you are suffering because you do not ask for help, here are 5 insights to help you:

1) **Ensure you have done your part**: Experts generally are willing to help, but they do not want to be used. Before you ask them for help, do your homework and let them know what you have done.

2) **Ensure you are asking the right person**: Do not ask a sales expert, how to program in Python. Do your work to reach the person who has the skills, experience and education. Otherwise, you are only insulting them.

3) **Ensure you frame your request precisely**: Do you know what you want? Have you framed your problem? Do you know how you want the help? Think about these questions before reaching out for a helping hand.

4) **Acknowledge the help with thanks**: When someone takes time to respond with an advice or offer a helping hand, the minimum expected is a 'Thank You'. Depending on the quantum of the help and your desperation in seeking it, you can say 'Thank You' in more than one way. When you do it you are starting a relationship.

5) **Follow-up, if required**: If you act on the advice or used their help, get back to them with the results, especially if it is a positive one. Nothing makes a person more than knowing that they made another soul happy through their time and knowledge.

These are simple steps but if you follow them you will be building relationships continuously and there will be more people to help you in the future.

_How often you ask for help and how often you get it?_

## Continue Reading

* [Have you changed your mind lately?](/changed-mind/)
* [Smart People Ask For Help](/smart-people-ask-for-help-do-you-ask-for-help/)
* [Common Traps In Getting Meaningful Feedback](/common-traps-in-getting-meaningful-feedback/)
