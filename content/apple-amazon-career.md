---
title="Building Your Career - Apple Or Amazon Way"
slug="apple-amazon-career"
excerpt="Take a suitable business model and mimic to build your career"
tags=["coach","frameworks","gwradio"]
type="post"
publish_at="11 May 21 18:01 IST"
featured_image="https://cdn.olai.in/jjude/mimic-apple.jpg"
---
Apple and Amazon are both successful technology companies. Yet their operating philosophies differ. Using their operating principles, I discuss a mental model to build a career. Let's dive in.

<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless src="https://share.transistor.fm/e/a6e0b916"></iframe>

## Apple

Apple's core thesis is: 

> People who are serious about software should make their own hardware.

![How Apple Works](https://cdn.olai.in/jjude/apple-layers.jpg)

So they make chips, design computers, develop software, and even create services. They control every layer of their offering. Because of this, they can give an excellent customer experience.

Because their customers get fabulous experience, Apple charges premium prices. Apple promotes all its products as luxury products.

## Amazon

When it comes to Amazon, their core philosophy is:

> The competitor's margin is my opportunity

![Automation at Amazon](https://cdn.olai.in/jjude/amazon-automation.jpg)

They squeeze out as many pennies as possible from their operations so they can give their customers the lowest price. They use technology to be frugal.

## Your Career

Now that we understand the different viewpoints of these two companies, let us see how we can apply these lessons to build our careers.

You can be like Apple. 

You can understand the customer needs so well and develop expertise in every layer of the delivery. 

![How businesses can mimic Apple](https://cdn.olai.in/jjude/mimic-apple.jpg)

If you are a software developer, this might mean developing expertise in a programming language like Python, a framework like Django, and a business domain like healthcare. So when a healthcare customer talks to you, you can understand their needs, design a system to meet those needs, and can develop that system as well.

When you can go up and down the layers, you can control the customer experience and charge a premium much like Apple. 

_Read [How to deliver value in the digital age?](/value-in-digital-age/) to know more about these layers_

Or you can be like Amazon.

Take one component of a layer. Optimize it so much with tools and scripts, so that you can deliver your work faster. When your delivery is fast, you’ll get opportunities to work on multiple projects which in turn will deepen your expertise.

I hope you got a mental model to think about your career.

Do you have any questions? Let me know via [email](mailto:joseph@jjude.com) or [Twitter](https://twitter.com/jjude).

Have a life of WINS. 

## Quote To Ponder

_If you change the way you look at things, the things you look at change. — Wayne Dyer_

## Continue Reading

* [A Roadmap For Developers](/learn-next/)
* [11 Lives to Build a Rich Career](/11-lives/)
* [Building a career amidst a pandemic](/career-in-pandemic/)