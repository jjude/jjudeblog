---
title="Simply better entertainer"
slug="simply-better-entertainer"
excerpt="A category entertainer from Tamil Nadu"
tags=["coach"]
type="post"
publish_at="17 Feb 09 16:30 IST"
featured_image="https://cdn.olai.in/jjude/simply-better-entertainer-gen.jpg"
---

It intrigues the movie critics; he has reigned as a "[Super-Star](https://en.wikipedia.org/wiki/Rajnikanth)" of the Tamil movie industry for more than three decades. It is ever more intriguing when one realises that he is not even a Tamilian. As of this writing, he is one of the highest paid actors in South-East Asia.

He is so popular in Tamil Nadu that you can stop anyone, anywhere in the state and they will know about Rajini Kanth.

Never mind that his dance sequences are not elegent; never mind that his voice variations remain the same in every movie; never mind that he wouldn't be praised for his acting; but every movie of his is a sure success (okay, most of them) in both rural and urban areas - now-a-days even in Japan!

How did he do it?

I was constantly reminded of him, as I read through '[Simply Better](http://www.simply-better.biz/ "Simply Better")', where the authors argue that providing "Core Category Benefits" is the sure path to long-time success than providing differentiation.

Evaluate the Super-Star on that - he has mesmerized the audience with his fighting sequences, punch dialogues and most importantly style and he has stayed within that boundary. He has not differentiated himself with directorial ventures; method acting and so on and so forth. He provided just the core category benefits of the entertainment industry and reaped the huge rewards in return.

If I could think of one fine example of 'Simply Better' entertainment experience it is none other than, Rajini Kanth.

