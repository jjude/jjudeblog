---
title="How to avoid failures with thirty percent feedback?"
slug="30-percent"
excerpt="Nail the overall strategy before scaling an idea"
tags=["wins","frameworks","insights","coach","visuals"]
type= "post"
publish_at= "16 Sep 20 09:00 IST"
featured_image="https://cdn.olai.in/jjude/early-feedback.jpg"
---

If you want to avoid failures, you should seek feedback. You should seek them at two stages. The ones you receive early in your project will determine the direction; the ones you receive later will polish your product. Though both types are essential, the early feedback determines if you are on the right path. The earlier you identify strategic mistakes, the cheaper it is to correct.

![Cost of change accelerates over time](https://cdn.olai.in/jjude/cost-of-change.png "Cost of change accelerates over time")

For getting early feedback, I use a framework called "30% feedback." Let [Kevin][7] explain it:

> Thirty percent feedback is strategic, it is big picture... it answers the question 'Am I heading the right direction?'  and because it is early enough that there is time to course correct...Because it is early in the process, it glosses over fine details and polish, knowing those will be addressed later. 

Getting such feedback is not easy because it needs a different mindset from both you, the seeker, and the giver. You are putting yourself out in the open with an unpolished work. The giver shouldn't obsess over details but know you will fill the details later. They should see the forest instead of the trees at this stage.

When I started with this approach, I would polish one section of the project and ask for feedback. That is like fine-tuning the "login" screen and making it performant and secure and asking for the client's feedback about a social media application. I might have completed 30% of the project, but the output doesn't indicate the project's direction and strategy.

Now I create a rough "end-to-end" process before asking for feedback. 

![Get early feedback](https://cdn.olai.in/jjude/early-feedback.jpg "Get early feedback")

Let me explain that with how JPEG images are displayed. In the traditional form of rendering, called baseline display, images are displayed top to bottom one line at a time. Each line is optimized before moving to the next line.

In the progressive form of display, the whole image is displayed blurry and then improved until fully loaded. For useful feedback, you need to follow the "progressive" way of display.

### How I used "30% feedback" for launching a course

![Getting feedback for Gravitas WINS course](https://cdn.olai.in/jjude/wins-feedback.jpg "Getting feedback for Gravitas WINS course")

I formulated a [framework][2] after reading "[Thou shall prosper][4]," an excellent book by Rabbi Daniel Lapin. When I discussed the framework with some of the friends, they wanted me to take them along in the journey. Isn't the best way to [learn][3] is to teach? So I decided to launch a coaching program based on the framework. 

I drafted a Google doc that described the course. I explained the target audience, the benefit that they will reap, course duration, and course fee. I felt great after writing the document since I thought I had captured all the details, and the document was crystal clear.

I shared with friends, which included potential participants in the course, for "30% feedback," as I hadn't completed the course materials at that time. It was just a write-up of the course.

Comments started coming in. I spoke to a few more.

Almost everyone agreed they needed a course like this. Some questioned the price. Why would anyone pay ten times the cost of Udemy courses, usually available at ₹1000?

Some questioned the content. Is it about 3Cs (competence, confidence, communication) or WINS (wealth, insights, networking, and self-control)?

I called [Md Ali][5], who is in the coaching business for a decade. He advised me to emphasize on the Gravitas part.

This is 30% feedback in action. That one-page content was small enough to be manageable but big enough to be relevant for feedback.

I moved to the next level of feedback by launching "Gravitas WINS" to select few. Five students signed up for the course.

Need for the course ✅    
Price ✅    
Format ✅    

Once this batch is over, I will record videos and open the course for the public. That will be 90% feedback.

If you are interested in taking the course, please [email](mailto:joseph@jjude.com) me. 

### Other ways I use the idea

As a [CTO][6] of an IT services company, I formulate and review designs for customer projects. As an executive, I launch focus areas for my department every year. I use the "30% feedback" idea in both areas. I write the initial concept on a single page and circulate it to the interested parties. I get their feedback, and then I expand. Such a practice has helped me to avoid stupidity and also to improve the competence of the department.

### Start practicing "30% feedback"

The fundamental idea is to seek feedback on the overall direction and strategy, which could be a problem for a detail-oriented person. If you are one, let go of the desire for details in the initial stage.

Start detailing the significant points and questions you might still have. Write freely without worrying about the number of pages. The document should help the reader to imagine the "end-to-end" process. Once you have captured everything, you wanted to write, sharpen the document and bring it down to one page. Remember, it has to be manageable for the reader.

Send only to a select few who can see the broader picture. Insist that you seek "30% feedback", so they shouldn't bother about spelling, grammar, or fonts. (better yet, send them the link of this post 😉).

Once you have nailed the big picture, start adding details to each sub-idea and sections.


### Read Next
 
 * [How To Get The Feedback That You Deserve](/how-to-get-the-feedback-that-you-deserve)  
 * [Common Traps In Getting Meaningful Feedback](/common-traps-in-getting-meaningful-feedback)   
 * [When To Listen To Your Critics?](/critics/)
 
 [2]: /wins/
 [3]: /sdl/
 [4]: https://geni.us/thou-shall-prosper
 [5]: https://www.calmachiever.com/ali-vakil/
 [6]: https://www.linkedin.com/in/jjude/
 [7]: https://www.kevinball.com/2016/04/25/effective-feedback-thirty-percent-vs-ninety-percent/
 