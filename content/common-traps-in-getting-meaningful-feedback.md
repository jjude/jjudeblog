---
title="Common Traps In Getting Meaningful Feedback"
slug="common-traps-in-getting-meaningful-feedback"
excerpt="Three traps in getting feedback."
tags=["sdl","problem-solving","engage"]
type="post"
publish_at="13 Feb 12 16:30 IST"
featured_image="https://cdn.olai.in/jjude/common-traps-in-getting-meaningful-feedback-gen.jpg"
---


If [obtaining feedback][2] is essential, it is even more critical to avoid common traps associated with getting that feedback. Here are some of the common traps, I have encountered:

### Composition of team

Since government functions based on rank and class, a meeting will inevitably have dead-weights. They have no intention of providing any useful feedback. They are there just because there were asked to. They are harmless. But there is a set of people who can derail any hope of having a meaningful discussions. They are the Pseudo-Experts[^1]. Not only most of their talk will be useless, they will hold you prisoner to their expertise. Handling them will require all the emotional and psychological strength that you can muster.

### Hijacked objectives

Another trap is that a feedback meeting is pulled back into a brainstorming session. Members start to discuss ideas that were left out, or come up with new ideas where much time had already been spent to arrive at the current solution. Most often these hijacks are unintentional, but sometimes it's done to subvert the project. Unintentional diversions can be graciously handled, while the later has to be handled diplomatically, only with the support of the client.

### Delegation to lower level (non-decision-makers)

Those who are in-charge of providing feedback do this in the pretext of being 'too-busy'. The lower level resources won't have the authority to take any decisions draining your enthusiasm for the project rather quickly. You need to bring this to the notice of project sponsers as quickly as possible.

These are the top three traps I have witnessed. What are the traps you have witnessed?

[2]: /how-to-get-the-feedback-that-you-deserve/

[^1]: Pseudo-Expert: Someone who once successfully wrote a 'Hello World' in Java and thus believe he has become an expert in software engineering.

## Continue Reading

* [Smart People Ask For Help](/smart-people-ask-for-help-do-you-ask-for-help/)
* [How To Ask For Help And Get It?](/how-to-ask-for-help-and-get-it/)
* [Have you changed your mind lately?](/changed-mind/)
