---
title="What are you doing with your anger?"
slug="what-are-you-doing-with-your-anger"
excerpt="We, Indians, are in this state because you and I, didn't care."
tags=[""]
type="post"
publish_at="27 Nov 08 16:30 IST"
featured_image="https://cdn.olai.in/jjude/what-are-you-doing-with-your-anger-gen.jpg"
---

My blood boils. Anger raises up. I'm not alone with these feelings.

This is not the first time [Mumbai](https://en.wikipedia.org/wiki/26_November_2008_Mumbai_attacks) is attacked. And Mumbai is not the first city to be attacked. Still we don't learn our lessons.

Why?

It is easy to blame our hypocritical, inefficient politicians, who lack political will to curb terror - they in turn blame Pakistan and get away.

Do you know why we have gotten hypocritical and inefficient politicians?

Because we, Indians, don't care.

We don't care to vote; we don't care to pay tax; we don't care to know our rights; we don't care to stand-up for our rights.

As students, as employees, as lawyers, as executives, as teachers, as politicians, as citizens, we don't care.

We don't care as long as our comfort is not disturbed.

When will we realize that because of our carelessness terror is knocking at our door? Because we didn't care we got this inefficient politicians ruling us; because we didn't care, corruption has gone into every level of security apparatus; because we didn't care, loads of ammunition are carried through the streets of Mumbai.

We are in this state because you and I, didn't care.

Because you and I didn't care, Bharat Matha is gang raped.

But, you and I can be different - You and I can care about India. You and I can be responsible citizens. You and I can be the change that we want to bring on in India.

Then the question is, will this anger motivate us to bring about this change?

