---
title="Notes from 'Steve Jobs'"
slug="steve-jobs"
excerpt="Key points from this fascinating book."
tags=["books"]
type="post"
publish_at="15 Feb 12 16:30 IST"
featured_image="https://cdn.olai.in/jjude/steve-jobs.png"
---
My brother-in-law gifted me the book '[Steve Jobs](https://www.amazon.com/dp/1501127624?tag=jjude-20)' by Walter Isaacson, owing to my love for everything Apple. I can't tell you why I couldn't put the book down before completing it - is it my curiosity about Steve Jobs or the easy-to-read style of the book. Either way, it was a fascinating read.

![Steve Jobs Book](https://cdn.olai.in/jjude/steve-jobs.png "Steve Jobs Book")

Enough is already written about the book, so I'm not attempting to write a review of the book. Rather I present here those points I noted from the book. These are not word-to-word highlights but my understanding and interpretations:

### On Creating Products
* That he is a genius is proved beyond a doubt by disrupting not just his primary industry but seven different industries; and not one smash hit but many others even until his death;
* He empowered his customers by taking away features rather than giving them boatload of features. They loved him for that. He proved his mantra, '**things often lead to the opposites**';
* He not only created new markets, but created new type of customers - evangelists. His idea was to create products that his customers will go on rave about, that their friends will feel stupid not to own those products.

### On Marketing
* I have read a lot about Apple & Steve Jobs but never read about their marketing principles set out at the initial days of the company by [Mike Markkula][1]. It goes like this:

* *Empathy* : an intimate connection with the feelings of the customer: we will truly understand their needs better than any other company
* *Focus* : In order to do a good job of those things that we decide to do, we must eliminate all of the unimportant opportunities
* *Impute* : People do judge a book by its cover. We may have the best product, the highest quality, the most useful software etc.; if we present them in a slipshod manner, they will be perceived as slipshod; if we present them in a creative, professional manner, we will impute the desired qualities.

If there is anything that CEOs and companies want to learn from Apple or Steve Jobs, it should be these principles. Not his idiosyncrasies.

### On Company Development
Despite his big blown ego, he didn't want Apple to be only about him. He wanted to build a company that will out-last him. Media overhyped his eccentricities but didn't do justice to his ability to develop a company. Here is my view of range of factors he displayed in building Apple:

* **Leadership models**: He had wide range of leadership style. He wasn't 'nasty and demeaning' boss to everyone. He expected that if you are passionate about  your idea, then you should defend it even if the boss dismisses it.

* **Hiring**: He said,

>_'A' players hire 'A' players. But 'B' players hire 'C' players so that they can feel superior. Soon you got a 'bozo explosion' in your company._

>In the pretext of cost cutting, he didn't hire B & C players. He had the knack to seduce the best to join his team.

* **Succession**:  He didn't say, 'when I'm gone, world will return to dark ages.' Rather, he ensured a proper succession was in place and groomed his successor well to take over from him. I admire him for it. How many founders / CEOs are doing that?
* **Do people matter?**: Yes, he did hire 'A' players and avoided 'bozo explosion'. But when he was unceremoniously thrown out of Apple, it had the same people. But they couldn't produce what Steve Jobs produced. So people matter, but leadership matter more.
* **He didn't buy companies rather improved supply chain**: With all the money that he had, he could've bought all his rivals. But he marched on with a single focus of building aesthetically pleasing and 'just works' products.

### On Personal Life
Living in India, all that I know about his life is through this book and other news media. So I'm not going to pass any judgement.

Enough to say, **his life is a quintessential Hollywood movie** - given for adoption at birth, raised in a middle-class family, dropped out of college, hit a jackpot and became a millionaire before thirty but went into obscurity only to come out to change the world forever, managed to find love both in personal and professional life and importantly was lucky enough to live out his passion to the last minute.

Tell me, who would't want to be Steve Jobs?

### Explore More

* [Where there is vision, people prosper](/vision/)
* [Book Review - Making Breakthrough Innovation Happen](/making-breakthrough-innovation-happen)
* [People Aren't Potatoes](/no-potatoe/)
* [All Books I have read](/books-read)


_Disclaimer: Book link is an affiliate link_

[1]: https://en.wikipedia.org/wiki/Mike_Markkula
