---
title="Approximately correct"
slug="approximate"
excerpt="If you wait for certainties, you will lose opportunities; instead, aspire to be approximately correct."
tags=["frameworks","luck"]
type= "post"
publish_at= "28 Apr 20 09:00 IST"
featured_image="https://cdn.olai.in/jjude/approximate.jpg"
---

Calculus and Probability are two major streams in Mathematics. Calculus is precise, and Probability is approximate.

When I call you on your mobile, I expect the system to work with precision. Say I dial, 9976812345, and I want to reach that exact number. Not xx344 or xx346. Also, I expect these mobile systems to operate with such precision for every call.

We don't need such precision for everything in life. Even things that we assume are accurate are just approximations.

![Be approximately correct](https://cdn.olai.in/jjude/approximate.jpg)

Let us take flying as an example. There are indeed a lot of instruments guiding the pilot to fly with precision. Guess what? The time to reach a destination is an approximation. That is why it is called **Estimated** Time of Arrival (ETA).

Think of other everyday questions. 

* How much money should I set aside for groceries?
* How much will it cost to renovate my house?
* If I work hard, will I become the president of this company?

Answers to all these questions and many more are just approximations.

If you want to be successful in life as a whole, then aim to be approximately correct rather than precisely wrong. Don't schedule a meeting at 11.43 for 22 minutes and expect to stick to that non-sense. That is an example of being precisely wrong.

If being approximately correct is so useful, can we train ourselves to think in approximations? I am not sure it will work for all, but I have trained myself in three ways.

(If you notice, above para itself is an approximation ;-) )

### Learning from ancient traditions

> For an idea to have survived so many cycles is indicative of its relative fitness - Nassim Taleb

There were a lot of wise people in ancient times. Some traditions called them rishis, and others called them rabbis. The rishis and rabbis knew that when you go out into the town square and come back home, you could bring along viruses and bacterias. Of course, they didn't call them by those names, but they knew dirt is a disaster for the family's health. So they told everyone to wash their hands and legs before entering the home.

Like the present times, the youth of the ancient times questioned every command of the elders. They argued with every answer. Sick and tired of the arguments, the wise elders turned the command into a fear - "if you don't wash your feet, demons will come into the house." They are right figuratively but not literally.

Like this command, they codified a lot of other ideas. The sum of all those ideas became religions. Many ideas died their natural death while passing from one generation to the other or when people migrated from one geography to the other.

As Taleb says, fittest ideas survived. If you want to be wise, learn those major ideas across religions.

What are the common big such ideas?

All of these religions have rituals, celebrations, and a central book. Every one of them encourages living in communities, fasting in phases, and taking care of the poor.

Staying true to the topic of this article, I've approximated a lot. Yet, I'm sure you'll agree that I'm not far off from the truth.

Scientists have not yet proven the benefits of these ideas. Hence those who consider themselves as intellectuals poo-poo these ideas as unscientific. Though it is changing in the last few decades as psychologists are starting to study these old practices and finding enormous advantages in following them.

How can you practice these ideas?

- Like the central book of a religion that codifies major principles, write a set of **principles** that will guide you through your life situations. If you need some help to think of such principles, you can read [mine](/principles/) to start. If you are an executive, you can write one for your company too. 
- Have **rituals** that you follow daily, monthly, and yearly in your family -- family dinners, date nights, and traveling together. If you manage a company, then have town halls, quarterly team-outs, and so on.
- **Celebrate**. Have mini celebrations, micro celebrations, and even mega celebrations. Celebrate milestones in your life. Every seven years or so, have a grand celebration.
- Go on fast. Fast not only from food, but also from relations, electronic items, and comfort. If you are an executive, nothing replenishes your energy as going on a retreat with just a set of books. **Rest and fast to renew** is a good mantra today as it was in the old times.

Embrace the surviving ancient ideas. None of them may be proven scientifically to benefit you, but those concepts will help you survive uncertain and volatile situations that come your way.

### Learning from trends

> What the wise do in the beginning, fools do in the end. ― Warren Buffett

I have been lucky to be around a lot of successful people. Many of them share a common trait. They are always on the lookout for an uneven, emerging trend that could either lift them high or destroy them. Once they have a fair idea about a pattern, they anticipate and act to gain or minimize the damage. We call it luck, but it is just a combination of preparation and positioning.

Don't believe me? I'm going to let Jeff Bezos tell you how he started Amazon:

> The wake-up call was **finding this startling statistic** that web usage in the spring of 1994 was growing at 2,300 percent a year. You know, things just don't grow that fast. It's highly unusual, and that started me thinking, "What kind of business plan might make sense in the context of that growth?"

Jeff Bezos is not the only one. Shane Snow studied winners in different fields who applied [Smartcuts](https://www.amazon.com/dp/0062560751?tag=jjude-20) to come up on top. His note about surfing champions is relevant here. The winning champs arrive early and stare at the ocean to **identify wave patterns**. Then they pick the conditions to surf.

*(It is a fantastic book. Read my [book review](/smartcuts/) to get a glimpse.)*

Trends by their nature are not sure shots. Their path is probabilistic. In some cases, you may have to cut your losses and move on like what Jeff Bezos did with Amazon Fire. But if you continuously follow this approximately-correct mindset, then you'll win inevitably. Like how Jeff Bezos used the lessons from the failure of Amazon Fire to [build a better ecosystem of Amazon hardware](https://www.engadget.com/2018-01-13-amazon-s-flop-of-a-phone-made-newer-better-hardware-possible.html).

Do you think only business tycoons take advantage of trends? The wise political leaders also do that. Leaders in [South Korea, Taiwan](https://timesofindia.indiatimes.com/india/how-countries-are-fighting-covid-19/articleshow/75218907.cms) and the [state of Kerala](https://www.telegraphindia.com/india/coronavirus-in-war-against-covid-19-kerala-stands-as-tall-as-germany-south-korea-tiwan/cid/1767193) stared at the emerging COVID waves and took preventive steps to protect their citizens.

Kerala's health minister, KK Shailaja, [talked](https://www.thenewsminute.com/article/kk-shailaja-interview-how-kerala-flattened-covid-19-curve-122891) about the state's preparedness, well before the country contemplated any action.

> I first read about the virus spreading on the Wuhan University premises. ... We started the control room on January 24. We sent a message to all District Medical Officers that a virus has been reported, and the chances are that it would come to Kerala catching a flight...Later COVID-19 was declared a pandemic, whereas **we had started precautions way back foreseeing** that it would turn out to be a pandemic.

(emphasis mine)

Want to get ahead of the maddening crowd? Arrive early, stare at the wave, and take a chance.

### Developing multidisciplinary thinking

> New and good ideas come from having a very broad and multidisciplinary range of interests - Robin Chase

You can not expect to be approximately correct if you limit yourself to a single field of expertise. You need to read widely and expose yourself to opposing views.

Peter Kaufman, an ace investor, narrated the necessity of multidisciplinary thinking in an interview with [latticework Investing](http://latticeworkinvesting.com/2018/04/06/peter-kaufman-on-the-multidisciplinary-approach-to-thinking/) blog. It is such an insightful interview. You should read it in full.

In that interview, he lays out how anyone can start with multidisciplinary thinking. He encourages a learner to construct three buckets of big ideas. What are these three buckets?

> non-living (physics, geology, chemistry); living things (biology); recorded human history

The more you explore big ideas in multiple disciplines, you will start to build expertise to identify winning patterns like a champion surfer.

### My experiences with "approximately correct" approach

I follow the "approximately correct" approach in matters small and big. 

I have written more than 300 blog posts. Still, every blog post is only "approximately correct." Sure, I could pick a precise word or create a better image. But I publish when it is approximately right.

The same is true for parenting. In this COVID lockdown times, kids are home all the time. I could wait to do precise parenting. Or I could start with approximately correct parenting. I taught my boys the exercises I know in the way I know. In a short span of 21 days, they have learned well and even made a [good video](https://youtu.be/P4-lWTz4HHI).

_(While you are watching the video, don't forget to like the video and subscribe to their channel)_

Life is a dance of the probabilities. If you wait for certainties, you will lose opportunities; instead, aspire to be approximately correct. You'll enjoy building the life of your dreams. I mostly do.

## Continue Reading

* [Mastering sales as a new CTO](/cto-sales/)
* [Future of jobs is Hollywood model](/future-of-jobs/)
* [How to choose technology for your business growth](/tech-for-biz-growth/)