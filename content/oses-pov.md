---
title="An User's Perspective on OSes: Horrible, Bearable, and Adorable"
slug="oses-pov"
excerpt="Compare the three popular OSes"
tags=["tools"]
type="post"
publish_at="17 Nov 11 16:30 IST"
featured_image="https://cdn.olai.in/jjude/oses-pov-gen.jpg"
---


My wife has been requesting, for sometime now, to configure one of the unused laptops at home for her. In between taking care of our two year old ever-active son, indulging in cooking delicious meals and watching over-stretched banal Tamil TV serials, she wanted to check emails, browse net and may be use a spreadsheet to budget & track household expenses.

There are two unused laptops at home - one quite old with Windows 2000 on it & another not so old with Windows XP on it. Both of them work but don't recognize the home network and so don't connect to Internet. I could've searched for drivers or tried to debug but the pseudo-geek in me overtook my senses and I decided to install one of the Linux variant. I thought, it will be a latest OS, so will detect all the hardware devices. I finalized on [Kubuntu](http://www.kubuntu.org/) & downloaded the latest version.

Installation went smooth. As I rightly predicted, it detected all the devices and I was able to connect to Internet. Wifi didn't work, but I decided it can be fixed later.

I did a quick test with the essential applications - [Konqueror](http://www.konqueror.org/features/browser.php), [Spreadsheet](http://www.kde.org/applications/office/kspread/) and [Word](http://www.kde.org/applications/office/kword/). I even played a bit with the default games. It felt good.

It was time for some ego-boosting.

Only it was not.

When I observed her using the laptop and looked at Kubuntu from her eyes, I realized that the user interface isn't polished; menus, buttons and dialog boxes appeared scaffolded rather than complete.

She didn't like what I gave her. :-(

I had overlooked an essential aspect through this entire process. My wife is a normal computer user and she doesn't care if it is a superbly engineered, open-sourced, freely distributable, community developed OS (or apps for that matter). She expects a smooth experience from the time she opens the lid of the laptop to closing it. She will hardly spend more than half hour daily. If she gets frustrated at initial tries, the laptop will remain closed.

With that realization, I apologized.

She was happy to leave the laptop. She went back to cooking lunch.

Then I took the laptop where Windows 2000 was installed and tinkered with it. Soon it connected to net, in a wired mode. When I gave it to her, she was happier.

But she could be even more happier.

She had used my Mac time-to-time and that is how her desire for a laptop started. It was cruel of me to introduce her to Mac and then relegate her to Kubuntu.

'I want a Mac,' she said without saying it.

That is when the 'feelings-side' of my brain, which is punctured for a man by default especially when it concerns his wife, woke up with a point:

If I'm callous, I will dump her with Ubuntu; if I'm indifferent, I will give her Windows; if I'm in love, I will gift her a Mac.

Now, if you will excuse me, I am going to the Mac store.

### Also Read

- [What Is A Tool?](/what-is-a-tool/)
- [I used Android for ten months, and I don't like it](/android/)
- [How I Came To Own An iPad?](/how-i-came-to-own-an-ipad/)

