---
title="Have you chosen well?"
slug="choose-well"
excerpt="When all is lost, memories and meaning will stay. That is a good enough for me."
tags=["insights"]
type="post"
publish_at="14 Dec 15 00:05 IST"
featured_image="https://cdn.olai.in/jjude/choose-well-gen.jpg"
---

I have fond memories of Chennai. I got my first job there and spent seven formative years in that fantastic city. 

So it was painful to see that city battered with rain in the last few days. The city was flooded and people were stranded in their house-tops without food. Some of them lost everything they owned—house, certificates and every household item. Watching them was saddening.

During the same week, on December 1st, a friend of mine lost a 9-month baby girl. Two days prior we had seen each other and he joked that the baby was crying because she was scared of my bald head. When my wife phoned to inform me, I lost myself for few minutes. I didn't know what to say or even think. These two events disturbed me deeply. I couldn't focus on anything.

As a Christian, naturally I turned to Bible and prayer for comfort and answer. As I mediated, one verse kept repeating in my mind. 

> Mary has chosen the good part, which will not be taken away from her. _Luke 10:42_

The question that still rings in my head is this: have I chosen well?

To tell you the truth, I don't know.

I went through such an introspective process watching the horrors of 9/11.

Among the many interviews with the survivors, one interview stood out. One of the survivors escaped from the twin towers, only to hear that his sister and his brother-in-law was in the plane that hit one of the towers.

"What's the meaning of all these?", asked the interviewer. "There is no meaning in these events.  **We can only impute meaning by mending our future**", replied the survivor. 

I mended my life after reading those wise words. In fact, I changed my life drastically.

I chose family and friends over materialistic pursuit. I meet my parents and sister at least twice a year and talk to them almost on a daily basis. Though my sister is younger than me, I turn to her for advice and she doesn't disappoint me. It is my way of building family ties.

I spend as much time as I can with my kids (who are aged 5 and 3). I chose wife and kids than hobbies and mindless work.

As an introvert, I chose my friends; but I give as much as I could with thought and time. I call them up as often as I can. I share with them my learnings. Even at work, I try to build people rather than obsessively looking for my own growth.
I chose this path with great consideration. It has given me meaning, joy and contentment in life.

Floods or a terrorist attack might still take away everything in a split second. When all is lost, memories and meaning will stay. That is a good enough choice for me.

As the year ends, take few hours to ponder if you have chosen well.

### Also read

* [Until everything gets better](/until_everything_gets_better/)
* [Life’s little lessons](/lifes-little-lessons/)
* [38 Lessons](/38-lessons/)
