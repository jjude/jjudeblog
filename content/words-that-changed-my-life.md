---
title="Words that changed my life"
slug="words-that-changed-my-life"
excerpt="The words that brough magic into my life."
tags=["insights"]
type="post"
publish_at="19 Jun 08 16:30 IST"
featured_image="https://cdn.olai.in/jjude/words-that-changed-my-life-gen.jpg"
---

Recently I read an article titled, 'The Best Advice I Ever Got' in [Leading Blog](http://www.leadershipnow.com/leadingblog/2008/05/fortune_the_best_advice_i_ever.html). That triggered a walk through the memory lane to capture the best advice I got. There were so many good advices that came to my mind. But only one stood out; that was the one that changed my life, forever.

### Why it mattered?

I will get to those 'magical words' in a minute. But let me first tell you why it mattered so much to me.

As a child I had developed severe inferiority complex - about my appearance, about my inability to speak English and about many aspects of life. Though it didn't have much impact on my curricular life - I managed to be among the top 10 students in class - it considerably impacted my interpersonal life. I would literally run away from anyone and everyone, always imagining that no one would be interested to talk to me. That turned into a vicious cycle - I didn't speak to anyone due to inferiority complex; so I had very few friends; because I had few friends I would feel still inferior.

Because I studied well, my parents didn't notice anything wrong. In fact no one noticed anything wrong. It was attributed to my personality.

It is true that in the early days it didn't have much impact on my studies. However as I started loosing self-esteem, it showed up on my progress card. My ranks started falling down (which again aggravated the feeling).

With such inflated inferiority complex, I entered college.

### Those miraculous words

What I saw in college didn't help, everywhere I saw rich and handsome guys who were speaking English fluently. Or I thought so. I drowned still further in my low-esteem.

I wouldn't know if my college-mate 'Arun Edwin' noticed anything wrong; or were they impromptu words, but he uttered those magical words:
  > You're inferior to none and superior none; You are unique.

Those words hit me.

That was it. Those were the exact words that I had to hear. Yes true, I am inferior to none. I am what I am. What a revelation! What followed was a magical transformation. I was still the black, village boy who can't speak proper English. But my perspective changed - 'I am unique'.

### The Impact

I'm looking back after about 15 years.

It is not a simple change; it is a heavenly transformation - none of my current colleagues will ever believe that I once suffered from inferiority complex. I don't feel 'threatened' by 'handsome' guys anymore . I can strike a conversation with ease.

And my English improved very well. I am confident of writing articles, proposals and similar materials. I'm not (yet) perfect, but I keep improving myself.&#160; I dream of writing for journals like 'HBS working knowledge'.

Not just the first part of advice but the second part too had a profound impact - not superior anyone. Even when I've tasted success after success, I keep repeating those words to myself. As a matter of fact, every success humbles me - I realize that I could've been a total failure in life; so I ensure that I don't spoil my success with haughtiness.

If you feel inferior for any reason, however genuine they might seem, repeat those words with me:
  > You are inferior to none; You are superior to none; You are unique.

Those words will bring magical results into your life, as it brought to mine.

