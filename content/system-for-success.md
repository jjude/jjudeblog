---
title="System for success"
slug="system-for-success"
excerpt="Want to succeed in life? Have a system."
tags=["coach","systems","books"]
type="post"
publish_at="29 Jan 16 08:07 IST"
featured_image="https://cdn.olai.in/jjude/process-outcome-matrix.png"
---
Of all the subjects I studied in Electronics Engineering stream, control systems is the only subject that continues to have a profound impact on me. It introduced the concept of closed-loop systems, otherwise called feedback control systems.

A closed-loop system can be explained in simple terms as a system that acts on input variables to produce an output. Information about the output is fed back into the system to adjust the output.

You see closed-loop systems in every day life——water heaters, oven, aeroplane engine and so on.

I view life itself, as one big closed-loop system. Apply hard-work, long term thinking, and smartness, life will become enjoyable. Do more of what works and stop what doesn't work. This is the way I improve myself.

Given my proclivity to feedback control systems, it is no surprise that I enjoyed Scott Adams' new book, '[How to Fail at Almost Everything and Still Win Big][1]'. His basic premises in the book is this: goals are for losers; if you want to win in life, put in a system. He goes on to explain, in his characteristic humorous way, the system that he has put in place in his life to achieve success.

The book can be summarised into three major points:

1) Observe life. Apply bullshit filters to seek truth. Apply that discovered truth in your life.

2) Learn multiple skills. If you learn diverse skills, you move from strategies with bad odds of success to strategies with good odds.

3) Observe what works for you and adjust.

Scott Adams mentions six filters to identify truth. The filters are:

- personal experience
- experience of people you know
- experts
- scientific studies
- commonsense
- pattern recognition

He says that at least two of these filters should confirm the pattern so that you can incorporate that pattern into your life.

What are the skills one should learn?

- public speaking,
- business writing,
- a working understanding of the psychology of persuasion,
- an understanding of basic technology concepts,
- social skills,
- proper voice technique,
- good grammar, and
- basic accounting

When we are talking about process and outcome, it is worth mentioning the matrix, Michael Mauboussin mentions in one of his books.

![Process Outcome Matrix](https://cdn.olai.in/jjude/process-outcome-matrix.png)

In life, as like in investing, when we follow a good process and play a long game, the outcome is bound to be exceptional.

What is your system for success?

[1]: https://www.amazon.com/dp/B00FHI0XK2?tag=jjude-20
