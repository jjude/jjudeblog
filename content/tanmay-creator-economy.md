---
title="Tanmay Vora on 'Mindset for Creator Economy'"
slug="tanmay-creator-economy"
excerpt="How can you create a niche for yourself in this new economy?"
tags=["gwradio","biz"]
type="post"
publish_at="08 Mar 22 06:00 IST"
featured_image="https://cdn.olai.in/jjude/tanmay-vora.jpg"
---

I feel incredibly lucky to have Tanmay for today's conversation. He has been a pioneer in sketchnoting and has become a soloprenuer to play in the creator economy. We explore creator economy and how we can play in it. Hope enjoy this conversation.

<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless src="https://share.transistor.fm/e/b872b7b1"></iframe>

### What you'll learn?

- How to create a niche for ourselves?
- Why creators should have their own audience?
- How to connect your art with a context?
- How to handle the artist - entrepreneur conundrum?
- What are the first steps to take to become a soloprenuer?
- How can Indian thrive in creator economy?

![](https://cdn.olai.in/jjude/tanmay-vora.jpg)

_Edited Transcript_

### How you started with Sketchnoting and created a niche?

I was a new manager in year 2006. I was always fascinated by the process of writing. So I started documenting my lessons as a new people manager in 2006. And I started writing a blog. This blog was named QAspire because I was in quality domain back then.

I think I still am in the quality domain today. There was a fervent aspiration to deliver quality. And that's how the blog came into being. I thought I was writing for myself, but very soon I realized that I was writing for others. So my writing was a service to others. There were people reading my blog, people commenting on it from across the globe.

And so I started **seeing my writing as a service**. And if I'm offering my writing or my ideas as a service to others, then the challenge is how do I make it better? So in year 2009, I wrote my first book, which was called "A Quality Tweet". And this book was a collection of 140 chapters. Every chapter being one tweet, about 140 characters and 114 chapters divided in sections like people, process and leadership.

And the book was one of the first tweet book in the world. And it made a very good impact on people who read it. Ever since then, I've been on the lookout for ways to express myself more clear. And that's what brought me to 100 word posts. I wrote three books after that. 

In 2014, I started leading one of the larger organizations, about 700 people organization, as the head of country operations. I was the country managing director and it gave me a lot of playgrounds to test my theories, to build a strong formidable culture.

When my experience started to grow my need to put that out **in a form and function that talks to people, and that's brain-friendly** also grew. I stumbled upon sketchnotes, thanks to one of my friend, Abhijit Bhaduri.

I got started with that journey, and the thing with me is that **if I start something, I try to keep at it for a long time**. So I started a blog in 2006 and since 2006, almost 14, 16 years now that I've been writing every single month.

If I start something I'm very consistent at doing it. I think that's my strength. Same applies to sketch notes as well. I started doing sketchnoting in 2015. By then I had a significant audience on my blog as well, because I've written books and I was part of twitter communities and stuff like that back in 2015.

I started sharing my rough scribbles out there. People started liking it. So I started upping my game by having a better pen, better paper, eventually in 2018 I invested in an iPad and Apple pencil. I started drawing digitally and have never looked back since then. After I started this journey, I also got opportunities to work with some of the best-selling authors across the globe, helping them illustrate their ideas.

I've worked with global organizations and exceptional thought leaders whom I really who I really respect and admire. So it's been a wonderful journey so far, I must say. 

Talking about creating a niche.

**Innovation happens at the intersection of two disciplines**. I could be a great leader, but if I don't know how to express myself out there, or if I'm not comfortable with social media and using it for disseminating my knowledge, it might not work.

The two things that I bring together is my 23 years of corporate leadership experience combined with the visual thinking experience, and when I combine those two things, purpose starts to come up. **My purpose has been to enable better leaders and better leadership within the organizations.**

Sketchnoting is not a skill in itself. It is a tool for me. **When the tool is used in a context that has a purpose, niche starts to happen.** So my advice to people who are aspiring creators, is that **do not look at what you're doing as an absolute**, but look at it as a **component of a larger system**. Look for **intersections** between two things. That's where **differentiation** will happen. Intersections are **less crowded** than absolute areas. Use your art as a tool to **make a difference**. Influence, building a niche,and  having a lot of followers, all of that will **evolve as a byproduct of the journey.**

I lived in Chandigarh for seven years. If you enter the famous Rock Garden, there is a large stone structure there on which it's written: **The journey is the reward**. That's been my mantra for the last 10 years. I don't think about how many followers I have or I don't look at my website statistics.

What I do is **I just keep doing what helps others**. I keep **doing what adds value to this world and doing it in a way that's unique to myself**. That's a unique expression of who I am - whether it's my handwriting, the way I draw, the way I speak, the way I write. It's all unique to me. **If I package all my uniqueness, into my work and apply it in a context and purpose, then niche starts to happen**. It's not a goal that I want to create a a niche for myself. **My goal is to make a difference. Niche happens as a byproduct.** 

### How do you ensure that what you're putting it out is creating value? 

First thing first, **creators have to be on their own platform**. They have to own their data, they have to own their content.

**Having your own audience is the most powerful thing in a connected world**. Otherwise you can still have an audience, but it will come to you through platforms. And platforms are driven by algorithms. So it starts with having your own audience,

Number two, is that as a creator, as a, as a Sketchnote creator, I try to **look for how this aligns to my purpose and then I iterate**. Every single drip of sketchnote that I share out there in the world may not be great. But, once I get a feedback that, this is a sketch note that helped, I'll ask, **how can I make it better**? Then the next sketchnote gets even better. 

So just to give you an example that I drew a sketchnote on how to simplify work. But there was so much content in it that it, that sketch note on simplifying was complex. Lot of people praised it. It was viral.However, a couple of people told me that this sketchnote on simplifying work is itself a very complex. 

Then I started to ask how can I make it simple? How do I make it more brain-friendly? How do I only extract the most essential parts of the idea? 

Because **my work is a service to idea**. As a customer, if you come to me and say, I want to create a sketchnote, I'll say, it's not about me and you, it's about the idea. And two of us are in the service of the idea. My work, my sketch notes, my visual articulation, visual synthesis, visual co-creation are all in the service of the idea.

I want to make ideas go far and wide. My sketchnotes are simply a tool to do it. If my ideas spread far and wide, I will basically have better leadership. And that's why I think it's important to connect your art with a context. My context is organizational and business leadership. How can we make better leaders? How can we cultivate better leadership within the organization? And if that's my purpose, my sketchnotes are a tool. And **when you start sharing it out to the world, people who are sharing the same context in the same problem statement with you would walk up to you and say, hey, this is fantastic**. This is great.

That **feedback fuels the pursuit**. It fuels the journey and that's how the journey goes forward. It. It defines what next step you will take next. I don't look at the whole journey and create a three-year or five-year plan. What I do is I'll say, what do I do today? And then based on the feedback, what do I do tomorrow, next month, and next six months. It's a very iterative process. 

Feedback, having your own audience, marrying your art with the purpose, and using it to solve important problems is important. 

Leadership is one problem that we have in organizations. But the other problem we have is that people are bogged down in information. There are books. Everything is craving for your attention. You're scrolling all the time. How do you make sure that you create something that people sit back and take a notice that they consume and it helps them? So sketches solves that problem.

It's not about how great I am. It's not about how wonderful my drawing or handwriting is. It is about servicing the idea in a way that it reaches far and wide, which in turn leads to behavior change, which in turn leads to better leadership and better organizations.

You can see the chain. It's not about art or ideas. It's about serving the idea. 

### How did you take the decision to become a solopreneur?

Just with everything else in my career, which was mostly unplanned, but, led to the right places. The **COVID accelerated the great awakening**. After being a corporate leader for 23 years, COVID really allowed me to sit back and reassess my priorities and thinking about what could I do? What are my strengths? 

I want to live a life that is inside out and not outside in. We are mostly driven by external demands and external KPIs and whatnot. I thought after 23 years, I could step back and live an inside out life. What that means is that I've already been successful in a corporate career.

Now, how do I become more significant in what I do? So the journey was from **success to significance** and from outside in to inside out. 

I already had the tools. I had this rich experience. I've been doing leadership trainings, workshops, coaching you know, executive coaching, performance consulting, etc. For almost last 10 years so I had that platform. If I combined all that experience, it might be a formidable offering maximizing my impact to more than one organization.

That pursuit led to very large organizations like Microsoft and several other global customers. The nature of work I do today is very diverse from working with authors to illustrate their ideas, to working with company directors to coach them, working with organizations as non-executive board member. So there are a lot of different things that I do today that it's creatively far more satisfying. The impact it makes is visible and it's been a wonderful journey.

### What mindset should I develop to pursue becoming a solopreneur

If you want to be an independent creator who wants to add value, and assuming that **you start with a purpose**, the first thing you need is the **mindset of generosity**. Because we have been trained to keep knowledge to ourselves, not collaborate, you know, really compete with people who are doing similar kinds of work and stuff like that. And the mindset of generosity, the mindset of abundance says that that **there's enough for everybody**.

We should just **focus on what we are doing and try to do it best**. And what that means is working out aloud. Putting your process out there, putting your lessons out there. I think that's very important. 

The second part is about **building audience**. Now, building audience is not easy. It took me like 15 years to build a following on my blog.

Follow **three C's of leading and learning on social media**, which is **creating, curating and contributing**. Now, what I mean by that is that we have unique ideas. We should be able to create new stuff and put it out in the world. That's step number one.

Step number two, is we need to curate, which means that like many people are sharing. How do we put all of that into a frame that people can relate? 

And then the third is contribution, which means that if Joseph has a great idea, and if I have an idea that adds to Joseph's idea, then I should be able to comment on it. I should take the idea further, maybe write a complimentary blog post.

**Social media is not about media**. To be honest, it is about being social, which means the way we are talking today. This is social. This is a conversation that's enriching both of us at the same time. And probably the listeners will listen to this podcast. 

The problem today is that creators get hung up with what platform should I use? What tools do I use? How do I set up my automated workflow etc? Those are problems that you can easily tackle later on. **The first problem is how do I build my own content on my own platform so that I'm not dependent on the algorithms such that I'm able to serve my audience**. That is at the heart of it.

The second is consistency. Lot of people, start putting things on Instagram. The moment the hits go down, they'll switch to Twitter. Then they will go on to blogging and then leave it and then go on to something else. They're always looking for the next shiny object. My submission is that once you own your own platform, which is in my case, my website or my blog, I think we need to be consistent, keep at it for a long time without getting bogged down. And as I said, **not look at the journey and outcomes, but look at the steps**.

One of the best advice that I ever heard on this on creating, you know, a consistent flow is from Austin Kleon. He says that **create stock**, a flow of work. Write everyday on your blog, tweet every day. Once in a while, take a stock of all of that, compile it into another useful form. 

Go with the flow. Do things every day. Once in a while, pick what's useful from that stock and offer it to your subscribers, share it online, maybe sell it.

The last thing is lifelong learning. Ask, how does this help others, what problem does this really solve? How can I do it? How can I make it better and what change will this bring? This is my **checklist for every creation**. If some creation doesn't answer these questions, then I will probably create it, but not share it. 

And **focus on the long game**, because there are no shortcuts in life.

### How do you handle the artist-entrepreneur spectrum?

You're putting your finger on a painful nerve. It's never been easy for me. As a solopreneur you get to do the normal business stuff. You have to send proposals, you raise invoices, you talk to customers, understand their specs and stuff like this. It's a lot of work and a solopreneur is very limited in resources.  But in the last one and a half years that I've been into business, I've learned how to deal with it. 

So what I do is that on my calendar, **I have blocked my time for delivering services**. So one of the things I do is I work with global organizations and authors and deliver services to them–training, teaching, coaching, visualization, graphic recording, visual facilitation. Whatever that service of our offering is, **it needs to be on my calendar**.

I also build products. I build information products and stuff like that. So part of my week is devoted to creating products. Luckily, I don't have any sales effort because all the sales happens in pull mode.

I establish **routines for reflection and retrospection** because on the treadmill of doing, we need to step down once in a while, take a breath, assess what we are doing, why we are doing it, what difference does it make? So I think that for solopreneurs, they need to **step down the treadmill of creating and consuming** and spend time reflecting and retrospecting what they've done, what difference has it made to them and to others, how can they up their game, what is their plan for next three months, and so on. Most people don't spend time reflecting and retrospecting. Therefore **learning happens, but it does not consolidated because you're never reflecting on it**. Blocking time on your calendar for reflection is a very powerful technique that has helped me.

Then the final thing I want to say is that, **choose wisely**! You cannot be everything to everybody. So you have to choose your customers wisely. You choose your pursuits wisely. You have to **experiment in short bursts** so that you don't spend too much time doing something which doesn't sell or doesn't make a cut.

The real entrepreneurial work is not raising invoices. The real entrepreneur work is not to find customers. The **real entrepreneurial work is to choose what's most effective**. 

### Will being a creator work in India?

When you're playing in the creator economy, you are essentially playing to a global audience because creators work on internet and they are sort of more broad-based in that sense, which means that I think that anyone can become a creator as long as they're engaging their audience in innovative ways.

Your audience could be in India, US, or anywhere in the world. The key is that **we have to believe that we can become a creator**. That's number one. If I started my creator journey as a side gig, not completely believing whether it will work, then I'm going to put half-hearted effort, then I'm not going to be committed and consistent. I'm not going to do lifelong learning around it because I'm not convinced this will work. We don't spend energy on things we not sure about. So the first thing is that belief. 

The second is that we have to **have fun along the way**, which means that we have to ensure that our work helps others, but in a fun way.

As a creator you have to **define your audience very clearly**. My audience, for example, is people who are senior business leaders, aspiring leaders, executive coaches, consultants, and people who are responsible for spreading ideas within the organization and in the society.

**Everything I do is in the service of them**. Whether we can thrive in creator economy or not, depends on how we take that journey in our head and start from there. But I personally believe anybody can be a creator. 

**There are challenges** along the way. 

The platforms are still evolving. The business models are not really clear. There's a lot of noise. There's a lot of competition, which means that platforms are actually squeezing the creators. They're charging up to 30% of what creators earn. That's why I think it's important that we build our own platform or an audience.

Focusing on your journey without focusing too much on the destination, taking one step at a time, seeing what works, interacting with people, getting feedback are very important.

We need to have our own platform, that should be the **hub of all our work**. And then **social media platforms could be spokes** that bring the traffic to your website. We have to adopt the hub and spoke model.

We have to **show we don't have to sell**. As creators, the first thing we do after connecting is to let them know what we offer. Then people get disconnected. We have to show our work and take every conversation with the mindset that **I don't want to squeeze this conversation**. I want to add value to the conversation. And then business automatically happens as a byproduct of that conversation. I've had customers who've been working with me for more than five years now.

We have to also understand that we have to **build for what makes difference**, not build for what makes it viral. There is so much of content that's aimed at being viral. It can be viral today, but tomorrow it's forgotten. My theory is that I will focus on creating stuff that has a long shelf life. And the key question I ask myself is if my daughter who is 15 years old now, or if my son who's 10 years old, if they consume this content after ten years will it still be valued?

I'm looking for **longer shelf life**, maybe **timelessness of the idea** before I convert it, because those are the ideas that the world needs more of. We need gentle reminders and nudges about what is right and what's wrong and how to do so. I don't build for virality. **I build for delivering value and then virality happens as a byproduct**.

The final thing, I want to say is that **we have to support other creators** because creator economy is not easy. And instead of looking at everyone as a competition, if you start collaborating with people, exchanging ideas, sharing our knowledge freely, I think what starts to happen is that **the ecosystem starts to lift itself**. That's how we grow together. That's how we learn together. Communities are powerful. Whether you like it or not, you're a part of community. Why not choose them authentically so that they add value to your pursuit and your pursuit adds value to that community.

### Mindmap of the podcast

[Sathya](https://twitter.com/aurasky_), a supporter of the podcast and a visual creator himself shared this mindmap on [twitter](https://twitter.com/aurasky_/status/1501925280583405571). When I asked for permission to include on my website, he graciously agreed. If you are on twitter, you should follow him.

![](https://cdn.olai.in/jjude/tanmay-mindmap.jpg)

### Watch On Youtube

<iframe width="560" height="315" src="https://www.youtube.com/embed/IzH3o3_1N_k" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Connect with Tanmay:
- Twitter: https://twitter.com/tnvora
- LinkedIn: https://www.linkedin.com/in/tnvora/
- Website: https://qaspire.com/

## Connect with me
- Twitter: https://twitter.com/jjude
- LinkedIn: https://www.linkedin.com/in/jjude/
- Website: https://jjude.com/
- Newsletter: https://jjude.com/subscribe/
- Youtube: https://youtube.com/gravitaswins
- Email: podcast@jjude.com
- Executive Coaching Program: https://gravitaswins.com

## Your feedback counts

Thank you for listening. If you enjoy the podcast, would you please leave a short review on Apple podcast or on YouTube? It takes less than 60 seconds, and it really makes a difference in finding this podcast. And it boosts my spirits. 😀

I would like to hear what resonated the most with you in this episode. Either you can email me or share it on social media and tag me. Thank you for your support.

Have a life of WINS.

_Gravitas WINS Radio is produced by [Joseph Jude](https://jjude.com/). Be sure to subscribe on [Apple Podcast](https://podcasts.apple.com/in/podcast/gravitas-wins-radio/id1562802449), [Spotify](https://open.spotify.com/show/7LOBjAyjBkechMC0xMIj9E),[Google Podcast](https://podcasts.google.com/feed/aHR0cHM6Ly9mZWVkcy50cmFuc2lzdG9yLmZtL2dyYXZpdGFzLXdpbnMtcmFkaW8=),[YouTube](https://www.youtube.com/gravitaswins), or the podcasting app of your choice._

Please check out other episodes of [Gravitas WINS Radio](/gwr-themes/). For easy consumption, they are categorized by themes. If you are a fellow podcaster, you might be interested in [How I Podcast](/how-i-podcast/) or [Tools I Use](/i-use/).

## Continue Reading

* ['Secrets of A Master Storyteller' with Jayaram Easwaran](/jayarame/)
* [Winning With Ideas](/winning-ideas/)
* [How to amortize your efforts in producing content?](/amortize-content/)
