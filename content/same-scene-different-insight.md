---
title="Are we even looking at the same thing?"
slug="same-scene-different-insight"
excerpt="Looking at the same scene and coming to a different insight from others"
tags=["coach","insights","wins","visuals"]
type="post"
publish_at="16 Aug 22 17:58 IST"
featured_image="https://cdn.olai.in/jjude/opposite-of-truth.jpg"
---

We don’t look into the world as it is, rather we look into the world as we are. 

It was clearly evident in the reporting of the recent Sri Lanka crisis. Twitter was abuzz with commentary when news of Sri Lanka's president fleeing was reported, not just what was happening, but why.

One set of Indian Twitter users was saying the Sri Lankan economy collapsed due to dynastic rule and corruption of these dynastic family members. According to them, India is immune to such a collapse since it has gotten rid of "that corrupt dynastic family".

Other people pointed out that the collapse resulted from oppressing minorities and revolutionizing agriculture in a stupid way. India is susceptible to such a collapse because Indian leaders have taken these steps and continue to do so.

While watching these two arguments roll through my feed, I wondered if they were even talking about the same issue.

Arguments like these aren't just limited to politics. Even religion. Such biased arguments also exist regarding climate change, both in terms of causes and remedies. With sports stars like Novak Djokovic throwing their opinion into the mix, even essential topics like covid vaccination can become polarized. You might not believe this, but programmers used to argue continuously over a mundane topic–whether to use spaces or tabs for indentation.

![Opposite of truth is another profound truth](https://cdn.olai.in/jjude/opposite-of-truth.jpg "Opposite of truth is another profound truth")

You can ignore the majority of these polarized debates without any risk. Inevitably, a topic will arise for which you must take a side. Even worse, seek the truth. It's worse because you can't delegate that responsibility.

When the stakes are high, you get to work in silence, closing the world and its chatter behind. You may look at the same situation, but you'll see the factors differently from those whose skin is not in the game.

When you find yourself in such a situation:
- Know that you yourself are going to be biased
- Seek experts who have spent time in the field rather than the critics playing on the side
- Listen to both sides. It may take a few tries to hear and understand what others say
- There is going to be truth on both sides. You need to decide what factors matter to you
- You should make your own hypothesis based on what's important to you as you listen to each party. Prove or disprove those hypothesis by looking for evidence.
- Don't hesitate to confront the parties with your hypotheses and evidences
- You shouldn't stick to your hypothesis if evidence disproves it. When the facts change, change your mind

Come to your own conclusions, not compromise. That is how you seek the truth.

## Continue Reading

* [Ignorant Of Many Things](/ignorant-of-many-things/)
* [Winning With Ideas](/winning-ideas/)
* [Season And Situation For Every Advice](/advice-you-need/)