---
title="An Award To End The Year"
slug="an-award-to-end-the-year"
excerpt="It is always nice to get an award."
tags=["retro"]
type="post"
publish_at="13 Dec 12 16:30 IST"
featured_image="https://cdn.olai.in/jjude/webratna.jpg"
---
I have been an independent consultant for the past four years. I have been associated with Ministry of Corporate Affairs (MCA) all these years. Initially I was in charge of the, then, newly formed LLP act in e-governance mode. Once that was successfully implemented I continued with other engagements within the Ministry.

When I became an independent consultant, there were certain aspects of corporate life that I left behind. One of them is, getting awards.

Or I thought so.

This week, MCA21 project was awarded a [webratna](http://webratna.india.gov.in/) award and I was also one of the recipient of the award as part of the project-team.

It feels nice to be appreciated. More so when least expected.

![Award Photo](https://cdn.olai.in/jjude/webratna.jpg "Web-Ratna Award Photo")

![Certificate](https://cdn.olai.in/jjude/webratna-cert.jpg "Web-Ratna Certificate")

### Retros of the earlier years

I've been publishing annual reviews almost every year. You can [read them](/annual-reviews/) to understand the journey I've come through.