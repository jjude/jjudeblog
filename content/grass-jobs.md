---
title="Green grass stain on Steve Jobs' shoes"
slug="grass-jobs"
excerpt="Are you obsessing over the right things?"
tags=["wins","self","coach","productivity"]
type="post"
publish_at="10 Nov 21 08:49 IST"
featured_image="https://cdn.olai.in/jjude/grass-stain.jpg"
---

Apple's marketing philosophy hinges on three principles. 

1. **Empathy** – an intimate connection with the customer’s feelings. "We will truly understand their needs better than any other company.""
2. **Focus** – "In order to do a good job of those things that we decide to do, we must eliminate all of the unimportant opportunities.""
3. **Impute** – "People DO judge a book by its cover. We may have the best product, the highest quality, the most useful software etc.; if we represent them in a slipshod manner, they will be perceived as slipshod; if we present them in a creative, professional manner, we will _impute_ the desired qualities."

Apple uses the same marketing philosophy for their conferences. The keynote speeches are [rehearsed][1] until they are perfect.

**How we do one thing is how we do everything. Isn't it?**

Or is it?

![Grass stain on Steve Jobs' shoes](https://cdn.olai.in/jjude/grass-stain.jpg)

John Grubber, is a popular technology blogger. He shared many anecdotes about Steve Jobs and Apple in his blog. I'd like to share a story I read from Grubber's blog, about the 2011 Apple Worldwide Developers Conference.

Steve Jobs enters and delivers an outstanding speech. Perhaps he knew it was his last performance. During this moment, [Grubber notices a green grass stain on Steve Jobs' shoes][3]. 

John hypothesizes, 

> ..in the run-up to his final keynote, Steve made time for a long, peaceful walk. Somewhere beautiful, where there are no footpaths and the grass grows thick. Hand-in-hand with his wife and family, the sun warm on their backs, smiles on their faces, love in their hearts, at peace with their fate.

Jobs either didn't notice the stains or he didn't care. 

As John concludes,

> Those grass stained sneakers were the product of limited time, well spent.

When you keep in mind, [your limited time on earth][2], you know what to obsess over and what to let go. 

As someone who publishes a newsletter, a podcast episode, and a live show every week, I think about this incident frequently. I don't want to ship a slipshod product, but I also don't want to obsess about grass stains. 

The balance is not easy, but it is required.

What are you obsessing over? What is holding you from shipping?

[1]: https://www.inc.com/carmine-gallo/steve-jobs-practiced-1-habit-that-turned-good-presentations-into-great-ones.html
[2]: /my-obituary/
[3]: https://daringfireball.net/2011/10/universe_dented_grass_underfoot

## Continue Reading

* [Building Your Career - Apple Or Amazon Way](/apple-amazon-career/)
* [Building In Public, Jeff Bezos Style](/build-in-public-bezos/)
* [Build on the rock for the storms are surely coming](/build-on-rock/)

