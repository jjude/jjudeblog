---
title="5 Steps To Achieve Your Goal – Lessons From Failures"
slug="achieve-your-goals"
excerpt="Are you moving towards your goal or just spinning round and round?"
tags=["coach","startup","insights","action","wins","visuals"]
type="post"
publish_at="02 Aug 10 16:30 IST"
featured_image="https://cdn.olai.in/jjude/goal.png"
---
Once upon a time, I created a popular software [application](http://www.tucows.com/preview/324669). It was featured in few technology print magazines in Greece, Germany & Holland. Though it was a free tool, I got sizable donations. And more importantly, I got appreciation from around the world.

All of this during the days of [Geocities](https://en.wikipedia.org/wiki/GeoCities). Yes before blogging and Twitter I witnessed [viral marketing](https://en.wikipedia.org/wiki/Viral_marketing).

Then I went stupid.

I had developed the application in C++ using [wxWidgets](http://www.wxwidgets.org/) tool. I found C++ to be cumbersome and converted the codebase into Python. Then into C#, then to IronPython; all this time without adding any feature. With changes in life & family commitments, this took almost 6 years and within that time the marketplace changed.

I did the same mistakes with blogging. Instead of adding content, I installed and played with one blogging platform after another - from [Nucleus](http://nucleuscms.org/) to Mambo to [Drupal](http://drupal.org/) to [Wordpress](http://wordpress.org/). This process killed any creative energy I had.

![To Goal?](https://cdn.olai.in/jjude/goal.png "goal")

Analyzing myself (& reading through others' experiences), I realized that I was going round & round and not towards the goal. Hell, at one time I didn't even know what my goals were. Is it to program or is it to compare cross-platform tool-sets; is it to improve my writing or to evaluate blogging tools.

My focus got blurred & I strayed from the road.

But burning fingers is a powerful teacher. It etches it's lessons in an unforgettable manner.

Those burns enabled me to have a sharp focus and achieve some personal & professional goals. Here are my takeaways from those failures and subsequent successes:

1.  **Know What You Want To Achieve** : You need to clearly define your objective. Is it to learn Python programming or to develop a software product in Python and market it. Both seem related but they are not. Take a pen and write your objective in your diary. Not in your laptop; not in post-it but in your diary. If you don't have one, buy one.

2.  **Set A Time Period** : Be reasonable while you set yourself a time period to achieve. Consider your family commitments, changes in life and so on. I give myself blocks of three months and I've found out that you can get a lot done in 3 months. I also found out that you can get absolutely nothing done in 3 months. Both are true. Try it.

3.  **Know What You Don't Want** : If there is one critical lesson that I learned through these episodes, it is this. Once you start moving towards your goal there is always a temptation to add 'a little more fat'. 'Its okay you can handle it', you tell yourself. But the painful truth is, you can't handle it and that little fat drifts you and you eventually fall off the mark.

4.  **Define Metrics To Measure** : Metrics define your steps. Steps to write 10 quality articles a month is not the same as getting a click-rate of 2\. Again both are related but they will take you in different directions in terms of efforts.

5.  **Measure Your Progress** : Measure against your metrics. There is always a temptation to define a metric on quality and then to measure against metric on popularity. I know it's a difficult temptation to resist but it should be resisted to keep your energy & enthusiasm focused.

Converting your brilliant idea into a revenue generating stream is hard. If you continue to spin, you are making it harder and harder to realize that revenue stream. Instead, have a clarity in objective and focus in execution.
