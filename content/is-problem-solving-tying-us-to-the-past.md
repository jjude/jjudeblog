---
title="Is Problem Solving Tying Us To The Past?"
slug="is-problem-solving-tying-us-to-the-past"
excerpt="If you want to create a better future for yourself and your company, be a better problem solver."
tags=["problem-solving"]
type="post"
publish_at="14 Feb 11 16:30 IST"
featured_image="https://cdn.olai.in/jjude/past-future.png"
---
The phrase problem solving isn’t as appealing as the word innovation to management consultants. The word innovation evokes optimism about a better future, whereas problem solving usually means ‘cleaning a mess created by someone else’. Further, problem solving involves investigating about the triggers (persons, events) that lead to the current mess, which means going back to the past.

Who wants to go back to past, especially if it is a troublesome one? Shouldn’t we be progressing forward? Isn’t that what we aspire? So why should we learn problem solving at all?

![Where do you want to go today?](https://cdn.olai.in/jjude/past-future.png)

Problem solving techniques are applied to wide variety of situations. Contrary to what the word signify, problem solving isn't only about the past. The techniques are also about creating the future. In the context of the present discussion (Is problem solving tying us to the past?), I'm categorizing the spectrum of problem solving into just three:

World Bank banning Satyam, one of the premier Indian IT company, should have been a **wake-up call** to investors and regulators alike. A preliminary investigation followed by remedial measures could have saved lot of heartaches that followed. Such wake-up signals abound in both personal and professional lives. When we pay attention to early signs of such impending problems and resolve them, we avoid subsequent stalemates.

When these early signs are ignored, we end up with** if-not-solved-now-there-is-no-future** problems. Anyone remember 'Sun Microsystems' or 'Palm'? With their breakthrough technologies, they changed the world. But then, when troubles started to appear, there was just management mumbo-jumbo; there was no resolution of the problem at hand. What happened? It lead to the slow death of those pioneers.

Then there is a common challenge that most business professionals face **- bridging the gap** between where they are and the desired future. The challenge isn't a crisis to be solved but to create a future to be relevant in the market. Companies like GE continue to reinvent themselves in the changing times and be a respectable company even today.

Problems are an integral part of business operations. Not all problems are about cleaning the past. Declining sales, blocked cash-flow, fighting competition are all problems directly tied to the future. If these problems are not solved then there is no future. So if you want to create a better future for yourself and your company, be a better problem solver.

_This post is part of ‘[Be a Problem Solver](/be-a-problem-solver/)‘ series._
