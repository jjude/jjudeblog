---
title="Dilemma"
slug="dilemma"
excerpt="Three factors that contribute to success of a project."
tags=[""]
type="post"
publish_at="10 May 08 16:30 IST"
featured_image="https://cdn.olai.in/jjude/dilemma-gen.jpg"
---

It happened a little more than a year ago. Yet it looks so relevant today.

The objective of the engagement was to develop a comprehensive project management tool for the organization. Ironically, none of the project management processes were followed in developing the tool. Every mistake that could be committed in a project was committed - scope creep, loosing focus, failure to plan, failure to execute; you name it and it was there. As a result, the project was getting nowhere and not surprisingly top management was furious.

Having spent close to 30 man year's of effort, scrapping the project became out-of-question. Something (anything) had to be done to bring life back into project so as to avoid embarrassment. Few project managers had quit and due to its state, no one else was willing to pitch in. Though I'm from ERP world and the tool was developed in J2EE, I offered to try my chance. ([Sastri](http://www.linkedin.com/in/sastry) who taught me project management on a practical manner, always encouraged to take risks; In fact I witnessed him completing a project that was abandoned by other project managers)

In six months, the project was back on track; stable; today it is the default project management tool in the company. I didn't do anything extra-ordinary - I just went by the book.

##### People, People, People

> You need two strong oxen to plough a field; not hundred chickens
First thing I did was to re-organize the team. Lots of folks were not enthusiastic to work on the project anymore. I didn't blame them. I called up a team meeting and said that, 'I'll bring stability for the project and we'll be a success; however if anyone want to leave I am okay with it. But leave now'. Only few people stayed back. In terms of skills, not all of them were strong oxen; but in terms of commitment, they were the best.

It was not just the team that was behind the success; I met up with the senior management and solicited their support and co-operation. I got it - my supervisor; head of department, and all other senior managers were so supportive of my efforts.

If I were to contribute the success to one factor, it would be **people**.

##### Lean Process

Once I got the right people, I went on to define the process that we will follow - from gathering requirements to release. Considering the fire-fighting situation, I kept it as lean as possible.

As we progressed, we reviewed the processes and tailored to suit our situation.

Having a lean process helped as it was easy for the team to understand and to follow.

##### Determined Execution

> What sets a leader apart is not those brilliant ideas; but the realization of those ideas.
Once I got the people that I can depend on and a process framework, I went about getting the project on track. Every day I had a stand-up status meeting with the team; I met with the stakeholders regularly; Luckily I got few enthusiastic champions for the tool in various departments; more importantly I was firm on the timelines and what got into each release. In few months, the trust was built and then it was easy to build on top of that trust.

Six months were insane. Very insane. There were days we were in the office for 36 hours(!). Unbelievable; but true. (I stayed that 36 hours too; as I believe I shouldn't ask the team to do something, if I myself am unwilling to do so).

Once we passed the insane days, it was smooth and in fact superb. The team was congratulated in every relevant forum/meeting. We grew in our confidence as well. It all worked out pretty fine.

##### The Dilemma

I was back to ERP projects and it all seemed settled. Until now.

Apparently there are enough projects that go through a disastrous phase. There is another critical project in such a phase (why do critical projects slips through the gaps in project management). With the past experience, it is falling into my lap.

Honestly speaking, I miss those days - the tension, the stress and everything associated with stabilizing the project. On one hand, I'm not so sure if I could go through another period of insanity. On the another hand, the very thought of taking risk excites me.

I am in a dilemma.

