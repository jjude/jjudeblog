---
title="Distilled wisdom"
slug="distilled-wisdom"
excerpt="A concise summary of what I have learned"
tags=["insights","wins"]
type="post"
publish_at="27 Oct 22 14:42 IST"
featured_image="https://cdn.olai.in/jjude/distilled-wisdom-gen.jpg"
---
How can we learn a big idea so it sticks in our memory? Summarize big ideas into one sentence. 

So here are my one sentences.

- **Marriage**: Witness life together
- **Personal Finance**: Earn, grow, protect money
- **Parenting**: Kids mimic you, they don’t obey you
- **Risk**: What is left after you have thought of everything that can go wrong
- **Project Management**: What needs to be done, by when, by whom
- **People Management**: Incentivize the behavior you want; punish the opposite
- **Running a business**: Never run out of money
- **Stock Market Investing**: Own profitable companies that pay you dividends
- **Leadership**: Lead them to a place they never thought they would go
- **Healthy Food**: Less steps and days from farm to plate
- **Exercise**: Weights + cardio + rest
- **Weight Loss**: Healthy diet + exercise 
- **Life**: Living your calling

## Continue Reading

* [You need to know very little to get ahead in life](/little-to-get-ahead/)
* [Money Can Buy Happiness](/money-buys-happiness/)
* [Life is more about depth than length](/depth-of-life/)
