---
title="Startups And Security"
slug="startups-and-security"
excerpt="An interview with Sastry, a serial entrepreneur about balancing speed in bringing a product to market and sufficiently securing it."
tags=["coach","interview","security"]
type="post"
publish_at="02 Nov 13 16:30 IST"
featured_image="https://cdn.olai.in/jjude/startups-and-security-gen.jpg"
---

Developing a secure product takes time. This conflicts with the goal of a startup — bring the product (or version) as quickly as possible into market.

I was looking for an answer to this conundrum. Thankfully, [Sastry](https://twitter.com/sastrytumuluri) was willing to answer it. He has been a serial entrepreneur himself; he designed India's premier e-governance project [MCA21](http://deity.gov.in/content/mca21), architecture of which has become a standard for most e-governance projects in India; he is also founder member of Chandigarh [chapter](https://www.owasp.org/index.php/Chandigarh) of OWASP. Hear what he has to say on startups and security.

### Q) Startups are under pressure to bring their products to market quickly. They don't know if their product would succeed. Given this uncertainty, how can startups release secure products quickly?

Let's compare building software with building homes. For a "trainee" mason, just getting the wall to stand up straight could be a big goal. For the next-level mason, doing it as fast as he can, with the latest brick/no-brick tech and using latest mixers could be fun and challenge. However, at some stage  someone —  usually the builder (or in our case a product manager / project manager) must worry about the people who'll stay in the home. If they could get away with it, they would simply build it just-enough to get their payment and vanish. I am sure you can see the parallels between this and building software.

So as software professionals (including when being in startups) have a choice. When at early stages of maturity, the challenge is only to build it and quickly. Those more mature will determine how much security is needed and build it into the software (or system as a whole).

**In any product, at first we want to "wow" the client with the core differentiators and our innovation. By all means we should do that.** However, before delivering the finished product, it is best that we give them a product that won't let them down in all respects that matter — including security. The cost of letting them down will most likely boomerang and eat up any "early-stage profit" made by taking the easy way out.

### Q) Startups prefer language-frameworks, like Rails and Django, to code their products quickly. What role do these frameworks play in securing apps? What parameters should startups consider in evaluating a framework from security perspective?

Gone are the days when the job of frameworks is merely to accelerate the development schedule. It is now possible to choose frameworks that have security built into them. e.g., .NET, CakePHP, Apache Shiro.

I'm pretty sure that frameworks that are weak in security will eventually remain only in internet archives; not in active products. So we need to be smart and choose those frameworks that meet **all our criteria — including security**. We did just that when we developed the Haryana OneState Platform. We chose [Yii](http://www.yiiframework.com) because it not only qualified to do all that we wanted, but it also helped us build more secure code with lesser effort.

That said, don't forget that using a good framework is not a substitute for good AppSec knowledge and practice. No framework can cover all the holes; and none can cover exploitation of business logic holes. So a security-savvy developer is happy to leverage what the frameworks offer, but is ever circumspect.

### Q) Bring your own device (BYOD) is in vogue and it brings with it it's own security challenges. What should firms do to embrace BYOD but secure their systems?

Many prescriptions exist. None are mature enough. At the risk of sounding like a paper-consultant, I'd suggest a "risk-based approach". In practical terms this means:

*   If all you want is to allow non-corporate (personally owned, unmanaged) smartphones &amp; tablets into your office, put in place a WiFi authentication (RADIUS is good) mechanism that connects all unregistered (but with proper password security) devices onto a guest VLAN that only gives direct internet access; nothing else (not even access to neighbours on the same switch). Most modern switches support this; some come with built-in RADIUS support too, I think). That puts only bandwidth hogging as the main risk, that could be handled at the UTM/firewall through bandwidth-shaping.
*   If corporate mobile apps are business critical, then there is no way around getting an MDM (Mobile Device Management) solution implemented. Usually means high-priced products and consultants, but we are already acknowledging that these are business critical, so...

*   In general, knowledge of what to do is the barrier. Some can get on the net / attend trainings and learn. Others hire top consultants, spend big bucks and still not learn. Hopefully, most are somewhere in between.

In the end, everything boils down to **some form of risk-mitigation (within budgets/resources) + residual risk-acceptance**.  Whether done with deliberation, plain living-in-denial, or a philosophical shrug.

### Q) You designed the successful Indian e-gov project -MCA21\. Now you are an IT advisor to Haryana govt. What are the specific security threats for e-gov systems? What roles can startups play in this segment?

This is a trick question! I'll try not give a long list of threats just to make sure no one can later say I missed something. :-)

There are different kinds of eGov projects. Let's focus on Citizen Services Delivery systems (like [MCA21](http://deity.gov.in/content/mca21), [eDistrict](http://deity.gov.in/content/e-district), [CSC](http://deity.gov.in/content/common-services-centers), etc.). There is a lot of PII (Personally Identifiable Information) involved. So data loss / theft is clearly a concern. Both online criminals &amp; hacktivists are potential threat actors in this category.

So is Denial of Service. Our citizens are quite forgiving, but extended downtimes tend to hurt the citizen service goals, so DoS risks cannot be ignored. Hacktivists are the main threat actors with the motivation to do this.

All other risks are secondary -- including interventions by nation-state actors; distractions like website defacing by cross-border hacking groups, etc., as they don't cause lasting harm on the main objectives of the eGov systems.

So what roles can startups play here? Most project managers in Govt tend to carpet-bomb (as their own personal reputation risk mitigation) and try to mitigate all risks, throwing as many products &amp; services into the mix as the budgets allow. This is a problem and hence an opportunity.

Startups can help give the right focus and give a risk-weighted treatment. If I were to start a company now, I would offer **software development, deployment and management services** that would integrate and address well, the above-mentioned security concerns.

### Q) How can we, individuals, safe-guard ourselves, as we move most of our transactions online?

**Two words: Awareness. Vigilance**.
We must educate ourselves on the risks. To quote someone else: "We wouldn't send our children out to cross a busy road without some education and training, would we? Why is the Global Information Highway any different?"

We are all children on the Internet. Let us equip ourselves with the necessary skills. Having equipped ourselves with the necessary knowledge &amp; skills, let us then exercise it through constant vigilance.

A few specific thoughts:

*   Don't download software from untrusted sources. Free or paid, doesn't matter.
*   Remember that even trusted sources can sometimes be compromised. Watering hole attacks (e.g., recent php.net and "buffer" compromises come to mind). So keep an eye/ear out for news. We do watch TV news, don't we? The same way let's pay attention to infosec news.
*   Don't open attachments without clear need. Even then, only after checking and double-checking the source. If you got a note from RBI, doesn't mean you should open it! Why would RBI write to a regular citizen like you? Think, check the source, etc... **Vigilance!**
*   Patch your system regularly, including your antivirus software.
*   If at all possible, try to use only trusted systems for banking &amp; e-commerce transactions. Avoid mobile platforms for these -- unless you've taken adequate precautions and are reasonably sure that your mobile is safe enough.
*   Don't answer personal questions, without ascertaining first, who is asking the questions. Is it really our bank's customer service agent? Did you dial the number or did they call? Even so, never give out passwords &amp; PINs.
*   Demand that your internet service provider give you a clean pipe (as of now, this is a pipe dream, but we must start asking; or we may never get there).

Trust this gives a starting point for folks thinking in this direction.

Thank you [Sas3](https://twitter.com/sastrytumuluri) for your views. It definitely, takes lot of practice to mature into a secure coder from a 'trainee mason', but that is the need of the hour.

If you enjoyed [Sas3's](https://twitter.com/sastrytumuluri) views, follow him on [twitter](https://twitter.com/sastrytumuluri).
## Continue Reading
- ['Talk With Me Security' With Shikhil Sharma](https://jjude.com/shikhils/)
- [Cyber Security for Women](https://jjude.com/cyber-security-for-women/)
- ['Building An Enterprise Data Strategy' with Ganesan Ramaswamy](https://jjude.com/ganesanr/)
