---
title=" Are you a monk, lotus, or salt in handling problems?"
slug="monk-lotus-salt"
excerpt="Most of us are a mix of the three."
tags=["wins","coach"]
type="post"
publish_at="11 Jul 22 10:55 IST"
featured_image="https://cdn.olai.in/jjude/monk-lotus-salt-gen.jpg"
---


Problems are inevitable in a society. When problems arise, we can either become a monk, a lotus, or a salt.

## Monk

**Escapes the problem**. Instead of blaming anyone, they just move on and try to find a suitable living situation. If there are no opportunities in India, they move to Canada. They move to a city if they have problems with their family in a town. They move to another project or company if they cannot work well with their teammates. All of us need our own space in life, and we find ways to achieve it.

## Lotus

**Signals virtue**. They don't leave the troubled pond, but they also don't clean it up. They are just signaling to others that they are better than everyone else in the dirty pond. That's what most of us do on LinkedIn. All of us are subtly tweeting, "look at me, I'm better." I am doing the same with this post.

## Salt

**Change makers**. They bring about change, even if it is tiny. Possibly only to a small number of people. A pinch of salt can't change the world. They can, however, change the world that knows them. Through their interactions, they can influence the world around them. There is no spotlight shining on them, and they are ok with it.

Most of us are a mix of the three, sometimes at the same time. 

## Continue Reading

* [If You Are Not Part Of The Solution, You Are A Problem](/be-a-solution/)
* [Don'T Torture Context To Fit Your Solution](/dont-torture-context-to-fit-your-solution/)
* [If You Don'T Blow Your Trumpet, How Can You Expect Music?](/blow-your-trumpet/)
