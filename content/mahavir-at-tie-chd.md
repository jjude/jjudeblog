---
title="Surviving hard times"
slug="mahavir-at-tie-chd"
excerpt="Notes from interactive session at TiE, Chandigarh with Mahavir Sharma, Serial Entrepreneur & Trustee, TiE, Global"
tags=["coach","startup","insights"]
type="post"
publish_at="01 May 15 00:39 IST"
featured_image="https://cdn.olai.in/jjude/2015-04-mahavir.jpg"
---
Yesterday (29th April), Mahavir Sharma, ex-trustee of global TiE board was in Chandigarh for an interactive session with members of Chandigarh chapter of TiE. The session was about surviving hard times — as a startup as well as an established company. As a successful serial entrepreneur in many business verticals, Mahavir was fluent in answering wide range of questions.

![Mahavir](https://cdn.olai.in/jjude/2015-04-mahavir.jpg)

Here are the notes that I took from the session (I took notes only that were relevant to me at my stage — Product/Market Fit stage):

1. **Secure yourself and your idea first**, if you have to survive for the long term. If that means getting a patent, get a patent; if that means raising enough funds, raise funds. Times are going to be tough, so put on whatever you need to survive the times. What do you do when earthquake happens? You take shelter under structures that can protect you so that those shakes don't bury you under the debris.

2. You should **invest heavily on R & D**, especially if you are in technology space. Velocity of change is fast in technology space that if you don't have strong R & D to bring new products or to improve your product line, you will be outrun by your competitor. You need a strong backend if you need to move fast on the field.

3. Don't get tied to your idea. **Don't be egoistic**. Acknowledge if your idea failed and move on. Entrepreneurs should be willing to take 180º turns if required.

4. Entrepreneurs **work within the system**. They don't complain about corruption, retrospective taxes or other regulatory non-sense. These conditions are same for all entrepreneurs. They learn to navigate within any system to establish their business and thrive.

5. **Ideas fail. People don't.** Invest in people.

6. When do you pivot? Like everything in entrepreneurship, it is a gut feeling, although it is possible to lay a framework. Look around you in your domain. Are you the only one who suffer or every one is suffering? Can that signal you a direction?

7. Someone asked about the mistakes he made. He said, if he has to start over he may make the same mistakes again, because he is where he is because of all that he did. He quoted actress Sunny Leon: As far as my career goes I don't regret anything that I have done. I believe that **everything that I have done has led me to today**. Lesson? Do what you think is right at that moment. It may be right or wrong at a later time.

8. **Don't get carried away by news of VC funding** or other such news. Only you can decide if you have to continue or change course. If you have invested in a strong differentiator (strong backend, new products in pipeline), you should continue. You should also continue if your business model is superior, though it makes loses at present.

It was indeed an interactive session. Everyone, from members of the executive board of TiE, Chandigarh to idea-stage entrepreneurs posed questions to him. Mahavir answered them with anecdotes from his many years of experience making the session interesting and beneficial for founders.
