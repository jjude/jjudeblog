---
title="Tribute to my teachers"
slug="tribute-to-teachers"
excerpt="Beyond the boundary of family, teachers impart values and a strong sense to live life in a worthy manner. I remember those who taught me, not just in school but in life."
tags=[""]
type="post"
publish_at="10 Sep 05 16:30 IST"
featured_image="https://cdn.olai.in/jjude/tribute-to-teachers-gen.jpg"
---

Beyond the boundary of one's family, it is teachers who impart values and a strong sense to live life in a worthy manner. When I pondered over those who impacted my life, my thoughts rolled back to Ms. Vijayalakshmi, my 3rd grade teacher. She was the right person for shaping the future of those little kids, never getting angry at the naughtiness, yet being strict. My memory of her is one of a gentle, compassionate and patient one. Almost every time, I visit my hometown, I make it a point to visit her.

I've been blessed with many such remarkable teachers. Yet another was Ms. Esther, our school headmistress, whom we fondly called 'periya teacher'. I'm sure my classmates would recall her favorite advice, 'Eat lots of onions, you'll do better in Maths'. Probably that says how I scored around 90% in Maths every single time. As naughty as we were, we also found another use for onions - keeping it under the armpit increased the body temperature thereby we would say, 'Ma, I've fever, no school today!' (I can't find medical reasons for both of these). Its pity that she died of cancer and I couldn't meet her in her last days.

Going by the trend, my parents transferred me to an English medium at 6th grade. I studied hard and came first in mid-term. For reasons beyond my reasoning, then Tamil master planned to change it. It was then that Mr. Cecil Raj (and many class mates) stood by me to imbibe strongly, 'Boy, bad things will happen, but you'll also find people to stand by you and get you through' and he saw to it that I retained the first rank. That has been a valuable lesson all along. Thank you sir.

Then there was Mr. Alexander, who took 8th & 9th grade science. He did pose a personality true to his name. But I remember him for a different reason. When few of us were struggling with spelling for 'together', he gave us an easy way to remember: 'to get her'. As you can see, I never forgot the spelling (Though I haven't got her). Later this kind of learning would come handy, to remember complex properties of salts & other chemicals in our Chemistry lessons.

Those days, we'd to memorize lots of Tamil poetry, especially religious, which we all loathed to do. But Mr. Murugan's classes were always special. He taught us to question the status quo - the well-acknowledged beliefs, more so of religious ones. On such an influence, I grew up to be a maverick, more often gaining notoriety.

I strongly believe life is a journey and learning doesn't stop at school and college. I learned the value of hard work from Dr. M.R. Girinath, my boss at the second job and another valuable lesson, 'be-early-to-work', from Dr. Vijayashankar, who, despite his old age and position, would be the first at work. When I joined a 'real' software firm after three years of sojourn, I met Mr. Sastry, who still signs as --Sas3--. It is unfortunate that I didn't work with him for a long time, but in the short duration he made a profound impact on me. He opened the world of management to me, mainly the challenge and importance of people management. He showed in practice, the measures a manager should take to keep his team motivated and also the results that those measures pay off. I attribute to him, any success I have with people.

When I stretch this concept of learning a bit further, I am highly indebted to Steven Covey, for his excellent book, 'The 7 habits of highly effective people'. To be honest, I've not practiced all the 7 habits to the full measure; however, I've reaped greater results even with whatever I've practiced.

I've always been grateful for such individuals. But, until I sat down to write this article, I didn't realize the amount of them in my life. My journey has been enriched with such teachers. I hope I'll continue to learn from such mentors and I myself will be one to someone along the way.

