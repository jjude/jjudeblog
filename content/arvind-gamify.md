---
title="Arvindh Sundar on 'Gamification of marketing for small businesses'"
slug="arvind-gamify"
excerpt="Heroes, monsters, and rewards when applied to marketing..."
tags=["gwradio","biz"]
type="post"
publish_at="23 Aug 23 10:46 IST"
featured_image="https://cdn.olai.in/jjude/arvind-yt.jpg"
---
When we fuse two domains we get amazing insights. Joining me today is Arvindh, who has done exactly that. We are going to talk about gamification and how it can be used to engage your customers, creating lasting connections, and elevate your brand to a whole new level.

<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless src="https://share.transistor.fm/e/2f0cf0cb"></iframe>

## What you'll hear

- How Arvindh started with gamification
- How to use gamification for marketing
- How IT services companies can use gamification of marketing
- How coaches can use gamification of marketing
- Mistakes people make in applying this concept
- Tools for gamification of marketing
- How to measure effectiveness of the framework
- Kindest thing anyone has done for you
- Who manifested leadership quality
- Definition of living a good life

## Edited Transcript
### How did you get into this intersection of games and business? What is your backstory?
I love telling the story, Joseph, and thank you for this opportunity. It all began in 2006, right after I graduated from college. Following the common path for many Indian boys, I found myself working at an IT company.

However, my time there was exceptionally brief, lasting only about a year. I was an ASP.NET developer during that period. Subsequently, I made the decision to leave and transition into roles involving sales, marketing, and copywriting – a significant departure from my initial path. Jumping ahead five years, I attended a five-year reunion event for individuals who had joined the same company as I did.

I was feeling quite confident and proud at the time. I was engaged in the advertising field, happily married, owned a Royal Enfield Standard 350 motorcycle, and had 40,000 rupees saved up. I felt like I was on top of the world. However, upon meeting my peers who had remained in IT for those five years, my perception shifted dramatically.

One was about to embark on a vacation in Venice, another had purchased a Swift Desire car, and a third was in the process of building a house. Suddenly, I was taken aback and questioned my choices. Should I have stayed in the IT sector? This reunion left me feeling more than slightly demoralized.

But I resolved not to let this negativity hold me back. I made up my mind to chart my own course. This decision led me to enroll in NIIT to learn about Linux and related subjects. I embraced a new routine, waking up at 5 AM, engaging in exercise, and dedicating time to reading. Initially, this plan worked wonders, but it didn't last.

After around 10 days, my old habits resurfaced, and I began neglecting the positive changes I had introduced. Slowly, I started skipping my NIIT sessions and disregarding my new routine. Unfortunately, this downward spiral extended beyond my initial state of demotivation. I began to berate myself and entered a deeply negative cycle.

In search of an escape, I turned to the one realm where I consistently found success – video games. Throughout my life, being overweight and struggling with self-esteem, I felt a sense of belonging in the worlds of books and video games, particularly fantasy literature like "The Lord of the Rings" and "The Wheel of Time" series.

In this fragile state, I poured myself into video games to an unhealthy extent. Gradually, I began to neglect work, my wife, and even personal hygiene. I would play computer games for 12 to 14 hours a day, going several days without bathing. I vividly recall a pivotal moment one night when I felt hungry and reached for a snack.

As I opened the fridge, my standards for an acceptable snack were initially high, but they progressively lowered with each failed attempt. Eventually, I resorted to eating dry coconut powder – a situation that highlighted the depths of my predicament. Sitting on the couch, consuming this meager snack, I had a moment of clarity.

I questioned my actions: Why was I squandering my time like this? I had responsibilities, a wife in the next room, and work obligations the following day. It was then that an intriguing thought surfaced – what if my life could resemble the video games I so enjoyed? This single question ignited a transformative journey.

I resolved to convert my life into a game. Initially, I faced considerable challenges, as the mindset, skill set, and tools required to gamify life were distinct from those needed for playing games. Thus began a learning journey spanning over a decade. I delved into topics such as game design, gamification, motivation, psychology, human behavior, system design, and user experience.

Gradually, I synthesized these insights into a cohesive framework called the "Game Plan." This approach prioritizes the player and centers around three foundational principles that I've embraced over the past decade-plus. First, I discovered that any aspect of life can be transformed into an adventure, akin to an adventure game.

Second, I recognized the importance of placing the player's needs at the forefront. This entails designing experiences not for oneself, but for the individual engaging with the "game" of life. Lastly, I understood that true victory comes from participating in an infinite game – one that remains enjoyable and fulfilling, encouraging ongoing engagement.

Since then, I've applied this approach across various domains, from acquiring new skills and striving for fitness to aiding marketing efforts and assisting tech founders in building more prosperous businesses. This journey has proven immensely rewarding since I initially conceptualized this unique perspective on life.

### How to use gamification for marketing
Before we delve further, let's establish a shared and clear working definition of gamification. The concept of gamification has evolved over time, having been present for a substantial duration. The definition I adhere to is as follows: Gamification entails integrating game elements into contexts that are not inherently related to games. Is this understanding accurate? Essentially, it involves taking elements from the realm of games and incorporating them into unexpected areas. When we apply this concept to marketing and, by extension, the entirety of business, the approach I'm discussing, known as the game plan, functions as follows: It allows you to reimagine your business as a game. This concept aligns with the first rule.

Now, moving on to the second rule: Prioritize the player. Within this adventure structure, let me elaborate on how this works, particularly within the framework I've developed. At the outset of the story, a hero takes center stage. The gender of the hero, whether male or female, is of no consequence. This hero is besieged by various challenges, represented metaphorically as monsters. These "monsters" symbolize the obstacles troubling the hero, causing disruption and turmoil in their world. The hero's ultimate aspiration is to attain a state of paradise – an ideal existence devoid of these problems.

One day, the hero reaches a breaking point, realizing that the burdens have become insurmountable. At this juncture, they embark on a transformative journey to effect change in their life. This pivotal decision marks the call to adventure. During this stage, the hero encounters a guide. In the context of business, you, the business owner, assume the role of the guide. As the guide, you possess distinct advantages. Firstly, you possess a Unique Selling Proposition (USP) or a "superpower." This unique trait sets you apart as the sole solution for the hero. It distinguishes you from others who could aid the hero on their journey to paradise.

Furthermore, an adept guide holds a second asset – the "big bad." This abstract concept represents a formidable adversary that you, as the guide, have dedicated your efforts to conquer. In essence, it's the ultimate antagonist, perhaps equivalent to the source of all negativity. In my case, working with solo technical founders in marketing, my "big bad" is the misconception that marketing is complex. I've committed myself to dispelling this false belief.

It's crucial to note that, as the guide, your role revolves around aiding the hero, not assuming the mantle of the hero yourself. You equip the hero with specialized "weapons" to combat each monster they face. These weapons are tailored solutions to their challenges. Yet, your guidance goes beyond this. You furnish the hero with a series of quests, akin to a to-do list. These quests outline the steps required to traverse from their tumultuous world to the paradise they seek. You convey that they possess the knowledge and resources needed to execute these tasks.

As the hero embarks on these quests, they engage in battles against their metaphorical monsters, advancing in their journey and earning rewards, referred to as "loot." Loot encompasses external rewards, analogous to gold coins or gems in a story. Additionally, the hero "levels up," signifying their increased proficiency. In a business context, loot might encompass heightened leads, customers, profitability, and enhanced brand recognition. Leveling up equates to an internal transformation, a shift in mindset or capability.

Yet, the journey isn't without its trials. The hero faces an abyss, a crisis of faith, akin to my personal struggle after ten days of pursuing a new routine. At this juncture, you, as the guide, intervene once more, offering insights and revelations. These insights rekindle the hero's determination, propelling them towards paradise.

This framework derives inspiration from the influential work of Dr. Joseph Campbell, who studied myths across cultures and epochs. Furthermore, it draws from the dynamics of video games, incorporating elements such as loot, leveling up, weapons, and monsters. It's worth noting that this framework differs from the storytelling approach advocated by Donald Miller in his book "Story Brand." While stories are static, video games offer interaction and dynamism. In this framework, agency is paramount.

Incorporating this framework involves stepping into the role of the guide and constructing the entire experience around your customer. Continuously applying this concept constitutes playing the infinite game – an ongoing mission to guide your customer to paradise, thereby fostering a profitable business.

### How IT services companies can use gamification of marketing
Allow me to clarify the assumptions forming the foundation of our approach. Firstly, let's consider the guide we are developing the game plan for. The guide in this context is an IT services company. Their intended customer is an expert in a specific domain with a vision they aim to realize. With this context established, let's proceed.

We begin by defining the guide, which is the IT services company. However, there are two crucial aspects we must address upfront: the superpower and the big bad. This becomes particularly significant when operating in a commoditized business landscape, where differentiation is key. In such instances, the big bad serves as a powerful differentiator.

In essence, if you find yourself in a space where your IT services company is similar to others, your story becomes pivotal. Your unique perspective, driving force, or the abstract challenge you've chosen to combat sets you apart. This differentiation can revolve around aspects like timely delivery, and it's something you must determine. Various exercises can assist in this endeavor.

The next step involves understanding the hero, who is the subject matter expert in this scenario. Although we are broadly categorizing them as such, it's vital to explore deeper. We need to identify what compels them to distance themselves from their current situation and what pulls them towards their desired goal. There are several pertinent questions to address in this context.

Subsequently, we focus on the monsters, which hold utmost importance. For a subject matter expert, potential monsters could be issues such as having a brilliant idea but lacking the knowledge to develop an app. Another challenge might be navigating a perplexing tech stack, replete with unfamiliar terms like Python, React, and Next.js.

For each of these monsters, we delve into their implications. For instance, if the subject matter expert lacks the expertise to bring their idea to life, it implies an extensive research process, soliciting bids from multiple IT services companies without clarity, and selecting the right option becomes daunting. By listing the first and second-order implications of each problem, we outline the "broken world."

Cascading from this broken world, we can envision its antithesis – paradise. Inverting the challenges unveils the desired outcome: the subject matter expert's app successfully launched to the market, robust, scalable, and devoid of any negative impact on their status. This construct allows us to map the progression from the guide to the hero, monsters, broken world, and ultimately paradise.

With these elements elucidated, the guide's role shifts to equipping the hero with tools or "weapons." For instance, if the hero is unfamiliar with starting the process, you could swiftly create an informative product such as "10 Criteria for Selecting an IT Services Consultant," which doubles as a lead magnet. Alternatively, a specialized checklist focused on launching an MVP within the automotive domain, targeting the specific audience, can be highly valuable.

These tools might manifest as PDFs, video courses, or even practical tools like a certificate generator. With these weapons in place, the guide, in this case, the IT services company, can articulate the steps for the hero to traverse from their current state to their envisioned success. These steps compile into a to-do list guiding them on their journey.

Once this to-do list is compiled, the guide proceeds to outline the benefits or "loot" the hero will attain by following these steps. Additionally, the "level up" signifies internal growth and transformation. This approach acknowledges that each entry is an assumption and emphasizes the importance of validating these assumptions.

My recommended validation method involves engaging potential customers in conversations to gauge the accuracy of your assumptions related to monsters, the broken world, and paradise. These discussions offer insights that facilitate iteration and refinement until the insights gleaned outweigh the noise. Once this process solidifies the framework, you can confidently initiate the building phase.

To put it succinctly, our approach involves defining the guide, understanding the hero's challenges and desires, addressing the monsters, outlining the broken world and paradise, equipping the hero with tools, and ensuring assumptions are validated through customer conversations before embarking on the execution phase.

### How coaches can use gamification of marketing

The approach you've described aligns closely with the process we've been discussing. Notably, you possess a well-defined superpower—your "gravitas approach"—which serves as a pivotal aspect of your differentiation strategy. While it's not imperative to immediately vocalize your "big bad," it's a concept that can take shape as you progress. As you've aptly advised your coaching clients, taking action and embarking on the journey can unveil insights that might not be evident at the outset.

In your specific case, the guide being yourself, the next step involves identifying the hero, which you've already accomplished. Following this, delving into the monsters is pivotal. Given your wealth of client interactions, revisiting past recordings can provide a plethora of problems that you can compile into a comprehensive list. These problems might manifest as challenges like feelings of unluckiness or a stifling fixed mindset, impeding personal growth.

Moving forward, extrapolating the implications of these monsters is crucial. For instance, an unhappy life, stagnated career, or the perception of being cursed could all be subsequent effects. These challenges paint the picture of a "broken world" that the hero seeks to escape. Conversely, envisioning paradise entails a world where these obstacles cease to exist.

Given your level of expertise, an advanced strategy you could employ involves structuring your communication to align with aspirational individuals seeking to attain your current status. This approach resonates with those aiming to reach your position and can significantly engage your audience.

With these elements solidified, you can capitalize on the concept of "infinite content." This ingenious approach involves using your developed framework to generate a wealth of valuable content. Armed with your understanding of the hero, monsters, broken world, paradise, and the tools you'll provide, you can create diverse content forms, ranging from info products to workshops, catering to your target audience's needs.

Additionally, you can highlight the rewards awaiting those who follow your guidance, including promotions, career success, and even TEDx speaking opportunities. To simplify the process, a structured template resembling a business model canvas could be employed.

A further strategy to facilitate content creation entails employing prompts based on the "who, what, when, where, why, and how" framework. For example, if one topic revolves around achieving promotions, consider the perspective of "who" is most likely to be promoted. This prompts the generation of content focused on factors like a growth mindset, embracing learning, and a willingness to take risks. Ultimately, each piece of content not only engages your audience but also naturally integrates opportunities for you to introduce your services and offerings.

Given the structure of your game plan, you'll have around 14 different prompts, each with several related questions. This translates to a substantial pool of content ideas—over 420 prompts—that you can continually draw upon. By capitalizing on this approach, you're empowered to create content in a fluid, productive manner.

In essence, the process revolves around defining your superpower and hero, identifying monsters and implications, envisioning paradise, equipping the hero, and employing an expansive content creation approach that maintains audience engagement while seamlessly integrating your services.

### Mistakes people make in applying this concept

The question you've raised is indeed quite insightful, as the answer depends on the distinct types of individuals you encounter in this context—those who are overwhelmed or confused by marketing, and those who have already experienced challenges or setbacks with marketing endeavors.

When presenting this approach as a tool for clarity, it often resonates differently depending on the background of your audience. Those who find themselves perplexed by marketing, and individuals who have encountered marketing failures, perceive the process as an instrument to overcome their struggles. They might acknowledge its significance, but those who have a working sales funnel or existing strategies might mistakenly believe that they have it all figured out. In such cases, a candid reality check is in order.

In the realm of marketing, the phrase "you can't polish a turd" holds significance. This implies that regardless of how impressive your marketing might be, it cannot compensate for a subpar product or offer. If the foundation isn't strong, marketing merely amplifies the flaws. This principle underscores the importance of creating a solid product or service, understanding the target audience, and presenting yourself effectively.

A cardinal principle to remember is that it's much easier to craft something that aligns with existing customer desires than to mold customers into wanting what you offer. This underscores the significance of identifying genuine market demand and catering to it.

For individuals who already possess some degree of marketing systems, your role is to guide them through the process in a structured manner. This involves revealing gaps in their current approach and demonstrating how these gaps can be rectified. The key lies in pinpointing the areas that are amiss or in need of enhancement. Importantly, it's not about discarding everything they've built, but rather pinpointing weak points and subsequently rectifying them. Knowledge of where things are off-course is essential for effective course correction.

An intriguing facet you've highlighted involves the transition from comprehending a concept to its execution. Many grasp the theoretical aspect, but the practical implementation often proves to be quite distinct. This transition is marked by understanding that taking action requires consistent effort, sometimes in repetitive ways. For individuals who struggle with this aspect due to challenges like ADHD, the workaround is ingenious—transforming tasks into a gamified approach. By devising multiple approaches to tasks, the process retains a sense of novelty, keeping engagement levels high.

For example, in the realm of networking, a variety of methods can be employed—commenting, sending personalized videos, or even creating mind maps of online presences. Although the overarching goal remains unchanged, the diversity of execution keeps the process engaging and sustainable. By recognizing personal strengths and weaknesses, and subsequently customizing the approach, you're creating a system that suits your unique style.

Another group that often faces challenges involves tech founders who prematurely dive into building without a comprehensive strategy. Here, a crucial lesson to impart is that building is a later step in the journey, not the starting point. The path begins with idea generation, market analysis, identifying gaps, validating concepts, and understanding customer requirements. Only once strong signals of viability emerge can building commence. This perspective might clash with the enthusiasm of tech founders eager to dive into coding. However, emphasizing the step-by-step process's significance is paramount.

In essence, your approach caters to a diverse range of individuals, addressing their distinct needs and challenges. It empowers them to navigate the complex terrain of marketing, bolster their products or services, and ensure that they're creating something that genuinely resonates with their audience.

### Tools for gamification of marketing

I will approach this in two distinct ways, Joseph. Firstly, I'd like to offer a comprehensive array of tools that I personally advocate for. I tend to be quite enthusiastic about books and tools, so let's delve into that. Secondly, I'll provide insights specifically tailored to implementing the framework.

Starting with the broader scope, the tools I wholeheartedly endorse are as follows:

1. **Mind Mapping Software**: I prefer eDrawMind, but if you're on a Mac, I highly recommend checking out MindNode. These software solutions are excellent for efficient mind mapping, enabling swift capture of thoughts and ideas.

2. **Video Communication Tools**: Consider Loom or Komodo Dex. These platforms facilitate asynchronous communication through short videos, allowing effective sharing of information and ideas.

3. **Obsidian**: An indispensable tool in my arsenal. It's free and operates using Markdown files. Obsidian also provides the unique capability to construct a "second brain" for those inclined towards such knowledge organization methods.

4. **ChatGPT**: This tool's value isn't confined to routine tasks like sending emails. I've devised a novel application where I engage in conversations with ChatGPT, effectively turning it into my marketing coach. This dialogue-based interaction aids me in exploring alternative strategies, prompting questions like "Have you tried this? Why not attempt this approach?" It's surprisingly insightful.

Switching gears to tools specifically relevant for implementing the framework:

I've developed a free course accessible on my website, https://arvindhsundar.com/freecourse. This course, now undergoing an upgrade to version 2, has been optimized for clarity and efficiency. Instead of wading through a barrage of around 38 videos, you can gain a comprehensive understanding by viewing just three succinct videos. These videos encapsulate the essence of the framework discussed here.

Furthermore, I've incorporated a Miro board into the course structure. This board serves as an interactive platform where individuals can directly input and organize their information. With the essential framework components at your fingertips, you're well-equipped to move forward.

Ultimately, it's vital to recognize that while tools play a pivotal role, the core lies in the framework itself. The true power emerges when you commit to embracing this framework across a multitude of iterations, rather than fixating on the tools or peripherals. This unwavering adherence holds the key to your journey's success, irrespective of the specific tools employed.

### How to measure effectiveness of the framework
At its core, this framework is geared towards generating revenue. While I've termed it "growth," it can also be applied in the realm of personal development. However, in a business context, its primary aim is to boost your earnings. To initiate the process, you begin with a baseline assessment. This involves determining your current financial standing as a business or even on a specific funnel level. If your audience isn't familiar with the concept, a funnel serves as a mental model to track a customer's journey—from their initial lack of awareness about you to becoming advocates for your offerings.

Upon pinpointing the revenue sources, you then embark on the process. Validation becomes a crucial step, followed by crafting content that aligns with each stage of the funnel, tailored according to the game plan. I assure you that the financial outcomes you'll witness will amount to a minimum increase of 30%. This projection is rather conservative, as evidenced by the results I've achieved for one of my coaching clients.

The real challenge, however, lies in people giving up prematurely before the full impact materializes. I'm well aware of life's unpredictability and the potential for circumstances to shift. Thus, it becomes imperative to provide a suitable environment, perhaps a sandbox or a secure testing ground, to facilitate implementation and experimentation before dismissing the approach.

To illustrate, if I were to work with a marketing team from a company, they could grasp the framework's fundamentals within a mere two days. Yet, the subsequent steps, ranging from two to two hundred, entail unwavering execution without second-guessing the chosen strategy. To phrase it succinctly, you must allow the strategy time to manifest results. This entails putting it to work and maintaining consistency, resisting the urge to doubt its efficacy.

### Rapid fire questions

#### Kindest thing anyone has done for you

My dad told me that he's proud of me. It wasn't easy, but that's the nicest thing that happened. 

#### Who manifested leadership quality
In my most recent role, I served as the Chief Marketing Officer at an EdTech startup based in Bangalore, known as Jigsaw Academy. During my tenure there, I found myself in an unusual scenario. While I had one individual directly reporting to me and two others indirectly, I had the distinct experience of answering to three different leaders: Sushma, Gaurav, and Sarita. What made this situation intriguing was that each of them embodied a markedly unique style of leadership.

Sushma, for instance, became a mentor who imparted valuable lessons on achieving tangible results. I owe her my appreciation for this insight. On another front, Gaurav introduced me to the concept of "fight Madna." This philosophy entails persevering despite the most dire and discouraging circumstances. His guidance underscored the importance of maintaining resilience. And then there was Sarita, whose approach was characterized by a resolute yet polite demeanor. She demonstrated the ability to stand her ground while remaining courteous, and from her, I gained significant insights into interpersonal interactions.

These three individuals collectively served as my bosses and offered me a diverse spectrum of leadership styles and lessons.

#### Definition of living a good life
I'm already living it, to a large extent. I firmly believe that each person has the capability and the right to establish their unique interpretation of Nirvana or ultimate contentment. The concept of happiness varies for you and me due to our individual histories and perspectives on the world.

Once we've achieved this personal understanding, attaining Nirvana isn't solely about its realization. It's about the entire journey itself. This sentiment reminds me of the poem "[ithaka](https://www.poetryfoundation.org/poems/51296/ithaka-56d22eef917ec)," where the reward is found within the voyage. Every single day, I wake up with the aspiration to reach my destination, my version of Nirvana. However, I'm resolved not to allow the journey to become a ceaseless struggle and relentless toil.

My objective is to derive enjoyment throughout this journey. Just prior to recording this podcast, for instance, I was immersed in a gaming session. This context is what I meant when I mentioned that my life is almost in alignment with my vision of Nirvana. There are just a couple more elements I wish to integrate – embarking on bodybuilding and getting a dog into my life – and then I can confidently say I've arrived.

## Resources mentioned
- [Storyworthy](https://geni.us/sotryworthy) by Matthew Dicks
- [Building a story brand](https://geni.us/building-storybrand) by Donald Miller

## Social Media Shares
- [Twitter Thread](https://twitter.com/jjude/status/1693792611440656855)
- [LinkedIn Post](https://www.linkedin.com/posts/jjude_gamification-marketing-gravitaswins-activity-7099555780525211648-AwY-)

## Your feedback counts

Thank you for listening. If you enjoy the podcast, would you please leave a short review on Apple podcast or on YouTube? It takes less than 60 seconds, and it really makes a difference in finding this podcast. And it boosts my spirits. 😀

I would like to hear what resonated the most with you in this episode. Either you can email me or share it on social media and tag me. Thank you for your support.

Have a life of WINS.

_Gravitas WINS Radio is produced by [Joseph Jude](https://jjude.com/). Be sure to subscribe on [Apple Podcast](https://podcasts.apple.com/in/podcast/gravitas-wins-radio/id1562802449), [Spotify](https://open.spotify.com/show/7LOBjAyjBkechMC0xMIj9E),[Google Podcast](https://podcasts.google.com/feed/aHR0cHM6Ly9mZWVkcy50cmFuc2lzdG9yLmZtL2dyYXZpdGFzLXdpbnMtcmFkaW8=),[YouTube](https://www.youtube.com/gravitaswins), or the podcasting app of your choice._

Please check out other episodes of [Gravitas WINS Radio](/gwr-themes/). For easy consumption, they are categorized by themes. If you are a fellow podcaster, you might be interested in [How I Podcast](/how-i-podcast/) or [Tools I Use](/i-use/).

## Connect with Arvindh
- LinkedIn: https://www.linkedin.com/in/arvindhsundar/
- Website: https://arvindhsundar.com/
- Daily marketing newsletter: https://arvindhsundar.com/join/
- Twitter: https://twitter.com/arvindhsundar

## Connect with me
- Twitter: https://twitter.com/jjude
- LinkedIn: https://www.linkedin.com/in/jjude/
- Website: https://jjude.com/
- Newsletter: https://jjude.com/subscribe/
- Youtube: https://youtube.com/gravitaswins
- Email: podcast@jjude.com
- Executive Coaching Program: https://gravitaswins.com


## Continue Reading

- [Sathyanand on 'The Visual Solopreneur'](/sathya-visuals/)
- [Tanmay Vora on 'Mindset For Creator Economy'](/tanmay-creator-economy/)
- [Mohan Mathew on 'Management Consulting'](/mohanm/)