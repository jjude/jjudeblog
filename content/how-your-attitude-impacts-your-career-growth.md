---
title="How Your Attitude Impacts Your Career Growth?"
slug="how-your-attitude-impacts-your-career-growth"
excerpt="Your attitude determines the limits of your career."
tags=["coach"]
type="post"
publish_at="18 Feb 12 16:30 IST"
featured_image="https://cdn.olai.in/jjude/how-your-attitude-impacts-your-career-growth-gen.jpg"
---

Pratibha Devegowda, who was my colleague at iGATE, asked in LinkedIn, the relation between one's attitude and career growth. With a decade and a half of working experience, I can say that, its one's attitude that drives their professional growth either up or down, more than their qualifications. As I was commenting in LinkedIn, I took a step back and thought what drives that attitude. This blog post is the answer of that question.

I believe, your attitude manifests your world view. In this post I elaborate on my world view, how it shapes my attitude and its effect on my professional growth.

My mental model of life can be summarised as '**its a non-zero sum game**'.

Without going into the details of the game theory (of which I don't know much), this world view translates as:
1. It is a game, so **prepare hard but play smarter**. Days of preparation, which no one sees, precedes hours of play which everyone witness. Your preparation, or its absence, comes out in the open sooner or later.
2. Since it is a game, you will **loose few to win others**. Loosing is okay as long as you prepared well and played well. Your enjoyment shouldn't just depend on the outcome.
3. Though it is a game, it is also a non-zero sum game. The world and life is large enough for you to win without being nasty. You can win **playing as a gentleman**.

This mental model and beliefs form core of my living. So my attitude in life, I don't split life into professional and personal, reflects this. Of course there may be aberrations every now and then, but these are few.

With that said, let me now describe how this impacts my professonal career, which was the question by Pratibha.

1. Because the world is big enough, **opportunities abound**; they are not limited within a company, geography or domain. In these sixteen years, I have lived in Chennai, Brussels, Bangalore and now in Delhi (and travelled on short term to few other cities). None is similar to other - in language spoken, climate, infrastructure and tendency of people. I have worked as an employee as well as a consultant. Mostly I've worked with executives. I was able get all of these variety of experience because 'the world is my playground'; and the enriching experience reinforces the open minded attitude.
2. Moving around with this attitude aid me in **building long-term relationships** from people from different background. Relationships are important for success. Some opened new doors for me, some took the liberty to tap me on the head when pride gets there, some lend me their ears to vent my feelings. Overall, they have provided a balanced perspective.
3. It also means sometimes others took credit for my work or grabbed opportunities that belonged to me. I don't stand by silently when that happens, and **it hurts** when these happen. It's also emotionally draining to deal with such nasty people. But because of the world-view that I have, I quickly come to grounds and concentrate on the next opportunity.
4. Often times, it also leads me to **risky projects**. Though I have failed in few occasions, grand success in others compensate such embarrassments.

In conclusion, my world-view and attitude have lead me through a path of rich professional experience and from where I am standing, I can say, it has been a joyful journey.

