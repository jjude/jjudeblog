---
title="Solo Developers Aren't Just Programmers"
slug="solo-developers-arent-just-programmers"
excerpt="If you are a indie programmer, you are a businessman. Earlier you realize it, it is better."
tags=["tech"]
type="post"
publish_at="12 Nov 11 16:30 IST"
featured_image="https://cdn.olai.in/jjude/solo-developers-arent-just-programmers-gen.jpg"
---

Back When I started in software field, in early 90's as an employee, it was enough to master a programming language - in my case it was C and Visual Basic. You needed to know these languages really well and I did. In fact, I would program for others, for free, just to learn various aspects of the language. Once I wrote a program to control arms & legs of a robot, just to learn serial programming in C; another time I created dlls in VisualBasic that could be used in PowerBuilder to send emails.

But then I became solo and quickly I realised that solo developers don't enjoy the luxury of that silo-expertise.

As a solo developer, you are required to master a whole [gamut of software tools](https://www.prudentdevs.club/tools-of-software-craft). Its difficult, but not impossible. Mostly we know the challenge and 'know' we can handle it. After all, all of these are still software.

What came as a surprise to me and am sure to others in this flock is that suddenly you need to master so many other non-software skills.

You are expected to have good, if not excellent, communication skills - and many fellow programmers will admit it's a nightmare; and you need a knack to close a sale, which includes among many others strong negotiation skill.

Being in India, I am aware that some, if not most, of these tasks can be outsourced. But even that requires these non-software skills like contract negotiation and cash-flow management!

So what I'm saying is, when you transition from an employee-programmer to a solo developer, you are no more a programmer. You are a businessman. Earlier, you realize it, it is better.

