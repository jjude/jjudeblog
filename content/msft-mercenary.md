---
title="Microsoft still competes on price, not on customer obsession."
slug="msft-mercenary"
excerpt="My short lived enthusiasm with Microsoft OneDrive"
tags=["coach","tech"]
type= "post"
publish_at= "29 Jan 20 09:00 IST"
featured_image="https://cdn.olai.in/jjude/one-drive-error.jpg"
---

Satya Nadella has done a fantastic job of transforming Microsoft. That transformative job is showing up in Microsoft's stock price. It has more than tripled after he became the CEO of the company.

I have not used any Microsoft applications or services in the last decade. Since I read so many positive stories about the "new" Microsoft, I wanted to try one of their services.

I have been using Dropbox for sync and storage. Their service works fine across many scenarios - I use Mac, iPad, and Android phone syncing photos, files, and videos. So far, I have been a happy customer. Dropbox recently increased prices. While I wasn't outraged about the hike, it became the necessary nudge to look for other syncing services.

Microsoft has a similar service called OneDrive. The starting plan, Office 365 Personal, is only 4,200 per year for 1 TB of space. Even 6TB of storage space costs only 5,300 per year, while 2 TB of storage at Dropbox costs 8,500 per year. 

There was one more advantage to OneDrive. It came with Office 365 subscription, which means I can use Microsoft Office applications across devices. Looked like a great deal. I immediately subscribed to Office 365 Personal plan. 

**I was waiting to become an ardent fan of Microsoft.**

![MSFT One Drive Pricing](https://cdn.olai.in/jjude/msft-one-drive-pricing.jpg)

My enthusiasm was short-lived.

I had all data in an external drive, but Microsoft OneDrive didn't like it. **It wouldn't sync with the external drive.** Syncing with an external drive is such a standard feature, and I never faced any issue with Dropbox in syncing with one.

Not only Microsoft OneDrive doesn't support an external drive; this particular scenario **doesn't seem to be properly tested**. Look at the error message shown:

> "Make sure that the location **isnvt** on a removable drive..."

![OneDrive error](https://cdn.olai.in/jjude/one-drive-error.jpg)

I ditched the idea of having data in an external drive. I decided to keep all the data in the primary disk itself.  I thought all my problems would go away, and I can get on with my routine work as OneDrive syncs data. 

Hmm...not so fast, said OneDrive with this error.

![Onedrive filename error](https://cdn.olai.in/jjude/one-drive-filename-err.jpg)

Probably in the Windows world, filenames can't contain spaces, but the Mac file system supports these characters. I had not faced any issues with filenames while using Dropbox. That is what is called "**having an eye for detail**" or customer obsession.

I paused to think. I faced three issues with OneDrive:

* I can't sync with an external drive
* The error message has a spelling mistake which indicates the system may not have been properly tested
* I can't use filenames which are acceptable in Mac system

If I am going to have all my photos, e-books, and contracts in cloud storage, I want to be sure that the system is well tested and the data I store in it will still be there years later. With these issues, **I couldn't trust OneDrive** with all my data. I gave up and moved back to Dropbox.

It is a surprise that Dropbox exists as a successful business. Steve Jobs infamously [called](https://www.businessinsider.com/drew-houston-dropbox-steve-jobs-2017-6) Dropbox as a "feature, not a product."

> "Steve started trolling us a little bit, saying we're a feature, not a product, and telling us a bunch of things like that we don't control an operating system, so we're going to be disadvantaged." – Drew Houston, founder of Dropbox

Steve Jobs was right. Dropbox doesn't control the operating system, and it has to depend on the OS for it to function well. But they are a successful business because those OS companies don't obsess about customers.

> The missionary is building the product and building the service because they love the customer, because they love the product, because they love the service.... 

>The mercenary is building the product or service so that they can flip the company and make money....

>It's usually the missionaries who make the most money."

So said, [Jeff Bezoz](https://www.businessinsider.in/strategy/jeff-bezos-said-the-secret-sauce-to-amazons-success-is-an-obsessive-compulsive-focus-on-customer-over-competitor/articleshow/65823979.cms).

Is it possible that Dropbox is a missionary, and Microsoft is still a mercenary?

### Read these posts next

* [Delightful Customer Experience in a Highway Restaurant](/cx-in-highway/)      
* [How to choose technology for your business growth](/tech-for-biz-growth/)     
* [Mastering sales as a new CTO](/cto-sales/)