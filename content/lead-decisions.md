---
title="One decision instead of hundred decisions"
slug="lead-decisions"
excerpt="Lead decisions remove cognitive frictions so we can play at optimal speed and effectiveness."
tags=["wins","coach","frameworks","insights","decisionmaking"]
type= "post"
publish_at= "09 Sep 20 09:00 IST"
featured_image="https://cdn.olai.in/jjude/lead-decisions.png"
---

> Civilization advances by extending the number of important operations which we can perform without thinking about them - Alfred North Whitehead

Southwest Airlines has been profitable for 47 consecutive years. Think about that for a minute. They have been profitable for close to half a century in a competitive industry of which Richard Branson quipped once:

> If you want to be a millionaire, start with a billion dollars and launch a new airline.

How can Southwest achieve this impossible feat? Their profitability is because of a single decision, as made public in their SEC filing.

> The Company's strategy includes the use of a single aircraft type ... Southwest's use of a single aircraft type allows for simplified scheduling, maintenance, flight operations, and training activities. 

They use a single aircraft. Every other success factor is a derivative of this single decision. That one decision of using a single aircraft, Boeing 737, eliminated thousands of decisions. 

The master management guru, Peter Drucker, and his student, Jim Collins, are proponents of "making **a lead decision that removes hundreds of other decisions**." 

!["Lead Decisions"](https://cdn.olai.in/jjude/lead-decisions.png)

I follow this principle for banal as well as beneficial aspects of life. Let me give you a few examples.

**Dieting**: All dieting programs demands you to track the calories you eat. I used to meticulously track the calories of food I ate, juice I drank, and even biscuits I snacked. If I ate more, I would be guilty; If I forgot to enter an item, I would be anxious. It was a frustrating practice for my temperament. This year, I switched to **intermittent fasting**, in which I eat between 10 am to 6 pm. At this time, I eat as usual, only avoiding aerated drinks, fast food, and packed items. Dieting is stress-free with this simplified practice. It has also benefitted me. This simplified dieting, along with home-exercises, helped me lose seven kilos in the last five months (since lockdown started in India).

**Stocks Investing**: Investing could be tricky as you are putting your hard-earned money into an unknown asset for an unknown future. I don't want to spend my time and palpitations locked to tickers running at the bottom of the screen. I invest only in debt-free mid-cap and large-caps with average five-year ROCE of at least 15% with reasonable dividend yields. I also invest in a low-cost index fund monthly.

**Dressing**: My annual shopping trip gets over in twenty minutes since it involves going to an LP showroom and buying dark pants (usually blue jeans) and three light shirts (generally white or blue). The simplified dressing decision is not only about eliminating a few hours from annual shopping time. It also **eliminates dressing decisions every day**. I can randomly pick up a pant and a shirt and go on about the day. 

**Design**: An image amplifies the message. So I try to design an illustration for each blog post. Since I'm not a designer, it has not been easy to design. This year, I started designing slides for my [talks](/talks/) and the new [Gravitas WINS](https://gravitaswins.com/) course. Learning from [Jack Butcher](https://twitter.com/jackbutcher) and [Doug Neill](https://twitter.com/douglaspneill),  I limited myself to four colors—black background, white font color, two more colors for emphasis, one font ([Inter](https://fonts.google.com/specimen/Inter/)) and three font sizes. I was able to create images **faster with this self-imposed limitation**.

**Writing**: I use [Markdown](https://en.wikipedia.org/wiki/Markdown) format for writing and publishing. I forgo some sophistication because of this choice. But again, like every other choice, I gain speed and productivity. Additionally, since Markdown is plaintext, I gain portability too.

### Reducing cognitive friction

All grown-ups play multiple roles - mother, sister, wife, employee, and friend. Information required to play each role is not only increasing, but increasingly becoming complex too. Such load and complexity overwhelm us, and hence we play below our capability. Lead decisions remove cognitive frictions and aid us in **playing at optimal speed and effectiveness**.

### Hedgehog AND Fox

"The fox knows many things, but the hedgehog knows one big thing," wrote the Greek poet Archilochus. Hedgehogs reduce problems into the central organizing principles; on the other hand, foxes apply different strategies for different problems.

Our human tendency is to juxtapose choices as either-or. I prefer "and." As a CTO, I learn multiple strategies and nuances of them to follow the right strategy for a situation, much like a fox. Everywhere else, I am a hedgehog.

## Continue Reading

* [If you see everything, you'll get nothing 🎯](/focus-to-win/)     
* [Principles trump processes](/principles-trumps/)    
* [Build on the rock for the storms are surely coming](/build-on-rock/)


**References**

1. [Southwest Airlines Reports 47th Consecutive Year Of Profitability](http://investors.southwest.com/news-and-events/news-releases/2020/01-23-2020-112908345)
2. [Southwest SEC filing - 2018](https://www.sec.gov/Archives/edgar/data/92380/000009238019000022/luv-12312018x10k.htm)
3. [Finding the One Decision That Removes 100 Decisions](https://tim.blog/2020/01/20/one-decision-that-removes-100-decisions/)