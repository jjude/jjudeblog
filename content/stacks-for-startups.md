---
title="Three stages of startups and how to chose tech-stack for each of them"
slug="stacks-for-startups"
excerpt="Startups can be classified into three stages as per their life-cycle. Here I present guidelines on choosing a tech-stack for each of these stages."
tags=["coach","talks","startup"]
type="post"
publish_at="31 Jul 17 06:49 IST"
featured_image="https://cdn.olai.in/jjude/stages-stacks.png"
---
_AWS invited me for a session at [innov8](https://innov8.work/) on life cycle of a startup and how tech-stack evolves in different stages of a startup. This is the edited version of the talk I presented in the event._

![Stages of startup and tech-stacks](https://cdn.olai.in/jjude/stages-stacks.png)

I was a co-founder of a startup until we ran out of money. As the saying goes, "*those who can, do; those who can't, teach*", I've earned my right to be a teacher 😜.

Now I'm CTO of an IT services company. Here, I design systems for startups and enterprises. Earlier I designed an e-governance system for LLP Act, India. In these roles, I have seen systems of all kind. In this talk, I'll distill what I have learned on this topic - choosing tech-stack for different stages of a startup.

First a caution. Presenting in such diverse group is always a challenge. The group will almost always consist of students, first time founders, and serial entrepreneurs; some in the group might bootstrap their startup, some have outside funding; some might be technical and others may have business background. Any advice given in such a diverse group will be a wrong for one set and apt for another set. As you listen, remember, **there are no universal truths**.

Since *advice is contextual*, I hope you are able to abstract the key points and take what is applicable to you. Feel free to discuss with me via [twitter](http://twitter/jjude) or [email](mailto:feedback@jjude.com).

### Startup Stages

You can classify startups in many ways. For this talk, I am categorizing startups into **three stages as per their lifecycle**:

### Stage 1: Problem and solution fit

In this stage startups are trying to identify a problem that is worth solving. **Not everything that glitters is gold**. Likewise, not every pain is a worthwhile problem to build a startup. 

Once you nail the problem, you need to evolve a viable solution. Viability may or may not mean solving the problem for the first time. Google was not the first search engine; iPhone was not the first smartphone; Amazon was not the first site to sell online. Viability may mean easier to use, cheaper to use, or faster to distribute.

In this stage, you build a **Minimum Viable Product** (MVP), to validate your idea of the problem and the solution. There is no rule that an MVP should have mobile app, API, and database. Many tech founders arrogantly believe if they don't code their product, it is not an actual product.

If you read stories of successful startups, you will find one theme repeated enough. They **composed their solution on top of existing products**. Find some time to read [Reverse Engineering A Successful Lifestyle Business](https://www.toomas.net/2017/07/18/reverse-engineering-a-successful-lifestyle-business-heres-everything-ive-learned-from-reading-indiehackers-com/). I'm quoting two of the points here.

Here is one who **duct-taped his product** instead of coding the product.

> I launched with zero custom software. I used Gumroad, Zapier, Memberful, and Campaign Monitor to basically duct-tape my own product together.

Some even go further. They don't even have a website:

>I wanted to start really simple, so I didn’t even have a website when we launched. I just sent a PayPal subscription link to potential customers over email and then started sending candy to their address periodically.

Product Hunt, the popular product discovery portal, [began as an email list](http://ryanhoover.me/post/69599262875/product-hunt-began-as-an-email-list) in 2014. In 2016, it was sold for $20 million.

### Stage 2: Product and market fit

[Marc Andreessen](https://en.wikipedia.org/wiki/Marc_Andreessen), defines "Product/Market fit" succinctly:

> Product/market fit means being in a good market with a product that can satisfy that market.
 
Two key phrases in the sentence are: **good market** and **product that satisfies**.

At this stage, market means **set of people who search the problem, list out available solutions, and decide**. If you think you can educate people ("the market") about the problem and make them choose your product, you will face a crude shock. I'm an example for it.

In 2014, I co-founded a cyber-security startup. We observed that [security breaches](http://fortune.com/2014/10/03/5-huge-cybersecurity-breaches-at-big-companies/) were common for both large and small companies. Big corporations are able to afford and operate expensive solutions. But small and medium businesses (SMB) can not. We wanted to democratize security operating centers for SMBs.

We saw the problem, we researched and found that the affected market is large enough, and we were able to stitch together a working solution. We demoed our solutions to SMB owners in our network. We reached out to associations and other industry groups. Most of the owners who saw the demo, wanted the product. We crossed the "Problem-Solution Fit" stage with certain success.

As we drained our life-savings, we started to see the _elephant in the room_. These SMB owners weren't **searching** to secure their infrastructure on an ongoing basis. Most SMB owners had a local hardware vendor and they went with whatever the vendor suggested. If the vendor suggested firewall, the SMB owners installed firewall. If the vendor suggested anti-virus, the owners installed anti-virus. Then they forgot about security. They **weren't thinking about the problem** any more. We started educating our potential customers about the ongoing security problem.  We ran out money doing that.

The lesson is simple. If there aren't enough people searching for a solution, you will not live through this stage. As you live through this stage, you will realize that **competition is a good thing** for a startup.

> **If you pick the right market, creating a satisfying product is easier.**

But, you can't create a product that satisfies everyone in the identified market. Google is the dominant search engine. It serves most of the search engine market. But there are privacy conscious users, who refuse to use Google. [Duckduckgo](https://duckduckgo.com/) serves them well. Similarly, [Dropbox](https://www.dropbox.com/) and [Box](https://www.box.com) are in the cloud storage domain but serves different markets. Dropbox and Box **solves the same problem in a different way that attracts different set of people**.

In this stage, tech-stack starts to take shape. Lessons from the duck-tape solution should help you identify features to satisfy this target market. Not all solutions need to have sophisticated mobile app for all platforms, a web app, a backend, and a database. [Instagram](https://en.wikipedia.org/wiki/Instagram) was launched as a mobile app, that too only for iOS. They launched Android app and web-app after 2 years.

> **When founders have more money than sense, they always over-engineer their solution**.

Even on the infrastructure side, you should **start small and scale as needed**. Long back we have to provision whole servers to serve the solution. Now, you can start with minimal infrastructure on a cloud provider like AWS and scale as the traffic builds up.

Founders should invest more in **monitoring, analyzing, and alerting** tools, instead of over-engineering the solution. Say you have an e-commerce application. Then invest in tools that inform about customer journey, pages where customers drop-off, and so on. This helps you learn and iterate as quickly as possible.

### Stage 3: Scale

I have not worked for a growing startup. Hence, I don't have a first-hand experience of the challenges faced by a growing startup. But I have second-hand knowledge acquired by designing and managing systems for large companies (grown startups resemble companies).

When a startup reach this stage, it starts to **serve multiple customer segments with price-points relevant to these segments**. Take example of [Buffer](https://buffer.com/pricing). It targets individuals and companies with 5 different prices. [Dropbox](https://www.dropbox.com/business/pricing) has three types of prices.

At this stage, the company starts to have multiple tech-stacks. It may not build all, it most probably rents. At a minimum, it is going to have **three types of tech-stacks—core product tech-stack, sales and marketing tech-stack, and support tech-stack**. Even the tech-stack for its core product will have multiple sub-stacks.

If you read engineering blogs of [dropbox](https://blogs.dropbox.com/tech/) and [uber](https://eng.uber.com/), you will realize that they need to have multiple tech stacks. Uber, for example, has its core product tech-stack, data-analytics and prediction tech-stack, data-visualization tech-stack, and so on. In fact their core product tech-stack itself is huge, considering that there are driver-side app and consumer-side app in almost all mobile operating systems.

Coming to infrastructure, these companies would need high-availability and disaster recovery configurations with monitoring and alerting mechanisms. This in itself is a challenge and a nightmare at the same time.

As it scales, the company needs **many tools for sales and marketing**. As per Chief Martec site, [an average enterprise uses as many as 91 marketing tools](http://chiefmartec.com/2017/06/average-enterprise-uses-91-marketing-cloud-services/). Same goes for its support stack like HR, Finance, and Admin.


### Guidelines for picking tech-stack

![Guidelines for choosing tech stack for startups](https://cdn.olai.in/jjude/guidelines-for-startup-tech-stack.jpg)

One of the best [anecdote on consulting](https://hbr.org/2016/03/clayton-christensen-what-ill-miss-about-andy-grove) comes from Professor Clayton Christensen. Andy Grove, Intel chief, asked the professor what his theory of disruption means for Intel. Instead of giving a direct answer, the professor taught Andy Grove, "**how to think**" about the problem. I have followed this method for decision making. Instead of seeking answers, I seek "how to think" about a particular problem. Such method has helped me make decision in varying situations. In the similar vein, let me give three guidelines to design tech-stacks for startups.

### Pick the right tool for the job

When a fly invaded his meeting, President Obama [rolled up a magazine and whacked the fly](http://nypost.com/2014/12/31/obama-shows-hes-so-fly-swats-pest-in-oval-office/). He didn't ask for a gun or call up his army generals, or punch-in nuclear codes. He picked a near-by magazine, rolled it, and whacked it himself.

I'm often asked if .Net is better or Python is better or node.js is better for building a startup product. If there is a technology co-founder, my advise is always to **start with the stack that they know**. Later as the product grows, add what is needed, or move to a more suitable stack. [Marco Arment](https://marco.org/) coded backend for his popular [overcast](https://overcast.fm/) podcast player in PHP because that is what he knew. Later when it grew he [switched to Golang](http://highscalability.com/blog/2015/2/2/marco-arment-uses-go-instead-of-php-and-saves-money-by-cutti.html). Likewise, Twitter was originally written in Ruby and then switched to Scala.

As it should be clear now, the **right stack changes as the startup goes through its growth stages**. 

### Build for iteration and feedback

Startup is all about **validating ideas quickly**. So choose a tech-stack that gives you this advantage. Validating ideas quickly is not only about tech-stack. It is also about company's [devops](https://en.wikipedia.org/wiki/DevOps) process. It is also about having probes in the application to give you operational insights. This combination of **tech-stack, devops, and application analytics help validating ideas quickly**.

Most technology teams in **startups don't invest in devops and application analytics**. Lack of devops and analytics limits their agility and speed, more than the tech-stack.

### Decide well or fast

Every business executive has to take ton of decisions. Lack of data complicates the decision making. I use a rule-of-thumb to help me decide. I ask myself, is this something **money can solve at a later time?** There are few problems that money can't solve. 

Company **culture is one problem that more money can't solve**. In fact, money amplifies what is already there. So if a company starts with a sexist, dishonest, authoritarian culture, more money later won't turn the culture around later. Uber is an example.

However, lot of problems can be solved by bringing more money. **Logo, website design, and tech-stack are some of the problems that money can solve**. As I said earlier, Twitter switched from Ruby to Scala. Dropbox is experimenting with Golang. Facebook invested in writing its own compiler for PHP.

If more money can solve a problem, choose to iterate fast. You shouldn't wait on more data. Decide by your gut feel and most probably it will work out ok. If not, bring in more money to solve that problem.

If more money won't solve a problem, think deep. If needed, get outside help. Talk to people who have already solved the problem. These decisions will have lasting impact on the company. Getting them wrong will be a disaster for the founders and the company.

### Conclusion

Entrepreneurship transforms societies. Especially in a developing country like India. India has suffered because of caste discrimination. But today when a delivery guy delivers a pizza from Dominos Pizza or a book from Amazon, we don't check if the guy is from a reputable caste. Or we don't check the caste of the driver before getting into an Uber cab. We need more startups in India to transform India.

Technology used to be a barrier for entrepreneurship. It is now so easy to spin up needed infrastructure on the cloud. There is no reason not to try out the idea that has been haunting you in your head. Go try it and make the world a better place.

<iframe src="https://www.slideshare.net/slideshow/embed_code/key/2SRZEXH2K91QPa" width="595" height="485" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe> 

<div style="margin-bottom:5px"> <strong> <a href="//www.slideshare.net/JosephJude4/stages-of-startups-their-tech-stacks" title="Stages of Startups &amp; Their Tech Stacks" target="_blank">Stages of Startups & Their Tech Stacks</a> </strong> slides from <strong><a target="_blank" href="https://www.slideshare.net/JosephJude4">Joseph Jude</a></strong> </div>
