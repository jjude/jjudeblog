---
title="P.K. Khu₹ana on 'Happiness, Joy and Bliss'"
slug="khuranapk"
excerpt="Money buys comfort not happiness and other truths about happiness"
tags=["wins","gwradio"]
type="post"
publish_at="18 Jan 22 06:00 IST"
featured_image="https://cdn.olai.in/jjude/khuranapk.jpg"
---

<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless src="https://share.transistor.fm/e/0d4a9dd8"></iframe>

My guest is a serial entrepreneur and an award-winning public relations expert. I've known him for the last four years and he has become a guide, mentor and a friend. Despite all the success, he is humble and happy. He's the right person to be a happiness guru. And that's exactly what we are going to talk. I hope you will enjoy the conversation.

### What is your definition of happiness? 

Happiness is:

- One you should be able to be happy, despite the challenges, despite the situations;
- Two, you don't have a sense of comparison with anybody;
- And third is you should be able to control your time.

### Can you really teach somebody to be happy? 

Yes we can teach. This is an acquired skill.

When I was a teenager, I read Dale Carnegie's book - "How to influence people and make friends", and then "Stop worrying, start living." These two books, changed me and I became happier 

So it is an acquired skill. One can learn it. One can teach it.

### Compared to our childhood days, we can connect with anybody and we also have a lot of prosperity. Why are we not happy? 

People don't just think they emote as well. We don't try to understand the feelings of the person. We try to be logical. Sometimes it is not helpful if we only remain logical, if we only use brain without using heart.

And none of the schools unfortunately teaches us how to handle emotions. Unless we understand how to handle emotions. We can't make others happy. We can't be happy ourselves. So that is the reason that's why there is a need of being a happiness guru or helping people as a happiness guru.

Money can buy you comfort, but not happiness. People confuse between comfort and happiness. That's the problem. 

### What is the connection between spirituality and happiness?

One can be happy even without being spiritual. But if you are spiritual, you will be one notch higher in the happiness stream. We are all energy. We are also made of electrons, protons, and neutrons - whether it is H2O. Or CO2 or C O. We are basically energy and energy means we spread and emit vibes to the universe. And we get back what we send.

If you want to listen to an FM station, you will tune to that frequent. Our frequency can be happiness. Our frequency can be sadness. Our frequency can be sorrow. Our frequency can be regret. We get the frequency that we tune into.

### Can we be perpetually happy?

This concept of positivity is a toxic concept. It is not real. And we should not even try to go for this. You are a human being. I'm a human being. If I get a phone call, it may make me sad. We have hurt also, and it is not useful to chase the concept of being always happy in all situations.

The emotions will come. You should only be able to handle that emotion and move on, but it's not possible that you won't be sad. It is not possible that you won't be angry. It is not possible that we are talking about sustainable happiness. Sustainable means you should be able to come back to normal situation as fast, as fast as you can.

This doesn't mean that you will always be happy.

### In what ways it can be toxic? 

We'll be chasing the unreal thing hmm. We will be chasing an impossible target. The target should be achievable. 

### How can we embrace technology and still be happy?

This is a very valid question, but it, whether you use technology or you don't even know there is a technology that exists, it doesn't make any difference at all. It is your state of mind. It does not facebook that makes you angry.

If anything makes you sad or angry, you should be able to come back to the normal situation. As simple as that. Whether you are on Facebook, whether you are on Twitter, whether you are on LinkedIn or any other social media platform, that doesn't matter at all.

Whether the source of anger or sadness is social media or somebody else. That doesn't matter at all. Control is with you. 

### How can my environment affect my happiness?

Though it is up to you, but there are tools to make you happy. Our brain releases, various chemicals that make you happy; that control your emotions; that give you energy; dopamine is one. So if, if you start loving somebody, you play with your child or you play with pets, there is a love chemical that gets released. So you feel happy if you go for a jogging or light exercise, it makes you happy.

Good book reading makes you happy; talking to your family, talking to your friends, makes you happy. So these are these simple actions, plus being organized makes you happy because you are able to accomplish more.

That's why I said there are tools that can be taught or that can be acquired to be happy. 

It is not just the money. It is not just the resources you have, but your mindset and your skills, all things contribute to your happiness.

### Time Management & Happiness

When you get up in the morning, you have the most of the energy. And if you start with the difficult tasks in the morning, you'll be able to finish them because you have better energy cycles. 

And if you finish the difficult task, you'll be happier. Then you have got enough time, to accomplish the easier jobs. Otherwise you keep on doing the easier jobs. And at the end of the day, you, you come to know that you have not even able to touch the difficult tasks. 

It ultimately makes you unhappy because somebody else is unhappy. You have not finished your work. You are not productive. You are not contributing to your organization or to your life, or even to your family. 

### How can I spread happiness?

Very easy sir. The title of today's talk is happiness, joy and bliss. These are not equal to each other.

Happiness is, as we have said, it is a state of being happy. This is actually the lowest. Joy is one notch higher. When you make someone else happy, you became joyful. When you see your child being happy, you are more happy. You are happier. That is joy. When your child gets graduation degree and he throws the cap in the air, you become happy. He is happy. You enjoy the sense of joy. If you make someone happy, that is joy. If you see someone being happy and you relish that feeling, it is also joy.

And suppose if I go to a doctor and he tells me Mr. Khurana, you've got only one hour or one day to live. Then I realized the nothingness of money. When I understand the nothingness of money, I get to the state of bliss.

### What's the kindest thing anyone has done for you?

My wife has been a constant support to me. My family has been constant support to me, always. They've been gently poking me my faults without being arrogant, without judging me and always support.

I'm a human being. I'm not perfect. I also commit mistakes. I have faced losses in business. Also, I have lost some times some dear friends because of my attitude. My family helped me. They brought my friends back. They addressed the issues and they brought those friends back. That made me happier.

### What's your definition of living a good life? 

I should say the example I know is Mr. Joseph Jude. Because as I said earlier, also, you didn't run after money alone. Money is only one thing. Only one factor in life.

If you know this, you are in control of your life. And you support your family, your family supports you. This is how you lead a good life. 

Today people outsource even the parenting. For petty jobs, I can have a maid; for certain things I can have a servant. You can order food and Swiggy will deliver it. You can order a car and Uber will be there to address your needs. And this has made us think that we can also outsource parenting. We should never ever think of outsourcing parenting.

Harvard university has a continuous ongoing research for the last 75 years. They have tracked their students, their alumni, who been in successful, who been criminals or who been very good achievers. They've tracked all that. And for the last 75 years, they have been doing this. This is an ongoing search till date. It is ongoing. 

The crux of the result is people are happy who have managed their relationships.

It's not the money, it's not the power. It's not the position. It's not the branded clothes. 

If I buy Jaguar today and I start driving in Jaguar, people will see, people will notice, but they will notice Jaguar. They won't notice me. They are impressed by the car. Not by me. I'm a monkey sitting in the car.

### Connect with P K Khurana

- Twitter: https://twitter.com/PeeKayPedia

## Connect with me
- Twitter: https://twitter.com/jjude
- LinkedIn: https://www.linkedin.com/in/jjude/
- Website: https://jjude.com/
- Newsletter: https://jjude.com/subscribe/
- Youtube: https://youtube.com/gravitaswins
- Email: podcast@jjude.com
- Executive Coaching Program: https://gravitaswins.com

## Your feedback counts

Thank you for listening. If you enjoy the podcast, would you please leave a short review on Apple podcast or on YouTube? It takes less than 60 seconds, and it really makes a difference in finding this podcast. And it boosts my spirits. 😀

I would like to hear what resonated the most with you in this episode. Either you can email me or share it on social media and tag me. Thank you for your support.

Have a life of WINS.

## Amazing tools I use:
- I record all the remote interview with [Riverside.fm](https://riverside.fm/?via=joseph-jude)
- I use [Transistor.fm](https://transistor.fm/?via=jjude) for hosting these podcast episodes

_Gravitas WINS Radio is produced by [Joseph Jude](https://jjude.com/). Be sure to subscribe on [Apple Podcast](https://podcasts.apple.com/in/podcast/gravitas-wins-radio/id1562802449), [Spotify](https://open.spotify.com/show/7LOBjAyjBkechMC0xMIj9E),[Google Podcast](https://podcasts.google.com/feed/aHR0cHM6Ly9mZWVkcy50cmFuc2lzdG9yLmZtL2dyYXZpdGFzLXdpbnMtcmFkaW8=),[YouTube](https://www.youtube.com/gravitaswins), or the podcasting app of your choice._

Please check out other episodes of [Gravitas WINS Radio](/gwr-themes/). For easy consumption, they are categorized by themes. If you are a fellow podcaster, you might be interested in [How I Podcast](/how-i-podcast/) or [tools I use](/i-use/).

## Continue Reading

* [How to become a writing LION?](/writing-lion/)
* ['Words Change Lives' with Ritika Singh](/ritikas/)
* [Themed Days - My Productivity Secret](/themed-days/)
