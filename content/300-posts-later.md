---
title="After 300 blog posts..."
slug="300-posts-later"
excerpt="Through 12 years and 300 posts, blogging has become an integral part of me. It has been a rewarding journey."
tags=["blogging","insights"]
type="post"
publish_at="07 Dec 17 15:29 IST"
featured_image="https://cdn.olai.in/jjude/300-posts.png"
---
I have been blogging since 2005. In 12 years, I managed to post 300 posts. This is the 301th post. Unbelievable that I stayed through it for this long.

![300 posts later](https://cdn.olai.in/jjude/300-posts.png "300 posts later")

Most of these posts are [code](https://www.prudentdevs.club/tags/code/) samples; some of them are lessons in and for [business](/tags/biz/); and few of them are tips to building [career](/tags/coach/) in software industry. Without an explicit plan, this blog has become my portfolio.

### Objectives

I encourage everyone to blog. It is one way to build a [portfolio](https://www.prudentdevs.club/dev-portfolio/) that showcases your expertise. But blogging doesn’t have to be only about showcasing expertise. You could have other objectives. Some of these are:

**Money**: If you write with insight you can earn through advertisements, subscriptions, and sponsorship. [Daring Fireball](https://daringfireball.net/) is one such blog. But, there are people who blog only to generate income from affiliate links. They become boring after a while.

**Reputation**: If you keep posting insightful posts, people will flock to your blog and that could bring fame. [Troy Hunt](https://troyhunt.com/) started posting about application security and cyber-security. He became a Pluralsight author. He is also recognized as a Microsoft MVP. Recently he has been selected to [testify before US congress](https://www.troyhunt.com/im-testifying-in-front-of-congress-in-washington-dc-about-data-breaches-what-should-i-say/). How cool is that!

**Learning**: "I write for the unlearned about things in which I am unlearned myself", said C.S. Lewis. I blog to learn. I framed a [learning strategy](/sdl/) — consume, produce, and engage. I consume books and articles. I produce blog posts. I share and engage with other readers.

Whatever is your objective, one thing you should know. It takes a long time to reach it. Really long time. Knowing why will get you through low periods when you don't feel like writing one more word.

You don’t have to be limited by your initial objective. As you scale one objective, you can set your sight on another.

### What to blog

Once you figure out the objective, you should know what to blog.

**Blog what you know**: [Sowmay Jain](https://sowmayjain.com/) blogs about what he is good at — investing. That makes his blog interesting. Blog about what you know well — sports, fitness, classical dance, music. Share how you learned. Share why it is interesting. Share a road-map if someone were to start today. These topics are interesting.

**Blog what you want to know**: As I said earlier, I blog what I learn. Writing helps me to think clearly. I learned [problem solving](/be-a-problem-solver/), [Hapijs](https://www.prudentdevs.club/hapijs/), and now [React Native](https://www.prudentdevs.club/learning-react-native/).

### Where to blog

Once you have figured out why and what, figure out where. There are many blogging [platforms](/blogging-platforms/).

**Wordpress** ([WordPress.com](https://wordpress.com/)): This is the grand-daddy of the blogging revolution. More than half of blogs use Wordpress software. You can host on your own site or use their hosting. If you are not technical and just starting out, I recommend using their hosting. It is free to start and easy to post.

**Medium** ([Medium](https://medium.com/)): This is a recent addition. It is also free to start like Wordpress.

**Olai** ([Olai](https://www.olai.in)): I created this platform. It is evolving. [Sign up](https://app.olai.in/signup) and start telling your stories.

As Arthur Ashe says: "Start where you are. Use what you have".

### Some tips learned along the way

If you are starting blogging, keep these tips in mind.

**Write for one**: Don’t worry about traffic. Don’t worry about what others will think about your posts. Start writing for yourself. Only criteria that matters is, will you read the post you just published. If you answered yes, then you did a good job. Move on to write the next post. If you find it difficult to write for yourself, then write for one of your friend. Not friends but one friend. Just focus on one. Like writing an email.

**Don’t mix writing and editing**: When you start it is extremely difficult to separate the two. But if you mix them, you will never finish your post. I write on my mobile walking and edit on my laptop sitting.

**Use simple language**: Whether you blog in English or Hindi or Tamil, use simple language. Don’t write like what you read in newspaper. Write how you speak to your friend. That way you can get your thoughts quickly and it is also nice to read.

**Quantity vs Quality**: When I started I would write, re-write, and re-write one post for a month. I would get discouraged and never blog for months. Then I learned to differentiate between [quantity and quality](/quantity-vs-quality/). As a creator, you need to practice with quantity. You build a momentum by writing and posting. As you write more and post more, it becomes easier to write and post even more.

**Have a theme**: If you write about movies one day, javascript another day, and ships on the third day, the readers will get confused. Have a theme that ties your posts. You can have multiple [themes](/its-not-a-blog-its-a-book/) through different periods though.

**Play a background music**: I don’t know the science behind it. But having a pop / rock music at the background gets my fingers to move fast. Every time I feel slow and not able to write, I play some songs. As I write this, [Little Drummer Boy](https://www.youtube.com/watch?v=5l1CS0Jhk90) is playing at the background in a loop.

Through 12 years and 300 posts, blogging has become an integral part of me. Needless to say it has been a rewarding journey.

I'm looking forward to next 300 posts. Hope it doesn't take another 12 years.

## Continue Reading

* [Seven tips to writing fantastic blog posts regularly](/improve-blogging)
* [Seven Insights From Two Months of Blogging](/seven-insights-from-two-months-of-blogging)
