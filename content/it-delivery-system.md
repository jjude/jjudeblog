---
title="How to improve project delivery in an IT services company?"
slug="it-delivery-system"
excerpt="How do you take a mid-size IT service company into its next level?"
tags=["coach","systems"]
type="post"
publish_at="11 Feb 16 08:57 IST"
featured_image="https://cdn.olai.in/jjude/process-outcome-matrix.png"

---

People drive growth. This is true of every company, despite its size. This truth is felt even more acutely in small and medium sized companies.

Every small and medium company depends on rock-stars. These rock-stars carry their companies on their shoulders through their skills, grit, and determination. They achieve amazing feats. They might even become demigods within the company because of their achievements.

This works well, as long as the number of employees is less than 100. As the [company doubles its size, everything breaks][1]. Size brings its own challenges. Now the company can't depend only on people. They need processes -- a mechanism to share knowledge, estimate, deliver projects, hire, and even to fire.

I have been advising a mid-sized IT service company for the past few months in building their project management office. As a believer of systems-theory, I have been building a system in order to deliver projects in a **predictable** fashion. What can such a system do?

A good system aids people to excel through it. It also self-corrects with feedback. A system doesn't have targets (like $2 million in 2 years), though it has milestones (crossing $2 million). A self-correcting system has an another advantage. It can evolve with the changing needs.

![System vs Outcome](https://cdn.olai.in/jjude/process-outcome-matrix.png)

Though I learned control-systems in my Engineering course, I understood the applicability of systems only after reading [More than you know][2] by Michael Mauboussin. In this book Michael presents a simple matrix, which he borrowed from 'Winning Decisions' by Russo and Schoemaker, depicting the relation between process and outcome in investment decision making. It means that, if we follow a good system and stick with it for long enough, good outcome is inevitable. This is as much true for company's growth as for investing.

When viewed as a system, IT delivery has projects and people as inputs; revenue and profit as output; regular reviews, employee satisfaction surveys, and customer satisfaction surveys as self-correcting feedback. The core of the system consists of development methodology, deployment techniques, project management methodology, and standards and governance. This system should be data-driven if it has to be effective.

![IT Delivery System](https://cdn.olai.in/jjude/it-delivery-system.png)

Any system is **inter-dependent**. Though each component can be improved independently, the benefit of a system can be realised only if **all the components improve** simultaneously.

Building a predictable, stable delivery engine is impossible if a company picks up every **project** that comes along its way. Picking up a php project, developing a Wordpress theme, and developing a product for a startup might work for a small company. But as it grows, it should focus on building a sales pipeline with certain type of projects, say e-commerce products. Automating deployment of a Wordpress project is not the same as that of an iOS product. If sales team can focus on bringing certain types of projects, then delivery team can focus on automating as many tasks as possible in delivering and deploying such projects.

**People** form the pillar of the IT delivery engine. Hiring, firing and training are three pieces of improving skills withing a company. [Henry Ward][3], CEO of eShares, wrote about his hiring and firing principles in a recent [article][4]. He makes an excellent point about two aspects of hiring.

> A False Positive (FP) is when we hire somebody who doesn’t work out (i.e. we falsely believed they would be great). A False Negative (FN) is when we did not hire somebody who would have been great. Hiring efficacy is measured by a low False Positive and False Negative rate.

> We should not be afraid of False Positives. We can quickly fix a False Positive hiring decision. However, we should be afraid of False Negatives. We can never fix a False Negative mistake.

If you are in-charge of hiring, go read that article by Henry Ward. It contains many contrarian, yet useful ideas about hiring.

Once you get the best guys and gals into the team, you should continue to train them. Even the people in top positions need training. There are some common information like history of the company, values of the company, and vision of the CEO for the company, that should be spread as frequently as possible. Everyone in every level should understand these aspects well. Everyone, especially the top management, should understand the values & vision of the company. **A united house stands and grows; a divided house destroys itself**.

Every activity within a company should increase either **revenue or profit**. For the purpose of this article, profit is a function of productivity. How can we boost productivity within a company? There are two ways by which we can improve productivity: choosing better tools for the job and automation. Like every aspect within this system, both of these are continuous processes.

What is the core of this system? As this is a system for IT delivery, it goes without saying that delivery, deployment, and project management are the key components. Of late, agile methodology has become the popular management methodology. Though, there are definite merits in agile methods, it is still early years of agile methods, compared to waterfall model. We will see agile methodology mature in the coming years.

Deployment is one process that is still manual in mid-sized companies. As with any manual process, it is error-prone and time-consuming. Deployment should be automated with tools like [Ansible][5] or [Docker][7].

If this system has to self-correct and improve, feedback should become part of the process. Many systems are sabotaged by powerful individuals because "feedback" was suppressed. Is it possible to avoid this tragedy? Yes. How? Institute a regular review. And review with metrics. Let everyone know the metrics, and their calculation. Metrics should help in comparing projects, and they should indicate if there is improvement in the system.

Most of these metrics are already available for IT delivery units. Some of them are billing, cost, velocity, bugs density, technical-debt and so on. If you use a tool like [jira][6], most of these metrics are automatically calculated for you.

Other feedback that feed the self-correcting engine are satisfaction surveys. If customers, both external and internal, don't feel the improvement, the system has to undergo change. Customers should "feel" the benefits, because business is all about relationships and relationships are seldom developed purely on metrics.

Initiating a system is only the beginning of the road to excellence. It is a tiring and draining exercise to keep the system going. But as Michael notes a good system produces success. Who said success is easy!

[1]: http://www.forbes.com/sites/alisoncoleman/2015/09/13/the-scale-up-challenges-every-audacious-startup-must-face/
[2]: http://www.michaelmauboussin.com/excerpts/MTYKexcerpt.pdf
[3]: https://twitter.com/henrysward
[4]: https://medium.com/swlh/how-to-hire-34f4ded5f176
[5]: http://www.ansible.com
[6]: https://www.atlassian.com/software/jira
[7]: https://www.docker.com
