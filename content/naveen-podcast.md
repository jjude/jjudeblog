---
title="Naveen Samala on 'Benefits of podcasting'"
slug="naveen-podcast"
excerpt="If you are thinking of starting a podcast, listen to this episode."
tags=["gwradio"]
type="post"
publish_at="21 Mar 23 07:16 IST"
featured_image=""
---

I'm talking to Naveen, a fellow podcast host, a successful one. He has interviewed more than 300 guests in his podcast "The Guiding Voice". Not only that, over the years, that venture has grown to include a book and podcasts in 2 other languages. For my own selfish benefit, I want to know how he manages to do all of that.

<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless src="https://share.transistor.fm/e/70fb8466"></iframe>

Hope this episode is inspirational and educational too. Enjoy the conversation.

## Topics discussed

- How Naveen started with The Guiding Voice
- How big is the team?
- What is Naveen's time management secret?
- Rest Routine
- From scheduling interviews to release
- Podcast Tools
- Promoting the podcast episode
- Podcast Metrics
- Building an ecosystem around podcast
- Skills Naveen acquired by interviewing guests
- Advice for beginners
- Future of The Guiding Voice
- Kindest thing anyone has done for Naveen
- Best leadership quality

## Connect with Naveen
- Twitter: https://twitter.com/naveensamala
- LinkedIn: https://www.linkedin.com/in/naveensamala/
- Website: https://www.naveensamala.com/

## Connect with me
- Twitter: https://twitter.com/jjude
- LinkedIn: https://www.linkedin.com/in/jjude/
- Website: https://jjude.com/
- Newsletter: https://jjude.com/subscribe/
- Youtube: https://youtube.com/gravitaswins
- Email: podcast@jjude.com
- Executive Coaching Program: https://gravitaswins.com

## Your feedback counts

Thank you for listening. If you enjoy the podcast, would you please leave a short review on Apple podcast or on YouTube? It takes less than 60 seconds, and it really makes a difference in finding this podcast. And it boosts my spirits. 😀

I would like to hear what resonated the most with you in this episode. Either you can email me or share it on social media and tag me. Thank you for your support.

Have a life of WINS.

## Amazing tools I use:
- I record all the remote interview with [Riverside.fm](https://riverside.fm/?via=joseph-jude)
- I use [Transistor.fm](https://transistor.fm/?via=jjude) for hosting these podcast episodes

_Gravitas WINS Radio is produced by [Joseph Jude](https://jjude.com/). Be sure to subscribe on [Apple Podcast](https://podcasts.apple.com/in/podcast/gravitas-wins-radio/id1562802449), [Spotify](https://open.spotify.com/show/7LOBjAyjBkechMC0xMIj9E),[Google Podcast](https://podcasts.google.com/feed/aHR0cHM6Ly9mZWVkcy50cmFuc2lzdG9yLmZtL2dyYXZpdGFzLXdpbnMtcmFkaW8=),[YouTube](https://www.youtube.com/gravitaswins), or the podcasting app of your choice._

Please check out other episodes of [Gravitas WINS Radio](/gwr-themes/). For easy consumption, they are categorized by themes. If you are a fellow podcaster, you might be interested in [How I Podcast](/how-i-podcast/) or [tools I use](/i-use/).

## Continue Reading

- [Sampark A. Sachdeva On 'Linkedin Success Mantras'](/sampark-linkedin/)
- [Ritika Singh on 'Words Change Lives'](/ritikas/)
- [Roshni Baronia On 'Digital Sales For Startups & SMEs'](/roshni-digital-sales/)