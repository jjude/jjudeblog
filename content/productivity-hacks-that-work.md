---
title="Productivity Hacks That Work"
slug="productivity-hacks-that-work"
excerpt="Practical hacks to improve your productivity."
tags=["productivity"]
type="post"
publish_at="24 Oct 12 16:30 IST"
featured_image="https://cdn.olai.in/jjude/productivity-hacks-that-work-gen.jpg"
---

I became an independent software vendor, with the launch of BlogEasy, a blog-editor for MacOSX. With a pre-existing consulting gig, product development is a demanding task. Add to it parenting of two kids—one 3 years and another 6 months. I want to witness their growth without relegating them to periphery as I concentrate on building my business.

If I've to be successful at each of these, then I got to be absolutely efficient in getting things done.

Thankfully, I got a very supporting wife. Encouraging wife and naughty kids can relieve a lot of stress in building a solopreneur venture.

I have been tweaking my productivity system long before these ventures began. Sowing seeds well before you need shade, comes in handy when you need …well, a shade.

So here is what is working for me.

<a name="tie"></a>**Tie activities to devices/places**: It was [Seth Godin][1] who blogged about the idea of tying activities to devices. I follow this with a rigorous discipline - iPhone is tied to social-media & games (in addition to, of course, communication); laptop to development and client assignments; desktop to photo and video editing. On my laptop there are no social-media clients, games or editing software.

I have also gone a step ahead and tied activities to places.

My home-office is only for doing work—playing with kids, entertainment, and resting is strict no-no here and I don't take laptop to bed.

Contrary to my initial skepticism, this has worked wonders. This hack alone has boosted my productivity to a greater extent.

<a name="onething"></a>**Do only one thing at a time**: Technology has enabled us to open as many tabs as needed on browsers, open as many applications as possible, check twitter streams while talking and so on and on. The temptation to squeeze a little bit more while waiting is deep but equally damaging.

A clown in circus may be able to throw many balls in the air and catch them too, but I'm no clown and life isn't a circus. I focus on getting one thing done at a time. If opening of an application takes few seconds, I wait; if the program is compiling, I wait. When the application finally opens, there is no cognitive drag.

It is counterintuitive. But it works!

<a name="buytime"></a>**Buy time**: Like everyone else, I have known the equation: time = money. But it was an epiphany when I realized that the reverse is also true like in any equation.

After the realization, I hire a cab if I've to drive for more than an hour. It costs, but I also get time in which I can listen to podcasts, write a blog post, read an article, code, or nap, if tired.

[Dr Alan Weiss][3] said it best—*discretionary time is the real wealth*.

<a name="lessdecision"></a>**Make as less decisions as you can**: Much has been written about Steve Jobs' turtleneck, but the reasoning behind the 'personal-uniform' is that he doesn't have to be distracted by trivia. He focused his cognitive effort in what mattered the most in his job. President Obama too, follows such a routine as [narrated][2] by Michael Lewis.

I'm not wearing turtlenecks or white-shirt with a red-tie but I'm learning to make as less decisions as I can.

If I don't have to make a decision, I don't; if I can postpone the decision-making, I usually do; if I can outsource that decision-making, I do; if I can stick with an earlier decision, I do.

<a name="tweak"></a>**Tweak, but not obsessively**: Every now-and-then I tweak my 'getting-things-done' system with a hope of getting better results. But interval between these tweaks are long and even when I do, I change little rather than making big-bang changes. Constant tweaking wastes both time and energy.

Productivity hacks work best within a context. Whenever I read about an useful productivity hack, I adapt it to my situation. If you find a useful hack in this list, adapt it to your situation.

[1]: http://sethgodin.typepad.com/seths_blog/2011/03/are-you-making-something.html
[2]: http://www.vanityfair.com/politics/2012/10/michael-lewis-profile-barack-obama
[3]: https://alanweiss.com/

