---
title="This ≠ That"
slug="this-not-that"
excerpt="this is not that"
tags=["coach","startup"]
type="post"
publish_at="30 Mar 12 16:30 IST"
featured_image="https://cdn.olai.in/jjude/this-not-that-gen.jpg"
---

Delegation ≠ Decision Making
Dissent ≠ Disloyalty
Busy ≠ Progress
Problem Solving ≠ Innovation
Past Trends ≠ Future Trends
Exception ≠ Norm
University Degree ≠ Competence
Honest ≠ Competent
Success in one field ≠ Success in any field
Productivity ≠ Squeeze water from stone
More ≠ Quality
Can ≠ Should

