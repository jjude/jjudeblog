---
title="How To Gain From Insights"
slug="profitable-ideas"
excerpt="To be profitable, an idea must be both correct and contrarian. And you've to hold it for long."
tags=["wins","biz","coach"]
type="post"
publish_at="16 Aug 21 16:25 IST"
featured_image="https://cdn.olai.in/jjude/profitable-ideas-gen.jpg"
---

### Short History Of My Investing

I asked a trusted friend for a stock tip when I wanted to start investing. On his advice, I purchased 500 Ashok Leyland shares at Rs. 20 each. As is normal in markets, the price dropped to Rs. 15 in a few months. Then, I panicked and sold all of my stocks, vowing never to invest in stocks again.
  
I never keep a promise. I returned to investing in stocks. This time, I educated myself. Based on my market analysis, Ashok Leyland was indeed a fantastic stock. I started accumulating the stock. The stock prices fluctuated, as they always do. I would examine any news that contradicted my analysis. I bought more when I found none. When the price went up, I bought the stock; when it went down, I bought it.
  
I accumulated in thousands at an average price of Rs. 24. As soon as the price rose above 44, I stopped buying. It was 15 years ago. Ashok Leyland's stock price today is Rs. 120.
  
I followed the same path for Manali Petrochemicals. Rs. 11 is the average cost price. Recently the price went up to Rs. 100. The price crashed to Rs. 10 last year. For a moment, I wondered if I should book profit and exit the investment. The stock still looked good based on my analysis. As a result, I remained steadfast. It is currently trading at Rs. 98 in the market as I type.

### Profitable Ideas

To be profitable, an idea must be both correct and contrarian.

We implicitly accept all of our ideas as correct. For centuries, however, the brightest minds thought that "Earth is flat" and "Sun revolves the Earth". Even today, many societies believe that women are more suitable in the kitchen, even though women have accomplished everything on earth and gone to space. Over the past two years, we have all proved that we can all work without supervision. Yet, many CEOs prefer to bring all employees into the office rather than relying on remote work. Validate your ideas always.

Your idea may be correct. However, if it is an idea that everyone believes, then it is not a profitable one. Before the crowd sees the correct idea, or when no one believes in it, you need to embrace it. Only then will you win the jackpot. Without it, you gain little or nothing.

### How To Generate Profitable Ideas

The following three steps will help you generate profitable ideas:

### Think For Yourself

I will illustrate this point with one of my favorite stories about Mulla Nasrudin. 

Once, Nasrudin and his son went to market with a donkey. A passerby remarked, "This man is a fool. He has a donkey that his son can ride, but he makes him walk to the market."

After hearing this, Nasrudin lifted his son and placed him on the donkey. Then another passerby commented: "The young son is so heartless. He let the old father walk." Hearing this, Nasrudin got the son off the donkey's back and climbed on it himself.

Someone commented, "What a shame! The old man is so mean to his little child to let him walk while he is riding a donkey." Hearing this, Nasrudin picked up his son, seated him on the donkey's back, and they continued the journey.

Nasrudin overheard some people saying, "How cruel these father and son are, overloading a donkey like that. They show no mercy to poor animals"

The comments annoyed Nasrudin and his son, so they got off and continued their walk to market as they had started.

Moral of the story? You'll be nothing more than chaff in the wind if you listen to everyone. 

It is **not necessary to be original** when thinking for yourself. You could read industry reports, observe trends, and speak with experts.  Then, you form your conclusion based on all of the inputs. When you think for yourself, **you don't outsource your decisions**, even to experts.

### Have Conviction

_The problem with the world is that the intelligent people are full of doubts, while the stupid ones are full of confidence. - Charles Bukowski_

**You can borrow ideas from others, but not conviction**. If you haven't thought about an idea on your own, you cannot have conviction.

Often even highly competent individuals don't have the conviction on their idea. They are unable to make up their minds. They have brittle minds.

You won't go all-in unless you have the conviction. If you don’t go all in, your eventual gains will be minimal.

Conviction helps you face difficult situations with calmness. I was able to hold onto my stocks when Manali Petrochemical fell to its earlier levels because I trusted my analysis.

### Hold For Long

_The big money is not in the buying or selling, but in the waiting. - Charlie Munger_

Once you have a conviction, you can remain committed to that conviction as long as no fundamental circumstances change. 

Nowadays, we expect instant results from every effort made in social media. When we post a photo on Instagram, we expect likes, comments, and dollars. That's not how the world works.

A long game involves believing in your idea and holding onto it for as long as it takes. Really long. Say 20 years. It is only possible if you have thought about this idea on your own. A borrowed conviction has a short lifespan.

### Summary

I can summarize the idea into a formula that is easy to remember:

**Profit = Idea * Time**

Although I've given examples of investing, the principles apply equally well to other domains as well. Parenting, marketing, career building, and so on. If you work on a correct idea long enough, you will reap the rewards.

## Continue Reading

* [Is There DOPE In Your Network?](/network-dope/)
* [Future Doesn't Happen To Us, Future Happens Because Of Us](/shape-the-future/)
* [Ideas Don't Matter, If You Don't Act](/ideas-and-actions/)
