---
title="Dr Deborah Thomson on 'One Health And Science Communication'"
slug="deboraht"
excerpt="How to communicate complex subjects like climate change to kids, public, and politicians. Also how environment and animals are deeply connected to our well being."
tags=["wins","gwradio"]
type="post"
publish_at="21 Dec 21 06:00 IST"
featured_image="https://cdn.olai.in/jjude/deboraht-gen.jpg"
---

<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless src="https://share.transistor.fm/e/b7624128"></iframe>

- What are the rules for leading a conversation on controversial subject?
- How Dr Deborah used these principles to communicate science?
- How talking to kids and toastmasters helped Dr Deborah sharpen her communication skills
- How to build trust while communicating controversial subjects?
- What is one health and why does it matter?
- Since Covid hit every country's economy, will political leaders take one health seriously?
- What measures can we, as common people, play in build one health?
- What are the qualities of leaders you admire?
- What is your definition of living a good life?
- What is the kindest thing anyone has done for you?


### Connect with Dr. Deborah

- LinkedIn: https://www.linkedin.com/in/deborahthomsondvm/
- Instagram: https://www.instagram.com/deborahthomsondvm/
- Website: https://www.deborah-thomson.com/
- Book: https://amzn.to/3rAemM4


### Connect with me

- Twitter: https://twitter.com/jjude
- Newsletter: https://jjude.com/subscribe/
- LinkedIn: https://www.linkedin.com/in/jjude/
- Youtube: https://youtube.com/gravitaswins
- Executive Coaching Program: https://gravitaswins.com

## Your feedback counts

Thank you for listening. If you enjoy the podcast, would you please leave a short review on Apple podcast or on YouTube? It takes less than 60 seconds, and it really makes a difference in finding this podcast. And it boosts my spirits. 😀

I would like to hear what resonated the most with you in this episode. Either you can email me or share it on social media and tag me. Thank you for your support.

Have a life of WINS.

## Amazing tools I use:
- I record all the remote interview with [Riverside.fm](https://riverside.fm/?via=joseph-jude)
- I use [Transistor.fm](https://transistor.fm/?via=jjude) for hosting these podcast episodes

_Gravitas WINS Radio is produced by [Joseph Jude](https://jjude.com/). Be sure to subscribe on [Apple Podcast](https://podcasts.apple.com/in/podcast/gravitas-wins-radio/id1562802449), [Spotify](https://open.spotify.com/show/7LOBjAyjBkechMC0xMIj9E),[Google Podcast](https://podcasts.google.com/feed/aHR0cHM6Ly9mZWVkcy50cmFuc2lzdG9yLmZtL2dyYXZpdGFzLXdpbnMtcmFkaW8=),[YouTube](https://www.youtube.com/gravitaswins), or the podcasting app of your choice._

Please check out other episodes of [Gravitas WINS Radio](/gwr-themes/). For easy consumption, they are categorized by themes. If you are a fellow podcaster, you might be interested in [How I Podcast](/how-i-podcast/) or [tools I use](/i-use/).

## Continue Reading
- ['Words Change Lives' with Ritika Singh](/ritikas/)
- [Culture is what you do, not what you believe](/culture-is-doing/)
- [Book Notes - Smartcuts](/smartcuts/)
