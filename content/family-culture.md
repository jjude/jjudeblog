---
title="Building a family culture"
slug="family-culture"
excerpt="What are the three key elements of building a strong family culture?"
tags=["gwradio","parenting"]
type="post"
publish_at="12 Feb 24 16:10 IST"
featured_image="https://cdn.olai.in/jjude/family-culture.jpg"
---
Hello and welcome to Gravitas WINS conversations. KK and I are doing a series on "Being a dad".  We already talked about how we learn to be a dad, who are our role models, and how we bond with kids. In the Today we are going to talk about building a family culture.

I hope you find this actionable and insightful. If you have any questions or comments, feel free to send them to either of us.

<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless="" src="https://share.transistor.fm/e/936eb4a0"></iframe>

## What you'll hear

- Why family culture is important
- Elements of family culture
- It is not easy
- Role of grandparents
- Role of environment
- We have not figured out everything
- Closing thoughts

## Edited Transcript

_coming soon_

## Watch on YouTube
<iframe width="560" height="315" src="https://www.youtube.com/embed/txfhZfSuX94" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

## Connect with KK
- LinkedIn: https://www.linkedin.com/in/storytellerkrishna/

## Connect with me
- Twitter: https://twitter.com/jjude
- LinkedIn: https://www.linkedin.com/in/jjude/
- Website: https://jjude.com/
- Newsletter: https://jjude.com/subscribe/
- Youtube: https://youtube.com/gravitaswins
- Email: podcast@jjude.com
- Executive Coaching Program: https://gravitaswins.com

## Your feedback counts

Thank you for listening. If you enjoy the podcast, would you please leave a short review on Apple podcast or on YouTube? It takes less than 60 seconds, and it really makes a difference in finding this podcast. And it boosts my spirits. 😀

I would like to hear what resonated the most with you in this episode. Either email me or share it on social media and tag me. Thank you for your support.

Have a life of WINS.

_Gravitas WINS Radio is produced by [Joseph Jude](https://jjude.com/). Be sure to subscribe on [Apple Podcast](https://podcasts.apple.com/in/podcast/gravitas-wins-radio/id1562802449), [Spotify](https://open.spotify.com/show/7LOBjAyjBkechMC0xMIj9E),[Google Podcast](https://podcasts.google.com/feed/aHR0cHM6Ly9mZWVkcy50cmFuc2lzdG9yLmZtL2dyYXZpdGFzLXdpbnMtcmFkaW8=),[YouTube](https://www.youtube.com/gravitaswins), or the podcasting app of your choice._

Please check out other episodes of [Gravitas WINS Radio](/gwr-themes/). For easy consumption, they are categorized by themes. If you are a fellow podcaster, you might be interested in [How I Podcast](/how-i-podcast/) or [Tools I Use](/i-use/).

## Continue Reading

- [Mahendran Kathiresan on 'Homeschooling In India'](/mahendrank/)
- [How our fathers, society, and peers shape our fatherhood](/fatherhood-influences/)
- [How we learn about fatherhood?](/learn-to-be-father/)