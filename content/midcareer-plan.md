---
title="How to strengthen your career prospects at mid-career?"
slug="midcareer-plan"
excerpt="Our careers give us with a great deal of satisfaction. Mid-career is the right time to reflect and reshape the career."
tags=["wins","coach"]
type="post"
publish_at="14 Sep 22 18:24 IST"
featured_image="https://cdn.olai.in/jjude/midcareer-plan-gen.jpg"
---


After working in a corporate job for ten to fifteen years, it is time to reflect on what you want out of your career. With your experience, you can shape the rest of your career for meaning and impact. The following are three steps you can take to improve your career prospects at mid-career.

1. Choose the game you want to play
2. Learn the game
3. Build relations

Let us look deep into each of these steps.

## Choose the game you want to play

When you are the middle of the career, you can pick one of the three options for your career:

1. Entrepreneurship
2. Management
3. Technical

The experience you gain from 10 - 15 years in a corporate job will allow you to **notice the deeper problems** in the domain. Your skills can be used to launch your career as an entrepreneur. It is not necessary to start a company to be an entrepreneur. Even becoming a freelancer is an option. Today, playing in a creator economy is a viable option. You could operate as [Multi-SKU creator](https://hunterwalk.medium.com/why-a-paid-newsletter-wont-be-enough-money-for-most-writers-and-that-s-fine-the-multi-sku-f41daa074cdb) as [Hunter Walk](https://twitter.com/hunterwalk) proposes:
- There could be a podcast SKU
- A speaking fee SKU
- A book deal SKU
- A consulting SKU
- A guest columnist SKU. 

And so on

Another option is to become a manager. Management is a popular career path for Indian employees. The transition from an individual contributor to managing a team is not easy. In order to succeed, you need a combination of skills, the most critical of which is people skills. People management is not for everyone. **The ability to work with people and collaborate with them can propel your career forward** more quickly if you master this skill.

The last option is to be a technical contributor. You could stay as close to the technical side as possible. For example, you can choose to be a software architect and work towards becoming a chief technology officer. Opportunities like these are few and far between. To succeed in the technical field, you need to **gain a deep understanding of the subject matter and practical experience**.

## Learn the game, rules, content in deep

Once you've decided on the game to play, you need to learn the intricacies of the game. You could learn the games via:

- University courses
- Self-created learning path
- Building a portfolio

The best thing about university courses is that they give you a **guided path** towards improving your skills. Not only that. They also give you access to a broader network of alumni and peers. You will be able to faster your career journey, if you enroll in a branded university and build your network while you study.

It is not possible for everyone to afford a course at a prestigious university. The internet has made it possible to access high-quality material from around the world. You can **build your own curriculum** using the available MOCs. The challenge here is that you must be highly disciplined in order to complete this task. If you decide to go this route, I recommend finding an accountability partner who will keep you on track.

If you would rather learn through practice rather than theory, you can build a portfolio of small experiments. In this way, you will gain experience in **building things in public**. You can also gain social proof for your knowledge and expertise this way.

## Build relationships

Opportunities come from people. You must connect with people who can open those magical doors of quick career advancement, no matter how you build your knowledge.

You should build relations with three sets of people:
1. Leaders of the field
2. Mentors
3. Peers

A leader in the field has a comprehensive understanding of the domain, has seen multiple cycles of hype, knows what works, and knows how to thrive in an uncertain environment. Their **approaches to challenging situations and frameworks to use** in those situations can be useful to you.

A mentor is someone who is a few steps ahead of you. They have walked the path you wish to follow. Their situations are similar to yours. They can guide you properly to **avoid the pitfalls** and guide you on the path that will accelerate your progress.

Peers walk alongside you on the journey. You can exchange notes with them on how they are handling the challenges. You must **be a giver in this peer group** instead of only taking.

## Conclusion

Our careers give us with a great deal of satisfaction. Making the right career choices is therefore crucial. In order to have a meaningful and impactful career, you should evaluate and change course during mid-career. When you decide on a career path, you can learn the intricacies of the career and cultivate effective relationships that will help you pursue it.

If you liked this post, can you please forward the post to your friends? And if you have any questions, either leave them in the comments or ask on Twitter.


## Continue Reading

* [What game are you playing?](/game-you-play/)
* [Season and situation for every advice](/advice-you-need/)
* [Work, Life, and Fun is a flywheel not a balance](/work-family-fun-flywheel/)
