---
title="Season and situation for every advice"
slug="advice-you-need"
excerpt="You need specific idea not a generic one"
tags=["wins","coach","insights"]
type="post"
publish_at="01 Aug 22 08:46 IST"
featured_image="https://cdn.olai.in/jjude/advice-you-need-gen.jpg"
---


There is a season and situation for every advice. 

All good things come to those who wait.
BUT
Time and tide wait for no man.

Look before you leap.
BUT
Strike while the iron is hot.

Don't cross your bridges before you come to them.
BUT
Forewarned is forearmed.

You don't need generic advice. You need specific idea for solving your current dilemma. That can come only from those who have walked your path. Seek them. Seek their advice.

## Continue Reading

* [What game are you playing?](/game-you-play/)
* [The curse of EVERYTHING and NOW on building your career](/all-and-now/)
* [If you see everything, you will get nothing](/focus-to-win/)

