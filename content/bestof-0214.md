---
title="Top Ten Articles Read In February"
slug="bestof-0214"
excerpt="February was a month for consumption. I read a lot. I'm listing the top 10 blog-posts with key-points."
tags=["insights"]
type="post"
publish_at="27 Feb 14 16:30 IST"
featured_image="https://cdn.olai.in/jjude/bestof-0214-gen.jpg"
---

February was a month for consumption. I read a lot. I don't know how many blog-posts I read but I have stored about 50 blog-posts in Evernote library. When I go through my stored articles library in Evernote, this number is high for a single month.

These blog-posts may not have been written in this month, since I queue reading to [Pocket](http://getpocket.com).

In addition to these articles, I also managed to read two books. The first one is '[Future Shock][11]' by Alvin Toffler and second one is '[Time Management 2.0: 15 Secrets of a Self-Made Millionaire for Getting Things Done][12]' by H. Reardon, C. Kane. Both of them were useful in their own sense. I will write a summary of these books, sometime in the future.

Here in this post, I'm listing the top 10 blog-posts with key-points:

 1. [Byron Wien’s 20 Rules of Investing & Life][1]: Luck plays a big role in life, and there is no better way to increase your luck than by knowing as many people as possible; On philanthropy my approach is to try to relieve pain rather than spread joy. Music, theatre and art museums have many affluent supporters, give the best parties and can add to your social luster in a community. They don’t need you. Social service, hospitals and educational institutions can make the world a better place and help the disadvantaged

 2. [Rheostats][2]: What we all need are rheostats that can both brighten and dim the lights (or the music) by degrees and by nuance. We have created single lenses, one-dimensional litmus tests, to decide our positions and who are our “friends.” That’s patently absurd, like having the sound set at a given level despite the music, or the lights at one setting despite the time of day.

 3. [It’s fine to get an MBA but don’t be an MBA][3]: Getting an MBA means you’re curious to learn broadly about theories and explore how these techniques can be applied to various businesses. Being an MBA means you think you’re getting taught the one right answer to problems – to a hammer everything is a nail – and that only MBAs know these dark arts.

 4. [When Product Features Disappear – Amazon, Apple and Tesla and the Troubled Future for 21st Century Consumers][4]: In the 20th century if someone had snuck into your garage and attempted to remove a feature from your car, you’d call the police. In the 21st century it’s starting to look like the normal course of business.

 5. [Be Proud of Your Accomplishments, Not Your Affiliations][5]: tangible accomplishments shows the world what you’ve actually done while de-emphasizing who accepted you into their organization. The latter is a superficial vanity device designed to boost confidence; the former is a validated, objective measure of your skills and experience.

 6. [The forest versus the trees][6]: The whole is much greater than the sum of its parts. Yet, this is only the case if the whole is well-designed.

 7. [Technology Merely Facilitates][7]: This problem stems from an overabundance of faith that technology, alone, grants competence - that a person or an organization who is completely unable to do something will be somehow transformed into an expert if the technology that experts use competently is made available to them.

 8. [How To Think][8]: Two of the most important executive functions are cognitive flexibility and cognitive self-control. Cognitive flexibility is the ability to see alternative solutions to problems, to think outside the box, to negotiate unfamiliar situations. Cognitive self-control is the ability to inhibit an instinctive or habitual response and substitute a more effective, less obvious one.

 9. [How Big is your Circle of Control?][9]: By deliberately limiting the irrelevant things you do and think about, you automatically become much, much better at the relevant things on which you spend your time.

 10. [A Message To Garcia][10]: What is Initiative? I'll tell you: It is doing the right thing without being told.

Want to know how I read so much? Read, [Build An Ecosystem For Learning](//jjude.com/build-an-ecosystem-for-learning/)

_Disclaimer: Links to books are amazon affiliate links_

[1]: http://www.ritholtz.com/blog/2013/07/byron-wiens-20-rules-of-investing-life/
[2]: http://www.contrarianconsulting.com/rheostats/
[3]: http://hunterwalk.com/2011/12/24/its-fine-to-get-an-mba-but-dont-be-an-mba/
[4]: http://steveblank.com/2013/11/21/when-product-features-disappear-amazon-apple-and-tesla-and-troubled-future-of-21st-century-consumers/
[5]: http://blogs.hbr.org/2012/04/be-proud-of-your-accomplishmen/
[6]: http://earlyretirementextreme.com/the-forest-versus-the-trees.html
[7]: http://jimshamlin.blogspot.in/2011/12/technology-merely-facilitates.html
[8]: http://www.farnamstreetblog.com/2014/02/how-to-think/
[9]: http://www.mrmoneymustache.com/2013/10/07/how-big-is-your-circle-of-control/
[10]: http://www.foundationsmag.com/garcia.html
[11]: https://amzn.to/33PsjJn
[12]: http://www.amazon.com/gp/product/B00FIPLWYK?ie=UTF8&camp=213733&creative=393177&creativeASIN=B00FIPLWYK&linkCode=shr&tag=jjude-20

