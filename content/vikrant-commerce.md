---
title="Vikrant Sukhla on 'Trends in Digital Commerce'"
slug="vikrant-commerce"
excerpt="From new business models to generative AI in Digital Commerce..."
tags=["gwradio","tech"]
type="post"
publish_at="26 Jul 23 16:46 IST"
featured_image="https://cdn.olai.in/jjude/vikrant-yt.jpg"
---
I have the privilege of having a seasoned digital commerce architect at Adobe as a guest. We discuss the trends that we already see and the ones that are shaping up in digital commerce. Hope you find this conversation useful.

<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless src="https://share.transistor.fm/e/823b4c53"></iframe>

## What you'll hear
- How Vikrant moved from running a Magneto agency to Adobe
- Difference between e-commerce and digital commerce
- Steps to move to digital commerce
- Business models of digital commerce
- New technological patterns in digital commerce
- How can IT Services companies stay ahead of the curve?
- Generative AI and digital commerce
- Kindest thing anyone has done?
- Best leadership quality
- Definition of living a good life

## Edited Transcript

### How Vikrant moved from running a Magneto agency to Adobe

It's been quite an interesting journey, especially since I am based in the Tri-City area of Chandigarh. My story goes back to 2015 when I made the decision to leave my job. At that time, I was working as the head of product engineering for a well-established organization in Chandigarh.

Interestingly, this was not my first venture into entrepreneurship. Back in 2010, I had already started my first startup which grew to a team of 18 people. However, circumstances led me to leave that venture within 16 months and return to the corporate world. But, as they say, once you get bitten by the entrepreneurship bug, it's hard to let go.

In 2015, I took the plunge again and started my agency called Envision E-Commerce. We experienced rapid growth and expanded to a team of around 110 people. Our focus was on Magento, and we became the third company in India to be Magento partners, working with enterprises, Fortune 500 companies, and major Indian e-commerce brands.

By 2018, I started to **feel burned out** from managing everything on my own. Despite the success, I lacked the exposure and knowledge on how to effectively manage it all. So, I decided it was time to move on. The good news was that the legacy of the company continued to thrive, and it remained a prominent name in the global digital e-commerce space. This was truly my legacy.

In 2019, while working with another well-known company as VP of E-Commerce, I received an offer from Adobe. They were starting a delivery practice in Bangalore after acquiring Magento in 2018. Since I had been involved with Magento since its beta days in 2007 and had dedicated myself to mastering it, it made sense to work with the mothership itself. I firmly believed that being the best at one thing rather than juggling multiple technologies and languages was the way to go.

Throughout my journey, I've come to realize the **importance of having an entrepreneurial mindset**, whether you are a developer or a product manager. It's essential to see beyond completing tasks and Jira tickets and understand how your efforts contribute to the overall business goals. This perspective allows you to deliver what truly matters to the success of the company.

### Difference between e-commerce and digital commerce

In the past, e-commerce merely consisted of websites where products were sold. However, with the widespread digitization and technological advancements worldwide, we now witness **personalization at every stage of the customer journey**. As a customer, your identity remains consistent across various touchpoints, whether you visit a retail store, use your mobile device, or interact with a website.

In the digital landscape, customers are considered valuable assets, and this concept is referred to as a Customer Data Platform (CDP). **CDPs enable stitching together of a customer's identity, treating them as a single entity, regardless of the digital platform they engage with**. Comparing the present to five years ago, even loyalty programs in major enterprises were not as integrated across different platforms.

Take, for example, one of Adobe's customers, a renowned lifestyle brand in Australia. They embarked on a digital transformation journey using various Adobe products. Presently, when you browse their website, they can recognize you as a logged-in user. When you pass by their retail store, you receive notifications on your app, informing you about a 20% discount coupon for the specific product you were browsing.

Once you step into the retail store, the staff will have access to your customer identity, which includes your email ID and phone number. For instance, if you were interested in black shoes of size eight, they will be aware of your preference. As a result, you won't need to inquire about the availability of such shoes. Instead, the retail executive will approach you directly, addressing you by name, and offer assistance tailored to your requirements, demonstrating a personalized and seamless experience.

This level of personalization and integration exemplifies the transformation of the complete digital economy. It showcases how various aspects of the digital world come together to create a cohesive digital commerce experience, ultimately enhancing customer satisfaction and loyalty. This paradigm shift in business operations is commonly referred to as digital transformation, digital transactions, or the digital economy.

### Steps to move to digital commerce

I come from a background of extensive consulting experience, which has shaped my approach when working with customers. When a customer presents a business plan or a growth strategy for their company, I first inquire about their short-term, long-term, and very long-term goals. Understanding their vision for the business is essential. This is particularly relevant for businesses that have a history of traditional brick-and-mortar operations but are now exploring digital transformation and e-commerce.

In India, there are many family-owned businesses undergoing this transition, aiming to leverage digital assets and monetize them. For some of them, e-commerce was previously seen as a minor sales channel, perhaps comprising only a small percentage of their overall business. However, the COVID-19 pandemic brought about a significant shift. It acted as an unexpected blessing for the e-commerce industry, compelling businesses to take a massive leap forward in e-commerce growth due to limitations on physical retail operations.

I would engage the customer in a series of counter questions to better understand their objectives. For instance, I would ask about the volume of online business they aim to achieve. **While a goal of generating millions of dollars online is commendable, it is crucial to consider the foundational aspects that underpin successful e-commerce operations**.

Questions about logistics, supply fulfillment, manpower, delivery processes, and returns management need to be addressed. Often, businesses, including SMEs, MSEs, and even Fortune 500 companies, **focus on the ambitious revenue targets without fully planning and integrating the backend systems required to support the e-commerce platform**. Seamless integration between the e-commerce platform and the order management system is just one example of the complexities that need attention.

At Adobe, we understand that every customer is unique, even if they belong to the same industry or sell similar products. Their business logic, fulfillment processes, and overall e-commerce journey can differ significantly. As a result, assisting businesses in this transformation requires a thorough examination of the fundamental elements and aligning them cohesively to achieve the desired outcome for each brand.

### Business models of digital commerce

Throughout my extensive 16-plus years of experience in e-commerce, I've encountered various flavors of this domain. Let me break them down:

1. One-to-Many Model: In this common model, a single brand or company establishes its e-commerce presence, typically through a mobile app or website, to sell products to many customers. Nowadays, we have tools like PWAs (Progressive Web Apps) and Adobe's AEM and PWA Studios, enabling businesses to create mobile-responsive websites easily.

2. Marketplace Model: An example of this model is Amazon, though there are others like eBay, Flipkart in India, and Lazada in Southeast Asia. In a marketplace, it's a many-to-many scenario, where multiple sellers offer their products to numerous buyers. Various submodels exist within this category, such as commission-based or partnership-based setups.

3. B2B E-commerce Transformation: The COVID-19 pandemic has catalyzed a significant transformation in B2B e-commerce. Traditionally, B2B warehouses or manufacturing warehouses operated with a considerable workforce manually handling orders and contracts. However, due to the pandemic, businesses have shifted towards digital solutions. For instance, a major medical supplier transitioned to a digital presence where vendors could place orders directly, necessitating complex B2B logics for seamless operations.

4. B2B2C Model: In this model, a company (e.g., Company X) produces goods, and wholesalers across different states purchase these goods. The wholesalers, in turn, sell to end customers who can either make digital purchases or buy from physical shops. As we move from B2B to B2C or B2B to B2B, the complexity of operations increases.

For instance, B2B e-commerce involves contract agreements between two companies, with several layers of processes for procurement, approvals, financial calculations, and payment fulfillment. In the past, these operations relied heavily on human intervention, but platforms like Adobe Commerce now enable automation and integration of these processes within a single product, facilitating the digital transformation of B2B businesses.

These diverse e-commerce models keep e-commerce professionals continuously engaged, navigating the complexities of each unique approach.

### New technological patterns in digital commerce

When discussing Adobe Commerce, it's essential to acknowledge the evolution of e-commerce platforms. Initially, Magento started as a monolithic product, a common approach at the time. However, in recent years, the concept of composable or microservices architecture has gained traction, allowing for greater flexibility and scalability.

When Adobe acquired Magento in 2018, the objective was to transform it into a microservices-based architecture with an API-first approach. Although Magento already had a modular structure with various modules for different functionalities, the goal was to enhance its capabilities to cater to enterprise-level requirements and make it more adaptable, useful, and scalable.

Today, Adobe Commerce maintains the core modules while introducing various microservices such as catalog, search, and pricing. This provides customers with the flexibility to choose the microservices that align with their business logic, making the product highly composable. The frontend layer can be coupled or decoupled, with options to use Luma team, PWA Studio, AEM, or other front-end technologies, along with GraphQL for frontend connections.

Adobe Commerce offers features like the App Builder and API Mesh, acting as lightweight middleware to connect with ERP systems, shipping gateways, and WMS based on specific business needs.

It's essential to distinguish between two available products in the market. One is Magento Open Source, which remains the open-source version without the added microservices capabilities. The other is Adobe Commerce, which can be deployed on-premises or on the cloud. As a cloud solution architect, advocating for Adobe Commerce Cloud is preferred due to its advantages.

The flexibility of Adobe Commerce is evident in various use cases. Some banks in India use it as the backend to handle card fulfillment, marketplace modules, checkout, and payments. Additionally, prominent automotive companies leverage Adobe Commerce for online car bookings and customer car configuration.

The true power of Adobe Commerce lies in its adaptability, offering different customers diverse options to suit their unique requirements. While some customers may opt for a monolithic setup, others can embrace the composable architecture. Choosing the right architecture is crucial, as it can determine the success or failure of an e-commerce implementation.

### How can IT Services companies stay ahead of the curve?

I believe that services are not going to disappear. From my experience with Adobe Commerce, I can see that simply providing vanilla Magento or vanilla Adobe Commerce doesn't cater to every customer's needs. Customization is often required, and this is where service companies play a crucial role.

In the past, when I ran my own SI (System Integrator) company, we used to have eight hours of work, but I made it a rule for my team to work only seven hours. The remaining one hour was dedicated to personal learning. I encouraged them to come up with a weekly learning plan, and we would connect at the end of the week to review their progress.

I've noticed that many developers complain about not having enough time to learn new things because they are constantly working long hours on the same tasks. This concerns me because the world is evolving rapidly. For instance, generative AI has emerged in the last year, and it's already changing the landscape of various fields, like content creation using tools like Photoshop.

As an SI or an individual like myself, it's crucial to evolve and adapt to these technological changes. I firmly believe that I am responsible for my own growth and sustainability in this ever-changing world. **My motto has always been to embrace new tools and technologies and become proficient in using them**.

For example, take Chat GTP. While it's a useful tool, knowing how to efficiently use it can make a significant difference in the results you achieve. Learning the right prompts and giving precise instructions can produce much more tailored and inspiring content.

During my 16 to 17 years working with Magento, I've heard numerous times that Magento is on the verge of dying. However, it's still here and evolving. This reminds me that technology is always changing, and as an SI or an individual, we must constantly evolve with it.

I firmly believe that there is an abundance of opportunities for all SIs, and they can coexist in the market. However, the key to success lies in how quickly you adapt to new developments and how open you are to embracing change. Relying on past experiences alone won't suffice, and we must consistently learn and strive to produce our best work every day.

Therefore, I consider continuous learning and adaptation as vital aspects of staying relevant and ensuring growth in this dynamic industry.

### Generative AI and digital commerce
Allow me to present a compelling example without revealing any specific names. We have a customer who operates as a global enterprise, serving 26 different countries with their commerce presence. However, the challenge arises when it comes to creating design content for their website, marketing, and branding across all these regions. Managing 26 designers, along with the necessary localization and managerial support, becomes quite impractical and resource-intensive.

To address this issue, generative AI steps in as a game-changer. Adobe, for instance, has recently introduced "Firefly," a tool that allows users to input prompts and receive relevant images, streamlining the creative process. This AI-powered technology enables brands to produce content at a faster pace and with greater efficiency compared to traditional methods.

The impact of generative AI is not limited to images alone. It extends to content creation as well. Companies, like Buzzfeed, and others in the content space, are already embracing this technology to replace human-generated content. The speed and cost-effectiveness of AI-driven content creation are revolutionizing the market.

Consider an example where a brand aims to promote a car on a beach with a model. Traditionally, arranging such a photoshoot would involve numerous logistical and human resources, spanning several days. However, with generative AI, a realistic image can be generated in mere minutes using the right prompts. Additional touch-ups in tools like Photoshop further refine the picture, producing results comparable to a week's work within one working day.

While this automation certainly enhances efficiency, it may raise concerns about potential job losses. Nevertheless, it's essential for individuals and businesses to embrace this technological shift and remain at the forefront of developments rather than lagging behind and getting adversely affected.

With the digital commerce landscape rapidly evolving, understanding and learning about generative AI and similar technologies are critical for staying relevant. Being adaptable and proactive will enable professionals to thrive in the dynamic market, especially as newer AI models continue to emerge.

In the realm of digital commerce, Adobe Sensei, an underlying AI technology, has been leveraged to provide personalized search results for customers. Based on the user's browsing patterns, the search page adapts to display products of interest, making the shopping experience more tailored and efficient.

As AI continues to influence various aspects of commerce and marketing, businesses must embrace these advancements to unlock their true potential and achieve unparalleled success. By integrating generative AI and other AI-driven functionalities into their strategies, brands can streamline their processes, create engaging content, and cater to customers' personalized needs like never before.

### Rapid fire questions
### Kindest thing anyone has done?

Back in the year 2008, I was actively engaged in writing blogs. During that time, I wrote a blog that revolved around a political situation. I was quite enthusiastic about expressing my thoughts through writing, and I posted this particular blog on LinkedIn and some other platforms.

To my surprise, I received feedback from a gentleman whom I didn't know personally. He reached out to me on LinkedIn and commended me for writing an excellent blog. However, he also posed a thought-provoking question: "Have you considered where you see yourself when you are 45 or 50 years old?" This simple act of reaching out and offering valuable insight struck me as one of the kindest things someone has ever done for me.

The reason it left such an impact was that the gentleman made me realize the significance of expressing myself on a political matter while maintaining a logical and well-reasoned approach, even though it might not have had a direct impact on my career.

His gesture made me ponder whether I was truly focused on my goals. This random act of kindness became a turning point for me, leading me to be more focused in both my career and personal life.

As a result, I have embraced objectivity in everything I undertake, striving to stay focused and purposeful. While I continue to write blogs, this experience has shaped my perspective and approach to various aspects of my life, and I am truly grateful for the unexpected guidance from a stranger, which has had a lasting positive influence on me.

### Best leadership quality

I believe that an essential quality of leadership is **being a Servant leader**. I learned this valuable lesson from my very first boss, whom I consider myself fortunate to have encountered in my initial IT organization. Let me provide some background: I graduated as an electronics communication engineer and embarked on my career journey with my first job at Tel. Afterward, I made a move to an IT startup in Noida in the year 2007.

During my time at this startup, my first boss left a lasting impression on me. He possessed a wealth of knowledge and had previously worked at Microsoft, even coding projects in Excel. What struck me most about him was his approach to decision-making. Whenever we faced a particular requirement or had ideas to contribute, he took the time to genuinely listen to each one of us before arriving at a decision. At times, I admit I felt a bit frustrated, wondering why he needed our input if he would ultimately make the decision himself.

However, as I reflect on those experiences now, I realize the genuine leadership displayed by my boss. He was not wasting our time; rather, he was striving to understand all perspectives before making well-informed decisions. In essence, he exemplified the qualities of a Servant leader, placing importance on comprehending the viewpoints of his team.

This profound lesson on leadership has stayed with me throughout my career. As a leader myself, I make it a point to adopt this same approach and strive to be a Servant leader. Understanding all aspects and perspectives before making decisions is an integral part of my leadership style. I find it essential to pass on this valuable leadership lesson to the people I work with, fostering a culture of genuine understanding and collaboration within the team or organization.

### Definition of living a good life

This is a very tough and complicated question because, for me, success doesn't solely revolve around monetary achievements or reaching a particular position. It's not about being recognized as "40 under 40," "30 under 30," or "50 under 50." My definition of a fulfilling life is one where I have sufficient financial security to avoid constantly worrying about money.

My idea of a good life is achieving financial freedom, allowing me to have ample time for myself and my family. It also means having the opportunity to give back to the community in any way I can, whether through donations, volunteering my time, or simply being a responsible and compassionate citizen.

In essence, a good life is about finding a balance in everything. This concept reminds me of the Wheel of Life, where we assess and chart different aspects of our lives, such as career, romance, social life, and health. The key is to strive for a well-rounded and balanced existence, where no single aspect overshadows others.

Therefore, my idea of a good life is to maintain harmony across all areas, ensuring that I am not too focused on just one aspect while neglecting others. It's something I aim to practice every day to find contentment and fulfillment in life.

## Your feedback counts

Thank you for listening. If you enjoy the podcast, would you please leave a short review on Apple podcast or on YouTube? It takes less than 60 seconds, and it really makes a difference in finding this podcast. And it boosts my spirits. 😀

I would like to hear what resonated the most with you in this episode. Either you can email me or share it on social media and tag me. Thank you for your support.

Have a life of WINS.

_Gravitas WINS Radio is produced by [Joseph Jude](https://jjude.com/). Be sure to subscribe on [Apple Podcast](https://podcasts.apple.com/in/podcast/gravitas-wins-radio/id1562802449), [Spotify](https://open.spotify.com/show/7LOBjAyjBkechMC0xMIj9E),[Google Podcast](https://podcasts.google.com/feed/aHR0cHM6Ly9mZWVkcy50cmFuc2lzdG9yLmZtL2dyYXZpdGFzLXdpbnMtcmFkaW8=),[YouTube](https://www.youtube.com/gravitaswins), or the podcasting app of your choice._

Please check out other episodes of [Gravitas WINS Radio](/gwr-themes/). For easy consumption, they are categorized by themes. If you are a fellow podcaster, you might be interested in [How I Podcast](/how-i-podcast/) or [Tools I Use](/i-use/).

## Connect with Vikrant
- LinkedIn: https://www.linkedin.com/in/shuklavikrant/

## Connect with me
- Twitter: https://twitter.com/jjude
- LinkedIn: https://www.linkedin.com/in/jjude/
- Website: https://jjude.com/
- Newsletter: https://jjude.com/subscribe/
- Youtube: https://youtube.com/gravitaswins
- Email: podcast@jjude.com
- Executive Coaching Program: https://gravitaswins.com

## Continue Reading
* [Jinen Dedhia on 'Building enterprise applications with Low code tools'](/jinen-dronahq/)
* [Liji Thomas on ‘Conversational Bots’](/lijit/)
* ['Building An Enterprise Data Strategy' with Ganesan Ramaswamy](/ganesanr/)
