---
title="What's On This Site"
slug="sitemap"
excerpt="Sitemap of jjude.com"
tags=["sitemap"]
type="page"
publish_at="11 Nov 14 06:40 IST"
featured_image="https://cdn.olai.in/jjude/sitemap-gen.jpg"
---

**Popular Posts**

* [Why It Hurts To Be An Indian?](/hurts2bindian/)
* [Focus On What Matters Or Sleep Alone](/focus-on-what-matters-or-sleep-alone/)
* [What Is Your Ideal Job?](/ideal-job/)
* [How to be Effective as a New Manager](/how-to-be-effective-as-a-new-manager/)
* [Smart people ask for help. Do you ask for help?](/smart-people-ask-for-help-do-you-ask-for-help/)
* [Documenting Your Decisions](/documenting-your-decisions/)

You can access all posts via [Archives](/posts/).

**Blog Series**

* [Self-Directed Learning](/sdl/)
* [Problem Solving](/be-a-problem-solver/)
* [Books Read](/books/)

**Subscribe**

* Via [Newsletter](/subscribe/)
* Via [RSS](/feed.xml)

