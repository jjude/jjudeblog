---
title="Impact of digital technologies in breaking barriers"
slug="impact-of-digital"
excerpt="Digital technology is subverting hierarchies"
tags=["talks","coach","tech"]
type="post"
publish_at="23 Sep 20 09:00 IST"
featured_image="https://cdn.olai.in/jjude/impact-of-digital-gen.jpg"
---


_The United Nations is celebrating its 75th anniversary with a global dialogue on the impact of digital technologies on our lives. I was a panelist in an event conducted by a local non-profit organization affiliated to the UN. Here is the transcript of my speech._

If we conclude based on daily news, we would say we live in an awful period in history. Each day brings terrible news of rapes, murders, genocide, and poverty. I do not deny any of it. These are so obvious. What we are oblivious is that **we are also living in a golden era**.

We are editing the human genome, going to space, and coming back as if we are on a picnic, and printing houses and human organs. Today a poor in a slum has better tools than kings of the previous century. These are either miracles or magic or just technological wonder.

Let me talk specifically about the impact of software technology. **The software has the same impact on us as money and writing**. Both money and writing are flexible. We can write on the sand, stone tablet, or a digital tablet. Similarly, money can be stored in coins, notes, or cards. The software works the same way. It can be in a huge mainframe computer or on a tiny device collecting data from Mars.

There is a manifesto called Cluetrain manifesto. One of the thesis of the manifesto summarises the theme of my talk. It says **hyperlinks subvert hierarchy**. Hyperlinks, as you know, means Internet-based technologies. In the last decade, hyperlink based technologies have indeed subverted all and every hierarchy.

Let me illustrate that with an obvious example. I don't think any political leader understands the power of digital technology better than our PM Modi. Until Modi came to the scene, the news media shaped and reshaped political careers. But **Mr. Modi subverted the hierarchy of the mainstream media**. Using hyperlink based technologies like social media, he reaches out to his constituents directly. He doesn't need an intermediary. Whether you subscribe to his style of politics or not, there is a lot you can learn from his adaption of technologies.

After a few years of my graduation in 94, I joined a software firm. I had to write a program to create a pop-up window. To learn how to do it, I had to go to the British Library, borrow a fat book, and type 100 lines. All of this took me a day. Today I can google my question, copy, paste the answer, and do it in ten minutes. I can learn from the best of the minds, either of the past or the current, via the internet. Stanfords and IITs are throwing their lecture videos on the net. There is no shortage of ideas on the net. **Today, we are all sitting in the comfort of our homes and learning from the best of the minds**. All thanks to digital technologies.

Talk about social changes. I suppose you order food from Swiggy or Zomato. When the delivery boy comes, do you ask his caste? His family background? His eating habits, maybe? This was unimaginable a few years back. A lot of us were conscious of social hierarchies. We were careful about who delivered us food and who drove us. Hyperlink based digital technologies thwarted all of that. **Technology is not only changing our social perspectives but also provide a livelihood for thousands of people**. They have opportunities that were not accessible to them earlier. 

Like all inventions, **digital is a double-edged sword**. As much as it arms us with the power to better ourselves and society, digital can easily make our lives miserable. That's the reason why dialogues like this are essential. We need to spread knowledge, debate their impact, and shape the future together. 

### Read Next
 
 * [How to deliver value in the digital age?](/value-in-digital-age/) 
 * [One decision instead of hundred decisions](/lead-decisions/)   
 * [Principles trump processes](/principles-trumps/)
