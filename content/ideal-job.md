---
title="What Is Your Ideal Job?"
slug="ideal-job"
excerpt="Have you paused to think what your ideal job would be?"
tags=["coach","gwradio"]
type="post"
publish_at="10 Mar 11 16:30 IST"
featured_image="https://cdn.olai.in/jjude/ideal-job.png"
---
<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless src="https://share.transistor.fm/e/dc07fbc9"></iframe>

Most of us slide from college life into office grind without a deep thought about career. Then after years under the grind we wrongly conclude everyone is paid to be unhappy.

The problem is we don't know what we expect out of our jobs. Most feel satisfied if they are paid relatively higher than the one is sitting in the next cubicle.

Have you paused to think what your ideal job would be?

When you define, you strive to it.

I define my ideal job as one having all of the below factors. Not just one or two, but all. That is what makes it ideal.

![Ideal Job](https://cdn.olai.in/jjude/ideal-job.png "Ideal Job")

*   **Stimulating Work** : I enjoy those assignments where I apply my knowledge but I enjoy more when there is an opportunity to learn new things. There is a fine line between being stretched and not broken. That tension between being stretched and being a failure is what makes an assignment challenging and thus enjoyable.

*   **Supportive Boss** : Corporate structure is hierarchical and our immediate supervisor is an important link in that structure. If he is a variant of the [pointy-haired boss](https://en.wikipedia.org/wiki/Pointy-haired_Boss), life becomes irritating and awful. On the contrary, it is a pleasure to work with those who give you enough freedom and offer required support to make you and them a success.

*   **Co-operative Team** : Despite the hype built around leadership, successful leaders aren't loners. Behind every triumphant leader, there is a hard-working team. Through the highs and lows of the assignment, it is the team that makes the journey joyful.

*   **Appreciative Clients**: Often a single appreciation from the client can make you forget all the travail of the assignment, especially if the recognition is public. Not many clients do it but when done right, you are energized and ready to ensure success of the assignment.

*   **Flexible Hours** : 9 to 5 work hours is a terrible inheritance from industrial revolution. Ideas don't come within a fixed block of time and flexible hours doesn't mean lethargy. It just means I am free to work when I work the best.

*   **Commensurate Compensation** : Some would consider the pay pie to be bigger than others in the circle of 'ideal job'. Not me. I realized long back, money is essential but not the only necessity in life. Pay package should commensurate with the outcome. If not it leads to irritation, not joy at work.
The list may seem like a castle in the air. But I have been lucky enough to live in such a castle every now-and-then.

### Quote To Ponder

_If you look forward to going to the job every morning, you have a great job - Rochak Chauhan_

## Continue Reading

- [How to avoid failures with thirty percent feedback?](https://jjude.com/30-percent)
- [11 Lives to Build a Rich Career](https://jjude.com/11-lives)
- [Building Your Career - Apple Or Amazon Way](https://jjude.com/apple-amazon-career)