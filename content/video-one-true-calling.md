---
title="Video to watch - Why Some of us Don't Have One True Calling"
slug="video-one-true-calling"
excerpt="Do you have range of interests? The world needs you."
tags=["sdl","video"]
type="post"
publish_at="01 Jun 18 10:30 IST"
featured_image="http://cdn.olai.in/jjude/one-true-calling.png"

---

I want to learn about different cultures. I want to know what makes them unique, what they share with other cultures and how their theological thinking help them cope with life's major events.

That's just one of my many interests.

- I want to practice photography. 
- I want to sketch. 
- I want to write.

Even in my software career, 
- I am interested in software development, 
- application security, and 
- devops. 

Oh, before I forget, I also want to be an investor (specially value investor).

I know. I know. I'm many.

The conventional wisdom recommends,**focus**. Focus means choosing. Focus means exclusion. Focus means a dream remains unfulfilled.

So far, I have run with the conventional wisdom. I chose to focus on software development; I haven't picked up my camera for two years;

Sometimes, I hear the silent weeping of my heart.

As I was listening to Emilie, I could feel a sense of relief spreading to my inner-self. Breaking of bondage, so to speak.

[![One true calling](http://cdn.olai.in/jjude/one-true-calling.png)](https://youtu.be/QJORi5VO1F8 "One true calling")

Emilie calls such people who pursue diverse pursuits as **multipotentialites**. She mentions three super-powers of multipotentialites:

1. **Idea Synthesis** - combining two or more fields and creating something new at the intersection
2. **Rapid Learning** - less afraid of trying new things and stepping out of comfort zones
3. **Adaptability** - the ability to morph into whatever you need to be in a given situation

She says:

> As a society, we have a vested interest in encouraging multipotentialites to be themselves. We have a lot of complex, multidimensional problems in the world right now, and we need creative, out-of-the-box thinkers to tackle them.

I agree with her. When you become a chief executive of a company, every issue you face is complex and multi-dimensional. To solve these problems, you need ideas from diverse areas.

This complex world needs more multipotentialites. If you are one, nurture all of those interests.
## Continue Reading
- [11 Lives to Build a Rich Career](/11-lives/)
- [Intentional Imbalance](/imbalance/)
- [The curse of EVERYTHING and NOW on building your career](/all-and-now/)