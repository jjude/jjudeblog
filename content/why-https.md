---
title="Benefits of https"
slug="why-https"
excerpt="If your site is only static site should you move it to https? Will moving to https impact site's seo? Read to find out."
tags=["tech","security","martech"]
type="post"
publish_at="24 Nov 16 17:45 IST"
featured_image="https://cdn.olai.in/jjude/why-https-gen.jpg"
---

Only [24% of top 25000](https://prudentdevs.club/25k-site-audit/) sites are on https.

Top content marketing experts like [Neil Patel][1] and [Brian Dean][2] still host their sites on http. Even homepages of [BBC](http://www.bbc.com/), and [Best Buy](http://www.bestbuy.com/) are on http.

This has raised a valid question - What are the benefits of https?

### Benefits

### Prevent man in the middle attack

First and foremost benefit is, **https prevents man-in-the-middle attack**. If you wonder, what that is, don’t worry. I too don’t understand the technical details of that phrase. But I understand the impact and that’s all that matters.

Let us see it from the perspective of a static blog, like this one. All I have in this site are plain HTML pages. There are no forms and so no userids and passwords sent to any server. Even then, https is a good deal, for a simple reason.

Of late, ISPs have started to inject their ads on webpages. I am on [BSNL](http://www.bsnl.co.in/) and I have seen those ads while browsing through the sites. It is not only BSNL. [ISPs around the world](http://www.infoworld.com/article/2925839/net-neutrality/code-injection-new-low-isps.html) are doing the same.

![penalty of http](https://cdn.olai.in/jjude/2016-bsnl-ads.jpg "BSNL Ads even on https sites")

There is no way for you to know that your readers are seeing these ads. There won’t be any indication in your server logs. Yet, your readers will hold you responsible for those ads (rightly so!).

Thankfully BSNL serves ads only about themselves. But it could be worse. They could inject ads for your competitor!

When they can serve ads, they can serve anything — like malware.

This is from a static site perspective. If your site is powered by a blog engine like Wordpress, you should move to https.

### Improve your rank

There is another benefit for blogs to move to https. Google [announced](https://webmasters.googleblog.com/2014/08/https-as-ranking-signal.html) **HTTPS will be as a ranking signal**. "For now it's only a very lightweight signal", says Google, but "over time, we may decide to strengthen it". Today is the right time to move to https.

### Concerns

### Performance Penalty

You might have heard, https impacts performance. It used to be, not any more.

KeyCDN [analyzed](https://www.keycdn.com/blog/https-performance-overhead/) the performance of http and https and concluded that there are **no performance penalty for https**. So there is no reason to stay at http.

### Adsense

If you serve ads using google adsense, you might have another concern: Google may not serve its ads on https. Again, it used to be, not any more. Google Adsense FAQ [says](https://support.google.com/adsense/answer/10528?hl=en) they serve ads over https.

### Cost of https

Of course, there is a cost to migration. I wrote about my experience in moving this blog to https. If you run a blog, you should [read](https://prudentdevs.club/cost-of-https/) that post too.

Do you still have questions? [Tweet](https://twitter.com/jjude) to me. Or leave a comment.

Move to https, now.

[1]: http://neilpatel.com/
[2]: http://backlinko.com/

