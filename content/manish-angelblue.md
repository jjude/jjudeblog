---
title="Manish Verma on 'How Angel Investors Think?'"
slug="manish-angelblue"
excerpt="If India has to grow, we need lot of startups and angel investors to fund them. Let us find out how these investors think."
tags=["gwradio","coach","wins"]
type="post"
publish_at="28 Nov 23 15:05 IST"
featured_image="https://cdn.olai.in/jjude/manish-verma-yt.jpg"
---
If India has to grow, we need lot of startups and entreprenuers. Not all of them will be privileged to bootstrap their ideas. That is where angel investors come in. Today I have my friend and an angel investor, Manish Verma. He has been a banker, operator, and investor. So he knows all angels of running a business. We are going to discuss how to get funded from angel investors. 

<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless="" src="https://share.transistor.fm/e/b237e3f9"></iframe>

## What you'll hear
- How Manish got into angel investing
- Overall investment philosophy of Angel Blue
- How can startups get funded from Angel Blue
- PERSISTENT framework to evaluate startups
- How to value startups
- Let's talk about one recent investment
- Strategy for exit
- How to invest via Angel Blue in startups
- Challenges for Indian startups
- What is the kindest thing anyone has done for you?
- Best leadership quality
- Definition of living a good life


## Edited Transcript
__coming soon__

## Watch on YouTube
<iframe width="560" height="315" src="https://www.youtube.com/embed/A7BX_Qk4BnA?si=Qo3UHm2FNe0zpJzx" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

## Connect with Manish Verma
- LinkedIn: https://www.linkedin.com/in/manish-verma-18a3912/

## Connect with me
- Twitter: https://twitter.com/jjude
- LinkedIn: https://www.linkedin.com/in/jjude/
- Website: https://jjude.com/
- Newsletter: https://jjude.com/subscribe/
- Youtube: https://youtube.com/gravitaswins
- Email: podcast@jjude.com
- Executive Coaching Program: https://gravitaswins.com

## Your feedback counts

Thank you for listening. If you enjoy the podcast, would you please leave a short review on Apple podcast or on YouTube? It takes less than 60 seconds, and it really makes a difference in finding this podcast. And it boosts my spirits. 😀

I would like to hear what resonated the most with you in this episode. Either email me or share it on social media and tag me. Thank you for your support.

Have a life of WINS.

_Gravitas WINS Radio is produced by [Joseph Jude](https://jjude.com/). Be sure to subscribe on [Apple Podcast](https://podcasts.apple.com/in/podcast/gravitas-wins-radio/id1562802449), [Spotify](https://open.spotify.com/show/7LOBjAyjBkechMC0xMIj9E),[Google Podcast](https://podcasts.google.com/feed/aHR0cHM6Ly9mZWVkcy50cmFuc2lzdG9yLmZtL2dyYXZpdGFzLXdpbnMtcmFkaW8=),[YouTube](https://www.youtube.com/gravitaswins), or the podcasting app of your choice._

Please check out other episodes of [Gravitas WINS Radio](/gwr-themes/). For easy consumption, they are categorized by themes. If you are a fellow podcaster, you might be interested in [How I Podcast](/how-i-podcast/) or [Tools I Use](/i-use/).

## Continue Reading

- [Sohail Khan on 'Why startups fail'](/sohail-tbc/)
- [Arvindh Sundar on 'Gamification of marketing for small businesses'](/arvind-gamify/)
- [Sukhwinder Singh on 'Mantras for going from ₹1200 to ₹1 Crore of sale'](/sukhwinders/)