---
title="Can we truly sing ‘Joy to the world’?"
slug="can-we-truly-sing-joy-to-the-world"
excerpt="I only dream of a theological revolution that breaks the shackles of religion for all people. Then we can truly sing, 'Joy to the world'."
tags=[""]
type="post"
publish_at="24 Sep 08 16:30 IST"
featured_image="https://cdn.olai.in/jjude/can-we-truly-sing-joy-to-the-world-gen.jpg"
---

Every 'Holy Scripture' strikes a claim that it is either written by god or inspired by one. Believing such a claim, its followers have interpreted its text literally, ignoring the cultural context and generational gaps leading to holy wars, blood baths and genocides.

Even today such interpretations let loose hell-on-earth in New York, Jerusalem and even in Bangalore.

Too often we hear such statements in godly discourses.

&quot;My God is the only true God&quot;

&quot;Hmm...Who says so?&quot;

&quot;The book says so&quot;

Well, the other book says the same about the other god!

Whenever I hear such arguments, my mind goes back to childhood days. It is not uncommon to hear similar statements in kid's conversations.

&quot;My Dad is the strongest&quot;

&quot;Who says so?&quot;

&quot;My Mommy says so&quot;

When the other kid runs out of argument points, he punched you on your face. With a swollen face you get home, only to find that your dad slipped in the bathroom and lying on bed.

You wondered if your dad was really the strongest!

Religious wars are no different.

&quot;Only through my God, one can go to heaven&quot;

&quot;Any one killing unbelievers will enter heaven&quot;

Bang!

You got burnt villages, mass-murdering mobs and polarized countries fighting with one another.

All the while, gods were taking a nap after slipping in the bathroom!

Such comedy has caused centuries of tragedy. Will there be a remedy? I'm not that naive to think that a simple blog entry can bring in a solution to this age-old problem.

I only dream of a theological revolution that breaks the shackles of religion for all people. Then we can truly sing, 'Joy to the world'.

