---
title="Insights From Startup Expo @ ISB, Mohali"
slug="gew2014-expo"
excerpt="A crash course on everything startups need to know."
tags=["coach","startup","insights"]
type="post"
publish_at="23 Nov 14 22:26 IST"
featured_image="https://cdn.olai.in/jjude/isbexpo.jpg"
---
My co-founders at [DSD][1] are top engineering graduates from [Baddi University][2] & [Kurukshetra university][3]. As recent technology graduates, they didn't have enough knowledge on business development areas like sales or marketing. True to startup DNA, we were looking for a frugal and a quick way to fill that gap.

That opportunity came in the form of startup Expo organised by YI Chandigarh at Indian School of Business, Mohali on 21st & 22nd of November. We took a bet and registered the entire team for the expo.

Nothing brings satisfaction to startup founders than high return on talent investment.

It was a crash course on product positioning, talent acquisition, raising funds and agile thinking. All the speakers were entrepreneurs, so sessions were full of practical examples. The best thing was that every speaker was available for discussion throughout the day.

The highlight of the program was the stall expo. There were about 20 startups who participated in the stall expo. We got a chance to pitch to speakers, established entrepreneurs and venture capitalists.

Following are the points I noted from various sessions. These are not all the points I noted; just those that are relevant to startup founders. I'll elaborate many points in later posts. Subscribe to my newsletter to get every post I write.

![DSD team at ISB Startup Expo](https://cdn.olai.in/jjude/isbexpo.jpg)

**Kiran Karnik, keynote speech**

 - There are three factors about future: We can predict it; manage it or shape it. Startups and its founders should shape the future.
 - A company needs both generals and soldiers: generals to set the strategy and soldiers to carry out the strategy.
 - A manager works within a context; a leader changes the context. Say the task is to shorten a line without touching it. A manager will consider the situation fixed and conclude that the task impossible. A leader will change its context, by drawing a longer line next to it.
 - Indians are good at contextual advice because their language is so. In Hindi, same word is used for yesterday and tomorrow. The meaning depends on the context.
 - Indian culture is diverse unlike Japanese culture which is homogenous. Indians are exposed to different dressing, cuisines and languages. This encourages out-of-the-box thinking. When there is a traffic jam, you will notice bikers taking every possible avenue to move forward, including going on the side paths.

**Atul Khosla on Raising Funds**

- Everything you do in your company is a journey towards increasing capital and talent.
- What do investors look in a startup?
    1. Goal : Do you have a big, hairy audacious goal?
    2. Scale: Investors want to multiply their money. Money multiplies only if your business scales. Show investors how your business can scale.
    3. Governance: Investors want to be sure they are giving money to a trustworthy team that will not squander the money. A great advisory board and a clean balance sheet are two ways to show good governance to investors.
    4. Talent: Do you have quality talent in your company to achieve the audacious goal.
    5. Differentiation: What makes you better than the others in the same field?
- Investors fear that they are saying no to the next big thing. Play to that fear.
- Some ideas on how can you get quality talent into your startup:
    1. Access part-time talent
    2. Give equity

**Prof Siddhart Singh on Marketing**

- Value = Benefit - Cost
- Value is a 3-dimensional factor consisting of economical, functional & psychological.
- Economical isn't only about price but includes factors like productivity gains of customer.
- Psychological benefits sustain customers. They increase the switching costs.
- Nobody can copy total customer satisfaction, while everything else like product design can be copied
- How to increase perceived value? Increase benefits or reduct cost.
- Reducing cost is not just a function of price. Think about the total cost that customer incurs to get your product. Cost for customer includes getting information, acquiring, delivery and disposal of your product. Can a customer get your product delivered to his home rather than going to a store? If so, you have reduced his cost.
- If you focus on transaction price alone, you are already lost.
- There is no complex definition of market. Market is bunch of people having a need.
- Margin used to be low on eggs, because consumer viewed it as a commodity. Now eggs are marketed as green egg, organic food chicken egg, naturally grown chicken egg and so on, increasing the perceived value of the eggs. This leads to better pricing.

**Nandini Hirianniah on Agile thinking in startups**

- Don't be rigid about raising funds. Don't fall into startup fashion trap and think raising funds is must if you are a startup. Also, don't be silly to set your mind on bootstrapping.
- If you are revenue positive, continue to focus on increasing revenue rather than raising funds.
- Startups need quality money. Take money from someone who will give intangible benefits like network. Look for investors who can push you in the right direction to multiply his 50 lakhs investment to 5 crores.
- Improve your company so much that investors come looking for you rather than you chasing investors.
- Startups should be thought-leaders in the fields in which they operate. That will attract good talents; talented individuals won't mind working for thought-leaders for less pay.
- Early stage startups should hire those willing to work on any task. When a phone rings, a graphic designer can't say, I'm not a receptionist so won't pick the phone.
- Applying for patent is a business decision. Early stage startups have hundreds of problems to solve. Patent process can divert focus of early stage startups. When a startup becomes a revenue-making business and have enough employees then it can consider applying for patents.

**Jaiprakash Hasrajani, on Managing talent in startups**

- As a CEO, do you have guts to go to your competitors and hire. Steve Jobs hired from Xerox and Compaq.
- Flipkart and Whatsapp are in data analytics business, not in e-commerce or communication business.
- As a CEO, do you have a developmental plan for yourself?


[1]: http://www.dsdinfosec.com
[2]: http://www.baddiuniv.ac.in
[3]: http://www.kuk.ac.in
