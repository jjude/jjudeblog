---
title="Ask Me Your Question"
slug="ask"
excerpt="You got a question that bothers you? Ask it. Get your answer."
tags=["engage"]
type="page"
publish_at="24 Apr 20 13:06 IST"
featured_image="https://cdn.olai.in/jjude/ask-gen.jpg"
---


* Should I become a specialist or a generalist?
* How to find a mentor?
* How can I learn to speak well?


<iframe src="https://www.speakpipe.com/widget/inline/xmc764aysv6yrth2346bz4d0ew1z13mm" allow="microphone" width="100%" height="200" frameborder="0"></iframe>
<script async src="https://www.speakpipe.com/widget/loader.js" charset="utf-8"></script>


I answer all these and more career questions on my [Youtube Channel](https://www.youtube.com/channel/UCPDIeR7Lzvt9uATvMZSZX-Q). If you have questions, send them using this [form](https://forms.gle/j662XydxqKH3dvr59) or email me at askme@jjude.com or send an audio using the above widget.
