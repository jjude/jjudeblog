---
title="From complete obscurity to little bit of popularity"
slug="from-complete-obscurity-to-little-bit-of-popularity"
excerpt="Django makes me popular"
tags=["tech"]
type="post"
publish_at="12 Mar 08 16:30 IST"
featured_image="https://cdn.olai.in/jjude/from-complete-obscurity-to-little-bit-of-popularity-gen.jpg"
---

When I posted about my first impression and experience with Django, I had no idea what was going to come.

Though this site existed for about 5 years, the traffic was primarily from my friends; and few others looking for FOW, the freeware that I developed as a photo album generator. Not much attention and I didn't kind of bother about it.

But today was different.

When I posted about Django in the morning, I didn't expect anything to be different in the traffic. But I was wrong. When I checked on the access log, I saw something strange. The access count was in the range of 100. It was strange because usually I hit 100 in a month, not in a day. So I opened the access log and found that there was a whole lot of traffic from reddit. I opened the referring URL and found that there were sites listed out in some ranking form and my article was swinging somewhere between 8 - 15. (Until today I had no clue about reddit).

![on reddit](https://cdn.olai.in/jjude/django-reddit.png)

Honestly speaking, there are better articles on the net about Django. I am not sure on what basis my article was posted there and who were providing points and how it ended up where it was. Anyway I am completely flattered by all this attention. I found that even Jacob (one of the lead developer) left a comment. All of this is too much for someone who is just starting in the web development area. 'Thank You' to those countless strangers who were kind to me.

![hits on site](https://cdn.olai.in/jjude/django-hits.png)

As I am writing, the count is 2300 and growing! My only concern is that it shouldn't run into any bandwidth issue. Hope the server is able to handle the load.

As the saying goes, every one has their fame of 15 minutes; I had it for a day!

