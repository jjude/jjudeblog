---
title="Priyanka Sud on 'Prevention of sexual harassment at workplaces'"
slug="priyanka-posh"
excerpt="Managing sexual harassment at work - a guide for victims, responders, and colleagues "
tags=["gwradio"]
type="post"
publish_at="17 May 23 11:56 IST"
featured_image=""
---

Millions of women are coming into workforce every year. If we have to keep them working, we have to make these workplaces safe for women. India has a comprehensive prevention of sexual harassment act. Today I'm discussing different aspects of the act with Priyanka, a practicing advocate.

<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless src="https://share.transistor.fm/e/0a7807df"></iframe>

## What you'll hear

- What is sexual harassment?
- Example of sexual harassment
- Does POSH cover harassment of men?
- What is ICC and its role?
- Can sexual harassment happen online?
- what is the process of reporting sexual harassment?
- How should respondants act when charged?
- Can the victim seek transfer to another project?
- Role of the team
- Evidence of expressing disinterest
- How can we prevent sexual harassment?
- Why POSH programs fail?
- How can victims minimize the after effects?
- What can families do to build healthy environments?

## Connect with Priyanka
- LinkedIn: https://www.linkedin.com/in/priyanka-sud-654254186/

## Connect with me
- Twitter: https://twitter.com/jjude
- LinkedIn: https://www.linkedin.com/in/jjude/
- Website: https://jjude.com/
- Newsletter: https://jjude.com/subscribe/
- Youtube: https://youtube.com/gravitaswins
- Email: podcast@jjude.com
- Executive Coaching Program: https://gravitaswins.com

## Your feedback counts

Thank you for listening. If you enjoy the podcast, would you please leave a short review on Apple podcast or on YouTube? It takes less than 60 seconds, and it really makes a difference in finding this podcast. And it boosts my spirits. 😀

I would like to hear what resonated the most with you in this episode. Either you can email me or share it on social media and tag me. Thank you for your support.

Have a life of WINS.

_Gravitas WINS Radio is produced by [Joseph Jude](https://jjude.com/). Be sure to subscribe on [Apple Podcast](https://podcasts.apple.com/in/podcast/gravitas-wins-radio/id1562802449), [Spotify](https://open.spotify.com/show/7LOBjAyjBkechMC0xMIj9E),[Google Podcast](https://podcasts.google.com/feed/aHR0cHM6Ly9mZWVkcy50cmFuc2lzdG9yLmZtL2dyYXZpdGFzLXdpbnMtcmFkaW8=),[YouTube](https://www.youtube.com/gravitaswins), or the podcasting app of your choice._

Please check out other episodes of [Gravitas WINS Radio](/gwr-themes/). For easy consumption, they are categorized by themes. If you are a fellow podcaster, you might be interested in [How I Podcast](/how-i-podcast/) or [tools I use](/i-use/).

## Continue Reading

- [Emmy Sobieski on 'I'd far rather be lucky than smart'](/emmys/)
- [Ritika Singh on 'Words Change Lives'](/ritikas/)
- [Liji Thomas on 'Conversational Bots'](/lijit/)