---
title="Are you chasing success or status?"
slug="status-or-success"
excerpt="I would rather succeed with a small team than be proud of managing a large one and failing"
tags=["wins","coach"]
type="post"
publish_at="01 Jun 22 18:03 IST"
featured_image="https://cdn.olai.in/jjude/status-or-success-gen.jpg"
---

The project was doomed to fail. It dragged on for two years, missing deadlines and budget allocations. Team members were frustrated with the project. They were concerned about their futures. 

M (Mike, not the one from the Bond movies) the business unit head, wanted to give one last shot. He assigned the project to Julia, a new project manager on the team.

Julia had just finished reading Partha Bose's book on Alexander the Great. The size of the army was one of the reasons for the winning streak of this army from an unknown state in those days. Alexander came to the battlefield with forty thousand fiercely loyal soldiers when his enemies sent hundreds of thousands. Alexander's army compensated for its size with its commitment.

She wanted to try that idea for the project. She had a team meeting as soon as she took over.

"I appreciate your hard work so far. I have few ideas on how to make this project a success. But I need people committed to the project. If you wish to leave, I will release you without any ill feelings. I promise you one thing: either we will successfully complete this project and win a company award, or we will learn valuable lessons on running projects". 

After the team meeting, almost half of the team wanted to be relieved. 

"Are you sure you know what you are doing? Who will deliver the project if you let go of half the members?” chided M. 
"I would rather succeed with a small team than be proud of managing a large one and failing"

M was not convinced. But he decided to play along. 

Julia went to work. She talked to key stakeholders about reducing the scope. They didn't want to reduce any scope at all.
"Do you want something useful to show the CEO in the next three months or nothing at all?" 

Following some hard back and forth negotiations, everyone agreed on a trimmed scope for the first release. Since Julia had been able to contain the scope, the team members trusted her. They noticed she worked harder than the previous manager. Not just working harder, but also building supporters among key decision-makers.

The team made the first release in three months. Users were happy that they could show the CEO meaningful progress. In six months a large scope of the project was delivered. They even won the department award, as Julia predicted. Julia nominated key members for promotion and salary hikes. She can be seen reading a book on Napoleon.

## Continue Reading

* [Fame Or Fortune](/fame-fortune/)
* [Principles Trump Processes](/principles-trumps/)
* [Build On The Rock For The Storms Are Surely Coming](/build-on-rock/)
