---
title="We Don't Need More; We Need Better"
slug="we-dont-need-more-we-need-better"
excerpt="More of the same only produces more of the same results. Nothing improves."
tags=["coach","startup"]
type="post"
publish_at="14 Jan 12 16:30 IST"
featured_image="https://cdn.olai.in/jjude/we-dont-need-more-we-need-better-gen.jpg"
---

The Internet is buzzing with [Code Academy](http://www.codecademy.com/)'s [code a year](http://codeyear.com/) program. Everyone is signing up for it, even Mike Bloomberg, New York Mayor, [signed](http://twitter.com/MikeBloomberg/status/154999795159805952) up for it.

Is [Code Academy](http://www.codecademy.com/) trying to beat the universe?

>Programming today is a race between software engineers striving to build bigger and better idiot-proof programs, and the Universe trying to produce bigger and better idiots. So far, the Universe is winning. ~ Rick Cook

Oh! wait, you can't do that with 200,000 people signing to code. That is like sending everyone in town with a camera hoping that someone will turn out to be Ansal Adams. It doesn't work that way. Not in photography, not in coding, not in project management, not in anything. But we don't learn our lessons, letting the universe win.

As [Vijay Sankar](http://twitter.com/vijayasankarv), [tweeted](http://twitter.com/vijayasankarv/status/155859107147030528),

> hmm..not too sure about this "everyone shud learn to code" movement...world does not need MORE coders, we need BETTER coders #justsayin

How I wish some psychology expert would explain the illusion of quantity magically producing quality.

Governments, corporates, and individuals are all obsessed with this illusion.

> What to do when a project goes for a toss? Add people[^1]
> How do we prevent fraud? Add more controls[^2]
> I want to improve my writing. What should I do? Write 700 words every day[^3]

So the magical answer is 'do more'. But, more of the same only produces more of the same results. Nothing improves. Wait, it is not same results; it gets worse.

As I have realized after managing many projects,
> to plough a field, you need two strong oxen; not one hundreds chicken

More often than I care to remember, I'm given hundreds of chicken. The field smells only of chicken shit.

[^1]: '[Mythical Man Month](https://en.wikipedia.org/wiki/The_Mythical_Man-Month)' by Fredric Brooks should be a mandatory reading for everyone associated with project management.

[^2]: [SOX](https://en.wikipedia.org/wiki/Sarbanes%E2%80%93Oxley_Act) introduced stringent financial controls with a hope to prevent further financial frauds. Has the magic wand done its miracle?

[^3]: Practice makes it perfect. So what happens when you practice something in a wrong way? You don't improve; you suck at that whatever you are practicing.

