---
title="How to succeed in the stock market with a simple and structured approach?"
slug="4m-stocks"
excerpt="I look for four Ms - Meaning, Management, Moats, and Margin of safety"
tags=["wealth","wins","systems","frameworks","visuals"]
type="post"
publish_at="21 Aug 22 11:21 IST"
featured_image="https://cdn.olai.in/jjude/stock-analysis-comparison.jpg"
---

## Short History Of My Investing

I asked a trusted friend for a stock tip when I wanted to start investing. On his advice, I purchased 500 Ashok Leyland shares at Rs. 20 each. As is normal in markets, the price dropped to Rs. 15 in a few months. Then, I panicked and sold all of my stocks, vowing never to invest in stocks again.

I never keep a promise. I returned to investing in stocks. Based on my market analysis, Ashok Leyland was indeed a fantastic stock. I started accumulating the stock. I accumulated in thousands at an average price of Rs. 24. As soon as the price rose above 44, I stopped buying. It was 15 years ago. Ashok Leyland's stock price today is Rs. 120.

What changed in-between?

I learnt. I joined courses. I read books. Ton of them. Courses and books were valuable. But nothing is more valuable than experience. I applied what I learned and came up with my own framework borrowing heavily from everywhere.

I don’t follow the framework step by step. It is jumbled. Think of this as an acronym rather than a step by step approach. 

## Meaning

Can I understand the business and the business model?

For example, I can’t understand real estate, sugar, and banking industries. I have tried. But the business models in these industries are beyond my comprehension. So I don’t touch those companies. 

But even within the industries I understand, the business model followed by a company might be deliberately complicated. So again I stay away from them. 

Simple test I have if I understand the business model is if I read the annual report can I understand it as I read? Or do I have to Google to understand what they are saying or are the sentences unnecessarily complicated?

Since I am not managing other’s money, the capital to invest is minimal. There are plenty of good companies to invest and hence it is better to eliminate most companies and focus only on few. 

Once I believe I can understand the business and their business model, I decide if I am going to invest for growth or dividend. When I was young I focused on growth but as I age I am shifting more to companies that pay regular dividends. Even otherwise I have come to understand the importance of cash flow even for the investor. It is the only hard thing I can put my finger and say I benefited from this company. 

But for a company to continue to pay dividends (regular income) it has to continue to grow (capital appreciation. So even though I am looking for dividends, the expectation of growth is implicit.

Two companies might be in the same business but might have a different business model.

## Management
I spend bulk of my research time in this area. It is the responsibility of the executives of the company to run the company profitably today but also keeping growth options for tomorrow. 

### Qualitative 

These are not hard numbers to compare. I look for these questions and answers:
- Who are the executives, board of directors, and advisors 
- How long they have been with the company?
- Are they being paid fairly - not too low not to high
- Are there any criminal cases against them?
- What news have been about them in the recent past

I prefer founder run companies esp when they come with a vision. Next I prefer executives who have stayed long in the company. If there are frequent changes at the top level that indicates, there is something wrong at the top level - either strategic or otherwise.

### Quantitative 

I use screener and moneycontrol to get all the numbers. There are tons of numbers one can look at and still not able to decide. So I have very limited number of numbers I look at. 

Usually I look at ratios than absolute numbers because absolute numbers can cheat you. 

I start with running this query on screener. 

```
Market Capitalization > 500 AND
Price to earning < 20 AND
Average return on capital employed 5Years > 15% AND
Return on capital employed> 15% AND
Debt to equity < 1 AND
Dividend yield > 3
```

![Screener Query](https://cdn.olai.in/jjude/screener-query.jpg "Running A Screener Query")

This gives me a smaller bucket of interesting companies to study further. 

![Screener Query Results](https://cdn.olai.in/jjude/screener-query-results.jpg "Screener Query Results")

Then I start collecting sales, NPM (net profit margin), ROCE (return on capital employed), EPS (earnings per share), and CR (current ratio) for these companies. And then compare these numbers across companies. 

![Comparing Companies For Stock Market](https://cdn.olai.in/jjude/stock-analysis-comparison.jpg "Comparing Companies For Stock Market")

I don't have any affiliation towards any industry or company. I only look for companies what will continue to give me good dividends and also grow. So I don't look for the best company in an industry, rather look for the best company that will give me good returns both in terms of dividend and growth.

## Moat

If a domain is good and profitable, it will attract many players. Management is responsible to build competitive advantage so the business can grow and capture the market but also resist competition.

Every good business has a moat. There are multiple forms of moat. [Jerry Neumann](https://twitter.com/ganeumann/status/1143159186798366720) documented these moats. They can be classified under:

-   The state,
-   Special know-how,
-   Scale, or
-   System rigidity

You should read the [original post](https://reactionwheel.net/2019/09/a-taxonomy-of-moats.html) to understand all the types of moats.

![Types of moats](https://cdn.olai.in/jjude/moats-types.jpg "Types of moats")

When a company has more than one type of moat, it has a greater chance to grow for decades to come.

It takes great strategy and work to build a moat, but it also requires equal amount of strategy and hard-work to sustain it. New technology, regulations, business models weaken moats for companies. So the management should always on the look out for the forces that can weaken moats. That is why it is important to evaluate the capabilities of the management.

The only trouble with building moats is, company executives might focus on moat and not really improving the product and expanding the market.

## Margin of Safety

Other three factors help me nail a good company. But I can't profit if I buy a good company at a wrong price. With margin of safety, I'm trying to buy ₹1 for 50 paise if possible.

Stocks are always mis-priced. As an example, even three years back when covid hit all stocks were down. I had the hunch that Thyrocare being in the diagnostic business would be in demand in the future. I bought it at a cheaper rate and now it has doubled in price.

Price matters a lot for both dividend and growth. If I buy at the peak, both the dividend yield and the returns are not going be substantial.

There are many ways to calculate the intrinsic value of the company so one can find a margin of safety. A popular way to calculate the value is to use discounted cash flow. 

There are times I use those calculations. But generally, I follow a rule of thumb. If a company is fantastic according to the other three factors, and PE is less than 18, then I accumulate the stock.

## Sum up

1. Run a query in screener to identify solid companies 
2. Narrow the query parameters (like ROCE > 20 from 15) to lessen the number of companies
3. Pick 5 companies from the results at random
4. Collect ratios and other financial numbers for these 5 companies
5. Compare the results
6. Pick 1 or 2 companies to go deeper
7. Read annual report & use Google to understand the management qualitatively
8. Understand the business model and moat
9. Assess if the moat and products line-up will continue to give the company competitive advantage
10. Repeat the process if you are not happy with the company you picked
11. Calculate the price range to buy (either using intrinsic value or using PE)
12. Start accumulating the stock
13. Sit tight and let the stock prices do the compounding magic

## Continue Reading

* [Value Investing Is Better Than Real Estate](/value-investing-real-estate/)
* [Proven Biblical Principles To Build Wealth](/biblical-wealth/)
* [Truth About Passive Income & Financial Freedom](/pasive-income-truths/)