---
title="'Carving a unique career path' with Meenakshi S, Practice Head "
slug="meenakshi-career"
excerpt="How to build a career that matches your skill set"
tags=["gwradio","coach","wins"]
type="post"
publish_at="17 May 23 20:51 IST"
featured_image="https://cdn.olai.in/jjude/meenakshi-career.jpg"
---
<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless src="https://share.transistor.fm/e/f790ef25"></iframe>

Most of us become engineers and doctors because our parents wanted us to become one or our classmates chose to be one. Even when we build our career, we are mostly looking at others to build ours, instead of building our career on our unique skill set. Today I'm speaking with Meenakshi about building a unique career path depending on our skill set. Hope you enjoy the conversation.

## Edited Transcript

### Talk to me how you built your career.

As of this August, I'll be completing 25 years in the industry. And when I look back I don't think I, I knew what I was going to be in 20 years or 25 years. My career has taken me to different opportunities which is very helpful because it totally aligned with my strength.

I look back to my career and I can see it in phases. Each of the phases has helped me and groom me to be the person that I am today. That's, in short, I would think that everything was a stepping stone to push me to where I am today.

### Is your skillset driving your career or the career driving your skillset? 

There was a TED talk with a Harvard professor where she talks about three ways that one gets to a career.

The first one is about inclusion. Inclusion is everything is about skillsets. We tell our kids, life is much more than college. A good college to study at is always important in order to get a first window into a career. Because I did an engineering degree at that point in time and there was a lot of campus interview on the in the college, it was much easier for me to get into the career smoothly.

The second part is about influence. So getting an open door and getting into a career is good enough, but influence is what really makes you sustain and grow. And influence is more about, it's a combination of what you do, what you want to do in future, and who can help you do that.

And then the impact is what really I'm going through in the last five years where, you feel you've decently accomplished a few milestones in life's way and in your career path. Now what are you going to do with that? What's the impact that you can create to the people at large? Not just in the organization but to the world at large, there's something I can give back. 

Skills are important, but it's not the only thing that is important.

### How the company that you joined and the first managers that you had, how they influenced your career? 

I'm what I'm because of the kind of leaders that came my way, especially in the first five years of the career. I had three type of leaders.

First type of leaders was a perfectionist. She used to keep sending us back saying, hey, this is not perfect. This is not perfect yet. It was more terrorizing. Because we used to be scared as juniors, but I think she instilled in us the the essence of being detail oriented and attention oriented and understanding the business.

The second type of leader, was more an innovator. He used to walk to each person's desk. And he used to kind of be very friendly with them and push them to think out of the box.

The third type of leader was breaking the norms. This is the person also who impressed me a lot, who in a room of say 50 or hundred people, he'll never shy away from asking the silliest question or the hardest, until he gets what he needs to out of the whole problem statement. 

### Role of company size in shaping your career

More than the company, what matters is how flexible we are and how agile we are; and do we understand the organization's goals and needs.

When a person starts out, it is very important to sense the business and the political environment of a particular organization. It's not about you being the best, but are you best aligned to what the organization wants to do for the next two years, next three years?

### How somebody can identify the company goals and how they can align their skillset to that particular company goals?

They can read it on net, they can go to the company's website. Understand what the company does. Is it a product company? Is it a services company? What is their vision? Where are they in the market?

It takes quite a while and it'll be fed to you in smaller bits. The only thing matter is if you are getting the supportive environment for you to keep learning.

I'm sure by end of year five through all the conversations that they've had, both formal and informal, they'll be able to understand how they're aligning to the organization goal. And at that, it's just more decision making process. Do you like what you're doing?

Do you like what your organization is doing? Is there a match, almost like a matchmaking couple? Would you want to explore something else? So as youngsters, if they take time up to five years, I'm sure they'll understand what they're doing and how it contributes to the organization goals yourself.

### Did you have to make any sacrifices to build the career that  you wanted? 

The word sacrifice almost means like one person is winning and one person is losing. I've always thought of the things I had to do in my career as more a priority.

I've been faced with various different priorities along life's way. I did take a maternity break, when my daughter was born. So that was a priority thing at that point in time. 

Sometimes they feel, hey, did I lose out because just because I took off that time; I prioritized something else. But to me, every single break, I've thoroughly enjoyed what I did. Because that was a priority to me at that point in time.

After 25 years of career, when I talk to people, I am seen more authentic and genuine because I don't just talk about, hey, I did this at work. I became these designations, I did these roles projects, but I talk to them as a human being saying, you know what? I, I had a family along the way and this is how my family grew up along with me in the career, and they're part and parcel of who I am, and I cannot let go of one identity to another.

With each transition, I picked up different roles. 

Like I said, every single transition I've taken helped me to connect the dots and help me to be a well-rounded leader that I am today.

### Do you think there is a maximum age before which you should shape your career? 

It wouldn't be fair if I say all the roles that you got when you are a youngster is available when you are senior. So becoming seniors brings its o its own challenges. And I would say upto 30 years, the sky is the limit. It's just up to you.

Whatever you want, you can push yourself and do. When you reach the 30s to 45 years, there are opportunities open, but it could be some things that are not aligned to your strength or aligned to your comfort zone sometimes, and then it becomes a priority on whether you want to do those roles or whether you want to do something else.

Seen a lot of people between 35 to 45 then deciding, I've done like 10 years or 15 years. Now I would want to become entrepreneur, or I wanna do something else. So I've seen them taking a different path. 

After 45, you have to keep our ego in hiding. There will be youngsters, there will be many young people who are execs, who are the CEOs of the company, and you would have to report to them.

It doesn't matter who's giving me the instructions, but this is how they want it to be done. Because they pay my salary. So after 45 years, it's just a choice we make in our minds. Do we do what we do or do we give it up? And I think retirement is more a mental thing than a physical thing.

If you feel you're getting tired of what you're doing, and if you feel that you have more to do. Either you crib and make a change or you don't crib and continue what you're doing. So I wouldn't like somebody to crib and keep continuing to doing what they're doing.

### How do you develop such mindset? How do you coach your team members to develop this mindset

I wasn't this person when I was say five years, 10 years into my career. I don't think people like me, frankly, at that point in time. 

I used to be this perfectionist. I was like, it doesn't matter, what you guys are going through, I need this. I wouldn't like to work for that person. I was delivering and I was a results oriented leader, but I wasn't the empathetic leader.

After the 11th or 12th year and slowly when I started looking at the people I admire, there was one big gap and it was the empathy. 

They were people leaders, and they were so empathetic in nature. They knew that job had to get done, but they didn't kill the energy while the job was getting done. They made sure that the energy was intact until the job was done.

Those are the leaders who really made a big impact on me, and I think slowly over the years I've picked up the tricks of the trade of being empathetic. 

### Empathy vs result oriented leadership

It's like being an employee and being a stockholder. Many of the employees are given stock options and it's like do I want these stocks to have higher price or should I get a higher salary? You never get the right answer for that. 

When I look at result oriented and empathy oriented It, it, it has to come together. It'll not come together easily, but it has to come together. 

If we continue to only be results oriented, then we are only operating within the sphere of what we know.  The key for unlocking a lot of business potential in the long run is only going to come through empathy oriented approach. You can solve for bigger problems only with the empathy oriented approach. 

### Impact of generative AI on leadership & management

I had a like colleague who put it very nicely, and we were talking about technology versus domain.

He says, doing it right is understanding the business and domain. Doing it fast is understanding technology. So generative AI is going to help you do things faster. It'll be rarely good. But knowing what to do, what problem are we trying to solve, what is the issue that the client is facing today or customers facing today is something we need to know.

What's the point of running a hundred meter race, when you knew that it was a marathon? If you don't get the context right, you're just gonna fail miserably, even if you know the technology.

### Limitations of building your own career path

One biggest limitation for anybody is the fear. It could be the fear of doing something new. It could be the fear of failures. It could be the fear of missing out on things. It could be any amount of fear. We have to let go of the insecurity.

A mentor told me, there are just three things that are important in life as a business leader: whether we are doing the right things for the customer,  whether we are doing the right things for the teams on the ground, and whether we are doing the right things for the organization.

If something ticks all these there is nothing wrong in trying out things. 

### Impact of working from anywhere

Work from anywhere is definitely like generate ai. It's going to be there. You cannot get away from that. How do you use effectively is the point. 

I would think of this in two ways. 

There are people genuinely in life situation, they have different priorities. They're having older people to take care of. They're having younger people to take care of and instead of giving up a job, they're doing their best saying that I will still work from anywhere and make sure that things get done right and you need to give them a break. I don't think it's fair to say that, hey, you know, you definitely need to come back. But that's the exception.

Then there are people who don't have personal constraints. It is very convenient, it is sometimes creative and it is a lot more easy to manage time. People who manage their time are much richer than people who manage money. But there is a word of caution. There is nothing like two minds thinking together. There is nothing that beats the physical connect, a physical meeting, meet and greet of the people on the team.

You can adopt work from anywhere but what percentage we adopt and do we still make time to come together as a team physically is also important. 

#### Rapid fire questions
##### what do you think as a leadership quality and the, the best leadership quality and who has exhibited it in your life? 

Decision making. And many of the elders in my family as well as many of my senior leaders they never sleep on a decision. So decision making's absolutely important.

##### What's the kindest thing anyone has done for you? 

They understood my constraints whenever I was working, all the leaders that I work with. So, and they believed and trusted me. I think you know, understanding and trust, definitely. 

##### What's the definition of living a good life? 

Living a good life is waking up and feeling proud to be known without your work designation. Even if I didn't have my job, if I can describe myself in two, three different ways, I think that's a good life lived and the number of people you impacted. 

## Your feedback counts

Thank you for listening. If you enjoy the podcast, would you please leave a short review on Apple podcast or on YouTube? It takes less than 60 seconds, and it really makes a difference in finding this podcast. And it boosts my spirits. 😀

I would like to hear what resonated the most with you in this episode. Either you can email me or share it on social media and tag me. Thank you for your support.

Have a life of WINS.

_Gravitas WINS Radio is produced by [Joseph Jude](https://jjude.com/). Be sure to subscribe on [Apple Podcast](https://podcasts.apple.com/in/podcast/gravitas-wins-radio/id1562802449), [Spotify](https://open.spotify.com/show/7LOBjAyjBkechMC0xMIj9E),[Google Podcast](https://podcasts.google.com/feed/aHR0cHM6Ly9mZWVkcy50cmFuc2lzdG9yLmZtL2dyYXZpdGFzLXdpbnMtcmFkaW8=),[YouTube](https://www.youtube.com/gravitaswins), or the podcasting app of your choice._

Please check out other episodes of [Gravitas WINS Radio](/gwr-themes/). For easy consumption, they are categorized by themes. If you are a fellow podcaster, you might be interested in [How I Podcast](/how-i-podcast/) or [tools I use](/i-use/).

## Connect with Meenakshi
- LinkedIn: https://www.linkedin.com/in/meenakshi-s-36897b15/

## Connect with me
- Twitter: https://twitter.com/jjude
- LinkedIn: https://www.linkedin.com/in/jjude/
- Website: https://jjude.com/
- Newsletter: https://jjude.com/subscribe/
- Youtube: https://youtube.com/gravitaswins
- Email: podcast@jjude.com
- Executive Coaching Program: https://gravitaswins.com

## Continue Reading

- [Three Types Of Goals You Should Set](/three-goals/)
- [Ubellah Maria on 'Transitioning from developer to manager'](/ubellah-maria-becoming-a-manager/)
- [Culture is what you do, not what you believe](/culture-is-doing/)