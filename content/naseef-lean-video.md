---
title="Naseef KPO on 'Lean video marketing'"
slug="naseef-lean-video"
excerpt="Video marketing doesn't have to be expensive. "
tags=["gwradio","biz"]
type="post"
publish_at="06 Sep 23 06:00 IST"
featured_image="https://cdn.olai.in/jjude/naseef-yt.jpg"
---
When we think about marketing, we need to use all formats of content. That includes text, images, and video. But most small businesses think it is too expensive to market via videos. Naseef, today's guest, says it is possible to have a lean video marketing setup. Let us find out.

<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless src="https://share.transistor.fm/e/6e336d17"></iframe>

## What you'll hear
- What are the steps involved in creating a video marketing campaign?
- Creating video for IT services company
- Tools for lean video marketing
- Different ways to repurpose videos
- KPIs for video marketing
- Examples of B2B videos
- Mistakes in video marketing
- Kindest thing anyone has done for you
- What is the definition of living a good life

## Edited Transcript

### Why video should be part of marketing for small biz?

Videos are the content format that every platform supports nowadays. Videos are important, especially if you are in the early stages of your business and you don't have a recognisable brand, and people have not seen you, like me who launched a company recently.

They have not seen me; I don't attend live events or conferences. So the only way for people to see me is through videos. The sessions we're having right now are how people get to see me, and they at least feel like they've seen this guy, even though we haven't met in person. So, from a business standpoint, **videos are very important for people to get to know you**, especially if you are not a well-known personal brand or if people haven't met you or the people behind your business in person.

The second thing is that, as I mentioned, a lot of platforms are now prioritizing video. We know that YouTube shorts are becoming very popular and gaining a large number of views. YouTube's algorithm is promoting them significantly, and even active B2B platforms like LinkedIn are pushing video content. I've noticed recently that LinkedIn is making a big effort to promote videos organically. 

So, I don't think there's any reason for any business, especially B2B businesses, not to start using video. **Videos are one of the best ways, if not the best, aside from in-person meetings, for you to establish a connection with your audience**.

### What are the key steps involved in creating a successful video marketing campaign for a small or medium-sized business?

I usually don't view a video as a campaign; rather, I see it as **an asset created for eternity**. 

Essentially, there are three steps. The first is **planning**, where you decide what **type of video** to create. Are you going to make explainer videos, product-specific videos, talking head videos, or other types of videos? You also need to determine the topics for your videos, which is critical. Consider the video's duration, how the storyboard should look, and although you might not create the entire script in the first step, you should plan it out comprehensively from start to finish, including keywords, content format, and so on.

The second step is the **creation process**. This is where, if you're using a professional voiceover, you need to write the script and convert the storyboard into the script. Then you actually create the video, handle the editing, review it, and so on. Depending on the type of video you're creating, the scripting approach may vary. For example, if you're doing a webinar, you might go unscripted or fully scripted. You can deliver it live or on-demand. However, for a short two to three minute video where you're sharing tips, like "Five Tips for Video Marketing," it will typically involve you speaking in front of a camera, which again requires a different type of script.

The third stage is **optimization and promotion**. Depending on the platform you're using to promote the video, such as YouTube, you'll need to optimize various elements like the video's title, file name, description, and thumbnail to perform well on that platform. If you're promoting a video on LinkedIn, there are also best practices to follow, such as starting with a compelling hook, using an appealing thumbnail, and keeping the video reasonably concise. Different platforms may require different optimization techniques.

For promotion, there are numerous methods you can employ. As I mentioned, YouTube and LinkedIn are just a few examples, and we can discuss more techniques as we progress. In essence, this is my approach: start with planning, move to the creation stage, and finally, proceed to the optimization and promotion stage.

### Creating video for IT services company

They need to discuss how each of their services will help particular use cases in the industry. For example, let's talk about Hexaware Technologies, where I started my career.

Hexaware Technologies primarily served 4 to 5 industries back in the day, and one of them was banking and financial services. So if your company offers application support and maintenance services, you probably want to talk about how these services can make a change in the way a bank operates digitally. There are more details to it, but I'm keeping it at a 10,000 feet level. When you start thinking in this direction, you'll begin to create a tree-like structure, as I mentioned earlier. You start with an anchor, divide it into different categories, and then add multiple branches to it.

In the realm of banking and application support, consider the use cases and how your company's capabilities can make a difference. That's one way to approach it.

The second thing is that any video should have **three major objectives**, or at least one of these three objectives: entertain, evoke emotions, or educate. This applies to businesses as well, not just individual creators. Think in this direction.

For **education**, you can help your prospects understand how a specific set of services solves their problems. That falls under the educate segment. 

For **emotional engagement**, consider an example from HP in the cybersecurity space. They talked about the implications of a cybersecurity data breach, aiming to evoke fear in viewers. They did a great job with this emotional approach, even though they didn't directly promote their solutions. They included an intro and outro about HP but focused on creating a story about the implications of a data breach and how cybersecurity can help. That's an example of an emotional video.

The third category is **entertainment**. For instance, if you're celebrating a festival in your office, you can turn that into a video. Such videos and posts tend to get more engagement on YouTube or any platform because people enjoy seeing positive events happening around them.

These are the three pillars to consider, and by following this structure, you'll discover more ideas within these three categories.

### Let us talk about tools

It all **depends on the stage** you are in. You can create videos with very little investment, or you can go all the way and set up a professional video studio. It really depends on how you want to approach it. So if you're just starting out, especially if you're someone like me, a business owner who has recently launched, you obviously want to operate lean.

You have to look for ways to avoid spending too much money. There are a few elements to consider. First is, of course, a **camera**. You don't have to go for those 5 lakh INR cameras; you can start with your own laptop's camera, which might not offer great quality, but if you want to step it up a bit, you can invest in a web camera. For example, I'm using a web camera now.

The second thing is **lighting**. As much as possible, if you don't want to invest in lighting, you can use good natural lighting sources in your room. I'm not using lighting right now, although it's not something I would personally recommend. However, if you want to keep costs low, these things matter less. What's more important is your content.

The third thing is a **microphone**. You can either use your existing headset or invest in a decent microphone for as little as 2000 rupees, not more than that. You can upgrade as you gain more experience, but you can start with that.

When it comes to software, if you're hosting your own content and conducting sessions like these with guests, you can go for something like Riverside. Alternatively, you can use popular video conferencing applications like Zoom or Teams if you already have a paid subscription. There are also other options like Restream and StreamYard, among others, if you're considering paid solutions.

If you're recording videos or doing screen recordings, you can use tools like Loom or Vid.io. Some of these tools offer free versions with watermarks on your video, which can be a good starting point.

For editing, my all-time favorite is Canva. I do very basic editing on Canva since I'm not a professional video editor. However, if you have video editing skills, you can consider using software like Adobe Premiere. If you have a MacBook, iMovie is a decent option as well.

Basically, it's about **making the most of the resources you already have**, whether it's software or hardware. Consider what you might need to purchase on a limited budget, and you'll find plenty of options. Search on YouTube, watch reviews, and choose something that suits your budget.

That's how you start. If you want to scale up, you can do so gradually. If you want to improve your lighting, you can invest in a softbox lighting setup. You can also consider getting a professional camera. One popular microphone used by YouTube creators is the [Blue Yeti mic](https://geni.us/blueyeti-usb-mic), which starts at around 9,000 to 10,000 rupees. There are plenty of options available, but you can begin small, just like I am doing, and then gradually improve from there.

### How to repurpose videos

One of the biggest mistakes, especially for B2B businesses, is creating a video and then forgetting about it. If you look at individual creators or influencers, they excel at using their video content in various ways. However, B2B businesses, in general, tend to under-utilise the potential of videos when it comes to distribution.

You can start by **posting the video on YouTube** and optimising it as much as possible. Utilise elements I mentioned earlier, such as optimising the title, description, hashtags, and thumbnail. Make sure to include end cards. These are essential YouTube optimisation techniques to follow. Over time, if you correctly implement SEO elements, you'll rank well on YouTube. Tools like VidIQ or TubeBuddy can help you understand trending topics in your space, keywords to focus on, and hashtags to use. Additionally, YouTube's native platform has a research tab where you can explore keywords' volume, whether it's high, medium, or low. You can use all these tools to ensure you optimise your video for YouTube SEO.

The second thing to consider is where else you can use and how you can use your video content. Regarding "where," **think about other platforms**. Can you share it on LinkedIn? Within LinkedIn, can you promote it on your company page or your employees' and leaders' individual pages? Consider their handles. If your business has multiple pages, leverage them as well. If you have the time and resources, consider posting in relevant LinkedIn groups. This is how you can approach LinkedIn.

Another aspect to explore is where else you can **embed the content**. For instance, in blog posts, if you're writing an article about five video marketing tips (a generic example), you can create a video that discusses those tips or focuses on a specific tip from the article. Embedding such videos adds to the context of your content. If, let's say, 100 people visit the article each day, and 10 people click on the video, that's an additional 10 views for your YouTube channel. These increasing views positively impact YouTube SEO. Can you also include these videos in other campaigns, such as email campaigns or LinkedIn outreach efforts? Often, salespeople or SDRs on LinkedIn reach out to prospects with a pitch deck or a link to the services page. However, you can **enhance your outreach by sharing thought leadership content from your CEO** or founder, especially if they've spoken at recent conferences related to your niche. Including this kind of supporting content can improve the reach and distribution of your videos.

Thinking in this way opens up various avenues for including videos in your outreach efforts. Some people even **add video links to their email signatures**, although this depends on your preferences. For event-related videos, like when your company secures funding, you can add the link to your email signature. This way, every email you send out allows recipients to see the video, and some may click on it.

Furthermore, if you're hosting webinars or speaking at live conferences, you can use your video as supporting material for your presentation. The suitability of this approach depends on the type of video, but it's about exploring how you can leverage every opportunity to promote your business. Video is undoubtedly one of the most critical content types you have at your disposal.

### KPIs for video marketing

Video is demand-gen-specific channel. I see this tendency for people to view it as a demand-capture channel.

And what I mean by that is people think, "Okay, I've created ten videos. I should be getting at least five leads from each video. So, shouldn't video have contributed to 50 leads in total, right?" I think we'll have to do away with that kind of mindset because videos don't work that way. I'll give you a real-life example from my last company, Econ Systems.

We had a customer come and tell us, "We saw your video on this particular product, and it was very well done. Why don't you create more videos like that?" So, there was no attribution, no tracking in your Google Analytics. Your analytics wouldn't provide you with this information, but you have to understand that this kind of impact happens. So, with that said...

You obviously want to try and track this as much as possible. You can set up tracking in Google Analytics to see how much traffic you're getting from YouTube and how many conversions are happening if you set up goal definitions properly in Google Analytics. You'll be able to see how many conversions YouTube, as a channel, has contributed to.

So, that's how you attempt to capture how much it has contributed to increasing the pipeline or leads of the company. But what I personally prefer to do is, like I said, consider it as a pure demand generation channel. I want to **measure more top-of-the-funnel metrics**.

I mean, for example, it depends on the platforms you use. Let's say if you're using YouTube, what are the different metrics you can look at? One is, of course, at a video level, you can look at the number of views, likes, and comments that you get. But at a consolidated channel level, in YouTube, you can also look at the total number of impressions for your video, the total number of clicks, the click-through rate, and the number of subscribers you have.

You can also track the total watch hours you've received and the retention rate of your video. YouTube says that **retention rate is one of the most important metrics** among the others I mentioned. This is why you see a lot of creators starting with hooks like "Here are seven ways to improve your productivity. The seventh one will shock you," right? So you would have seen hooks like those, intros like those. That's because YouTube places a lot of value on retention rate, and creators want viewers to watch the video till the end. So retention rate is another crucial metric. This is what you can see on YouTube.

If you're using LinkedIn as a platform, you can look at the number of unique viewers and total views when you post a video. LinkedIn provides information about the number of minutes the video has been viewed. The metrics are slightly different there. Depending on the platform you use for hosting the video, the metrics might change.

Additionally, on YouTube, you can delve deeper and look at the demographic data of your viewers. You can see how many subscribers versus non-subscribers have viewed your video. All this data is available in YouTube analytics. If you're embedding the video using the YouTube code snippet on your website or in an email, you'll also be able to see how much traffic has come from those sources. It will show up as external sources in YouTube analytics, allowing you to dig down and see that kind of breakdown as well.

So, these are essentially the top-of-the-funnel metrics you can measure. Moreover, as I mentioned at the beginning, you can also go one level deeper and look at your Google Analytics data to see how much traffic YouTube has brought in and how many conversions it has resulted in.

### Examples of well done B2B video campaigns
One of the companies, an Indian example that I like a lot and think has done video marketing, especially on LinkedIn, is [Factors AI](https://www.factors.ai/). They are not that big on YouTube yet, but they actively promote videos on YouTube. Factors AI is a Bangalore-based company that sells a marketing attribution and analytics platform. They have created some videos, such as platform walkthroughs, where they demonstrate how the platform works and the type of data you can expect from it. 

In the B2B space, this approach is similar to tools like Loom, where screen recording videos provide a real feel for the software's functionality, rather than just listing its features. So, I see Factors AI doing this well. They also host a lot of LinkedIn live events, collaborating with their ideal customer personas, such as VPs of marketing, chief operating officers, or even CEOs. These events are also a form of video marketing.

The CEO of the company is also involved, appearing in some of these videos to discuss how the product helps, or to talk more generally about how marketing analytics is changing and assisting businesses in making better decisions.

Another example is [Gong](https://www.gong.io/), a product that we've all heard about. It's an excellent example of category creation in B2B. Gong is a sales intelligence and conversational analytics platform that helps analyse sales conversations with prospects and provides insights on how to improve them. Gong has a YouTube video that guides you through a typical day in the life of a VP of sales and how the product helps improve their productivity. What's particularly attractive about this video is how it is **laser-targeted towards VP of sales, who are a key customer segment** for Gong. The two to three-minute video takes you through the daily routine of a VP of sales, showing how they use Gong's insights to save time, boost productivity, and achieve better results.

Another Indian example is an online media company called [Analytics India Magazine](https://analyticsindiamag.com/). They have a strong presence on YouTube, offering a mix of videos, including live video podcasts and founder-led discussions on various topics. Like the previous examples, they employ a diverse range of video content to engage their audience.

### Mistakes in video marketing

One thing we've already covered is that you **create a video and then forget about it**. That's the biggest mistake you can make as a marketer because you're not leveraging your existing potential. People often chase new things but sometimes forget to utilise what they already have.

The second thing is that **people think it's too difficult or expensive to get started** with videos. I think, to some extent, this perception is created by marketing agencies as well. Whenever you try to outsource video creation, it can seem pretty expensive. However, as a matter of fact, it's not that costly because we've discussed how you can do video marketing. 

So it won't cost you much in terms of equipment and software, etc. What else do you need? You may require a video editor, probably. In India, you can find a decent video editor for around 60K per month. Not more than that. As an individual contributor, you can find people willing to work for that salary.

The second thing is that you need a subscription to stock image and video databases, right? You have Shutterstock, iStock, or whichever you prefer. They offer different plans, and they're not very expensive either. You can choose between a per-image purchase model or packages that allow you to buy 20 or 30 images or videos per month. That's all you'll end up spending. So, it's actually not a lot if you look at it. You can start with video marketing quite affordably. 

If you don't want to hire a video marketer internally, you can go to websites like Fiverr or Upwork for basic editing work. For something more comprehensive where you need end-to-end help, I suggest considering a specialised video marketing agency or a marketing agency that excels in this area. You can get things done for much less than you might think when it comes to video marketing.

The third thing is that **people think it takes a lot of time**. I'm not saying it doesn't take any time at all, but there are ways to reduce the time investment. Instead of creating long, scripted, well-lit videos like those on YouTube, think about conversations like these. These conversations can last for 45 minutes to an hour, and you can easily repurpose and split them into shorter pieces to post on multiple platforms. So, one hour of effort and perhaps a couple of hours of preparation at most can give you content that you can use for a long time. It doesn't necessarily have to involve a significant amount of effort. Of course, as you progress, you can invest more time and produce more professional videos. But what I'm saying is that it doesn't require as much effort as you might think. I'm running an agency, and I handle all my video marketing efforts. I already have a YouTube channel where I've published 14 videos. So, if someone like me can do this, anyone can. That's the third misconception I'd like to address. It's time for people to change these beliefs, and there's no excuse not to start with video marketing.

### Rapid fire questions
#### What is the kindest thing some one has done for you?

Whenever someone agrees to give their time, either virtually like this or in person, for something that doesn't concern them immediately, I find that very valuable and considerate of them. Given the stage that I'm at in my business, I'm trying to build relationships. If someone has taken the time to meet me, let's say at a coffee shop, for something that doesn't immediately concern them, I consider it to be a kind and thoughtful gesture.

#### This podcast is about leadership. In that sense, can you please share what you think as the best leadership quality and who has manifested it in your life.

One of the biggest responsibilities of a leader, in my opinion, is to **foster an environment for learning**. It doesn't necessarily involve conducting daily 15-minute sessions on how to perform your job, but rather guiding individuals in the right direction. This guidance could involve suggesting courses, recommending particular events, and not necessarily limited to the business's specific domain.

It extends beyond that. Suggesting seeking assistance from a particular person or recommending a book are examples of ways to create an atmosphere of learning. Sometimes, you may not even realize that this learning environment is being nurtured, and leaders can do this unintentionally as well.

When I began my career at Hexaware, I worked under the former CMO of Hexaware, Parnajera, who was my boss's boss at the time. While she is no longer with Hexaware, she was highly enthusiastic about technology.

Having a tech background, she transitioned into marketing and explored nearly every marketing tool available. She either tested these tools herself or had the team evaluate them. As part of the marketing analytics and martech team, it was my responsibility to assess these tools. This exposure allowed me to explore various tools that most marketers wouldn't typically have access to.

For instance, take Demandbase. It's now a well-known intelligence and ABM platform, but back in 2018-19, it was relatively unknown. During that time, I had the opportunity to work hands-on with that platform. I believe that creating opportunities for people to learn is crucial. It's a key trait that leaders should possess.

#### What is your definition of living a good life?

It keeps evolving as you grow older. If you had asked me five or six years ago, my focus would have likely been entirely on work. Now, as I mature and continue to navigate life's challenges, my perspective on **what constitutes a good life has shifted**. Currently, a good life, in my view, involves finding a balance between work and family while steadily progressing toward your long-term goals every day.

Professionally, my long-term goal is to establish and stabilise my business, create a team, and provide a space for people to collaborate. Therefore, every conversation, including the one we're having today, contributes to achieving that long-term goal.

## Resources mentioned in the podcast
### Video streaming 
- Riverside: https://riverside.fm/
- Restream: https://restream.io/
- StreamYard: https://streamyard.com/

### Video screen recording
- Veed: https://www.veed.io/
- Loom: https://www.loom.com/

### Video editing 
- Canva: https://www.canva.com/
- Adobe Premiere: https://www.adobe.com/in/products/premiere.html
- Adobe After Effects: https://www.adobe.com/in/products/aftereffects.html

### Video SEO
- VidIQ: https://vidiq.com/
- TubeBuddy: https://www.tubebuddy.com/

## Connect with Naseef
- LinkedIn: https://www.linkedin.com/in/naseef-kpo/
- Skalegrow’s website: https://skalegrow.com/
- Skalegrow’s YouTube channel: https://www.youtube.com/@Skalegrow
- The Skalegrow newsletter – learn 1 B2B growth hack every week: https://skalegrow.substack.com/
- Skalegrow’s LinkedIn page: https://in.linkedin.com/company/skalegrow

## Connect with me
- Twitter: https://twitter.com/jjude
- LinkedIn: https://www.linkedin.com/in/jjude/
- Website: https://jjude.com/
- Newsletter: https://jjude.com/subscribe/
- Youtube: https://youtube.com/gravitaswins
- Email: podcast@jjude.com
- Executive Coaching Program: https://gravitaswins.com

## Watch as a video

<iframe width="560" height="315" src="https://www.youtube.com/embed/LU5NKgCnDVM?si=iYhv4abwIYa9jQ9R" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

## Your feedback counts

Thank you for listening. If you enjoy the podcast, would you please leave a short review on Apple podcast or on YouTube? It takes less than 60 seconds, and it really makes a difference in finding this podcast. And it boosts my spirits. 😀

I would like to hear what resonated the most with you in this episode. Either email me or share it on social media and tag me. Thank you for your support.

Have a life of WINS.

_Gravitas WINS Radio is produced by [Joseph Jude](https://jjude.com/). Be sure to subscribe on [Apple Podcast](https://podcasts.apple.com/in/podcast/gravitas-wins-radio/id1562802449), [Spotify](https://open.spotify.com/show/7LOBjAyjBkechMC0xMIj9E),[Google Podcast](https://podcasts.google.com/feed/aHR0cHM6Ly9mZWVkcy50cmFuc2lzdG9yLmZtL2dyYXZpdGFzLXdpbnMtcmFkaW8=),[YouTube](https://www.youtube.com/gravitaswins), or the podcasting app of your choice._

Please check out other episodes of [Gravitas WINS Radio](/gwr-themes/). For easy consumption, they are categorized by themes. If you are a fellow podcaster, you might be interested in [How I Podcast](/how-i-podcast/) or [Tools I Use](/i-use/).

## Continue Reading

* [Arvindh Sundar on 'Gamification of marketing for small businesses'](/arvind-gamify/)
* [Vikrant Sukhla on 'Trends in Digital Commerce'](/vikrant-commerce/)
* [Sathyanand on 'The Visual Solopreneur'](/sathya-visuals/)