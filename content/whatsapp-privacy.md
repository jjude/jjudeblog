---
title="Whatsapp Privacy"
slug="whatsapp-privacy"
excerpt="Do't think of privacy in silos"
tags=["coach","tech"]
type="post"
publish_at="20 Jan 21 09:00 IST"
featured_image="https://cdn.olai.in/jjude/whatsapp-privacy-tribune.jpg"
---

Whatsapp kicked off a debate on privacy with their recent app updates. Their inconsistent message started a mass exodus into other platforms, mainly signal. Tribune Chandigarh, [published my comments](https://www.tribuneindia.com/news/lifestyle/whatsapps-changed-privacy-policy-has-sent-netizens-into-a-frenzy-198285) on the privacy issue:

![Joseph Jude comments on WhatsApp Privacy](https://cdn.olai.in/jjude/whatsapp-privacy-tribune.jpg)

We voluntarily upload our photos, tag our kids and friends, and then raise hue and cry when technology companies announce they are going to share data between applications they own. **We can't think of privacy in silos**.

Though related, **security and privacy are the not the same**. Privacy is about who has your personal information; security is about how well it is protected.

I'm sure Whatsapp and Facebook, as services, are secure; but do they respect our privacy? Do they share our data with other parties that we don't know? There is no clear answer to these questions.

Among the popular messaging apps, [Signal](https://signal.org/en/) app is better from privacy perspective, since it is **open source**. Technology enthusiasts can audit the code and fix any issues.

So should you move to signal for privacy?

Move if you could. Better yet, stop broadcasting your life live.

## Continue Reading

* [Impact of digital technologies in breaking barriers](/impact-of-digital/)
* [Sweet home is a safe home, even digitally](/sweet-home-safe-home/)
* [Startups And Security](/startups-and-security/)