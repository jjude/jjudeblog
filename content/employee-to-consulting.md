---
title="How to transition from employee to one-person biz?"
slug="employee-to-consulting"
excerpt="Mindset shift needed for a successful solopreneur business"
tags=["coach","biz"]
type="post"
publish_at="20 Jul 23 19:29 IST"
featured_image=""
---
I began as an independent advisor to a cardiac surgeon, tasked with computerizing the entire unit. I created their forms, developed software, and trained their staff. However, India offered few opportunities for solo consultants at that time. Not wanting to start a business, I joined another company as an employee.

In 2009, though, I returned to consulting and never looked back. Mostly, I contract for one company—my steady income source—but also run a coaching program and do due diligence on digital products for venture capitalists. These activities bring additional revenue but my primary earnings come from consulting.

Being a solo businessman suits me; it's not about money but freedom and family time—I only work three days per week. Besides the coaching program, I host a podcast and give talks at local colleges.

Even now in IT industry in India being an individual consultant is challenging. Some people ask how can they become solo consultants here? This post narrates my journey of becoming—and remaining—a solo consultant.

## Believe it is possible

When I made this switch in India, there were no role models or precedents in the IT industry. Most people I knew were employees. Some thought my idea was foolish as Indian organizations didn't support such a model.

I tried to become a consultant at my previous job but failed. However, I persisted and kept searching for opportunities.

For me, an opportunity came to consult for the government of India. Despite its challenges— potential termination at any point and politics—I took it because without risk there is no reward. After that there was no turning back.

So yes, becoming a one-person business in India is possible if you believe and are willing to take risks..

## Develop business owner mindset

Transitioning from employee to solo consultant is tough. It requires a shift in mindset: you're no longer just an employee, but the boss. You must find work, market yourself, sell your services, deliver results and chase invoices. All this responsibility can seem daunting.

**Three mindset shifts** are key for success as a solo consultant.

Firstly, develop an **abundance mentality**. As an employee, you receive a fixed income and operate within that limit. But as a business owner or consultant, your earning potential expands exponentially - even doubling or multiplying tenfold if played well. This doesn't mean being solely money-driven; it might be about having more time with family or working less while maintaining income levels - like me who works three days per week.

Secondly, understand that **reality is negotiable**. In both employment and business negotiation skills are crucial but they're particularly important when running your own show since outcomes often hinge on successful negotiations. You should know what you want and willing to let go to gain what you want. I wanted to live in a nice place and spend time with growing sons. I was willing to let go of high salary for that freedom.

Lastly adopt the **'peer' mindset** when interacting with clients rather than acting subserviently because of their title or company size. Regardless of whether you’re hired at developer level or management level remember you too are a business owner, albeit maybe smaller-scale, which means treating them as equal peers not superiors.

These changes aren’t easy especially if long ingrained with traditional employment attitudes yet mastering these mindsets will significantly smooth transition into consulting role.

## Attracting work

As a solo consultant, you're the business. You market yourself, draw in work and deliver. It's not as hard as it seems if you know what to do.

Firstly, **create content**. This is how you prove your consulting expertise. Discuss the problems you solve, share your thought process and successes, and offer thoughts on evolving technologies. If you have the opportunity to speak in public, take advantage of it since it helps establish you as an authority. An even better option is to publish a book (I have).

Next up: **build your network**. Not thousands of contacts but a tight-knit group—say 15 to 50 people max, who'll speak for you and refer clients to you—a team working for your benefit. Keep them updated through regular meetings or updates; they may even bring fresh opportunities.

Lastly: **build social proof**—a deciding factor for leads considering hiring you. It's about trust-based referrals from respected sources that really matter—not degrees or certifications—they'd call immediately when such referral comes their way.

Consider this example—you're at Taj Mahal approached by two photographers—one just offers his service while the other shows his portfolio too—who would you hire? When someone vouches for another person’s work competency—they are given preference—so build that social proof of expertise and capability—that speaks volumes!

In sum: create valuable content (video, text etc.—a book would be supreme), cultivate a supportive network and amass solid social proof demonstrating competence!

## References

Most my thought-process is modeled around Alan Weiss' concept. I own many of his books and implemented the principles he talks about. If you want to establish yourself as a solo-consultant, there is no better book than "[Million Dollar Consulting](https://geni.us/mil-dollar-consulting)"

## Continue Reading

* [I have seen the future of jobs and it is Hollywood model](/future-of-jobs/)
* [Mastering sales as a new CTO](/cto-sales/)
* [Life is series of lost, found, and lucky breaks](/life-is-lost-and-found/)