---
title="Should You Run That Extra Mile to Make a Sale?"
slug="extra-mile-for-sale"
excerpt="Customer is king, is a mantra that Sales team lives by. But there are times we need to differentiate between bad customer service and annoying customer demands."
tags=["coach","startup","sales"]
type="post"
publish_at="30 Jun 10 16:30 IST"
featured_image="https://cdn.olai.in/jjude/extra-mile-for-sale-gen.jpg"
---


'Customer is king' is a mantra that Sales team lives by. But there are times we need to differentiate between bad customer service and annoying customer demands.

There is a scene in the famous British romantic comedy - Notting Hill. In that movie, William Thacker - the protagonist - runs a travel book shop. One day a well dressed customer walks into the shop and the conversation goes something like this:

Customer: Do you have any books by Dickens?

William: No, we're a travel bookshop. We only sell travel books.

Despite that clarification from William Thacker, the annoying customer goes on to ask for a 'John Grisham' thriller and 'Winnie the Pooh'.

At that point, what is expected of William Thacker? Should he send his assistant to get the latest John Grisham thriller and hand it over to the customer as a show of exemplary customer service? Isn't that what customers demand so often?

Such scenes are repeated all over the world in the business. Sadly, companies entertain such annoying customers only to loose reputation of their business. Companies fail to understand that they can not satisfy every one of their customers. If they can satisfy 80% of their customers well and that 80% can pass on their satisfaction to others, businesses will flourish.

In later part of the movie, there is an interesting sequel. The same customer will be entering the shop. And immediately William Thacker will command the customer to leave. He was 'firing' the customer.

Established companies often restrict their customer base. Such restrictions give resources - both time and energy - to these businesses to concentrate on their selected customer base and please them well.

## Continue Reading

* [Delightful Customer Experience in a Highway Restaurant](/cx-in-highway/)
* [Customer Service Is Sales](/customer-service-is-sales/)
* [System for success](/system-for-success/)
