---
title="Agile in the C-Suite"
slug="csuite-agile"
excerpt="This is the best of times and worst of times to be a CXO. Here is my story of how I am learning to be a CXO using agile principles."
tags=["coach","talks"]
type="post"
publish_at="04 Jun 18 10:30 IST"
featured_image="https://cdn.olai.in/jjude/principles-matter"

---

![Talk at Agile Chandigarh Conference](https://cdn.olai.in/jjude/2018-agile-talk.jpg)

_This is the blog version of my talk at Agile Chandigarh event, Inspire 2018_

When I became a chief technology officer 3 years ago, only one question occupied my mind.

Am I going to be a proof for Peter Principle? For those of you who don’t know about it, it says every competent person raise to their position of incompetence.

There are quite a few examples of this Peter principle.

Marissa Mayor was a great product manager at Google. She got hired as a CEO of Yahoo, where she couldn’t perform as good as at Google. Closer home, we had Manmohan Singh. He was an awesome finance minister. But he lost his reputation and good standing when he became a prime minister.

What follows is my story. The story of how agile principles helped me defy Peter Principle. It could be your story into c-suite, if you follow the principles outline here

C-Suite is filled with chief officers - chief sales officer, chief technology officer, chief operations officer, and so on. All of them are led by the chief of all them, the chief executive officer.

Let us first examine **what it means to be a chief officer**.

### CHIEF officer - one with a vision

Peter Thiel places people and societies in a 2 x 2 matrix. People are either optimistic or pessimistic about the future. An optimist thinks future is better and a pessimist thinks otherwise. A determinant person thinks the future is knowable and so can plan for it. An indeterminate person thinks the future is unknowable.

![Chief](https://cdn.olai.in/jjude/chief.png "Who is chief")

A determinant optimist has a vision of the future. JFK said, "I believe that this nation should commit itself to achieving the goal, before this decade is out, of landing a man on the moon and returning him safely to earth." He was a determinant optimist. He wasn’t generally optimistic. He didn’t say, space technology is going to be important in the future. So let’s commit ourselves to conquer space. Let’s give our best shot. No. He was specific - man to the man and back within a decade. When a determinant leader rallies, the nation follows. We all know that the US achieved his vision, though he didn’t live to see it.

On the contrary, an indeterminate leader says, “There is no reason to be disappointed. India will progress very fast and the skills of our youth will take India ahead.” There is no vision in this statement. It is generally positive. Should Indian youth focus on manufacturing, or on services, or on R&D? No definite answer.

We have determinant optimists on the corporate side. Take Jeff Bezos. He wrote his first letter to his shareholders in 1997.

> Today, online commerce saves customers money and precious time. Tomorrow, **through personalization, online commerce will accelerate the very process of discovery**.

He wrote this 20 years back! That's a determinant optimist.

Elon Musk is another determinant optimist. He wrote this in 2006 on his [blog](https://www.tesla.com/blog/secret-tesla-motors-master-plan-just-between-you-and-me):

- Build sports car
- Use that money to build an affordable car
- Use that money to build an even more affordable car
- While doing above, also provide zero emission electric power generation options

**A chief is expected to have a vision. A vision of the future.**

Having a vision isn't everything. Determinant optimists also find means to get that future by passion, pursuit, and positivity. That is when he becomes an officer.

### chief OFFICER - Beware of the busy officers

Most executives boast, "I work for 16 hours a day" or "I have 1000 unread e-mails" or "I have not taken a single day off in two years" and so on. They **sound good but don't do any good** for the organization.

They run on a treadmill going nowhere.

![Officer](https://cdn.olai.in/jjude/officer.png "Officer")

I talked about Peter principle earlier. Great officers know that what got them here won’t get them there. So they start to **acquire the competence** to be their best in their current role. They seek out mentors, they attend relevant workshops, and they read books to equip themselves for their role.

Great officers also get things done. Many executives believe that when they hit the c-suite, they don't have to "do" things. They believe that they have to only delegate and direct. That is not true. Oftentimes, they have to roll their sleeves and do things. But not any task. Not every task. Only those **tasks that move their organization toward the vision**.

### Principles trump practices

Some people come to agile conferences, attend a workshop on Jira, and conclude that if they use Jira then they too are agile. C-suite officers aren’t an exception to this **temptation of latest practices and tools**.

![Principles trump practices](https://cdn.olai.in/jjude/principles-matter.png "Principles trump practices")

They might try to become a better CEO by wearing a turtleneck like Steve Jobs. They might listen to a professor from London School of Business and then spend their energy in framing an "elevator pitch".

A comedy scene from one of the Tamil movies highlights this phenomenon really well.

> There is a pair of comedians in Tamil movies, who can be compared to Laurel and Hardy. They together have produced many hilarious scenes, some of which evoke laughter even today. Below scene appears in one such comical scene.

> In this movie, [Goundamani](https://en.wikipedia.org/wiki/Goundamani) is head of a village and [Senthil](<https://en.wikipedia.org/wiki/Senthil_(actor)>) is his assistant. Goundamani is smart, knows what to say in any situation, and is well respected in the village. Goundamani is haughty and never misses a chance to deride Senthil. Senthil envies him and wants to prove he is smart too. He accompanies Goundamani everywhere and observes what he says in every situation.

> In one such situation, an elderly woman of the village passes away. As usual, Goundamani is invited and Senthil tags along. "She wasn't just a mother to you," he says to the family that lost their mother, "she was a mother to all of us." He goes on with his eulogy, but Senthil notes down this point.

> At the climactic scene, another lady, a wife this time, dies. Goundamani is sick and can't leave the bed. Senthil recollects the earlier scene and offers to go to the funeral.

> You guessed what he would have said, didn't you? "She wasn't just a wife to you," he cuts & pastes the words, "she was a wife to all of us."

Many conversations in c-suite are comical like this.

Latest trends, tools, and practices are visible. They are easy to copy. They are the seed for fun talks in the cocktail parties.

Principles are the opposite. Nobody enjoys squeezing their brain challenging it’s cemented beliefs. Yet **principles determine whether you thrive or fade away**.

If you understand the principles well, you could use any tool to achieve the results. In fact, if you understand the principles well, you would be further than those who use the tool without an understanding of underlying principles.

### Improving decision quality

An executive faces many challenges daily. These challenges could be internal as well as external.

Newer trends, newer regulations, new business models are all external challenges. Keeping the employees motivated, delivering projects on time and within budget, hiring competent employees are all internal challenges. To face these challenges he has to take a ton of decisions daily.

In almost every case, he has to **decide with incomplete and imprecise information**. How to decide in such uncertain circumstances?

![Improving Decision Quality](https://cdn.olai.in/jjude/decisions-quality.png "Improving Decision Quality")

Most successful executives have a **decision-making process**. As I explained earlier, a chief officer is always betting on one of the many versions of the future. They divide their journey to that vision into milestones, conduct small experiments, get feedback, and decide next course of action. As they do this, they improve their decision making quality about choosing experiments.

A sound decision-making process improves the eventual outcome.

### How agile principles help in the c-suite

If I were to summarize Agile it would be this:

> Building a product iteratively and incrementally in small work cycles with feedback from those who matter.

If you replace _a product_ with _future_, then the above statement is true for what happens in the c-suite.

Each of the officers in the c-suite creates their part of the company future. They can't achieve this overnight. They create this future one milestone after another in an incremental and iterative way.

Let us take an example: Say a determinant optimistic CEO bets on a **conversational commerce** as the future. He himself would've decided this after scanning the market and observing trends. When he bets the company's future on it, each of the CXOs start working on their part.

The CTO (chief technology officer) team might build a small Facebook bot that sells an e-book. The bot itself might have enough tracking built in to give the required feedback — how many people interacted with it, how many of the conversations were answered by the bot, how many people bought the book, and so on. The CMO (marketing) team might build a campaign around the bot to drive traffic to the bot. The CSO (sales) team might call up existing commerce customers to demo this product to understand if there is interest in this product.

This is an experiment. As you can see, it is **as small as it can be**. It sells one e-book on one channel and converses in one language. Data from this experiment will either prove or disprove the hypothesis of the future that the CEO has. Data might point to one of the following:

- People love it
- People are confused about it
- Customers want to build a proof of concept for them.

The c-suite team will use this **feedback to calibrate** their next action. Next action could be:

- Build for other channels
- Build to sell an Amazon product
- Build a SAAS product

And the cycle continues until that future is created. Usually, determinant optimists aren't the ones who change their mind about the future after every sprint. They might correct their course, not the destination. Sometimes they might focus on other initiatives and come back to this at a later time.

### OKR - Agile for C-Suite

We have been following OKR as a goal setting framework for the past two years. We have seen amazing results out of it.

![OKR - agile for c-suite](https://cdn.olai.in/jjude/okr.png "OKR")

The acronym OKR stands for **Objectives and Key Results**. Andy Grove introduced it at Intel when he was a president. Google, Twitter, and other Silicon Valley companies use it now. OKR is a battle-tested framework for at least 30 years in many different companies.

OKRs are set by answering **two simple questions**:

1. Where do I want to go?
2. How will I know I'm getting there?

The Objective is a qualitative goal for a longer time-period, usually a year or two. The Key Results are a quantitative measure that confirm the fulfillment of the objective. Key results are usually set for a quarter.

You can write down the OKRs as:

> I will achieve (objective) as measured by (results).

You will recognize that this is **similar to the format of user stories**.

> As a xx , I want xx , so that xx

This is not the only agile principle at play in OKR. The key results are time-boxed to conduct retros often. The key results are built incrementally and iteratively towards the objective.

### Agile is more than a project delivery model

This is the **best of times and worst of times to be a CXO**. The quantum and depth of business challenges thrown at executives can paralyze even the seasoned ones. Buried within those challenges are the never ending opportunities for the same executive.

Traditional long-term planning is no match to deal with this dynamic interplay of challenges and opportunities. **Evolving business environment needs a matching evolving strategy**.

If you follow the agile principles, you will build your future one work-cycle at a time.

_You can view slides for this talk in [Slideshare](https://www.slideshare.net/JosephJude4/agile-in-csuite-99737030)_

## Continue Reading:

- [I have seen the future of jobs and it is Hollywood model](/future-of-jobs/)
- [Mastering sales as a new CTO](/cto-sales/)
