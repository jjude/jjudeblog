---
title="Gravitas WINS Radio interviews categorized by theme"
slug="gwr-themes"
excerpt="If you want to build gravitas and attract luck, this show is for you."
tags=[""]
type="page"
publish_at="22 Feb 23 20:21 IST"
featured_image="https://cdn.olai.in/jjude/all-interviews.jpg"
---

![Gravitas WINS Radio Guests](https://cdn.olai.in/jjude/all-interviews.jpg "Gravitas WINS Radio Guests")

## Business
- [Arvindh Sundar on 'Gamification of marketing for small businesses'](/arvind-gamify/)
- [Benjemen Elengovan on 'Employee-like benefits for the self-employed'](/benji-self-employed/)
- [Gaurav Aggarwal on 'How to sell on Amazon US from India'](/gaurav-amazon-fba/)
- [Kavitha Ashtakala On 'Championing Customer Success'](/kavita-customer-success/)
- [Manish Verma on 'How Angel Investors Think?'](/manish-angelblue/)
- [Munish Jauhar on 'Building A Business From A Tier-2 City'](/munishj-on-biz-from-2nd-tier-city/)
- [Naseef KPO on 'Lean video marketing'](/naseef-lean-video/)
- [Maj Gen Neeraj Bali on 'Building the winning culture'](/culture-neeraj/)
- [Rishabh Garg on 'Power of Twitter, Side-hustle, and Community'](/rishabhg/)
- [Roshni Baronia On 'Digital Sales For Startups & SMEs'](/roshni-digital-sales/)
- [Samson Selladurai on 'How not to operate a startup'](/samson-failures/)
- [Sohail Khan on 'Why startups fail'](/sohail-tbc/)
- [Shresth Shrivastav on 'Thrive In Gig Economy'](/shresth-shrivastav-gig-economy/)
- [Sathyanand on 'The Visual Solopreneur'](/sathya-visuals/)
- [Sukhwinder Singh on 'Mantras for going from ₹1200 to ₹1 Crore of sale'](/sukhwinders/)
- [Tanmay Vora on 'Mindset For Creator Economy'](/tanmay-creator-economy/)

## Career
- [Abhinav Goel on 'Product Manager In A VUCA World'](/abhinavg/)
- [Dr Deborah Thomson on 'One Health And Science Communication'](/deborat/)
- [Emmy Sobieski on 'I'd far rather be lucky than smart'](/emmys/)
- [Gaganpal Singh on 'Good Sales Skills Are Same As Good Life Skills'](/gaganpals/)
- [Ian Heaton on 'Listening leaders are effective leaders'](/ian-listening/)
- [Jayaram Easwaran on 'Secrets of A Master Storyteller'](/jayarame/)
- [Meenakshi on 'Carving a unique career path'](/meenakshi-career/)
- [Mohan Mathew on 'Management Consulting'](/mohanm/)
- [Ranjitha Jeurkar on 'Nonviolent Communication'](/ranjithaj/)
- [Ruby Grace on 'Impact of technology on mental health'](/ruby-mental-health/)
- [Ritika Singh on 'Words Change Lives'](/ritikas/)
- [Capt. Sahaya Arputharaj on 'Leadership on High Seas'](/sahayaa/)
- [Tamizhvendan on 'Teaching software fundamentals in Tamil'](/tamizh-sw-funda/)
- [Ubellah Maria on 'Transitioning From Developer To Manager'](/ubellah-maria-becoming-a-manager/)
- [Xavier Roy on 'Personal knowledge management for career growth'](/xavier-pkm/)

## Networking
- [John Mohana Prakash on 'Podcasting Lessons From An Internet Radio Host'](/john-podcast-lessons/)
- [Krishna Kumar on 'Power of storytelling in business'](/kk-sotry/)
- [Naveen Samala on 'Benefits of podcasting'](/naveen-podcast/)
- [Rajesh Madan on 'How To Build A Thriving Community'](/rajesh-community/)
- [Sampark A. Sachdeva On 'Linkedin Success Mantras'](/sampark-linkedin/)

## Parenting
- [Mahendran Kathiresan on 'Homeschooling In India'](/mahendrank/)
- [How our fathers, society, and peers shape our fatherhood](/fatherhood-influences/)
- [How we learn about fatherhood?](/learn-to-be-father/)
- [Bonding with kids](/bonding-with-kids/)
- [Building family culture](/family-culture/)
- [Dealing with Dad Anxieties](/dad-anxieties/)

## Politics
- [Dr Sarika Verma on 'Why politics is important for all of us'](/sarika-politics/)

## Self-Development
- [Anuj Magazine on 'Sketchenoting And Learnings'](/anujm/)
- [Manjula Sularia on 'Life Skills For Personal And Professional Success'](/manjula-life-skills/)
- [P. K. Khu₹ana on 'Happiness, Joy and Bliss'](/khuranapk/)
- [Siddhartha Ghosh on 'What we can learn from nature'](/siddharthag/)

## Technology
- [Ganesan Ramaswamy on 'Building An  Enterprise Data Strategy'](/ganesanr/)
- [Jinen Dedhia on 'Low code tools in enterprises'](/jinen-dronahq/)
- [Jyothi & Aravinda on 'Why Robotic Process Automation Matters To Enterprise'](/jyothi-rpa/)
- [Krishnakumar on 'Solve and scale with Generative AI'](/genai-for-leaders/)
- [Liji Thomas on 'Conversational Bots'](/lijit/)
- [Lee Launches on 'Application Development With No Code And Low Code Tools'](/lee-lcnc/)
- [Manish Kumar on 'Edge computing'](/manishk-on-edge-computing/)
- [Prakasm Anand on 'Printing Hearts, Parts, And Home'](/anand-3d/)
- [Shikhil Sharma on 'Talk With Me Security'](/shikhils/)
- [Tamanna Sharma on 'Your Roadmap To A Career In Data Science'](/tamanna-ds/)
- [Vikrant Sukhla on 'Trends in Digital Commerce'](/vikrant-commerce/)

## Wealth
- [Priyanka Sud on 'Protect your wealth with will'](/priyanka-will/)
- [Rahul Chauhan on 'Learning from Annual Reports'](/rahul-ar/)
- [Regi Thomas on 'Investing for short-term goals'](/regi-short-invest/)
- [Rishi Jiwan Gupta on 'Financial Planning For A Meaningful Retirement'](/rishi-retire/)
