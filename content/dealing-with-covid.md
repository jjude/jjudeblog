---
title="How I'm dealing with isolation during covid"
slug="dealing-with-covid"
excerpt="Create a sense of togetherness and a purpose, to get through difficult times."
tags=["coach","tiechd","parenting","networking"]
type= "post"
publish_at= "31 Mar 20 08:30 IST"
featured_image="https://cdn.olai.in/jjude/tie-covid-session.jpeg"
---

> A surprise to no one at this point, but still a hell of a thing to see. This is the first time the Olympics have ever been postponed, and they've only ever been canceled during World Wars I and II. - [Daring Fireball](https://daringfireball.net/linked/2020/03/25/tokyo-olympics)

Like many of you, the coronavirus pandemic has changed my world. Ever since this scare started spreading wildly, I have been swinging between extremes — from "the world is going to end tomorrow" to "we will get through just fine", and all stages in between. We — kids, wife, and I, have not stepped out of our home for the past ten days. Our old routines, every one of them, are thrown out of the window.

The initial days were chaotic. As a [Chief Technology Officer](https://www.linkedin.com/in/jjude/), I had to roll out a secure "work from home" program for our employees. As a father, I had to engage two boys in creative pursuits, so they don't get addicted to TV. Finally, as a household partner, I have to do my part to continue to have a sweet home for all of us. It was **overwhelming** when it all started.

Thankfully I formed new routines, which I share here. Take what suits you, make it fit you, and enjoy the days ahead of you.

### Isolate yet Socialize

"Sitting at home" is the best thing to happen for an introvert. But, I also know that if I isolate myself for too long, I'll get into depression. So I'm using every tool available to **stay in touch with people**. I'm contacting them on Whatsapp, phone, and zoom. I'm talking to at least two persons every day — not counting the work-related discussions.

I'm also conducting Bible study over zoom for two of our church members. We used to meet at homes for this earlier, but now we have switched to zoom calls.

All of these have given me **a sense of togetherness and a purpose**. I have never been so connected as in this isolation time.

### Fitness

I'm already overweight (84 kg) for my height (172 cm). By locked at home and sitting in front of the computer every day  without much body movement, I would put on even more weight. How can I counter that? I'm **exercising and going on intermittent fasting** to counter fat settling in my body.

Before this lockdown, once in a while, I would fast for 12 hours. I would have dinner at 8 pm, and then breakfast at 8 am. During this lockdown, I increased the hours slowly, and now I fast for 16 hours. I eat dinner at 8 pm, skip breakfast, and eat lunch at 1.30 pm. That makes it as 16 hours of fasting. I drink three black coffees in between, and nothing else. I've been fasting for about a week now. So far, it is good.

Along with fasting, I exercise. 

![QuarnatineExercises](https://cdn.olai.in/jjude/quarantine-exercises.jpg)

I exercise for about 30 min daily in the morning. I mix and match exercises so that I am not bored. I'm listing the usual exercises here. The numbers in the parenthesis are the counts and not the minutes:

* Squats (50)
* Lunges (10)
* Skipping (250)
* High Knee skipping (50)
* Jumping jacks (75)
* Plank
* Donkey Kicks (20 each leg)
* Push-ups (15)

Exercises in the morning give me the freshness I need to start the day in a positive mood.

### Kids

I have two boys aged ten and seven. They are polar opposite in everything. The elder one likes to read books; the younger one can't read ten pages sitting; the elder one is creative; the younger one can't sit in a place for ten minutes. It is **difficult to manage them together**.

For the past two weeks, I have trained them to join me in the exercises. They do all the exercises I do but in small numbers. I make it fun for them, so both of them look forward to it.

After exercise, we play mini-football and dodging ball. All of these physical exertions help them to sleep well at night.

They have created their own [YouTube channel](https://www.youtube.com/channel/UCkMTYnmOdNndliks8c2ueZg). I encourage them to create as many videos as they can. The elder is interested in creating stop-motion and AR videos. The younger one imitates his brother in creating videos. 

![Garretts Youtube](https://cdn.olai.in/jjude/garrettsyoutube.jpg)

These are my favorites from their channel:

* [Story Of A Poor Man Who Became Rich](https://www.youtube.com/watch?v=PFDzWwT_cYo)
* [Lego Stop Motion - Flip Flop of Robbery](https://www.youtube.com/watch?v=RCiiMLHwywk)
* [An AR War Movie](https://www.youtube.com/watch?v=0ATFiHVihyk)
* [Clear Couch With Magic](https://www.youtube.com/watch?v=AT7qJNt7oP4)

During the day, they read the Bible, [Geronimo Stilton](https://geronimostilton.com/) books, and write a story or something akin to that.

Khan Academy has an [Introduction to story telling](https://www.khanacademy.org/partner-content/pixar/storytelling/we-are-all-storytellers/v/storytelling-introb) course. The elder one is hooked to it. The other day he was describing 'character arc'! Maybe this **lockdown will turn them interested in storytelling**.

At night we have dinner together. We play math, geography, and word games during dinner.

So far, my wife and I have managed to keep them entertained and engaged. They miss going out for shopping, dinner, and just playing with his friends. I don't know how long before they get bored.

### Learning

I have been part of [TiE, Chandigarh](https://chandigarh.tie.org/). TiE Chandigarh, in particular, and TiE, in general, conducted many online sessions for business executives to deal with this volatile season. **Borrowing on other's ideas and knowledge is a sure way to scale up fast**. These sessions helped me to think through the situation from different perspectives and to rollout the "work from home" program in a secure way.

![TiE Learning](https://cdn.olai.in/jjude/tie-covid-session.jpeg)

Many colleges and online learning platforms are offering their courses for free or on deep discounts. Yale is offering its [popular course](https://news.yale.edu/2018/02/20/yales-most-popular-class-ever-be-available-coursera) titled, "The Science of Well Being" for free on [coursera](https://www.coursera.org/learn/the-science-of-well-being). It only seems fit to take this course at an uncertain time like this. I finished one week course in a day. I like it so far, and I might even finish it and be happy.

Are there any interesting online conferences? Share them with me on [Twitter](https://twitter.com/jjude/) or [LinkedIn](https://linkedin.com/in/jjude/).

### Investing

> Be fearful when others are greedy and greedy when others are fearful - Warren Buffett

Now is the **best time to buy quality stocks** as a value investor. I use [Screener](https://www.screener.in/) tool to track some of the best-run companies with a good balance sheet. I hope to sow well in this season so I can reap well in a decade.

### Work

Until this pandemic hit us, we were optimized for working from an office, with a well-architected building and best-in-town facilities. Thankfully we woke up early and ran a pilot program way before it became necessary, which gave us enough time to fine-tune our options and roll out the "work from home" program in about a week for all our employees.

It also helps when your [CEO](https://www.linkedin.com/in/samjain/) takes decisions quickly rather than asking to compare and debate options endlessly. We migrated to [Slack](https://slack.com/) as a communication tool (from skype). Slack allows us to bring everyone to a single platform, thus making it easy to broadcast our plans to everyone. We have conducted weekly reviews using its audio calling facility. We are using [AWS Workspaces](https://aws.amazon.com/workspaces/) generously as a "cloud desktop", wherever required.  Rest everything runs on-prem accessible via VPN. We anticipate a form of compulsory "work from home" program to continue even beyond the 21-day lockdown in India. So we are in-midst of moving all our development pipeline, like the CI/CD process, to the cloud.

### Stay home, stay healthy

> A healthy man wants a thousand things, a sick man only wants one - Confucius

Sometimes I think we're overreacting. The last time I went out of India was on a personal trip to Dubai a year back. Our employees don't travel abroad for work. I usually don't go on partying or meeting others.

When I hear the rising number of infected cases and deaths in countries that have not taken a drastic measure like lockdown, I think it is prudent to stay and rest at home rather than ending up resting in peace without a proper funeral.

Create a sense of togetherness, form new routines, and find a purpose. You'll get through this difficult times. Good luck.

## Continue Reading

* [Life is series of lost, found, and lucky breaks](/life-is-lost-and-found/)
* [How to choose technology for your business growth](/tech-for-biz-growth/)
* [Teaching storytelling to kids](/storytelling-to-kids/)