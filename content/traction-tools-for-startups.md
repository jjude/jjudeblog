---
title="Essential tools to build traction for your startup"
slug="traction-tools-for-startups"
excerpt="Here is a step-by-guide for all the essential tools to build traction for your startup"
tags=["tech","martech"]
type="post"
publish_at="06 Aug 15 03:36 IST"
featured_image="https://cdn.olai.in/jjude/traction-tools.png"
---
Building and launching your product is only half the battle. Other critical half is gaining traction and converting that traction into customers.

Thankfully successful startups have published how they built traction. Most common traction channels are:

- Email marketing
- Search engine marketing
- Blogging
- Blogging on other high traffic sites
- Community building
- Advertisements
- Affiliate programs
- Social media marketing

(You can read about these channels in the book aptly titled Traction)

The book and tons of other blog posts give you a good introduction to these channels. Do you know what is missing? A comprehensive guide on how to use a tool in each channel to start experimenting the channel. There are so many tools in each channel that much of your effort is wasted in searching those tools and figuring out how to get started with a particular channel.

So I decided to put together a series that will explore a tool in detail under each channel.

![Traction Tools For Startups](https://cdn.olai.in/jjude/traction-tools.png)

**Email marketing**: Email remains the most effective marketing channel, despite the hype about social media platforms like Twitter, Facebook, and LinkedIn. According to a [McKinsey study][1], Email converts 40 times more than Twitter and Facebook combined. With such high conversion rates, startups may well focus solely on email marketing. Zoho is a free tool for email marketing. Read the [step by step guide][2], to setup Zoho Campaign and start your email marketing.

**Search engine marketing (SEM)**: When is the best time to place an ad for your product? When a user searches for it, of course. Pick the best keywords for your product and place an ad in Google. When users searches using those keywords, they will see your ad and if they click the ad, they will land at your website.

**Landing pages**: When a user clicks on an ad, they should land on a specialized page on your site designed for nudging the user to take a specific action. It could be subscribing to your newsletter, download an e-book or pay for your product. Though there are sites that offer landing pages as a service, I prefer to create them and host them on my own domains.

**Blogging**: Blogging falls under the broader bucket of content marketing. When you write free and valuable content, you attract potential customers towards your product. If your content matches the problems faced by the potential customers, they are most likely to engage with you and use your product. Wordpress is the most popular blogging platform, but there are other [emerging blogging platforms][3]. I will write a detailed post on [Olai][4], a blogging platform that I developed to create a fast, secure and scalable blog.

**Community building**:  The most successful companies build a community around their products. Building a community is a slow process but if you build it right, it becomes easier to launch successive products to your community. If you are in the entrepreneurship for the long haul, you should start building a community.

**Support tools**: Building traction depends on variety of support tools like password manager, writing tools, and URL shorteners. These support tools will help you focus on building the traction in a fast pace.

I'll be adding more tools and step by step guides in this section. Bookmark it and visit often or [subscribe][5] and get the new guides via email.


[1]: http://www.mckinsey.com/insights/marketing_sales/why_marketers_should_keep_sending_you_emails
[2]: /zoho-campaign-101/
[3]: /blogging-platforms/
[4]: http://olai.in
[5]: /subscribe/
