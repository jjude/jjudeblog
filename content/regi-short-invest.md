---
title="Regi Thomas on 'Investing for short-term goals'"
slug="regi-short-invest"
excerpt="Unlocking short-term investing success - Strategies and avoidable mistakes."
tags=["gwradio","wealth"]
type="post"
publish_at="03 Oct 23 17:00 IST"
featured_image="https://cdn.olai.in/jjude/regi-yt.jpg"
---
Can you invest your short-term funds for a better return? Regi Thomas introduces ladder of risk so you can consider the different options for short-term investing. Join us on a journey to discover the strategies that work and the common mistakes that can hinder your success in the world of short-term investing.

<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless src="https://share.transistor.fm/e/c13e1cfa"></iframe>

## What you'll hear
- Investing for short-term goals
- Definition of short-term investing
- Avenues for short-term investing
- What to optimize in short-term investing?
- Impact of fees on returns
- Play of advisory firms
- Can we be tax efficient with respect to investments?
- Diversification of short-term investments
- Common mistakes people do
- Emerging trends to track
- How to stay informed and adapt our investment strategy?
- Kindest thing anyone has done for you
- Best leadership quality and who manifested in your life
- What is your definition of living a good life?

## Edited Transcript
### Definition of short-term investing

I believe a concise answer suits short-term investing. So, you know, Joseph, the term you just used is actually an oxymoron.

Investing, by definition, implies a longer-term commitment. When you tack on 'short-term' to it, you're combining opposing concepts, right? However, in today's context, the world views 'short-term' as spanning from a few hours to perhaps a week, or maybe even a month. That qualifies as short-term. Then, there's the 'ultra-short-term,' where transactions occur within a single day, typically during regular working hours.

So, for instance, you might buy something around 11:30 AM and exit that position due to a price increase shortly after. That's the realm of ultra-short-term business. Therefore, today's definition of 'short-term' encompasses anything from a week to a month, all the way to 'ultra-short-term,' which can be as brief as a few minutes or a couple of hours. **Technically speaking, 'short-term investing,' as we commonly refer to it, is an oxymoron.**

So, exercise caution when using the term.

### Avenues for short-term investing

Alright, now let's talk about what we call the '**ladder of risk**.' This concept should help put things into perspective for those of you tuning in. As you climb higher on this ladder, you'll find that the risk becomes more pronounced. So, if you grasp this idea, you'll easily understand where I'm coming from. Essentially, the higher you go, the harder the potential fall. In other words, **as you aim for better returns, you're also increasing your risk**, similar to elevating your position from the ground, making a fall riskier.

Now, if you visualize this scenario and overlay your thoughts onto it, you'll see that at the bottom rung of the ladder, you have fixed deposits. We all know that banks are generally perceived as safe, offering returns of around 5-6 percent with almost zero risk, unless, of course, the bank goes under. So technically, there's very little risk involved.

As you move up one level, you encounter mutual funds. We discuss mutual funds because IT professionals and individuals in other businesses often lack the expertise and time required to build the necessary skills for in-depth analysis of their investments. They place their trust in the fund manager's expertise to grow their investments. This level is a few notches higher than fixed deposits. While the returns are potentially better, averaging between 8 to 15 percent per year, the associated risks increase because you're delving into market-related investments.

The third level on the risk ladder is the highest, where you directly invest in stocks. Here, you handpick stocks based on your insights into the future market trends. For instance, as we recognize the growing importance of AI, we might look for stocks related to AI technology. If, hypothetically, ChatGPT were listed, we could consider investing in it or similar AI offerings. We also see a rising demand for electric vehicles, so we might short-term invest in stocks related to that sector, anticipating price appreciation.

However, as we ascend this ladder, our risk levels also soar. Why? Because if the initial perception or belief behind our investment doesn't materialize in the short term, your money might stagnate at best or even decline if economic conditions take an unfavorable turn. In essence, this is the 'ladder of risk,' and it's all about finding the right balance between risk and return, especially when considering short-term investing.

### What to optimize in short-term investing?

When we talk about investing, I'd like to **draw a comparison to the agricultural system**, specifically farming. Just like when someone says they're a farmer, it's not like they plant their crop at 6 AM and expect to harvest it by noon to have lunch with it. That's just not how it works. So what's really crucial to understand here is the concept of time.

Time is what leads to appreciation. If you invest your money or sow your seeds in fertile soil, water them, tend to the weeds, and with the arrival of the monsoon season, you'll eventually reap a decent harvest. Keeping this in mind, let's look at stock investing. You invest your money in a stock, and you need to give it time to grow.

So, when we talk about time to grow in the context of short-term investing, I think the closest comparison I can draw is a period of about 90 days, Joseph. I'm talking about one quarter. Why a quarter? Because **companies typically release their financial results once every quarter, and there are four quarters in a year. So, it's a neat way to track your investment**. 

Now, here are **three key factors** to consider when looking at these 90-day investment periods. First and foremost, **time** is critical. You need to define a specific time frame, which, in this case, is 90 days. Second, you need to consider the quality and nature of the stock. Fixed deposits (FDs) and mutual funds (MFs) are straightforward choices, but when it comes to stocks, you need to be selective. Maybe it's an electric vehicle (EV) company, perhaps an AI-related stock, or even something tied to a current hot topic, like crude oil due to geopolitical events.

Third, it's important to grasp that investments linked to **items of mass demand** tend to be reliable. Take crude oil, for example; it's essential for various aspects of our lives, from powering vehicles to generating electricity. This podcast you're listening to right now relies on power and the internet, both fueled by energy sources. So, anything that has a significant impact on the masses is generally a good choice for a 90-day investment, which can help reduce risk perception.

So, to summarize, the key factors are time, the type of stock or investment, and ensuring that it's linked to something in high demand. Now, Joseph, I must add a word of caution. In today's investment landscape, price often takes center stage. People tend to look for stocks priced at, say, 100 or 150 bucks, hoping they'll shoot up to 5,000 in just three months. But let me be clear, there's no investment that guarantees such explosive returns in such a short time. It's just not a realistic expectation in the world of investing.

### When to exit short-term investments

We often return to the concept of a risk ladder, which is why I always stress the importance of sticking to the fundamentals. When you have a strong foundation, answering questions like these becomes much simpler. The lowest-risk investment option for most people is probably a Fixed Deposit (FD) or a savings bank account. In India, these accounts typically offer an interest rate of around 7.5% to 8%, depending on factors like age, such as senior citizenship or junior citizenship, among others. However, let's consider it as 8% for our discussion. Now, when you compound 8% over a 12-year period, that's precisely the kind of return you'd want to achieve, right? So, when you're evaluating your investments over a 90-day period, anything that yields over 8% can be considered fantastic. It's truly remarkable. That's undeniable.

Imagine this: even if you're looking at a modest 5% growth within a 90-day timeframe, when you extrapolate that over a year, it amounts to approximately 20%. Yes, indeed. That's more than 12% higher than the returns you'd get from the lowest-risk investment products in your portfolio. So, consider the timeframe when assessing your investments. **Anything exceeding 5% in a 90-day period is what I would label as a fantastic return**.

But, as I mentioned in my previous response, greed is a powerful motivator for all of us, and in some ways, it can be a good thing. However, it's essential to keep our greed in check. Anything in moderation is beneficial. Greed is, indeed, good, but it should be kept within reasonable limits. When you hit that 5% mark, most of us find it difficult to pull out. We want to push for that extra 1% or maybe even 2%.

That's where the risk factor comes into play. Sometimes, when the markets take a downturn, contentment eludes us, and if we let our greed get the better of us, we might end up incurring losses. So, remember, anything above 5% in a three-month period is an enticing prospect, but it's crucial to strike a balance and not let that greed lead you astray.

### Impact of fees on returns

When considering investments in India, it's important to divide your options among brokerage houses into two distinct categories. 

First, we have **fintech companies that specialize in execution capabilities**. These companies primarily offer you a trading platform without much else. Think of them as the Riverside FM of the financial world. Essentially, they provide a platform where you can easily register, conduct your transactions, and move on. What's notable about these platforms is that they typically **charge very low brokerages fees**.

On the other hand, there are **full-service brokers**. These brokers operate differently as they charge you a percentage of the total volume of business you conduct with them. They offer additional services beyond mere execution. For instance, they may provide research capabilities, access to a personal relationship manager for advice, and send you research reports, all at no extra cost.

Now, there are two sides to this coin. The full-service brokers, with their added services, are technically more equipped to assist you. They offer valuable research and advice resources. However, the execution-only brokers are, as the name suggests, primarily focused on providing you with a platform for trading. They don't offer research, and if you reach out to them for guidance, they may charge you for it.

So, the decision between the two boils down to your level of expertise and what you need. If you're well-versed in trading and have a solid grasp of your investment strategy, the execution-only brokers are an economical choice. Their costs are quite low, especially if you know what you're doing. However, if you're less experienced or prefer having access to research and expert advice, the full-service brokers might be a better fit. They typically charge around 0.2% to 0.3% of the total volume of business you conduct with them, which is the industry average. For example, if you're dealing with a hundred rupees' worth of transactions, it would cost you about 0.2% of that amount to work with them.

### Play of advisory firms

We serve **two primary types of clients**. First, there are those who have been with us for an extended period, some for over two and a half decades or even more. These loyal clients place great trust in our advice; they've witnessed the benefits of their long-term investments with us. We've helped them secure their financial future, not only for themselves but also for their children and now even their grandchildren. This group relies on us for guidance and sticks with us faithfully.

The second type includes individuals who live abroad, such as NRIs (Non-Resident Indians). The tax landscape in India changes rapidly and can be quite confusing, even for professionals. So, these clients seek extensive investment advice, particularly because the returns on investments here often outshine those in other countries. Therefore, NRIs often come back to India to invest, and they depend on us for our expertise in navigating the intricate financial landscape.

Lastly, there's the emerging group of neo-investors. This surge began post-pandemic and consists mainly of the younger generation, typically in their twenties, with an average age of around 26. Contrasting with the scenario 30 years ago when I entered this field, the average age of investors has dropped significantly. These individuals are open to receiving advice but are very cost-conscious. They prefer using execution-only platforms. They may initially engage with us to set up their accounts, especially since I frequently visit schools and colleges to conduct financial literacy programs.

We engage in financial education programs, ranging from MBA classes to undergraduate courses and IT companies. These younger investors do listen to us, but they tend to rely on their peers for investment decisions. In today's world, the word "advice" has taken on a negative connotation; they prefer alternative terminology. They'd rather follow their friends' recommendations. We've seen instances where they initially go with their peers' choices and later return to us for guidance. It's a learning curve for them, and our role is to empower them with the tools to execute their investments while offering advice on the necessary steps.

Our aim is to bridge this gap and provide investors with the capability to execute transactions while guiding them on the right path. This approach has gained popularity, and this universe of investors is expanding significantly.

### Can we be tax efficient with respect to investments?

Tax efficiency and investments often don't align. **What's efficient for the government tends to be inefficient for us**, right? Now, let's delve into this further, especially considering that today's podcast focuses on short-term investments. In India, short-term taxation lacks any semblance of efficiency.

When we talk about "short-term," we're referring to transactions, whether they result in a profit or a loss, that occur within a span of 365 days. Unfortunately, there's no tax advantage associated with these short-term dealings. You'll be subject to taxation based on the tax bracket you fall into. There are various tax brackets, each with its own set of rules. The old scheme and the new scheme have their own distinct tax brackets. These apply to short-term gains or losses, depending on the circumstances.

So, whether you make a profit of a hundred rupees or a lakh of rupees through short-term investments over the course of a year, you're liable to pay taxes on those earnings. There's no efficient way around it. Regardless of your short-term financial outcomes, you have a tax obligation to fulfill. In contrast, long-term investment strategies play by different rules and offer potential tax advantages, but when it comes to the short term, tax efficiency is essentially nonexistent. You simply have to meet your tax obligations.

### Diversification of short-term investments

The concept of the short term, Joseph, often leads people to view it as a bit of a lottery, you know? They think, "Okay, Reggie is sharing this today. Let me scratch the surface of this conversation and see if there's some luck hidden in there, and maybe I'll make some money." So, whether it's our investors or anyone else for that matter, many folks initially approach the short term as if it were a lottery.

However, it's worth noting that **diversification is a wise move at any stage of investing**, whether it's short term, medium term, or long term—doesn't matter. Diversifying your investments is a fundamental lesson we can draw from life itself. **It's similar to driving a car; you don't maintain a constant speed all the time**, right? There are highways where you can go up to 120 miles per hour and city roads where you can't exceed 25. So, you need to possess a range of skills to navigate those situations.

Just as you encounter various factors like two-wheelers and road obstacles while driving, investments also come with their own set of complexities and variables. Therefore, if you have a specific amount of money earmarked for the short term, consider allocating, say, 30 to 40 percent of it to the short term, but the remaining 60 percent should be diversified into the medium term. By doing so, you can potentially extend your investment horizon to six or nine months instead of putting everything into a three-month period like you're playing the lottery.

This approach offers diversification, allowing you to spread your risk and potentially yield more efficient results. It's akin to the old saying, "Don't put all your eggs in one basket," and it's a principle that applies just as well in the world of investments.

### Common mistakes people do

One of the most common behaviors I've noticed is that people tend to **follow the market noise**. Now, don't get me wrong; I'm not against the media. In fact, we're in the media business ourselves, and we recognize its importance. However, the media often strives for attention, and this can sometimes lead to sensationalism. For instance, consider a recent article about someone trading on the Bombay Stock Exchange (BSE) Sensex Futures, resulting in what's known as a "fat finger error." This occurs when someone mistakenly adds a couple of extra zeros to their trade volume, causing an unusual price movement. In this case, the individual incurred a loss of 78 lakhs within a matter of minutes. 

Now, when people read about such incidents, they might think, "Well, that guy lost 78 lakhs, but someone made that 78 lakhs." It's essential to remember that investing often involves a zero-sum game. If one person profits, someone else incurs a loss because trading occurs between individuals, not directly with the exchange. There's a facilitator involved; someone like Joseph buys from Reggie, and Reggie buys from Joseph. It's an exchange process. So, when people see these stories, they might be intrigued, thinking they can also make quick money. 

The second common behavior is **paying too much attention to external noise**. There are instances where stocks that were worth just 10 rupees a few months ago suddenly shoot up to 40 rupees. These are often referred to as "penny stocks." When a penny stock experiences such a rapid rise without a clear reason, it can be tempting for investors to chase after it. In these cases, investors might throw caution to the wind, neglecting research and the company's fundamentals, and simply hoping for a quick windfall.

The third behavior that poses a risk is **not having a well-defined plan for the short term and the long term**. I'm not against short-term trading; it can be enjoyable, just like driving at high speeds on a highway. However, you can't drive at 120 miles per hour all your life; even race car drivers don't do that outside of the track. Similarly, you need a clear financial plan that outlines how much you want to allocate for the short term and how much for the long term.

It's like setting a budget for your investments. You might decide that, for this year, you'll invest a specific amount, and let's say 20% or 30% of that will go into short-term plays where you might win or lose quickly. The remaining 70% should be earmarked for the long term, where you allow your investments time to grow. Not having such a clear plan is a significant risk. 

In summary, these are the three biggest risks in the order I've mentioned. Following market noise without a thorough understanding, getting caught up in the allure of quick gains without proper research, and lacking a well-defined financial plan can lead to significant challenges in the world of investing. Just like in agriculture, investments need time to work, and rash decisions can lead to losses, potentially jeopardizing your financial stability.

### Emerging trends to track
The day we landed Chandrayaan on the moon, and the rover rolled out, something interesting happened, Joseph. You can fact-check this if you'd like. The stocks associated with that lunar landing, companies directly involved, there were around 8 to 10 of them listed on the market. And here's the kicker: in the days following that event, those stocks shot up by more than 20%. It's worth noting that not all companies involved in such endeavors are publicly traded; some remain unlisted.

Now, we're setting our sights on the sun, figuratively speaking, as India's involvement in space exploration gains significance. A lot of this work is happening within our country, which is fantastic. However, many of these space-related companies are not yet listed on the stock market. Despite this, I believe that space exploration is an area with great potential. However, it demands patience, as missions to such distant destinations take time. For example, it takes about 154 days to reach a perigee position near the sun, and even reaching the moon requires 80 to 90 days. So, even in the realm of space exploration, short-term thinking, which typically spans around 90 days, might not apply.

Another area to consider, as you rightly mentioned, is **defense**. India has transitioned from being a defense importer to becoming a defense exporter, with several nations purchasing various defense equipment from us, including guns and missile technology. This sector is poised for significant growth.

The third noteworthy area is **telecommunications** and all things related to it. This includes data transmission through mobile phones, advancements in 5G technology, and various other aspects. Technology is at the heart of everything we do today, from financial transactions to communication and entertainment. The convergence of technology with mobility is a trend to watch, though we're still on the path toward full realization.

**Healthcare** is a compelling sector, given our large and growing population of 1.43 billion. Healthcare challenges are inevitable, and this sector holds significant potential, particularly in geriatric care and specialized housing for retirees.

Furthermore, **retail** is an evergreen investment in India. Whether it's food retailing, clothing, jewelry, or any form of retail, our consumption appetite is insatiable, and it's only growing with our expanding population.

So, to sum it up, consider a mix of these emerging and enduring trends: healthcare, retail, communication, defense, and space exploration, among others. These sectors present opportunities for investors, but it's essential to remember that some trends may require a longer-term perspective to fully realize their potential.

### How to stay informed and adapt our investment strategy?
I believe you should produce more podcasts like this one. It was quick and off the cuff, but it's essential to stay updated, even for professionals like us. That's one of the reasons I shifted from core mechanical engineering to financial engineering—the dynamic nature of the financial world always keeps us on our toes. What was true yesterday might not hold today, and it's unlikely to remain the same tomorrow because something is always changing. Geopolitical tensions, G20 meetings, lunar missions, earthquakes, droughts, El Niño—every global event, whether we like it or not, has an impact on financial markets.

These markets encompass everything from grain and energy to stocks, currencies, and commodities. The influence of any global event is concurrent and continuous, affecting the financial landscape as we speak. There's no single approach to staying informed, but I believe every investor should have a trusted advisor to turn to—not just someone who doles out tips via SMS promising quick riches, but an advisor who can provide insights into the outlook, current micro and macro trends, and the broader changes that may affect the next 90 days.

While journals and magazines can provide valuable information, I strongly discourage relying on television channels for financial advice. They often tell you what has already happened, not what's currently unfolding or what might occur in the next 90 days. You need a trusted relationship manager or advisor who can offer a forward-looking perspective.

**Managing money isn't as complex as it's sometimes made out to be**. In fact, it's quite straightforward. We tend to complicate it with jargon and terminology. I believe that financial education should begin at a young age, similar to life skills such as swimming or driving. Learning to manage money should be ingrained early on so that as individuals grow, they naturally develop the skills and the interest to stay on top of their financial game.

Whether you choose to work with a trusted relationship advisor or advocate for broader financial education, the key is to ensure that individuals have the tools and knowledge they need to make informed financial decisions as they navigate the ever-changing landscape of finance.

### Rapid-fire questions
#### Best leadership quality and who manifested in your life
I've had the privilege of working under two exceptional bosses, and what's particularly unique is that both of them share the same birthday as my father, the 7th of July. Let me tell you a bit about each of them.

The first leader I had the opportunity to work with was abroad, and he taught me the **invaluable lesson of what it takes to build and lead a team**. This experience was more than two decades ago when I was considerably younger, trying to find my footing in a foreign land. He was a Singaporean, and under his guidance, I learned the intricate art of team building. In the financial industry, learning the ropes and discovering what makes a great leader can be exceptionally challenging. People aren't always forthcoming with their secrets and insights, but he took me under his wing, and those lessons have stayed with me ever since.

The second leader I had the privilege of working with was based in India, and I spent a remarkable 17 years with his company. He provided me with **a level of trust and autonomy** that allowed me to grow and expand my horizons. When I first joined, he closely scrutinized my decisions, as any leader should. However, as the years passed, he began to grant me the latitude to experiment and take on more responsibility. If I look at where I am today, it's a result of the combination of these leadership styles. What I learned from them not only helped me develop my career but also played a pivotal role in shaping the team I've built over the last two decades.

I must emphasize that the core team I have today, comprising approximately 15 individuals, has remained remarkably stable for the past 20 years. While people have come and gone, this steadfast core team has evolved and thrived, thanks to the leadership lessons I imbibed from those two remarkable mentors who guided me at the outset of my career.

#### What is your definition of living a good life?
I believe that family is of utmost importance. Having a supportive family is point number one. Leading a fulfilling life isn't just about money; it's about being comfortable and understanding how to use that money to enrich your life. Money certainly plays a significant role in our lives, especially since we're discussing money management in this podcast. However, when you ask me about what it means to live a good life, I believe that family and their well-being are more important.

If you can **provide for your family and make investments that generate returns to enhance your family's comfort**, that's crucial. I'm not suggesting you shouldn't have aspirations; in fact, you should. But if you can fulfill those aspirations while also finding contentment, you've struck a balance. It might seem contradictory, starting with greed and ending with contentment, but it's essential to find contentment within the pursuit of your aspirations. Achieving that balance, Joseph, is the key to living your best life.

## Watch on YouTube

<iframe width="560" height="315" src="https://www.youtube.com/embed/USrvwHZOndY?si=31Vmua0kWZ-G05GI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

## Social Media Shares
- [LinkedIn Post](https://www.linkedin.com/posts/jjude_regi-thomas-on-investing-for-short-term-activity-7114824165911650304-bIPD)

## Connect with Regi Thomas
- LinkedIn: https://www.linkedin.com/in/regi-thomas-375758277/

## Connect with me
- Twitter: https://twitter.com/jjude
- LinkedIn: https://www.linkedin.com/in/jjude/
- Website: https://jjude.com/
- Newsletter: https://jjude.com/subscribe/
- Youtube: https://youtube.com/gravitaswins
- Email: podcast@jjude.com
- Executive Coaching Program: https://gravitaswins.com

## Your feedback counts

Thank you for listening. If you enjoy the podcast, would you please leave a short review on Apple podcast or on YouTube? It takes less than 60 seconds, and it really makes a difference in finding this podcast. And it boosts my spirits. 😀

I would like to hear what resonated the most with you in this episode. Either email me or share it on social media and tag me. Thank you for your support.

Have a life of WINS.

_Gravitas WINS Radio is produced by [Joseph Jude](https://jjude.com/). Be sure to subscribe on [Apple Podcast](https://podcasts.apple.com/in/podcast/gravitas-wins-radio/id1562802449), [Spotify](https://open.spotify.com/show/7LOBjAyjBkechMC0xMIj9E),[Google Podcast](https://podcasts.google.com/feed/aHR0cHM6Ly9mZWVkcy50cmFuc2lzdG9yLmZtL2dyYXZpdGFzLXdpbnMtcmFkaW8=),[YouTube](https://www.youtube.com/gravitaswins), or the podcasting app of your choice._

Please check out other episodes of [Gravitas WINS Radio](/gwr-themes/). For easy consumption, they are categorized by themes. If you are a fellow podcaster, you might be interested in [How I Podcast](/how-i-podcast/) or [Tools I Use](/i-use/).

## Continue Reading

- [Rishi Jiwan Gupta on 'Financial Planning For A Meaningful Retirement'](/rishi-retire/)
- [Rahul Chauhan on 'Learning from Annual Reports'](/rahul-ar/)
- [How To Succeed In The Stock Market With A Simple And Structured Approach?](/4m-stocks/)