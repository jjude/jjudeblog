---
title="Beware of Cloning Best Practices"
slug="beware-of-cloning-best-practices"
excerpt="It is regretful that consulting profession is crowded with sheeps in lion's cloth."
tags=["coach","change"]
type="post"
publish_at="15 Nov 11 16:30 IST" 
featured_image="https://cdn.olai.in/jjude/clone-best-practice.png"
---
As a consultant, I deal with 'Best Practices' often.

What were the practices followed in other successful e-Governance projects so that we can emulate them?

How are the other IT companies managing their internal tools development?

What Steve Jobs did that made him the richest man on the grave, though he didn't pursue making money?

![comedy legends can teach consultants](https://cdn.olai.in/jjude/clone-best-practice.png)

There is nothing wrong per se, about this quest. After all, we want to learn and when we learn, let us learn from the best.

But so often, this pursuit blinds us from the context. What was best in another context may not be applicable in the current context.

Before I delve into this further, let me pause to narrate a comedy scene from one of Tamil movies that I believe brings out the point well.

There is a pair of comedians in Tamil movies, who can be compared to Laurel and Hardy. They together have produced many hilarious scenes, some of which evoke laughter even today. Below scene appears in one such comical scene. I hope to do justice in words what they did in acting.

>In this movie, [Goundamani](https://en.wikipedia.org/wiki/Goundamani) (Hardy) is head of a village and [Senthil](https://en.wikipedia.org/wiki/Senthil_(actor)) (Laurel) is his assistant. Goundamani is smart, knows what to say in any situation, and is well respected in the village. Of course Goundamani is haughty and never misses a chance to deride Senthil. Senthil envies him and want to prove he is smart too. He accompanies Goundamani everywhere and observes what he says in every situation.

>In one such situation, an elderly women of the village passes away. As usual Goundamani is invited and Senthil tags along. "She wasn't just a mother to you," he says to the family that lost their mother, "she was a mother to all of us." He goes on with his eulogy, but Senthil notes down this point.

>At the climactic scene, another lady, a wife this time, dies. Goundamani is sick and can't leave the bed. Senthil recollects the earlier scene and offer to go to the funeral.

>You guessed what he would have said, didn't you? "She wasn't just a wife to you," he cuts & pastes the words, "she was a wife to all of us."

I can't count how often I encountered Senthils walking in Goundamani's suite turning whore of the best practices. In retrospect, may be even I have been a Senthil, quite a few times.

It's indeed regretful that, consulting profession is crowded with sheeps in lion's cloth.
