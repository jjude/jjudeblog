---
title="A distant dream comes true with Django"
slug="a-distant-dream-comes-true-with-django"
excerpt="Getting into web-development with django."
tags=["tech"]
type="post"
publish_at="12 Mar 08 16:30 IST"
featured_image="https://cdn.olai.in/jjude/a-distant-dream-comes-true-with-django-gen.jpg"
---

Years back, when every one jumped into Java world, I was content with developing 'Windows-only', client-server applications.

Sometime later, I moved into CRM (customer relations management) domain and I stayed with it for years before moving into ERP domain. Though ERP products used web architecture - thereby I learned little bit of that architecture - I was constrained to the toolset of the product.

During this time, I did attend a Java course and tried moving into it. But the 'comfort' of developing within ERP framework made me to stay-put (ok, to tell you the truth, the money part was good). Over the years, 'web development' remained a distant dream!

After a decade in CRM/ERP domain, the itching to develop for web was getting more and more day by day. So I decided to try my hands on it. In the beginning it was limited to setting up [WordPress](https://www.wordpress.com), customizing its themes and so on. WordPress' design was simple, and efficient. Installation is 5 minutes; customizing a theme is simple; so in about 20 minutes, my blog is up and running.

Though web development with WordPress was minimal, it boosted my confidence to get into this world of unknown. Only then I realized that the web development world is so full of languages, frameworks and standards. Not to mention the flame wars between each camps - MSFT vs Open Source, Python Vs PHP, Django Vs Rails. For a long time, I got side-tracked with these flame wars, instead of doing anything useful. I would read for hours how one Python framework is better than the other one; and then watch a presentation on how Rails is the best thing that happened to the web world. After months, I knew a lots of information; but confused more than ever.

As I carried along, I observed that [Django](https://www.djangoproject.com/) developers/users generally didn't get into a 'yours-is-bad-ours-is-best' kind of argument; rather their comments were on the lines of, 'choose a framework in the language that you already know'. That made a lot of sense to me. As I already knew Python, I decided to try Python frameworks instead of learning Java or Ruby.

Also, in most of the discussions/comments, Django developers provided information; but didn't criticize the other party. They cleared any misinterpretation; but didn't start any flame wars. Even when they were attacked with words like 'they don't know to write a decent API', they took it pretty neatly and improved their framework. I liked that attitude and it fitted with my personality.

So I decided to try it out - coming from the corporate world, I wanted to try out a PoC (proof of concept) on Django. One of the users of Django had created a package called, 'Instant Django'. Download it, unzip into a directory and you are on your way to play with the framework. Fantastic. Impressed at the first shot. I didn't have to install many applications; tweak with lots of settings; painless development was possible.

Whenever I was stuck, I could log into their [irc](irc://irc.freenode.net/django) channel and I should tell you this - these guys are awesome; very knowledgeable but very helpful. You will not get a RTFM from any one of them; they might point out to the documentation; but not in a RTFM manner. Oh, Django documentation is exhaustive and neat. You don't get lost in the sea of bytes!

So I did my first working web application in Django - a micro-blog kind of application. I show cased it to my colleagues and they loved it. Now I get a 'here-goes-a-geek' kind of a look from them, which of course I enjoy. And did I tell you that I could develop that over the weekend! It was a great feeling. My boss said, 'why don't you roll that out for our department?'. He even added that, 'Let me talk to our folks and find out if it is ok to make it an open source'. Wow! cool. I am very happy.

Who can despise the days of little beginning? This might be the start of another exciting phase in my development carrier.

