---
title="Stones for locals, crowns for others"
slug="stones-and-crowns"
excerpt="Be vocal for local"
tags=["coach"]
type="post"
publish_at="25 Nov 20 09:00 IST"
featured_image="https://cdn.olai.in/jjude/stones-and-crowns-gen.jpg"
---


Indians are a bunch of interesting people.

When Kamala Hariss is elected as a VP, we Indians go crazy in celebrating her victory. We are elated that an Indian origin woman is elected to a top position in the US. Forgeting that, in 200 years of democratic history, just now they are electing a women to such a high position, while within 75 years, Indians had a woman Prime-minister, finance minister, defence minister, and many chief ministers.

When Sunder Pitchai becomes a CEO of Google, we shout, hippie, one of our own is heading the largest technology company.

It is all good if we extend the same to our local heroes. When a woman makes it to the top here in India, we throw stones at her and call her nasty things. In the same way, when a local businessman makes it big, we say he should've bribed his way to the top.

When you see the other garden green, it is time to water your garden. Only then you can create a space for you to thrive, excel, and celebrate.

## Continue Reading 

* [Serving The Nation With A Delight](/serving-the-nation-with-a-delight)
* [Beware of Cloning Best Practices](/beware-of-cloning-best-practices)
* [If Church Can Reinvent Itself, Why Can't Companies](/church-reinvents)

