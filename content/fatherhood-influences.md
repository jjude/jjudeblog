---
title="How our fathers, society, and peers shape our fatherhood?"
slug="fatherhood-influences"
excerpt="In this first episode on the series 'Being a dad', KK and I talk about influences on our parenting."
tags=["gwradio","parenting"]
type="post"
publish_at="02 Jan 24 06:21 IST"
featured_image="https://cdn.olai.in/jjude/learning-to-be-a-father.jpg"
---
In this series on "Being a dad", KK and I talk about everything about fatherhood. In this first episode, we talk about our families, fathers, and everything that shapes our thought process as a dad. I hope you find this helpful in being a dad.

<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless="" src="https://share.transistor.fm/e/a5905ab6"></iframe>

## What you'll hear

- Background of our families
- Influence of our fathers in our childhood
- Kids mimic us
- Does timing of our marriage matter?
- Peer pressure and influence of society

## Edited Transcript

_coming soon_


## Watch on YouTube

<iframe width="560" height="315" src="https://www.youtube.com/embed/5ODFyggaREI?si=3arlYNQKNSw7xet8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>


## Connect with me
- Twitter: https://twitter.com/jjude
- LinkedIn: https://www.linkedin.com/in/jjude/
- Website: https://jjude.com/
- Newsletter: https://jjude.com/subscribe/
- Youtube: https://youtube.com/gravitaswins
- Email: podcast@jjude.com
- Executive Coaching Program: https://gravitaswins.com

## Your feedback counts

Thank you for listening. If you enjoy the podcast, would you please leave a short review on Apple podcast or on YouTube? It takes less than 60 seconds, and it really makes a difference in finding this podcast. And it boosts my spirits. 😀

I would like to hear what resonated the most with you in this episode. Either email me or share it on social media and tag me. Thank you for your support.

Have a life of WINS.

_Gravitas WINS Radio is produced by [Joseph Jude](https://jjude.com/). Be sure to subscribe on [Apple Podcast](https://podcasts.apple.com/in/podcast/gravitas-wins-radio/id1562802449), [Spotify](https://open.spotify.com/show/7LOBjAyjBkechMC0xMIj9E),[Google Podcast](https://podcasts.google.com/feed/aHR0cHM6Ly9mZWVkcy50cmFuc2lzdG9yLmZtL2dyYXZpdGFzLXdpbnMtcmFkaW8=),[YouTube](https://www.youtube.com/gravitaswins), or the podcasting app of your choice._

Please check out other episodes of [Gravitas WINS Radio](/gwr-themes/). For easy consumption, they are categorized by themes. If you are a fellow podcaster, you might be interested in [How I Podcast](/how-i-podcast/) or [Tools I Use](/i-use/).

## Continue Reading

- [Mahendran Kathiresan on 'Homeschooling In India'](/mahendrank/)
- [Sweet home is a safe home, even digitally](/sweet-home-safe-home/)
- [Teaching storytelling to kids](/storytelling-to-kids/)