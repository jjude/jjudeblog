---
title="Intentional Imbalance"
slug="imbalance"
excerpt="There are work-life choices, and you make them, and they have consequences - Jack Welch"
tags=["wins","insights"]
type="post"
publish_at="25 Apr 21 12:37 IST"
featured_image="https://cdn.olai.in/jjude/symmetry.jpg"
---
Nature is beautiful because of symmetry. Take our human body for example. It has symmetrical balance and there is a certain beauty to it. That’s why we have populated the earth for thousands of years.

Look at the sunflower. It has **radial symmetry** and that is beautiful. For that matter, most flowers are symmetrical. Walking through a flower garden is never boring. 

![Symmetry is beautiful](https://cdn.olai.in/jjude/symmetry.jpg)

We try to mimic nature in what we do. We create walking trails with trees on both sides. We build monuments like the Taj Mahal and Cathedrals like St Peter's in symmetrical form. 

So far good. When we try to bring that symmetry into our lives, we get into trouble.

We want to spend the same amount of time with our kids, spouse, friends, and jobs. We want a symmetrical life. We even call it a **work-life balance**.

We want balance in other areas of life too like investing.

Harry Browne proposed a perfectly balanced investment portfolio. His advice is to invest equally in bonds, stocks, gold, and cash. Equal. Symmetry.

**I have not been able to keep a balance in life**. I don't know anyone else who kept such a balance. When we fail to have a balance, we feel guilty, unhappy, and even angry. The pursuit of balance in life leads to mental imbalance.

The solution I have found is **intentional imbalance**. What do I mean by it?

For a while, you induce imbalance into your life. I practice intentional imbalance in almost all things in life. For this article, I'm going to give you three examples. If you have any questions, reach out to me via [email](mailto:questions@jjude.com) or [Twitter](https://twitter.com/jjude).

![Focus on family](https://cdn.olai.in/jjude/family-focus.jpg)

The first one is getting involved in the growth of my boys. Since they are six, I **took a pay cut** and worked only three days a week. I spent the rest of the time with them. I told them stories, taught them to use computers, and helped them learn cycling. They have become interested in [story-telling](https://jjude.com/storytelling-to-kids) and creating [stop-motions](https://youtube.com/garrettsboys). Of late they are also interested in playing Roblox and creating Roblox games.

_Don't forget to subscribe to their [YouTube Channel](https://youtube.com/garrettsboys)_

For six years, I intentionally focused more on family than on my career. It was an out-of-balance choice. But it was my choice and I'm ok with it.

![Career Focus](https://cdn.olai.in/jjude/career-focus.jpg)

Now that they have grown, I have shifted the focus to career. I created a course, **[Gravitas WINS](https://gravitaswins.com/)**, for those who aspire to the chief executive posts. I also run a [podcast](/podcast/), [blog](https://jjude.com), and [newsletter](/subscribe/) around the same theme.

The second area where I have intentional imbalance is investing. I follow Harry Browne's advice, but my portfolio is almost always skewed. When the stock market crashes, I load up stocks of great companies. At any given time, my portfolio has either too much stock or too much cash. It is intentional.

![Day themes for productivity](https://cdn.olai.in/jjude/day-themes.jpg)

I stretch this concept a little bit more. To manage the workload and still maintain my creativity, I work on one theme every day. For example, Sunday is a spiritual day. That means I focus on meditation, prayer, and mind-refresh. That doesn't mean, I don't meditate on other days. I do. Or I don't do other activities on Sundays. I do. But I focus on the spiritual aspect more on Sundays. Every day is intentionally imbalanced to a theme.

**Imbalance is a choice and like every choice, it has consequences**. Because I took a pay cut, I have less money in the bank. Because I go on intermittent fasting, my friends don't invite me to their dinner parties. These are the choices I made. I am ok with the two sides of the consequences of those choices.

Let me leave you with this quote from Alain de Botton: "Everything worth fighting for unbalances your life."

Do you have any questions or comments? Send them to me via [email](mailto:questions@jjude.com) or [Twitter](https://twitter.com/jjude).


## Continue Reading

* [11 Lives to Build a Rich Career](https://jjude.com/11-lives)
* [Striking a balance](https://jjude.com/balance)
* [We Need Is Integrated Thinking. Not Polarized Perspectives](https://jjude.com/no-polarization)