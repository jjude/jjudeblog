---
title="Structured Communication"
slug="structured-communication"
excerpt="Let your listeners do their awesome work"
tags=["wins","biz","insights","systems"]
type="post"
publish_at="01 Sep 21 08:14 IST"
featured_image="https://cdn.olai.in/jjude/what-is-sc.jpg"
---

When you communicate in a structured manner, your listener (manager, peer, teammate) can take what you said, and do the awesome work they have to do.   Instead, when you throw a pile of sentences at them, they've to work harder to decipher what you said. 

The lazier you are, your audience has to work harder to understand what you say. **The harder you work, the easier it is going to be for your audience**.

Consider this: would you rather work with people who help you with your work or those who expect you to interpret their words for them?

![What is a structured communication](https://cdn.olai.in/jjude/what-is-sc.jpg)

### Example

Here is an example from an actual interview session.

I usually ask this question: What was your achievement in the current project? Most often people will answer in chronological order and tell me unimportant things.

> I joined the project in 2009. It had a team of 15 developers, 3 testers, and 1 business analyst. The project was written in old PHP. I don’t know PHP, so I had to learn PHP.
>  
> There was no one who knew the functionality. So we had to spend a lot of time understanding the functionality.

> Once we understood the functionality and the existing code base, we started rewriting the code in Python. We chose Python because the business owners were interested in using the collected data for analytics and also had fewer security issues than the old PHP. Mind you, the current PHP has gotten ridden of security issues, though.

> I worked with the team to re-architect the application. We completed the application within budget and schedule, though we had to work overtime many times.

This is a ramble. How can you improve this?

Start with the important point:

> I took over a legacy application and re-architected it to a modern code-base within budget and schedule.

This answer will beget the question;

> Wow, how did you do it

You can branch out from there keeping the interview engaging.

### Why

![](https://cdn.olai.in/jjude/noise-bias.jpg)

We live in a world that is full of noise and bias. For your words to be of any use, they must cut through this smog.

Then we all have cognitive limitations. We can communicate and absorb information only sequentially. At the best, as research suggests, we remember seven items at once.

### How

Instead of saying everything, **say only what is needed**. Filter out everything else. Of course, this requires work. 

![](https://cdn.olai.in/jjude/say-needed.jpg)

For any communication to be effective, you need answers to these questions:

• Who is the audience for this particular piece?
• What do they need to know?
• How much do they need to know?

If you think through and structure your communication, you'll stand out in any conversation.
  
There are structures for every communication:

• official emails
• comedies
• storytelling

One structure for official communication is called "Pyramid Communication."

### Pyramid communication

![](https://cdn.olai.in/jjude/pyramid-comm.jpg)

In a pyramid style of communication:  

• You state the key point first
• Then logical sub-points
• Support each sub-point with arguments and data

You have to remember that when you are preparing the content, you have to **think bottom-up**. You collect all data, analyze them which will inform you of the logical arguments, and then the key point will emerge out of these arguments.

But **when you present, you start from the top and go down**. 

### Conclusion

In a noisy world, structured communication gives you an edge. Learn to communicate in a structured manner and you'll have a life of WINS.

## Continue Reading

* [The McKinsey Way](/mckinsey-way/)
* [Structured Thinking](/structured-thinking/)
* [Smartcuts](/smartcuts/)