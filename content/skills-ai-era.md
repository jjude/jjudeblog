---
title="The skills you need in the age of AI"
slug="skills-ai-era"
excerpt="What skills AI can't replace? Let us find out."
tags=["tech","aieconomy","visuals","gwradio"]
type="post"
publish_at="20 Jun 23 12:37 IST"
featured_image="https://cdn.olai.in/jjude/02-skills-that-matter.jpg"
---

<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless src="https://share.transistor.fm/e/98dff9fc"></iframe>

AI technology has advanced rapidly in recent years, raising questions about its impact on jobs. It's clear that new technologies disrupt some jobs; computers replaced stenographers and typists, for example. AI will undoubtedly change the job market, so it's essential to develop skills that make you valuable in an AI-driven world.

To survive the rise of AI, avoid becoming a specialist who knows only one thing well or a generalist who knows a little about everything but lacks depth. Instead, focus on developing T-shaped or U-shaped expertise.

![Skills you need in the age of AI](https://cdn.olai.in/jjude/02-skills-that-matter.jpg "Skills you need in the age of AI")

T-shaped expertise means honing related skills within your field. For instance, software engineers can learn project management and public speaking to become more versatile and hard-to-replace employees. This approach applies to various professions like law as well; anyone can benefit from improving their public speaking abilities.

U-shaped expertise involves mastering peripheral skills not directly linked to your core domain. A software engineer might explore visual arts or theater alongside their tech career. Combining unrelated skill sets makes you an irreplaceable expert with unique insights from multiple domains.

Cultivating combinatory skills is crucial for thriving in an AI-disrupted era. Focus on developing related or peripheral abilities within your field to stand out and secure your future career prospects.

I hope you enjoyed this episode. If so, can you please share the episode with others? And also send me an email with your feedback? It helps me to improve and evolve.

Thank you for reading. Have a life of WINS. 

_This is part of [Short notes on AI Economy](/ai-economy-notes/)_