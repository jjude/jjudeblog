---
title="Apps I use - Pocket"
slug="apps-pocket"
excerpt="Starting point for adult learning is reading. Pocket app is the best tool to save articles and read them in batches."
tags=["sdl","consume","apps"]
type="post"
publish_at="16 Jul 18 10:30 IST"
featured_image="https://cdn.olai.in/jjude/pocket-app.jpg"
---
![Pocket App](https://cdn.olai.in/jjude/pocket-app.jpg "Pocket App")

For most people, learning stops after college. But ways of doing business changes constantly. If we need to continue to grow in our career, we need to learn continuously. 

That is why I framed a [learning framework](/sdl/) which consists of these phases — **consume, produce, and engage**.

The **starting point for continuous learning is consumption**. We need to learn from books and blogs.

I schedule time for reading books. But articles come from many directions — newsletters, RSS feeds, twitter, and so on.

It may not be possible to read an article immediately as soon as it comes to my attention. I might be in the middle of completing something I started. That is why an app like [Pocket ](https://getpocket.com/) is absolutely important. 

Whenever a good article comes my way, I store it in Pocket. The team at Pocket has excelled in creating an **amazing set of integrations** with other every-day tools. There are extensions or bookmarklets for every browser. The Pocket app is deeply integrated with Firefox browser. I can e-mail an article to a dedicated Pocket e-mail id, add@getpocket.com. I can also store an article in the iOS device using share extension. This rich ecosystem has made it easy to store articles.

Once stored in Pocket, the article is available in the web application and mobile applications.

Later **when I get time**, like standing in a queue, I open the [mobile app](https://itunes.apple.com/us/app/pocket-save-stories-for-later/id309601447?mt=8&at=1000lMsn) and start reading articles. I also dedicate an hour every week to read articles stored in the app.

One cool feature of Pocket is that it **removes all the clutter** like ads. It presents the article in a clean page to read.

After I read an article, I share it on social media sites via [Buffer application](http://buffer.com/). This leads, sometimes, to engage with other readers or even with the author.

You can't beat the price. It is free. Yes, all of these features are available for free. They have paid plans too, but you get all the sufficient features in the free plan itself.

The Pocket app has helped me immensely to keep learning. I strongly recommend using the Pocket application.

### You May Also Like

* [Self-Directed Learning (SDL)](/sdl/)
* [Learning Phases And Its Support Systems](/learning-phases-and-its-support-systems/)
* [How I Use Twitter As A Learning Tool?](/learn-via-twitter/)

_Disclaimer: Link to mobile application is an affiliate link. That means, if you buy through that link, I get a very small commission._
