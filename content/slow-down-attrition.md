---
title="How to slow down attrition? Keep an eye on what employees need."
slug="slow-down-attrition"
excerpt="It is lazy thinking to say that employees only want fat salaries."
tags=["coach","experience"]
type="post"
publish_at="03 Oct 22 17:40 IST"
featured_image="https://cdn.olai.in/jjude/an-ideal-job.jpg"
---
Companies are having trouble attracting and retaining talent. Most talent management initiatives are band-aids rather than addressing fundamental issues.

Every CEO will tell you that their employees are their greatest asset. Yet dollars spent on employee experiences reveal a different priority. CEOs go to great lengths to know every detail about their top clients, but have no idea who their top employees are. What a pity.

When companies treat employees as potatoes (replaceable commodities), employees start behaving as potatoes (not caring about the company).

Companies need to go back to basics and ask, "what do employees want?" It is lazy thinking to say that employees only want fat salaries. It is equivalent to saying that clients are only interested in the lowest price. People who chase only money exist, but the companies don't need to chase them if they can provide other aspects of an ideal job as well.

![An ideal job](https://cdn.olai.in/jjude/an-ideal-job.jpg "Aspects of an ideal job")

The first and foremost aspect of a great job is a **supportive boss**. Employees want to work for a boss who gives enough freedom but step in when the employee need help.

Second, employees look for **co-operative team**. Success is a team work. Having dependable colleagues is important when you spend most of your day with them. 

Third aspect is **appreciative clients**. All of us work for someone else. We feel better when that someone appreciates us. Don't we?

The next aspect is **flexible working hours**. We live in a knowledge-age where 9 to 5 work hours are a terrible, terrible idea. Some of my friends work better in the morning, while others work better at night. In addition, some of us would like to be on the ground for our kids' first soccer game at 3 pm on a Wednesday. It shouldn't be a problem as long as employees finish their assignments?

Fifth aspect is a **good salary**. Having a big salary alone does not make a job ideal; that doesn't mean earning less than what an employees deserve. Since this is the only aspect that can be measured and compared, the higher the better.

**Stimulating work** is the sixth aspect that make a job an ideal one. An employee doesn't look forward going to a boring routine work. They want to work in a job that challenges them, where there is variety in the work, where the time passes so quickly.

And finally there should be **constant learning** in the job. Employees like to work in a job where they can apply what they already know but also learn something new. That tension between being stretched and being torn is what make a job challenging and so enjoying. Most companies don't even have an employee training plan. How can they retain the talent that joins them?

As job opportunities abound, companies will fight to attract and retain talent. That fight starts with understanding what employees want and crafting a process to meet those needs. It is not difficult if the top boss gets involved.

## Continue Reading

* [Building Your Career - Apple Or Amazon Way](/apple-amazon-career/)
* [An Awesome Tip From Jack Ma & Derek Sivers To Build Your Career](/specialize-or-generalize/)
* [Building A Personal Flywheel](/flywheel/)