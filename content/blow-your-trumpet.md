---
title="If you don't blow your trumpet, how can you expect music?"
slug="blow-your-trumpet"
excerpt="If you don't talk about yourself, who will know you in this digital world?"
tags=["wins","gwshow","gwradio","branding"]
type="post"
publish_at="21 Apr 21 11:45 IST"
featured_image="https://cdn.olai.in/jjude/trumpet.jpg"
---
Most cultures define humility as a virtue. And humility is defined as "not talking about self." But if I don't talk about myself, how will anyone know me, especially in this forest called Internet?

<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless src="https://share.transistor.fm/e/81c2148f"></iframe>

Let me start with a personal story. 

Back in 1997, I was a junior programmer. Computers were expensive and I was single. So it made sense to spend all the available hours in the office tinkering with code.

At that time, one of the hardest problems was sending an email programmatically. You can understand the Internet dark ages that I come from. By trial and error, I solved that problem. I managed to send an email using a Visual Basic program.

While having a cup of coffee, I trumpeted about my achievement to a friend. I told him all the problems I faced and how I was able to solve them. Do you know what he said?

“Joseph, I am facing the same problems. Can you help me?”

I guided him to send emails through his program. He was generous enough to introduce me to his project manager. That project manager became my mentor and gave me 3 career breaks. Through him, I went to the US for the first time in 1998; I became an advisor to the Government of India in 2009, and I got a job as a chief technology officer in 2015. All because I trumpeted my achievements. **Trumpeting got me dollars, opportunities, position, and esteem**.

That incident taught me a good lesson on humility and that is what I'm sharing with you today.

When we talk about ourselves, usually we talk about what we have or what we are. 

We talk about the BMW car or the penthouse, or the degree from a top institution or any other expensive and fancy things we possess. Sometimes, we also say things like, my IQ is awesome or the language I speak is the best in the world.

**When we talk about what we have or what we are, the information we share doesn't help others or lift their souls. That is why they are resented**.

What happens when you share what you did, the problems you faced while doing it, and the reason you selected the solution you selected? The exact opposite of resentment.

You inspire them. You give them a toolkit they can follow.

When you share what you do or did, others want to hear more from you. They tell about you to others who are on a similar journey, forming a community around you. The more you share what you do, the more crowd gathers around you.

**The lesson I learned about humility is this: Don't talk about what you have or what you are. But trumpet about what you do**.

Let me give you a guide, to get you started.

![A guide to trumpeting your success](https://cdn.olai.in/jjude/trumpet.jpg)

### Guide to trumpeting

First, choose something you like to do. It could be reading books, working out from home, or cooking delicious meals. Do them consistently.

Second, choose a platform to share your learnings. If you are comfortable writing choose a blog, if you are ok with videos, choose YouTube, or if you talk freely choose a podcast. 

Also, choose one social media platform. It could be LinkedIn, Twitter, or Instagram. In the beginning, choose only one social media platform so that you can share your work in the best way possible for that platform.

Third: There is no third. That is it. It is as simple as, do and talk interesting things.

### Examples

Let me leave you with some examples that might inspire you.

Gaganpal works with VisionSprings, a non-profit organization in the field of eye care. In a [LinkedIn post](https://www.linkedin.com/posts/gaganpal-singh_rct-study-quantifies-eyeglasses-impact-on-activity-6786536497551052800-O4x1), he juxtaposes the consumers of Assam Tea and the tea-workers. In an inspiring way he says:

> Our day begins with a hot cup of Assam Tea.
> Her day begins with plucking tea leaves under scorching sun. 
> Her hand might hurt, but she carries on. 
> Hours with basket on her back which gets heavy each minute. 
> When sun sets, her responsibility to her home and family starts.

He ends with how his organization helps these workers.

> VisionSprings helps Assam Tea Workers with eyeglasses so their productivity improves.

The next [example](https://vibhor.dev/remote-indian-it-workplace-challenge-1-trust-482820ea1cfa) is from Vibhor Mahajan. He watched "My Octopus Teacher" on Netflix. If you've not watched it, it is about a filmmaker forging an unusual friendship with an octopus. This story of trust between a man and an animal influences Vibhor to wonder if the same lessons can be applied in remote working. He published his thoughts in a medium post.

There are many more examples like these. But I hope I have convinced you enough to blow your trumpet as loud as possible. 

Thank you for reading. If you have a question or comment, reach out to me via [email](mailto:post@jjude.com) or [Twitter](https://twitter.com/jjude) or [LinkedIn](https://www.linkedin.com/in/jjude/).

If you liked the post, please share it with others. Thank you. 

Have a life of WINS.

## Continue Reading

* [Building In Public, Jeff Bezos Style](/build-in-public-bezos)
* [Fame or Fortune](/fame-fortune)
* [The dichotomy of contentment and ambition](/contentment-and-ambition)
