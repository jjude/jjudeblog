---
title="Video - 3 Daily Needs"
slug="video-3-daily-needs"
excerpt="We all need something to look up to, another to look forward to, and another is someone to chase."
tags=["coach","video","speech","visuals"]
type="post"
publish_at="08 Jun 18 10:30 IST"
featured_image="https://cdn.olai.in/jjude/3-things-to-chase.jpg"
---
Matthew McConaughey is one of my favorite actors. I liked him in Fool's Gold.

He won an Oscar for his performance in Dallas Buyers Club. In his acceptance speech, he says:

> There are three things that I need each day. One, I need **something to look up to, another to look forward to, and another is someone to chase**.

![3 Things To Chase](https://cdn.olai.in/jjude/3-things-to-chase.jpg "3 things to chase")

He goes on to say:

> First off **I want to thank God, because that’s who I look up to**. He has graced my life with opportunities that I know are not of my hand or of any human hand.

Whether you believe in a God or not, be thankful for the opportunities you have. There are thousands of people who wish they have the opportunities that you have. Be thankful. That is a first step for happy living. Then ask yourself, what are you doing with those opportunities.

> To **my family, that’s who and what I look forward to**. ...To you Dad, you taught me what it means to be a man. To my Mother who is here tonight, who taught me and my two older brothers, demanded that we respect ourselves. And what we in turn learned was that we were better able to respect others. Thank you for that Mama. To my wife Camila, and my kids Levi, Vida, Livingston. The courage and significance you give me everyday I go out the door is unparalleled. You are the four people in my life that I want to make the most proud of me.

I'm what I'm because of my family. It is not a cliché. They have been a strong support that I can lean on whenever I am disappointed or depressed. You need a strong support network if you wish to take risky decisions. You need someone to bounce of ideas, someone to talk to when things go wrong, and someone to share the happiness when things turn out pretty well. Without the family, your life is incomplete in some aspect.

> And to **my hero, that’s who I chase.** ... every day, every week, every month and every year of my life, my hero is always 10 years away. I’m never going to beat my hero. I’m not going to obtain that, I know I’m not. And that’s just fine with me because that keeps me with somebody to keep on chasing.

This is the twist that attracted me to this video. It is common to thank God and family. But I have never heard of this definition of a hero. **My hero is me in 10 years**.

Watch the [video](https://youtu.be/wD2cVhC-63I?t=114) for yourself.

What do you look up to, look forward to, and chase?

## Continue Reading

* [The dichotomy of contentment and ambition](/contentment-and-ambition/)
* [If you are not part of the solution, you are a problem](/be-a-solution/)
* [Build on the rock for the storms are surely coming](/build-on-rock/)