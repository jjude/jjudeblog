---
title="'Building An  Enterprise Data Strategy' with Ganesan Ramaswamy"
slug="ganesanr"
excerpt="We all have heard data is the new oil. In this episode, I talk to my college-mate Ganesan who is incharge of data-strategy for FedEx to explore this topic more."
tags=["gwradio","tech"]
type="post"
publish_at="23 Nov 21 06:00 IST"
featured_image="https://cdn.olai.in/jjude/ganesanr.jpg"
---

<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless src="https://share.transistor.fm/e/93b8a451"></iframe>

- What is data strategy for an enterprise? Why is it needed?
- What are the domains benefit from data strategy - e-com, logistics, HR?
- Who drives the data strategy - business or tech?
- People, process, products for data strategy
- Challenges faced in the journey
- How does your day, week, quarter looks like?
- What is the next big thing in data?

![Ganesan Ramaswamy, IT Manager, Data Strategy, FedEx](https://cdn.olai.in/jjude/ganesanr.jpg "Ganesan Ramaswamy, IT Manager, Data Strategy, FedEx")

### Connect with Ganesan

- LinkedIn: https://www.linkedin.com/in/ganesan-ramaswamy-6819b867/

## Connect with me
- Twitter: https://twitter.com/jjude
- LinkedIn: https://www.linkedin.com/in/jjude/
- Website: https://jjude.com/
- Newsletter: https://jjude.com/subscribe/
- Youtube: https://youtube.com/gravitaswins
- Email: podcast@jjude.com
- Executive Coaching Program: https://gravitaswins.com

## Your feedback counts

Thank you for listening. If you enjoy the podcast, would you please leave a short review on Apple podcast or on YouTube? It takes less than 60 seconds, and it really makes a difference in finding this podcast. And it boosts my spirits. 😀

I would like to hear what resonated the most with you in this episode. Either you can email me or share it on social media and tag me. Thank you for your support.

Have a life of WINS.

## Amazing tools I use:
- I record all the remote interview with [Riverside.fm](https://riverside.fm/?via=joseph-jude)
- I use [Transistor.fm](https://transistor.fm/?via=jjude) for hosting these podcast episodes

_Gravitas WINS Radio is produced by [Joseph Jude](https://jjude.com/). Be sure to subscribe on [Apple Podcast](https://podcasts.apple.com/in/podcast/gravitas-wins-radio/id1562802449), [Spotify](https://open.spotify.com/show/7LOBjAyjBkechMC0xMIj9E),[Google Podcast](https://podcasts.google.com/feed/aHR0cHM6Ly9mZWVkcy50cmFuc2lzdG9yLmZtL2dyYXZpdGFzLXdpbnMtcmFkaW8=),[YouTube](https://www.youtube.com/gravitaswins), or the podcasting app of your choice._

Please check out other episodes of [Gravitas WINS Radio](/gwr-themes/). For easy consumption, they are categorized by themes. If you are a fellow podcaster, you might be interested in [How I Podcast](/how-i-podcast/) or [tools I use](/i-use/).

## Continue Reading

- ['Why Robotic Process Automation Matters To Enterprise' With Jyothi & Aravinda](/jyothi-rpa/)
- ['Conversational Bots' With Liji Thomas](/lijit/)
- [How to deliver value in the digital age?](/value-in-digital-age/)