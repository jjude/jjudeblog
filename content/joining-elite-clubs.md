---
title="Joining Elite Clubs"
slug="joining-elite-clubs"
excerpt="What are the benefits of joining clubs?"
tags=["coach","startup","networking"]
type="post"
publish_at="10 Oct 12 16:30 IST"
featured_image="https://cdn.olai.in/jjude/joining-elite-clubs-gen.jpg"
---

So far, I loathed the idea of joining any club. I didn't find it appealing to pass a test or pay membership fee to join a group—like [PMP][3] or industry associations. To me, there was no visible benefit of associating myself with any clubs[^1].

Even after becoming an independent consultant, I resisted joining clubs. Over the years that I've been one, I was able to meet my needs. So I didn't find any convincing reason to join one.

That perspective changed when I dived into developing a product for Apple Macs. First thing I did was to join their Developer Program (joining this program is not compulsory for developing desktop applications; but it is mandatory if your apps should be listed in AppStore). I paid an annual membership fee and faxed a duly signed application form[^2].

This program has more constraints than other development programs that I know of. There is a long list of "can't-do-and-shouldn't-do" that you have to adhere to if you want to be in the program. Even their distribution program is restrictive. Sellers can't know buyers; they can't know about traffic; they can't bundle programs for promotions and so on and so forth. Yet I, like thousands of others, joined.

Why?

I'm already part of this group—although only for a month, and have already reaped benefits. So let me list out the benefits instead of my reasoning to join.

**Discoverability** is the first advantage of being in a group. My app—[BlogEasy][4], is listed under 'Social Networking' and it is right there in front of anyone who select this category. Compared to thousands of apps for other platforms fragmented all over the web, I've leaped a major marketing hurdle!

The prime advantage, however, is the **level of confidence buyers grant** to members of a group they trust. As long as the group is trusted, they don't validate each member. They assume a certain level of quality from the member. As a seller, you want to eliminate as many friction points as possible and being a member of a trusted group certainly decreases friction of first contact.

Are these just matters of mind? Largely yes. But consider this: I have already sold two copies of [BlogEasy][4] without a single marketing activity. So there is the proof.

There is an added psychological benefit for you—the seller, too. If feels good to be accepted as part of a trusted club. It **boosts your ego and a boosted ego is essential for marketing**.

Now, I understand why people rush to join elite clubs. May be I will join few more.

## Continue Reading
- [Is There DOPE In Your Network](https://jjude.com/network-dope/)
- [Did you compliment your friend recently?](https://jjude.com/compliment/)
- [Book Notes - Networking like a Pro](https://jjude.com/network-like-pro/)

[3]: https://en.wikipedia.org/wiki/Project_Management_Professional
[4]: http://blogeasyapp.com

[^1]: I'm excluding college and employment, though they too are forms of club going by this definition.

[^2]: Apple turns to legacy fax mode for India; developers from most other countries can join online.

