---
title="Book Notes - Smartcuts"
slug="smartcuts"
excerpt="Nine ways you can bypass unnecessary cycles and accelerate success"
tags=["frameworks","sdl","books","sketch","systems"]
type= "post"
publish_at= "12 Nov 19 11:00 IST"
featured_image="https://cdn.olai.in/jjude/smartcuts.jpg"
---
"Average pace is for chumps," said Kimo Williams to Derek Sivers.

[Kimo](https://sivers.org/kimo) explained, "the system is designed so anyone can keep up. If you're **more driven than most people, you can do way more than anyone expects**. And this principle applies to all of life."

Sivers took this principle to heart, completed his Berklee music college in half the time, started CD Baby, and sold it for millions. That's the power of a smartcut. They are the ways you **bypass unnecessary cycles** in achieving your goals. They are not shortcuts. There is nothing unethical or illegal about it.

Shane catalogs nine such smartcuts in this [book](https://geni.us/smartcutsbook) under three categories: shorten, leverage, and soar. The following are my notes of the book, not a summary. If you enjoy reading, would you mind subscribing to this blog using the form at the end?

![Smartcuts](https://cdn.olai.in/jjude/smartcuts.jpg "Smartcuts")

## Shorten

### Hack the ladder

"If I can make it there, I can make it anywhere," so sang Frank Sinatra about New York. Why? There is an aura around those who returned from NY, even if they didn't succeed there. This occurrence is so prevalent in India. There is always a premium on "foreign return." That "foreign return" might have studied only in a low graded university with just average marks. Yet he will be respected higher than someone smarter but studied and worked only in India.

The idea is to gain experience in a credible place (city, university, institution) and then use it to jump to a more significant role somewhere else. That is a way to hack the ladder towards any worthy goal.

Another way to jump the ladder is to **parlay**: accrue wins from series of bets and use the victory as a stake for a further bet. While parlaying, understand that the strength of cheetah is not its speed but agility.

### Train with masters

Seek out a master, who "has been there, and done that," in whatever you are trying to accomplish. You will still have to undergo long and deliberate hours of practice, but a master, like Kimo for Sivers, can cut unnecessary cycles.

When you are working with a master, **mimic them first before transforming**. I have the first-hand experience in this. I was lucky enough to find a fantastic mentor, [Sastry Tumuluri](/tribute-to-teachers/), early in my career. He opened up the world of management to me. In those days, whenever I struggled to make a decision, I would ask, "What would Sastry do?" And the answer would be obvious.

When you are working with a mentor, try to gain experience in shaping the journey rather than just the task at hand. That is when you learn how the masters think about a particular problem and why they think that way. You need to learn the mindset, not just a set of tactics.

### Rapid feedback

Feedback improves us. But if the feedback cycle is too long, we can't reliably correlate feedback with the actions we took. So design systems to give you rapid feedback, especially leading indicators.

When you are attempting a new venture, lower the stakes so you can take risks. If you are the person giving feedback, depersonalize the input, so the other person receives the essence of the feedback.

We have heard that we learn from the mistakes. It is only partially true. When we observe others making mistakes, we learn; we don't learn so much from observing our own mistakes.

## Leverage

### Platforms

Platforms are those existing abstractions that amplify your effort and teach new skills as you use them. These platforms could be tools like Trello or environments like racing leagues. 

When you first get into management, probably it is easier to learn it by using a tool rather than first attempting to understand all the theory that goes behind it. Initially, you need to get a grip on making things work; then it is easier to understand why they work that way.

Similarly, if you get an opportunity to speak in front of entrepreneurs about lean canvas, that will shape your speaking skills as well as your understanding of the lean canvas.

Learn the ability to seek out and use platforms for your success.

### Waves

Those who compete in surfing arrive early and stare at the ocean to identify the wave patterns. Then they pick the conditions to compete. They follow-fast the first-movers, learning from the other's mistakes.

Every path has well-established patterns. Study the winners, recognize the patterns, and change direction if the environment shifts. This is why I am curious about [frameworks](/tags/frameworks/). These are codified knowledge of successful people. Use them.

### Super-connectors

Building your network will be difficult. Connect with those who have their network. Invest in those networks to create enough value before you can draw from them. Once you have access to other's networks and invested in them enough, build your fan base. 

## Soar

### Momentum

Momentum is not only an ingredient but also a predictor of success. When you approach a mentor to guide you if there is already a momentum, they would know that they are helping someone who is in the path to success.

Everyone gets their luck-events. Only those who have a reservoir of the backlog can leverage such luck-events to propel themselves even further. Spotlight furthers momentum only if substance exists. Seek spotlight, but on the way, create enough substance on your own.

### Simplicity

If you want to get ahead of others, you need to strip meaningless choices. How can you do it? One way is to avoid adding trivial options by saying no. Learning to say no is the path to awesomeness.

### 10x Thinking

Incremental progress depends on working harder. 10x goals force you to look for smartcuts. Such 10x thinking demands bravery and creativity to succeed.

To achieve goals thought through 10x thinking, you need the support of others. So you have to become better at storytelling. Only by telling stories, you can build a tribe that supports you to achieve such 10x goals.

## Watch my review 

<iframe width="560" height="315" src="https://www.youtube.com/embed/c9XU-rzcFtg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

## Read the book for sure

I read the book in early 2019. Initially, I was skeptical as the book is based on observations, and most observational studies lead to delusional conclusions. I read widely to find out if there are other cases of smartcuts. That's when I happened to read Derek Siver's evidence, which proves the essential point of the book. As I looked back into my own life, I saw traces of these smartcuts, which made me get hooked to this idea. I have started applying these smartcuts. It is too early to tell, but I'm sure these are going to be useful.

[Buy the book](https://geni.us/smartcutsbook). Read it. Make your own smartcuts. Live happily.

Before you leave, don't forget to subscribe so you can get excellent content every week.

_Disclaimer: The book-link is an Amazon affiliate link._