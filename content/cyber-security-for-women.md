---
title="Cyber Security for Women"
slug="cyber-security-for-women"
excerpt="Women are the change makers. Even in cyber-security they will lead the change."
tags=["coach","talks","security"]
type="post"
publish_at="17 Aug 17 12:48 IST"
featured_image="https://cdn.olai.in/jjude/cyber-security-for-women.jpg"
---

![Cyber-security for women](https://cdn.olai.in/jjude/cyber-security-for-women.jpg)

Last week (10-August), I was invited to be part of a panel on "**Cyber-Security for Women**" at an event organized by [Young Indians](https://youngindians.net/) (YI), which is an initiative of Confederation of Indian Industry (CII). The panel was part of a day long event titled "Empowering Women - I am the change". I was surprised but glad that they were talking about cyber-security as part of empowering women.

The other member in the panel was Ms. Rashmi Sharma Yadav, DSP, Cyber crime cell & IT, Chandigarh. Ms. Sonika Gupta, moderated the session. Ms. Sonika had researched well and asked us wide-range of pertinent questions. I felt that the questions were applicable to all rather than just women.

Points made in the event are:

- The strength of a chain lies in its weakest link. The weakest link could be men, women, senior members of the family, or kids. We **all have to be aware of security**.

- **Digital technology is evolving so fast**, that whatever we discuss as precautions may not be valid in another 6 months.

- As **like in car driving**, we all should be looking through our surroundings to assess what we are doing and if there is any danger. In digital world, there is not much is visible to our plain eye. Lack of single probing tool amplifies the problem. We should utilize collection of tools to keep us safe.

- How to build a safer cyber world is a similar question to **how to build a safer kitchen**. There are knives, fire, and other dangerous items in the kitchen. But we know to keep it safe. Same way, on cyber world too there are lot of tools that can be used for harm or for benefit too. It is up to us to use them for one way or the other.

- In our young days, we will have our grandmas in-charge of locking all the jewelry boxes and almariahs and keep the key with them. We need to evolve such facility in the digital world.

- One such facility is password manager. We usually use same or similar password across all the digital services we use. I agree that remembering passwords isn't an easy option. This is where password managers help. **Password managers** allow you to generate unique password for each of the service that you use and store and lock them with a master key. If you have your own domain you can go a step further and use unique email id for each services. If you use gmail, then you can use id+service@gmail.com format to generate a unique email id. Even if the service that you use gets hacked, you are safe because the password is not re-used anywhere.

- What are the **precautions for using public wifi**? Now-a-days mobile internet is powerful. So use your mobile internet than public wifi. If you have to use public wifi, always open a new browser window rather than using an already open window. When you search and click on a result, ensure that the address of the site is what you were wanting to connect. Also check that there is a green bar on the address bar, indicating the address is an https and not htpp. When you are done with your work, log off and close the browser window.

- Anything you upload to Facebook or other similar social network sites are stored for ever. They are **never deleted, even if you close your account**. Keep that in mind, when you share photos.

- You might think you are sharing photos only with your friends. But it is visible to friends of your friends and if they share, it is visible to a wider network than you might otherwise know. This can be controlled by settings, still know that **your photos can be circulated to wider circle than you wanted**.

- Consider the **business models of the services and products you are using**. There are two broad business models: you pay for a product or a service; then there are ad-supported products and services. When you use an ad-service service or a subsidized product, they track you so they can serve you more ads. Then there are other sets of products that you pay to use. They might track you to increase their business, but they may not sell your data to others for ads. I'm not saying paid products/services won't sell your data; rather I am saying free products and services definitely sell your data.

- A recent college graduate asked about **opportunities in cyber-security**. Much of the money today is in training and content-curation. Today content-curation is more like list of hacked sites and why they got hacked. Most of these are only spreading fear and not awareness. The need of the hour is to strengthen defence. If you want to be "I'm the change" (as in the theme of the event), get into defence side of cyber-security. Today there may not be much money in it, as in FinTech and other fancy startups, but in the near future, there is going to be lot of demand for such professionals and tools.

There is a concern in the cyber-security groups that there is not enough interest about security among professionals — developers, business owners, and analysts. So it was encouraging to be invited to talk about cyber-security. As the theme of the day claimed, Women are indeed the change makers.

## Continue Reading
- [Startups And Security](/startups-and-security/)
- ['Talk With Me Security' With Shikhil Sharma](/shikhils/)
- ['Building An Enterprise Data Strategy' with Ganesan Ramaswamy](/ganesanr/)