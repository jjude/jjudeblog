---
title="How to think structurally?"
slug="structured-thinking"
excerpt="Learning how to think is the best intellectual asset you can build for yourself. Here is one framework to help you."
tags=["coach","sdl","frameworks","systems","wins","insights"]
type="post"
publish_at="11 Jun 18 10:30 IST"
featured_image="https://cdn.olai.in/jjude/build-wealth.png"
---
![SODAS for generating wealth](https://cdn.olai.in/jjude/build-wealth.png "SODAS for generating wealth")

Most corporate situations you get into will be either chaotic or vague. You can turn to friends or senior colleagues for advice. At best, they will tell you what worked for them. That advice won't always work for you because your situation is different.

If you want to build a thriving career (and a joyful life), you have to learn to think for yourself. Teachers in schools and trainers in corporate training sessions teach you what to think, not how to think. **Learning how to think is the best intellectual asset** you can build for yourself.

Given to itself, your thinking mind generalizes or complicates issues at hand. Your emotions too influence the decision making. A structured approach will help you organize the information, reveal gaps in understanding, and select the best solution within the constraints of the situation at hand.

One such structured thinking framework is **SODAS — situation, options, disadvantages, advantages, and solution**.

In this approach, you start with **understanding the situation**. It can be a current state — crisis that needs a clean-up or a future desired state like increased market share.

It is not just a problem statement. Take time to describe the situation as much as possible. Describe constrains, your strengths, support of team, and so on. If you could appraise the situation well, then you can describe next steps better.

As Mary Poppins quips, "**Well begun is half done**".

Then you **generate as many options** as possible. Your options could be MECE (mutually exclusive, completely exhaustive) or independent & complete. You need to generate as many options as possible for a better solution.

For each of these options generate both disadvantages and advantages. You should generate at least three of these. Why at least three? It is possible that as we generate the options, we come to like or dislike some options. If we like an option, we will tend to generate only advantages and fail to see any disadvantage. Similarly for options that we dislike. If we don't have the positives and negatives of these options, we may end up with an ineffective solution. So it is necessary to think through the advantage and disadvantage of each option.

You can't copy paste a best practice. **Context matters**. Information from the above steps should help you in choosing a workable solution to the situation.

_When you are applying your learning, be sure to customize to your current situation. Otherwise you will only embarrass yourself. Read [Beware of Cloning Best Practices](/beware-of-cloning-best-practices/) to know more._

Keep in mind that SODAS does not arm you with techniques to generate options or methodology for implementation. It simply **provides a structure to go from a situation to a solution**.

Another factor to keep in mind is that it is not a single left-to-right journey. As you generate the disadvantages and advantages you might get creative and think of more options. So it is an **iterative** process.

Let us apply this to **a hypothetical situation**. Say you are a developer working as an independent consultant. You want to increase your wealth through your work (for this thought exercise we are leaving out investing in any form). Now you have two options — increase revenue or decrease expenses. To increase revenue, you can increase your fee or take up additional engagement. Now you list out the advantages and disadvantages of taking up additional engagement. Do the same for increasing your fees. Your SODAs tree might look like the figure in the beginning of the article.

After going through the exercise you might decide to pick-up a second assignment instead of increasing fee for the current engagement.

SODAS is **not the only structure** available. There are plenty others too depending on the issue. There is GTD (getting things done) for getting work done, JTBD (jobs to be done) to know what products to build, lean canvas to map out the business plan for a startup, and so on.

Once you are able to put in structural thinking into practice, you can apply such thinking to many situations. You could apply such thinking to debug an application, to architect an enterprise application, to approach a sales lead, to improve sales and marketing of your company and so on.

Learning how to think is the best intellectual asset you can build for yourself. This skill become even more valuable in this era of fake news and paid editorials.

Want to thrive in your life? Learn to think structurally.

## Continue Reading

* [The McKinsey Way](/mckinsey-way/)
* [Structured Communication](/structured-communication/)
* [Smartcuts](/smartcuts/)