---
title="Thank God For Internet"
slug="thank-god-for-internet"
excerpt="New models of co-creation built around the internet are emerging. How will it impact businesses?"
tags=["startup"]
type="post"
publish_at="10 Jul 10 16:30 IST"
featured_image="https://cdn.olai.in/jjude/thank-god-for-internet-gen.jpg"
---

Technology innovation has been at the center of business evolution since we were hunter-gatherers. Industrial revolution enabled mass production and set the initial phase of globalization. Next stage of massive and rapid business growth was and is built around internet. We have witnessed Amazon, iTunes and Youtube tap internet as their distribution channels. Not only this enabled them to reach consumers otherwise unreachable, it also lead to new means of customer engagement, avenues of revenue stream and talent discovery.

Until recently, internet has been utilized primarily for pre-production market discovery and post-production distribution. Products (books, music) have been created within the four walls.

Now new models of co-creation built around the internet are emerging.

‘[Business Model Generation](http://www.businessmodelgeneration.com/ "business model book")’ is a book co-created by 470 authors from 45 different countries. They have challenged the established business norms by bypassing the publishing industry. Their first print-run was sold out just in few weeks and the book has come out in 2nd edition now.

A similar co-creation is happening in music production. That disruption of music industry is by Indians. A singer in Canada and a producer in Italy utilized the power of internet and recorded music online from musicians in India. Here is a trailer:

<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="640" height="385" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,40,0"><param name="allowFullScreen" value="true" /><param name="allowscriptaccess" value="always" /><param name="src" value="http://www.youtube.com/v/hIk1t_z5JHk&amp;hl=en_US&amp;fs=1" /><param name="allowfullscreen" value="true" /><embed type="application/x-shockwave-flash" width="640" height="385" src="http://www.youtube.com/v/hIk1t_z5JHk&amp;hl=en_US&amp;fs=1" allowscriptaccess="always" allowfullscreen="true"></embed></object>

_If this is not the power of internet what else is?_

