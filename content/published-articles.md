---
title="Published Articles"
slug="published-articles"
excerpt="Articles I wrote for reputed magazines and newspapers"
tags=[]
type="page"
publish_at="27 Jul 20 11:31 IST"
featured_image="https://cdn.olai.in/jjude/published-articles-gen.jpg"
---


_Some of the links might become unavailable because newspapers and magazine rearrange their links. If so you can [search](https://www.google.com/search?domains=www.tribuneindia.com&q=joseph+jude+site%3Awww.tribuneindia.com&oq=joseph+jude+site%3Awww.tribuneindia.com) via Google_

* [Toby Walsh’s 2062: The World that AI made](https://www.tribuneindia.com/news/reviews/story/toby-wals-2062-the-world-that-ai-made-118126), Tribune Chandigarh, Jul 26, 2020
* [Stuttering at the starting block](https://www.tribuneindia.com/news/archive/book-reviews/news-detail-832713), Tribune Chandigarh, Sep 15, 2019
* [Demystifying cryptocurrency](https://www.tribuneindia.com/news/archive/book-reviews/demystifying-cryptocurrency-781891), Tribune Chandigarh, Jun 2, 2019
* [Shaping AI's future](https://www.tribuneindia.com/news/archive/book-reviews/shaping-ais-future-761331), Tribune Chandigarh, Apr 21, 2019
* [What I learned as a CTO](https://hashnode.com/post/what-i-learned-as-a-cto-ciyvn6tjp00083h53frb6gp12), Hashnode, Feb 7, 2017

