---
title="Life is series of lost, found, and lucky breaks"
slug="life-is-lost-and-found"
excerpt="Get lost with your experiments so you can find your lucky breaks"
tags=["coach","systems","luck"]
type= "post"
publish_at= "10 Feb 20 15:00 IST"
featured_image="https://cdn.olai.in/jjude/lostfoundluckybreaks.jpg"

---

If you zoom into the daily lives of successful people, you will find that they are often lost. They are confused, not knowing if they will survive another day and doubting if they are on the right track or if they are doing the right things. Like all of us. Yet, there is one aspect that differentiates them from the rest of us.

They follow a system. As Michael Mauboussin [says](/it-delivery-system/), **when you have a good process, you inevitably obtain a good outcome, which looks like a lucky break to the outsiders**. They follow a process, whether it rains or shines. This routine gives them the power to carry on even when everything around them looks dull or dark.

![Life is series of lost, found, and lucky breaks](https://cdn.olai.in/jjude/lostfoundluckybreaks.jpg)

Their system has most of the following components.

### They have a vision

> I skate to where the puck is going to be, not where it has been. - Wayne Gretzky

Successful people are always a **step ahead of reality**. They can see into the future and ready themselves. Do they have a crystal ball? No. Do they have divine power? Of course not. Then how are they able to see into the future? They **observe trends and act fast**.

### They keep experimenting

> I have always been more interested in experiments than in accomplishment. - Orson Welles

Because they skate to where the puck is going to be, aka, a step ahead of reality, everything they do is an experiment. They know it well that if they **double the number of experiments, they double the number of their success**.

### They know what to measure

> I care about how many people's lives I've positively impacted. That's the unit of measurement I measure myself by. - Brock Pierce

How to pick up experiments to conduct and identify winners? That's where metrics come into play. Successful people obsess about **leading and lagging indicators** about progress. Some may choose money, others fame, and some others impact. But all have metrics. It is by metrics they communicate to the group that works on experiments.

### They calibrate

> When it is obvious that the goals cannot be reached, don't adjust the goals, adjust the action steps. - Confucius

Not all the experiments go well. Unlike the Ancient Rome, where all roads led to Rome, not all ventures lead to success. Winners avoid sunk cost fallacy. They don't mind **pivoting or abandoning experiments** if those experiments do not meet the metrics.

### They celebrate

> Celebration is a confrontation, giving attention to the transcendent meaning of one's actions. - Abraham Joshua Heschel

Celebration **eases the tension of single-minded pursuit**. It also energizes for the next cycle of hard-work. The celebration doesn't have to be always about champagne bottles and dance floor parties. It can be as simple as taking a walk in the park or reading a book of poetry under a tree.

### They are in it for the long haul

> Every company requires a long-term view. - Jeff Bezos

Life is not a 100-meter dash. It is not even a marathon. It is a never-ending journey, and they are happy to **go on this journey with a sense of wonder**.

Got questions. Ping me on [twitter](https://twitter.com/jjude).

### Also read

* [Tension between hope and reality](/hope-and-reality/)
* [Approximately correct](/approximate/)
* [The dichotomy of contentment and ambition](/contentment-and-ambition/)