---
title="We Need A Better Google Plus"
slug="we-need-a-better-google-plus"
excerpt="If Google wants its G+ platform to surpass existing social media sites, it should open it through a 'create' API, as early as possible."
tags=["networking"]
type="post"
publish_at="18 Mar 12 16:30 IST"
featured_image="https://cdn.olai.in/jjude/we-need-a-better-google-plus-gen.jpg"
---

There is a definite need for a social sharing and discussion tool that is unlimited by 140 characters. Among the existing social tools [G+](http://plus.google.com)  has the potential but in its current form it lacks an essential factor.

Let me start with where G+ fits between twitter and blogs.

I see G+ as a _social scratchpad_, where I can capture and discuss thoughts doing rounds in my mind. Since these thoughts are not yet well formed, I can't capture them within 140 characters. Because these are just random rants on many random topics, I don't want them on my blog.

Another scenario is longer _replies to blog posts_. Grand daddy of blogging, [Dave Winer](http://scripting.com/2007/01/01.html#theUneditedVoiceOfAPerson), believes that comments spoil the voice of the blogger. He encourages (in someway even forces) readers to write their comments as a post in their own blogs. But I don't want to pollute my blog(s) with replies to posts that I find interesting across the web. A social scratchpad would be the place for it.

Even though it is a scratchpad, I still want to engage others on those random thoughts, because learning happens through those serendipitous interactions. In a blog based commenting system, I am forced to moderate because of spam. _Existing moderation system discourages conversation_.

Social media sites, on the other hand, promote engagement. They encourage informal conversations, much like real life. By already being a member of the tribe - called circles in G+, I can initiate conversation with anyone. I can also ignore any such initiated replies and comments, if I don't perceive any value.

Another peripheral benefit that G+ brings is the aggregation of comments made within G+. I leave comments all over the place and there is no single place to aggregate the conversation. Engage.io[^2] is just for twitter and it doesn't add any value better than twitter clients like [echofon].

When all these work together, a spark might be generated in twitter, carried over to G+ and if it develops further there may be a blog post. So the flow of creation might be:

> twitter -> g+ -> blog

While G+ does well in all of the above, it fails in a crucial aspect - getting onto G+ is expensive.

Despite the resources at hand, Google is not able to bring a mobile application that makes it easy to get onto G+. On top of it, there is no 'create' API[^1]. If there is one essential factor behind the explosive growth of Twitter &amp; Facebook, it is their API, which enabled integration with every application that is found in mobile. But Google has been adamant in not making a public 'create' API.

If Google wants its G+ platform to surpass existing social media sites, it should open it through a 'create' API, as early as possible.

Will Google listen?


[^1]: [GPlus API](https://developers.google.com/+/api/) is just a read API

[^2]: seems to have gone out of business already

