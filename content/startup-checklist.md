---
title="Checklist For Evaluating Startups"
slug="startup-checklist"
excerpt="Before investing in a startup, I ask these questions which are based on PASTOR framework"
tags=["coach","startup","frameworks"]
type="post"
publish_at="03 May 21 19:53 IST"
featured_image="https://cdn.olai.in/jjude/pastor.jpg"
---

_Time to time, I work with VCs to assess a startup before they invest. These are the questions I ask to assess the startup. Hope these are helpful to you. If you want to chat about your startup or want me to assess a startup, contact me via [email](mailto:blog@jjude.com)._

This article uses [PASTOR framework](https://jjude.com/pastor) to evaluate a startup. 

![](https://cdn.olai.in/jjude/pastor.jpg)

### Problem
- Where does the problem lie in Maslow's hierarchy?
- Does it need a pain killer or a vitamin?
    - would it be cut if there is a budget cut?
- Does the problem change its meaning and impact as per the context of the users?
- What is the lifetime of the problem?

#### Existing Alternatives
- Is the problem novel and not solved so far?
- If the problem is solved in some fashion how is it solved, what’s the cost, what are the flaws in the current solution?
- What is the possibility that the current solution can scale to replace the solution proposed here?

### Audience
- Reaching an audience is sales-driven or marketing-driven?
- Do marketing materials reflect the transformation possible through the solution

#### Actual Users
- who are the actual users of the solutions (ex: kids for toys)?

#### Influencers
- Who influences the users?
- Who influences the economic buyers?

#### Economic Buyers
- Are there clearly identified, economic buyers? (In a bureaucratic system the EBa aren’t clearly defined)

#### Channels to meet these audiences
- If influencers and economic buyers are different from actual users, how will you reach influencers and economic buyers?

#### Immediately Addressable Market & Totally Addressable Market
- What channels are best to reach the IAM?
- What channels are owned, paid, earned, and rented?
- What are the adjacent markets that you can seep into?

### Solution
- Is there an experience factor built into the solution?
- Is there stickiness in the solution?

#### USP?
- what is the moat?

#### Technology 
- Architecture and Design
    - Maintainability
    - Extensibility
    - Flexibility
    - Scalability
    - Testability
    - Security (API end point security, data security, data storage security, SSL)
    - Data security (in-store, in-transit, data compliances)
    - 12 factor app compliance
    - Multi-tenant? Why and why not?

- Operation and Management
    - Reliability
    - Performance
    - Availability
    - Functionality
    - Correctness
    - Monitor ability
    - Versioning and Licensing

- Source Analysis ( Sampling)
    - Components and Software Inventory
        - Open Source
        - Closed Source
        - Original
    - Static code analysis (linters / analyzers )
    - Code Quality Analysis
    - Tools and Best Practices
    - Test scripts / automation

- Build and Deploy
    - Tools
    - Continuous Integration
    - Deployment Model and strategy
    - Capacity

- Governance and Compliance
    - Governance framework
    - Compliance Framework
    - Business Continuity
    - Data Exchange

#### Product Roadmap
- Planned roadmap of features in existing product
    - Qualitative comments on major features, differentiators, priorities
    - Time and cost estimates
- New planned products
    - USP and features
    - Time and cost estimates

#### R & D
- Current R&D portfolio — mature, in-progress, and planned
- Assessment of R&D production readiness and alignment with product roadmap
- R&D capabilities - people, processes, and performance

#### Team
- does the team have what it takes to pivot the solution if needed?
- Is there cohesiveness in the team?
- How do they resolve conflicts?
- Do they have a “disagree but commit” culture?

### Transformation
- How will the transformed situation of actual users, influencers, and economic buyers look like?
- Do the users need the solution continually to keep the transformation or can they wean off the solution?
- Does the solution has an inbuilt mechanism for word-of-mouth spreading of the transformation?
- Does the solution has a mechanism to receive feedback on the transformation?
- Does the solution has a leaderboard to reward usage?

#### Metrics?
- What are the leading and lagging metrics?
- Are there different metrics for different types of audiences?
- How can you identify usage and non-usage of the solution?
- What is the process to engage and attract the churned audiences?

### Offer
- what is the price of the solutions?
- is it variable price (per usage)?
- is there a membership fee?
- does the price differ according to the membership fee?

#### Cost Structure
- What is CAC?
- How much is spent on product development?
- How much is spent on Marketing & Sales
- How much is salary (top mgmt mainly)?
- Is sales commission based or fixed salary?

#### Financials
- Sales growth over the years
- Is there Pricing power?
- What are the gross margins?
- If there are competitors, what are they charging?
- Is there a need for consistent CAPEX and working capital to grow?
- Is it generating more cash than expenses?
- Percent of depreciation costs compared to the gross profit (lower is better for competitive advantage)
- Are there debt & interests?
- What is the average DSO?

### Rewards
#### Revenue Streams
- Life Time Value
- Is there a single revenue stream or multiple?

## Continue Reading

* [Think clearly about your venture with P.A.S.T.O.R. Framework](https://jjude.com/pastor)
* [Three stages of startups and how to chose tech-stack for each of them](https://jjude.com/stacks-for-startups)
* [Insights From Successful Indian Entrepreneurs](https://jjude.com/five-insights-from-successful-indian-entrepreneurs)