---
title="I am no bag of potatoes"
slug="i-am-no-bag-of-potatoes"
excerpt="compensations and promotions should be based on merits and not on seniority"
tags=[""]
type="post"
publish_at="26 Apr 08 16:30 IST"
featured_image="https://cdn.olai.in/jjude/i-am-no-bag-of-potatoes-gen.jpg"
---

'**Products should be generalized; People should be differentiated**'

This is a paraphrased form of what Jack Welch says in his book, 'Straight from the Gut', about people and products. Coming from someone who managed both labor-intensive organizations and knowledge-intensive organizations, this is a valuable advice to manage people. Yet, often organizations get this wrong.

We build specialized products for customers but generalize employees by labeling them as those who belong to 'last year batch' or '2 year batch' or 'non-BE batch' or something similar. Though employees are encouraged to compete across these 'labels', they are limited to their batches while deciding for promotion and compensation. Such continued labeling and restrictions frustrates even the highly motivated employees.

Let us say a 'fresher' demonstrates the quality of a team lead and does well in that role as well. However when the time of reckoning (appraisal time) comes in, she can't be promoted because she hasn't yet completed two years in the firm!

I face this issue year after year, both for myself and for those in my team. In my opinion the compensations and promotions should be based on merits and not on seniority. Wouldn't that be misused? Of course yes. May be there should be few parameters on which a candidate should be evaluated for the role rather than seniority. And may be there should be a panel too.

I used to think that this issue pertains to Indian IT companies as most in the HR department are from government positions. However recently I read few posts on the same topic from Scott Berken PM Clinic Forums and I was surprised that this is a global issue.

As I said earlier, I don't know the best answer for this issue. I'm like the guy who goes to a million dollar budget movie with a hundred rupees ticket and feels that the movie is not that good. Can I direct a better movie? Hell no. But I can differentiate a good movie and a bad movie. So is this issue. Do I know a better solution? No. But I know that current solution is not the right one. It needs to change.

