---
title="Sweet home is a safe home, even digitally"
slug="sweet-home-safe-home"
excerpt="Mistakes happen. Is your home safe enough to discuss mistakes?"
tags=["tech","vlog","parenting"]
type="post"
publish_at="25 May 20 10:30 IST"
featured_image="https://cdn.olai.in/jjude/sweet-home-safe-home-gen.jpg"
---

In this lockdown, schools are forcing kids to attend classes using online platforms, increasing the exposure of kids to the Internet. But parents want to protect their kids online. Technology isn't the answer to this predicament.

My friend, Arun's mother, is my role model for parenting. After graduating from college, I stayed in their house for three months as I was searching for a job. She was a successful lawyer, but she is also a fantastic mother. I'll give you an example.

Once Arun, I, and another friend decided to go to a retreat. Only Arun had a motor-bike, so we planned to go triples on his bike. Going triples in a bike is unsafe and illegal. But do young bloods care for such trivial things like safety and legality?

On the day of the event, Arun's mother sensed that we might do something stupid, like going triples. So she gave him enough money to go in an auto. Yet we ragged him to go triples. We enjoyed dodging the traffic police as well as the time at the retreat.

When we returned home, the first thing Arun uttered was, "Ma, I'm sorry. We went triples."

As much as it made me feel guilty, it also taught me the value of parenting. I vowed to emulate my friend's mother in creating a safe atmosphere for kids to share their mistakes.

Sure, create as many rules as needed, in the router and devices. But know this too: some kids will unknowingly wander into unsafe online areas; others will break those rules from time to time.

There is a saying in the corporate world. There are only two types of companies: hacked but unaware and hacked and aware. The same is valid for homes.

Sooner or later, the stringent cyber barriers will be broken. When that happens, your kids should feel safe to share their mistakes with you. If you don't have a psychologically safe home, your cyber-security controls are useless.

_This post is a detailed version of the comments I made to a local newspaper on this topic. You can read [my comments online](https://www.tribuneindia.com/news/lifestyle/focus-on-net-gain-81472)._

## Continue Reading

* [Teaching storytelling to kids](/storytelling-to-kids/)
* [How I'm dealing with isolation during covid](/dealing-with-covid/)
* [Life is series of lost, found, and lucky breaks](/life-is-lost-and-found/)
