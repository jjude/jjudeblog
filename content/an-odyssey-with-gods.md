---
title="An Odyssey With Gods"
slug="an-odyssey-with-gods"
excerpt="It is an age old question - is there a god? I journey through life to find practical answers. The journey isn't easy; but the answers out of the journey is practical and they work good for me."
tags=["opinion"]
type="post"
publish_at="19 Jul 08 16:30 IST"
featured_image="https://cdn.olai.in/jjude/an-odyssey-with-gods-gen.jpg"
---

It is an age old question - is there a god? I journey through life to find practical answers. The journey isn't easy; but the answers out of the journey is practical and they work good for me.

![candles](https://cdn.olai.in/jjude/mpj04276070000-thumb.jpg)

##### Innocent, Ignorant and Indifferent

Been born into a catholic family, my childhood days revolved around Church - Sunday Mass, Annual Festivity, Christmas and Easter celebrations and so on. Being in a predominately Hindu society, Diwali and Pongal were equally anticipated. Vocation Bible school and '_Kirubananda variyar_' discourses were attended with the same enthusiasm. We're innocently ignorant of our differences. Gladly these differences didn't matter.

I read Mahabaratha and Ramayana much before I read a single page in Bible. Right and wrong were taught from a cultural perspective than with a religious tone. I was friends with Vasantha, Jacqueline and Fatima (along with Augustine and Rama Subbu). We were either ignorant or ignored religious differences. What a bliss!

##### I got to save the world!

I transitioned from the blissful childhood into 'I-know-everything' college life. In college, I was introduced to 'deeper' Christianity. I read Bible cover-to-cover; prayed fervently for the salvation of the world; desired to be a Stephan or a Paul of this era. I was so passionate about Bible that you could say that I was breathing Bible.

I was interested in 'practical' Christianity than scholarly or religious Christianity. Instead of reading Bible 'to find favor in the eyes of God', I read it to find solutions for problems that I faced on a daily basis. Another aspect - as instilled by Mr. Murugan, Tamil master of High School - was 'to check out if it was so', rather than blindly accepting some theory or doctrine just because it came out of someone from the stage.

Though I read Bible and went for prayer meetings, there an aspect that I couldn't reconcile which is, 'Jesus is the only way to heaven; whoever have not accepted will not reach heaven'. It was the fundamental belief of Christian system. But I had difficulty accepting it. I believed in inclusion than exclusion - whoever accepted Christ will go to heaven; but I didn't believe whoever didn't accept will not go to heaven. My belief - rather disbelief of the fundamental truth - arose because of personal reasons: I lost my brother at an early age - he died when he was one. I couldn't accept the fact that he wouldn't be in heaven.

With that fundamental difference, I held the belief, 'Listen, I'm telling you what worked for me. In all probability it will work for you. But I'm not saying something else will not work'.

Except for that one difference I went around happy with hope.

Naively I looked into world and life with an attitude of 'Bible & I have the answers for all the questions; we have the solutions for all the problems of the world'. What Bible offered worked well for me; except when it didn't work, because the other party was under the satanic influence (or in some cases its not God's will).

##### Is God hiding somewhere?

While I was looking at life naively, life looked back with a sarcastic smile.

I was attending a small but vibrant Church. We were 'jolly good' youth bonded well with 'love of Christ'. While we were praying, working hard and waiting for revival, an unexpected happened - the Church split. I was shocked. I didn't understand. Personal differences cut the chords of love, bond and unity. The pastor and the youth leader went in different ways. I was torn - youth leader was my best friend who brought me into this Church; but Pastor seemed right. I stayed with Pastor, but I was still friends with the other side of the camp. Yet there was an 'unanswered bitterness' in me.

Couple of months later the Church was burnt in a 'mysterious' ways. Initial investigations indicated few miscreants. But the Pastor was future-focused and he didn't want to be clogged with the past.

I admired the way that Pastor conducted himself but I was drowning with questions with only an answer of 'God acts in special ways' answer.

What came after threw me off completely. In a personal disaster, Pastor disowned me. I did everything I knew (prayer, consultation) before I took my decision. But it went awry. When it went from bad to worse, I was all alone to face the situation. I was hurt, puzzled; left with no direction. The pastors, priests and religious leaders that we approached behaved in selfish and materialistic way. As I was hiding from situations so was God hiding from me.

##### May not be innocent; may be ignorant; definitely indifferent

What Christ couldn't do, whisky did; and what whisky didn't do, family and friends did. I sailed through the toughest of the time through tons of self-help, loads of family support and friendship of those who didn't know they were helping (I'll ever be grateful to Xavier, Venkat and Steven).

I crossed the roughest of the sea; but the journey thwarted pretentious religiosity and strengthened me spiritually.  I'm neither a naive Catholic nor a passionate Christian; but an indifferent agnostic.

I still read Bible. I still go to Church - on my birthday and on Christmas day - because that makes my mother happy. But I don't believe that Christ is the only way to heaven; in fact I don't believe in God, Christ or Heaven.

I have a practical belief - if we act with love and consideration, we can have heaven 'here and now'. If I don't make a heaven out life before death, I'm not worthy of heaven in life after death. Since I can make a heaven for myself and those around me, even I'm God, may be in a small way.

If you believe, you are a God too.

