---
title="Creating Knowledge Networks between Academic Institutions and Industry"
slug="kn"
excerpt="We need knowledge networks to bridge the demands for skilled engineers and lack thereof."
tags=["talks","networking","flywheel"]
type= "post"
publish_at= "19 Aug 20 10:00 IST"
featured_image="https://cdn.olai.in/jjude/network.png"
---

_National Institute of Technical Teachers Training & Research (NITTTR) trains teachers of technical institutions, like polytechnics and engineering colleges, in North India. The institute invited me for a talk on creating knowledge networks between educational institutions and industries. The article is an edited version of the speech._

About a million engineering students graduate every year in India. But only 20% of them have technical skills to get a job. Since India is focusing on building digital India, engineering jobs are on the rise. India can **bridge the increasing demands and lack of skilled engineers by forming a strong partnership between academic institutions and industry**. One specific partnership is building a knowledge network between academia and industry.

### Networking 101

Before we go into the nuances of building a knowledge network, let us see what a network is.

![What is a network](https://cdn.olai.in/jjude/network.png)

A network is "Nurturing mutually beneficial relationship." Don't see people within your network as inanimate objects. As humans, we have the innate desire to help and to get help. So look at others as humans and start building a relationship with them.

Academia and corporates remember each other only during the placement season, which is transactional thinking. Instead, nurture the ties throughout the year, as you would nurture a plant.

### Knowledge Network is ...

![What is a knowledge network](https://cdn.olai.in/jjude/kn2.png)

Now that we understand networks let us move to Knowledge Network. Knowledge Network is a specialized form of the network formed to develop, distribute, and apply knowledge. How can you build knowledge? Only when people come together from across boundaries. When you bring men and women from across geographies and streams of practice, you form **a vibrant knowledge network whose wells will never dry**. Everybody in the network should contribute to the body of knowledge as much as they take from the network. Only then the network will continue to thrive. If everyone looks after themselves and not the benefit of others, that network will wither away soon.

Archeological excavations have [proved](https://en.wikipedia.org/wiki/History_of_science_and_technology_in_the_Indian_subcontinent) that Indians were far ahead in scientific thinking. We have used sophisticated tools as early as [1.5 million years](https://en.wikipedia.org/wiki/Madrasian_culture) ago. So what is hindering our progress? **We box our people and knowledge**. We don't reach out across the boundaries. The reason for the tremendous growth in the developed nations is **the sharing of knowledge and collaborating for mutual benefit**. 

### Build a flywheel for success

![Flywheel for success](https://cdn.olai.in/jjude/factors-for-success.png)

What are the ingredients of a successful knowledge network? Every successful knowledge network is made of people, processes, and tools. All three have to work in tandem to create a knowledge network beneficial to all its participants.

When all three components work in unison, the network forms **a positive feedback loop, attracting more people who bring new tools into the network**.

If you are starting to build such a network, start with people. Without people, a system with process and tools is only a skeleton. People bring life into the network.

### People in Knowledge Network

![People in your knowledge network](https://cdn.olai.in/jjude/who-in-kn.png)

For a knowledge network for an academic institution, there has to be a collaboration between industry, other educational institutions, Government organizations, and students.

What is the **role of the Government** in the knowledge network? A significant role. They form the policy and lay the stones for a thriving ecosystem in which we can build a knowledge network. If successive governments have not played a role in bringing the Internet to every corner of this country at a cheaper rate, we won't meet today via online tools.

While industry experts bring in specialized knowledge, your alumni know your institution well, especially the recent graduates. They remember your institution's strengths and weaknesses. Bring them into the network and nurture the relationship. As they grow in their job, they will help other students.

Coordinators play a crucial role in building any network. They are the conduits between all the interested parties. So it is vital to get an energetic and optimistic person as a coordinator.

Everyone within the network should be willing to share their expertise, support each other grow, and interested in building the network.

### Process to build and grow knowledge network

![Process to build and grow knowledge network](https://cdn.olai.in/jjude/process-for-kn.png)

People have to work within a process, as the **process brings clarity** for everyone.

Consider this example. What will happen if I say I am a CTO, so I will come at any time for this presentation, talk for however long I want, and leave when I feel like? There would be chaos. So you have created a schedule for this seminar. This is an example of a process within which people should work. 

**Hackathons and Internships are the way to acquire and apply knowledge, and seminars aid us in distributing the acquired knowledge.**

Your students can organize hackathons where they come together to solve a problem in a fixed period of time. If hackathons are held regularly, they can become the highest value addition for the institution and every participant. You can invite industry experts to the hackathons to guide the students in understanding and solving problems.

In our ancient model of learning, we used to have [gurukulas](https://en.wikipedia.org/wiki/Gurukula), where students live and learn under a guru. Internships are supposed to be the modern version of it. Students learn from accomplished experts. Professors should also undergo internships. Otherwise, they won't know the problems faced in the industry. Once professors get exposed to these problems, they can formulate a better curriculum for their institution. Even CEOs and other executives regularly undergo internships.

Even though we are meeting virtually, this is a seminar where we are exchanging ideas. Likewise, you need to organize seminars where people share whatever they learned during hackathons and internships.

Though we in the industry focus on practical applications, **we can't discount theory**. If our employees don't understand the theory, we will waste an inordinate amount of time in getting anything done. 

### Tools for knowledge network

![Tools for knowledge network](https://cdn.olai.in/jjude/tools-for-kn.png)

People and processes alone are not sufficient enough. We need **tools to amplify our efforts**. If we take this online seminar as an example, the Internet, the Free Conference Call service, our smartphones have enabled us to connect and share our expertise. We need such tools to distribute knowledge beyond the boundaries of time and geography.

The first tool is a wiki. You **capture established ideas in wikis**. Examples of established ideas include a procedure for admission, applying for patents, and arranging hackathons. Wikis allow you to link to other concepts within the wikis and also track the changes to a post, both of which are helpful in building a body of useful ideas to students and teachers alike.

The second one is the blogs. **Blogs are used to capture and spread emerging ideas**. Machine learning and artificial intelligence are on the rise. There are many applications of these two. Your institution can set up a blog and ask your students to write about these topics, job opportunities in those areas, and learning paths for beginners.

Wikis and blogs are for internal consumption--for your students, lecturers, and professors. Your knowledge network also contains people from outside of your institution. You can use newsletters to inform them about progress in your institution. You can **distribute newsletters** to government organizations, alumni, industry leaders, and peers. Your newsletter can contain the gist of the knowledge your institution acquired or developed in the form of hackathons and patents. This will help others to reach out to you if they need help in building knowledge in the areas where you progressed.

### We need a knowledge network now more than ever

Gupta empire in the north and the Chola empire in the south are generally considered as the [Golden Age of India](https://en.wikipedia.org/wiki/Golden_Age_of_India). Both empires "made great advancements in many academic fields." If India needs to thrive and prosper, she needs strong academia, industries, and partnership between the two. When we build a collaboration between academia and industries, we can develop and distribute knowledge to every corner of the country as we did with internet access. Only when we make knowledge accessible to every citizen of India, we can claim to have arrived at a golden age.

_Do you know an institute, corporate, or a professional body that can benefit from my speaking? Feel free to refer me. You can view all the [talks I have given](/talks), for reference._

