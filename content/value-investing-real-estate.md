---
title="Value investing is better than real estate"
slug="value-investing-real-estate"
excerpt="Value investing is a gateway to all the wonderful things I learned in life"
tags=["wealth","wins"]
type="post"
publish_at="06 Jul 22 12:08 IST"
featured_image="https://cdn.olai.in/jjude/value-investing-real-estate-gen.jpg"
---


Value Investing > Real Estate

Around 2003, I bought a flat in Whitefield, Bangalore. Its value has more than doubled in 20 years.

I invested approximately the same amount on the stock market during the same period. In spite of all the market gloom and doom, its value also doubled.  
 
Why am I saying investing is better than real estate if they have reached similar values?  
  
For buying the flat, I had to **lock my money in a lump sum and make heavy monthly payments**. In contrast, I **started investing with as little as ₹1,000**. Each month, I invested a different amount. In some months, I was able to invest, but in many months, I couldn't. I increased the investment amount, as my revenue increased.
  
Despite this, the absolute return has remained the same.

Those are financial returns. Efforts in life are **not only about first order effects**. As far as life's unseen effects go, value investing has been an enormous benefit to me.

Value investing became **a gateway to all the wonderful things I learned in life**. It opened doors to understanding of mental models, psychology, anthropology, structured thinking, effective communication, and a desire to learn constantly. These topics have been a great help in my career development.

If I were 25 years old now, I would invest my money now so that it can compound. Buying a flat could wait.

_There are good comments when I shared this on LinkedIn. You might like the conversation there: https://www.linkedin.com/posts/jjude_value-investing-real-estate-around-2003-activity-6949905423634178048-hien_

## Continue Reading

* [Truth About Passive Income & Financial Freedom](/pasive-income-truths/)
* [Proven Biblical Principles To Build Wealth](/biblical-wealth/)
* [Fame Or Fortune](/fame-fortune/)
