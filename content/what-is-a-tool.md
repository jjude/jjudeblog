---
title="What Is A Tool?"
slug="what-is-a-tool"
excerpt="Is software first-order creation or a second-order creation?"
tags=["tools"]
type="post"
publish_at="08 Apr 12 16:30 IST"
featured_image="https://cdn.olai.in/jjude/what-is-a-tool-gen.jpg"
---


In an essay titled, [Software is not epic,](http://www.scottberkun.com/blog/2012/software-is-not-epic/) Scott Berkun, argues that Software is only a tool, a second-order creation, which is used by others to create first-order creations. So, he concludes, the Software is not an epic - a first-order creation - by itself.

While making that point, he defines a tool as

> something you make so someone else can make something.

I agree with this definition of a tool. But I disagree that the creations are static in their type. Consider the below sequence of creation:

A hammer is a tool to create a table.  
A table is a tool to create a novel.  
A novel is a tool to spark ideas.  
Ideas are a tool to create a revolution.  
A revolution is a tool to bring in change.

So the first-order creation of one becomes a second-order creation on someone's hand. That is the way the world progresses. And it is true of software too.

