---
title="How about banning bribe, Mr. Health Minister?"
slug="how-about-banning-bribe-mr-health-minister"
excerpt="there are so many vital issues to be resolved in India for a common man; so much of energy (time, effort, money) is spent on trivial issues."
tags=[""]
type="post"
publish_at="12 Jul 08 16:30 IST"
featured_image="https://cdn.olai.in/jjude/how-about-banning-bribe-mr-health-minister-gen.jpg"
---

For the record, I don't smoke. Nor do I like to suffer with second hand smoke.

Yet, I'm annoyed by the [Health Minister](https://en.wikipedia.org/wiki/Anbumani_Ramadoss)'s obsession to drag smokers to roads for a simple pleasure of a puff or two. One might ask, isn't it a good thing? Of course it is. But there are so many vital issues to be resolved for a common man; so much of energy (time, effort, money) is spent on this trivial issue. And I believe there is a better way, if at all one is resolute about solving the issue.

It is almost impossible to force people to stop smoking; they aren't kids man. They have their own senses. If someone doesn't realize that it is bad for their health, twisting their hands will lead only to rebellion - they will smoke more, not less.

If the health ministry is so particular about health hazards of smoking, stop it at the source. Ban the production and import of cigarettes. Oh! no, you don't have the guts to kick those rich industrialists; they will blow you off in a single puff. So you continue to nip the helpless common-men and continue to get into the TV box cheaply.

I am concerned about the health of the country than of those who chose to puff away their health. Will any of the minister fight against corruption? If bribe is banned (and strictly enforced), the country will flourish in leaps and bounds improving the overall health of the country. Will you (or your party) take up that cause sir?

