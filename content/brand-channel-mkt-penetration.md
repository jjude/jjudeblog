---
title="Branding, channels, and market penetration"
slug="brand-channel-mkt-penetration"
excerpt="Talk at IDE bootcamp as part of Startup India event"
tags=["talks","coach","startup"]
type="post"
publish_at="26 Jun 23 14:35 IST"
featured_image="https://cdn.olai.in/jjude/nitttr-talk.jpg"
---

GoI's Ministry of Education's Innovation cell organized a bootcamp on "Innovation, design, and entrepreneurship" at [NITTTR](https://www.nitttrchd.ac.in/), Chandigarh. I was invited to deliver a lecture on "Branding, channels, and market penetration". Here is an edited version of the talk.

![Branding, channels, and market penetration talk at NITTTR](https://cdn.olai.in/jjude/nitttr-talk.jpg "Branding, channels, and market penetration talk at NITTTR")

## True Essence of Branding

When we think about branding, colors, fonts, and logos often come to mind. But that's not what branding is.  You don't choose Netflix because of the color of its logo or buy books from Amazon for the font they use in their logo. **Branding is the promise a company makes** to you as a customer and whether it consistently upholds that pledge.

What truly matters in branding are the promises kept or broken. This generates word of mouth about your brand—whether it met expectations or fell short. Take Netflix as an example; their appeal isn't based on logo color or font design but on their promise: unlimited movies, cancel anytime, watch anywhere.

As a viewer, you consider if these three claims hold true in one way or another—explicitly or implicitly. If they can't deliver—for instance by lacking a mobile application—that breaks their promise and hurts their brand image.

So remember this: good branding means knowing your customers well enough to make promises you can keep—and avoiding those you can't uphold. That's all there is to branding—a commitment from a company towards its customers.

## Channels

Understanding branding leads us to the importance of channels, our pathways to customers. To navigate these channels effectively, we need clear answers for three things:   
- Who is our customer?
- Where do they gather?
- What is the best way to reach them?

Firstly, identify who your customers are. This is where many startups or service companies falter. They fail to define their customer in terms of needs, problems faced and spending capacity.

Then you must determine where they congregate. If your customers are startup founders, they might gather at Y Combinator or Tech Ranch. Real estate agents may have an association for networking purposes.

With this knowledge in hand, you can pinpoint how best to reach them. For instance, Rolls Royce doesn't showcase its cars at auto shows since their target audience isn't there. Instead, they participate in private jet shows because that's where their potential buyers are located - making a Rolls Royce seem like a bargain by comparison.

The final step is choosing your channels which could be online or offline depending on what suits best for your business model and customer base. Online platforms include websites and podcasts while offline ones consist of newspapers and industry events.

In some cases when reaching out directly seems challenging due to various factors such as being new into the market etc., partnerships can prove beneficial; like how Rolls Royce uses private jet shows as a channel instead of car exhibitions.

## Market Penetration

Let's talk about market penetration. First, we need to understand the game we're playing. What's our brand and what promise does it make? Who are our customers, what problems do they have, and how do we help them solve these issues? Only then can we plan for increasing our market share.

To penetrate a market effectively, ask three questions: 

1. What is your winning aspiration?
2. How will you win?
3. What capabilities must be in place?

Remember that strategy involves making choices that lead to other choices or eliminate some options while generating new ones.

For Netflix, their goal is to offer unlimited movies on all devices at any time. This defines their game plan. Then every decision about capabilities and strategies stems from this initial question.

So first off - what does "winning" mean for you? Is it about gaining a larger market share or achieving higher profit margins? Android leads in smartphone market share but Apple enjoys greater profit margins – which victory would you prefer?

The second query focuses on how you'll win; through which channels will you reach out to customers and at what price point(s)? Will prices differ based on customer type or will everyone pay the same rate?

Finally, consider necessary capabilities needed for success as defined by Tom Peters: having both "toilet paper" (resources) and "bullets" (tactics) ready when required isn’t just good military strategy—it’s critical business acumen too! This might involve hiring staff or streamlining processes among other things.
 
So ponder over these points because often times sales teams bring well-qualified customers into play but without an effective strategy in place their efforts could go wasted.

## Continue Reading

* [Guest lecture on entrepreneurship at Christ College, Bangalore](/entrepreneurship-talk-at-christ-college-bangalore/)
* [Three stages of startups and how to chose tech-stack for each of them](/stacks-for-startups/)
* [How to deliver value in the digital age?](/value-in-digital-age/)