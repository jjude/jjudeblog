---
title=" Customer Experience Is A Leaf, Not A Trunk"
slug="cx-is-leaf"
excerpt="If a company focuses on CX to the exclusion of sound business model, there may not be a company anymore"
tags=["talks","coach","experience"]
type="post"
publish_at="09 Dec 20 09:00 IST"
featured_image="https://cdn.olai.in/jjude/cx-round-table.jpg"
---
_Programic Asia invited me to a roundtable session on customer experience. This is the summary of my talk in the session._

![CX Roundtable Session by Freshworks](https://cdn.olai.in/jjude/cx-round-table.jpg)

Many of us, Indians, loved to fly Kingfisher airlines when it was still flying. Among the airlines at that time, it had the best customer experience. Yet, it failed. Why?

Customer experience (CX) is a buzzword now, rightly so. If you don't know, CX is a measure of **how customers feel when they contact a company**. That contact could be via ads, browsing on the site, shopping in the store, or the contact center. If the company provides a pleasant experience, customers talk about it positively.

If I borrow an analogy from Elon Musk, **CX is a leaf in the product tree**. It is an essential piece of the tree. The leaf changes through seasons and indicates life in the tree. We can easily differentiate one tree from another through leaves. But leaves don't survive on their own. They need to hang on a branch.

**Branches are the technology-architecture behind the products**. When the leaves are not properly attached to the branches, it becomes an easy target for hackers. That is how you end up with 20 million user-data in the black market.

Both CX & product architecture should rest on a sound business model, the trunk. As Rajiv Bajaj said, Kingfisher had **only models; there was no business model**. When you have a sound business model, you're clear about your customers and the problem you are solving for them. 

From a marketer's point of view, a company should focus on CX. But suppose a company focuses on customer experience to the exclusion of sound business model. Then, there may not be a company anymore, as it happened with Kingfisher airlines.

## Continue Reading

* [Delightful Customer Experience in a Highway Restaurant](/cx-in-highway)
* [How to choose technology for your business growth](/tech-for-biz-growth)
* [Agile in the C-Suite](/csuite-agile)