---
title="What small IT businesses can learn from Mary Meeker's trends report"
slug="2017-trends"
excerpt="Since 2001, Mary Meeker has been releasing annual internet trends report. This year report is full of insights that small IT business can use."
tags=["coach","insights","wins"]
type="post"
publish_at="26 Jun 17 20:57 IST"
featured_image="https://cdn.olai.in/jjude/2017-trends-architecture.png"
---
Mary Meeker is a partner in a venture capital firm, Kleiner Perkins. Since 2001, she has been releasing annual report on internet trends. It is always packed with insights for both technologies and business executives.

I have been studying [this year's report](https://www.kleinerperkins.com/perspectives/internet-trends-report-2017/) with a lens of a technologist in a small IT business. Small IT service companies are going through tough times, as competition is increasing. In India, especially, the cost arbitrage is vastly disappearing. Given this situation, what can they learn from this trend report?

![2017 Internet Trends](https://cdn.olai.in/jjude/2017-internet-trends.jpg "2017 Internet Trends")

**Ad Tech (and MarTech)** are on the rise. There are three types of opportunities:

* Ad placement —  video ads, in-app ads, contextual ads (location, time, and route).
* Ad display forms — video ads, ad boards like Instagram or Pinterest like ads. This is an opportunity for design oriented IT agencies to design various forms of ad placement in apps both web and mobile.
* Ad tracking — clicked on ad, didn't click on an ad, didn't purchase after clicked on an ad, purchased etc. These tracking mechanism should work in different placement and forms. Like from in-app ads and ad-boards.

I witnessed a perfect contextual ad recently. It started to rain in Panchkula, where I reside. Soon after, I got a [SMS from Uber](https://twitter.com/jjude/status/876700720862347264) with these text:

```
Joseph, look what the rains brought along = Your UberPASS!
```

This requires tracking many factors (weather, events in the city, special occasions, and so on) and sending a targeted message.

**Conversational commerce** is on the rise. They are happening either on websites (like what Intercom provides) or riding on chatbot platforms like Facebook messenger and Slack. Traditionally marketplaces and plazas are the best places to showcase your product and if possible to convert them into customers.

> **FB messengers and Slacks are the digital plazas.**

What this means is you need to win **Natural language processing** and connect it to ad tech. Can you place contextual ads in a digital conversation and track them for the above metrics?

**Gamification** is another trend that is permeating every aspect of digital life. It is used to improve engagement, productivity, loyalty, learning, and so on.

From **infrastructure perspective**, there are only two players that count. AWS is way high in popularity. 57% of the respondents are running on AWS. But, its growth is stagnant from last year. Azure, on the other hand, is hungry. It has grown 14% in a year! The statistics predicts it will grow even more in the coming years. 21% are experimenting with it, and 12% are planning to use it. These numbers are way more than other two platforms — Google cloud, and IBM. The message to IT services companies is clear: **if want growth, piggyback on Azure**.

![2017 AWS Azure Growth](https://cdn.olai.in/jjude/2017-aws-azure-growth.jpg "2017 AWS Azure Growth")

The report also contains a slide about software engineering trend. The world is moving to **API mashups**. When every aspect of businesses is digital, there are many combinations of business requirement. No one software service can satisfy it. So the world is catching up to what [Amazon has been doing for a decade](https://apievangelist.com/2012/01/12/the-secret-to-amazons-success-internal-apis/) — every service has an API.

Current way of scaling such service model is microservices. Every engineering team has to embrace DevOps culture.

Smartphones and other edge devices are becoming heavy computing machines. **Combine this computing power with APIs and Functions as services (aka serverless model), and businesses will build powerful applications on the edge devices**. Recently, Google announced [Tensor Lite](https://www.infoq.com/news/2017/05/google-tensorflow-lite) so as to run machine learning models on mobile.

Based on these trends, here is the **architecture that small IT services companies can embrace to win**.

![Architecture for small IT companies](https://cdn.olai.in/jjude/2017-trends-architecture.png "Architecture for small IT companies")

As an IT services firm, you should really be an expert on Engineering, DevOps, and infrastructure. If not, now is the time to build those skills. But these skills are not going to bring you business. Money is in what you can build with those expertise. Can you tap into martech, gamification, and parsing conversation? Even better, can you offer them as APIs for business verticals like healthcare, retail, or automotive? If you can build such an architecture, then you can shout "Show me the money".

<iframe src="https://giphy.com/embed/KrkvAXa1HZAjK" width="480" height="288" frameBorder="0" class="giphy-embed" allowFullScreen></iframe><p><a href="https://giphy.com/gifs/business-development-give-KrkvAXa1HZAjK">via GIPHY</a></p>
