---
title="Mastering sales as a new CTO"
slug="cto-sales"
excerpt="Prepare, Perform, and Probe is a three-stage approach that can help new chief technology officers to master and excel in sales."
tags=["coach","sales"]
type="post"
publish_at="06 Apr 18 07:42 IST"
featured_image="https://cdn.olai.in/jjude/ctosales.png"

---

"Most careers turn into sales jobs when you get senior enough," [tweeted](https://twitter.com/sama/status/943618320825118720), Sam Altman, president of Y-Combinator. Having spent two years as a chief technology officer (CTO), I have to agree this is true even for a highly technical role such as mine.

**People buy from people, based on trust**. For an offshore IT services company, there is none better suited to build trust in the minds of prospects than a chief technology officer.

![Selling as a CTO](https://cdn.olai.in/jjude/ctosales.png)

**Sales is both science and art**. Science is the rigorous application of techniques and methods for discovery. Art is presenting that discovery in an emotionally appealing form. In that sense, preparation is the science part, and performance on the sales call is the art part.

I can **repeat** my preparation routine as many times as I want. I can have a checklist of items and go through them one by one. I can teach my colleagues item by item. Preparing for a sales call is like sitting in a science lab and repeating an experiment along with a teacher.

On the other hand, performance on a call is an art. No matter how much I prepare, there is always a surprise. Prospect will throw a question I didn't anticipate. I can't say, I will prepare and come back. I have to think on my feet for a convincing answer and articulate it persuasively. I don't get a second chance. There is **no replay** with the prospect. That is why it is an art.

I follow a structured approach to handle sales calls. There are three parts to this approach. They are:

- Pre-call Preparation
- On-call Performance
- Probe (post-call and post-closure)

### Pre-call Preparation

I believe in the saying, "One-half of life is luck; the other half is preparation." I prepare relentlessly. **Preparation builds confidence and confidence wins trust**.

I prepare by asking questions. Here are some questions that help me prepare for sales calls.

### What are we selling?

When I started attending sales calls, I thought I could just say we are a well established IT services company and prospects will just sign-up for our services. How naive!

Stuart Cross, a strategy consultant, [says](http://www.crosswiresblog.com/strategy/go-on-pick-a-card/) there are **four types of companies**. They are:

1. best product (or service)
2. lowest price
3. most convenient
4. most customized

As an offshore IT services company, we are cheaper than most onshore companies. But we didn't want to compete on price (alone). We are definitely not developing niche products. That meant we had to play with the other two types. **Customized convenience** seemed like a winning option. Though I never mentioned these words in any calls, I am convinced of what we are selling.

### How are we different?

We are an IT Services company. So are the other dozen companies in the city. And there are hundreds more in the country. Then there are freelancers in Thailand and Eastern Europe offering similar services. Why should a prospect talk to me instead of a freelancer in Thailand?

Scott Adams, the creator of Dilbert cartoon, helped me think through it. He [says](http://dilbertblog.typepad.com/the_dilbert_blog/2007/07/career-advice.html) this while talking about building a career:

> Capitalism rewards things that are both rare and valuable. You make yourself rare by **combining two or more "pretty goods"** until no one else has your mix.

We, as a team, discussed this over and over. We didn't want to have platitudes as our differentiation. After all, prospects can identify these banalities from a distance. The best offshore IT services company? Best bang for the buck? Process-oriented value creators? We rejected all of them. It was tiring to reject one phrase after another. It was one of the toughest mental exercises we did. Eventually, we nailed it. How did we do it?

We listed all the projects we did. Then we poured over client testimonials. We listed what we believed as our strengths. We looked for common patterns in those. Our differentiation was obvious from these patterns.

### What are the objections?

Since sales is trust-based, I work to identify all the objections that could come our way and frame an honest answer. Trust is relative, not absolute. As long as the prospect trusts us better than the next competitor, we will win the sale. Funny thing about trust is, if you honestly accept certain disadvantages and flaws of yours, others trust you more.

### Have we practiced together?

Sales is a **team-sport**. Like any team-sport, you can't come together on the field and expect to play a great game. I "play with them" well before the call so as to understand what we are going to say and show in the call. Together we rehearse so that we can put up a great show on the call.

### Who is the prospect?

Our customers come from two broad categories—startups and medium enterprises. Startups at different stages have [different problems to tackle](/stacks-for-startups/). Enterprises deal with challenges that startups don't bother about. Knowing as much about the prospect and their requirement helps in **identifying the relevant arguments** for the call.

### Do we have examples?

Once I know enough about the prospect and their requirements, I look for projects that we can showcase. If I can get something into their hands to play with, that is the best. Not always I can get an app in their hands to play with. Next, best thing is a demo. Even if they don't want to view a demo, they can relate if I can quote relevant examples. **If they can relate to every word I say then there is a better chance of conversion**. That is why this is a key aspect for every call.

### What can go wrong?

Instead of doing a post-mortem of a failed lead call, I **do a pre-mortem**. I ask the team what can go wrong and work backward to avoid those issues. Still, I have had dry mouth, forgetting an exact word, and connectivity issues. I don't have control over everything that can go wrong. More calls I attend, I bring more factors under my circle of influence.

### On-call Performance

I take a note and a pen with me for calls.

I note down **names of people** on the call. Everyone likes to hear their name, and hear it pronounced properly. In fact, it is a good ice-breaker, if you ask them first to help you pronounce their name.

As the call proceeds, I take notes. I **listen for signals** and note them down. Is the prospect indicating a choice of technology, or delivery methods, or future plans? I scribble anything that will help me during the course of the call. It is easier to sell to someone of what they are already planning to do.

**If they laugh, I have won**. It is always a good time to have a good laugh. If they laugh with you, they will be happy to do business with you. I am always looking for a signal to make a joke. But way too often a clean, harmless joke offends people. That is why I prefer **self-deprecating jokes**.

### Post-call Probe

After every call, I write notes about the call. I note down what went well as well as what went wrong.

Did a prospect like a ppt based demo or a live product demo? Did a planned demo fail? Did a prospect like certain types of demos? Did they got attracted to certain types of explanation? We note them down and analyze if there is any bias in our thinking.

We **do more of what worked well**. We try to eliminate what didn't go well. At the least, we try to bring them under control. This iterative process improves subsequent sales calls.

Did the prospect had a new objection? Did we stumble answering it? It goes to the list of objections that I talked about earlier.

### Post-closure Probe

Whether we win or lose, I note down the reasons behind the result. Most often, companies have a process for analyzing failures, but not for wins. Doing more of what works well is a smart strategy.

The only aspect to be aware is that the world changes and what worked well last quarter may not work this quarter. That is why a consistent process of retro is a must.

### Getting better together

It is a privilege to represent a team to prospects to convince them that we can make their idea come to life. The more we prepare, play, and retrospect together, we get better together. Our win rates increase every quarter.

When we strive together, we thrive together. And then we become lucky.