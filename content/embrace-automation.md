---
title="Will A Chatbot Take Away My Job?"
slug="embrace-automation"
excerpt="Don't be afraid of losing job because of technology. Automation and new technology will create more opportunity for you."
tags=["wins","biz","coach","gwradio"]
type="post"
publish_at="27 Sep 21 12:56 IST"
featured_image="https://cdn.olai.in/jjude/automation.jpg"
---

It is not new to be scared of losing one's job to technology. The fear has been around since 1807. Gas lamplighters in New York went on strike in 1807 to protest electric street lights. Unfortunately, they couldn't stop the inevitable.

History repeated itself over and over again. However, the story is taking a twist.

Until recently, automation was all about reducing costs. Cutting costs improved the bottom line (profit) without significantly affecting the top line (sales). Executives have realized that **cutting costs has a limit, but increasing sales has no limit** in a globalized economy. CEOs want their top employees to work on the business rather than in it.

![Technology enhances job, not eliminate it](https://cdn.olai.in/jjude/automation.jpg)

Employees who are stretched thin trying to cope with their daily tasks have a difficult time coming up with new services, reaching untapped markets, and creating a better pricing strategy when their time is limited. Automation still lures companies, but not for reducing headcount, but so that employees have more thinking time.

The following two examples illustrate the shift in thinking.

In the early days of ATMs, tellers feared that they would lose their jobs. However, banks like the State Bank of India didn't let go of their employees, rather they **deployed extra tellers to open new branches**. SBI now has a branch every 10 kilometers. Tellers are happy, banks are happy, all because of technology. 

Chartered accountants feared losing the business they had when the Ministry of Corporate Affairs implemented digital signatures (DSCs) for filing forms. Later, they realized that DSCs freed them from geographical restrictions. For example, a CA in Delhi can serve a client in Mizoram. **Technology gave CAs more business, not less**.

During a recent discussion with [Liji Thomas](https://www.linkedin.com/in/liji-thomas/), an expert in machine learning and chatbots, I was reminded of these incidents. In response to my question, "will chatbots replace contact centers," she replied,

<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless src="https://share.transistor.fm/e/46957ca2"></iframe>

_Listen to our conversation - Conversational Bots: Yesterday, Today, and Tomorrow_

> Human beings are well known for their authentic intelligence. There is no way artificial intelligence is going to replace authentic intelligence. Both have their strengths. What is happening is **artificial intelligence augmenting authentic intelligence**.

When I pressed her, if there is a danger to my job as a CTO because of AI/ML, she replied:

> Chatbots probably have a lot of data to give to a CTO. So as a CTO, you can offload all the menial tasks to a chatbot and focus on creative work.

Liji's statement aligns with what Alex Kantrowitz, the author of "Always Day One: How the Tech Titans Plan to Stay on Top Forever" said in an interview with CMS Wire:

> Building technology into the workplace is not about efficiency. It is all in service of invention.

He quotes, not surprisingly, Amazon as an example:

> Amazon reinvented itself over and over again. It went from being an online bookstore to a website that sells everything on the internet. It became a thriving third-party marketplace, cloud services powerhouse, academy award-winning producer, voice-computing platform, successful hardware manufacturer, and grocer.

When he was asked about the playbook of the tech giants, he answered:

> One, it is about minimizing all possible support work they can to make room for employees to come up with new ideas. Two, it is about building systems inside companies to make sure that good ideas get to decision-makers.

Liji advises CTOs and other executives to **minimize all possible support work to make room for new ideas**.

As an employee, embrace automation. In fact, go even further. Introduce technology that allows you to spend more time thinking about the firm instead of handling support work. Checkout my course [Gravitas WINS](/gwcourse/) if you are interested in continous learning even when technology changes everything.

If you are an executive, focus on technology that gives your employees thinking time, instead of just efficiency. You will increase productivity and employee satisfaction, as well as sales. Everyone wins.

### ⚙️ Take Small Steps

- List all the work you do 
- Research on tools that can either eliminate it or reduce the menial work
- Use that time to start something you always wanted to do

### Notes
  - https://www.cmswire.com/leadership/alex-kantrowitz-create-a-culture-of-constant-reinvention/
  - https://sloanreview.mit.edu/article/learning-from-automation-anxiety-of-the-past/


## Continue Reading

* [Power Of Storytelling In Business](/biz-storytelling/)
* [Build on the rock for the storms are surely coming](/build-on-rock/)
* [Building a personal flywheel](/flywheel/)