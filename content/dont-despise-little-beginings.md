---
title="Don't Despise Little Beginnings"
slug="dont-despise-little-beginings"
excerpt="Long before an achievement comes to light, somewhere there is a small step. Don't ignore it."
tags=["insights","gwradio"]
type="post"
publish_at="20 Jun 10 16:30 IST"
featured_image="https://cdn.olai.in/jjude/dont-despise-little-beginings-gen.jpg"
---

The world celebrates achievement. Of course, bigger achievements are celebrated bigger.

<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless src="https://share.transistor.fm/e/869d784b"></iframe>

But long before that achievement came to light there was a small step, an insignificant beginning; sometimes leading to a doubt if anything good will come out of it at all.

Even in our own lives, we despise our little beginnings. Be it software development or photography or blogging, we compare our initial creations with the achievements of others and our creation 'looks' insignificant. We fail to realize those achievers started somewhere small too.

So, take the small steps, knowing giant leaps will follow.

