---
title="Video To Watch: Ideas are easy, execution is everything by John Doerr"
slug="john-doerr-video-execution-is-everything"
excerpt="John Doerr, venture capitalist, talks about the next big things and how to identify great ventures"
tags=["coach","startup"]
type="post"
publish_at="01 Oct 15 19:07 IST"
featured_image="https://cdn.olai.in/jjude/2015-10-john-doerr.png"
---
[John Doerr][1] is a venture capitalist who has backed some of the most successful entrepreneurs like Larry Page, Jeff Bezos, and Bill Campbell. In this lecture at UC Berkeley, he talks about startups, investments, career decisions and so on. Particularly interesting topics to me are, 'the next big things' and 'how to identify great ventures'.

[![Ideas are easy, execution is everything](https://cdn.olai.in/jjude/2015-10-john-doerr.png)](https://youtu.be/4xWGSUZmkIc "Ideas are easy, execution is everything")

**Next big things**:
- drone
- green technology
- local on-demand services
- cyber-security
- digital health

**How to identify great ventures**:
- technical excellence
- outstanding team
- strategic focus on un-served markets
- speed (execution)
- reasonable financing

If you are interested in startups, you should watch this video. It is about an hour long, but it is worth it.


[1]: https://en.wikipedia.org/wiki/John_Doerr
