---
title="Culture is what you do, not what you believe"
slug="culture-is-doing"
excerpt="What the book 'What You Do Is Who You Are' means to me"
tags=["books","coach"]
type="post"
publish_at="09 Aug 21 19:36 IST"
featured_image="https://cdn.olai.in/jjude/culture-is-doing-gen.jpg"
---

Every founder and executive wants to build a great company culture. Culture is usually defined as a set of shared values. In his book, '[What You Do Is Who You Are](https://geni.us/whatyoudoiswhoyouar),' author Ben Horowitz debunks that notion. 

## About The Author

Ben has been associated with the early stages of the Internet. He worked with Marc Andreesen at Netscape as a product manager. When Netscape was bought by AOL, he served as vice president of the eCommerce division. Afterward, he co-founded Loudcloud and pioneered what is now known as software as a service and infrastructure as a service. HP bought Loudcloud, and he became a VP at HP. Later, he and Marc Andreesen founded a venture capital firm and invested in unicorns like Airbnb, Stripe, Okta, and Skype.

Across every venture, he has played a **direct or indirect role in building the company culture**. This book vividly depicts that experience.

## About The Book

The book is his second. Ben is not only a practitioner, but he is also a great storyteller. By combining these two skills, you not only get entertainment, but also education and inspiration. You would expect a book on company culture from a tech entrepreneur to focus on how large tech companies built their culture. That's not what he does. 

His examples are drawn from historical figures. He is borrowing from Toussaint Loverture who led the only successful slave revolution in history, the Samurais of Japan, and Genghis Khan. Shaka Senghor, a gang leader in prison, is the only contemporary. An unlikely mix, but an eclectic one. This makes the book a great example of business storytelling.

## What Is Culture?

Culture is not a collection of beliefs. Ben says, "**Virtues are what you do, values are what you believe**." Your company's culture is how it makes decisions when you're not present. They are the assumptions your employees use to solve everyday problems. It’s how they behave when no one is looking.

Culture cannot be copied. You need to develop your own. Amazon's early culture exemplified frugality, for example. They are known for building tables out of cardboard boxes. Would that work for Apple, which promotes itself as a luxury brand? No way.

As a company grows, its culture will also change. Amazon built tables from cardboard boxes in its early days. A large organization with thousands of employees can't afford to do so.

## Why Do You Need Culture?

What is the purpose of having a culture?

The purpose of culture is not to beat a competitor or to attract new employees. Its goal is to **make the workplace a better place to work**. A better workplace **encourages employees to do the most meaningful work** of their careers. 

Having a good culture also **makes a company easier to do business with**, in the long run. The sales team can promise whatever they want, but the customer will form their own opinion of the company based on the daily interactions with the employees.

## How To Build A Great Culture?

Even though Ben discusses how to think about culture in different scenarios, he is not prescriptive. He does not provide a step-by-step guide to forming a great culture. Rather, Ben educates you in a fun and inspirational way about the principles upon which you can build a culture that is right for your company.

**People take their cue from the leader**. Thus, the culture will mirror the founder's virtues. Culture is built through constant contact. A leader can educate a team on cultural values by eating together, working out together, and studying together. There is no other option.

Steve Jobs explained about the 1997 Think Different campaign, "**One way to remember who you are is to remember who your heroes are**." Having heroes will inspire your team to reflect those values.

## What It Meant To Me?

From time to time, you come across a book that is a delight to read and at the same time shifts your thinking completely. "You are what you do" is one such book. This book has been an inspiration to me as both a parent of two boys and as a Gravitas WINS coach.

## Parenting

What is a family culture? What virtues do we espouse? Who are our heroes? I already thought about these deeply and wrote a post titled: "[Writing my obituary](/my-obituary/)." 

Constant contact transfers values, as Ben explains. Every day, **we eat at least two meals together**. We share stories, we tell jokes, and we have fun together. When we are together, we rarely look at the devices. I emphasize the importance of family time. I respect it. You better respect it too.

## Coach

In 2020, I launched a coaching program called "Gravitas WINS". Gravitas is a combination of confidence and competence. Competence comes from learning and confidence comes from displaying it. Both skills can be learned. Building gravitas requires a challenging and supportive community.

I am building a diverse community as part of the Gravitas WINS program. Every Thursday, we [learn](/gwshow/) different topics, from the power of writing to smartcuts to build a career and life. Every Saturday, we come together on [Clubhouse](https://www.clubhouse.com/club/gravitas-wins) to discuss books. Community members lead these discussions, thus building their competence and confidence. We have feedback calls after these sessions to discuss performance and ways to improve.

We have also started one-to-one meetups. Every two weeks, I match people for networking calls. During that call, they discuss their interests, challenges, and hobbies. So far, all the calls have gone well.

I have also begun [interviewing community members](https://www.youtube.com/watch?v=iKE7gwMAleE&list=PL538B_xQcbX7ugqIoZ-jkOOuO6x4jLKcR) for my [podcast](/podcast/), where I dig deeper into their careers and the skills needed to build one. Podcast interviews are a way of getting to know a member deeply while also displaying their competence and confidence.

## Quote To Ponder

No culture can flourish without the enthusiastic participation of its leader. - Ben Horowitz

## Continue Reading

* [What I learned about leadership from the book 'Lead like Jesus'](/lead-like-jesus/)
* [Book Notes - Smartcuts](/smartcuts/)
* [Book Summary : The McKinsey Way](/mckinsey-way/)
