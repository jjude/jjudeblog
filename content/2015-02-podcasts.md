---
title="Podcasts I Listen"
slug="2015-02-podcasts"
excerpt="I have never been a 'listening' guy; I'm more a 'reading' guy. But with launch of my startup, I turned to podcasts to learn."
tags=["sdl","consume"]
type="post"
publish_at="02 Mar 15 10:49 IST"
featured_image="https://cdn.olai.in/jjude/2015-02-podcasts-gen.jpg"
---


I have never been a 'listening' guy; I'm more a 'reading' guy. I like the whole process of holding a book, reading, highlighting and making notes at the side. Reading, highlighting and making notes evoke a feeling of interacting with the content.

Times, they change.

I launched a [startup][15] and my world turned upside down. I had no time to read or reflect. That concerned me. As a first-time founder, I knew I had to learn a lot but here I was plodding with what I knew. Podcasts came to the rescue.

I started with two podcasts. [How To Start A Startup](https://itunes.apple.com/us/podcast/how-to-start-a-startup/id922398209?mt=2) by Y Combinator and [Startup School](https://itunes.apple.com/au/podcast/seth-godins-startup-school/id566985370?mt=2) by Seth Godin. I got to hear from masters and I gained enormously from both of them.

I got hooked to podcasts. I searched for other ongoing podcasts that could help me in my startup journey. At about this time, I also started walking as a way to de-stress. I was walking twice a day, 45 minutes each session. So I looked for podcasts with episodes that I can hear within single session. I also didn't want to get into entrepreneurial constipation by listening only to startup podcasts. I chose variety.

Here are the podcasts that I listen currently. I don't feel compelled to listen to all episodes. If any episode gets boring, I skip them.

In each of the podcasts, I have listed the episodes that I liked. Listen to them and if you like them, I encourage you to subscribe to the podcast.

**[The Tim Ferriss Show][1]**: Tim brings amazing people to his show. [Peter Thiel](https://overcast.fm/podcasts/episode/112259102295135), [Peter Diamandis](https://overcast.fm/podcasts/episode/112259471561336) and [Matt Mullengweg](https://overcast.fm/podcasts/episode/112259262386475) are some of the recent ones that I enjoyed. Tim knows to ask questions that will invoke brilliant answers and once he asks a question, he shuts up for his guests to speak.

**[TED Radio Hour][2]**: Guy Raz curates TED talks around a common theme. [Success](https://overcast.fm/podcasts/episode/13541352123) and [Seven Deadly Sins](https://overcast.fm/podcasts/episode/13542148812) are the two episodes that I enjoyed.

**[Invisiblia][14]**: Another podcast by NPR. Invisibilia (Latin for all the invisible things) deals with intangible forces that shape our behaviour — ideas, beliefs, assumptions and emotions. [The Power of Categories](https://overcast.fm/podcasts/episode/278110745323521) is a fascinating episode. I don't like to box people, but this episode made me to think if it is even possible to throw away categories completely. After listening to this episode, I realised that categories are short-cuts to deal with the complexities of life; a world without categories will be chaos.

**[The Rocketship Podcast][3]**: Short, 20 minute, episodes about early days of startups. I'm at pre-revenue phase at [DSD][15], and so each of these episodes are helpful. In a [recent episode](https://overcast.fm/podcasts/episode/105980921465100), Steli Efti talks about following up indefinitely until you get a hard yes or no. He narrates one instance where he followed up more than 40 times until he had a hard answer! Talk about persistence. That's exactly what I have started to do.

**[The Truth][9]**: Short stories performed for radio. I have enjoyed every one of the episodes. Well crafted and performed. Listen to [Don't touch a thing](https://overcast.fm/podcasts/episode/3544677265144) and you will agree.

**[Security Current Podcast][4]**: When you are building a startup, it is possible to loose track of events in your own domain. In this podcasts, Vic Wheatman brings the latest news in security industry.

Do you listen to podcasts? If so, let me know the podcasts you listen to.

[1]: https://overcast.fm/podcasts/show/104549/m0Ipwo
[2]: https://overcast.fm/podcasts/show/12/YJwIRB
[3]: https://overcast.fm/podcasts/show/98702/nie8WH
[4]: https://overcast.fm/podcasts/show/100941/6spCxJ
[5]: https://overcast.fm/podcasts/show/104562/1RWx9J
[6]: https://overcast.fm/podcasts/show/56/XPriZi
[7]: https://overcast.fm/podcasts/show/559/UFVpCd
[8]: https://overcast.fm/podcasts/show/203802/LPrVqI
[9]: https://overcast.fm/podcasts/show/3301/N7JlHg
[10]: https://overcast.fm/podcasts/show/554/AH5MCM
[11]: https://overcast.fm/podcasts/show/139239/VJYRwj
[12]: https://overcast.fm/podcasts/show/274004/tYh2cI
[13]: https://overcast.fm/podcasts/show/262517/lAjFKR
[14]: https://overcast.fm/podcasts/show/259010/sRVuQt
[15]: http://www.dsdinfosec.com

