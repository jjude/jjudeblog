---
title="As I drive through chaotic Bangalore traffic…"
slug="as-i-drive-through-chaotic-bangalore-traffic"
excerpt="Can you learn from the chaotic Bangalore traffic?"
tags=[""]
type="post"
publish_at="26 Mar 08 16:30 IST"
featured_image="https://cdn.olai.in/jjude/as-i-drive-through-chaotic-bangalore-traffic-gen.jpg"
---

Bangalore is associated with growth of Indian IT industry. Unable to cope with such a tremendous growth, its roads are known for maddening, chaotic traffic. Ask any Bangalorian and they will tell you (horror) stories of 'getting stuck in traffic for hours'.

When I moved in two years back, I used to get irritated with such traffic jams. Now I sit in my car and listen to new CDs; or make that pending call; or if nothing to do, observe the mad crowd around.

I have spent such unbelievable amount of time in traffic jams that I draw parallels to the traffic around and the corporate world that I drive to, once such traffic jams clear. They might sound wacky; they should be since I thought over them stuck in traffic jams. Next time you are in a traffic jam, give a thought for this.

##### Risks, Risks, Risks

Regularly the main route becomes congested that every one has an alternate route. Whatever number of routes that you have, all 'variables' can come together to laugh at your high planning and cripple you!

It can so happen that on a Monday morning, you decide to leave early to beat the traffic (1st risk mitigation). On the way you find that the 'state roadways dept' dug the road in a way that you can't take it. You tell yourself, "Don't worry; I got time and an alternate route" and take an alternate route (2nd mitigation). As you drive along you find that the 'telephone dept' is busy laying cables and there is a diversion. Hmm...you start to get irritated. Yet not to loose your cool, you think of another alternate route (3rd mitigation) and there you are happily driving through. Not long buddy; there is an accident with a 'Tata Sumo' cab that is completely blocking the only route that is left. Now you are stranded. You are so close to office; yet you are so far!

This isn't further from the truth. Okay, it may not happen on a Monday morning; but it can happen on any day.

Being in IT, I keep relating to risk registers that we maintain and the multiple mitigation plans that we come up with. Even if you've been a smart project manager (or a corporate leader) to have thought of multiple mitigation plans, all 'variables' can hit you (or your project or your firm) and slide you out of control. Only by keeping 'cool' at those times, you can come out of the disaster.

##### Be Lean

As I get stuck in the traffic, I switch of the engine and watch in despair the cyclist and bike riders wade through the traffic and keep moving. I sit there with a powerful engine in a luxurious car, but I make no progress!  With all the luxury and power, I remain without any progress; but these guys make progress, however small it may be, and they reach their destination before me! Don't get me wrong - I am not saying luxury is not needed; it is of no use in a crisis.

When the going is good, you can have hundreds reporting to you. But when you have to salvage a project in crisis, you need 'two strong oxen; not hundred chickens'. Unfortunately you can find so many who just doesn't want to leave their power and luxury even at crisis. It only serves their ego; the group makes no progress.

##### Effective Enforcement

You will notice the chaos unleashed in its fullest possibility on the Bangalore roads -Jumping signals, Violating one-way and many other lawlessness. What amazes me is that whenever this is discussed by politicians or police, they seem to favor 'more-rules'; as if there are not enough laws.

As long as police think that every law is an opportunity for black money, any number of law will not achieve orderliness.

And as long as every politician want themselves to be an exception to the same rules that they devise, the common man continue to suffer.

Instead of devising 'more laws', enforce the existing ones firmly, effectively and without an exception. I've heard of one Kiren Bedi towing the illegally parked vehicle of a Prime Minister. She is retired and we have only that incident (and only one Crane Bedi) to celebrate!

I see a similar thought process in the corporate management. Despite their claims of employee-friendly and building family-like environment, the current corporates have so many restrictions - one can't take a photo of the campus; one can't bring their parents to show their workplace; one can't browse to social network sites and so on and so forth.

Google earth (and similar other sites) could be your security threat; not the photos that I take inside the campus. If you want me to feel proud of the company that I work for, I want to bring my family members and show them where I work; and if I decide to be unproductive, there are many ways of doing so; not just by browsing to social networking sites.

I know I'm making it sound so simple and there is another side of the story. But my point is instead of having a simple and effective guideline, the corporate culture is driven (in a madly fashion) with all restrictions. No wonder there is no creative solutions coming from here!

In a smaller scale, it is true of the design of enterprise systems and applications. There are so many restrictions placed on these systems that they have become so complex to use for end users while the management get away with exceptions.

##### Active Participation - the other side of the coin

I've watched with contempt those 'recently returned onsite' guys violating the well known rules without missing a heart beat. These guys will not stop a second praising the fantastic American/Singaporean/European roads as they zoom past the red signal. I've come to believe that infrastructure is not the major cause of chaos but we, the road users are. There are times that I go highly irritated and loose my cool, but mostly I say to myself, 'just because the other guy behaves as a son-of-a-bitch doesn't mean I need to behave one myself'. I'm convinced that if at least 10% of the road users become responsible users, the chaos will reduce to a large extent.

Coming to the corporate side, do you enjoy your work? Do you love what you do? I do. However, this is not true of so many of the millions in IT.

I keep telling my guys, either find a job that you enjoy or you enjoy the job that you get. Instead of complaining, you either quit or initiate or participate in activities that make your work and other's work enjoyable. Make yourself a better person to work with and make your workplace a better place to work for. I'm talking from experience and let me tell you it is possible.

This has been a long ramble. I've not attempted, either to solve Bangalore traffic woes or to bring a better corporate culture. I just drew parallel to each other.

