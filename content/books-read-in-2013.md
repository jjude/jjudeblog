---
title="Books Read In 2013"
slug="books-read-in-2013"
excerpt="Key insights from the eight books I read in 2013"
tags=["sdl","consume"]
type="post"
publish_at="08 Dec 13 16:30 IST"
featured_image="https://cdn.olai.in/jjude/books-read-in-2013-gen.jpg"
---

If I'm any smarter, its by reading. That is my way of standing on giants.

This year, I happened to read 8 books (am reading another two others; hope to complete them before the year). I didn't find time to write individual reviews. When I get time, I will write detailed reviews; in the mean time, here are short summaries and insights from these books.

1.  <a name="zen"></a>[Zen & Art of Motorcycle Maintenance](https://amzn.to/2JNXayK) by Robert Pirsig

    Lots of books and articles I read referenced this book. So I was intrigued to find out what made this book so special.

    To start with, this is a philosophical discourse written as a travelogue. Both topics interests me. Further the flow of the writing is captivating. Fascinated by the topics as well as by the writing, I kept reading whenever I could squeeze some time out of my schedule. A para or two of review won't suffice for this book. Here I'm listing just the distilled ideas from this fantastic book:

*   There are **two types of understanding**: classical & romantic. Romantic understanding concerns itself with the immediate appearance; while classical gets into the underlying mechanics. Lines and numbers of a blueprint is dull and boring for a person of romantic understanding but they are fascinating for a person of classical understanding.

*   "...rotisserie instructions begin and end exclusively with the machine." Technical manuals are abstracted from the customers using it and describes features of machine in a stand-alone fashion. What's the use? Product descriptions shouldn't list all the features, they should tell stories of "**jobs-to-be-done**."

*   "A person who cares about what he sees and does is a person who's bound to have some characteristics of quality." Every now-and-then, I get irritated by sloppy work done by so called skilled professionals. I never understood why skilled people would create sloppy work. After I read this line, I understood. **If you don't care, your output lacks quality**.

*   Time to time, we all get into "[**gumption traps**](https://en.wikipedia.org/wiki/Gumption_trap)." Whenever I'm stuck, I keep referring to the notes I have taken from this chapter. Identify the source of the trap — external or internal, and take steps to come out of it.

    If you are interested in philosophy, quality, systems-thinking, you will find this book a fascinating read.

2.  <a name="hdfc"></a>[A Bank for the buck](https://amzn.to/3mME0aV) by Tamal Bandyopadhyay
    Tamal takes us through the birth and growth of [HDFC Bank](http://www.hdfcbank.com/) through interesting vignettes. Business literature is filled with study of American businesses. Not enough focus is on Indian businesses. So I read with interest the account of a bank that took advantage of India's economic reforms.

   Three key insights from the book are:

*   How do you build a great company? Gather committed people, give them a vision and witness the magic unfold.

*   Technology plays a vital role in business. Businesses operate in a dynamic environment. So systems should be flexible enough to introduce products faster, even through multiple delivery channels like ATM, telephone, web and mobile.

*   A good brand, relationships, technology and smart marketing can open doors but after that delivery has to sustain the business.

1.  <a name="writing"></a>[On Writing](https://www.amazon.com/dp/B003BVFZ4Q?tag=jjude-20) by Stephen King
    If you want to get better at writing, get this book. The book is divided into two parts—first part is the auto-biography of King and the second part is the art and craft of writing. In the second part, King elaborates writer's toolbox—vocabulary, grammar and style.

    Two key-points from the book are:

   *   Even after tons of published novels, **it is a grind** for King to write. He sets a goal of writing 2000 words daily and meets it. He reads and writes for four to six hours. None of the developers I know, including myself, read and code daily. If only we did that, how much our software will improve.

   *   King got into an accident that nearly killed him. He had five operations and couldn't sit continuously to write. But he persisted to write. He says writing had a therapeutic power on him. I agree. **Working with passion indeed works magic** on you.

4.  <a name="rwd"></a>[Responsive Web Design with HTML5](https://amzn.to/33MPkfU) by Ben Frain

   In the past, I refrained from buying technology books since they become obsolete fast. I usually google for online tutorials, blog posts and articles littered over the web. This slowed my learning, and speed of developing products. So I started purchasing technical books. If they become obsolete so be it.

   Technical books are dry by definition. But Ben Frain **brings a conversational style to technical writing** and that makes this book easy to read and understand. (As I write this, I'm reading his second [book](https://amzn.to/3gj1Wk6), one on Sass & Compass. He is that good).


   As the author points out there is a particular dichotomy in Indian social values. We **accept women as divinities, not as equal human beings**. This book talks of few women who broke that social mould.

   I picked up this book when I saw [Jesse Paul](https://twitter.com/jessie_paul)'s story as the first one. My colleagues spoke highly of her and I wanted to know more about her. It is an interesting story and I started reading other's stories too. Another inspiring story is of [Kalpana Saroj](https://en.wikipedia.org/wiki/Kalpana_Saroj), a Dalit entrepreneur, who steered a failing company back into profit.

   The book is full of such David & Goliath stories, Goliath being the societal constraints. Any David & Goliath story is riveting, more so when you live in the social context pictured in the story.

   The stories themselves are great but the narration could have been better. Many pages are dull and boring. Secondly, the author amplifies the constraints laid by men but never acknowledges men who extended helping hands to these women, though the characters themselves mentions them. Despite these little flaws the book is an interesting read because of the strength of the characters.


   Jacqueline narrates events that eventually led to the creation of [Aumen](http://acumen.org/) fund.  It is relatively easy for a white female to tour a developing world country rather than call it her home and serve its people. But there are brave and committed souls like Jacqueline who does this impossible task to improve thousands of people in the process. I admire such sacrificing spirits.

   There are four key points that Jacqueline makes with her experience of serving in Africa & India:

   *   It's **no humility to live a lesser life** than the one you are capable of living. In fact it's a crime to live such a life.

   *   People don't want free money; they want **access to capital and market to make their lives better**.

   *   There are way too many problems that need to be addressed to lift people out of poverty—education, public health, access to clean water, access to capital and market. **Not every problem can be reasonably addressed by a single entity**, even government. It needs partnership.

   *   Bringing solution to any public problem involves multiple individual factors like belief of people, infrastructure, education, empowerment etc. Answers to individual parts may be easy; **challenge is in bringing all the answers together** to solve the problem.

7.  <a name="findingfavor"></a>[Finding Favor](https://amzn.to/37yLNmw) by Lana Long

   A novel, a gripping one. An orphan girl is forced to chose between freedom and friendship. In a misunderstanding she lets go of her friendship only to get back both her friend and the freedom she so desired. The plot and narrations are well made. Am happy that the only novel that I read this year turned out to be a good one.

8.  <a name="free"></a>[Free as in freedom](https://amzn.to/33Ptpot) by Sam Williams

   Stallman is almost a lone voice in the software field fighting for software-freedom. I have heard of him, I have read about him, but never read a detailed account of his fight. This book weaves a neat story about how Stallman came to fight against copyright in software and promote freedom in software field. For him, **freedom is an ethical issue; not a legal one**. I strongly believe, **world is a better place because of Stallman** and the world needs many more captivated with Stallman's philosophy (though his tactics may be wrong). When we start sacrificing freedom, we end up with NSA.

Reading is pivotal to [learning](/sdl). That too, adding variety makes it all the more interesting. I'm glad to have read and read variety this year.

