---
title="Mohan Mathew on 'Management Consulting'"
slug="mohanm"
excerpt="What is management consulting, how to thrive in management consulting, what are the best and worst part of consulting?"
tags=["coach","gwradio"]
type="post"
publish_at="28 Sep 21 09:20 IST"
featured_image="https://cdn.olai.in/jjude/mohanm-gen.jpg"
---

<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless src="https://share.transistor.fm/e/aefdbc1d"></iframe>

- How do you start a management consultancy assignment, how do you approach the situation, how does it end?
- Building context for successful completion of the project
- How to present the conclusion to decision makers?
- How Covid changed the focus of organizations and management consultancy?
- How can anyone get into management consultancy?
- What are the skills needed to thrive as a management consultant? 

### Watch on YouTube

<iframe width="560" height="315" src="https://www.youtube.com/embed/8m6iDizFwJA" title="When Outsiders Create Value" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Connect with Mohan Mathew

- LinkedIn: https://www.linkedin.com/in/mohanmathew/
    
### Resources mentioned

- David Marquet, Former-Captain, US Navy Seals: Turn The Ship Around!: https://www.youtube.com/watch?v=ivwKQqf4ixA

## Connect with me
- Twitter: https://twitter.com/jjude
- LinkedIn: https://www.linkedin.com/in/jjude/
- Website: https://jjude.com/
- Newsletter: https://jjude.com/subscribe/
- Youtube: https://youtube.com/gravitaswins
- Email: podcast@jjude.com
- Executive Coaching Program: https://gravitaswins.com

## Your feedback counts

Thank you for listening. If you enjoy the podcast, would you please leave a short review on Apple podcast or on YouTube? It takes less than 60 seconds, and it really makes a difference in finding this podcast. And it boosts my spirits. 😀

I would like to hear what resonated the most with you in this episode. Either you can email me or share it on social media and tag me. Thank you for your support.

Have a life of WINS.

## Amazing tools I use:
- I record all the remote interview with [Riverside.fm](https://riverside.fm/?via=joseph-jude)
- I use [Transistor.fm](https://transistor.fm/?via=jjude) for hosting these podcast episodes

_Gravitas WINS Radio is produced by [Joseph Jude](https://jjude.com/). Be sure to subscribe on [Apple Podcast](https://podcasts.apple.com/in/podcast/gravitas-wins-radio/id1562802449), [Spotify](https://open.spotify.com/show/7LOBjAyjBkechMC0xMIj9E),[Google Podcast](https://podcasts.google.com/feed/aHR0cHM6Ly9mZWVkcy50cmFuc2lzdG9yLmZtL2dyYXZpdGFzLXdpbnMtcmFkaW8=),[YouTube](https://www.youtube.com/gravitaswins), or the podcasting app of your choice._

Please check out other episodes of [Gravitas WINS Radio](/gwr-themes/). For easy consumption, they are categorized by themes. If you are a fellow podcaster, you might be interested in [How I Podcast](/how-i-podcast/) or [tools I use](/i-use/).
