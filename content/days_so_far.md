---
title="Days so far…"
slug="days_so_far"
excerpt="Can working in internal project be any fun?"
tags=[""]
type="post"
publish_at="03 Mar 06 16:30 IST"
featured_image="https://cdn.olai.in/jjude/days_so_far-gen.jpg"
---

I came in with a doubt. My concept of an internal system was a team of dull-headed, not-so-useful employees reporting to an equally dull-headed person. You can't blame me! I had not witnessed a successful IS department. Why did I join then? Well, I'm not going to give you all the reasons, but enough to say that I was lured. More over, who can stay in a hell hot city for long?

As the cliche goes, the first impression is the best impression. What I saw on the first day beat me and thrashed all conception - rather misconception - of an IS team. It was a big team - bigger than few of the client projects that I worked. This energetic team is the backbone of the fast growing company. Be it a system for HR, Sales or Finance, everything was implemented. They also had umpteen other tools. Not just implemented, but actively supported by a commited helpdesk team.

It is a place for growth. Literally so. The team is well constituted to be a training ground for freshers with enough experienced people guiding them in all process of software engineering. The more I came to know about the department and team, the more surprised I was.

Then came Cricket. Inter departmental cricket match. Girls gathered runs; Managers got bowled out; Head-of-departments danced! There were fire-cracks, drum beats, music, cheering, and somersault. Yes, there was somersault. Not just once, but for every wicket by IS team. That was awesome. I've never met someone who is as egoless as this person. He was whistling, shouting and did everything to cheer the team. Even when we lost, we lost in style.

And then the trip to Malpi. Almost the whole team came for the trip. Two days went so fast because all we had was nothing but fun. I was filled with college nostalgia.

Did I even mention about sweets? No, I'm not talking about team members being sweet to work with. Rather about real sweets. No week passed by without someone bringing a sweet box. I guess, I've eaten more sweets in the past three months than my entire life! His birthday, her wedding, she went home, I woke up fresh - oh! I lost track of reasons.

Let me not paint an all-play-no-work picture. The team is run like any other client project. For their part, the users pound us, punch us and few times pat us. There is always a new project, an enhancement or a bug to fix. Coaching freshers and getting work out of them is a challenge on its own but it is also fun to work with young minds. All of this isn't possible without an appropriate IS head. Whoever made the choice for IS head couldn't be more right. It is always wonderful to work for a well knowledgeable but a cool-headed person. (It is unfortunate that I'm writing this after appraisal is over and highly unfortunate that he doesn't fall for simple praises).

There is a little bit of everything and that makes it lovable to work in IS. Time to time, I feel ashamed of the prejudice I had of an IS team. For once, I am glad to be wrong.

