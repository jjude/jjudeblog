---
title="52 Checkpoints . 6 Questions . Fulfilled Life"
slug="weekly-review"
excerpt="How to get 52 chances to increase your chances of succeeding in goals?"
tags=["productivity","coach","wins"]
type="post"
publish_at="02 Apr 22 13:01 IST"
featured_image="https://cdn.olai.in/jjude/52-checkpoints.jpg"
---
![Weekly Review](https://cdn.olai.in/jjude/52-checkpoints.jpg "Weekly Review Template")

Every Sunday, I review the progress I made in the previous week and plan the coming week. As a result, I will get 52 chances to refine my priorities and increase my chances of succeeding in my goals.

I have reserved two hours on a Sunday evening for weekly reviews. Before the review, I will take a walk and think over these questions so my mind is alert and away from biases as much as possible.

After that, I sit down and fill out these answers. 

1. **What went  well and why?** In the past, I took things for granted when things went well, believing that there were no reasons to analyze them. Now, I analyze the reasons to repeat that process and reap the benefits again.

   The only way to postulate a theory about success is to study successive successes in multiple areas. And that is precisely what I am doing in this question.

2. **What didn't go well and why?**  Compared to the previous question, this is the opposite. I try to be objective rather than to assign guilt or blame. Did I do what I should have done? Should I have asked for help? Was it a tail-end of a wrong process I followed?

3. **What adjustments should I make to my goals?**  What adjustments should I make to my goals based on the above two assessments? Should I still prioritize a certain goal if I am repeatedly delaying it? Is it better to park it for a later date or abandon it altogether?

    Are there any changes in the external environment, such as technological, economic, or other factors? Should my goals be adjusted to reflect these changes? Are these external events making these goals obsolete or a lot more expensive? If so, I will make the changes.

4. **What 10x actions should I take?** This is the sequel to the adjustment. I will try to focus on certain goals and take 10x action toward them. 10x could be in the form of shortening the time, gaining a partner, or doubling what works in pursuit of the goal. Hunger should be demonstrated by actions, otherwise, it is just heart-warming tea-talk.

5. **What should I stop doing?**  To increase my effort, I should stop doing all the things that drag me down. It's like filling up a leaky bucket faster and faster. Then I try to eliminate things that drag me down, are not beneficial, or are simply wastes of time.

6. **What I need?** I need help if I want to achieve 10x what I had originally planned. I cannot do it alone. Help comes from tools and people. I will seek them out. The silver lining is that I have a specific job to do, and I'm trying to find a solution to that problem. Specific is terrific.

    When I say people, I am not referring to employees or virtual assistants. I am talking about people who have achieved what I want to achieve. I ask them specific questions. The only challenge in that is to get answers specific to my situation. So instead of asking "what will solve my current problem," I ask, "how can I think about this situation?". 

<!-- When you subscribe to my newsletter, you'll get the template I use for my weekly review. -->

_Be productive. Have a fulfilled life._

## Continue Reading

* [Three Types Of Goals You Should Set](/three-goals/)
* [Themed Days - My Productivity Secret](/themed-days/)
* [If You See Everything, You'll Get Nothing](/focus-to-win/)