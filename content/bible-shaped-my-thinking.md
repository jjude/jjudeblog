---
title="How the Bible shaped my thinking?"
slug="bible-shaped-my-thinking"
excerpt="If a book doesn’t change your mindset you have wasted your money in buying it and time in reading it."
tags=["faith","insights","coach"]
type="post"
publish_at="28 Oct 22 12:59 IST"
featured_image="https://cdn.olai.in/jjude/bible-shaped-my-thinking-gen.jpg"
---

If a book doesn’t change your mindset you have wasted your money in buying it and time in reading it. Well I have invested both time and money in reading the Bible. Here are some ways in which it shaped me.

1. Constantly renew your mind
2. Trust but verify (even if it is St. Paul)
3. Is life probabilistic or deterministic?
4. Man’s action vs destiny
5. Be ok with contradiction much like Jesus
6. If you don't love yourself, you can't love others
7. If you can't zoom in and out, you don't know what you are talking about
8. God blesses you to be a blessing
9. Jealousy can eat you from inside
10. If introspection is good for God, it is good for me too
11. Be a salt
12. Run your race

## 1. Constantly renew your mind

> “Don’t be conformed to this world, but be transformed by the renewing of your mind” Rom 12:2

The norms and truths you and those around you hold may not be true. Continually question your beliefs and renew your mind.

## 2. Trust but verify (even if it is St. Paul)

> "examining the Scriptures daily to see whether these things were so." Acts 17:11

Fake news is nothing new. It happens in all domains. Respect people, but always verify their claims, even if they are trusted folks.

## 3. Is life probabilistic or deterministic?

> "The lot is cast into the lap, but its every decision is from Yahweh." Prov 16:33

In the short run, life seems unpredictable; in the long run, if you play by rules, results are predictable.
Much like in the short run stock market is a voting machine (works according to popularity contest), but in the long run it is a weighing machine (shows the substance)

## 4. Man’s action vs destiny

> "The horse is prepared for the day of battle; but victory is with Yahweh." Prov 21:31

Don't expect success without preparation. In spite of preparation, events beyond your control (luck) determine success.

## 5. Be ok with contradiction much like Jesus

Jesus is both a lamb and a lion.

> Be wise as serpents and harmless as doves Mat 10:16

Different situations demands different responses. Know and respond. That is why you need wisdom (which is a gift from God, as per Bible)

Being a lion and contained is chivalry; Being a lamb and showing off as a lion is a bluff.

## 6. If you don't love yourself, you can't love others

> "You shall love your neighbor as yourself" Mark 12:31

Are you fat, short, black, bald? Love yourself as you are. Only then you will love others without any judgement.

## 7. If you can't zoom in and out, you don't know what you are talking about

> In Mark 12:30,31 Jesus sums up the entire Bible into 2 commands: Love your God; Love others like yourself.

That's it.
Want more, read 10 commandments.
Want even more?
Read the entire Bible for how others lived these principles, their situations, and the rest.
Try doing this for the important areas of life.

_For some of my own summary of what I've learned on different domains, read [distilled wisdom](/distilled-wisdom/)_

## 8. God blesses you to be a blessing

> "I will bless you. You will be a blessing." Gen 12:2

When God blesses you, he expects you to help others come up in their life. To the needy, not to the rich. That is the purpose of God's blessings.

## 9. Jealousy can eat you from inside

> You shall not covet your neighbor’s house. Ex 20:17

Be content with where you are; Be ambitious about your future. Don't look at others and feel envious.

Envy is a really stupid sin because it's the only one you could never possibly have any fun at. Charlie Munger

## 10. If introspection is good for God, it is good for me too

> "God saw everything that he had made, and, behold, it was very good." Gen 1:31

If God conducted retro daily and weekly, then I should too.

## 11. Be a salt

> You are the salt of the earth Mat 5:13

Don't be a recluse. It is your duty to preserve and contribute to the taste of society.

## 12. Run your race

> I have finished the race 2 Timothy 4:7

Each one of us have a calling. Find that calling. Run that race. You'll have satisfied life.
It is so easy to envy the races of others and switch tracks all the time. That causes stress and heartburn.

## Continue Reading

* [What I learned about leadership from the book 'Lead like Jesus'](/lead-like-jesus/)
* [Proven Biblical Principles To Build Wealth](/biblical-wealth/)
* [What are your mantras for life?](/life-mantras/)
