---
title="Get Over Your Fears, For They Are Just Imaginary"
slug="fears-are-imaginary"
excerpt="Learn to separate lies from facts; then, reject the lies and work hard and smart to turn the facts to our favor."
tags=["insights","luck"]
type="post"
publish_at="03 Jan 13 23:47 IST"
featured_image="https://cdn.olai.in/jjude/fear-unsplash.jpg"
---
2012 has been a fantastic year for me. BlogEasy, the Mac application that I created, sold 40 copies, though the app is in AppStore for less than six months. It is not the number of copies sold or the dollars it brought that  makes me glad about 2012.

What makes me glad about 2012 is that, I got over the annoying, scary, paralyzing fear about selling.

Way back in 2000, I created a cross-platform app called 'FotosOnWeb', an app to create photo albums for the web. This was before the wide-spread photo hosting services that automatically create photo albums. Some popular magazines covered FotosOnWeb, people who used it appreciated and even donated money, yet I was so scared of selling that I gave it for free.

Two writings goaded me to finally venture into commercial software development. First one was a book, [The Flinch](http://www.amazon.com/The-Flinch-ebook/dp/B0062Q7S3S) by Julien Smith. Another one was an interview with Paulo Coelho, the author of, 'The Alchemist'. Both authors discussed how we continue with avoidance of pain to our disadvantage and the factors that hold us back are mostly lies that we tell ourselves.

I was telling countless lies to myself.

![Fear](https://cdn.olai.in/jjude/fear-unsplash.jpg)

_Photo by [mwangi gatheca](https://unsplash.com/@thirdworldhippy) on [Unsplash](https://unsplash.com/)_

"This is the era of UI designs and your design skills sucks."

"AppStore criteria are so stringent and their rejection ratio so high, your app will be rejected."

"There are hundreds of thousands of apps. Whatever you develop will be drowned in this pool."

"You are an introvert, you are no good in reaching out to people behind marketing sites and convincing them to list your app."

"Selling involves so many setups, financial registrations and approvals. If you mess-up anyone, you will land in trouble."

So on and so forth.

Julien Smith and Paulo Coelho inspired me to face the lies I was telling myself.

I decided to develop a product that I will charge from the beginning. Getting started was half the battle, the other half was to persist in locking myself in a room, day after day, ignoring haunting doubts. It wasn't easy. It took a mix of courage and insanity. I met each milestone—developing, testing, using it myself, submitting to AppStore and so on and so forth, with the same mix of courage and insanity.

I also got lucky. After about three months of its release, BlogEasy got its positive review in mac.appstorm. That gave the needed push to make enough sale.

This is not the first time I scaled scary lies.

When I was a kid, around 12, I almost drowned. That incidence instilled a fear of body of water. Since then, I never went anywhere near a pool of water. But when I was 34, I realized I'm being idiotically paranoid. So I hired a swimming coach and for a month I went swimming everyday, no matter how I felt. That part about the feeling was important because most days I felt that I'm going to drown or other younger kids in the pool were mocking me. Somedays you just need to ignore your feelings and do what is right. I ignored my feelings many days and the benefit is I can swim now.

What did I learn from facing these lies?

The beauty of believable lies is that, part of the statement is a fact and a false part attaches itself to the fact part so innocuously that we believe the whole statement to be true. We need to learn to separate lies from facts; then, reject the lies and work hard and smart to turn the facts to our favor.

That is what I did to get BlogEasy to the AppStore in 2012. I plan to do more of that in 2013.

### Also read

* [Until everything gets better](/until_everything_gets_better/)
* [Life’s little lessons](/lifes-little-lessons/)
* [38 Lessons](/38-lessons/)