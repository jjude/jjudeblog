title="An awesome tip from Jack Ma & Derek Sivers to build your career"
slug="specialize-or-generalize"
excerpt="Should you specialize in a single technology or learn as many business functions as I could learn? Let us find what Jack Ma & Derek Sivers have to say."
tags=["coach","vlog"]
type="post"
publish_at="08 May 17 20:13 IST"
featured_image="https://cdn.olai.in/jjude/splst.jpg"
---
There was one question that occupied my mind in the early part of my career. That question was: Should I specialize in a single technology or learn as many business functions as I could learn? After all, my friends were choosing to build their career on SAP or Oracle Forms, or Java.

I’m going to be honest with you. I didn’t find the answer easily. I found it through lot of experimentation.

In the last few years, I’ve listened to popular entrepreneurs answering this question. I was pleasantly surprised that their advice confirms what I found through trail and error.

First is Jack Ma, the owner of Alibaba. He says in [this video](https://www.youtube.com/watch?v=cESSaKQYfnU) in the early part of your career, learn as many things as you possibly can. Then as you age, focus on your strengths and build on them.

[![Specialist or Generalist](https://cdn.olai.in/jjude/splst.jpg)](https://www.youtube.com/watch?v=dcpLmoXnSUI)

Let me read out to you what another successful entrepreneur has to say on this. He is Derek Sivers. I came to know about Derek through his TED talks. He is the founder of CD Baby. He sold it for 22 million dollars and donated all the money to charity.

In his podcast interview with Tim Ferriss, he talked about how he started his career. He didn’t have any lofty goals back then. He just wanted to be a professional musician and he started as one playing in a pig show. He said [this about starting a career](https://sivers.org/2015-12-ferriss):

> When you’re earlier in your career I think the best strategy is you just say yes to everything, every piddly little gig, you just never know what are the lottery tickets.

What I have found working, and what these two entrepreneurs say is this: Learn as much as possible in the early part of your career. If you get a chance to work in marketing department, do it. If your company asks you to develop a server application, develop it. If you come across an opportunity to sell, sell. Do this for about 10 years. By doing this, you would learn quite a lot about most of the business functions. You will also know what you like the most and what comes easily to you.

Once you find what you like and what you comes naturally to you, dive deep into it. Become master of those skills. That way you will have a rich life. When I say rich, I don’t necessarily mean money, but satisfying life.

For me, I found out that I love technology and teaching. That is exactly what I am doing through this blog and my new vlog.

Good luck finding what you like and what comes naturally to you.

### Similar Posts

* [The curse of EVERYTHING and NOW on building your career](/all-and-now/)    
* [11 Lives to Build a Rich Career](/11-lives/)    
* [How Your Attitude Impacts Your Career Growth?](/how-your-attitude-impacts-your-career-growth/)    