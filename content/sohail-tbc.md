---
title="Sohail Khan on 'Why startups fail'"
slug="sohail-tbc"
excerpt="An interview with 2x founder on overcoming startup challenges and building a thriving community of founders and builders."
tags=["gwradio","startup"]
type="post"
publish_at="01 Nov 23 20:25 IST"
featured_image="https://cdn.olai.in/jjude/tbc-yt.jpg"
---
Startup founders dream of building the next big thing. At least I did when I launched a cyber-security startup. But as the statistics show majority of the startups fail and fizzle out.

Today I'm talking to Sohail who wants to change that. He has been a founder himself, not once but twice. He understands the field well. He has started The Builders club, a vibrant community to support and help startup founders and other creators in their dream journey. We are discussing why startups fail and what can be done to reduce the chance of failure.

<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless src="https://share.transistor.fm/e/f3f1b9e9"></iframe>

## What you'll hear
- Data Driven Approch
- How Sohail started "The Builders Club"
- What startup lessons Sohail is using to build the community?
- Play vs Chasing a number
- Frameworks
- The Builders Club & Its Benefits
- Difference between other startup communities and TBC
- How community reduce the chances of failure?
- Keeping the sense of belonging as TBC grows
- Future of TBC
- What's the kindest thing anyone has done for you
- Best leadership quality and who manifested it
- What is the definition of living a good life?

## Edited Transcript

_coming soon_

## Watch on YouTube
_coming soon_

## Connect with Sohail Khan
- LinkedIn: https://www.linkedin.com/in/sohail-khan-55303410/
- The Builders Club: https://nas.io/tbc/

## Connect with me
- Twitter: https://twitter.com/jjude
- LinkedIn: https://www.linkedin.com/in/jjude/
- Website: https://jjude.com/
- Newsletter: https://jjude.com/subscribe/
- Youtube: https://youtube.com/gravitaswins
- Email: podcast@jjude.com
- Executive Coaching Program: https://gravitaswins.com

## Your feedback counts

Thank you for listening. If you enjoy the podcast, would you please leave a short review on Apple podcast or on YouTube? It takes less than 60 seconds, and it really makes a difference in finding this podcast. And it boosts my spirits. 😀

I would like to hear what resonated the most with you in this episode. Either email me or share it on social media and tag me. Thank you for your support.

Have a life of WINS.

_Gravitas WINS Radio is produced by [Joseph Jude](https://jjude.com/). Be sure to subscribe on [Apple Podcast](https://podcasts.apple.com/in/podcast/gravitas-wins-radio/id1562802449), [Spotify](https://open.spotify.com/show/7LOBjAyjBkechMC0xMIj9E),[Google Podcast](https://podcasts.google.com/feed/aHR0cHM6Ly9mZWVkcy50cmFuc2lzdG9yLmZtL2dyYXZpdGFzLXdpbnMtcmFkaW8=),[YouTube](https://www.youtube.com/gravitaswins), or the podcasting app of your choice._

Please check out other episodes of [Gravitas WINS Radio](/gwr-themes/). For easy consumption, they are categorized by themes. If you are a fellow podcaster, you might be interested in [How I Podcast](/how-i-podcast/) or [Tools I Use](/i-use/).

## Continue Reading

- [Munish Jauhar on 'Building A Business From A Tier-2 City'](/munishj-on-biz-from-2nd-tier-city/)
- [Rishabh Garg on 'Power of Twitter, Side-hustle, and Community'](/rishabhg/)
- [Rajesh Madan on 'How To Build A Thriving Community'](/rajesh-community/)