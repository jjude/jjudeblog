---
title="Three part solution to leaders burnout"
slug="leaders-burnout"
excerpt="Fundamental principles don't change."
tags=["wins","coach"]
type="post"
publish_at="01 Jul 22 07:33 IST"
featured_image="https://cdn.olai.in/jjude/leaders-burnout-gen.jpg"
---

Burnout is a term introduced to the common language in 1974. With everyone working from home after COVID, there is more discussion around occupational burnout, now than ever before. 
  
Did you know that burnout as well as its potential solutions for leaders were discussed many millennia ago?

As I was meditating, I found a reference to burnout in the second book of the Bible - Exodus.

According to the Biblical narrative, about two million Jews escaped Egyptian slavery and traveled to Israel via the Red Sea. Moses led them on this journey.

While the Jews were traveling to their land, Jethro, Moses' father-in-law visited them and observed Moses at work. After observing, Jethro, says:

> The work is too heavy for you; you cannot handle it alone. You'll wear yourself out.

That sounds so relevant, doesn't it? It is likely that Jethro would say the same if he observed today's leaders.

Jethro, then, offers Moses three pieces of advice to ease his burden in shepherding the Israelites. He lays out a template for any leader to avoid burnout:
- teach them
- delegate to a capable team and
- handle the toughest cases
  
The burden of Moses as a leader is eased after listening to his father-in-law. Let's take a closer look at these three pieces of advice.

## Teach

> Teach them the decrees and laws, and show them the way to live and the duties they are to perform.

Leaders should teach. Each interaction with the team is an opportunity to discuss values explicitly or implicitly.  

Leaders often abdicate their responsibilities to teach. Either they are scared of teaching or they consider it beneath their level. They teach implicitly by the way they decide, communicate, and handle the team, even if they choose not to. In any case, group members mimic those in authority. So, it is better to be explicit about it and teach as Jethro advised Moses.

## Delegate

> Select capable men from all the people--men who fear God, trustworthy men who hate dishonest gain--and appoint them as officials over thousands, hundreds, fifties and tens.

When leaders coach well, they can prepare the next layer of leaders, who in turn can train other layers of leaders. 

The next tier of leaders has to be set up for success if delegation has to work. They should be trained, know the expected outcome, and receive required support they need.

- ## Handle toughest cases

  > Let them bring every difficult case to you
  
Delegation doesn’t mean abdication. Leaders should still handle the toughest of the cases. When done well it does two wonderful things:

Leaders are often detached from the daily activities of their units or companies; they do not know what is going on. The leaders will remain informed of the company's activities by handling a few cases. By still handling cases a leader can keep their skill sharp and current.

Leaders who solve the toughest cases lead by example. Others can learn from them. Other members know that the leader is still on the field playing and they take courage from their example.

Leaders today face more challenges than ever before. But the principles to solve them are the same as they always have been.

### References
- [How many Israelites left Egypt in the exodus?](https://www.gotquestions.org/Israelites-exodus.html)

## Continue Reading

* [Work, Life, And Fun Is A Flywheel Not A Balance](/work-family-fun-flywheel/)
* [Are You Chasing Success Or Status?](/status-or-success/)
* [Approximately Correct](/approximate/)
