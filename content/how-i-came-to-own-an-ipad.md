---
title="How I Came To Own An iPad?"
slug="how-i-came-to-own-an-ipad"
excerpt="Not only Apple creates smooth and beautiful products, but they also devour their competition unexpectedly"
tags=["tools"]
type="post"
publish_at="16 May 11 16:30 IST"
featured_image="https://cdn.olai.in/jjude/how-i-came-to-own-an-ipad-gen.jpg"
---


Apple released its iPad 1 in India, just few weeks before launch of its iPad 2, leading to an assumption that Apple considers India as a [dumping ground](http://www.pluggd.in/is-india-a-dumping-ground-for-apple-products-297/). The buzz in the social media was that, [Apple is ignoring emerging markets](http://twitter.com/gmanka/status/60566108394098689). This meant that iPad 2 wont be available in India for a very long time.

I could have asked any of my friends in the US to buy me an iPad 2 and dispatch via someone returning to India. But too much of 'I-want-to-help-Indian-economy' [ideology](/how-ideologies-impact-problem-solving/) prevented me from doing it.

So I [decided](/is-problem-solving-an-art-or-science/) to buy an Android based tablet.

But there was no Android tablet available in the market. There was [Samsung Tab](http://www.samsung.com/global/microsite/galaxytab/2010/), but it wasn't really a tablet since it was running on Android OS meant for phones. [Samsung Galaxy Tab](http://www.samsung.com/global/microsite/galaxytab/) was announced but not yet released.

So here I was in India waiting for a real tablet. Then something unexpected happened.

There is a reason Apple is the [most celebrated brand](http://www.guardian.co.uk/technology/2011/may/09/apple-tops-google-global-brands) now. They beat their competitors in every factor. Not only they create smooth and beautiful products, but they also devour their competition unexpectedly. One such move was their introduction of iPad in India.

Apple announced that iPad 2 will be available in India by first week of May.

Unlike the US there were no long queues in India to get an iPad. Even the shortage of iPads were attributed to shops ordering just few units.

When I went to [Anything Mac](http://www.anymac.biz/) for servicing my MacBook, I casually asked, if they had an iPad 2. Since I didn't see any crowd and also the shop itself was so small, I had assumed they won't have one. But I was wrong. They had iPads. The receptionist apologetically said they had only the wi-fi ones, which was great since I wanted only wi-fi ones.

So there I became the proud owner of iPad 2.

### Also Read

- [What Is A Tool?](/what-is-a-tool/)
- [An User's Perspective on OSes: Horrible, Bearable and Adorable](/oses-pov/)
- [I used Android for ten months, and I don't like it.](/android/)

