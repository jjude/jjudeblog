---
title="Why I Support Koodankulam Protesters?"
slug="why-i-support-koodankulam-protesters"
excerpt="You may disapprove their point of view, but defend their right to have a point of view. Or else, tomorrow you will lose the privilege to have a point of view."
tags=["opinion"]
type="post"
publish_at="17 Oct 12 16:30 IST"
featured_image="https://cdn.olai.in/jjude/why-i-support-koodankulam-protesters-gen.jpg"
---


I support Koodankulam[^1] protestors. You should too, regardless of your support for nuclear power plant.

In fact, I believe, for India to develop, it needs to tap whatever energy it can get, including nuclear. So if I support nuclear energy why do I support the protestors?

1. They are exercising their 'right to protest' which is fundamental to democracy. Each of us are passionate about the stand we take in the issues that matter to us. If the state take an opposite stand, we have the right to protest. Didn't we get our independence because someone protested?

2. Not only they are protesting, they are protesting peacefully. When was the last time you heard of a protest that lasted for more than a year without burning buses, damaging shops and loss of life? I can't think of one.

3. Let us not confuse dissent with disloyalty. If we do that, each of us can be accused of disloyalty. As Pritish Nandy [tweeted][2], its only when we push protestors out of the mainstream political process that they turn extremists.

4. Those armchair intellectuals who criticize these protestors as fools should understand that these protestors aren't twitter trolls—those who sit comfortably at their houses and blabber whatever they want on a medium available for free. Each of these protestors contribute their hard-earned daily wages to organize the protests. Can the critics organize at least ten people for ten hours in support of the nuclear energy? Let us learn to disagree without despising, debate without demeaning, understand without undermining.

5. Some of the concerns are genuine—availability of uranium for and in India, contamination of sea water by daily discharge from the plant and unpreparedness and indifference of state to handle emergencies (we all know what happened during and after Bhopal gas tragedy). Let us also not morph 'nuclear energy is safe' into 'any nuclear plant is safe', because in this case, there are lot of unanswered questions like the liability and waste disposal. Without addressing these concerns, suppressing their protest is just autocratic, not a healthy democratic process.

You may disapprove their point of view, but defend their right to have a point of view. Or else, tomorrow you will lose the privilege to have a point of view.

[2]: http://twitter.com/PritishNandy/status/21819432529
[3]: http://sethgodin.typepad.com/seths_blog/2011/03/the-triumph-of-coal-marketing.html
[4]: https://en.wikipedia.org/wiki/Koodankulam_Nuclear_Power_Plant

[^1]: For those who don't know [Koodankulam][4] is a village in southern part of India where Indian Government is setting up a Nuclear Power Plant. Though the villagers in and around Koodankulam are protesting for almost a decade against the power plant, they have intensified their protest in the past year. It has also become popular via social media.

