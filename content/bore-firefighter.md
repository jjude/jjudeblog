---
title="Who gets your reward - Bore or Firefighter?"
slug="bore-or-firefighter"
excerpt="You get what you reward - Steven Covey"
tags=["wins","coach"]
type="post"
publish_at="01 Dec 21 09:20 IST"
featured_image="https://cdn.olai.in/jjude/bore-or-firefighter.jpg"
---

![You get what you reward quote](https://cdn.olai.in/jjude/bore-or-firefighter.jpg)

I'm lazy. My favorite job would be "sitting simply and getting monthly." I was not always like this. In the past, my preferred management style was firefighting. When I was putting out fires on one side of the project or the other, I came alive with a sense of purpose, and to my managers, **I looked busy and indispensable**.

I wanted to make a change. I framed procedures for dealing with each leaky area that caused fires. I loved it. But my managers were like the domesticated tigers that tasted blood. They wanted problems that they can show to their bosses. "Everything is ok" seemed like a status from an incompetent manager.

"JJ, everything looks normal for a month. Is it a calm before a storm?" they asked.
"Are you sure, you've not missed anything?" they doubted my competency and I hated it.

There was an even bigger problem. My managers **couldn't appraise boring and calm work**. Working late hours and weekends were visible and measurable. But, quiet deliveries were not measurable. Peers who invented newer fires got promotions and bonuses, but I was advised to enroll in a project management program.

Managers got more of what they rewarded.

I faced a choice: either keep inventing escalating layers of fiery coal or stay out. I chose to stay out. I am too proud to be rewarded for my incompetence.

## Continue Reading

* [Culture is what you do, not what you believe](/culture-is-doing/)
* [Build on the rock for the storms are surely coming](/build-on-rock/)
* [When To Listen To Your Critics?](/critics/)