---
title="Luck Loves The Paranoid"
slug="luck-loves-paranoid"
excerpt="The paranoid prepare and hence prosper. Others perish."
tags=["luck","wins"]
type="post"
publish_at="20 Apr 22 06:19 IST"
featured_image="https://cdn.olai.in/jjude/luck-loves-paranoid.jpg"
---

![Luck Loves The Paranoid](https://cdn.olai.in/jjude/luck-loves-paranoid.jpg "Luck Loves The Paranoid")

"What could possibly go wrong?" is the first question I ask myself every time I am part of a project. My goal is to identify any flaws that may have occurred as a result of my enthusiasm for the project. As soon as I know what could go wrong, I can install enough **guardrails and guiding lights** to allow the team to travel without colliding on the sides.

Charlie Munger said this in a humorous way: 

> Tell me where I'm going to die. So I won't go there

One thing is for sure in life. Life is uncertain. As my friend Rishabh Garg [tweeted](https://twitter.com/rishabh_grg/status/1513336158897733634) recently, 

> Life is so unpredictable. Everything was going good and then in few minutes, it becomes complete opposite.

Life is unpredictable, so can you stop living? Of course not. You **create as much safety** as you can. Knowing there will be emergency expenses in a given year allows you to start saving. If you're aware that your physical body will deteriorate as you age, you can maintain your fitness by eating healthily and exercising regularly. When you are aware that your cognitive abilities will deteriorate with aging, you can maintain mental alertness by meditating and continuing to learn. So, when an emergency situation arises, and it will, you will not only survive but also thrive. **Those who are prepared will prosper; those who are not will perish.**

Stock markets have a concept called margin of safety. The margin of safety is a cushion that allows for some losses to occur without causing major problems. You will always see the price of the stock go lower from the price you bought when you buy a stock for the long term, no matter how much you calculate the correct price to buy. By knowing this, you can purchase at a price that will reduce the impact of stock market fluctuations. When the price falls, you won't panic and sell. I purchased Ashok Leyland at a price of 20 and Manali Petrochemicals at a price of 10. Both decreased by 25%. Even a 50% reduction suited me fine. My cognitive cushion allowed me to stay put. Both stocks are currently trading at 120, and I have a handsome profit. You might say I'm lucky. At the time, I was paranoid.

Richard Branson used a similar logic when he started Virgin Airlines. He knew his new airline was more likely to fail. If that happened, he didn't want to be in charge of expensive planes. Therefore, he leased second-hand planes instead of buying. Once he was sure, he added more planes. That's how paranoid people keep their good luck.

Rephrasing former FBI Director Robert S. Mueller, III, "there are only two types of companies: those that have been hacked and those that don't know they have been hacked." An optimist will be pleased with all of the tools and processes in place. Paranoid people prioritize monitoring and alerting since they know it's a matter of when, not if, they'll be hacked.

COBOL was my first programming language. Nobody would know it today. It's a software saying, if you're not updated, you're outdated. In the face of fierce competition from the younger generation, I'm afraid I'll become obsolete. As a result, I invest a ton of time, money, and effort into keeping myself relevant.

"Only the paranoid survive," Andy Grove, former CEO and Chairman of Intel, said. Why? The **paranoid attracts luck by planning for things that could go wrong** and preventing or minimizing the damage.

### Next Action
How can you put this into practice?

You could be managing a project, investing in a stock, or running a business.
Make a list of everything that could go wrong.

Your "what could go wrong?" concerns will fall into one of three categories:

- things over which you have authority
- things over which you have no control
- things you can redefine to gain a little more control

Often, we worry about things beyond our control. Focus instead on the things you can control. And reframe everything else.

The first time you attempt this exercise, you will have difficulty categorizing, identifying mitigation plans, and then implementing them. Don't worry. Just keep going. You will learn how to be paranoid and avoid every risk you can.

Do you want to be lucky? Start being paranoid.

_You can [read](/my-writing-method/) about how I wrote this article if you're interested._

## Continue Reading

* [How To Get Lucky?](/luck/)
* [Life Is Series Of Lost, Found, And Lucky Breaks](/life-is-lost-and-found/)
* [Approximately Correct](/approximate/)