---
title="New year brings a new challenge"
slug="new-year-brings-a-new-challenge"
excerpt="New challenges as I become a freelancer."
tags=[""]
type="post"
publish_at="23 Dec 08 16:30 IST"
featured_image="https://cdn.olai.in/jjude/new-year-brings-a-new-challenge-gen.jpg"
---

Life throws challenges at you at the most unexpected time. You can either choose to accept the challenge and make the best of it; or ignore it and probably regret for the rest of your life.

I wanted to be a freelancing consultant six years back - when the markets were high and CRM (and ERP) consultancy was hot. At that time I was posted in Belgium and most of my colleagues were freelancing. But for various reasons, I couldn't get into freelancing.

Now, there is a widespread pessimism in the market and I got a freelancing contract! How ironic is that?

I took more than a week to debate with myself. The boss, the team, the work and the clients made the work environment one of the best I ever had; but having already handled series of roles that were available, I was running the risk of becoming complacent.

On the other hand, the freelancing contract is on e-governance. With increasing economic growth in India, e-governance is becoming prominent; and with tech savvy Barack Obama taking control in the US, I believe, many countries will embrace e-governance. Downside? I've to move to Delhi, learn Hindi and probably understand 'red tape', 'bureaucracy' and 'politics' in their real terms.

Having debated with myself, the pros and cons, I discussed with the family. Honestly speaking, I expected resistance from them. Because, for the past three years my sister and I are settled in Bangalore and our parents are with us. We were able to take family trips and have quality time together as a family. To my pleasant surprise, they were extremely supportive. That gave me the confidence to take it further and discuss with friends. Finally I took the decision to be a freelancer.

Slowly things fell in place - resignation was accepted; last date finalized; contract was signed; and joining date accepted.

I've been preparing myself by reading a lot about freelancing (and e-governance); yet I'm sure there will be plenty of things that I'll have to work out as I continue through the freelancing world. I'm also convinced that the thrill of being a in a new place, meeting new people and learning new things will keep me motivated to continue the journey.

For now, I'm looking forward to Jan 1st. Because on Jan 1st, 2009, I am becoming a freelancer.

