---
title="Themed Days - My Productivity Secret"
slug="themed-days"
excerpt="Build a virtuous loop of progress, which builds so much joy, which, in turn, makes you more productive"
tags=["coach","wins","productivity"]
type="post"
publish_at="06 Oct 21 09:06 IST"
featured_image="https://cdn.olai.in/jjude/themed-days.jpg"
---

Paul Graham wrote a popular essay, **Maker schedule, Manager schedule**. In it, he argues, 

> There are two types of schedule, which I'll call the manager's schedule and the maker's schedule.

The manager's schedule is for bosses. They can cut their day into one-hour intervals and fill them with meetings. 

![](https://cdn.olai.in/jjude/themed-days.jpg)

Creators, such as programmers and writers, work on the maker's schedule. They require uninterrupted time to create. A creator can't make any substantial contributions in an hour. It takes me at least half an hour to get focused.

CEOs of fast-growing product companies like Shopify have realized that they are creators rather than managers. 

Tobi Lütke, CEO of Shopify, has expanded on Paul Graham's idea to have day themes. Half a day of his calendar is dedicated to thinking about the biggest challenges that his company faces. He believes **a calendar is a strategic tool**.

He begins the week with a meeting with his small team to begin the week. The afternoons are reserved for product reviews.

> ... on Wednesdays, Shopify doesn't do scheduled meetings. Usually, I have a list of memos to read or reactions to record to various mock-ups and so on.

I have tried day themes and it is such a great way to get things done as productive as you can get.

My day theme looks something like this:

- Monday - Consume / Create
- Tuesday - Consulting
- Wednesday - Consulting
- Thursday - Admin / Networking / Marketing
- Friday - Consulting
- Saturday - Coaching / Investing
- Sunday - Rest / Refresh

With themed days, I was able to accomplish so far this year:

- 35 weekly Gravitas WINS shows
- 23 Gravitas WINS Radio podcasts, which have taken the podcast to the top 20 in the Apple Podcasts / India / Management chart
- 40 newsletters
- 7 cohorts of Gravitas WINS course, each cohort running for 12 weeks.

Why themed days are so effective?

- If you dedicate a day to a theme, **your thinking isn't fragmented**. The more cognitive power you put into your work, the more productive you will be on both a quantum and quality level.
- Themed days allow you to **prime your mind**. The concept of priming can be explained as a psychological technique in which a previous stimulus influences the response to a subsequent one. Focusing on investing a few Saturdays will prime your mind to take part in investing every Saturday. As soon as you wake up, all you have to do is ask "what day is it?" Your mind will automatically start thinking about themed activities.
- Themed days **provide structure and rhythm**. The cadence lets you focus on important matters every week. In this way, you can build a virtuous loop of progress, which builds so much joy, which, in turn, makes you more productive.

How can you get started with themed days?

- **Identify themes**. Don't overthink it. Try reading, writing, investing, and networking as examples. Adjust until you find your rhythm. 
- To find time, **work early or late**. That's how I got started. I would get up at 5 a.m. and dedicate a block of time to a theme.
- **Start small**. It may not be possible to dedicate an entire day to one theme. That's okay. Start with an hour. Theme them like: Mondays - Reading; Tuesdays - Writing, etc.

The time blocks can be extended once you find the themes and routines that work for you.


### References

- [Maker's Schedule, Manager's Schedule](https://www.paulgraham.com/makersschedule.html)
- [The Observer Effect – Tobi Lütke](https://www.theobservereffect.org/tobi.html)

## Continue Reading

* [Are you sure you are not shaving the yak?](/shave-the-yak/)
* [Future Doesn't Happen To Us, Future Happens Because Of Us](/shape-the-future/)
* [Culture is what you do, not what you believe](/culture-is-doing/)