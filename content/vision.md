---
title="Where there is vision, people prosper"
slug="vision"
excerpt="World is a better place because of definite optimists. Their vision continues to prosper the world."
tags=["wins","coach"]
type="post"
publish_at="22 May 16 09:28 IST"
featured_image="https://cdn.olai.in/jjude/optimism.png"

---

Indians are optimistic in nature. [Studies][1] prove [that][2]. This optimism is on display in conferences, group chats and interaction with entrepreneurs. Everyone is confident that the future will be bright.

But, what that bright future will look like? What skills do we need to reach to that future?

There is no answer. We are all happy running in an optimistic treadmill.

None has a vision. Not for the country, not for their companies, not for their societies, not for their families, and definitely not for their lives.

In the words of Peter Theil, we are all indefinite optimists.

In his book [Zero to One: Notes on Startups, or How to Build the Future][5], Peter Theil, places people and societies into a 2 X 2 matrix.

![Peter Theil's Optimism, Pessimism grid](https://cdn.olai.in/jjude/optimism.png)

[Will Price](https://twitter.com/pricew) sums up this classification in his [LinkedIn post][4]

> The _definite optimist_ has a concrete plan for the future and strongly believes in that future being better than today. The _indefinite optimist_ is bullish on the future but lacks any design and plan for how to make such a future possible. The _definite pessimist_ has a specific vision for the future but believes that future to be bleak. The _indefinite pessimist_ has a bearish view on the future but no idea what to do about it.

A definite optimist says, "**I believe that this nation should commit itself to achieving the goal, before this decade is out, of landing a man on the moon and returning him safely to earth.**"

While an indefinite optimist says, "**There is no reason to be disappointed. India will progress very fast and the skills of our youth will take India ahead.**"

One nation raised itself to meet that clear vision. Another remains an 'emerging super-power.' For ever.

When a country is full of indefinite optimism, it [hurts][7]. Growth stagnates. Promising BRICS become "[Fragile Five][8]".

This isn't just the state of the country. Many entrepreneurs, too, cultivate this mental model.

"**Do a good job and customers will come**," is based on a strategy of hope. Hope doesn't delight customers. Hope doesn't create efficiency. Hope doesn't retain employees.

Bold vision does.

Elon Musk [blogged][3] his master plan in 2006!

> Build sports car
> Use that money to build an affordable car
> Use that money to build an even more affordable car
> While doing above, also provide zero emission electric power generation options.
> Don't tell anyone.

He went about doing it. In a decade he achieved his master plan. That's the power of vision.

I enjoy reading Jeff Bezoz's shareholder letters, because it is succinct, bold, and in them, he explains his long term vision. In his [1997 shareholder's letter][6], he laid out his long-term plan in the section appropriately titled: "It's all about the long term." His plan was, lower margins lead to market leadership, market leadership translates "directly to higher revenue, higher profitability, greater capital velocity, and correspondingly stronger returns on invested capital." His decisions have consistently reflected this focus. Would you say he achieved this?

As a definite optimist, you analyze the marketplace, figure out what you can achive with your strengths, and place bold bets. Then, you work out a strategy to make that bet come true.

It is simple, but not always practiced.

Time to time, someone comes along and shows us what definite optimism can achieve. They change things. They push us forward.

Nehru, Gandhi, Martin Luther King, JRD Tata, Verghese Kurien, E Sreedharan, Narayana Murthy, Steve Jobs, Elon Musk, Jeff Bezoz, and many others.

World is a better place because of definite optimists. Their vision continues to prosper the world.

[1]: http://timesofindia.indiatimes.com/business/india-business/Indian-employees-more-optimistic-than-those-in-Asia-Pacific-Study/articleshow/52114118.cms
[2]: http://www.gallup.com/businessjournal/184277/indians-optimistic-business-challenges-persist.aspx
[3]: https://www.teslamotors.com/blog/secret-tesla-motors-master-plan-just-between-you-and-me
[4]: https://www.linkedin.com/pulse/20140924171143-103827-zero-to-one-peter-thiel-s-view-on-the-importance-of-definite-optimism
[5]: http://amzn.to/1XGzIRQ
[6]: http://media.corporate-ir.net/media_files/irol/97/97664/reports/Shareholderletter97.pdf
[7]: /hurts2bindian/
[8]: http://www.nytimes.com/2014/01/29/business/international/fragile-five-is-the-latest-club-of-emerging-nations-in-turmoil.html?_r=0
