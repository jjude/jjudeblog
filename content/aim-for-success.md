---
title="Aim For Success"
slug="aim-for-success"
excerpt="Efficient problem solvers aim at success rather than perfection"
tags=["problem-solving"]
type="post"
publish_at="31 Jan 11 16:30 IST"
featured_image="https://cdn.olai.in/jjude/aim-for-success-gen.jpg"
---

Successful entrepreneurs excel in identifying problems and solving them with an urgency. They don't agonize to find a perfect solution because they know it does not exist; they proceed with a solution that will provide a reasonable success. Mind you, a solution guaranteeing reasonable success is not an inferior solution. It is the best solution within the constraints of available finance, time and technology. They move from idea to implementation as quickly as they can targeting success instead of perfection.

What does entrepreneurship and problem solving have in common?

Efficient problem solvers too aim at success rather than perfection. They don't get bogged down with analysis paralysis. They hunt for a workable solution within the existing configuration of the system they deal with.

Is there a guideline for problem solvers to achieve success without being intimidated by perfectionists? Considering that troubleshooting a situation is usually undertaken as a project - with a precise outcome and fixed timeline, below points should serve as a guideline:

1.  **Get an executive support** : Every one wants to be associated with a successful project; none with a troubled project especially if they perceive a doomed future. So if you are troubleshooting a problematic situation, the critical step is to ensure executive support. With the executive backing there will be faster decision making, guaranteed assistance from administrative support teams and importantly, stakeholders will oblige to try prototypes solutions.

2.  **Constitute a board of advisors** : In the heat of problem solving, it is possible that the manager and the team members overlook essential aspects of the problem. So it is essential tap the wisdom of the crowd. Learning from the book '[Wisdom of the crowds](https://geni.us/crowdwisdom)' by James Surowiecki, the group should be heterogenous, independent and each member should have an incentive to contribute towards success of the project.

3.  **Establish a lean team** : Troubleshooting business problems and solving them isn't a solo venture. You need a team that backs you up with implementation. It is essential that the team is as small as possible but as large as the situation demands. Large teams generate issues of their own - inter-team communication issues, people issues, co-ordination issues and so on. Within a lean team it is possible to minimize such issues. So go for lean team.

4.  **Embrace champions; ignore distractors** : Every project attracts champions and distractors. It is essential for the troubleshooter to embrace the champions. Champions will be willing to test prototype solutions, be evangelists and also to advice whenever needed. At the same time, criticisms of distractors should be evaluated and accepted, if needed, for success of the project.

5.  **Communicate early; communicate often** : In '[Mythical Man Month](https://geni.us/mythical-manmonth)', a classic on Software Engineering, its author Fredric Brooks observes lack of communication as a major factor for project disasters. It is the duty of the troubleshooter to periodically inform all stakeholders - executives, champions, team members. The content and form of the communication should be varied according to the role of project stakeholder.

6.  **Revisit** : Solving problems isn't a linear operation. While solving problems, a general way is to prioritize the issues needing attention. As you progress in resolving top-priority issues, you should revisit the issues to find out if any of the low-priority, non-urgent issues will become urgent, high-priority issues. It is better to solve them pro-actively than make it another problem to be solved.

7.  **Celebrate (Be generous in giving credit)** : Problems drain energy quickly and constantly. So it is up to the leader to keep charging the team periodically. Sure way to charge the team is to give credit and celebrate. Vague credits ('you did well') don't inspire members; rather be as specific as possible. It is easy to fall into the trap of crediting the seniors (after all they are playing an important role in implementation) or the juniors (after all they need the pat more). But the truth is those in the middle needs it and they should be recognized as well.

Troubleshooting projects is like walking up on a slippery slope. Enthusiastic people and supporting processes help in climbing that slippery slope. Embrace the people and processes and proceed towards success without being distracted by perfectionists. Good luck.

_This post is part of '[Be a Problem Solver](/be-a-problem-solver/)' series._

