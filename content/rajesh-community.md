---
title="Rajesh Madan on 'How to build a thriving Community'"
slug="rajesh-community"
excerpt="Learn from Rajesh who has built successful online and offline communities"
tags=["gwradio","networking"]
type="post"
publish_at="22 Mar 22 06:00 IST"
featured_image="https://cdn.olai.in/jjude/rajesh-community.jpg"
---
We learn and grow when we are part of communities. But growing communities is not easy. I have been part of a community that Rajesh has built. There is fun, learning, and sharing all under the vigilant eyes of Rajesh. In this episode, Rajesh shares his experiences on building communities.

<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless src="https://share.transistor.fm/e/e8a64096"></iframe>

## What you'll learn
- What were the inspirations for Rajesh to build communities?
- What kind of communities you should build?
- What are differences between online and offline communities?
- How to grow a community keeping the rules but also enthusiasm of the members?
- What are the benefits of building communities?

![Community building with Rajesh Madan](https://cdn.olai.in/jjude/rajesh-community.jpg)

## Connect with Rajesh
- LinkedIn: https://www.linkedin.com/in/rajesh-madan-31b4865/
- Twitter: https://twitter.com/rajeshthemonk
- GrayLeap Book Club: https://www.linkedin.com/in/grayleap/
- Jungli CEO: http://jungliceo.com/

## Watch on YouTube

<iframe width="560" height="315" src="https://www.youtube.com/embed/Yiw48quDAgY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Connect with me
- Twitter: https://twitter.com/jjude
- LinkedIn: https://www.linkedin.com/in/jjude/
- Website: https://jjude.com/
- Newsletter: https://jjude.com/subscribe/
- Youtube: https://youtube.com/gravitaswins
- Email: podcast@jjude.com
- Executive Coaching Program: https://gravitaswins.com

## Your feedback counts

Thank you for listening. If you enjoy the podcast, would you please leave a short review on Apple podcast or on YouTube? It takes less than 60 seconds, and it really makes a difference in finding this podcast. And it boosts my spirits. 😀

I would like to hear what resonated the most with you in this episode. Either you can email me or share it on social media and tag me. Thank you for your support.

Have a life of WINS.

## Amazing tools I use:
- I record all the remote interview with [Riverside.fm](https://riverside.fm/?via=joseph-jude)
- I use [Transistor.fm](https://transistor.fm/?via=jjude) for hosting these podcast episodes

_Gravitas WINS Radio is produced by [Joseph Jude](https://jjude.com/). Be sure to subscribe on [Apple Podcast](https://podcasts.apple.com/in/podcast/gravitas-wins-radio/id1562802449), [Spotify](https://open.spotify.com/show/7LOBjAyjBkechMC0xMIj9E),[Google Podcast](https://podcasts.google.com/feed/aHR0cHM6Ly9mZWVkcy50cmFuc2lzdG9yLmZtL2dyYXZpdGFzLXdpbnMtcmFkaW8=),[YouTube](https://www.youtube.com/gravitaswins), or the podcasting app of your choice._

Please check out other episodes of [Gravitas WINS Radio](/gwr-themes/). For easy consumption, they are categorized by themes. If you are a fellow podcaster, you might be interested in [How I Podcast](/how-i-podcast/) or [tools I use](/i-use/).

## Continue Reading

* [Is There DOPE In Your Network](https://jjude.com/network-dope/)
* [Power Of Storytelling In Business](https://jjude.com/biz-storytelling/)
* [What's In A Name? Everything](https://jjude.com/name-is-everything/)