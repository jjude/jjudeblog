---
title="You have to be ready when the opportunity knocks"
slug="ready-for-opportunity"
excerpt="Only the prepared become the lucky ones"
tags=["wins","luck","systems"]
type="post"
publish_at="21 Sep 22 15:56 IST"
featured_image="https://cdn.olai.in/jjude/future-sceanrios.png"
---

A friend of mine told me this story. His son was admitted to a US university. They were dropping him off at the airport. Just like any Indian mother, his wife gave her son advice about dos and don'ts throughout the drive.

- Don't go out and party
- Call home at least once a week
- Study daily
- Go to temple

Eventually, my friend told her, "If you did not teach him these values from an early age, your 20-minute lecture will not help him follow them."

You can't prepare for opportunities on the way to meet them. Because there are no announced opportunities. They come like thieves. No one knows what time or day they will knock on your door.

Only prepared people become the lucky ones. When they see an opportunity, they can seize it with all their might and act aggressively. Because they are prepared.

The unprepared can't even recognize an opportunity. Even if they get a breakthrough by chance, they do not execute with the required rigor because they do not have necessary support systems. The opportunity slips through the cracks.

How can you get ready for the upcoming opportunities?

Prepare.

You might ask, how can I prepare if I don't know what the future holds? Fair point. 

![Future is not a single point](https://cdn.olai.in/jjude/future-sceanrios.png "Future is not a single point")

Don't think of future as a single point. Prepare for different scenarios that can happen. Frame at least three different scenarios that could happen in the next three years.

- What will happen if your life continues as it is, without much change?  
- What trends are happening around you and what can you take advantage of?  
- Would the future be different if you applied 10x effort?

Don't just think about these scenarios. Write them down. Provide as much detail as you can. If you need an example, read my essay, [Future doesn't happen to us, future happens because of us](/shape-the-future/).

It is only the first step. But as the dear Marry Poppins said, "Well begun, is half done"

## Continue Reading

* [First Bullets. Then Cannonballs](/bullets-cannonballs/)
* [Embrace The Unknown](/embrace-the-unknown/)
* [Rise With The Rising Tide](/rise-with-rising-tide/)