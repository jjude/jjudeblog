---
title="Leadership is seen, felt, and heard"
slug="leadership-is-seen"
excerpt="What I have learned from the Ukrainian president"
tags=["wins","coach"]
type="post"
publish_at="10 Mar 22 06:00 IST"
featured_image="https://cdn.olai.in/jjude/zelensky.jpg"
---

The Ukrainian president, Volodymyr Zelensky, has emerged as a hero you read about only in epics. Zelensky, in defiance of the aggressors, galvanized his soldiers and citizens. When the US administration offered him a way out of the country, he refused the offer, saying, "**I need ammunition, not a ride**". Zelensky's ability to galvanize his troops and citizens is unprecedented in recent memory. 

The more I learn about the president, the more I admire him. Zelensky was trained as a lawyer but found his calling in comedy. In a television series called 'Servant of the people,' he played a history teacher who became president after a rant against corrupt politicians went viral. In a remarkable turn of events, the fiction became a fact when he ran in the subsequent presidential election.

As a student of leadership, I've been studying Zelensky for the last few days and am in awe of his leadership. I see three distinct leadership characteristics in Zelensky.

![The Ukrainian president, Volodymyr Zelensky](https://cdn.olai.in/jjude/zelensky.jpg)

### He was seen
He posted videos with senior officials, usually from the streets of his capital, to dispel the rumors that he was fleeing. Just that will make soldiers who are fighting with little ammunition against a powerful army more resilient. They wouldn't want to let down their leader who chose to stay in a city in the city's worst nightmare rather than go to a faraway country in comfort.

### He was heard
As a comedian, he may have learned to make his speeches as punchy as possible. If so, he certainly made good use of his skills. He spoke to his citizens, to Russian citizens, and to world leaders. His words were measured, but they left the most impact.

- I need ammunition, not a ride
- We are all here. We are protecting Ukraine.
- We are fighting for our rights, for our freedom, for our lives. And now we are fighting for our survival.

Words can stir the souls. Zelensky has not only stirred his army and citizens, but he has also set a high standard for world citizens to expect from their leaders.
### He was felt
In his speech to the EU leaders, he was so passionate that the interpreter broke down during the speech and everyone in the room gave him a standing ovation. 

Most countries did not offer any assistance to Ukraine at the beginning of the war. Zelensky's relentless diplomacy changed that. Several countries are now supplying arms and ammunition to the battered Ukrainian army. Everyone wants to stand on the right side of the conflict.

In the fight between Volodymyr and Vladimir, might might win the war. Yet, without a doubt, Zelensky has won hearts. In defeat or death, the Ukrainians will remember a leader who made them laugh, gave them hope, and stood with them in a fight for their freedom. That is the mark of a true leader.

## Continue Reading

* [Great Leaders Are Invisible](https://jjude.com/great-leader/)
* [Culture is what you do, not what you believe](https://jjude.com/culture-is-doing/)
* [Principles trump processes](https://jjude.com/principles-trumps/)