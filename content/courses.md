---
title="Courses"
slug="courses"
excerpt="A digital school to teach full-stack programming"
tags=[""]
type="page"
publish_at="29 Jul 17 18:26 IST"
featured_image="https://cdn.olai.in/jjude/courses-gen.jpg"
---

I'm combining my passion for coding and coaching to create a digital school to teach full-stack programming.

Come back to learn full-stack programming.

