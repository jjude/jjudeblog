---
title="What We Need Is Integrated Thinking. Not Polarized Perspectives"
slug="no-polarization"
excerpt="In our proclivity for quick closure, we simplify and settle on one side of the camp."
tags=["insights"]
type="post"
publish_at="20 Jul 10 16:30 IST"
featured_image="https://cdn.olai.in/jjude/no-polarization-gen.jpg"
---

The tactic effectively practiced by politicians to gain popularity has found its way into management, book selling and blogging and that is to polarize people.

Caste, race and color have lost their impact to polarize management thinking. However gender continues to be a prominent polarizing topic. "Are women better CEOs than men", "Do women negotiate better", are some of the hot topics within the current management writing.

Another trend is emerging in **management polarization**. It is setting one group of people upon another or one department on another. Ex: Sales vs Engineering or Employees vs Customers.

Even in this scientific age, our minds have not evolved to hold supposedly **conflicting ideas**. We run to closure and settlement as quickly as possible (and then rush to write a book about it to sell as quickly as possible). In our proclivity for quick closure, we simplify and settle on one side of the camp.

"The test of a first-rate intelligence", wrote F Scott Fitzgerald in 1936,  "is the ability to hold two opposed ideas in the mind at the same time, and still retain the ability to function." He is relevant in today’s business landscape where we are forced to encounter uncertainty than ever before. Instead of simplified models and polarized perspectives, we need to develop **integrated thinking** - an ability to embrace contradictory ideas and arrive at a context-aware solution.

**How often do you run for a simplified but polarized perspective?**

