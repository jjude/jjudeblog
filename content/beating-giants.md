---
title="You can beat the giants - My address to Karunya Students"
slug="beating-giants"
excerpt="We, graduates of 1990-94, met in our college for a reunion. I addressed the students under the theme 'spirit of excellence.'  This is an edited version of that address."
tags=["talks"]
type="post"
publish_at="16 Jul 19 08:03 IST"
featured_image="https://cdn.olai.in/jjude/karunya.jpg"
---

![Addressing Karunya Students](https://cdn.olai.in/jjude/karunya.jpg)

Twenty-nine years back, I was sitting where you are sitting and told myself a lie. A lie that constrained my potential, a lie that limited what I could achieve.

Before I tell you about that lie, I want to narrate a historical anecdote. It is about the Jews.

Jews were slaves in Egypt. God rescued them miraculously from Egypt. They were standing in front of the river Jordan to enter the land that was promised to them. Their leader, Moses, sent twelve men to find out details of the land they are going to possess.

Ten of them came back with a lie. They said the men in that land are giants. We looked small in front of them. There is no way we can win over them to possess the land.

But two of them, Caleb & Joshua, had a different report. They said that our heavenly father has put in us enough power to win every battle that comes our way. In essence, they said, God has given us the spirit of excellence. We have only to demonstrate it. We can beat those giants and possess that land.

Coming to the lie that I was telling myself. I told myself I studied only in a village school. There are students from city schools. I can't compete with them.

I told myself, I studied in state-board syllabus. There are students who studied in CBSE and ICSE board. They are smarter than me. I can't compete with them.

They were lies that chocked me. Thankfully I got over them while studying in this college.

That same boy who studied in a state-board village school went on to advice top IAS officers of the country.

I am not alone. While driving here, I passed Iruttupallam government school where one of my class-mate studied. After graduating from Karunya, this boy who studied in a government school went to study in one of the top colleges in the US. He then founded a startup and sold it for millions of dollars.

I am sure some of you sitting here are telling yourself similar lies - I can't speak in English; I come here from a village; I am not smart enough to complete Engineering. Don't let those lies strangle you of your destiny. Our heavenly father has put in each of us a spirit of excellence. You only have to raise and demonstrate that spirit of excellence. I hope you do.