---
title="Ignorant Of Many Things"
slug="ignorant-of-many-things"
excerpt="There are three types of ignorance - should know but don't know, better to know but don't know, don't care to know, so don't know."
tags=["opinion","sdl","insights"]
type="post"
publish_at="16 Dec 13 16:30 IST"
featured_image="https://cdn.olai.in/jjude/ignorant.png"

---

Few days back, my younger son, who is 1½ years old, fell off upside-down while playing in a slider. I saw drops of blood and I froze in panic. I didn't know what to do immediately. Should I take him to a doctor or should I wait and see? I had absolutely no idea.

It turned out to be nothing serious. But I was so ashamed that I didn't know what to do.

I would come to know my ignorance once more the same day.

Many on my [twitter][6] timeline, was tweeting about Supreme Court of India overthrowing an earlier verdict by a High Court decriminalizing LGBT. Most questioned why the court didn't challenge an arcane law; some tweeted that it is better that court stays within its jurisdiction; and some others explained how this ruling will have a far-reaching impact in non-LGBT's life too.

Though I'm a citizen of India, I don't know the constitution and in particular this section 377. I didn't know a great deal about LGBTs either. Who are they? Why so much fuss? Do they cause harm to social values? **I don't know. I don't know. I don't know.**

As I thought about these events further, I realized my ignorance is of three types.

![Ignorant of many things](https://cdn.olai.in/jjude/ignorant.png)

There are very many things in this universe that I don't care. They don't have direct relation to me and don't have any direct impact. Like Neptune, Queen Elizabeth, Eskimos, Christmas Island. I know they are there. I read about them once in a while. But they don't concern me. **They are so distant.** I never developed a curiosity to learn about them. LGBTs are one of them.

If there are things that don't matter to me (and I don't matter to them), there are other things that will **improve my life**, if I know them. Like administrative procedures in the government, negotiation skills, healthy lifestyle, a command on English. If I know that passport processing is made online, I won't pay hefty fee to an agent to get a passport; if I develop better negotiation skills, I won't leave money on the table and so on and so forth.

The above two sets don't frustrate me much. What annoy me the most is the ignorance of the last set. They are integral part of my daily life and most of them are important to make a living[^1].

My professional ignorance is limitless. As a technical consultant, I feel, my knowledge of web-apps, application security, and other related aspects are shallow (it may be better than many, but it is still shallow)[^2].

Most damaging are the ignorance in daily activities in personal life.

As a parent of two, it frustrates me that I don't know enough about bringing up kids. Most of the parenting knowledge is through observation, not out of deep thought. When I take time to think, **I have more questions than answers**.

Given the fact that we are at the last era of industrial age, are schools still useful? Should I homeschool kids rather than making them part of a herd? Should they be allowed to play with sharp objects (may be under supervision)? Should I discipline them (with a stick) as I was or should be one of those proud liberal dads?

I don't have answers; I'm afraid, when I have answers, they will be useless.

[6]: http://twitter.com/jjude

[^1]: I'm a programmer who evolved to be a consultant
[^2]: I've tried to bridge them by developing apps on my own
