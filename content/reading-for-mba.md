---
title="Reading for MBA"
slug="reading-for-mba"
excerpt="an alternative way to build managment knowledge without going to school"
tags=[""]
type="post"
publish_at="11 Jul 08 16:30 IST"
featured_image="https://cdn.olai.in/jjude/reading-for-mba-gen.jpg"
---

![](https://cdn.olai.in/jjude/reading-for-mba-gen.jpg)

When I stepped out of the Engineering School, I decided to have a full-stop to schools. I could not force myself into the pain of going through formal study anymore. I was okay to attend few days of training every now and then, but to spend a year or two in school was a big no, no.

Little did I know, when I decided the above, that I will get into IT! In a highly competitive, knowledge centric industry (like IT), it is the knowledge (and the subsequent application of that knowledge) that differentiates a super star from the mediocre.

So I have to build my knowledge.

Still I didn't want to go to business school. I tried distance education. It didn't work either.

That is when I heard of [Personal MBA](http://personalmba.com/), the brain child of Josh Kaufman. Personal MBA is a collection of best of the books under various management fields like Personal Effectiveness, Applied Psychology, Strategy and so on.

Personal MBA is designed for people like me - lazy and don't want to attend schools, yet want to understand all about business management. All that one needs to do is to get hold of these books and start reading. While reading will feed you with enough knowledge, your experience will grow only when you apply them (which is true for any education). I have gotten about 6-7 of the books recommended by Josh and have read about 3 books. I can tell you that the money spent is worth spending.

The factor that appealed to me the most was the fact that I didn't have to go to school. Instead I can read to be a MBA (well not exactly as I won't have a degree).

In these pages, you can follow my progress in reading for MBA and how I employ them in my life and career. Your thoughts will make this an interesting journey. Feel free to drop me a comment.

Did I hear you whisper 'Good luck'? Oh! thanks, I need a good deal of them.

