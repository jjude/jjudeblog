---
title="Thank you for signing up"
slug="thanks-for-subscribing"
excerpt="Now starts your journey to a thriving life and career"
tags=[]
type="page"
publish_at="21 Jul 20 07:33 IST"
featured_image=""
---

Welcome to Gravitas WINS.

As a token of thanks, here is a gift for you: A [weekly WINS worksheet](https://cdn.olai.in/jjude/weekly-planner.png) to define your goals and achieve them in the areas of wealth, insights, network, and self-control. [Download](https://cdn.olai.in/jjude/weekly-planner.png), print it, and use it. Daily.

Using the sheet should be easy. But in case you need a guide, please read, [If you see everything, you'll get nothing](/focus-to-win/).

If you want to know more about me, you can read the [about](https://www.jjude.com/about/) page. I also maintain a [now](https://www.jjude.com/now/) page, which describes my interests nowadays.

To make sure you never miss a Gravitas WINS email, can you please add joseph@jjude.in to your address book?

That is it for now. 
Have a life of WINS, 
Joseph  
[Website](https://www.jjude.com) • [Twitter](https://twitter.com/jjude) • [LinkedIn](https://www.linkedin.com/in/jjude/) • [Spotify](https://open.spotify.com/show/7LOBjAyjBkechMC0xMIj9E)