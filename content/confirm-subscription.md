---
title="Just one more step..."
slug="confirm-subscription"
excerpt="Please look for this email"
tags=[]
type="page"
publish_at="24 Jul 20 06:15 IST"
featured_image=""
---

All you’ll need to do is click the "Subscribe Me" button inside the email.

![Confirm email subscription for gravitas wins newsletter](https://cdn.olai.in/jjude/confirm-email-subscription.jpg)

BUT!

If you don’t see the message in your inbox, **check your promotions or spam folder**.

Excited to have you on board,

Joseph