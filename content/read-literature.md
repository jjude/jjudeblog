---
title="Leaders should read literature"
slug="read-literature"
excerpt="Literature helps us see the world in all its dimensions"
tags=["coach","wins"]
type="post"
publish_at="13 Apr 22 16:55 IST"
featured_image="https://cdn.olai.in/jjude/read-literature-gen.jpg"
---


I hire top students from prestigious universities. I still can't use them on client projects right away. Why?

They have extensive technical knowledge. They can code. They are logical thinkers. However, they lack the required professional skills to work in a team and communicate with clients. Unfortunately, our educational system does not teach them. As a result, we have to train them. Some grasp these skills in a year, many take even a decade.

I can't blame them for taking their time learning these skills. We train them for twenty-five years to sit, listen, and never question the authority of the person in front of the class. Then when they get into jobs, we expect them to collaborate, ask questions, and be proactive. Many people can't make the transition.

We have placed far **too much emphasis on engineering at the expense of art and literature**. Engineering certainly improves our lives, but it also dulls our minds to a black-and-white worldview. 

I grew up reading [Ponniyin Selvan](https://en.wikipedia.org/wiki/Ponniyin_Selvan), [Mahabharata](https://en.wikipedia.org/wiki/Mahabharata), [Ramayana](https://en.wikipedia.org/wiki/Ramayana), [Silappatikaram](https://en.wikipedia.org/wiki/Silappatikaram), and [Kural](https://en.wikipedia.org/wiki/Kural). I had an excellent Tamil teacher by the name Murugan, who taught us to critique the characters in those literary pieces. I have carried that practice with me even after leaving his tutelage.

Literature is more than just epics and books. This category also includes films. Though I enjoy action films such as Die Hard, my heart belongs to social dramas such as [Veedu](https://en.wikipedia.org/wiki/Veedu), [Just Mercy](https://en.wikipedia.org/wiki/Just_Mercy), and [Jai Bhim](https://en.wikipedia.org/wiki/Jai_Bhim_(film)). I've seen [Anbae Sivam](https://en.wikipedia.org/wiki/Anbe_Sivam) at least ten times. 

I've come to appreciate the numerous advantages of literature. Here are five benefits: 

As you immerse yourself in literature, your **empathy grows**. You start to understand what a grandfather and his granddaughter should feel as they struggle to build a house in Veedu. You feel their anguish and disappointment, and you cry as you witness their helplessness.

The epics **sharpen your moral compass**. Distinguishing between good and evil in everyday life is not easy. As you read Mahabharat, you may ask yourself the same question that Arjuna does: What comes first, brotherly love or the destruction of evil? Human psychology has not changed in thousand years, so you can still find guidance in these epics.

In Anbae Sivam, two men with fundamentally opposing viewpoints become best friends. If they can become friends you too can **become friends with those who have opposing views**. All it takes is a trip through a heavy storm and rain.

In both Just Mercy and Jai Bhim, the tenacious lawyers use every tool at their disposal to fight for the wrongfully accused disadvantaged. Both films will inspire you to **fight for the helpless** and to speak up for the oppressed. 

The literature will inevitably **impart you some storytelling** ability. You will master to move the emotions of the audience even after they leave the session. In narrating a story, whether corporate or personal, you will draw from a wealth of sources.

These five advantages, combined with an engineering mindset, are what you need to establish your gravitas. Read literature to become a valuable leader no matter where you are. 

If you're ready to get started, I've already mentioned some of my favorites. Because I learned English as a utility, I don't know the best English literature. If you are aware, please add them in the comments section, so others can enjoy them.

## Continue Reading

* [Have You Changed Your Mind Lately?](/changed-mind/)
* [Leadership Is Seen, Felt, And Heard](/leadership-is-seen/)
* [Culture Is What You Do, Not What You Believe](/culture-is-doing/)
