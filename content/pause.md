---
title="Is there a 'pause' time on your calendar?"
slug="pause"
excerpt="Pausing is good for the heart, business, and life"
tags=["wins","speech","coach"]
type="post"
publish_at="24 Nov 21 06:00 IST"
featured_image="https://cdn.olai.in/jjude/pause.jpg"
---

![Pausing is good for the heart, business, and life](https://cdn.olai.in/jjude/pause.jpg)

_The heart operates in two phases: Systole, where it pumps blood to the vital organs, and diastole, where it relaxes. Most people think that systole is where all the action is and the more time in systole the better. But diastole—the relaxation phase—is where the coronary blood vessels fill and supply life sustaining oxygen to the heart muscle itself. Pausing, it turns out, is what sustains the heart._
_- Dr. Vivek Murthy, Surgeon General in his University of Arizona Commencement Address_

Relaxation provides life-sustaining oxygen to heart muscles. We can benefit greatly from aligning with nature.

Sukhwinder, an entrepreneur I interviewed for the Gravitas WINS Podcast, practices breathing oxygen into his business by pausing. Here's what he said:

> I identified those activities where I’m involved for 80% of the time and the value which is coming up is about 20%. I delegate those activities and disengage to spare more time for myself and the company so that I can do the strategic thinking and planning.

During the second half of every day, he devotes up to three hours to thinking about market conditions, emerging business trends, and internal resource allocation.

Ritka, a guest on the podcast, told the story of her friend who spent eight hours alone on a beach thinking about his business.

Pauses aren't just for entrepreneurs. They're also followed by athletes. Michael Phelps shared this in an interview with Tim Ferriss:

> for me to be able to do everything I need to do, from playing golf, having enough energy with the kids, and doing everything I need to do personally, I have to recover.

When you wish to do everything you wish to do, you need to pause, breathe, and recover before the next cycle of action.

Pausing, after all, is good for the heart, business, and life.

### References

- [Dr. Vivek Murthy's University of Arizona Commencement Address][1]
- [Interview with Sukhwinder][2]
- [Interview with Ritika][3]
- [Michael Phelps interview with Tim Ferriss][4]

[1]: https://time.com/4337454/vivek-murthy-commencement-speech-arizona/
[2]: https://jjude.com/sukhwinders
[3]: https://jjude.com/ritikas
[4]: https://jjude.com/recovery