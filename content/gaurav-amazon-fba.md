---
title="Gaurav Aggarwal on 'How to sell on Amazon US from India'"
slug="gaurav-amazon-fba"
excerpt="Taking advantage of the Internet to explore new businesses"
tags=["gwradio","coach","biz"]
type="post"
publish_at="09 Aug 23 16:39 IST"
featured_image="https://cdn.olai.in/jjude/gaurav-yt.jpg"
---
The internet has opened up many business models that were not available earlier. One such business model is selling on Amazon sitting in India (in fact from anywhere). Gaurav, my guest today, has being doing this business for almost 2 years successfully. Today we are going to find everything about this business model. Hope you find this conversation useful.

<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless src="https://share.transistor.fm/e/1d04e0d3"></iframe>

## What you'll hear

- What is FBA?
- How Gaurav started with FBA
- How to get started with FBA?
- How to get products to Amazon warehouse?
- How to price the product first?
- Talk about Amazon Ads
- Handling customer reviews
- Hardest parts of FBA
- Can Amazon ban you?
- Odds of success in Amazon FBA
- Getting profits back to India
- Handling taxes
- Kindest thing anyone has done for you
- What is leadership quality and who has manifested it in your life?
- Definition of living a good life

## Starter Guide

[Amazon FBA Starter Guide](https://gaurav-amazon-fba.notion.site/Amazon-FBA-StartUp-Guide-High-Level-By-Gaurav-Agrawal-cfdba31f683a41aaaf1966b6c6dcd3ee)

## Your feedback counts

Thank you for listening. If you enjoy the podcast, would you please leave a short review on Apple podcast or on YouTube? It takes less than 60 seconds, and it really makes a difference in finding this podcast. And it boosts my spirits. 😀

I would like to hear what resonated the most with you in this episode. Either you can email me or share it on social media and tag me. Thank you for your support.

Have a life of WINS.

_Gravitas WINS Radio is produced by [Joseph Jude](https://jjude.com/). Be sure to subscribe on [Apple Podcast](https://podcasts.apple.com/in/podcast/gravitas-wins-radio/id1562802449), [Spotify](https://open.spotify.com/show/7LOBjAyjBkechMC0xMIj9E),[Google Podcast](https://podcasts.google.com/feed/aHR0cHM6Ly9mZWVkcy50cmFuc2lzdG9yLmZtL2dyYXZpdGFzLXdpbnMtcmFkaW8=),[YouTube](https://www.youtube.com/gravitaswins), or the podcasting app of your choice._

Please check out other episodes of [Gravitas WINS Radio](/gwr-themes/). For easy consumption, they are categorized by themes. If you are a fellow podcaster, you might be interested in [How I Podcast](/how-i-podcast/) or [Tools I Use](/i-use/).

## Connect with Gaurav Aggarwal
- LinkedIn: https://www.linkedin.com/in/amazon-marketing-hacker/

## Connect with me
- Twitter: https://twitter.com/jjude
- LinkedIn: https://www.linkedin.com/in/jjude/
- Website: https://jjude.com/
- Newsletter: https://jjude.com/subscribe/
- Youtube: https://youtube.com/gravitaswins
- Email: podcast@jjude.com
- Executive Coaching Program: https://gravitaswins.com

## Continue Reading
* [Jinen Dedhia on 'Building enterprise applications with Low code tools'](/jinen-dronahq/)
* [Liji Thomas on ‘Conversational Bots’](/lijit/)
* ['Building An Enterprise Data Strategy' with Ganesan Ramaswamy](/ganesanr/)