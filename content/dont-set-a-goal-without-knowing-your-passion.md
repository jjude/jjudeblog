---
title="Don't Set a Goal Without Knowing Your Passion"
slug="dont-set-a-goal-without-knowing-your-passion"
excerpt="An interview with a whiz kid from Tuticorin on how he launched and grew his online business while still at school."
tags=["interview"]
type="post"
publish_at="23 Jul 10 16:30 IST"
featured_image="https://cdn.olai.in/jjude/dont-set-a-goal-without-knowing-your-passion-gen.jpg"
---

I got to know [Shankar Ganesh](http://about.me/shankarganesh) via [Twitter](http://www.twitter.com/_shankarganesh). Through subsequent twitter interactions I came to know he had established a successful online business utilizing his knowledge of Windows OS. He has written for other popular technology sites too.

![Shankar Ganesh](http://cdn.jjude.con/biz/sg-170x300.jpg)

With all of this background, I assumed that he should be at least a college grad. But he surprised me when he sent me a LinkedIn invite. He was just 17 years old.

I couldn't have been more surprised, impressed and proud (he hails from the same town as [mine](https://en.wikipedia.org/wiki/Thoothukudi)). Since then I've been following him with a curious interest. He continues to amaze me. He has served as a marketing intern at [Zoho](http://www.zoho.com/company.html); he is pursuing his public speaking practice and so on. All this while still at college. It is encouraging to know Indian youths possess such entrepreneurial spirits and are already creating waves.

Its a pleasure to interview Shankar Ganesh for this blog.

### Q) Give us a background of how you came about launching your online business?

Honestly, my [blog](http://www.killertechtips.com/) is far from an online business. At least, till I deliver an actual product. It's an outlet for my creative passion - writing and for teaching others about what I've learned.

My first blog was launched on Blogger.com after I found the blogging buzzword in online forums. Initially, I wrote reviews of web applications but later I found myself writing how to guides, and tech tips based on my own experience. Throughout, it's been a learning process - my writing has constantly improved. The money that comes out of it is good - enough to pay my college fee and manage my expenses by myself. I'm thinking of building new sites and communities based around my interests in the future.

### Q) You are not from one of the popular metros. What challenges you had to face in establishing your business?

Internet connectivity was a challenge, till BSNL (the national telecom network) came out with extensive broadband coverage. It was not that cheap then. Prior to that, I had only dialup as an option which was pathetic nonetheless.

I wanted to blog on my own domain, and getting one was another challenge I faced. I didn't know where the money would come from. Somehow my AdSense pennies added up and finally I invested some of that into getting my own domain and hosting. This may not seem like a location issue, but I think if I had been in a city, I'd had people to meet in person and consult.

### Q) What are some of the memorable milestones?

There have been many happy moments. Getting featured on the Digg front page, being recognized by some big A list bloggers and many more. But there's nothing that matches the happiness that I get when someone genuinely thanks me for what I've written. Sometimes, it's students. They've found my tips on improving English helpful. Sometimes, it's others who've found a solution to a tech related issue on my site.

### Q) How Technology has empowered today's youths of India?

If anyone's got a doubt, they've got Wikipedia to refer today. My Dad didn't have something like that. If I want to connect with my friends, I've got Facebook. My Dad is not in touch with many of his school friends.

Computers are cheaper. Five years ago, let's say, one kid in a class of 40 had a computer. Now, it's at least ten. Technology has become affordable. Five years from now, I'm sure that internet connectivity on mobiles will become even more ubiquitous - leading to a lot of opportunities that could be leveraged. These are probably universal - but I think the mobile explosion in India really happened very quickly.

### Q) Let's talk about your stay at Zoho. How did you get into Zoho? What motivated you to spend time as an intern?

I had gone somewhat bored of blogging (I'm waiting for some good ideas to strike) so I was thinking of a good way to spend my summer vacation. Over the last couple of years, I've learned a lot about online marketing, writing and search engine optimization and I thought Zoho would be the right place to do something with those skills.

Zoho is one of the very few IT based "product companies" from India that I admire and respect. I've heard about their pretty cool work culture. I couldn't think of another IT company in India where I'd fit in.

I approached guys from Zoho on Twitter, showed them my website for some credibility. They liked what I've been doing, and approved me. I went there to see in person how things actually work in a web product company, how they execute things, and to meet and talk to new people.

I can safely say that this wouldn't have happened if not for my web presence.

### Q) What are the lessons learned while at Zoho?

Quite a number of things:

Things take a while in companies. It's not a blog, it's not a small business, so it takes a while to get things approved. But that's okay and it's for good.

You've got people with a wide array of skill sets - if you want a comic strip on your new landing page, there'll be a talented guy out there to help you out. There are people to meet and always learn from. There are people from whom you can seek feedback on what you're working on very quickly. One's skill complements another's.

It's easy to get excited and say "Hey, I want to make a million dollar business!" but the amount of effort and chaos that goes into getting sales is enormous and it requires a lot of effort to create a sustainable business.

### Q) Now about your college life. Are you the tech geek or a guru in the campus?

People do ask me suggestions and questions on tech related stuff once in a while, but it's a University - and there are plenty of others who are equally geeky. I can and I am trying to learn a lot from other tech geeks in campus as well.

### 8) Do any of your class mates have an online business?

As far as I know, none of my friends has an online business but a few seniors in my college do freelance coding or work on their own pet projects. There's this guy called [Shrihari](http://www.twitter.com/shrihari) whose pet project is Kontactr.com. There's another guy by the name Sanjeev Gopinath who's made [BunkBazaar](http://sourceforge.net/projects/bunkbazaar/), a cool mobile app. They're cool people, and I'm happy they're working on stuff they love.

### Q) Do they know about your online ventures and how do they respond?

Yes, some of my friends do know about my blog and they're happy for it. They do ask me about blogging and how to go about starting a blog. I try my best to help them. I tell them not to blog for money, but to write about what they really love and what their passions are. The money will follow.

### Q) Given your background, what would you change in our educational system?

It pains me that kids aren't respected for their talents. We keep saying every kid is unique, but the curriculum treats them as if they're all the same. I think that should change.

The school system currently seems like it's preparing children for jobs. It should instead teach them how to find a living based on their passions. If not help them find a living, at least help them find where they're good at.

A kid who's good in math should be encouraged to excel in it. At the same time, a kid who's good at making funny cartoons should also be encouraged to excel in it. That is also a talent. Today's schools tell kids to set goals when they don't even know what their passion is. Only when they find a calling, they can set a confident goal. I wrote about this recently in my blog.

And most importantly: failing is important. Come out to the real world and you're bound to fail, once or twice or even an umpteen number of times. Schools are teaching that failing is a taboo. Schools should instead tell kids to embrace failure and learn from them, not scare them away. If I had a magic wand, I'd try to make learning organic, customized to individual kids, where emphasis is put on creativity and not earning potential.

My school's former principal told us: "Today, children walk to school and run home; They should instead run to school excitedly and walk home". Schools should be fun.

It could be tough bringing up this change, because it could be logistically challenging, but people are doing it already: [SchoolofOne](http://www.schoolofone.org/) and that's inspiring.

### Q) Do you plan to drop out for a garage startup? If an opportunity comes up would you do it?

I might drop out if I think I'm on an idea of a lifetime. Getting my family's support is going to be toughest part. Also, keeping
naysayers away.

### Q) What do you want to do after college? Would you join infosys or a startup or start one yourself?

That's three years from now so I can't be sure. Everyday is a new day and every day I learn something new. It's better to follow my purpose and collide with destiny later, as Bertice Berry says. I want to be an entrepreneur or work online full time. I might take up a job with a company - but whatever I do, I want to wake up in the morning excited about what I'll be doing on that day.

I wish Shankar Ganesh all the very best to wake up excited every morning. Only such kids can take India to the very top.

