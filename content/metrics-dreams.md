---
title="Your answer is not under a streetlight"
slug="metrics-dreams"
excerpt="Once you understand the Streetlight Effect, you'll see it everywhere. Why, you might even suffer from it."
tags=["coach","startup","systems","bias"]
type="post"
publish_at="05 May 21 06:45 IST"
featured_image="https://cdn.olai.in/jjude/cowin-sms.jpg"
---

### Lost Keys

A policeman sees an old man frantically searching for something. Taking pity on him, the policeman decides to help him. He approaches the old man and asks:

"Sir, What are you searching?"

"I lost my car keys"

"Where did you lose them?"

"I lost them in the parking lot."

"If you lost them in the parking, why are you searching here in the bus stand?"

"Because, sir, the light is only here, not in the parking lot."

😳

The story highlights a bias, commonly known as the **Streetlight Effect**. Once you know it, you'll see it everywhere - from India's top bureaucrats to corporate board rooms.

### Measuring SMSes, not Vaccines

![Measuring SMSes, not Vaccines](https://cdn.olai.in/jjude/cowin-sms.jpg)

India is going through its worst healthcare crisis since independence. [Thousands][1] are dying because hospitals are overflowing with COVID patients and there is no oxygen to administer to them.

You would assume the government bureaucrats would measure the distribution of vaccines or the total vaccinated population.

No. They are measuring the [number of SMSes sent](https://twitter.com/rssharma3/status/1387476056563601410).

Because the parking lot is dark (vaccines are not available), and there is light in the bus stand (it is easy to count the SMSes).

### Metrics

In the corporate world, this bias usually shows up in metrics.

It is easy to measure hours worked rather than the impact. So every company measures hours an employee sat on a chair rather than how effective those hours were.

Similarly, every marketing manager knows the most visited page, not the most valuable page.

### Truth Hides

If you are after the right solution, you should be ready to look amidst rocks, stones, and fleeing sound bites. It requires an open mind, patience, and willingness to call out bullshit.

If you can, then you'll find your keys, your solution, and your truth.

### Quote To Ponder

_Metrics can tell you what users like and dislike, but they can't tell you what they dream about - Scott Gursky._

[1]: https://ourworldindata.org/explorers/coronavirus-data-explorer?zoomToSelection=true&time=2020-03-01..latest&pickerSort=desc&pickerMetric=new_deaths_per_million&Metric=Confirmed+deaths&Interval=7-day+rolling+average&Relative+to+Population=false&Align+outbreaks=false&country=~IND


## Continue Reading

* [Beware of Cloning Best Practices](/beware-of-cloning-best-practices)
* [Surgery Is Successful, Patient Is Dead](/system-vs-goal/)
* [Map is not the territory](/map-is-not-territory/)