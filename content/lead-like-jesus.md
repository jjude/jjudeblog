---
title="What I learned about leadership from the book 'Lead like Jesus'"
slug="lead-like-jesus"
excerpt="Leadership is holistic. Leaders should lead with humility and confidence."
tags=["sdl","books","consume","coach"]
type="post"
publish_at="25 Jun 18 10:30 IST"
featured_image="https://cdn.olai.in/jjude/lead-like-jesus.png"
bsky_id="3lbvilysbpo2y"
---
![Lead Like Jesus](https://cdn.olai.in/jjude/lead-like-jesus.png "Lead Like Jesus")

Ken Blanchard is the author of a popular management book "One minute manager". For some reason, I wasn't attracted to that book. Early last year (2017), I was searching for books to read in the year. Another book by Ken came up again and again in the search. The book was "**Lead like Jesus**".

As a Christian, I became interested in the book. I immediately bought the [book](https://geni.us/lead-like-jesus) and read it. It was so insightful that **I read the book many times in the year**. I took copious notes and returned to the book throughout the year to compare notes.

Ken mentions **three key leadership concepts** in the book:

- Leadership is **holistic** involving heart, head, hands, and habits of the leader.
- Leaders **swing between fear and pride**. Rather, leaders should work with humility and confidence
- There are **four stages of leadership**:
  - Novice – just starting out
  - Apprentice – in training
  - Journeyman – capable of working alone
  - Master – highly skilled and able to teach others

Out of these three concepts, the first two were interesting ones to me.

In general, stages don't interest me. The word "stage" indicates that we are either in one or another group. However, in practice, we are always in many groups depending on the skills we pursue.

Let me explain.

In areas like time and task management, I am somewhere in between 3rd and 4th stage. I can work independently and can also teach others. In people management, I'm in "apprentice" stage. In financial management, I'm a novice. So, as a leader, am I a novice or an apprentice or a journeyman or a master? In fact, I don't care, as long as I'm progressing.

First **two concepts had a profound impact** on me. I'm sharing my experience using those two concepts.

In the holistic leadership approach, Ken talks about four domains:

- Heart: **motivation for leadership** — are you a self-serving leader or a servant leader;
- Head: **point of view** — your beliefs and theories about leading and motivating people;
- Hands: **actions** — setting clear goals and observing performance, followed by praising progress and redirecting inappropriate behavior
- Habits: **daily renewal activities** — solitude, prayer, reading scriptures, and involvement in a safe community

I tried to improve in each area last year. I would read about each area, pick up one or two ideas to implement, evaluate the progress, and come back to the book and my notes to read and adjust. I repeated this cycle every month.

As an example, **I took the ideas mentioned by Ken and improved the organization where I'm the CTO**.

Every quarter, I involved my team-members to frame objectives for that quarter. Then, I worked with them to implement those objectives. Whenever needed, I also coached them with role-plays. At the end of the quarter, we had "retro" sessions to evaluate the progress to give two-way feedback. With this cycle, we improved handling of sales leads and introduced two new service-offerings. Not bad, huh?

The second concept had an equally deep impact on me.

Ken writes that **leaders swing between fear and pride**. When I read it, I was cynical. I thought it wouldn’t apply to me. Before dismissing the idea, I started noticing my thoughts and document them. This self-reflection made me realize Ken was right. I was indeed swinging between these two negative emotions.

Once I realized I was swinging between emotions, I went back to the book about the alternative. Ken says leaders should lead with humility and confidence.

He defines humility as: "It is not putting yourself down. It is lifting others up."

Best description of confidence comes from Alan Weiss, another favorite author of mine: "Confidence: Belief you can help. Arrogance: Belief you know it all."

When you combine these two, you get a meaningful definition of leadership: **Leadership is lifting others up.**

Why humility and confidence are needed to lift others up? How fear and pride hinders it?

If you are fearful, you think others will take up your job or role. If you are confident, you view others as a compliment and not as competitive. You help them so you can grow along with them.

At first, pride seems to be the required emotion to lift others. Only when you feel you are at a higher place, you can help. If you feel you are at a higher place, isn’t pride a rightful emotion?

It is true that you need to feel you have achieved something to help others. There are so many who say I have nothing to give because I have nothing. But Ken says in the book, **humility isn’t thinking less of ourselves**.

Humility is empathizing with others. It is going down to their level to understand their challenges so you can help them where necessary.

Last year I trained about 10 people in my company on effective communication. Part of the training was presenting in front of a crowd. Each of them had a different challenge. Some believed they can’t go in front and speak. Not even a word. Some had no problem going in front and talking as long as it was in their native language. Some others had no problem speaking in English, but they couldn’t speak cohesively. I worked with each of them as per their need. I was happy to see everyone improved by the end of the training session.

Even though I was the boss, in the end, **I learned a lot via concepts mentioned in this book**.

If you don’t mind the religious bent in the book, [pick up the book](https://geni.us/lead-like-jesus) to read. You will definitely learn a lot about leadership.

_Disclaimer: Link to Amazon book is an affiliate link. That means, if you purchase through that link, I get a commission._

_All icons in the image are from [The Noun Projects](https://thenounproject.com/)_

## Continue Reading

- [Where there is vision, people prosper](/vision/)
- [Documenting Your Decisions](/documenting-your-decisions/)
- [People Aren't Potatoes](/no-potatoe/)
- [All Books I have read](/books-read/)