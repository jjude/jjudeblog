---
title="Switching Laptops (or PCs)"
slug="switching-laptops-or-pcs"
excerpt="Steps to follow when you switch laptops (Windows based)"
tags=["tech"]
type="post"
publish_at="18 Aug 07 16:30 IST"
featured_image="https://cdn.olai.in/jjude/switching-laptops-or-pcs-gen.jpg"
---

Steps to follow when you switch laptops (Windows based)

[Preliminaries](#Preliminaries)
[Configuring ‘My Documents’ folder](#mydocs)
[Configuring ‘Microsoft Outlook’](#outlook)
[Restoring IE Favorites](#ie)
[Restoring FireFox Profiles](#firefox)
[And the others](#rest)

<a name="Preliminaries" title="Preliminaries"></a>

##### Preliminaries

I learned to store all documents and applications (that I install) under a single folder so that it is easier for migration. So when the time comes for migration, all I have to do is to copy this single folder with all its contents to the new laptop. In some cases, like returning from on-site or changing clients or jobs, you can copy this folder into a CD/DVD. Otherwise you can share this folder over the network and transfer files into the new laptop.

<a name="mydocs" title="mydocs"></a>

##### Configuring ‘My Documents’ folder

Obviously, the first thing to do, is to create a folder and map that as the new ‘My Documents’ folder.

To change the location of the default ‘My Documents’, right click on it. Enter the new location and click ‘Apply’. As this is a new setup, there is no need to move the old folder.

![My docs](https://cdn.olai.in/jjude/my_docs.png "My docs")

This sets you on the go. Now copy your existing folders and files to this new ‘My Documents’.

<a name="outlook" title="outlook"></a>

##### Configuring MS-Outlook

This is a time consuming one, but not a difficult one.

Click on Mail under ‘Control Panel’. This will pop-up a window like the below one:

![Outlook Mail](https://cdn.olai.in/jjude/mail.png)

You need to create multiple profiles if needed. I use the default profile.

Go ahead and add all of your .pst files to Outlook by clicking on ‘Data Files’.

Then setup the mail accounts. Either you know the parameters to set the mail accounts (personal mail accounts like gmail) or your net admin should help you (office mail account).

If it is an exchange server account and it supports access over internet, then you can configure outlook to fetch mail over internet, not just via office LAN. Under 'Email Accounts', double click on the Exchange server mail account. Then click on ‘More Settings’.

Under ‘Connection’ tab, enable ‘Connect to my Exchange mailbox using HTTP’.

![HTTP Access](https://cdn.olai.in/jjude/outlook_http_access.png)

Click on the ‘Exchange Proxy Settings’. Provide the HTTPS URL next to https://.

![HTTP URL](https://cdn.olai.in/jjude/outlook_http_url.png)

Most of the firms limit the storage space on the Exchange server. If you are in such a situation, it is better to have mails delivered to a local file. With the last step, you’ve added all the .pst files to ‘Outlook’ data files. Select one of this file as a delivery location.

![Default Delivery](https://cdn.olai.in/jjude/outlook_email_accounts.png)

Next let us set up Contacts. Contacts stored in one of your .pst file have to be available as an Address Book. In Outlook, go to Contacts. Right click on the ‘Contact’ and go to ‘Outlook Address Book’ tab. Enable ‘Show this folder as an email Address Book’. If you have stored contacts in more than one folder, repeat this for each of those folders.

![Outlook Contacts](https://cdn.olai.in/jjude/outlook_address_book.png)

You are done with most of it. While you are still configuring Outlook, don’t forget to modify fonts, signature and tool bar as per your liking.

<a name="ie" title="ie"></a>

##### Restoring IE Favorites

By storing IE favorites under ‘My Documents’, it becomes easy to take it along. True, you can export and then import it. But it is possible that you forget to do it when you hand over the old laptop.

This fundamentally involves changes in registry. If you don’t have rights to change the registry then Export/Import is the only alternative.

In Windows Registry, navigate to the below path. HKEY_CURRENT_USERSoftwareMicrosoftWindowsCurrentVersionExplorerUser Shell Folders. On the right side point the ‘Favorites’ to the appropriate folder.

![IE Favorites](https://cdn.olai.in/jjude/ie_fav_reg.png)

<a name="firefox" title="firefox"></a>**Restoring FireFox Profiles**
FireFox stores its bookmarks, themes and add-ons in a profile folder. Again, storing this profile folder also under ‘My Documents’ is a good idea for the same reason as above.

As noted in [Mozilla Support](http://www.mozilla.org/support/firefox/profile), this involves invoking FireFox ProfileManager, creating a new profile folder and copying the content of the older Profile Folder into the new one.

<a name="rest" title="rest"></a>

##### And the rest

This is most of it. Final points in the migration are:

* Setup Printer
* Modify Windows Explorer Setup
* Reinstall needed applications
* Modify Environment variables like Path
  Finally don’t forget to delete these files from the old laptop. Delete hidden files too.

That is it. Enjoy your new toy!

