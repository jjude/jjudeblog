---
title="If you see everything, you will get nothing"
slug="focus-to-win"
excerpt="When you want to achieve something, focus on what you have and what you target 🎯. Everything else is noise."
tags=["wins","productivity","systems"]
type= "post"
publish_at= "29 Jul 20 10:30 IST"
featured_image="https://cdn.olai.in/jjude/wins-plan.png"
---
Every day, we carry burdens of family, job, and community. We manage the load by breaking them into pieces and put them into a to-do list. The agony of life is that the more tasks we tick off the list, the more added. Running faster seems to be the only way out, so we run faster aided by tools, only to discover at the end of a period that we have been running on a treadmill with ever-increasing speed, going nowhere.

I have run on the treadmill of tasks with panting of disappointment for more weeks than I can count. For reasons I can't explain, enlightenment in life strikes only at the trough of despair. I was reminded of a story in the great Indian epic, [Mahabharata](https://en.wikipedia.org/wiki/Mahabharata), during one of the downcast days. The story goes like this. 

_I'm paraphrasing the story here, so if you know the story, allow me some latitude._

> One day, Drona, the teacher, lines up his students to teach archery. He commands them to focus on a bird on a near-by tree. Every student raised their bow and was ready to shoot.    
> Before they shoot, he walks over to each student and asks them what they see.   
> "I see the tree, the branch, and the bird" answers the first student. Drona asks him to put the bow down and stand aside. He moves to the next student.    
> "Tree, branch, and the bird …" The second student repeats the same answer. And the next, and the next. Finally, Drona approaches Arjuna. "What do you see, my favorite boy?"   
> "The tip of the arrow and the eye of the bird," Arjuna replied.     
> "Do you see the tree and the leaves?" Asked Drona.    
> "No, just the tip of the arrow and the eye of the bird."    
> "Shoot," commands Drona.    
> Arjuna releases the arrow and hits the target.    

> Moral of the story: When you want to achieve something, your attention should be on what you have and what you target. Everything else is noise, and you should shut them out.

I'm no archer, but I can transfer the fundamentals to any domain of interest. I started building a system that embodies the principles of the above story. I have used and fine-tuned the system for long, so I feel sure to share it with you. Here are the fundamental principles behind this system:

- Know the outcomes that you want to achieve
- See the outcomes often
- Create projects to reach the outcomes
- Divide each project into weekly wins
- Win a day
- Review to learn, pat, and adjust
- Iterate this cycle to win the outcomes

### Know the outcomes

> It's hard to do a really good job on anything you don't think about in the shower - Paul Graham, [The top idea in your mind](http://www.paulgraham.com/top.html)

![Planning Template](https://cdn.olai.in/jjude/wins-plan.png "Planning Template")

We don't stop thinking about our personal life when we enter the office, and we don't stop thinking of our job when we play with our kids. Thus it is futile to try for a balance; rather, we should integrate all areas of our lives to have a fulfilling life. 

After many iterations, I have found **four areas that encompass all of life**, and if I do well in those areas, I would have a fulfilling life. They are **w**ealth, **i**nsights, **n**etwork, and **s**elf-control.

For each area, I think of one or two outcomes I want to achieve in three years. The outcomes I want to accomplish in each area in the coming three years looks like this: 

**Wealth**

- Launch Gravitas WINS course

**Insights**

- Learn to run a digital business (Gravitas WINS course)

**Network**

- Five mentors & Five Thousand Students

**Self-control**

- Stay fit and healthy

All the projects are related, hence it is easier to remember and think about them cohesively. Because they are cohesive, ideas pop into my head during showers or when I take a walk.

Once I know the outcomes, I divide them further into quarterly projects and weekly milestones. At any quarter, I run only one or two projects. In this quarter, JAS 2020, I am focusing only on two projects:

- Completing the material for the Gravitas WINS course
- Double the subscribers for this newsletter

### Win the day

> Slow is smooth, smooth is fast - Navy SEALs

![Daily WINS worksheet](https://cdn.olai.in/jjude/daily-wins.png)

WINS sheet is not a glorified to-do list; it is not even a to-do list by another name. It is a WINS sheet. I enter only those activities that will move me towards the outcome.

There are always other tasks that I need to do. I do one of the three with those tasks: 

- avoid them
- delegate them
- do a good enough job to move on

### Reflect daily & Review weekly

Every day, I reflect on the day that went by. 

- Did something catch my attention?
- Did I do something beyond expectation? 
- Was I distracted? 
- Did I spend too much on movies or YouTube?


![Weekly Reflection sheet](https://cdn.olai.in/jjude/weekly-reflection.png)

Every Sunday night, I sit with the sheet and review the previous week.

There is always a surprise every week. New things pop up; I completed more than I planned; but most weeks, I completed only 80% of what I intended.

Since the outcome is clear, I am not disappointed even if I have not completed everything I planned. Eighty percent progress every week towards the outcome is a great move forward.

When I review every week, I get 54 chances to review and amend my ways.

### Adjust as you go along

After the Sunday review, I take a print-out for the next week. I write down the three-year outcome and the quarterly projects again. Repeatedly writing the expected results gives me a moment to consider the outcomes and projects in light of what I know. 

* Do these outcomes still make sense?
* Are these projects the best way to achieve the outcomes?
* Is there a way to achieve the outcome differently?
* Can I partner with someone to reach the milestone in half the time?
* Can I use any tool to amplify my efforts?

I go through these questions, and I adjust the outcomes, projects, and weekly wins. Usually, I don't make any changes to outcomes and projects. But there are always changes to weekly wins.

### Drown out the noise 

Going through such routine cement the outcomes in my mind. My unconscious mind reflects these outcomes all the time - when I sleep, work out, or take a shower. My mind doesn't think of anything else. So there is clarity when I sit down to do the work.

That is how I drown out the noise and win the day.

### WINS productivity system is yours to have

> Move forward three things a mile, not hundred things an inch - Alan Weiss

This system has aided me to publish a [weekly newsletter](/subscribe/), write for [reputed magazines and newspapers](/published-articles/), [speak](/talks/) in conferences, amidst consulting as a [CTO](/cto/) and [being a dad](/tags/parenting/) to two amazing boys. 

If this interests you, subscribe using the below form, and you will get the Weekly WINS sheet for free.

## Continue Reading

* [Three Types Of Goals You Should Set](/three-goals/)
* [Themed Days - My Productivity Secret](/themed-days/)
* [52 Checkpoints . 6 Questions . Fulfilled Life](/weekly-review/)