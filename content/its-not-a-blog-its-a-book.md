---
title="It's Not A Blog, It's A Book"
slug="its-not-a-blog-its-a-book"
excerpt="How Guy Kawasaki's advice changed my blogging practice."
tags=["blogging","insights"]
type="post"
publish_at="03 Jan 11 16:30 IST"
featured_image="https://cdn.olai.in/jjude/its-not-a-blog-its-a-book-gen.jpg"
---


[Consume-Produce-Engage](/how-to-be-a-champion-and-remain-one/) is the model that I follow to build my knowledge. I 'consume' Books, Blogs and Journals and 'Produce & Engage’ through blogging.

Like many bloggers, my interest is multi-fold. Even in the professional side, I'm interested in project management, business analysis, problem solving, e-Governance, process re-engineering, people management, product development, leadership and so on. I 'consume' in all of these domains. Naturally I wanted to 'produce' in all of these domains too. That was not a problem for me since I consume in all of these spaces.

Before I started this blog, I read through many of the top bloggers like [Chris Brogan](http://www.chrisbrogan.com/about/), [Men with Pen](http://menwithpens.ca/) and [Copyblogger](http://www.copyblogger.com/). They recommended to stay with a single theme so that it doesn't confuse the subscribers and readers. But then I will have to create many different blogs! I didn't like the thought of managing many blogs and moderating comments in those blogs. So I blogged on consulting, project management, emerging trends in India and so on - all within this blog itself.

I don't know if readers had a problem with blog entries from those range of topics. But I did.

I had assumed that with variety of subjects to choose from, I will never run out of topics for blog entry.

I was wrong. I was wrong because I wrote whatever hit my thought-stream and I didn't have a flow. My mind kept wandering and I had many unfinished posts in draft mode. There was no coherence. I was stuck.

Since I was stuck, I took a break. I even considered creating multiple blogs. But that thought didn't enthuse me. So I kept delaying getting back to blogging.

That is when I happened to read [Guy Kawasaki](http://www.guykawasaki.com/)'s [The 120 Day Wonder: How to Evangelize a Blog](http://www.evancarmichael.com/Entrepreneur-Advice/352/The-120-Day-Wonder-How-to-Evangelize-a-Blog.html). He advised bloggers to think of blogging as writing a book. I got hooked. I told myself, that is what I should be doing. I don't have to create multiple blogs. Just think of blogging as writing book. So stay with the theme of the book as long as it takes to complete it. Once completed, move on to the next 'book'. If readers like the theme, they will subscribe; if they don't like the running theme, they will unsubscribe. What a liberating thought! Guy is truly genius. No wonder he is what he is.

Then I debated on the theme for the first 'book'. I am equally passionate about all the subjects. So choosing one was a difficult choice. When I thought deeply (and thought about what has been a common thread all through my carrier), 'problem solving' emerged as the theme.

So the title of the first 'book' is '[Be A Problem Solver][1]'.

Problem solving is a critical skill in any domain. I plan to write at least one article a week on this theme. I was excited as I created a list of possible topics. Now I got a flow.

Wish me luck.

Thank you for reading, commenting and being part of my 'book' writing.

[1]: /be-a-problem-solver/

