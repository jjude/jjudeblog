---
title="Business and personal branding during crisis"
slug="branding-in-crisis"
excerpt="Never waste a crisis."
tags=["coach"]
type= "post"
publish_at= "11 Apr 20 08:30 IST"
featured_image="https://cdn.olai.in/jjude/marketing-during-crisis.png"
---

Ambi is a consultant on branding strategies. I attended a webinar by him on branding during a crisis like Covid. Here are my notes:

### Covid will change behaviors

The last ten years have been great for millennials. Every two years, they changed jobs, getting a 10% hike just for doing their job. 

They have become asset-less and experience-led. They thought saving is unnecessary, cooking is a waste of time, and going on holiday is the way of life. This crisis will force them to change their mindset and behaviors.

### Should a brand advertise during the crisis?

![Affinity based marketing during crisis](https://cdn.olai.in/jjude/marketing-during-crisis.png)

There are three types of companies.

1. Some companies can help consumers during this covid crisis. They might be in the areas of hygiene, healthcare, food, and so on. They can advertise. But they should spread helpful advice rather than talking about their product. As an example, Vicks might spread a message for steam inhalation, or Yakul might talk about boosting the immune system.
2. Other companies have a tenuous connection to the crisis. Mutual funds, online education startups fall in this category. They can also advertise.
3. All others should shut up and save their advertising budget for Diwali. Advertising anti-covid mattress is just stupid.

### Allocating marketing budget

An attendee asked how they should allocate their marketing budget (not during a crisis but in regular times).

![Marketing budget allocation](https://cdn.olai.in/jjude/marketing-budget-allocation.png)

Follow the 70-20-10 model.

* Spend 70% budget on what already works for you
* Spend 20% on new avenues of marketing
* Reserve 10% on outlandish avenues

### How to build a personal brand

Building a strong personal brand is essential to thriving within your company as well as outside in the community.

Throwing random posts on LinkedIn and Facebook doesn't build a brand. You need to identify your strengths to build a brand. Once you know your capabilities, you can create a brand around it.

### How to utilize this crisis personally

Never waste this crisis. Plan to come out stronger.

The dangerous thing is to binge watch for 8 hours or sleeping after lunch for 4 hrs. Don't watch the news all the time. None of these are useful to you.

Instead, learn in this downtime. Talk to old friends and mentors. Come out of this crisis not as a tired person but with renewed contacts and learning something new.

Follow Ambi on [Twitter](https://twitter.com/ambimgp/) or on [LinkedIn](https://www.linkedin.com/in/aparameswaran/)

## Continue Reading

* [11 Lives to Build a Rich Career](/11-lives/)
* [Future of jobs is Hollywood model](/future-of-jobs/)
* [Life is series of lost, found, and lucky breaks](/life-is-lost-and-found/)