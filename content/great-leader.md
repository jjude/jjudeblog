---
title="Great Leaders Are Invisible"
slug="great-leader"
excerpt="What makes a leader great and two example of such quality"
tags=["coach","wins","gwradio"]
type="post"
publish_at="22 Sep 21 09:43 IST"
featured_image="https://cdn.olai.in/jjude/great-leader-gen.jpg"
---

In my interview with Liji Thomas, the topic of leadership came up. I asked her, "what makes a leader great?"

<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless src="https://share.transistor.fm/e/46957ca2"></iframe>
_Listen to our conversation - Conversational Bots: Yesterday, Today, and Tomorrow_

"Great leaders are invisible," answered Liji. She explained further, "Their impact is seen through the team and execution. But the leader is not there."

A leader is not defined by the seat, but by their mindset. **Great leaders don't seek attention and admiration**. They take delight in molding members and facilitating the team's success.

I remembered two such leaders.

Dr. Kalam, our ex-president, loved to tell a story about his boss **Satish Dhawan**. In 1979, Kalam was the mission director of the launch of a satellite by the Indian Space Research Organization. When the computer data showed an issue in the control system, he overruled and ordered to go ahead. 

Instead of going into space, the satellite plunged into the Bay of Bengal. As the leader of that mission, Kalam felt humiliated. But his boss Dhawan took him to the press conference and said, "I believe in my team. They will succeed next year."

The following year, Kalam and his team successfully launched the satellite. Dhawan asked Kalam to address the press conference. In recalling this incident, Kalam said, "When the failure occurred, the leader owned it. When the success came, he gave the credit to his team."

Another leader who displayed this characteristic is **M.S. Dhoni**. 

I am not a cricket fan but have always admired Dhoni for his leadership skills, especially in the IPL. He exacted high standards from his players and demanded even more from himself. He didn't mind getting into risky situations and play the big game. But when the time came for celebrations, he disappeared. When he was asked about it, he [replied][2]:

> Well, don't you think, it is unfair that you play a team sport where the captain goes and receives the trophy? It's like an over-exposure.

He could've easily thought, hey the team won because of my strategy, which is true in many cases. Yet, he is not a narcissist.

The leaders who are invisible are not just followed, they are revered, admired, and talked about long after they are gone.


## Continue Reading

* [Lessons I learned from IPL](/lessons-i-learnt-from-ipl/)
* [Future Of Jobs Is Hollywood Model](/future-of-jobs/)
* [How to develop your gravitas?](/how-gravitas/)
