---
title="How to amortize your efforts in producing content?"
slug="amortize-content"
excerpt="If you say, 'I know I have to create content but I don't have time for it', this post is for you.'"
tags=["wins","produce","coach"]
type="post"
publish_at="11 Dec 21 12:03 IST"
featured_image="https://cdn.olai.in/jjude/trends2021.png"
---

When I interviewed [Emmy Sobieski](/emmys) for Gravitas WINS podcast, she said:

> Amortize your efforts among the areas where there are 85% overlap

I have been thinking about this quote often, especially with respect to creating content. Why?

As I mentioned in the [I have seen the future of jobs](/future-of-jobs/), as we move more and more into digital workplaces, three factors will help us stand out from the crowd:

-   Content Creation
-   Network
-   Social Proof

The content we create will aid us building a network, which in turn will generate social proof.

Even though people get the idea that they have to create content, most often people will tell me, "but, I don't have time to create content." If you are one of those who wonder where in the world to get that time to create content, hopefully here you'll find the answer.

Truth to be told, all of us already create content, especially when working from home. Let me share how I've amortized the effort in creating content.

### As a developer

Tracking down errors and searching through StackOverflow is a daily affair. After we fix it, we forget the error and solution. Amortize the effort. Publish the error & solution as a blog post (or a LinkedIn article).

Here is an example (you can [read the entire article](https://www.prudentdevs.club/flask-errors) on my technology blog)


### As a CTO

I study market trends to prepare ourselves to meet upcoming needs. I took time to post my process here:

- [How to find trends in your industry](/trends-in-industry)
- [Trends As A Guide To The Future](/trends21)

In 2017, I studied Mary Meeker's Internet report in detail to plan our annual focus. I posted that analysis [on my blog](/2017-trends).

![Architecture for small IT companies](https://cdn.olai.in/jjude/2017-trends-architecture.png "Architecture for small IT companies")

As an advisor to startups and also as an investor I ask set of questions to evaluate a startup. The questions are based on a framework. I posted them here:

- [P.A.S.T.O.R. Framework](/pastor/)
- [Checklist For Evaluating Startups](/startup-checklist/)

![PASTOR Framework](https://cdn.olai.in/jjude/pastor.jpg "P.A.S.T.O.R. Framework")

### **As a content marketer**

Whatever post I write on my blog, it find its way to LinkedIn as a post, article, or even as a newsletter. As a matter of fact, this article was first posted as a [twitter thread](https://twitter.com/jjude/status/1469103246497226755) (if you are on Twitter, please follow me there).

If you have spent time and effort in creating a content, amortize that effort in creating other forms of the same content.

Post -> Twitter thread -> Podcast -> Video -> Slides

### Amortize your effort

-   If you are a developer, post what you are learning or fixing
-   If you are an executive, post your thought process on team management, industry trends
-   If you are a marketer, re-purpose your content onto different platforms

### I want your questions

Thanks for reading. Do you have any questions on the article? Post them on [twitter](https://twitter.com/jjude). I'll answer them. In fact that may be your first effort in creating content. 😉

## Continue Reading

* ['I'd far rather be lucky than smart' with Emmy Sobieski](/emmys/)
* [Lessons Learned Running A Newsletter](/newsletter-lessons21/)
* [I coded for a year. Here are ten lessons](/one-year-coding/)