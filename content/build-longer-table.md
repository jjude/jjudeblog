---
title="If you are fortunate, build a longer table"
slug="build-longer-table"
excerpt="Start here and now"
tags=["wealth","wins"]
type="post"
publish_at="01 Feb 23 17:43 IST"
featured_image=""
---
You don't have to be a millionaire to be a philanthropist. You can start here and now.  
  
I have been donating even when I was earning ₹1500. Every time I earned more, I donated more. When COVID was raging through the country, I increased my total donations to 15% of my revenue.  
  
My son too picked up this habit. Even when he was 10, he donated part of his savings to Kerala CM fund when there was a devastating flood there.  
  
Let me recommend two services for your donations. I have known them personally and they are doing just an amazing job in their respective areas. (I'm not a spokesperson or they have not asked me to request for donation. I'm using my platform to spread awareness of their work).  
  
First is [Manjula Sularia](https://www.linkedin.com/in/ACoAAAUt08EB0Lqd_XjJMt300P88tNOh-BVUkW0). She is humble but resolute in serving under-privileged kids. Among many services via her [Prasanchetas Foundation](https://www.linkedin.com/company/prasanchetas-foundation/), she works on prevention of child sexual abuse, and empowering women and children through skill development. Connect with her and donate as much as possible.  
  
Second is BMG Thomas, who runs an orphanage in Coimbatore. I have known him little more than a decade. He invests his own money and effort in caring for the orphans. I have visited their orphanage multiple times and the stories are both heartbreaking and encouraging. You can donate via: http://www.cropsrehab.org/part.html
  
If you are fortunate, build a longer table, not a higher wall.

## Continue Reading

* [Money Can Buy Happiness](/money-buys-happiness/)
* [Fame Or Fortune](/fame-fortune/)
* [Build On The Rock For The Storms Are Surely Coming](/build-on-rock/)