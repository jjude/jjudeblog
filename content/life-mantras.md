---
title="What are your mantras for life?"
slug="life-mantras"
excerpt="I have two mantras. What they mean to me?"
tags=["faith"]
type="post"
publish_at="18 Aug 19 06:14 IST"
featured_image="https://cdn.olai.in/jjude/life-mantras-gen.jpg"
---

There are two mantras, both based out of the Bible.

First one: "**The horse is prepared for the battle, but the victory comes from the Lord.**"

What does it mean for me? I have to prepare. I have to prepare well and smart. But preparation depends on knowing what I have to compete for and against what. So I should form a vision of the future and then build assets and skills to build towards that future.

But...there are external factors and forces that play in winning. Be aware of it. Sometimes those external forces will favor you, sometimes not. Don't give up if those factors don't line up in your favor. Keep practicing and go on competing. One day, you will have the future you want.

Second one: "**Don't be entangled by this world, but renew your mind daily.**"

There are so many norms in the society that surrounds us - the place of women, casteism, beliefs like "millions can't be made in honesty," and so on. If I'm going to subscribe to those norms, I'll be beaten down to the average in that society.

I have to introspect and challenge myself of the chains the society holds on me. Before I can challenge these norms, I need to know them. The only way to be aware of these chains is by reading widely, exposing myself to opposing viewpoints, engaging respectfully with differing views, and then take the journey of a new path to break those chains.

These two have helped me to come to where I'm and hopefully to where I want to go.

_I already published, the [principles](/principles/) under which I operate in professional life. You may be interested to read it._


## Continue Reading

* [What I learned about leadership from the book 'Lead like Jesus'](/lead-like-jesus/)
* [Proven Biblical Principles To Build Wealth](/biblical-wealth/)
* [How the Bible shaped my thinking?](/bible-shaped-my-thinking/)