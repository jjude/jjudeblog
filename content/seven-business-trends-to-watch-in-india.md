---
title="Seven Business Trends to Watch in India"
slug="seven-business-trends-to-watch-in-india"
excerpt="What are the future trends in emerging markets?"
tags=["coach","opinion"]
type="post"
publish_at="08 Sep 10 16:30 IST"
featured_image="https://cdn.olai.in/jjude/seven-business-trends-to-watch-in-india-gen.jpg"
---

Some say India is an [emerging market](http://bsr.london.edu/lbs-article/410/index.html). Others say it is a [faster growth market](http://bsr.london.edu/lbs-article/575/index.html). Leaving such semantics aside, it is no doubt that this decade will be the determining decade for India.

The race is already on, between Buffalo and Bangalore and Shanghai and Mumbai. In the sectors where government regulations are less, Indian talents are already racing on par with mature markets. On the other hand, the heavy handed regulatory system, sadly, hurdles growth in many sectors. Overall, I’m optimistic that the tension between regulators and entrepreneurs will result in positive changes in India.

Until now investments in India were, primarily, for the growth of other nations. In the coming decade we will witness investments for domestic growth and I bet on the following sectors:

1.  <a name="education"></a>**Education** : Indian parents have traditionally placed a premium on education and it has paid off well. With economic growth, there will be new models of education emerging. Already Zoho and Arvind Eye Hospital are experimenting with a beyond-the-university-campus training model. Such models might emerge as a business track on their own.

	On top of it, there will be considerable investment into research and development. So far we have been happy to re-engineer or copy other’s research and satisfied ourselves with tinkering. But this decade will witness India specific research in health, public administration, agriculture and so on.

2.  <a name="entertainment"></a>**Entertainment**: For most Indians, entertainment is limited to watching a movie or cricket or a TV serial. Though we are witnessing large budget movies and too much cricket, these markets are yet to saturate. So we will see more of the same. In addition, we will also witness development of allied markets - theme parks, music bands, youtube theaters and like. As Indians grow rich, there will also be markets for adventure sports, yachting, gambling and so on.

3.  <a name="mobile"></a>**Mobile** : There will be a lot happening on our palm. E-commerce, Location based services, Entertainment will come into mobile. Mobile communication is a highly competitive market in India and hence prices will go down and features will go up. Mobile users will demand a lot more in their hands in the coming days.

4.  <a name="rural"></a>**Rural** : Indian villages have not yet been touched by the much celebrated economic progress. True India is in her villages and if India has to progress as a whole without disintegration, progress and growth has to reach the rural areas. Acclaimed management guru, [C. K. Prahalad](https://en.wikipedia.org/wiki/C._K._Prahalad), has already sealed the idea that serving the bottom of the pyramid is a profitable business. Many companies are also experimenting with the concept - like [ITC e-choupal](https://en.wikipedia.org/wiki/E-Choupal). As rural India gets exposed to technological innovation, entrepreneurs will emerge from within their communities. They will posses the best insight about their markets and will partner with technology vendors to create services and products specifically made for their markets.

5.  <a name="governance"></a>**Good Governance** : With explosion of ‘soft-industries’, first time entrepreneurs are springing up everywhere. They will demand less complicated and transparent regulatory systems. This will lead to many good governance programs, delivered via e-governance platform. This in turn will bring lot of private-public partnership deals.

6.  <a name="security"></a>**Security** : Security of the nation and business in both physical and virtual will be a big business. As India develops, internal and external forces will try to disrupt the growth path. So there will be a lot of investment within the security sector. There will be both preventive and corrective technologies emerging in the security sector.

7.  <a name="living"></a>**Art of Living** : With growth and steady rat-race men and women will feel a vacuum - or would be made to feel a vacuum. Yoga, meditation and such are already a billion dollar business. This will continue to grow. One set of people will spend on pubs; others on yoga; both won’t mind spending on a therapeutic massage.

As new entrepreneurs emerge and new investments are made, there will be a strong temptation to transplant the best practices of the western world into India in each of these sectors. While we can certainly learn from these practices, it should be emphasized that tools, techniques and methodologies relevant to our Indian ecosystem has to evolve. Talking of ecosystem, it is also true that even our ecosystem should undergo change to support such a growth. How?

Growth comes from experimentation; but experimentation means failures, lots of them. Traditionally our society looks down on failures. But if India has to progress, we should be willing to accept and even encourage failures as we experiment to grow.

In all, this decade is going to be an interesting and action packed decade for India and Indians.

