---
title="Gravitas WINS"
slug="gwcourse"
excerpt="Build flywheels of success for life and career"
tags=["course","biz"]
type="page"
publish_at="24 Jul 21 16:14 IST"
featured_image="https://cdn.olai.in/jjude/gw-course-testimonials.jpg"
---

<div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/473379288?badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen style="position:absolute;top:0;left:0;width:100%;height:100%;" title="Why join Gravitas WINS course?"></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>

Those who have gravitas attract dollars, opportunities, positions, and esteem.
Learn how to gain gravitas in this practical course.

<p class="center"><form><script src="https://checkout.razorpay.com/v1/payment-button.js" data-payment_button_id="pl_Hd7ec1IQBGlYDA" async> </script> </form></p>

***

<h2 class="tc">Course Contents</h2>

### 1.  Build your vision

![Build your vision](https://gravitaswins.com/vision.png "Build your vision")

 Vision gives us hope when going gets tough. Build a vision that brings the best of you and empowers the world around you.

### 2. Turn your vision to action

![Turn your vision to action](https://gravitaswins.com/action.png "Turn your vision to action")

 Building a vision is just a start. Learn to execute your vision incrementally and iteratively with a 12-week sprint. 

### 3. Benefit from Insights

![Benefit from Insights](https://gravitaswins.com/insights.png "Benefit from Insights")

To stand out from the crowd you need insights. Generate useful insights with "Consume, Produce, Engage" framework. 

### 4. Let network work for you

![Let network work for you](https://gravitaswins.com/network.png "Let network work for you")

Your success depends on the number of people who want you to succeed. How will you keep that number high all the time? 

### 5. Think like wealthy

![Think like wealthy](https://gravitaswins.com/wealth.png "Think like wealthy")

If you don't own assets, you can't become wealthy. Learn how differently wealthy think. 

### 6. Stay on top with self-control

![Stay on top with self-control](https://gravitaswins.com/self-control.png "Stay on top with self-control")

 Building wealth, insights, and network will get you to the top. Discover how you can build self-control so you can stay on top. 

 ***

<h2 class="tc">Course Information</h2>

* The course is 6-week-long, starting on July 2nd.
* We’ll meet every Saturday for an hour between 5 pm to 6 pm IST.
* It is delivered via Zoom.
* Each topic will consist of three main sections:
  * **Lecture**: A 45-min session of diving deep into the topic
  * **Q&A**: A 15-min session of Q & A clearing doubts or discussing implementations of the topic discussed
  * **Worksheets**: You'll have to complete worksheets for each topic before the next session.
* Recordings of the sessions will be available if you miss some sessions.


<p class="center"><form><script src="https://checkout.razorpay.com/v1/payment-button.js" data-payment_button_id="pl_Hd7ec1IQBGlYDA" async> </script> </form></p>

***

<h2 class="tc">What others are saying</h2>

![Gravitas WINS Course Testimonial](https://cdn.olai.in/jjude/gw-course-testimonials.jpg "Gravitas WINS Course Testimonial")

There are lot more [video testimonials](https://www.youtube.com/watch?v=bFke3nY1XPA&list=PL538B_xQcbX6mS-6TyXsefj0fVaWBdDv7)

***

<h2 class="tc">Frequently Asked Questions</h2>

<dl>
  <dt>1. What tools will I need to take the course?</dt>
  <dd>You'll need just a computer and internet connection. We'll connect using zoom app, so download the app. </dd>
  <dt>2. Can I sample the course?</dt>
  <dd> Absolutely. Join the first day of the course. If you are not convinced by the end of the first session, ask for refund. I'll refund the entire fee without any delay or question.</dd>
  <dt>3. What if I was not able to attend few sessions?</dt>
  <dd>Life happens. I understand. You'll get recordings of each session. You also get free pass to every subsequent batches. So you can catch up either via recordings or by attending subsequent batches.</dd>
  <dt>4. Can I pay via bank transfer?</dt>
  <dd>I use Razorpay for the payment. They accept payments via credit cards, netbanking, UPI, and so on.</dd>
  <dt>I still have questions. How to ask?</dt>
  <dd>Send an email with your questions to gravitaswins@jjude.com</dd>
</dl>

***

