---
title="Is SOPA Only About Movie Industry?"
slug="is-sopa-only-about-movie-industry"
excerpt="Why tech community isn't going against their own in the fight against SOPA?"
tags=["opinion"]
type="post"
publish_at="23 Jan 12 16:30 IST"
featured_image="https://cdn.olai.in/jjude/is-sopa-only-about-movie-industry-gen.jpg"
---

Call me foolish. But help me understand the reaction of technology community to [SOPA](https://en.wikipedia.org/wiki/Stop_Online_Piracy_Act).

SOPA was viewed within tech community as an effort by the rich and filthy movie and recording industry to kill internet. Obviously businesses that depend on internet were outraged. Internet users targeted [GoDaddy](http://www.godaddy.com/), an internet based business that supported SOPA. In a short period, GoDaddy saw over [16,000](https://en.wikipedia.org/wiki/Go_Daddy#Backing_of_SOPA.2C_and_resultant_boycott) domains transferred. These efforts didn't stop even after SOPA bill was withdrawn. [Paul Graham](http://www.paulgraham.com/), suggested to [kill hollywood](http://ycombinator.com/rfs9.html) with startup concept.

But while going through arguments on both sides, I found that there were many non-movie/non-music organizations that supported it. I found many book publishing agencies in the supporting list. And then companies like Nike & L’Oreal. Oh it gets interesting, [Business Software Alliance](http://www.bsa.org/), which has all the prominent software companies as its members like Apple and Microsoft, supported it as well. (Later, BSA withdrew its support as like [Godaddy](http://www.godaddy.com/)).

So why didn't the tech community propose that no technical books will be published through McGrawHill or HorperCollins or other publishing agencies who supported SOPA? Why PG didn't propose to kill Microsoft or Apple who were part of BSA? [Richard Stallman](https://en.wikipedia.org/wiki/Richard_Stallman) would have been happy. Or force these companies to leave BSA as Kaspersky did?

Is it intentional blindness? Or there is a point at the other side of the table which tech community doesn't (want to) understand?

