---
title="On the blue mountain"
slug="on_the_blue_mountain"
excerpt="Spending New Year in Ooty"
tags=["travel"]
type="post"
publish_at="10 Sep 05 16:30 IST"
featured_image="https://cdn.olai.in/jjude/on_the_blue_mountain-gen.jpg"
---

Ooty is a well-known hill station in Tamil Nadu, India. In fact Ooty was the only tourist town known to us when we grew up. From School trips to Honeymoon trips every one went to Ooty. My sister and I had been to Ooty in family, school and college trips. But my brother-in-law had not seen Ooty. So off we went to Ooty during 2004 New year holidays.

At noon 28th December, we checked into YWCA. After a warm bath and a homely lunch, we set out to explore Ooty. Ooty looked similar to any other major city in the region - totally crowded with people, here and there some cows and dogs, walls full of graffiti, and large hoardings; but with a notable difference - a pleasant cilmate. We took an auto-rickshaw to botanical garden. It is a huge garden hosting species from all over the globe. The British estabilished the garden as early as 1848. Though it is well planned and maintained, it has a bit of artificiality attached to it. We strolled around bullying each other, taking pictures and eating maze. We didn't want to do a lot on the first day itself and so we returned to the hostel quickly.

**29th**

We hired a taxi to see around. There are guided tours with mini vans. But then you are restricted in time and place. That one reason was enough for us to go for a taxi. First we went to Dodapetta peak which is 2623 meters high and is the highest peak in Tamil Nadu. From here there is a wonderful view of Ooty and near by towns, collectively known as Nilgris. Nilgris is full of tea estates. How about stopping by a tea-estate? We stopped by one whose owner has around 2500 acres of tea gardens. Oh Man! Not every one in India is poor. There are five kinds of teas - the first four qualities of tea are for exports and only the last one comes into Indian kitchens. We had a taste of all teas and bought few packets as well. Then we drove to Sim's park, a botanical garden in Coonoor, a near by town. It has much more natural settings than the one in Ooty. Sim's park has a tree planted in 1850\. We also visited Dolphine's nose, named for the shape of the rock, and Lamb's rock, named after an Englishman, before returning to Ooty for a boat ride. Ooty lake was constructed in 1824 by Mr. John Sullivan, the Collector of Coimbatore, who located and constructed early settlements of British Raj in Ooty.

**30th**

After enjoying the well trodden tourist path, we decided to go where others don't go usually. We were passing through woods and being with unspoilt nature. Each of us were saying, "Wow! we never knew such places exist in our region". All of these places became known to public after series of Tamil movie shootings here.

To start we walked down a pine forest region to reach a lake. There is no boating, no shops, no crowd - just the lake surrounded by green mountain ranges. We thought we found a great place. The driver told us to wait for the next. Of course to find something great you need to exert sweat, even in the cold climate. We did so when we walked up to a place called 9th mile. I don't know the reason behind the name. But it is worth walking up. There is nothing but lush vegetation everywhere you see. The mountain range at the background makes it even more romantic.

When we got to Pykara waterfalls, we learned that there is not much water there. Actually my sister was very much eager to see the 'Nilgris Niagara'. But still we wanted to walk there to see how it looks without water. Good that we walked there. There was no waterfalls, but it wasn't disappointing. It looked like we were in a canyon. We took lots and lots of pictures. The only disappointing trip was to Pykara boat house. It wasn't worth walking for an hour. Still, we were walking through the forest and so we did enjoy the walk, up and down. Wasn't it time to return? No, we still had Glenmorgan. This is a power station which lies between the mountains and a winch to get there, but we should have gotten the permission and we didn't. But no worry, even without the winch it was a gorgeous view. We were surrounded by layers of mountain ranges and beneath us stood thick, dense forest. We couldn't have asked for more.

With a complete satisfaction, we drove back to Ooty. We couldn't believe what we saw. Our legs needed rest, off we went for rest.

**31st**

After a full day of walking and enjoying the nature, we decided to take it easy. We took rest in the morning and in the afternoon we visited Rose Garden, the name tells what it is. But we were not in season, so couldn't see roses in full blossom. Still, we came to know that there exists so many types of roses, not just in color but even in structure. Afterwards, we wandered around the commercial road, mostly window shopping. We bought some herbal oils. At 20h00, there was a special New year buffet at YWCA. It was a homely atmosphere with homely food. I heard that many frequent visitors came by for the party. It is not surprising, considering the fact that YWCA staff are very friendly and courteous.

**1st**

For a change we decided to use the public transport to go around. So we took a bus to Kotagiri, a near-by hill station. There is another reason as well, we were told there is not much to see in Kotagiri except the Kodanadu view point. Kotagiri is 30 kms from Ooty and it takes one and half hour by bus and from Kotagiri we had to catch another bus to reach the viewpoint, which is another half hour travel by bus. From this view point one can see, Moyar river forming a natural boundary between Tamilnadu and Karnataka. It takes curves and bends and beautifully enriches the land for vegetation. Together with the river, other peaks of the western ghats presents an idyllic view. The stylish 'M' shaped Rengasamy peak is a unique sighting. After around an hour enjoying the scenic beauty, we left back to Ooty.

**2nd**

We planned to visit Mudumalai, a wild life sanctuary. We booked our trip with Sri Murugan Travels, a day prior and the guy came promptly and collected the money. The van was supposed to pick us by 14h00\. But around 15h00, he called us to say that there is seat only for 2 persons and would we be willing to postpone our trip. What a customer service! As we didn't have another day, we couldn't make it to Mudumalai.

We went on with walking around the city, watching TV, and talking to a German couple staying in YWCA.

After days of being with nature, homely YWCA and piercing cold, it was time to say good-bye. We enjoyed being in Ooty and found a part of India we never knew before. We have with us sweet memories to cherish and lots of photos to revisit.

