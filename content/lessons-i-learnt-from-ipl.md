---
title="Lessons I learned from IPL"
slug="lessons-i-learnt-from-ipl"
excerpt="While corporate business and cricket are different domains, IPL has brought them together. Here are some lessons for corporates from IPL."
tags=["coach","insights","wins"]
type="post"
publish_at="04 Jun 08 16:30 IST"
featured_image="https://cdn.olai.in/jjude/ipl.jpg"
---

Unlike most of the Indian populace, I was no fan of Cricket. It looked so dull.

That was until [IPL](https://en.wikipedia.org/wiki/Indian_Premier_League) came along. IPL followed the short and fast version of the game named T20. Suddenly it was interesting.

I was interested in IPL for one more reason. It brought the corporate world into Indian sports; it was interesting to watch corporate principles played out on and off the field. As the game was played with intensity and passion, I observed few lessons that I could learn from IPL.

![Lessons I learned from IPL](https://cdn.olai.in/jjude/ipl.jpg)

### A chance to learn from superstars

IPL provided an opportunity for the young players to rub shoulders with the legends of the game. Those who grew up admiring [Tendulkar](https://en.wikipedia.org/wiki/Sachin_Tendulkar) as a role model were bowling against him! With a packed schedule, these players got an opportunity to observe, interact, and learn from their heroes.

In the IT industry, the open-source movement provides such an opportunity. There are many exceptionally designed Open Source applications and frameworks. You can either contribute (in terms of development, testing, documentation, and so on) to such libraries/frameworks or create an applications using them. Working with open-source frameworks and libraries will enrich your programming experience. I’ve used/using [wxWidgets](https://en.wikipedia.org/wiki/WxWidgets), [Django](http://www.djangoproject.com/), [libXML](http://xmlsoft.org/) and [jQuery](http://jquery.com/) and each of these have given me a superb exposure to design, programming and different implementation styles.

### Money isn't everything; there is also honor

IPL Franchises paid obscenely high amounts to iconic players and some non-iconic players too. All of these ridiculously expensive players fumbled on the field, except [Dhoni](https://en.wikipedia.org/wiki/Mahendra_Singh_Dhoni), who led his team into finals. The most valuable players were from the cheapest teams - [Rajastan Royals](https://en.wikipedia.org/wiki/Rajasthan_Royals). While the flamboyant owners of the expensive teams bit the dust, the cheapest team brought home honor after honors. They went on to play one of the finest Cricket rather than worried about their "low pay."

Such a thought process is way too common in management. There are many instances where 'icon players' are paid ridiculously high amounts, and they don't appear anywhere near 'valuable players.' Working in such environments is a depressing one - one works their ass out while another gets to go home with a hefty package.
On the other hand, I've seen juniors complaining about their 'low pay' and let go of many opportunities to learn.
It seems an ironic truth that 'higher the cost; lower the value.' It has been proved many-a-times in the corporate world; and one more time in IPL.

### Team composition - a must for a win

Very few teams got their fundamentals right - forming a balanced team. While it is essential to have 'super' skilled batsmen or a bowler in an intense game like T20, it is imperative to compose the team in a well-balanced way - a disciplined team skilled in all matters of the game led by an inspiring skipper. There is no other substitute for victory; and an ongoing victory march.

It is a common misconception that hiring an 'icon' will guarantee a win. An IIM MBA or an IIT architect alone can not do miracles; one needs to invest in the entire team. A recent IT management trend seems to fire seniors; we will 'use' freshers and deliver projects. Sadly that doesn't work.

Another important lesson in team management is that everyone should be treated equally. [Warne](https://en.wikipedia.org/wiki/Shane_Warne) mentioned in an interview that any player who is late for the practice is fined - even [Watson](https://en.wikipedia.org/wiki/Shane_Watson). In the corporate world, it is a known practice to exclude the elites from all 'restrictive processes.'

### Leadership is not a privilege; it is a responsibility

Captains of all teams, especially the top ones, led by standing shoulder-to-shoulder with other teammates. They didn't order their fellow teammates to play better while enjoying a Caribbean cruise; rather, Warne, Dhoni, and Yuvi inspired their teams by getting their hands dirty - they scored runs and took wickets when demanded. They kept a higher yardstick to measure their performance, and they strived to live up to it.

I've repeatedly seen that the most inspired team is the one where the leader/manager stands one among the team, inspiring them with actions. When a leader fails to command respect and inspire the team, the spirit of the team decomposes, and they don't make much of headway.

It is also vital for leaders to create an environment where their teams can thrive with a definite goal in mind. Once such an environment is set, stand aside and let the team enjoy their work. Don't micro-manage.

Two quotes of George Patton are relevant: 

- Always do everything you ask of those you command. 
- Never tell them how to do; tell them what to do and let them surprise you with results.

### Each game is a new one

While it is true that each win builds confidence, successful teams didn't become complacent in their past glory - they faced each game with a passion to win. They adopted a strategy specific to the circumstances of that day. Those teams didn't allow their occasional failures to derail their progress. They adopted a winning strategy and won again.

A business leader/manager can't rest in past glory. A blind implementation of 'Best Practices' from past victories will not guarantee success. It needs to be tailored to the situation at hand.

Evaluate the current scenario and implement a suitable strategy; review and modify it on the field if it's not working. If one falters to decide on the field, the team will lose its confidence to win (and probably not respect the leader).

### You are a brand

An IPL player is not an employee. He is hired based on the perceived value, which is nothing but the brand value. As he accrues wins, he increases his value and hence attracts more money. An IPL player doesn't have a job; He has a career, which he builds with a portfolio of wins, fans, and endorsements.

The future of jobs is [network of experts](/future-of-jobs/). It is spreading in sports, governments, startups, and Hollywood. Those who wake up will reap tremendous benefits.

### Conclusion

While corporate business and Cricket are different domains, IPL has brought them together. We will see more corporate practices in the coming years of IPL.

## Continue Reading

- [I have seen the future of jobs and it is Hollywood model](/future-of-jobs/)
- [Building In Public, Jeff Bezos Style](/build-in-public-bezos/)
- [How to find your element?](/finding-your-element/)
