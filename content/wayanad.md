---
title="Wayanad"
slug="wayanad"
excerpt="Best of God's own country."
tags=["travel"]
type="post"
publish_at="07 Apr 08 16:30 IST"
featured_image="https://cdn.olai.in/jjude/wayanad-gen.jpg"
---

Kerala is a popular tourist destination in south India. With pleasant climate and equally pleasant people, it is rightly named as 'god's own country'.

Wayanad is one such popular destination on the north-east of the state. Being part of [Western Ghats](https://en.wikipedia.org/wiki/Western_Ghats), it boasts of excellent natural beauty - wild life sanctuaries, mountain ranges, islands and lakes.

Three days are not sufficient enough to enjoy all of Wayanad; but unfortunately that was all the time that we had. We planned to utilize the best of these three days - we had detailed plans of what to see on each day.

![Map](https://cdn.olai.in/jjude/wayanad-map.png)

We started at about 6 am. from Bangalore in Toyota Qualis. We traveled via Mandya and reached [Rain Country Resorts](http://www.raincountryresort.com/) at about 1 pm., ready for lunch. It was an indicator of what we were going to have in the coming days - it was an excellent lunch.

After lunch, we left to Pookot Lake. We took a boat ride and then strolled along the lake for sometime.

Later in the day, at the resort, we were informed that a bandh was organized the next day to protest against the recent fuel hike. We didn't like it; but what is a travel plan, if it doesn't change during its course?

Though all of us took a book with us, I didn't want to spend a whole day sitting in the resort. I discussed with the resort staff for options. One of them offered to be a guide to walk us through the woods to the near-by peak. My brother-in-law and I gladly agreed.

I was excited at change of plan. Walking through woods was not in our initial plan. We had planned to hop from one tourist spot to the other. But this sudden change will give us an opportunity to spend much more time with the nature.

I was excited for another reason too - I was reading Bill Bryson's adventurous walk through the Appalachian Trail' in his book '[A Walk in the woods](https://en.wikipedia.org/wiki/A_Walk_in_the_Woods:_Rediscovering_America_on_the_Appalachian_Trail)'.&#160; Obviously I was thrilled to take a walk through the woods; of course my brother-in-law is no match for Katz (Bryson's partner in the walk), but he is closer.

We left after breakfast. The initial climb was a steep one and tiring. While BIL (brother-in-law) and I were panting and falling behind (much like Katz), the guide went on walking as if it was anything normal. I was afraid that our hike will end much before it started! But we survived.

The woods were wild and unspoiled. It was a fantastic feeling to be in the woods listening to the birds chirp and to the rustling sound of dry leaves on the ground. The rest of the walk was not so much steep as the initial one and we were glad about it.

The guide warned us that there were leeches along the route and he took a bottle of salt water in case it was needed. We didn't use it though.

The final piece was a steep climb on the hill. While the guide climbed up without much effort, it took us a lot of sweat. But when we stood on top of the hill, we felt such an elation - not just because we made it to the top of the hill, but the scenery was splendid. We were on a hill surrounded on all directions by mountains with layers of natural beauty. Having done a basic degree in geology, BIL went about giving me information of the rocks that were surrounding us. Though it was mid noon with sun scorching, we stayed back as long as we could.

We did have a concern that we will be bored for the rest of the day. But we were wrong. As my sister was flying frequently and all of us were caught up in the pace of our work, we didn't have any family time for few months. So this gave us a chance to sit down as a family and talk. We talked - talked about our childhood; dad's and mom's childhood days; our growing up; how life has changed and all else. We didn't realize how fast time went by until one of the staff came in to call us for dinner. We resumed the family chit-chat after dinner too. All of us felt pretty good about the day.

Next day we left to Tholpatty wild life sanctuary. We were the first ones to take the safari and we expected a thrilling one; but we didn't see any animals on the way. It was a disappointing one.

Immediately after breakfast, we went to Kuruva island, an uninhibited group of small islands. We were in the absolute tranquil with the nature. Having developed a liking for un-spoilt nature, I was enjoying each moment of the walk through the tall trees.

Kuruva island is not just a long stretch of tall trees; but it is made up of many rivulets, if I remember there are around 64 of them. Crossing each of them was dangerous - we had to cross on rocky bed as the stream flows through them. In some places water was knee deep. We were debating if we need to return without taking a risk on these rivulets, as my mom and dad were with us. But my mother surprised us saying, let us do it boys! We carefully crossed about 9 rivulets; in each my mother surprising us with her strength and stamina.

Every good thing comes to an end and so was this trip. But the trek through the woods, Crossing of rivulets with my parents and the quality time we had with each other will make us to remember our trip to Wayanad stay in our mind for a long time to come.

Enjoy some of the photos here. More photos in [Flickr](http://flickr.com/photos/jjude/tags/wayanad/).

![Biker following us](https://cdn.olai.in/jjude/man-on-the-bike-wayanad.jpg "Biker following us")

Group of bikers followed us.

![abandoned boat](https://cdn.olai.in/jjude/boat-wayanad.jpg "abandoned boat")

In Pookot Lake

![motherly care](https://cdn.olai.in/jjude/monkey-wayanad.jpg "motherly care")

Cute, isn't it?

![a natural pool](https://cdn.olai.in/jjude/pool-wayanad.jpg "a natural pool inside the rain country resort in wayanad")

Natural Swimming Pool in the Resort

![inside rain country resort](https://cdn.olai.in/jjude/light-wayanad.jpg "inside rain country resort in wayanad")

Shot during the Camp-fire in the Resort

![deer poached?](https://cdn.olai.in/jjude/deer-skull-wayanad.jpg "deer poached?")

Deer skull found on our trek

![wild mushroom](https://cdn.olai.in/jjude/wild-flower-wayanad.jpg "wild mushroom")

Wild Mushroom. There were many more wild flowers

![scenic beauty](https://cdn.olai.in/jjude/moutain-wayanad.jpg "scenic beauty")

BIL and the guide walking down from the hill

![ubiquitous vehicle](https://cdn.olai.in/jjude/jeep-wayanad.jpg "ubiquitous vehicle")

Jeep is an economical vehicle owing to the hilly surroundings

