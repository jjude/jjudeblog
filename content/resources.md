---
title="Resources"
slug="resources"
excerpt="All the resources needed for starting and maintaining a blog"
tags=[""]
type="post"
publish_at="18 Feb 14 16:30 IST"
featured_image="https://cdn.olai.in/jjude/resources-gen.jpg"
---

![Resources](https://cdn.olai.in/jjude/resources.png)

**Hosting**: I have been hosting my sites on Webfaction for close to a decade. After comparing many of the other hosting plans, I have concluded that they have the best deal for shared hosting. More than the price, what keeps me with Webfaction is their excellent customer support. They seem to work round-the-clock to help their customers.

**Illustrations**: I believe illustrations (or images) should accompany posts to make the point clearer. I use [xLine][4] for mind-mapping, like the one above. I use [GraphSketcher][5] for simple graphs as in [this][6] post. It is simple to use and open-source. For all other illustrations, I use [Omnigraffle][7].

**My Workflow**: Most of my posts start as a quick note in [Notesy][9], a dropbox based iPhone app. It helps me capture thoughts that flash through. These quick-notes are later expanded on my Mac using [nvAlt][8], another dropbox based note-taking app. Until a post is final, editing happens on both of these apps. I use [TextExpander][10] for entering repetitive texts like headers for blog posts. Final touches, if any, are done in [TextMate][11] before publishing.

**Publishing**: After running with Wordpress for many years, geek in me took-over. I switched to a static blog engine. [olai][2] is a self-coded blog engine in nodejs. Two primary advantage of static blog: 1) it loads fast 2) all my posts are now in source-control.

[2]: https://www.olai.in
[4]: http://www.xlineapp.com/en/index.html
[5]: https://github.com/graphsketcher/GraphSketcher
[6]: /critics/
[7]: http://www.omnigroup.com/omnigraffle
[8]: http://brettterpstra.com/projects/nvalt/
[9]: http://notesy-app.com/
[10]: http://smilesoftware.com/TextExpander/index.html
[11]: http://macromates.com/

