---
title="Book Notes - Industries of the future by Alec Ross"
slug="future-industries"
excerpt="Fields of next decade are-robotics, genetics, digital commerce, cyber-security, and big data. Countries that choose open systems will attract future industries."
tags=["books","coach","sketch"]
type="post"
publish_at="06 Feb 17 20:03 IST"
featured_image="https://cdn.olai.in/jjude/future-industries.jpg"
---
Alec Ross was an advisor to Hillary Clinton. During this stint, he interacted with many political leaders and entrepreneurs. This has given him insights about fields that will shape our lives in the next decade. In this [book](https://www.amazon.com/dp/1476753660?tag=jjude-20), he explores those fields and their impact on culture, economy, and geopolitics.

The fields of the next decade are: **robotics, genetics, digital commerce, cyber-security, and big data**. As a policy advisor, he also lays out recommendations for countries to prepare for the future. He recommends, **developing infrastructure, empowering citizens, and opening economy, culture, and politics**.

![Book Notes - Industries of the future](https://cdn.olai.in/jjude/future-industries.jpg)

Advances in artificial intelligence and machine learning means robots will shape our workplace and society. We can view robots as companions like the Japanese or as just machines like the Europeans. Either way, **robots will free us for productive activities**.

The connected world leads to new possibilities for medical specialisation and globalising the supply chain for medical diagnosis. Those in rural areas can use a mobile phone to take blood samples and transmit the data to specialists on the other side of the world. **Healthcare for just about any part of the body will be appified**.

**Codification of money, market, and trust will become the next big inflection point of economy**. On-demand economy makes market out of anything and a micro-entrepreneur out of anybody. This has long-term side-effects: As local population become entrepreneurs, large chunk of GDP of that place moves to silicon valley; there is more independence and flexibility but fewer worker protection and rights.

We have moved from **cold war to code war**. The new-age pirates attack corporate networks and hack elections. The same digital forces that create opportunities for a commoner brings a high-scale cyber attack to their door-steps. "We all want the liberty that comes with a vibrant online life, but **liberty without security is fragile, and security without liberty is oppressive**".

Alec Ross balances the promises and perils of innovation. As much as he discusses the promises of each innovation, he repeats a theme in every chapter: "**poor and wealthy will reside at different technological levels**". This will lead to cultural, economical, and political tensions. Because we live in a connected world, **these tensions won't stop at a country’s borders**. How countries respond to these tensions will affect the character and performance of their economies.

He dissects responses of countries to these changes. He contrasts Estonia with Belarus and China with India, and arrives at his recommendation: "**Countries that choose more open systems and maintain them will be the places where the industries and businesses of the future are founded, funded, and come to market**".

Estonia and Belarus started at the same position. Estonia opened up and flourished, Belarus closed off and deteriorated. China has an open economy and closed politics; India has an "inefficient democracy". China is poised to tap the next trillion dollar business of genomics, while India’s future remains uncertain, as ever.

> Are you looking for growth and opportunity? Look for next Estonia.

 He finishes with a poignant advice to world leaders: "there is great shame for society and its leaders when a life is made less than what it could be because of a lack of opportunity. **The obligation of those in positions of power and privilege is to shape our policies to extend the opportunities that will come with the industries of the future to as many people as possible**".

This advice is not only true for national leaders. It is equally applicable to entrepreneurs and parents.
