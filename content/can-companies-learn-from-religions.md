---
title="Can Companies Learn From Religions?"
slug="can-companies-learn-from-religions"
excerpt="Most religions follow a pattern to build a community. Can companies adopt these patterns?"
tags=["startup","faith"]
type="post"
publish_at="29 Jan 12 16:30 IST"
featured_image="https://cdn.olai.in/jjude/can-companies-learn-from-religions-gen.jpg"
---


The spread of Christianity has always fascinated me. It started as an offshoot of a major but a regional religion; the initial followers were not the decision makers & influencers but the outcasts; all of them - including their leader were ridiculed as crazy; even before they could take a strong root, their leader was killed and not the least, the mighty empire of that time was against them, persecuting[^1] and murdering most of them. Despite all of these odds, it spread far and wide to be the most popular religion and remains one even after 2000 years.

Now look at another popular [religion](https://en.wikipedia.org/wiki/Hinduism)[^2]. The contrast is telling. There is no founder; there is no unifying school of thought; it's polytheistic and every one is free to make a god for themselves, and few even make a god of themselves. That such an institution survived for these many years is beyond my comprehension. But it not only survived but spread over the world to be a popular religion. Beats me.

If you leave out the beliefs that separates them, most religions follow a pattern: start with an idea[^3], nurture the idea within a close group, build radical followers and enthusiastic evangelists simultaneously, retain the essence of the idea while spreading culturally and geographically, be relevant through generations, all the while generate enough monetary support to carry out the expansion[^4].

Isn't this the dream of CEO of every company?

Steve Jobs mastered the art of taking an idea, building a following of fanboys and generating billions of dollars of cash. Its a known fact that he achieved this with a 'strong hand'. In the religion of Apple, he is god and his followers often crusade against infidels of Microsoft and recently Google.

Then there are companies like [Hubspot](http://www.hubspot.com/), which has set itself a mission of "transforming the way the world does marketing" but [freed](http://onstartups.com/tabid/3339/bid/13420/Startup-Culture-Lessons-From-Mad-Men.aspx) employees to choose their way of achieving the mission.

So there are parallels in the corporate world to the way religions have taken wings. It may be worthwhile, to look at _how_ these religions achieved their goals and cross-pollinate the corporate world.

## Continue Reading
- [Can Companies Learn From Religions?](/can-companies-learn-from-religions/)
- [Culture is what you do, not what you believe](/culture-is-doing/)
- [What are your mantras for life?](/life-mantras/)

[^1]: Point relevant to the present discussion is that early Christians had strong conviction of their beliefs that they risked their lives; whether it's right or wrong is irrelevant.
[^2]: I'm limiting the discussion to Christianity and Hinduism. I'm born and brought up in a Christian family, so I am well versed with its believes, history and importantly it's culture. Secondly, I grew up in a predominantly Hindu community, hence I'm aware of its history. I'm not familiar to this extent with other religions.
[^3]: Most of the religions have a founder, but like Judaism and Hinduism there may not be one. It may just be the life style of a community.
[^4]: Vatican and Thirupathi are the richest religious places in the world.

