---
title="Roshni Baronia on 'Digital Sales For Startups & SMEs'"
slug="roshni-digital-sales"
excerpt="Here is a mirror challenge for you - stand in-front of the mirror and sell your product or service to yourself."
tags=["gwradio","biz"]
type="post"
publish_at="01 Mar 22 06:00 IST"
featured_image="https://cdn.olai.in/jjude/roshnib.jpg"
---

<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless src="https://share.transistor.fm/e/f8281f5b"></iframe>

I am so thrilled to have Roshni, an award winning podcast host and a strategic sales expert as a guest. We are going to discuss how sales is changing post covid, especially for startups and SMEs. Hope you'll enjoy our conversation.

- How has covid changed sales esp for startups and SMEs?
- What hasn't changed in sales and what won't change?
- Will we see fractional sales officers soon?
- Do you think current time is the best for women to get into sales?
- How can developers get good at sales?
- I don't want to get in front a real prospect and then mess up my chances. What are some of the ways I can practice selling?
- What makes a person great at sales - skills, behavior etc?

![](https://cdn.olai.in/jjude/roshnib.jpg)

## Connect with Roshni:

- LinkedIn: https://www.linkedin.com/in/roshnibaronia/
- Twitter: https://twitter.com/roshnibaronia
- Website: https://www.roshnibaronia.com/

## Connect with me
- Twitter: https://twitter.com/jjude
- LinkedIn: https://www.linkedin.com/in/jjude/
- Website: https://jjude.com/
- Newsletter: https://jjude.com/subscribe/
- Youtube: https://youtube.com/gravitaswins
- Email: podcast@jjude.com
- Executive Coaching Program: https://gravitaswins.com

## Your feedback counts

Thank you for listening. If you enjoy the podcast, would you please leave a short review on Apple podcast or on YouTube? It takes less than 60 seconds, and it really makes a difference in finding this podcast. And it boosts my spirits. 😀

I would like to hear what resonated the most with you in this episode. Either you can email me or share it on social media and tag me. Thank you for your support.

Have a life of WINS.

## Amazing tools I use:
- I record all the remote interview with [Riverside.fm](https://riverside.fm/?via=joseph-jude)
- I use [Transistor.fm](https://transistor.fm/?via=jjude) for hosting these podcast episodes

_Gravitas WINS Radio is produced by [Joseph Jude](https://jjude.com/). Be sure to subscribe on [Apple Podcast](https://podcasts.apple.com/in/podcast/gravitas-wins-radio/id1562802449), [Spotify](https://open.spotify.com/show/7LOBjAyjBkechMC0xMIj9E),[Google Podcast](https://podcasts.google.com/feed/aHR0cHM6Ly9mZWVkcy50cmFuc2lzdG9yLmZtL2dyYXZpdGFzLXdpbnMtcmFkaW8=),[YouTube](https://www.youtube.com/gravitaswins), or the podcasting app of your choice._

Please check out other episodes of [Gravitas WINS Radio](/gwr-themes/). For easy consumption, they are categorized by themes. If you are a fellow podcaster, you might be interested in [How I Podcast](/how-i-podcast/) or [tools I use](/i-use/).

## Continue Reading

* [Mastering sales as a new CTO](/cto-sales/)
* ['Good Sales Skills Are Same As Good Life Skills' With Gaganpal Singh](/gaganpals/)
* [Should You Run That Extra Mile to Make a Sale?](/extra-mile-for-sale/)