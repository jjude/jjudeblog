---
title="Book Review – A Game Plan For Life by John Wooden"
slug="game-plan-for-life"
excerpt="If you wish to be mentored by a meaningful life, pick up this book."
tags=["books","wins","self","coach"]
type="post"
publish_at="04 Aug 10 16:30 IST"
featured_image="https://cdn.olai.in/jjude/game-plan-for-life-gen.jpg"
---

Out of many lives that we come into contact, only some touches our soul. Such lives are, without a doubt, remarkable. But how remarkable a life is that touches your soul even when you have never met, spoken or seen them. Coach John Wooden's is one such life.

He wrote '_[Game Plan For Life - The Power of Mentoring](https://amzn.to/2JTgmLC)_' when he was ninety-nine and he probably knew it would be his last book. He begins the book by saying, "this might be my most important work". I haven't read his other books, but I have come to believe his other works would be no less significant.

It is not surprising that he writes about his mentors; after all, this is a book on mentorship. But as you glance through his mentors list, you realize that five out of seven mentors are not famous or popular or can not be considered to have 'great' lives by normal standards. Then again, as coach says, "**there are tremendous lessons learned from the great people of the world, but there are just as many to be learned from the quiet people around us**". By this, coach gives us two subtle instructions. First, you don't have to look too far for mentors. They are in your family, school and society. Secondly, you better lead a life worthy of inspiration, "and in so doing, collectively shape the character of our nation as much as it's most visible leaders". I sincerely hope that we will collectively shape the character of our nation as we get into a period of economic growth.

The second part of the book is written by seven of his mentees. They are not just players and assistant coaches he trained but even those "who knew him from afar", as Roy Williams. A rather interesting addition to the list is Andy Hill who was displeased with the coach for twenty five long years. But it does not matter because in a moment of epiphany he realized that "**mentoring often occurs even when you don't want it to**". That realization helped Andy Hill to make peace with the coach and be a mentor himself.

As I read the pages, I made quite a lot of notes, underlined and cross-referenced many pages. But I did not expect what came at the concluding paragraph. It froze me for a minute and I stared as if he was talking to me in flesh. No book I've read, finished with such an impact. He whispers to his readers, in his characteristic gentleness, "**you have the potential- no, you have the responsibility - to be a mentor and be mentored**".

If you wish to be mentored by a meaningful life, pick up this book. You will thank me.

## Continue Reading

- [Build on the rock for the storms are surely coming](/build-on-rock/)
- [Quotable quotes from 'A Game Plan For Life' by John Wooden](/quotes-from-a-game-plan-for-life/)
- [Writing my obituary](/my-obituary/)
