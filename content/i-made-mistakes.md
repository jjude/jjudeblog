---
title="I made mistakes"
slug="i-made-mistakes"
excerpt="Everyone talks about their achievements. Tell me some of the mistakes you made."
tags=["coach","vlog"]
type= "post"
publish_at= "07 May 20 10:30 IST"
featured_image="https://cdn.olai.in/jjude/i-made-mistakes.jpg"
---

My friend [Rishabh](https://www.linkedin.com/in/rishabh-sharma-a669147b/) asked me an interesting question. "Everyone talks about their achievements. **Tell me some of the mistakes you made**."

I have scars all over my life. I've made mistakes in every aspect of life. Here in this post, I share just three mistakes. 

[![I made mistakes](https://cdn.olai.in/jjude/i-made-mistakes.jpg)](https://www.youtube.com/watch?v=w-Es6tRYCkU)

I learned in a Tamil medium school. We used to discuss even English in Tamil. Neither my parents nor my friends spoke English. So I had no way to practice whatever little English I learned in school. When I came to college, I didn't know much English. **Even when I graduated, I could hardly make one sentence coherently in English**. 

After college, I got a job as a lecturer at a computer Institute. I was teaching dBase, and C. Since I already liked software and coding, it was a lot easier for me. I would stretch my lectures for the complete hour, so I don't have to speak anything other than code, logic, and syntax. I told my students to approach me after the class if they had any questions. 

In one of the sessions, the logic was too tough to follow. One of the girls, an Anglo-Indian, asked me to clarify in the session itself. I was smart enough to anticipate and had prepared an answer precisely for such a situation. I paused for a minute and remembered the answer. What followed was a disaster.

I was to tell her **don't embarrass me** with questions, but I blurted out **don't embrace me** with questions.

From there to here, a lot has changed. I took it up as a challenge to speak in English. I would stand in front of the mirror and practice speaking in English. Today I write and speak in English comfortably. Why am I sharing this?

If you are from a village and studied only in a vernacular language, don't get discouraged or intimidated. English is just another language. You can embrace it without any embarrassment. 

The second mistake happened while I was deputed to a telecommunication company in Belgium. I was a lone Indian working with Belgian, mostly Flemish, men. My manager was a kind soul, Steven Tilley. He made every effort to make me feel part of the group.

We were implementing a helpdesk software for them. On a chilly March evening, I had to deploy a software release. We reviewed and tested everything. All seemed okay. We were to start the deployment at 7 pm, after the regular office hours. My manager asked me to start the deployment at seven and went to get a cup of coffee. At precisely 7, I started the deployment process. And the nightmare of every deployment engineer flashed in front of me.

```
drop table customers
drop table accounts
drop table transactions
```

I was supposed to comment these lines before starting the deployment. I forgot. I pressed ctrl-x, ctrl-d, ctrl-z, and every combination of keys I could press. The script was running on another server, and the damage was done.

Even though it was a cold March evening, I started to sweat. I imagined myself packing my bags to India with shame.

Steven walked in a few minutes later, and he sensed my mistake. I told him I deleted all the tables. With a wicked smile, he said, "I anticipated something like this. So I gave you credentials of the staging server instead of the production server." Phoof. I was still ashamed but relieved.

Today, I'm a chief technology officer. **That mistake ingrained in me the need for checklists**, among other things. Today I have checklists for everything important. 

The third mistake is a little personal. It happened when I was in the 8th standard, sometimes when I was 12 or 13. We went to a church in Kerala. As per tradition, after the church, we went take a bath in the vast pool next to the church. Neither my mom nor I knew swimming, so we were sitting on the steps and were taking a bath. Suddenly **I slipped and fell into the pool and started drowning**. My mom did not notice, but thankfully somebody else noticed. He rushed swimming to me and rescued me. That incident left a terrible fear in me of a pool of water. Even though I come from a beach area, I wouldn't go near any pool of water after that incident. 

I let that fear paralyze me until I was 34. When I was 34, I decided to do something about that paralyzing fear. I joined a swimming class. For the first few days, I would stand at the short side of the pool. Though the water was only up to the hip-level, I wouldn't move. The coach would tease me that I was wasting money. In my mind, though, I was already a winner because I was standing in the pool. In about a month, I managed to swim. **Even today I have a fear of drowning. I swim despite the fear**.

Why am I sharing this? During this COVID lock-down, you might be paralyzed with a lot of fear. Do not let fear cripple you. **The demons that you fight internally are more potent than anything else that you see on the outside**.

_If you have any career-related questions, feel free to [ask](/ask/). I answer them on [Gravitas WINS](https://www.youtube.com/channel/UCPDIeR7Lzvt9uATvMZSZX-Q) channel._

## Continue Reading

* [Future of jobs is Hollywood model](/future-of-jobs/)
* [How to deliver value in the digital age?](/value-in-digital-age/)
* [The curse of EVERYTHING and NOW on building your career](/all-and-now/)