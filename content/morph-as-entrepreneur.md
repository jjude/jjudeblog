---
title="Entrepreneurship is a metamorphosis, not a baptism"
slug="morph-as-entrepreneur"
excerpt="When I was an employee, I used to think that being an entrepreneur is much like an employee, only with added responsibilities. Now that I am an entrepreneur, I realize how foolish I was."
tags=["coach","startup","insights"]
type="post"
publish_at="06 May 15 03:29 IST"
featured_image="https://cdn.olai.in/jjude/2015-05-baptism.jpg"
---
_In Evangelical Christian circle, there is a long running joke, which goes like this._

_There was a minister who went from village to village preaching the gospel. After preaching, he would invite people to be baptized. Whoever comes front was dipped into the pool of water, and baptized. Usually he would also give them a new name. Ponnusamy dips in and Paul comes out; Periyasamy dips in and Peter comes out._

_One day the minister had a long day and was tired. He requested his cooking maid for his favorite dish - fish curry. She had just one problem - she couldn't get fish late that night. She got a brilliant idea having watched his master for long. She took ladies-fingers (Okra), dipped them one by one in a bowl of water, saying 'You will henceforth be called Sardine'. She then proceeded to cook a delicious fish curry with those new 'Sardine fishes' to her master._

When I was an employee, I used to think that being an entrepreneur is much like an employee, only with added responsibilities. Now that I am an entrepreneur, I realize how foolish I was.

Just consider problem solving, which is at the heart of entrepreneurship. As a corporate employee, the boundaries of solving problems are fixed, frozen and final. Usually the space to operate is limited as well. Employees learn to operate within those limits and don't bash into walls often.

As an entrepreneur, you are interested in solving the problem, not so much about the boundaries. For an entrepreneur, the boundaries are negotiable. You often poke at the boundary to see if a solution will pop at the other side of boundary.

Whenever I would call my mentor, [Sastry][4], with objections and hurdles I face, he would repeat, sometimes to my annoyance, "**stay loyal to the problem**." My annoyance was because I was listening to that phrase with an employee mindset.


![Baptism](https://cdn.olai.in/jjude/2015-05-baptism.jpg)

**If only I could be dipped into a pool of entrepreneurship and come out as an entrepreneur!**

After six months of long walks in every park near his house and my house, I think I am slowly beginning to get what he means.

My recent venture is a [blogging tool][1] that integrates with existing blogging editors to generate static HTML files which can be hosted anywhere -- Github pages, Amazon S3, or any Web servers. I evaluated node.js, python and golang and chose to develop in golang, though it is relatively new. I got into a fundamental problem when I had already developed most of the product I conceived for beta launch. To support existing editors, which was a key requirement, the tool has to support XMLRPC API. When I started in golang, I checked if XMLRPC server can be developed in golang with a proof-of-concept. I found the solution to be ok and went ahead with the development of the core features. When the time came to implement the XMLRPC API, I discovered a stumbling block. The existing XMLRPC blogger/metaweblog API methods were all lowercase; in golang the methods have to start with uppercase. I [asked][3] around if there was a work-around. To my disappointment, there were none. I was stuck. Without this key feature, there is no differentiator between existing solutions and mine. I might as well forget the venture. I was frustrated.

I took another long walk, this time alone, with the words "be loyal to the problem" echoing in my ears. During the walk, it occurred to me that I had created an unnecessary boundary. The key feature is XMLRPC API and not that it has to be in golang.

When I got back to my desk, I searched and was pleasantly surprised that a solution exists in Python. I was thrilled. Now I can mash-up a solution. I was back to solving the problem.

As I was designing the mash-up, another phrase of my mentor echoed. "**When you find a solution, see if you can push to find even a better one**". That was going to be tough. I had already searched everywhere, gotten nowhere and concluded that it can't be done in golang. Why should I look further? It would be a waste of time. My beta launch will be delayed. Yet, I persisted to see, if there is any truth in my mentor's maxim.

With my earlier search, I knew that I won't find a third-party solution; I will have to develop it myself. So I read through source-code of the Python implementation, read through few other golang implementations and finally arrived at a hack. My mentor's maxim seems to be a valid one.

I launched the beta on April 28th. Next day, I posted in MarsEdit forum. MarsEdit is a popular blog editor of choice for Mac. Daniel, the developer of the editor, [replied][2] that he liked the solution and will promote it once out of beta. (I'm looking for beta testers. If you would like to beta test the product, [signup][5] for it.)

![Transform](https://cdn.olai.in/jjude/2015-05-transform.png)

This is just one difference between an employee and an entrepreneurial mindset. There are other aspects too. Entrepreneurship is about discarding prior beliefs when situation change, handling ambiguity and willingness to discuss uncomfortable topics with uncomfortable people. This change of employee mindset into an entrepreneurial mindset doesn't happen overnight. If you persist in going through the emotional roller-coaster, you will morph into an entrepreneur. I hope I do.

Photo Credit: [Lakeside Fellowship](https://www.flickr.com/photos/71836609@N00/5802093281/)

[1]: http://olai.in
[2]: http://www.red-sweater.com/forums/discussion/3515/posting-from-marsedit-to-github-pages
[3]: http://stackoverflow.com/questions/29520593/lowercase-methods-for-gorilla-xmlrpc
[4]: http://twitter.com/sastrytumuluri
[5]: http://app.olai.in/signup
