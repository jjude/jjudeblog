---
title="Start the year with reflection of what happened"
slug="start-year-with-reflection"
excerpt="Don't have goals; instead count your wins and misses of the previous year."
tags=["retro"]
type="post"
publish_at="18 Jan 23 06:40 IST"
featured_image=""
---

It is a ritual for most of us to go with New Year resolutions. 

- Lose weight
- Learn Spanish
- Stop smoking
- Get out of debt
- Spend time with family

All of them are worthwhile ambitions and goals. But by the end of January, we have fallen so far behind these goals, it is not even funny to repeat them year after year.

After repeating this comedy for years, I stopped making new year resolutions.

Instead, I conduct **personal annual reviews** of the past year. That one action has brought me enormous **self-accountability**. I have done that for well over a decade.

You don't have to overthink it. Take the roles you play:
- father
- mother
- son
- CTO
- board member

Or alternatively, pick up the areas you are interested in: 
- finance
- health
- learning
- and so on

I conduct my retros on WINS - wealth, insights, networking, and self-control.

For each of these areas, write down your wins (what went well) and misses (what didn't go well) in the last year.

It might look like this

| Areas        | Wins                           | Misses                           |
| ------------ | ------------------------------ | -------------------------------- |
| Wealth       | Stock value increased 30%      | Should've recorded a course      |
| Insights     | Attended a course              | xxx                              |
| Networking   | Met the hotshot wanted to meet | Missed going for reunion         |
| Self-Control | Kept intermittent fasting      | Didn't spend much time with kids | 

**Identify the causes** of wins and misses. If you can do more of what caused your wins, **do 10x of them**. Similarly, if you can control what caused your misses, control them.

Your wins and misses will tell you what you should focus for the next period. The period could quarter or a year, though I prefer quarter. Keep this practice going and you will **get a force multiplier of WINS** in your life.

If you need some sample retros to get inspired, you can read one of my [retros](/annual-reviews/) (I have published for almost 10 years). I recently published my [2022 retro](/2022/).

But you might say, Joseph, I don't know what all happened last year. Fair point. Most people don't. That is why you should use a weekly planning and review sheet. When you [subscribe](/subscribe/) to my newsletter, I'll send you a free weekly planning and review sheet. 

This year, I am also publishing it in public. You can view them: [JFM Quarterly Plan](https://notes.jjude.com/2023-quarterly-plans.html) & [Daily Log](https://notes.jjude.com/2023-daily-log.html).

Have a year of WINS.