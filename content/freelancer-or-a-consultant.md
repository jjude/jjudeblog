---
title="Freelancer or a consultant?"
slug="freelancer-or-a-consultant"
excerpt="Considering that I'm my own boss but work from client site, should I call myself as a freelance consultant?"
tags=["coach"]
type="post"
publish_at="10 Jan 09 16:30 IST"
featured_image="https://cdn.olai.in/jjude/freelancer-or-a-consultant-gen.jpg"
---

"Am I a freelancer?" or
"Am I a consultant?" or
"Am I a freelancing consultant?"

Those were some of the questions on my mind when I decided to quit the corporate world and be on my own. I googled and understood that they are not the same.

Generally, creative artists - like photographers and journalists - like to retain their creative freedom and they equally dislike being constrained by the company rule books. They prefer to roll their sleeves, get the work done and be paid; and not bother with other overheads. So they sign-up for specific assignments and if they like the company (the team, the work, the money), they continue to work with the same company; else they go with another. There is another group of people who prefer freelancing - those who can't commit to a full time work schedule, primarily due to their personal commitments. In both categories, freelancers need not visit client office on a day-to-day basis.

Consultants are a different breed. They are (considered to be) experts in a specific domain and they advice their clients in solving problems in those domains. Their service will significantly influence the policies and projects of their clients. While freelancers are their own bosses, consultants need not be; they could be employed. It is common for employees in an IT firm, to have job roles as consultants. However, realizing their self-worth, some consultants quit their employers and take up independent contracts. It is expected that these consultants visit client office regularly, even on a daily basis.

Considering that I'm my own boss but work from client site, should I call myself as a freelance consultant? How about Independent Consultant? It sounds fancier, right?

Ref: [http://freelance.geekinterview.com/42-Difference-Between-A-Freelancer-And-A-Consultant-.html](http://freelance.geekinterview.com/42-Difference-Between-A-Freelancer-And-A-Consultant-.html)

