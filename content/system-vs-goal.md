---
title="Surgery Is Successful, Patient Is Dead"
slug="system-vs-goal"
excerpt="In the absence of an accompanying goal, a system is meaningless. "
tags=["coach","wins","systems"]
type="post"
publish_at="22 Jun 21 19:37 IST"
featured_image="https://cdn.olai.in/jjude/system-vs-goal-gen.jpg"
---

### What I saw In A Surgery Unit

I worked in a cardiac surgery unit at the beginning of my career. 

Everything had an elaborate process: preparing the patient, wheeling the patient to the surgery theater, operating the patient, and moving the patient to the critical care unit. Since they dealt with life and death, they must have such a detailed process.

But not all players were aiming for the same result.

- The patient wants to maintain a good quality of life
- The hospital is focused on getting the patient out of the hospital alive
- The surgery department aims to get the patient out of the critical care unit alive
- The surgeon is focused on getting the patient out of the operating room alive

The disconnect between individual goals defeats all benefits gained from the detail process.

A surgeon may declare their surgery successful, because patient was alive when wheeled out of the theater; however, the patient may die a few hours after checking into the critical care unit.

### Kids Studying Before Video Games

I have two boys aged 10 and 12. I had a rule that they had to study for an hour before playing video games. The eldest son is obedient and sincerely follows the instructions.

The younger one is naughty. He'll sit in his chair and turn pages for an hour while blabbering something out loud. Though he followed the process in theory, he didn't achieve the outcome.

### Corporate Version

The CEO plans to launch a new digital business. He tasks the marketing team with bringing in top-of-funnel traffic, and the sales team with making at least 20 calls per day.

One quarter passes. There is a lot of traffic to the microsite created for the new venture. Every day, sales guys make 20 phone calls. However, the business didn't take off and the CEO shuts down the new venture in frustration.

Why?

To bring in traffic, the marketing team used tricky tactics (blackhat SEO?). Though the page ranks high, gets a lot of traffic, and users fill out forms, none of them generate a great deal of value.

The sales team contacted 20 leads who would easily pick up the phone and have a conversation but not convert.

Surgery is successful; the Patient is dead.

### System vs Outcome Is A Wrong Discussion

James Clear and Scott Adams raised awareness about processes and habits. They deserve a lot of respect for that.

Having a system is important. If there is no process, the delegation will never work.

In the absence of an accompanying outcome, a system is meaningless. Everybody will check boxes one after another, but the company will not go any further.

You need to combine the system and the result to achieve the desired outcome. And everyone involved in the process should have a clear understanding of the overall outcome. Only then you can avoid a disastrous "surgery is successful, but the patient is dead" situation.

### Quote To Ponder

A good system shortens the road to the goal. - Orison Swett Marden

## Continue Reading

* [Factors affecting the outcome of our decisions](/decision-outcome)
* [What Studying For Exams Taught Me About Decision Making](/decision-process/)
* [Your answer is not under a streetlight](/metrics-dreams/)

