---
title="Rise with the rising tide"
slug="rise-with-rising-tide"
excerpt="It is easier to ride to success on a raising wave rather than creating a wave yourself"
tags=["luck","wins","coach"]
type="post"
publish_at="17 May 22 16:17 IST"
featured_image="https://cdn.olai.in/jjude/rise-with-rising-tide-gen.jpg"
---

Jeff Bezos read a report that predicted a 2300 percent annual growth in internet usage in 1994. Bezos decided to take advantage of this wave. Next year, he founded Amazon.

Even in India many entrepreneurs got on the internet bandwagon and made a name for themselves.

It is easier to ride to success on a raising wave rather than creating a wave yourself. Would you set up your shop in a bazaar where there is a great deal of foot traffic or would you set it up in a remote area and try to attract customers?

There are three ways you can ride a rising wave.
- **Trend**: There is always a technology trend or a business trend on the rise. You should study these trends and capitalize on them. You can create a product or service around them, teach them, or blog about them.
- **Cities**: Every country has cities where innovative things happen. They could also be field-specific. In India, if it's a movie, it's in Mumbai or Chennai. Technology is most likely happening in Bangalore, Chennai, or Hyderabad. If it is finance, it happens in Mumbai. From ancient times people moved to urban areas for this precise reason. Opportunities cluster around specific cities.
- **Companies**: By joining a company that is playing in the trend, you not only gain a deeper understanding of the trend, but you also help shape it by building useful products. You will meet other people in the company with whom you can create amazing products later on in life.

_The outside world can push you into Day 2 if you won’t or can’t embrace powerful trends quickly. If you fight them, you’re probably fighting the future. Embrace them and you have a tailwind. - Jeff Bezos_

## Continue Reading

* [Trends As A Guide To The Future](/trends21/)
* [Approximately Correct](/approximate/)
* [How To Find Trends In Your Industry](/trends-in-industry/)
