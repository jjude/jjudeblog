---
title="Embrace The Unknown"
slug="embrace-the-unknown"
excerpt="Hidden treasures of life can be explored only by series of bets"
tags=["luck","wins"]
type="post"
publish_at="27 Apr 22 05:00 IST"
featured_image="https://cdn.olai.in/jjude/embrace-the-unknown.jpg"
---

![Embrace the unknown](https://cdn.olai.in/jjude/embrace-the-unknown.jpg "Embrace the unknown")

I'm from a lineage of fishermen. Even though my parents are now teachers, I still love watching the waves and fishermen fixing their nets on the beach. I once went deep sea fishing with colleagues. As my colleagues threw up from the roller-coaster of the sea waves, I managed to catch a fish among the roaring waves. You can take a man out of the sea, but not the sea out of a man!

Every day, fishermen go out into the unknown. Some days they catch a lot. Other days they come back empty-handed. On the worst days, they lose a loved one to nature. But they keep stepping into the unknown. They have to. That is their life.

Maybe it's the fisherman gene in me that pushes me to go into the unknown so much. I've been lucky enough to catch a couple of things I'm proud of. But I have also had my share of losses, though not the loved ones.

I was deputed to Belgium in 2000. I loved the country and saw great opportunities. I decided to set myself up as an independent consultant. I hired a lawyer for €2,000. My dreams were to embrace the unknown, but also to have a pleasant future. In a twist of many turns, my contract was cut short, and I returned to India with my dreams crushed.

After a few years, my mentor presented an opportunity that was a leap into the unknown in every sense. There was a short-term contract with a premier e-government project in Delhi. I did not know Delhi well. On top of all that, neither my wife nor I speak Hindi. I cast the net to the unknown. Everything changed for me.

My wife and I became closer because we didn't have anyone else. The six-month contract turned into six years. To cap it all off, I was presented with a national Web-Ratna award by the then IT minister of India. It was all because I wasn't afraid to take chances.

**My life is a series of bets on the unknown**. Sometimes I come home with a bounty, like the fishermen. At other times, I sleep with a heavy heart. Every day, however, I face the unknown. The treasures of life are hidden behind and beneath those unknowns.

If you would like to adapt this method, I recommend following these steps:

- Develop a "Life is an experiment" mentality.
- Develop a framework for identifying potential opportunities and threats.
- Pick a path to experiment for a minimum of three months
- Identify what could go wrong and put in place guardrails and guiding lights.
- Create an exit strategy
- Do it
- Document your progress, mostly the reasons and hypotheses behind your actions
- Repeat, correcting mistakes as you go and incorporating what you learn

## Quote To Ponder
_"Let go of certainty. The opposite isn’t uncertainty. It’s openness, curiosity and a willingness to embrace paradox." - Tony Schwartz_

## Continue Reading

* [Luck Loves The Paranoid](/luck-loves-paranoid/)
* [Life Is Series Of Lost, Found, And Lucky Breaks](/life-is-lost-and-found/)
* [Approximately Correct](/approximate/)