---
title="Replace Evernote with Joplin, an open source note taking app"
slug="joplin"
excerpt="With Joplin, I am finally ditching Evernote"
tags=["apps","sdl"]
type= "post"
publish_at= "22 Sep 19 06:00 IST"
featured_image="https://cdn.olai.in/jjude/joplin.jpg"

---

![Joplin App](https://cdn.olai.in/jjude/joplin.jpg)

I have been using Evernote for close to a decade now. It has notes from all the books I have read, best paras from the articles I liked, and even reasons for investing in specific stocks. With about 2000 notes, it serves as my external brain.

Evernote has a fantastic ecosystem. I can email note, I can send an article from Pocket, and I can clip a webpage from Firefox. I can use the app on every platform. I can use Evernote even under Linux, using a 3rd party application.

Ever since the future of Evernote became uncertain, I started looking for an alternative. My expectations are simple (at least I like to think so):

- It should work on Mac, Android, and Linux, the platforms I use
- It should store data locally. If an app becomes my external brain, I better have access to the data all the time and have a backup too.
- It would be a bonus if it stored data in plaintext because text format will stay for decades.
- It should have browser integration so I can clip interesting webpages

I searched far and wide and tried many of the "coolest" apps. But nothing came close to Evernote. Even after all these years, Evernote is still the undisputed king of note-taking.

Recently, I came across an interesting open-source application called [Joplin](https://joplinapp.org/). Joplin is the first application that ticks every expectation I have.

I have been using Joplin for a week now diving deep into all the features it has—including its terminal application. In Joplin, I might have found the application to replace Evernote finally.

Here are some of the positives and negatives of Joplin.

### Positives

- It has almost all the features of Evernote that I care - notebooks, notes, tags, images, web clipping, sync, and apps for multi-platform.
- It stores everything locally in text files. So even if Joplin vanishes, I will still have access to my notes.
- Search in the desktop app is reasonably fast and accurate.
- You can import all your notes from Evernote. I imported around 2000 notes from Evernote.
- Joplin can support Dropbox, OneDrive, etc. for sync. I used Dropbox, and the sync is fast.
- I can link to other notes with Joplin, which means I can create a wiki within Joplin. If I'm writing a book, this is handy.
- Joplin comes with a web clipper equivalent to Evernote. Surprisingly it works without a flaw. This one feature might tie me to Joplin.

### Negatives

- Biggest negative for me is that the user experience of the apps is not very appealing like Evernote.
- Evernote import was not that clean. Had to go in and edit many of the imported notes.
- Initial sync took a lot of time. I left it for the night to complete. Tags took even longer time to sync. Maybe because there were 2000 notes.
- It doesn't have a highlight feature as in Evernote
- Desktop app is slow even in rendering the preview of the notes
- Mobile app doesn't have "pull to refresh" feature
- Mobile app has "all notes" feature. The desktop apps miss this feature, leading to a cognitive gap when switching between mobile and desktop.
- Tried export to pdf on Mac. It leaves out many lines. It is not complete and hence, unusable.
- In Evernote, I can share a note with others. Sharing a note is not possible with Joplin.
- There is no integration with spell check. When you are typing fast, you tend to make a lot of mistakes. There is a way around, though. There is an option to invoke an external editor. So you can configure to edit your notes in your favorite editor, even if it is vim.
- When I delete a note that has an image, the image in the `_resource` directory is not deleted. This leads to orphan images and heavy storage requirement.
- It doesn't have a 'Recycle bin,' so in case you accidentally deleted a note, you lost it completely. If you enable autosave in external editors, they usually delete the existing note and create a new note. This will cause Joplin to delete the note and create a new "untitled" note.

Even though there are many negatives, Joplin meets all my fundamental expectations. I am going to use it for at least six months.

### How can you get started?

The [homepage](https://joplinapp.org/) of the application has clear instructions to install applications on all platforms. I followed the given instructions for Evernote export and import too.

If you have a lot of Evernote notes, then initial sync will take a long time. Better to leave it for the night.

While you are at it, install the web clipper too. It is awesome.

### Bonus (git repo with notes)

Though you type in plaintext, Joplin uses SQLite internally to organize notes. But it has an `export` option. You can combine it with a git repository. Now you have version control for your notes. Here are the steps:

1. Create a repository in Gitlab / Github
2. Clone the repository locally
3. Install Joplin terminal application (if you have installed the GUI application via `brew` then `brew` won't create shortcut links when you install the terminal application. You should alias it)
4. Create the following script and run it whenever needed (don't forget to change the directories!)

```
BACKUP_DIR="~/joplin"
JOPLIN_BIN="/usr/local/Cellar/joplin/1.0.141_1/bin/joplin"
NOW=$( date '+%F_%H:%M:%S' )

rm -rf $BACKUP_DIR/notes
$JOPLIN_BIN --profile ~/.config/joplin-desktop/ export --format md $BACKUP_DIR/notes
cd $BACKUP_DIR
git add .
git commit -m "update-$NOW"
```

## Continue Reading

- [Topics that interest me now](/topics1920)  
- [Build An Ecosystem For Learning](/build-an-ecosystem-for-learning/)  
- [Apps I use - Pocket](/apps-pocket)
