---
title="First Experience"
slug="first-experience"
excerpt="Voting in an Indian election is an experience in itself."
tags=["opinion"]
type="post"
publish_at="10 May 08 16:30 IST"
featured_image="https://cdn.olai.in/jjude/vote.jpg"
---
I am not proud of saying this. Yet the truth is that, I never voted in Indian elections.

It is not that I am one of the many many frustrated Indians about Indian Democracy. It is true that most of our politicians have been detrimental to country's progress rather than contributing to it. Despite that sad fact, democracy is one of the greatest strengths of India.

![vote](https://cdn.olai.in/jjude/vote.jpg)

The reason I never voted is related to me being in IT industry. I was born in Tuticorin, a city on the south end of India. However I studied in Coimbatore and then worked in Chennai. When I came of age to vote, I didn't care about voting and elections. Once I got to realize that I should be a responsible citizen, I moved out of the country. As India doesn't have a e-voting mechanism, I couldn't exercise my rights.

Another constraint in voting is that one need to be in the registered constituency. With a job that demands a heavy migration, I couldn't vote until now.

But this time, I was determined to vote. I should say that my Dad was determined that all of us vote. He ensured that all of us got registered in the proper constituency and got us the voter ids. But we had fun (!) looking through our names - My Dad's name is Justin Rosary John Britto and I am Joseph Xavier Jude; our names were printed as vasa jastina rojari jana brote and josepha jeviyara judisa. Same was true for the entire family.

We went to the polling booth immediately after lunch. We expected that there will be huge crowd. There wasn't. We were the only ones at that time. We had already discussed about whom to vote. So we voted quietly and returned home.

I am proud to be a responsible citizen.
