---
title="Book Review - Making Breakthrough Innovation Happen"
slug="making-breakthrough-innovation-happen"
excerpt="Case studies of breakthrough innovations in India."
tags=["books"]
type="post"
publish_at="22 Aug 10 16:30 IST"
featured_image="https://cdn.olai.in/jjude/making-breakthrough-innovation-happen-gen.jpg"
---

Consultants face myriad of business situations, each with myriad of complications. Case studies are the quickest method to be introduced of a situation, it's complications, and attempted approaches in resolving them. But to be useful, the context of the situation should be relevant. Unfortunately, there aren't many interesting and engaging case study books in the library of Indian consultants.

_Making Breakthrough Innovation Happen_ by [Porus Munshi](http://twitter.com/porusmunshi), of Erehwon Innovation Consulting, fills that void. The book describes how eleven Indians challenged the status quo and pulled off the impossible in their fields. These orbit-shifting innovators, as the author calls them, didn't raise to international industry standards; they set up new industries. Make no mistake, it is not filled with Ambanis and IT Companies. That would have been a banal collection. Rather this book is an interesting mix of cases from public and private sector; startup and legacy companies; and product and service sector.

Think of some of the questions raised to and by Indian consultants:
> Can a filthiest city afflicted by malarial deaths be transformed into second cleanest city in India?
>
> When the Swiss can't do it, can Indians do it?
>
> In a country filled with licenses and administrative hurdles, can one man create a whole new industry?
This book describes how some Indians answered with an resounding yes to these questions.

In a land of armchair critics, it is heartening to read of Indians who refused to accept status quo, rolled their sleeves and reshaped the situation and in the process, emerged as inspirational leaders.

But the wisdom of these innovators are not limited to Indian context alone. Standardization of medical practices by Arvind Eye Hospitals, Insight Pilgrimage by CavinKare, Fight against dilution of strategy by Chola Vehicle Finance are applicable to every business.

The cases in the book are a testament to the fact that transformation of public sector, innovation in product development and beating international giants in their speciality happen within the geographical boundaries of India.

This book is a must-read for Indian business consultants; for others it will be an inspirational read.

_Disclosure: Some of the links in the post above are “affiliate links.” This means if you click on the link and purchase the item, I will receive an affiliate commission. Regardless, I only recommend products or services I use personally and believe will add value to my readers._

