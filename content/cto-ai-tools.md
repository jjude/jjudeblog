---
title="How I use GenAI tools as a CTO?"
slug="cto-ai-tools"
excerpt="I wrote an article for 'CIO News' about this topic."
tags=["tech","aieconomy","gwradio"]
type="post"
publish_at="13 Sep 23 15:06 IST"
featured_image=""
---
<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless src="https://share.transistor.fm/e/c184fcdd"></iframe>

I've used ChatGPT on and off since it came out. At first, I didn't get much success, either because I didn't know how to use it properly, or because the tool didn't work as advertised. But I kept playing with it. Over the period, as a CTO, I've added a lot of GenAI tools to my toolbox. I will talk about three of them in this episode.

Venture capitalist Marc Andreessen asked Steve Jobs, right before the first iPhone was launched, "Will people let go of the dominant physical keypads for touchscreen keypads?" Steve Jobs answered they would learn. As Mark Anderson says in the introduction to the book, "Breaking Smart", **people adapt to new technology and then forget they changed**. I feel the same way about ChatGPT. We'll get used to the tool, become better at writing prompts, and then it'll become the norm.

The most effective way to use ChatGPT is to **master prompt writing**. A good prompt commands chatGPT to act like it's playing a particular role instead of just saying, "Give me x.". Think about it as a conversation with a virtual assistant. If you give the virtual assistant as much context as possible, it will be able to help you out.

As a CTO, I create **policy documents, process documents, and other documents**. I have starting trouble. A blinking cursor on a blank screen freezes me. In the beginning, I asked ChatGPT to develop a BCP document, and the answer was terrible. After that, I learned to be a lot more verbose. "Act as the chief technology officer of an e-commerce company, whose infrastructure is on AWS with an EC2 instance connected to an RDS. Given this context, develop a table of contents for a business continuity plan for this business." Then ChatGPT gave more relevant sections, so I iterated and modified the TOC. While Google is keyword-driven, **ChatGPT is conversational**.

I've also learned you can't just trust the output from ChatGPT. You've got to validate it. Here's where your experience comes in. You can't make a useful document if you don't know anything about the topic.

Second, I'm using AudioPen. I'm actually dictating this essay to AudioPen. AudioPen is like your **virtual stenographer**, taking notes as you dictate. AudioPen cleans up the language and writes in the style you want. So you can say, "Write it in my style" or "Write it in simple, clear language without any jargon." And then AudioPen will rewrite it for you. So it is a fantastic tool for me while writing articles like this. I get a lot of ideas while I'm walking. Earlier, I had to stop and take notes in an app or hope those ideas would come to me when I'm back at my desk. Nowadays, I just dictate to the Audiopen app. AudioPen has become a tool I use almost daily.

Thirdly, I use video highlighter. I've got a lot to learn as a CTO. Most of this learning comes from recorded webinars. Any webinar would have many useless segments. I usually get 15-20 minutes of useful stuff from an hour-long video. Earlier, I used different tactics like playing the video at 2x speed and reverting to the default speed for interesting segments. Video highlighter gives you a **neat summary of the video, timestamped segments, and a summary of each segment**. Then I can skim through the highlights and listen to what I want. It has really helped me learn from YouTube videos.

I use Chat GPT and other generative AI tools as virtual assistants or virtual stenographers, which help me learn and produce things more easily. I use them almost every day as my virtual productivity assistant. What tools do you use? How do you use them? Let me know via email or in the comments.

Thank you, Have a life of WINS.

You can read the article at [CIO News](https://cionews.co.in/how-i-use-genai-tools-as-a-cto/).

_This is part of [Short notes on AI Economy](/ai-economy-notes/)_