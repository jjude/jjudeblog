---
title="Ritika Singh on 'Words Change Lives'"
slug="ritikas"
excerpt="We have heard 'Words have power'. Today's guest is an embodiment of that phrase. She has risen from ashes with words. I hope you enjoy my conversation with my good friend, Ritika."
tags=["gwradio"]
type="post"
publish_at="16 Nov 21 06:00 IST"
featured_image="https://cdn.olai.in/jjude/ritikas.jpg"
---

<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless src="https://share.transistor.fm/e/23c4611d"></iframe>
    
- Significance of the word "sapiosexual"
- Her father's influence on her love of books
- Role of books in shaping her life
- Struggles in starting a business
- Importance of a mentor especially when one is vulnerable
- Treating CEO of the company and gatekeeper of the company with respect
- Spending "thinking" time to shape the future
- Networking with grace and poise

![Ritika Singh, Founder of Kontent Factory](https://cdn.olai.in/jjude/ritikas.jpg)

### Connect with Ritika:

- Twitter: https://twitter.com/ritikasingh75
- LinkedIn: https://www.linkedin.com/in/ritikasingh75/

## Connect with me
- Twitter: https://twitter.com/jjude
- LinkedIn: https://www.linkedin.com/in/jjude/
- Website: https://jjude.com/
- Newsletter: https://jjude.com/subscribe/
- Youtube: https://youtube.com/gravitaswins
- Email: podcast@jjude.com
- Executive Coaching Program: https://gravitaswins.com

## Your feedback counts

Thank you for listening. If you enjoy the podcast, would you please leave a short review on Apple podcast or on YouTube? It takes less than 60 seconds, and it really makes a difference in finding this podcast. And it boosts my spirits. 😀

I would like to hear what resonated the most with you in this episode. Either you can email me or share it on social media and tag me. Thank you for your support.

Have a life of WINS.

## Amazing tools I use:
- I record all the remote interview with [Riverside.fm](https://riverside.fm/?via=joseph-jude)
- I use [Transistor.fm](https://transistor.fm/?via=jjude) for hosting these podcast episodes

_Gravitas WINS Radio is produced by [Joseph Jude](https://jjude.com/). Be sure to subscribe on [Apple Podcast](https://podcasts.apple.com/in/podcast/gravitas-wins-radio/id1562802449), [Spotify](https://open.spotify.com/show/7LOBjAyjBkechMC0xMIj9E),[Google Podcast](https://podcasts.google.com/feed/aHR0cHM6Ly9mZWVkcy50cmFuc2lzdG9yLmZtL2dyYXZpdGFzLXdpbnMtcmFkaW8=),[YouTube](https://www.youtube.com/gravitaswins), or the podcasting app of your choice._

Please check out other episodes of [Gravitas WINS Radio](/gwr-themes/). For easy consumption, they are categorized by themes. If you are a fellow podcaster, you might be interested in [How I Podcast](/how-i-podcast/) or [tools I use](/i-use/).

## Continue Reading
- ['Inspirations Don't Die' with Sukhwinder Singh](/sukhwinders/)
- [Culture is what you do, not what you believe](/culture-is-doing/)
- [Book Notes - Smartcuts](/smartcuts/)
