---
title="If Church Can Reinvent Itself, Why Can't Companies?"
slug="church-reinvents"
excerpt="Despite doom-sayers, the Church has survived thousands of years because it reinvented. Why can't companies survive the changing environment?"
tags=["coach","startup","faith"]
type="post"
publish_at="25 Dec 13 16:30 IST"
featured_image="https://cdn.olai.in/jjude/church-reinvents-gen.jpg"
---


I just returned from Christmas mass. Despite the raise of atheism in India, the Church was full. In fact the Church was so full that people have to be accommodated outside.

The Christmas message was about preventing female infanticide and frugality tied back to the birth of Christ. There was prayer for soldiers stationed at Siachen, and for youth to get out of drug addiction. Both the message and prayers were relevant to the society.

As I was listening to the message, I was stuck with the realization that the Catholic Church is thousands of years old and spread globally, in some countries to every village. It has stayed relevant to whatever society it served.

If you study history of Catholic Church, you will know that just forty years back, the mass used to be in Latin and priest was facing away from people.

The Church changed with times.

The format and medium of message was tailored to changes in society. In some cases even the message was customized. It has modernised itself so much that the Pope tweets.

It is ironic that an organization as old and big as Church can stay relevant for thousands of years, but companies die in 20 - 40 years.

My first mobile was from Nokia. Later, I got a Blackberry from the company. They are almost gone from the scene. Same is true of Kodak.

I know only two companies that reinvented and stayed through generations[^1]. One is GE and another IBM (there is also Apple, but it is still just a generation old).

Why do companies die, when the Church can reinvent?

Is it ego? Or is it hubris?

Whatever it is, surely companies can [learn][2] from the Church.

## Continue Reading
- [Can Companies Learn From Religions?](/can-companies-learn-from-religions/)
- [Little Talents](/little-talents/)
- [What are your mantras for life?](/life-mantras/)


[2]: /can-companies-learn-from-religions/

[^1]: A generation is forty years.

