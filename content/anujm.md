---
title="Anuj Magazine on 'Sketchenoting And Learnings'"
slug="anujm"
excerpt="How to get started with sketchnoting? What tools to use? Here is a conversation with a sketchnote artist I admire."
tags=["gwradio"]
type="post"
publish_at="05 Oct 21 09:17 IST"
featured_image="https://cdn.olai.in/jjude/anujm-gen.jpg"
---

<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless src="https://share.transistor.fm/e/16875229"></iframe>

- What is the process of sketchnoting?
- When you read a book or a twitter thread how do you decide what to leave out and what to include in the sketch?
- How has sketchnoting helped you?
- What tools do you use to sketch?
- Can anyone learn to sketchnote?
- What are the characteristics of a great leader?
- What is the kindest thing some one has done for you?

### Watch on YouTube

<iframe width="560" height="315" src="https://www.youtube.com/embed/WdviASMKI8s" title="Sketchenoting And Learnings" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Connect with Anuj Magazine:

- Twitter: https://twitter.com/anujmagazine
- LinkedIn: https://www.linkedin.com/in/anujmagazine/

## Connect with me
- Twitter: https://twitter.com/jjude
- LinkedIn: https://www.linkedin.com/in/jjude/
- Website: https://jjude.com/
- Newsletter: https://jjude.com/subscribe/
- Youtube: https://youtube.com/gravitaswins
- Email: podcast@jjude.com
- Executive Coaching Program: https://gravitaswins.com

## Your feedback counts

Thank you for listening. If you enjoy the podcast, would you please leave a short review on Apple podcast or on YouTube? It takes less than 60 seconds, and it really makes a difference in finding this podcast. And it boosts my spirits. 😀

I would like to hear what resonated the most with you in this episode. Either you can email me or share it on social media and tag me. Thank you for your support.

Have a life of WINS.

_Gravitas WINS Radio is produced by [Joseph Jude](https://jjude.com/). Be sure to subscribe on [Apple Podcast](https://podcasts.apple.com/in/podcast/gravitas-wins-radio/id1562802449), [Spotify](https://open.spotify.com/show/7LOBjAyjBkechMC0xMIj9E),[Google Podcast](https://podcasts.google.com/feed/aHR0cHM6Ly9mZWVkcy50cmFuc2lzdG9yLmZtL2dyYXZpdGFzLXdpbnMtcmFkaW8=),[YouTube](https://www.youtube.com/gravitaswins), or the podcasting app of your choice._

Please check out other episodes of [Gravitas WINS Radio](/gwr-themes/). For easy consumption, they are categorized by themes. If you are a fellow podcaster, you might be interested in [How I Podcast](/how-i-podcast/) or [Tools I Use](/i-use/).

## Continue reading
- ['Mindset for Creator Economy' with Tanmay Vora](/tanmay-creator-economy/)
- [‘Secrets of A Master Storyteller’ with Jayaram Easwaran](/jayarame/)
- [Winning With Ideas](/winning-ideas/)
