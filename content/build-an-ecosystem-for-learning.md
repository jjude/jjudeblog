---
title="Build An Ecosystem For Learning"
slug="build-an-ecosystem-for-learning"
excerpt="I explain the framework I have defined to assist my self-learning. I list the tools that help me to learn."
tags=["sdl","frameworks","wins","insights","systems"]
type="post"
publish_at="17 Mar 11 16:30 IST"
featured_image="https://cdn.olai.in/jjude/consume-produce-engage.png"
---
Our school curriculum is built on a culture of consumption. Students read text books, listen to lectures and attend workshops - all designed towards **consumption based learning**. Even the exam system is designed to test the ability of students to consume rather than their ability to produce. When they graduate into corporate world they bring with them this culture of consumption. And unfortunately academics, to serve their selfish purpose, fight to subdue corporate world with certification programs which are also primarily based on consumption.

As contours of knowledge continue to expand, consumption based learning will remain a key element in learning process. But it is only one side of learning.

Next leg of learning is '**Learning by producing**'. We learn the best when we apply the consumed knowledge to create something of value - a blog post, a research report or a mobile application.

Why is it the best form? Because it forces us to synthesize our learning from various domains. A well developed mobile application combines lessons from user interface design, algorithms, code development, product deployment and not forgetting the customer service. Similarly to produce a research report, one must combine analytical, communication and presentation skills.

While producing we pick and choose the needed and leave the rest. We may realize, as we create, the inadequacy of our understanding and go back to the original reference. Only, this time the study will be with reference to the context and thus crystallizing the learning.

The final leg of learning is **engaging with others**.

Consumption and production are private activities. Even if a group is involved in producing, until the product is shared, it remains private. By engaging others we gain their perspectives, and in the process we get our ideas validated.

In many cases, the product will be enhanced by the ensuing conversation. Twitter, in its original avatar, didn't have retweets and mentions. It was added after observing tweet stream of users. In cases, where the conceptual understanding was incorrect, the product has to be abandoned or modified completely. Most of Google's social ventures had to be abandoned because it didn't get it correctly.

Building a holistic model for learning is just the first step. To practice this model, relevant tools and techniques should be in place. They should be chosen and tailored according to the need. Ecosystem of software developer will be very different from that of a sales manager.

Here is the ecosystem that I have built for learning about problem solving, change management and other related topics.

![Ecosystem for learning](https://cdn.olai.in/jjude/consume-produce-engage.png)

(_All these tools are based on Mac OS; however, same or similar tools are available in other platforms_)

**Consume** : Paper books still form a large part of my learning. They are easy to carry, and it is convenient to highlight important concepts and jot notes. Next to books, I depend on large number of blog feeds (about 200) for knowledge. I use [NetNewsWire](http://netnewswireapp.com/ "NetNewsWire") on the desktop and [gReader](http://groups.google.com/group/g_reader) on the mobile for RSS reading. They sync with Google Reader and I can mark individual posts as favorites to be stored for filing. I use [Mendeley](http://www.mendeley.com/) for reading pdf documents (including e-books), since it makes annotating and cataloguing simple and easy. Questions, insights, notes are marked in the books or annotated in the pdf documents themselves. I'm still looking for a better way to annotate RSS feeds and store them offline.

**Produce** : I use host of application for this purpose - [OmniGraffle](http://www.omnigroup.com/applications/omnigraffle/ "OmniGraffle") for drawing, [Jing](http://www.techsmith.com/jing/) for taking screenshots, [Pencil](http://www.pencil-animation.org/) for sketches and [MindNode](http://www.mindnode.com/ "MindNode") for drawing mind maps. These are then imported into [Evernote](http://www.evernote.com "Evernote") while writing drafts. Its a feature rich application with hierarchical notes and tagging. Insights while on the transit are noted into Evernote in the mobile and then synced with the desktop application.

**Engage** : [Wordpress](http://wordpress.org/) is the platform I use for blogging and engaging the readers. Since there is no equivalent to Microsoft LiveWriter in Mac, I use Wordpress Editor itself for creating blog post (ideally, I would like to publish from Evernote itself). Though there are add-ons like Disqus commenting system, I try to avoid complex systems wherever possible and hence gone with the simple Wordpress comment system itself. Blog posts are automatically posted to Twitter and [LinkedIn.](http://www.linkedin.com "LinkedIn") However, I prefer Q&A in LinkedIn than conversations in Twitter.

I wish I had this insight long back. In fact, I wonder why such an ecosystem is not promoted within schools.

**Have you figured out an ecosystem for learning?**
