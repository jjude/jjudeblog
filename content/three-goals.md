---
title="Three Types Of Goals You Should Set"
slug="three-goals"
excerpt="'Be' goals drive 'do' goals which manifests in 'have' goals"
tags=["productivity","wins","coach"]
type="post"
publish_at="13 Oct 21 14:23 IST"
featured_image="https://cdn.olai.in/jjude/three-goals.jpg"
---

![](https://cdn.olai.in/jjude/three-goals.jpg)

We all set goals. 

- Save 1 crore in next 5 years
- Lose 5 kgs in this quarter
- Create a network of 1000 people

They are **"have" goals** - what we want to have. These goals have all the attributes of SMART goals - specific, measurable, achievable, relevant, and time-bound. Since we are taught to have big audacious goals, some of these could be far beyond what we can achieve. But that is not why we don't achieve these "have" goals.

If we have to achieve our big audacious "have" goals, we need to **match them with "do" goals**, which most of us miss. 

What is the benefit of "do" goals?

You can **review "do" goals at regular intervals and adjust your action**. What are "do" goals?

- Invest ₹15000 per month for the next 15 years in an index fund
- Call 2 persons per week
- Jog 5 km daily for 3 days a week

You can sit down on a Sunday and ask yourself, did I call 2 people? If not why? What should I do next week?

Matching "have" goals with "do" goals are better. But that is not the complete picture.

Doing something on a shiny day is easy. It becomes challenging when the going gets tough, which it will, without fail.

When the "do" goals are aligned with who you are then it becomes easy. When the "do" goals are not in harmony with your beliefs you'll find excuses not to do it or you'll do it half-heartedly.

Say you believe one can't make a million-dollar honestly and you also believe you are an honest person. Then, do you think you'll ever take steps to be wealthy? Will you invest in good quality companies that grow as well as pay dividends? If you do, you're only setting yourself up for cognitive dissonance.

If you are not frugal and think long-term, you'll not invest ₹15000 every month. You'll think you are not enjoying life as much as you can because you're investing a big part of your salary. 

**Who you are, are the "be" goals**. We don't focus on "be" goals. "Be" goals drive "do" goals, which help us to achieve our "have" goals.

You've to be generous and self-disciplined if you want to call 2 persons weekly and build a network.

Next time, when you sit down to set goals, drill down into who you should be to achieve the "have" goals. 

Be ambitious in being and doing, then your having goals are inevitable.


## Continue Reading

* [Dichotomy of contentment and ambition](/contentment-and-ambition)
* [Culture is what you do, not what you believe](/culture-is-doing)
* [Approximately correct](/approximate/)