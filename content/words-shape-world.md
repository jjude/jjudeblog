---
title="Language Shapes Your World"
slug="words-shape-world"
excerpt="Learn more than one language, to carve a better worldview"
tags=["coach","insights"]
type="post"
publish_at="02 Dec 20 08:00 IST"
featured_image="https://cdn.olai.in/jjude/words-shape-world-gen.jpg"
---


How do you say, "I'm sick" in Spanish? "Estoy enfermo."
How do you say, "I'm black" in Spanish? "Soy negro" 

(Both are masculine versions)

When we speak in English, we don't differentiate between a temporary condition, like being sick, and a permanent condition, like being black. But if you want to speak in Spanish, you have to. The Spanish language trains your mind to think of the duration of the condition. 

I'm a Tamilian. In Tamil, we would say, "the thorn pricked me," as if the thorn had a vengeance against me to take a life of its own to haunt me with a prick. I guess it is the same in other Indian languages. That is why we, Indians, won't take responsibility for coming late to meetings. The excuses are always external: the bus came late, the car broke-down, or there was a traffic jam. It is never, "I didn't plan to come on time."

The English take responsibility when they say, "I ran into the thorn." When you take responsibility, you can improve your life because you hold yourself and others accountable. You believe it is not karma but your actions that shape your world.

The attitude of responsibility can also make you pompous. I'm the center of the English world. I planned, I worked hard, and hence I won. You are a pathetic loser because you didn't work hard. So what, your father died when you were young, or the family debt fell on your shoulders, or you lost your job due to covid. None of those things matter. If you had the foresight, you could've overcome any challenge. Look at me; I was once driving a cycle to the office; now I drive a Benz car. All because of me, me, and me.

The Easterners don't think the world revolves around them. When they say the thorn pricked me, they acknowledge there are forces beyond their control in this world. I didn't plan to get pricked, but it happened. So if and when they win, they humbly attribute their success to God or others. They don't humiliate the not-so-fortunate because there are forces beyond one's control that provide opportunities.

Every language has its fine points and flaws. So if you want to carve a better worldview, you can do three things.

### Learn at least two languages

> To have another language is to possess a second soul -  Charlemagne.

Try to learn as many languages as possible, if it is not at least two. It is better if they have different roots.

Learn the language to the extent you can think in it rather than translating from your mother tongue. Only then you'll take advantage of its peculiarities.

### Increase vocabulary 

> A cup of coffee is not the same as a coffee cup - Unknown

When you know more words, you can describe events, situations, and feelings with more precision. Precision will sharpen your understanding and help you have richer conversations.

### Empathize

> As many languages you know, as many times you are a human being - Czech proverb

When you attempt to learn a language other than your mother tongue, you will stumble, which is a good thing. Then you'll empathize with others trying to learn a new language. You won't chuckle when someone pronounces "facade" as "fakade," because they can't differentiate "ca" in "car" and in "facade" (and English is a weird language).

We speak out of the abundance of our hearts. Our languages determine what fills our hearts.

## Continue Reading 

* [Abstractions are useful, if you know the context](/abstractions-fail)
* [Dichotomy of contentment and ambition](/contentment-and-ambition)
* [Numb is dumb](/numb-is-dumb)

