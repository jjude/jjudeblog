---
title="Three types of mistakes leaders keep making"
slug="three-types-of-mistakes"
excerpt="All mistakes are tactical or behavioral or psychological mistakes"
tags=["wins","coach"]
type="post"
publish_at="19 Oct 21 21:09 IST"
featured_image="https://cdn.olai.in/jjude/three-mistakes.jpg"
---
I asked my friends to list the mistakes leaders commit. I got this [list][1]:

- Centralizing decision making
- Non-transparency of work within the team  
- Taking individual decisions without knowing all facts
- Not learning from failure
- Overburdening the team
- Delegating too much or not delegating at all
- Making commitments without consulting the team
- Assuming people issues within the team will absolve on their own
- Being quick to reprimand and never/rarely appreciating
- Providing solutions instead of making the team think.
- Expecting every team member to be their carbon copies
- Become a bottleneck for all decisions

These and other mistakes leaders make can be categorized into three types:

- tactical
- behavioral
- psychological

![](https://cdn.olai.in/jjude/three-mistakes.jpg)

### Tactical

Mistakes in tactics are easily visible and easy to correct. Most coaching programs focus solely on correcting tactical errors. 

Budgeting and scheduling errors are execution mistakes. These tactical mistakes can be corrected or contained with the use of checklists, processes, and audits.

Sometimes these tactical errors are not the result of ignorance or incompetence. Often, they stem from underlying beliefs, like low self-esteem. If this is the case, they are never corrected. Thus, a leader will make those tactical errors in the future.

### Behavioral

We make behavioral mistakes when interacting with others. Training can help to fix some of them.

Let's look at an example. At first, developers will expect logical answers in sales calls, not understanding that sales is more about emotion and narration. Role-plays can be used to highlight and correct most behavioral errors.

### Psychological

Psychological errors are really errors of perspective. These errors are caused by our worldviews and beliefs. Since they're hidden, they're hard to catch and coach, but they guide all our actions, so mistakes of this nature are destructive to individuals and organizations.

### Beware of the hidden traps

Whenever you or your team makes a mistake, reflect on whether it could be due to a mistake of perspective. You may need a coach.


[1]: https://www.linkedin.com/posts/jjude_engineering-management-gravitaswins-activity-6853994982580252672-W8It

## Continue Reading

* [Great Leaders Are Invisible](/great-leader/)
* [How to avoid failures with thirty percent feedback?](/30-percent/)
* [Beware of Cloning Best Practices](/beware-of-cloning-best-practices/)
