---
title="Who are you chasing?"
slug="who-chase"
excerpt="When we constantly run on the treadmill of comparison, we work under pressure and live without leisure, making our everyday lives constant torture."
tags=["coach","wins"]
type="post"
publish_at="08 Aug 22 17:55 IST"
featured_image="https://cdn.olai.in/jjude/who-you-chase.jpg"
---

> "Half of life is lost in charming others.   The other half is lost in going through anxieties caused by others. Leave this play. You have played enough." - Rumi

Who you chase determines whether you suffer anxiety attacks or prosper with peace in your heart.

We are conditioned by our parents and society from our earliest days to compare ourselves to others.
  
How often have you heard variations of these?  
  
"When your sister scores first rank, why can't you study well and score the first rank?"   
"Your English is terrible. I sent you to the same school as your cousin. Her English is fluent."    
"Your brother eats everything I give him. You are always a pain. Chicken, beans, eggs, you reject them all. There's no way you'll ever be as strong as your brother."
  
Teachers take over from parents when we enter school.
  
"Mahesh is such a quiet boy. Why can't you all be like him?"   
"It would be wonderful if all my students could perform like Geetha who scored the highest in all her exams."
  
As we grow up in such an environment, we often desire what others have, whether it's a job, house, car, or even a spouse. 

The Internet has only amplified comparisons. There are photos from exotic locations, pay-hike announcements, images of new cars our friends bought, and other things we see on our feeds each day. We conclude our lives are banal after that.

"Comparison is the thief of joy," said Theodore Roosevelt. When we constantly run on the treadmill of comparison, we work under pressure and live without leisure, making our everyday lives constant torture.

Is there an alternative?

![Who you chase](https://cdn.olai.in/jjude/who-you-chase.jpg "Who you chase")


Matthew McConaughey [narrates](/video-3-daily-needs/) an alternative view, in his Oscar acceptance speech. He started the speech with these words:

> There are three things that I need each day. One, I need something to look up to, another to look forward to, and **another is someone to chase**.

Then he went on to say, who he chases.

> And to **my hero, that’s who I chase.** … every day, every week, every month, and every year of my life, my hero is always 10 years away. I’m never going to beat my hero. I’m not going to obtain that, I know I’m not. And that’s just fine with me because that keeps me with somebody to keep on chasing.


His hero is him in 10 years.

What are the benefits of chasing a better version of yourself?

Firstly, everyone fades out of your vision. What they have, what they chase, or what they say does not bother you. That alone is a huge relief.

You can look inside yourself to find out what you truly desire to be, do, and have. You drew those aspirations from within, so they reflect you. Hence, the pursuit has meaning.

Finally, when you reach your milestones, you have the satisfaction of reaching something you set for yourself, and if you're not satisfied, you can always correct and chase again.

You can put this approach into practice by following these two steps:

1. We can live a potent life rather than a palatable one when we have an end in mind. Visualize your funeral, as Steven Covey recommends in his book "7 habits." Write down what you want your spouse, children, and closest friends to say on that day. In writing your obituary, you will discover your life's values.
2. Discovering your values does not mean giving up the passions of the world, but rather finding the ones that matter to you. Once you have them, set three types of goals - Be, Do, and Have. "Have" goals are what you want to possess. Possessions won't just appear out of nowhere. Acquiring them requires action on your part. That's what "do" goals are. You need to "be" certain things to carry out those actions consistently and persistently. Pursuing those goals also transforms you. Those are the "be" goals.

While you may not be able to completely avoid comparing with others, carving your own path allows you to be ambitious and content as you live your life everyday.

_Thanks to [Rishabh Garg](https://rishabhgarg.me) for reading a draft of the essay and helping me to fine-tuning it._

## Continue Reading

* [The dichotomy of contentment and ambition](/contentment-and-ambition/)
* [Three Types Of Goals You Should Set](/three-goals/)
* [Build on the rock for the storms are surely coming](/build-on-rock/)