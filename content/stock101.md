---
title="Fundamentals of stock market investing"
slug="stock101"
excerpt="Understand the basics of investing in Indian stock markets"
tags=["wealth"]
type="landingpg"
publish_at="23 Mar 23 16:00 IST"
featured_image=""
---

<!DOCTYPE html>
<html lang="en">
  <title> </title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://unpkg.com/tachyons/css/tachyons.min.css">
  <body>
    <section class="hero pa4 pa5-ns">
      <div class="mw8 center">
        <h1 class="f2 f1-m f-headline-l lh-solid mb0">Learn to Invest in the Stock Market</h1>
        <p class="f5 f4-m f3-l lh-copy measure mt0">Take our online course and start making smarter investment decisions today.</p>
        <a class="f6 link dim br2 ph4 pv3 mb2 dib white bg-dark-blue" href="#enroll">Enroll Now</a>
      </div>
    </section>
  </body>
</html>


