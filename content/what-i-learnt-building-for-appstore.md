---
title="What I Learned Building For AppStore"
slug="what-i-learnt-building-for-appstore"
excerpt="Eight key lessons from building and selling Mac & iOS apps."
tags=["coach","startup","insights","sdl","produce"]
type="post"
publish_at="12 Oct 13 16:30 IST"
featured_image="https://cdn.olai.in/jjude/what-i-learnt-building-for-appstore-gen.jpg"
---

I joined the AppStore a year ago and released a Mac and an iOS app (_since then it is discontinued_). I joined AppStore, to learn first-hand, the process of product development and all associated activities in building a product business. It has been a fantastic learning.

Here are the eight points that I learned:

1.  <a id="lesson1"></a>**AppStore is great for users not for business** : There is a stringent approval process to get apps into AppStore, which means users can download apps with confidence without worrying about vulnerabilities. Secondly, users are abstracted from sellers which means no spam. These exact features, as [noted earlier](/what-an-indie-developer-expects-from-apple/), make building business around AppStore difficult.

2.  <a id="lesson2"></a>**Associated markets are bigger than development market** : As soon as BlogEasy was listed in the AppStore, even before a single copy was sold, I got lot of marketing emails advertising their services. Some offered translation services; others video marketing; some others offered paid reviews. AppStore has given a boost to developing icons, designing websites, writing books and many forms of consulting. Money from selling apps are [dwindling](http://www.marco.org/2013/09/28/underscore-price-dynamics) but apparently there is still lot of money to be made in these associated markets!

3.  <a id="lesson3"></a>**Developing a product is a sprint; building a business is a marathon** : A decade ago, building products was difficult. Now, with availability of advanced tools and high-speed internet (not to mention availability of Google &amp; Stackoverflow) it has become easy to develop products. In fact there are weekend-sprints organised across the globe to build products. Many products do get developed in these sprints. But building business is lot more than building an app. It involves marketing, selling, getting paid, and complying with umpteen regulatory requirements. These are tedious processes which can test patience of anyone. They can become and often become impediments to rate of progress.

4.  <a id="lesson4"></a>**Productivity is a function of discipline &amp; hunger** : Productivity is an important factor if you have to sustain in a fiercely competitive market as that of AppStore. [GTD](https://en.wikipedia.org/wiki/Getting_Things_Done) is probably the best and well known productivity procedure. There are tons of tools build around GTD. But tools and procedures are ineffective without discipline.

    Another aspect of productivity is you need to have a hunger, a sense of urgency. In this last year, lot of exciting things happened around me; and interesting articles, courses and books came out. I had to keep on saying [no](/no-im-developing-a-product/) and pray [solo-ISV&#39;s prayer](/no-im-developing-a-product/#solo-isv-prayer) to keep my focus on whatever I was building at that time.

5.  <a id="lesson5"></a>**Until you have audience of your own, ride on others&#39;** : When I started developing [BlogEasy](http://blogeasyapp.com), I did a search and found that although there were few popular blog editors for Mac, only [MarsEdit](http://www.red-sweater.com/marsedit/) was in AppStore. I decided to make [BlogEasy](http://blogeasyapp.com) available in AppStore and that turned out to be a good decision. I have sold 100 copies in AppStore but only one in the non-AppStore. The primary reason, I believe, is that, I took BlogEasy to the already congregated audience of AppStore. Majority of this congregated audience are willing to buy new apps. Outside AppStore there is no single place which attracts such audience. Same goes for review sites and social media. Take your product to where audience is, and then build your own audience from that larger group.

6.  <a id="lesson6"></a>**Always be shipping (coffee is for shippers)** : As an inquisitive developer, I tinker. If there are no deadlines are at sight, I will tinker forever. If you are developing a business, you should stop tinkering at some point and ship the app. You can start tinkering the next version. But ship. Let the app go out of your laptop constantly.

7.  <a id="lesson7"></a>**Marketing is hard, at least for developers** : As a developer, I lean towards certainty. I know hundred things can go wrong and will go wrong. So I like to talk about what I&#39;m working, only when I am certain. But marketing is an opposite mindset. You start talking about an idea or concept without worrying about implementation. Implementation can be done by others. As a solo developer, I don&#39;t have the privilege of that separation. I wait until I am certain but then it becomes too late. At least for the [next app,](http://memorisebible.com/) I am marketing before I start coding.

8.  <a id="lesson8"></a>**Everything is a package** : I said earlier that AppStore is hard on developers. What are the alternatives? Google Store? Nay. Amazon Store? Nope. There is a higher possibility that AppStore users becoming paying customers, than in any other store. So there is an advantage and disadvantage. Same goes for cross-platform tools like PhoneGap. They allow you to easily build for iOS and Android among other platforms. But their on-device performance is pathetic.

Learning about product business by building apps is tedious. But the lessons are invaluable. I&#39;m happy to learn this way.

