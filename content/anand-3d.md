---
title="Prakasm Anand on 'Printing hearts, parts, and home'"
slug="anand-3d"
excerpt="Will we live in a world where our home, dental capping, and even cereal are custom printed for us?"
tags=["gwradio","tech"]
type="post"
publish_at="29 Mar 22 06:00 IST"
featured_image="https://cdn.olai.in/jjude/anand-3d.jpg"
---
3D printing looks a mix of engineering and science fiction. It is one field that seems to have potential to change almost all areas of our lives. In this episode, I speak with Anand, who has been involved deeply in this field for 22 years.

<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless src="https://share.transistor.fm/e/8e3405bc"></iframe>

_Click on the ▶️ button above to listen to the episode_

### What you'll learn
- What is 3D printing and how does it work?
- How 3D printing helped treat a cancer patient?
- What are the opportunities in 3D printing?
- What is the impact of 3D printing on climate change?
- How to learn about 3D printing?

### Connect with Prakasam Anand
- LinkedIn: https://www.linkedin.com/in/prakasam-anand-a826b416/

### Resources Mentioned:
- Additive Minds Academy: https://store.eos.info/pages/additive-minds-academy
- Additive Academy: https://www.amchronicle.com/additive-academy/

![Prakash Anand about 3D Printing](https://cdn.olai.in/jjude/anand-3d.jpg "Prakash Anand about 3D Printing at Gravitas WINS Radio")

### Watch on YouTube

<iframe width="560" height="315" src="https://www.youtube.com/embed/_sN3O_tYqjs" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Connect with me
- Twitter: https://twitter.com/jjude
- LinkedIn: https://www.linkedin.com/in/jjude/
- Website: https://jjude.com/
- Newsletter: https://jjude.com/subscribe/
- Youtube: https://youtube.com/gravitaswins
- Email: podcast@jjude.com
- Executive Coaching Program: https://gravitaswins.com

## Your feedback counts

Thank you for listening. If you enjoy the podcast, would you please leave a short review on Apple podcast or on YouTube? It takes less than 60 seconds, and it really makes a difference in finding this podcast. And it boosts my spirits. 😀

I would like to hear what resonated the most with you in this episode. Either you can email me or share it on social media and tag me. Thank you for your support.

Have a life of WINS.

## Amazing tools I use:
- I record all the remote interview with [Riverside.fm](https://riverside.fm/?via=joseph-jude)
- I use [Transistor.fm](https://transistor.fm/?via=jjude) for hosting these podcast episodes

_Gravitas WINS Radio is produced by [Joseph Jude](https://jjude.com/). Be sure to subscribe on [Apple Podcast](https://podcasts.apple.com/in/podcast/gravitas-wins-radio/id1562802449), [Spotify](https://open.spotify.com/show/7LOBjAyjBkechMC0xMIj9E),[Google Podcast](https://podcasts.google.com/feed/aHR0cHM6Ly9mZWVkcy50cmFuc2lzdG9yLmZtL2dyYXZpdGFzLXdpbnMtcmFkaW8=),[YouTube](https://www.youtube.com/gravitaswins), or the podcasting app of your choice._

Please check out other episodes of [Gravitas WINS Radio](/gwr-themes/). For easy consumption, they are categorized by themes. If you are a fellow podcaster, you might be interested in [How I Podcast](/how-i-podcast/) or [Tools I Use](/i-use/).

## Continue Reading

* ['Building An Enterprise Data Strategy' with Ganesan Ramaswamy](/ganesanr/)
* ['Conversational Bots' With Liji Thomas](/lijit/)
* [How to deliver value in the digital age?](/value-in-digital-age/)