---
title="iPad App Review - Nebulous Notes Lite"
slug="ipad-app-review-nebulous-notes-lite"
excerpt="Review of an iOS text editor."
tags=["ios-apps"]
type="post"
publish_at="07 Jun 11 16:30 IST"
featured_image="https://cdn.olai.in/jjude/nebulous_lite.png"
---
One of the first apps that I downloaded into iPad is Nebulous Notes Lite and I love it.

Why?

Is it because it is free?

Is it because it has dropbox interface?

Is it because it has a full screen edit? (well almost)

Yes all of the above, but those aren't the only reasons for my &#8216;wow' feeling.

More than everything above, its the macro bar on top of the regular iPad keyboard that makes me tap it to capture notes.

The extra line of keys bring undo, redo, letter and word navigation and so on. It is so useful that, I think it should be a default in iPad.

Even though it's free, Nebulous Notes Lite lets you define your own macros and rearrange these macros (by way of export and import) to suit your need.

![Nebulous Lite](https://cdn.olai.in/jjude/nebulous_lite.png)

Another one of geeky feature that I love is it's support for [Markdown](http://daringfireball.net/projects/markdown/) tags. Now I can type blog posts in a readable format and convert to HTML when I post. This helps me concentrate on content creation rather than worry about presentation. I use TextMate on my Mac, so conversion is taken care of well by TextMate's blogging bundle.

Ever since I bought iPad, I have been compiling my blog posts solely on Nebulous Notes Lite and I observe that, my rate of getting the post out has increased.

I am so amazed by features in the free version, I might soon buy the full version. It's that good.
