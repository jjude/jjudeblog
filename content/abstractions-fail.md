---
title="Abstractions are useful, if you know the context"
slug="abstractions-fail"
excerpt="All abstractions are useful within a context. This is true in life as much as in technology."
tags=["tech","coach","insights","wins"]
type="post"
publish_at="09 Jul 18 10:30 IST"
featured_image="https://cdn.olai.in/jjude/abstractions-fail.jpg"
---
[![All Abstractions Fail](https://cdn.olai.in/jjude/abstractions-fail.jpg "All Abstractions Fail")](https://youtu.be/RAGcDi0DRtU)

Since you are reading this post, I am going to assume that you know English reasonably well. Also, I am going to assume you are a non-native English speaker like me.

English, like any other language, is an **abstraction to share ideas**.

It works well. Sitting in India, I am able to share my ideas with readers from Paris, London, and Madrid. Without this abstraction, I would have to write in French, English, and Spanish to share my ideas with readers from different regions. As an individual blogger, I can't hire that many translators.

As an abstraction, it works well in general cases. **It fails** as soon as I move out of that generic case into specific situations.

As the Finnish comedian Ismo says, you think you know English. But when you move to America, you realize you don't know English. **It takes forever to understand the meaning of those little things**, like "ass". Dumbass is just dumb, but badass is brave!

_(Watch the video by clicking on the image above. It is hilarious.)_

**Does this mean English is not useful? No.**

It is useful within a context. If you are in a different context, like a friendly chat with Americans, that context breaks down. You need a special variant of the language.

Same is true of technology.

Every technology is an abstraction. They are abstracted for a context. **When you stay within that context, the abstraction is super useful**. You enjoy using that technology. As soon as you hit the edges of that context, you start to feel frustrated. You need a technology suitable for your newer context.

At one time, you have to write software in C or in C++. Then Python came along. Python became the best language for many different tasks — scripting, web-development, desktop development, ETL, machine learning, and so on.

Python is the best language to develop a solution easy and fast. So it remains the darling of the startup world. Since 80% of startups fail, they never reach a stage to go beyond the capabilities of Python. But **when a startup becomes successful, they need capabilities beyond Python. So they usually migrate their application to some other language**. Dropbox is a case in point.

Patrick Lee of Dropbox [writes](https://blogs.dropbox.com/tech/2014/07/open-sourcing-our-go-libraries/):

> Dropbox owes a large share of its success to Python, a language that enabled us to iterate and develop quickly. However, as our infrastructure matures to support our ever growing user base, we started exploring ways to scale our systems in a more efficient manner. About a year ago, we decided to migrate our performance-critical backends from Python to Go to leverage better concurrency support and faster execution speed.

This approach is true in **developing mobile applications** too.

If you can afford or your application demands capabilities like augmented reality, you should start developing in native languages. For the rest, a cross-platform framework/library like [Ionic](https://ionicframework.com/) and [React Native](https://facebook.github.io/react-native/) are the way to go.

**With these frameworks, you can iterate and develop quickly**. If your application hits a spot where these abstractions fail, then you move out into native languages.

Airbnb is [sunsetting React Native](https://medium.com/airbnb-engineering/sunsetting-react-native-1868ba28e30a). They wrote 5-part series about their decision. If you are at Airbnb scale and  face the [organizational and technical challenges](https://medium.com/airbnb-engineering/building-a-cross-platform-mobile-team-3e1837b40a88) they face, sure, move out. Otherwise, the abstraction of React Native helps.

Whether it is English or Python or React Native, **your context should guide you to choose an abstraction. When you hit the edge of that context, evaluate again and choose another abstraction that fits your new context.**

## Continue Reading

* [Documenting Your Decisions](/documenting-your-decisions/)
* [Don't Torture Context To Fit Your Solution](/dont-torture-context-to-fit-your-solution/)
* [Are You Unlearning?](/unlearn/)
