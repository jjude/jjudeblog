---
title="Tamizhvendan on 'Teaching software fundamentals in Tamil'"
slug="tamizh-sw-funda"
excerpt="If India has to grow beyond metros, we need to teach software and business fundamentals in regional language."
tags=["gwradio","career"]
type="post"
publish_at="22 Jan 24 18:24 IST"
featured_image="https://cdn.olai.in/jjude/teaching-sw-in-tamil.jpg"
---
English has given us enormous leverage in growing our services industry. But if India has to grow beyond the creamy layer and the metro, we need to teach the same software fundamentals in vernacular languages. If you look at Germany, Japan, and Russia they teach in their mother-tongues. 

My guest today, Tamizhvendan is doing exactly that. True to his name and combining his profession, he is teaching software fundamentals in Tamil. We are discussing the challenges and impact of his teaching. If India has to grow multi-fold as we project, we need more such educators. Hope today's discussion is a small contribution in that journey.

<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless="" src="https://share.transistor.fm/e/6e40db88"></iframe>

## What you'll hear

- Origin story of Tamizh's venture
- What Tamizh teaches?
- What kind of students attend these sessions?
- Why Tamizh started with a podcast?
- Challenges in teaching in Tamil
- Focus for this year
- Rapid-fire questions

## Edited Transcript

_coming soon_

## Watch on YouTube

<iframe width="560" height="315" src="https://www.youtube.com/embed/E2RJNGnDYag" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

## Connect with TamizhVendan
- LinkedIn: https://www.linkedin.com/in/tamizhvendan/

## Connect with me
- Twitter: https://twitter.com/jjude
- LinkedIn: https://www.linkedin.com/in/jjude/
- Website: https://jjude.com/
- Newsletter: https://jjude.com/subscribe/
- Youtube: https://youtube.com/gravitaswins
- Email: podcast@jjude.com
- Executive Coaching Program: https://gravitaswins.com

## Your feedback counts

Thank you for listening. If you enjoy the podcast, would you please leave a short review on Apple podcast or on YouTube? It takes less than 60 seconds, and it really makes a difference in finding this podcast. And it boosts my spirits. 😀

I would like to hear what resonated the most with you in this episode. Either email me or share it on social media and tag me. Thank you for your support.

Have a life of WINS.

_Gravitas WINS Radio is produced by [Joseph Jude](https://jjude.com/). Be sure to subscribe on [Apple Podcast](https://podcasts.apple.com/in/podcast/gravitas-wins-radio/id1562802449), [Spotify](https://open.spotify.com/show/7LOBjAyjBkechMC0xMIj9E),[Google Podcast](https://podcasts.google.com/feed/aHR0cHM6Ly9mZWVkcy50cmFuc2lzdG9yLmZtL2dyYXZpdGFzLXdpbnMtcmFkaW8=),[YouTube](https://www.youtube.com/gravitaswins), or the podcasting app of your choice._

Please check out other episodes of [Gravitas WINS Radio](/gwr-themes/). For easy consumption, they are categorized by themes. If you are a fellow podcaster, you might be interested in [How I Podcast](/how-i-podcast/) or [Tools I Use](/i-use/).

## Continue Reading

- [Ubellah Maria on 'Transitioning From Developer To Manager'](/ubellah-maria-becoming-a-manager/)
- [Meenakshi on 'Carving a unique career path'](/meenakshi-career/)
- [Mohan Mathew on 'Management Consulting'](/mohanm/)