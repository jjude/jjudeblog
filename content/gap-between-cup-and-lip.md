---
title="Long gap between cup and lip"
slug="gap-between-cup-and-lip"
excerpt="Three things you need to make your dream a reality"
tags=["coach","wins"]
type="post"
publish_at="17 Jun 22 09:02 IST"
featured_image="https://cdn.olai.in/jjude/dreams-to-reality.jpg"
---
![What you need to turn dreams into reality](https://cdn.olai.in/jjude/dreams-to-reality.jpg)

Everybody has dreams. Dreams, for many of us, become regrets rather than realities.

Why?

I see three reasons.

- Lack of knowledge
- Lack of resources
- Lack of will

Let us unpack each of them.

### Lack of knowledge

Let's say you have a dream of becoming an independent consultant. For that dream to become a reality, you need a guide that shows you the steps to take from where you are to where you want to be. The guide should assist you in answering the following questions:

- How to market my services?
- How much should I charge?
- How to find clients?

Even though you can read books and watch videos, coaching is the best way to fill the knowledge gap. Especially from someone who is a few steps ahead.

Knowledge is power. But it is only the beginning.

### Lack of resources

Maybe you want to take advantage of the trend of working from anywhere to make your dream of living in the mountains come true.

No matter how much knowledge you have about remote work, you'll still need dependable infrastructure like high-speed Internet to turn it into a money-making opportunity.

Knowledge remains merely a dot in your brain without the necessary resources.

Oftentimes, we believe that only money stands between us and our dreams. Money is important, but we also need the right infrastructure and even the right regulations from the government.

### Lack of will

You will find a way to realize your dream if it becomes a hunger in your soul. Otherwise, it will remain a wish. Full of lame excuses.

You have to want it with all your heart, soul, and mind. When you can't stop thinking about it when you are awake and when you are asleep, that is when you'll find a way to turn that dream into reality.

In [Gravitas WINS](/gwcourse/), I teach you how you can gather insights and resources to turn your dream into reality. If you want to be among like-minded individuals, join the upcoming cohort.


## Continue Reading

* [Are You Chasing Success Or Status?](/status-or-success/)
* [First Bullets. Then Cannonballs](/bullets-cannonballs/)
* [Embrace The Unknown](/embrace-the-unknown/)