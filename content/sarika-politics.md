---
title="Dr Sarika Verma on 'Why politics is important for all of us'"
slug="sarika-politics"
excerpt="How can common people participate and improve politics in our country."
tags=["gwradio"]
type="post"
publish_at="18 Apr 23 06:00 IST"
featured_image=""
---

Politics touches every aspect of our lives - curriculum we study, roads we drive, businesses we can setup, interest rates we get, and nowadays, even the dress we can wear and food we can eat.  
  
But most of us keep away from politics saying one excuse or the other. And then we lament the state of the country.

I am excited to discuss with a first generation politician Dr Sarika Verma about this topic. She is a practicing doctor and a spokesperson for AAP in Haryana. Even though my guest is from a particular political party, this episode is not about a particular ideology. We talk about politics in general and what we as common people can do to participate and improve politics in our country. Enjoy our conversation.

<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless src="https://share.transistor.fm/e/0209863a"></iframe>

## What you'll hear
- How Dr Srika started in politics?
- Why Dr Sarika joined politics?
- Managing politics and other responsibilities
- Alternative to bigotry and corruption
- Common people should take a stand
- How can common people hold politicians accountable?
- Can politics & ethics go together?
- Can politics be a viable career option?
- Skills needed for politics
- How politicians have fun?
- Technology & Politics
- Kindest thing people have done
- Living a good life
- What is leadership & who has exhibited it?

## Connect with Dr Sarika Verma
- Twitter: https://twitter.com/Drsarika005
- LinkedIn: https://www.linkedin.com/in/dr-sarika-verma-b8970734/
- Facebook: https://www.facebook.com/AllergyDocIndia
- Instagram: https://www.instagram.com/drsarika005/

## Connect with me
- Twitter: https://twitter.com/jjude
- LinkedIn: https://www.linkedin.com/in/jjude/
- Website: https://jjude.com/
- Newsletter: https://jjude.com/subscribe/
- Youtube: https://youtube.com/gravitaswins
- Email: podcast@jjude.com
- Executive Coaching Program: https://gravitaswins.com

## Your feedback counts

Thank you for listening. If you enjoy the podcast, would you please leave a short review on Apple podcast or on YouTube? It takes less than 60 seconds, and it really makes a difference in finding this podcast. And it boosts my spirits. 😀

I would like to hear what resonated the most with you in this episode. Either you can email me or share it on social media and tag me. Thank you for your support.

Have a life of WINS.

## Amazing tools I use:
- I record all the remote interview with [Riverside.fm](https://riverside.fm/?via=joseph-jude)
- I use [Transistor.fm](https://transistor.fm/?via=jjude) for hosting these podcast episodes

_Gravitas WINS Radio is produced by [Joseph Jude](https://jjude.com/). Be sure to subscribe on [Apple Podcast](https://podcasts.apple.com/in/podcast/gravitas-wins-radio/id1562802449), [Spotify](https://open.spotify.com/show/7LOBjAyjBkechMC0xMIj9E),[Google Podcast](https://podcasts.google.com/feed/aHR0cHM6Ly9mZWVkcy50cmFuc2lzdG9yLmZtL2dyYXZpdGFzLXdpbnMtcmFkaW8=),[YouTube](https://www.youtube.com/gravitaswins), or the podcasting app of your choice._

Please check out other episodes of [Gravitas WINS Radio](/gwr-themes/). For easy consumption, they are categorized by themes. If you are a fellow podcaster, you might be interested in [How I Podcast](/how-i-podcast/) or [Tools I Use](/i-use/).

## Continue Reading

- [Emmy Sobieski on 'I'd far rather be lucky than smart'](/emmys/)
- [Ritika Singh on 'Words Change Lives'](/ritikas/)
- [Liji Thomas on 'Conversational Bots'](/lijit/)