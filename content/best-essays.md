---
title="Best Essays Online"
slug="best-essays"
excerpt="The essays that shape my thinking"
tags=["wins","insights"]
type="post"
publish_at="18 Jun 20 16:00 IST"
featured_image="https://cdn.olai.in/jjude/best-essays-gen.jpg"
---


> Don't ask me who's influenced me. A lion is made up of all the lambs he's digested. - Giorgos Seferis

Though I often search for "how-to" articles, the best ones I keep revisiting are those that are describing an underlying principle. I store them, make notes, and read them again and again, so they become part of me.

Here are some of those essays that have become part of me. I will keep adding to the list. So bookmark them or, better yet, subscribe using the below form.

[Untangling Skill and Luck](https://hbr.org/2011/02/untangling-skill-and-luck/)

> The outcomes for many activities in life — including sports, business, and investing — combine skill and luck. Most of us understand and accept this statement, but ... few of us understand the relative contributions of each.

[Don’t be a donkey](https://sivers.org/donkey/)

>  Are you trying to pursue many different directions at once? ... Don’t be a donkey. You can do everything you want to do. You just need foresight and patience. 

[There's no speed limit. (The lessons that changed my life.)](https://sivers.org/kimo/)

> one teacher can completely and permanently change someone’s life in only a few lessons...He taught me that “the standard pace is for chumps” — that the system is designed so anyone can keep up. If you’re more driven than most people, you can do way more than anyone expects. And this principle applies to all of life. 

[This is just one of many options](https://sivers.org/more-than-one/)

> Realizing the initial choice you made was just one of many brings all kinds of weathered wisdom and insight.

[Reversible business models](https://sivers.org/reversible/)

>  Are there things you assume you have to pay for, that might instead be willing to pay you?
> What current business models might as well be flipped around, or get their income from a different source? 

[Well-rounded doesn’t cut](https://sivers.org/rounded/)

> if you’re well-rounded, you can’t cut through anything. You need to be sharply defined, like a knife... Be sharp as a knife, cut through the pile of apathy, and make a point. Do this every year or two, and you will have a wide variety in the long run. 

[Call the destination, and ask for directions](https://sivers.org/destdir/)

>  Most of us don’t know what to do next. We know where we want to be, but we don’t know how to get there.
> The solution is incredibly simple and effective:
> Work backwards. Just contact someone who’s there, and ask how to get there. 

[Stay in touch with hundreds of people](https://sivers.org/hundreds/)

>  Every person you’ve ever met has the potential to help you. You need to make a simple automatic system to keep in touch without relying on your memory.
> A list: Very important people. Contact every three weeks.
> B list: Important people. Contact every two months.
> C list: Most people. Contact every six months.
> D list: Demoted people. Contact once a year, to make sure you still have their correct info. 

[Change careers like Tarzan](https://sivers.org/tarzan/)

>  Remember how Tarzan swings through the jungle? He doesn’t let go of the previous vine until the next vine is supporting his weight. 

[Switch strategies](https://sivers.org/switch/)

>  Life is like any journey. You need to change directions a few times to get where you want to go...you’ve arrived at your first destination. This is where you stop following old directions, and decide where you’re going next. The new plan means you need to switch strategies again. 

[The Multidisciplinary Approach to Thinking: Peter Kaufman](http://latticeworkinvesting.com/2018/04/06/peter-kaufman-on-the-multidisciplinary-approach-to-thinking/)

> You may know everything there is to know about your specialty, your silo, your “well”, but how are you going to make any good decisions in life…the complex systems of life, the dynamic system of life…if all you know is one well? ... I tried to learn what Munger calls, ‘the big ideas’ from all the different disciplines. I have ... my ‘three buckets’: non-living (physics, geology, chemistry); biology (living things); recorded human history.

[What You Need to Stand Out in a Noisy World](https://hbr.org/2017/01/what-you-need-to-stand-out-in-a-noisy-world/)

> there are three foundational elements to getting your ideas understood and appreciated... These are social proof, which gives people a reason to listen to you; content creation, which allows them to evaluate the quality of your ideas; and your network, which allows your ideas to spread.

[How To Be Successful](https://blog.samaltman.com/how-to-be-successful/)

> 1. Compound yourself
> 2. Have almost too much self-belief
> 3. Learn to think independently
> 4. Get good at “sales”
> 5. Make it easy to take risks
> 6. Focus
> 7. Work hard
> 8. Be bold
> 9. Be willful
> 10. Be hard to compete with
> 11. Build a network
> 12. You get rich by owning things
> 13. Be internally driven

[The 3 Stages of Failure in Life and Work (And How to Fix Them)](https://jamesclear.com/3-stages-of-failure/)

> Sometimes you need to display unwavering confidence and double down on your efforts. Sometimes you need to abandon the things that aren’t working and try something new. The key question is: how do you know when to give up and when to stick with it? 
> One way to answer this question is to use a framework I call the 3 Stages of Failure. 
> Stage 1 is a Failure of Tactics. These are HOW mistakes.
> Stage 2 is a Failure of Strategy. These are WHAT mistakes.
> Stage 3 is a Failure of Vision. These are WHY mistakes.

[This is Water by David Foster Wallace](https://fs.blog/2012/04/david-foster-wallace-this-is-water/)

> There are these two young fish swimming along and they happen to meet an older fish swimming the other way, who nods at them and says “Morning, boys. How’s the water?” And the two young fish swim on for a bit, and then eventually one of them looks over at the other and goes “What the hell is water? 
> The point of the fish story is merely that the most obvious, important realities are often the ones that are hardest to see and talk about.

[It’s fine to get an MBA but don’t be an MBA](https://hunterwalk.com/2011/12/24/its-fine-to-get-an-mba-but-dont-be-an-mba/)

> Getting an MBA means you shoot out of school wanting to prove yourself and see what you can contribute to others. Being an MBA means thinking the world owes you something...the “get, don’t be” applies not just to business school but any accomplishment that causes one to define their identity vis a vis an entity or action.

[Thinking in Ratios - Habits of Successful People](https://blog.bufferapp.com/the-habits-of-successful-people-thinking-in-ratios/)

> If you do something often enough, you’ll get a ratio of results. Once you’ve established the success rate, for example 20%, you can keep working and eventually the ratio will improve.

[The Top Idea in Your Mind](http://www.paulgraham.com/top.html)

> Most people have one top idea in their mind at any given time. That's the idea their thoughts will drift toward when they're allowed to drift freely. And this idea will thus tend to get all the benefit of that type of thinking, while others are starved of it. Which means it's a disaster to let the wrong idea become the top one in your mind.
> Turning the other cheek turns out to have selfish advantages. Someone who does you an injury hurts you twice: first by the injury itself, and second by taking up your time afterward thinking about it. If you learn to ignore injuries you can at least avoid the second half.

[Strong Opinions, Weakly Held — a framework for thinking](https://medium.com/@ameet/strong-opinions-weakly-held-a-framework-for-thinking-6530d417e364)

> This framework enables us to make decisions or forecasts with incomplete information.
> Allow your intuition to guide you to a conclusion, no matter how imperfect — this is the "strong opinion" part. Then –and this is the "weakly held" part– prove yourself wrong. Engage in creative doubt. Look for information that doesn’t fit, or indicators that pointing in an entirely different direction. Eventually your intuition will kick in and a new hypothesis will emerge out of the rubble, ready to be ruthlessly torn apart once again. You will be surprised by how quickly the sequence of faulty forecasts will deliver you to a useful result.

[The Computer for the 21st Century by Mark Weiser](https://www.ics.uci.edu/~corps/phaseii/Weiser-Computer21stCentury-SciAm.pdf)

> The most profound technologies are those that disappear. They weave themselves into the fabric of everyday life until they are indistinguishable from it.
> ...only when things disappear in this way are we freed to use them without thinking and so to focus beyond them on new goals.
> Machines that fit the human environment, instead of forcing humans to enter theirs, will make using a computer as refreshing as taking a walk in the woods

[Rheostats](https://alanweiss.com/rheostats/)

> What we all need are rheostats that can both brighten and dim the lights (or the music) by degrees and by nuance. We have created single lenses, one-dimensional litmus tests, to decide our positions and who are our “friends.” That’s patently absurd, like having the sound set at a given level despite the music, or the lights at one setting despite the time of day.

[The Best Things in Life Are Byproducts](https://zatrana.com/byproducts/)

> The best things in life are byproducts. They are almost never intentional first-order effects, but unintentional second-order effects. They emerge as a consequence of just doing what you ought to do in a way that is valuable and meaningful over a sustained period of time.
> these byproducts ... surprise us by giving us an answer we didn’t even know we were looking for.
> Complex systems are unpredictable which makes complex goals difficult to design. Rather, a better way of interacting with reality is by focusing on what can be controlled moment-to-moment, adjusting our orientation as we figure it out, letting the externalities play their part.

[Stop Stalling, Start Creating](https://goinswriter.com/stop-stalling/)

> If you are called to lead and wait to make a decision, you are stalling.
> If you obsess over your blog stats, you’re stalling.
> If you’re “mulling it over” or “weighing my options,” admit it: these are stall tactics.

[The Difference Between Happiness and Joy](https://www.nytimes.com/2019/05/07/opinion/happiness-joy-emotion.html)

> Happiness usually involves a victory for the self. Joy tends to involve the transcendence of self. Happiness comes from accomplishments. Joy comes when your heart is in another. Joy comes after years of changing diapers, driving to practice, worrying at night, dancing in the kitchen, playing in the yard and just sitting quietly together watching TV. Joy is the present that life gives you as you give away your gifts.

[Solve for X](https://medium.com/teletracking-design/solve-for-x-1818099bdb9c)

> Product + Design without Technology is Vaporware. We have a great idea and people seem to want it. But we can’t build it.
> Design + Technology without Product is a Hackathon Project. It looks great and it’s even fully functional, but there’s no market for it.
> Product + Technology without Design is an Office Printer. The necessary evils of the world that are completely at risk of being disrupted by a customer-centric innovation. 
> It takes balance between these disciplines in order to develop products that are meaningful, marketable, and memorable.

[The 30 Elements of Consumer Value: A Hierarchy](https://hbr.org/2016/09/the-elements-of-value)

> We have identified 30 “elements of value”—fundamental attributes in their most essential and discrete forms. These elements fall into four categories: functional, emotional, life changing, and social impact. Some elements are more inwardly focused, primarily addressing consumers’ personal needs...Others are outwardly focused, helping customers interact in or navigate the external world. 


[Ladder of life](https://rishabhgarg.bloggi.co/ladder)

> There is a ladder that we all are climbing. You’re at some particular step in the ladder at any particular instance of time in your life. Above someone, and below someone. Every time.   
> Don’t compare your 6th step with someone who is at the 20th step.   
> Don’t rush. Climbing more steps without enjoying the process is not the goal.

Do you have any essays that shaped your thinking? Share them via [twitter](https://twitter.com/jjude/) or [email](mailto:essays@jjude.com).

Want to know when I add to this list? Enter your email in the below form and I will inform you whenever I add to this list.

