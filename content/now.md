---
title="What I'm doing now"
slug="now"
excerpt="What I'm doing now."
tags=["now"]
type="page"
publish_at="30 Oct 15 08:49 IST"
featured_image="https://cdn.olai.in/jjude/now-is-a-gift.jpg"
---

!["Now is a gift"](https://cdn.olai.in/jjude/now-is-a-gift.jpg)

- Learning DuckDB for data engineering: As a software engineer, I always wanted to get into data engineering. But the field seemed too complicated. Now with DuckDB, probably it has become easier and possible.
- Building [siteaudit.tools](https://www.siteaudit.tools/): After my [mom passed away](/eulogy-for-mother/), I wanted a way to bring myself upto do work. I created this tool as a way to energize myself.

I [toot](https://cpn.jjude.com/@jjude) a lot on a daily basis about the progress I make on both of these. If you are on fediverse (aka mastodon), feel free to follow there.

_This page is inspired by [Derek Siver](http://sivers.org)'s own [now](http://sivers.org/now) page. You can view my [now](http://nownownow.com/p/SDvj) page and other's now page at [Now Now Now](http://nownownow.com/)._


_last updated: May 04, 2024; Photo by [Zachary Keimig](https://unsplash.com/photos/5r1bhGWQIHo)._
