---
title="Have you changed your mind lately?"
slug="changed-mind"
excerpt="When the world changes, it is risky to rely on an outdated view of the world."
tags=["coach","wins"]
type="post"
publish_at="06 Apr 22 05:00 IST"
featured_image="https://cdn.olai.in/jjude/changed-mind-gen.jpg"
---

We are blind to the restricting patterns of the world we live. We are taught that the world works a certain way, and as a result of our upbringing, we do not question or challenge these beliefs. 

Tell me if you believe, or used to believe, the following:
- Maturity comes with age 
- Men should not cry 
- High-quality products are expensive

They are all correct. Only in a specific context. When your circumstances changes they all crumble. Your situation may change due to a change in your life stage, or relocation to a different country, or technological advancement. **When the world changes, it is risky to rely on an outdated view of the world**. 

Everything in my life was going wrong at one point. I had a failed relationship, a high-paying contract was abruptly terminated, and my dream of starting a business came crashing down around me. All of this happened over the course of two years. It was all too much for me to bear. 

My good friend suggested I go to a church in the nearby hills. I took up the suggestion. I was the first to enter the church when it opened at 6 a.m. The church was empty and I sat on the last pew. Silence of the place, the weight of my heart, and my helplessness brought tears instead of words. I wept everyday, for three months. Emptying the heart brought peace. With peace came wisdom. Then came opportunities. 

Perhaps I was able to spot opportunities because I was at peace and wise.

Now, every quarter, I pause and **ask myself three questions**:
- Where am I struggling?
- What is blocking me?
- Who can help me?

In most cases, I am unsure of what is blocking me. Maybe I know what's hindering my progress, but I'm not sure if it's a blind spot on my part or if it's a genuine obstacle. This is why I **have a group of advisors** I meet and speak with on a regular basis. Often, they will see the same problem from a different perspective, and offer me suggestions for resolving it. With many advisors, our plans succeed. _(That's another thing I have [changed my mind about](/smart-people-ask-for-help-do-you-ask-for-help/).)_ Isn't that right?

## Continue Reading

* [Smart People Ask For Help](/smart-people-ask-for-help-do-you-ask-for-help/)
* [How To Ask For Help And Get It?](/how-to-ask-for-help-and-get-it/)
* [Common Traps In Getting Meaningful Feedback](/common-traps-in-getting-meaningful-feedback/)
