---
title="Good Morning. How Much Did We Make While Sleeping?"
slug="good-morning-how-much-did-we-make-while-sleeping"
excerpt="Wealth can not be created if revenue is a function of hours worked. What avenues are available for creating passive income."
tags=["wealth","wins"]
type="post"
publish_at="28 Jul 10 16:30 IST"
featured_image="https://cdn.olai.in/jjude/good-morning-how-much-did-we-make-while-sleeping-gen.jpg"
---


For most of the working population, the amount received at the end of the month is directly proportional to the number of hours worked. Solo practitioners & consultants may exercise flexibility of charging higher billing rates per hour to earn more but even then revenue is still attached to hours worked.

Wealth can not be created if revenue is a function of hours worked. For one there is a limit to billable hours and billable amount per hour is largely dependent on customer's perception of return on investment. For another there are hours where is there is no revenue generation (ex: sleeping) and there are hours where there is wealth depletion (ex: shopping, vacation).

![Passive Income](https://cdn.olai.in/jjude/active-passive-income.png)

For wealth generation & retention, one, more or all of the three should be done:

1. Earning per working hour has to increase
2. Spending has to be curtailed
3. Establish a mechanism to generate revenue during non-working hours

As discussed above, there is a limit to improving billing per hour; and most of us generally prefer not to curtail spending, especially when we get used to a luxury. That leaves the choice to generate revenue during non-working hours.

The conventional belief is that only entrepreneurs earn during their non- working hours. That is not true. Avenues are open for every one; either we are ignorant of them or find it easy to complain rather than implement.

Some of these avenues are investing, freelance writing, writing books (e-books and print), selling own products via web, affiliate sales via web and so on. With the world becoming flatter every day, such opportunities are opening up all around us in abundance.

What avenues are you utilizing for Wealth Creation?

### You may also like:
- [Topics of interest - 2019-20](/topics1920)
- [Truth About Passive Income & Financial Freedom](/pasive-income-truths)
