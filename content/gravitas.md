---
title="What Is Gravitas, Anyway?"
slug="gravitas"
excerpt="What are the factors contributing to executive presence?"
tags=["wins","coach"]
type="post"
publish_at="07 Sep 21 20:21 IST"
featured_image="https://cdn.olai.in/jjude/gravitas-gen.jpg"
---

There are only three types of leaders

• competent, but not confident
• confident, but not competent
• competent AND confident

Only the last has "executive presence" known as Gravitas

### Competent, not confident leaders

They were promoted based on their technical expertise. They know what needs to be done and get great results.

However, they let their insecurities hinder them from moving forward. They can't carry the team because they lack confidence.

I was one of those.

In general, I did well in studies and at work. However, I struggled with "confidence". Even though things were different in reality, I did not think of myself as being capable

### Confident, not competent leaders

As Marshall Goldsmith said, the skills necessary to get the job are not sufficient to do it.

However, many do not understand that concept. Because yesterday's confidence brought them here, they continue to rely on yesterday's competence.

Most companies are managed by confident leaders relying on yesterday's competence, as evident from their short lifespan.

### Competent AND confident leaders

They are the exception, not the rule.

When an organization gets such a leader, they will prosper, not only for the leader's lifetime but also for many years to come, since the leader will frame a structure for prosperity.

They possess gravitas

Then, can we develop gravitas?

Gravitas: Nature or Nurture?

That's a good question.

This will be our next topic.

## Continue Reading

* [The dichotomy of contentment and ambition](/contentment-and-ambition)
* [What can we learn from China?](/copy-china)
* [Mastering sales as a new CTO](/cto-sales)
