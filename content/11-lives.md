---
title="11 Lives to Build a Rich Career"
slug="11-lives"
excerpt="You take about 7 years to master a skill. You can continue to learn new skills to build a rich and valuable career."
tags=["coach","vlog","wins","visuals"]
type="post"
publish_at="23 May 17 00:08 IST"
featured_image="https://cdn.olai.in/jjude/11-lives.jpg"
---
![11-lives-to-build-rich-career](https://cdn.olai.in/jjude/11-lives.jpg "11-lives-to-build-rich-career")
Do you know you got 11 lives to live? I bet you didn’t. Today let us find out how to live those 11 lives.

[Kamal Hassan](https://en.wikipedia.org/wiki/Kamal_Haasan) is a popular South Indian actor. His career spans more than 50 years. He has built a rich career, not only in terms of money, but also in terms of versatility. He has been an actor, choreographer, director, producer, and playback singer. There is, probably, no role he has not played in the movie industry. That is the kind of **diversity that makes your career a rich and a valuable one**.

But how do you build such a versatile career?

I found the answer in a comic.

Zac from [Saturday Morning Breakfast Cereal](http://www.smbc-comics.com/?id=2722), says we take about 7 years to master a topic. That means, if we start at 11 and live upto 88, then we can master 11 topics.  He recommends to pick up a topic and focus for seven years. It is a metaphor, so don’t hooked to the exact numbers. The point is, **we all can become masters of multiple domains**.

Let us talk an example.

Say you are a PHP developer. You learn to create web applications, connecting to database, streaming video, hunting down performance bottlenecks, and so on. Once you master PHP, then you can focus on a package like Drupal or Magento. Say you pick up Magento. Then you learn about e-commerce domain and how to create an e-commerce store, how to add products, how to start a campaign, how to track sales and so on. After some years, you can pick up one or two industries, and learn to use Magento in those industries.

![Software skills for digital era](https://cdn.olai.in/jjude/digital-value.png "Software skills for digital era")

In this path, **you are building a rich career layer upon layer**.

You can follow the same methodology to build a career in marketing, design, and business analysis. In fact, you can mix and match too. In one iteration, you can focus on technology, another on marketing, and another on design. After few iterations, you will be one of the few to design and architect an easy to use marketing tool.

That my friend, is how you build a rich career.

## Watch on YouTube

<iframe width="560" height="315" src="https://www.youtube.com/embed/swHmwqJCzHg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

## Continue Reading

* [The curse of EVERYTHING and NOW on building your career](/all-and-now/)    
* [An awesome tip from Jack Ma & Derek Sivers to build your career](/specialize-or-generalize/)   
* [Intentional Imbalance](/imbalance/)    
