---
title="Simply Better Consultants"
slug="simply-better-consultants"
excerpt="What are the 'category benefits' expected from a consultant?"
tags=[""]
type="post"
publish_at="02 Feb 09 16:30 IST"
featured_image="https://cdn.olai.in/jjude/simply-better-consultants-gen.jpg"
---

'Predictable and reliable delivery of category benefits is the driver for success', argues Patrick Barwise and Sean Meehan in their book, '[Simply Better](http://www.simply-better.biz/)'.

I agree.

Taking parallel from the book, what are the 'category benefits' (those expected out of a category or class of products or services) expected from a consultant, as a product and as a service?

I could think of these two:

**Domain Expertise**: This is the only reason a consultant is in the room. They are expected to be a master in the domain - whatever be the domain - CRM, Project Management, Process Re-engineering, Peoplesoft.

**Appreciation of business reality**: Usually consultants have to deal with dilemma on the ground and steer their clients to a solution. The problem in hand will be a product of people and process. Yet, consultant should stay focused and find or create a path forward.

Domain expertise can be learned. In fact, during their life time consultants achieve mastry of more than one domain. However the other skill is hard to learn and judge. But it will be needed in almost all of the assignments. Without it, it is hard to be a consultant.

Do you agree?

