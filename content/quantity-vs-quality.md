---
title="When quantity trumps quality?"
slug="quantity-vs-quality"
excerpt="As a consumer, I prefer quality; as a maker, I prefer quantity."
tags=["coach","systems","blogging"]
type="post"
publish_at="27 Jan 16 02:20 IST"
featured_image="https://cdn.olai.in/jjude/2016-01-26-shorthand-practice.jpg"
---
I prefer quality over quantity. I'm sure you too prefer that way.

Instead of ten cheap replicas of a smart-phone, I prefer one quality smart-phone. Instead of reading ton of 'me too' posts, I prefer to read one thoughtful post. In project management, I prefer to add smart developers to the team rather than adding tons of bodies.

Given my proclivity to [better, not more][1] idea, I was cynical of [Justin][2]'s [Mega Maker][3], initiative. Who needs another [100 half-baked][4] products? I wasn't enthusiastic to sign-up to his initiative.

I even [tweeted][5] at his friend, when he tweeted to Twitter CEO, about quality over quantity.

But then ...

There is always another point of view. I'm glad that [Grégoire Gilbert][7], replied to my tweet with his point of view. He could've taken my tweet as a troll and ignored it. If you are open to ideas, you learn everyday.

Mine is a consumer point-of-view. As a consumer, I prefer to buy quality products. As a manager (consumer of skills), I prefer to take in the best quality skills. As consumers, we all should prefer quality over quantity.

But what about a maker?

I am a maker too. I create apps and write blog posts. As I went about my daily life creating apps and writing blog posts, Gilbert's tweet flashed in my mind often. **Makers suffer from two nightmares: fear of starting, and fear of finishing**.

When you start a project, your mind is filled with fear. Is the idea any good? Do I have enough skills to complete it? Will it find acceptance within my audience? So many other similar questions. Most of the makers don't start anything because of these fears.

Then there is fear of finishing. Is it good enough? Does it resemble the end-product I had in mind when I started? Will anyone mock me for doing this? Does it look like a copy-cat? Some even fear success! Can I handle the pressure of its success? Will success be its own burden? And so on.

How do you get rid of these fears? By practicing. In my mother-tongue, Tamil, there is a saying: you can become a doctor only after killing 1000 roots. Tamil medicine is based on plants and roots. Unless a doctor studies at least 1000 roots, he can't prescribe medicines properly. Similarly, you have to write 1000 crappy articles before you get published.

I remember my colleague practicing his short-hand skills. There are times, he would write pages and pages of shorthand. Other times, he would take a single paper and practice, what could only be called as child's scribbling. When I asked, he said, he was training his hands to take notes faster; he was building his muscle-memory.

![Shorthand Practice](https://cdn.olai.in/jjude/2016-01-26-shorthand-practice.jpg)

I have understood the mega-maker challenge now. It is to **build muscle memory for makers**. When you start the challenges, you will face all the fears. As you start and finish each challenge, you subdue your fears little by little. And then, one day, when you stare at a major maker challenge in your life, you will confidently say, 'I can do this'.

[1]: /we-dont-need-more-we-need-better/
[2]: https://twitter.com/mijustin
[3]: http://megamaker.co/manifesto/
[4]: https://docs.google.com/document/d/16gVonXDjdvlj6yBP-hOmbktugZ3-QkLOw_nhRN9xlUs/
[5]: https://twitter.com/jjude/status/683450922349940737
[6]: https://twitter.com/gregoiregilbert/status/683563781490913280
[7]: https://twitter.com/gregoiregilbert
