---
title="Manjula Sularia on 'Life Skills for Personal and Professional Success'"
slug="manjula-life-skills"
excerpt="To thrive in our careers, we need more than the technical skills. What are those skills? Let us find out in this episode."
tags=["gwradio","wins","self"]
type="post"
publish_at="05 Apr 22 06:00 IST"
featured_image="https://cdn.olai.in/jjude/manjula-ls.jpg"
---

<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless src="https://share.transistor.fm/e/6b4a90d4"></iframe>

## What you'll learn
- What are life-skills and why are they needed?
- How to learn these life-skills?
- How life-skills affect childhood, employment, and even retirement?

## Connect with Manjula
- LinkedIn: https://www.linkedin.com/in/manjula-sularia-28112325/
- Twitter: https://twitter.com/ManjulaSularia
- Instagram: https://www.instagram.com/manjulathelifeskills/

## Resources mentioned

- Life-skills list: https://en.wikipedia.org/wiki/Life_skills#Core_skills

![Manjula Sularia](https://cdn.olai.in/jjude/manjula-ls.jpg "Life Skills by Manjula Sularia")

## Connect with me
- Twitter: https://twitter.com/jjude
- LinkedIn: https://www.linkedin.com/in/jjude/
- Website: https://jjude.com/
- Newsletter: https://jjude.com/subscribe/
- Youtube: https://youtube.com/gravitaswins
- Email: podcast@jjude.com
- Executive Coaching Program: https://gravitaswins.com

## Your feedback counts

Thank you for listening. If you enjoy the podcast, would you please leave a short review on Apple podcast or on YouTube? It takes less than 60 seconds, and it really makes a difference in finding this podcast. And it boosts my spirits. 😀

I would like to hear what resonated the most with you in this episode. Either you can email me or share it on social media and tag me. Thank you for your support.

Have a life of WINS.

## Amazing tools I use:
- I record all the remote interview with [Riverside.fm](https://riverside.fm/?via=joseph-jude)
- I use [Transistor.fm](https://transistor.fm/?via=jjude) for hosting these podcast episodes

_Gravitas WINS Radio is produced by [Joseph Jude](https://jjude.com/). Be sure to subscribe on [Apple Podcast](https://podcasts.apple.com/in/podcast/gravitas-wins-radio/id1562802449), [Spotify](https://open.spotify.com/show/7LOBjAyjBkechMC0xMIj9E),[Google Podcast](https://podcasts.google.com/feed/aHR0cHM6Ly9mZWVkcy50cmFuc2lzdG9yLmZtL2dyYXZpdGFzLXdpbnMtcmFkaW8=),[YouTube](https://www.youtube.com/gravitaswins), or the podcasting app of your choice._

Please check out other episodes of [Gravitas WINS Radio](/gwr-themes/). For easy consumption, they are categorized by themes. If you are a fellow podcaster, you might be interested in [How I Podcast](/how-i-podcast/) or [tools I use](/i-use/).


## Continue Reading

* ['Transitioning from developer to manager' with Ubellah Maria](/ubellah-maria-becoming-a-manager/)
* ['Nonviolent Communication' with Ranjitha Jeurkar](/ranjithaj/)
* [Three Types Of Goals You Should Set](/three-goals/)