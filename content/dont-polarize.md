---
title="Don't Polarize"
slug="dont-polarize"
excerpt="Polarized solutions provide short-term success, they are ineffective in the long-term."
tags=["problem-solving"]
type="post"
publish_at="04 Feb 11 16:30 IST"
featured_image="https://cdn.olai.in/jjude/balance.png"

---

Shortly after 9/11 disaster, George Bush, then US president, categorized national leaders as 'either with us or against us'. With that dramatic phrase he polarized nations into just two camps. It goes without saying that, he implied, one of the camps is full of 'good guys' and another satanic. He isn't alone to fall into the fallacy of polarization.

Management is also littered with polarization.

" . . . The greatest failings of strategic management have occurred when managers took one point of view too seriously" says the management guru Henry Mintzberg. He continues, "Discourse seems often to have the following structure: Consider no more than two possible sides to a question, place them in opposition, focus on the approach you consider “correct,” and caricature, then vilify and ultimately dismiss the opposing point of view."

![Don't polarize](https://cdn.olai.in/jjude/balance.png)

But truth be told, polarization provides popularity - in politics as well as in management. When a CEO proclaims in front of his employees, "You come first; Customers second", there is a thunderous applause in the hall.

An article describing how women are better than men for the CEO post gets a wider circulation and raises to be a top management article, and may be providing the author with speaking, consulting and related offers.

Who doesn't enjoy a bit of flattery?

So if polarization leads to popularity why shouldn't troubleshooters polarize? Shouldn't they be popular or don't they are entitled for their share of flattery?

While I am not against anyone enjoying adulation - whether rightfully theirs or not - polarization has more negatives than positives. Here are some from my experience:

1.  **It exposes lack of awareness** : Is layoff the only solution for falling profits? Is advertising only way to increase market share? Propagation of such polarized solution only announces your naivete out loud.
2.  **It is focused on short term success** : Markets focus only on the next quarter; CEOs mind is occupied only with share price and consultants are interested only in finishing the assignment and cashing their cheques. The sure way to satisfy these needs is to play popularity contest. But none of these mindsets bring an enduring solution.
3.  **It is usually out of (false) ideologies** : Those who grow up in a male dominant societies generally develop false ideologies about capabilities of women. Their solution for any leadership crisis center around keeping woman far away from an executive role. On the other hand those with strong feminist ideologies will proclaim only a woman CEO can bring an ailing company back into profit. But is any of these effective solutions? Shouldn't business problems have business decisions rather than [ideological affirmations](/how-ideologies-impact-problem-solving/)?
4.  **It highlights inadequate people skills** : Solutions may be brainstormed behind closed doors but its implementation involves real people. People have opinions and emotions. Those who treat people like lifeless products polarize but those who can befriend their opponents, like the great US president Abraham Lincoln, leave a proud legacy behind.
5.  **It is probably to defend bruised ego** : Intelligent and powerful can be insecure too. They take up extreme positions as a way to assert themselves. They damage the problem solving process by such polarized positions and if they persist in such positions, they become a hurdle.

Though polarized solutions provide short-term success, they are ineffective in the long-term. Effective solutions are only arrived after elaborate study of the situation and are always contextual. When best problem solvers finish the assignment, the problem remains solved.

_This post is part of '[Be a Problem Solver](/be-a-problem-solver/)' series._
