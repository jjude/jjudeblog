---
title="What an Indie Developer Expects From Apple"
slug="what-an-indie-developer-expects-from-apple"
excerpt="At WWDC, Apple introduced improvements aimed at both users and developers. But it has to improve its 'doing business' part."
tags=["coach","startup","tech"]
type="post"
publish_at="25 Jun 13 16:30 IST"
featured_image="https://cdn.olai.in/jjude/what-an-indie-developer-expects-from-apple-gen.jpg"
---

At WWDC, Apple introduced improvements aimed at both users and developers. Its going to be exciting to develop for the new OSes - [Maverick](http://www.apple.com/osx/preview/) and [iOS 7](http://www.apple.com/ios/ios7/features/). Even dull routines are now automated in [XCode](https://developer.apple.com/technologies/tools/whats-new.html), which should improve developer's productivity.

But, the improvements introduced in the 'doing-business' part is disappointing. Yes, now developers can [transfer apps](http://www.tuaw.com/2013/06/10/itunes-connect-now-allows-developers-to-transfer-apps-to-another/) to another account easily. Beyond that there is nothing else.

These 'doing-business' part doesn't get discussed much. They are drowned in endless chatter on new features, comparison with other OSes and so on.

Here are some of the expectations of an indie developer in this particular area.

I know, all of the below are possible today but outside of Apple's eco-system. Developers have found work-arounds to all of these issues. They have to. But it will be helpful if Apple includes these within their eco-system itself.

### Access To Customers

For any business to grow, they need to know their customers. Who they are, what age group they belong to, how they are using the product, why they are using the product are some of the questions that help improving the product.

Today to get these answers, developers integrate with analytics tools like [Flurry](http://www.flurry.com/). Still that is no direct access to the customers.

Here is a dilemma though: As a customer, I value the abstraction Apple provides. I don't want to be spammed by promotional emails and newsletters. But as an indie-developer, I am in the dark about my customers. I don't know if there is a solution that solves both the problems.

### Make Beta-Testing Easy

I couldn't get my sister to test BlogEasy, because she is too lazy to find UDID. Testing should be as simple as clicking a link, downloading the app, testing and providing feedback.

Testflight solves this partially. If you have big enough audience, testers may go through needed registration, downloading another app to download your app and so on and so forth. But if you are starting out, it is almost impossible to find beta-testers.

_Testflight is no more. If you are interested you can read [What Happened to TestFlightApp.com?](https://theqalead.com/topics/what-happened-to-testflightapp-com/)._

I was lucky enough to get [Shankar Ganesh](http://about.me/shankarganesh) beta test BlogEasy thoroughly. But that was just one beta-tester.

### A Demo Store?

iOS apps can be distributed only through AppStore. An app can be described only to certain extent with screenshots and video. Users like to use an app before shelling out $$. I created a free, ad-supported version of [BlogEasy](http://bit.ly/BEMini), but it is not a feasible model for all apps.

Mac apps can be distributed outside of AppStore. So it is resolved partially. I say partially because now developers have to maintain multiple builds. Add to that, the painful license verification, since it is easy to crack every available verification schemes.

In absence of a demo store, developers are at the mercy of popular review sites.

### Selective Discounts

Say you are able to impress an editor of a review site; You want to give their readers exclusive discounts, say 50% discount for a week. Na, it is not possible.

Or you want to provide 10% discount for those who subscribe to your newsletters. Again not possible.

Or you want to participate in bundle sales, like [BundleHunt](http://bundlehunt.com/), to increase visibility &amp; sales. Nope, sorry its not possible.

As with everything, Apple gives you only a 'full or nil' option. You can give them free copies or the discount is for all.

There is already an option to redeem coupons in AppStores. It should be extended to give a certain percentage of discount on these coupons. And it should be possible to give more than 50 coupons per version.

Apple opened up whole new world to indie-developers. Would love to see it evolve even in 'doing-business' part as fast as they evolve in other aspects.

