---
title="Don't Torture Context To Fit Your Solution"
slug="dont-torture-context-to-fit-your-solution"
excerpt="Analyze the situation and tailor the solution rather than tinkering the problem statement to plug the solution at hand."
tags=["problem-solving"]
type="post"
publish_at="06 Apr 11 16:30 IST"
featured_image="https://cdn.olai.in/jjude/squarepegroundhole.png"
---
"What Laptop should I buy?"

This is probably the most asked question to me. It is understandable since I'm the only 'IT guy' in this government department. Usually they expect an absolute answer like, "Dell is good" or "You should go for Lenova". But staying true to my profession, I reply them, "It depends".

"It depends", I tell them, "if you want to use it primarily for email, typing letters and creating presentations. Or you want your kids to play games with it."

![Round hole](https://cdn.olai.in/jjude/squarepegroundhole.png)

Is your solution fit for the situation?

It is not to confuse them. Rather to help them as correctly as possible.

Each brand has laptops designed specifically for business, gaming and multimedia. It would be a mistake to recommend a gaming laptop from Dell for a business use (email, word processing & presentations) just because I bought one and am happy with it. It is not that it won't work but it is not worth the money.

We can see a version of this scenario in almost all parts of our life.

* "Should I invest in mid-cap?"
* "Traffic in Delhi came down after metro was introduced. Why not we introduce metro in Coimbatore?"
* "So and so company implemented Peoplesoft payroll and we should too"
* "My friend took PMP and got into a good company. Why don't you take up PMP?"
* "Be good. Good things will happen to you"
* "GE outsourced all its IT operation to India and its profit has gone up ever since. As a cost cutting measure, let us outsource to India"

Individually all of these solutions - from investing in mid-caps to outsourcing - may have produced awesome results. But recommending them as absolute answers is a mistake.

Analyze the situation and tailor the solution rather than tinkering the problem statement to plug the solution at hand.

_This post is part of ‘[Be a Problem Solver](/be-a-problem-solver/)‘ series._
