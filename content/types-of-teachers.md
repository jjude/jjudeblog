---
title="Types of teachers and coaches (applicable to managers too)"
slug="types-of-teachers"
excerpt="The best teachers don't just transfer skills, they're interested in overall development."
tags=["sdl","coach"]
type="post"
publish_at="21 Aug 24 06:52 IST"
featured_image=""
bsky_id="3lbwix6zvvp2d"
---

There are three types of teachers and coaches.

My sons have started learning swimming, and depending on the time we go, they are trained by two different coaches. The difference between them is stark.

**One coach views his job purely as a utility**. He is highly skilled and effective, as evidenced by the progress of his adult students who swim smoothly within about 15 days. However, he is strict, harsh, and to the point. He focuses solely on drills and is tough on those who do not comply. Students of such coaches can lose interest of learning quickly.

The other coach is **empathetic and understanding**. If a student struggles to learn, he tries to understand the reasons and provides space for growth. However, this approach can be exploited by students, especially when parents have forced them to learn. Students may mimic difficulties or cheat to avoid making progress.

The best teachers combine both approaches. They are not just interested in transferring skills but also in the **holistic development of the student**. These teachers understand students' difficulties while demanding excellence in a gentle manner. They engage with students through conversation and humor, making them feel relaxed. They can identify when a student is not putting in enough effort and address it effectively.

From my experience with all three types of teachers, I have found that the most effective ones are those interested in overall development rather than just skill transfer. This is why I am careful when choosing coaches for myself. If I only want to learn a specific skill, there are plenty of resources like YouTube videos and books available. However, I look for a coach who can provide feedback in an understandable way and motivate me to continue.

The best coaches blend a philosophical approach with a utilitarian one. They make learning enjoyable and make students feel that they have achieved their goals independently rather than relying solely on the coach's efforts. This approach leads to the most fulfilling learning experience.

## Continue Reading

* [Book Notes - Smartcuts](/smartcuts/)
* [How to think structurally?](/structured-thinking/)
* [Self-Directed Learning (SDL)](/sdl/)