---
title="AI transformation will start as lift & shift"
slug="ai-lift-shift"
excerpt="We will use chatgpt as a replacement for google until AI tools change the way things are done."
tags=["tech","aieconomy","gwradio"]
type="post"
publish_at="26 May 23 16:57 IST"
featured_image=""
---
<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless src="https://share.transistor.fm/e/27b4f017"></iframe>

All technology transformation starts as lift and shift solutions. 

When the personal computers were introduced people used it as a replacement for  typewriters, calculators, and music systems. When the internet was introduced, governments scanned paper forms and uploaded to their sites. In both cases, workflow of doing things didn’t change. These technologies didn’t change the lifestyle of us. 

It would be almost a decade before these technologies changed the way things were done.

The initial PC sellers like HP lifted and shifted their order management to the internet. Everything else remained the same. They manufactured and shipped to warehouses across the world, customers placed orders via online portal, then the computers were shipped to the customers. Only Michael Dell inverted the model. He went from manufacture, order, ship to order, assemble, and ship.

It is going to be the same with AI too. We are asking chatgpt the same questions that we search on Google. We treated computer as a bigger calculator. Same way we are treating chatgpt as a better Google. 

It will take time to reimagine how we communicate, educate, and shop using AI. My bet is that this technology cycle won't be that long like the personal computer and the Internet era.

Do you agree?

_This is part of [Short notes on AI Economy](/ai-economy-notes/)_