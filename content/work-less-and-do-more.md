---
title="Work Less and Do More - Interview with Stever Robbins"
slug="work-less-and-do-more"
excerpt="Interview with Stever Robbins on his new book 'Get It Done Guy Book'"
tags=["interview","productivity"]
type="post"
publish_at="28 Sep 10 16:30 IST"
featured_image="https://cdn.olai.in/jjude/work-less-and-do-more-gen.jpg"
---

I'm always on the lookout for a tip on improving productivity. So when it comes from someone who is a a serial entrepreneur, a regular HBR contributor, and author among many other things, I'm all ears.

![Stever](https://cdn.olai.in/jjude/stever.jpg "Stever")

[Stever Robbins](http://www.steverrobbins.com/ "Stever Robbins") has released a new book called "Get-It-Done Guy's 9 Steps to Work Less and Do More". His team has put together a website from which you can download the first two chapters of the book for free and tons of other podcasts on the same theme.

I read the free chapters and I'm impressed.

When I'm faced with a huge task, I ask myself, "How do you eat an elephant?" And then I answer, "Bit by bit". But in the second chapter titled, 'Stop Procrastinating', Stever says,
> Taking baby chunks isn’t about breaking your project into pieces, it’s about breaking time into pieces....
>
>
> With baby chunks, you’ll speed date the task you’ve been procrastinating. Set a time limit (preferably with an actual physical timer) and work 100 percent for that much time. Then get up and do something  else. From the moment you sit down, you’ll know that in just a few minutes, you’ll be free once again.

Since I read this, I've been practicing 'breaking time into pieces' and it has given me amazing results. Suddenly there is progress in 'bitter' projects. Hippie!

With that enlightenment, I sent out three questions to Stever. Walking his talk, he replied immediately. Here they are:

### Q) There are plethora of self-help books in the market already. How is 'The Get-it-Done Guy Book' different from '7 habits', 'Get it done' or '80/20', which you also refer in your book?

Seven Habits and 80/20 both present specific principles you can use to improve parts of your work life. In the case of Seven Habits, there are seven broad principles, which you're left to figure out how to apply. In the case of 80/20, you get a single principle and dozens of ways to apply it.

My book introduces the Nine Steps as big picture principles, and then applies those steps in several ways that you can instantly take and use as individual tips. If you're reading for big picture ideas, you'll find those in the book. But if you want a very hands-on, specific set of tools for making yourself more productive today, you'll find that too. I'm much more actionable  than the other two books. It's a reflection of my personality—I'm a big picture thinker with the ability to move from the big picture vision all the way down to the number of paperclips needed to implement the vision. The book is written that way, too.

The Get-it-Done Guy Book is much funnier than any other business book I'm aware of. Plus, it tells you how to use your file folders to organize a zombie army to take over the world. I'm pretty sure that Covey neglected that topic entirely in Seven Habits.

### Q) You head many successful start-ups, write a weekly column on HBR, produce a podcast and now authored a book. These are more than what most of us do. But you mention in the book that 'If you aren’t careful, you’ll end up working more than when you started'. So how does this line fit into your life?¨

You didn't mention the Get-it-Done Guy Musical, which I'm co-writing with an Off-Broadway composer, producing, and will be performing. :-)

Sometimes we teach best what we need to learn most. At the moment, I've consciously overloaded myself to produce the exposure to gain momentum with the book. It isn't sustainable, and I know that. Step One in the book is Live on Purpose, know the purpose behind everything you do and know how it fits into the map of your life. I know where the book fits in my life map, and while it's fun, it's one small part of several larger areas of my life. Much of what I'm doing now supports the book, but aren't related to other areas of my life. A few months from now, I'll be ramping down many of my current activities that are motivated by the book, but don't fit under my larger dreams.

And just for the record, my ultimate professional dreams are launching a paid media career and creating an information products business that lets me make a major impact in how people create the life they want.

### Q) Most of the people in the west side of the globe believe, 'Destiny is a choice'. So it is easy to say to those who live there, 'have a goal, and work towards that goal'. But on the east side of the globe, we generally believe 'Destiny is a chance'. How can we use the 9 steps you propose?

I am good at reaching goals. I've spent my entire life setting and working towards goals. Three years ago I stopped and asked whether setting and reaching goals was giving me a high-quality life. Upon reflection, the best things in my life were almost never explicit goal nor steps designed to reach a goal.

Now, I live more in the moment. The 9 Steps are still extremely useful, since they help you do things easily and freely whether you're working towards a goal or just doing what you enjoy doing in the moment. For example, today I am writing several guest columns about my book using my grid techniques from Step 5: Stay Organized. Whether these guest columns are part of a master plan to make my book a best-seller, or whether they're a spur-of-the-moment decision is irrelevant. I'm still able to work less and get more done, whether or not there's a bigger goal I'm trying to reach.

In Step 1: Live on Purpose, I introduce life maps, which are hierarchies of goals that show how you organize your life. Your question has made me realize that in my life map, my top goals aren't goals in the traditional sense. They're things I believe will be fulfilling in the moment, and everything flows from that. I didn't choose them because they seemed like good destinations, I chose them because they seemed like good journeys. And that, my friend, makes all the difference.

