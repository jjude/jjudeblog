---
title="A tale of two customer services"
slug="a-tale-of-two-customer-services"
excerpt="As businesses become virtual, it is not only necessary to ensure that customers have good experience during pre-buying and buying but post-buying as well."
tags=["coach"]
type="post"
publish_at="21 Apr 08 16:30 IST"
featured_image="https://cdn.olai.in/jjude/a-tale-of-two-customer-services-gen.jpg"
---

I believe that as businesses become virtual, it is not only necessary to ensure that customers have good experience during pre-buying (as they browse through product catalogue) and buying (shopping cart) but post-buying (customer service) as well.

**A positive one**

I was introduced to [Zoom In](https://www.zoom.in) by a colleague. He said that among the many online photo printing sites that he visited, he liked Zoom In. I decided to try it out because of his strong recommendation.

I had to agree with him because every thing was as easy as it could be - signing up, uploading photos and ordering photos. Uploading photos took sometime, but otherwise the site was quite fast; Utilizing their 25 free prints, I ordered few prints. The prints were neat and were delivered through courier. Neither the prints nor the delivery was expensive.

That was not the best part. I wanted to order few calendar prints that they advertised. When I tried to use one of the uploaded photos, it showed an red exclamation mark but didn't throw up any message. I was bit confused and sent them an email. Promptly came the reply with proper explanation. I agree it could've been set in their page in the first place; but I was forgiving considering all the other pleasant experience that I had with [Zoom In](http://www.zoom.in/).

When I shared this with my colleague, he informed that, [Zoom In](https://www.zoom.in) claims that they read every e-mail and respond. There should be cost involved, but probably [Zoom In](https://www.zoom.in) considers that it is worth. Good for them!

**A bad one**

I've been looking to purchase, '[Peopleware](https://en.wikipedia.org/wiki/Peopleware)' by Tom. I searched in most of the 'real' bookshops and whatever online bookshops that I knew, but without any success. So when I saw 'Available' against '[Peopleware](https://en.wikipedia.org/wiki/Peopleware)' in [NBC bookshop](http://www.nbcindia.com/), I was glad and immediately ordered one. I ordered one more book along with it and the amount was immediately debited.

However, I got only one book and when I sent them a mail they said the shipping time is 3-4 weeks for '[Peopleware](https://en.wikipedia.org/wiki/Peopleware)' and it will be shipped in that duration. Since it was a normal time duration, I didn't bother. But not so when that became 6 weeks. I sent them a reminder and I got a response:
  > **Your book is dispatched from the international stores. We will inform you once it reaches us.**

That seemed a reasonable response and I decided to wait for few more days. However after 8 weeks, I sent them a 'strong' message. Then they replied saying:
  > **The book you ordered is out-of-stock. We will pay you back.**

I'm not surprised that the book is out-of-stock or that they agreed to pay back, but the way they provided misleading information put me off. Had they told me in the first place that the book was out-of-stock, probably I would've continued to do business with them.&#160; But not after providing wrong information. (By the way, I am still waiting to get their refund).

When I have to print some more photos, I'll surely go to [Zoom In](http://www.zoom.in); when I want to buy a book, I will definitely look at some other online shop.

