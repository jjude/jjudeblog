---
title="Smart People Ask For Help"
slug="smart-people-ask-for-help-do-you-ask-for-help"
excerpt="You don't have to sweat it alone. Be smart and seek other's help"
tags=["coach","insights"]
type="post"
publish_at="07 Jul 10 16:30 IST"
featured_image="https://cdn.olai.in/jjude/ask-for-help.jpg"
---

![](https://cdn.olai.in/jjude/ask-for-help.jpg)

When I was younger, I thought smart people don't need help. My masochistic view was that smart people are self-sufficient. Because I thought I was smart, I avoided asking for help. What an ignorant fool I was.

> Asking for help with shame says:    
You have the power over me.   
Asking with condescension says:   
I have the power over you.    
But asking for help with gratitude says:   
We have the power to help each other.   
― Amanda Palmer

Through my friend Pam, I saw the light. Pam was the smartest in any group. She began her career with Father Damien Foundation in India and later became a consultant for the European Union, United Nations, and USAID. She epitomizes "smart people ask for help." She would often say, "I wouldn't be where I am without the help of others." Because of her, I learned how to ask and accept help. My idea of "smartness" changed along the way.

Here are five points I understand about being smart in asking help.

1) **Smart people operate from a platform of strength**: They know their strengths and put their strengths to work. Since they do their part well, they are often successful on their own. Their success attracts others to willingly extend help. Who wouldn't want to join forces with a winner?

2) **Smart people know their weaknesses**: Not only do they play to their strengths, but they are also level-headed enough to know where they fall short. They don't waste time working on their weaknesses. Instead, they partner with other smart people.

3) **Smart people are confident**: Knowing their strengths and weaknesses helps them feel confident about their abilities. They take on projects they can accomplish on their own, or in collaboration with other smart people. The result is a success that reinforces their self-confidence.

4) **Smart people play in a two-way street**: In addition to seeking help, they also offer their assistance. As a result, they build strong relationships. In a strong relationship, if everyone puts their best effort, the credit goes to everyone.

5) **Smart people know it's okay to be rejected**: They know that not all requests for help will be granted. When a request is rejected, even from a long-term partner or friend, they don't take it personally. They know that the other person rejected their request; not them.

Do you ask for help? Do you help? What has your experience? Would you mind sharing?

## Continue Reading

* [Have you changed your mind lately?](/changed-mind/)
* [How To Ask For Help And Get It?](/how-to-ask-for-help-and-get-it/)
* [Common Traps In Getting Meaningful Feedback](/common-traps-in-getting-meaningful-feedback/)