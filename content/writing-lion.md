---
title="How to become a writing LION?"
slug="writing-lion"
excerpt="A simple formula to make you a better writer"
tags=["wins","networking","insights","gwshow"]
type="post"
publish_at="24 Feb 21 07:30 IST"
featured_image="https://cdn.olai.in/jjude/writing-lion-gen.jpg"
---


What skill will be in demand as we grow into remote work more and more?  
  
What skill should you develop, if you want to stay in your hometown but work for big companies?  
  
Writing. Yes, you read that right. It is writing.  
  
We already write emails, slack messages, basecamp threads. In remote working, it is only going to increase.  
  
Here is a simple formula to make you a writing LION.

### Learn

Every writer is a learner. Use [Consume-Produce-Engage](/sdl/) framework to learn, unlearn, and relearn. 

Learn from books, videos, and workshops. If possible learn from books that have stood the test of time.

Don’t learn only from one discipline. Develop a multi-disciplinary learning list. When you understand the basic principles of different fields you will develop a rich toolset to draw for your writing. 

### Innovate

In the startup world there is a concept called bundling and unbundling. You can embrace that model for writing. When you read an article from the author you admire, unbundle the article into multiple topics. In the same way, assemble ideas from multiple sources to write an article in your voice.

### Outwork

I searched for a shortcut to success. I have read, listened, and talked to many successful people. Everyone chants the same mantra - there is no shortcut to hard work.

If you want to succeed, you should be willing to outwork anyone in your chosen field. Always Look for opportunities to write. Write on you blog; write on LinkedIn; write on Twitter; write on your company’s internal knowledge tool.

As you draw from the well of opportunities, it will continue to spring more and more. As [Mathew effect](https://en.wikipedia.org/wiki/Matthew_effect) shows, anyone who has more will be given more. Go grab more. 

### Network

All our success is social. Share your writing with your mentors, peers, and juniors. Use your articles as a base for networking. Words open doors which you didn’t know it existed. 

### Act on this learning

Now that you have learned this simple formula, go to LinkedIn and post a summary of this article. Or bundle it with another article you read and post in your blog. Don’t forget to send me the link. 

Go LION. Go.

_I learned this model from [PK Khurana](https://www.pkkhurana.com/). He shared this model in the 2nd episode of [Gravitas WINS Show](https://www.youtube.com/watch?v=77d5nrUDWxc)._

## Continue Reading

* [Are You Learning A New Domain? Visit Its Zoo](/are-you-learning-a-new-domain-visit-its-zoo/)
* [How I Use Twitter As A Learning Tool](/learn-via-twitter/)
* [Do Credentials Matter?](/do-credentials-matter/)
