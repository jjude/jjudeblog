---
title="How to get lucky?"
slug="luck"
excerpt="When you understand there are different types of luck, you can benefit from them"
tags=["wins","systems","gwradio","luck"]
type= "post"
publish_at= "14 Oct 20 09:00 IST"
featured_image="https://cdn.olai.in/jjude/types-of-luck.png"
bsky_id="3lbtqfyb5z22l"
---

> The horse should be prepared for the battle, but the victory comes from above - Jewish Proverb

I spent my infant days in a leaky hut. I wish to say I worked hard to move from that leaky hut to the CTO office. The truth is, I was lucky.

I was lucky that I graduated without a burden of debt weighing on me.     
I was lucky to graduate amidst economic liberation in India and the software revolution across the world.    
I was lucky to come across a mentor who changed my career's course not once or twice, but thrice.    

I am not saying I didn't work hard. Every day I wake up at 5.30 in the morning and read a book or write an article or code a part of a software. I have followed this routine for the past twenty-five years. Yet, the fact remains that my life is shaped by luck as much as by my hard-work.

<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless src="https://share.transistor.fm/e/20f1027c"></iframe>

James H. Austin classifies four types of chances (the word Austin uses for luck) in his book [Chase, Chance, and Creativity: The Lucky Art of Novelty](https://amzn.to/36zyMtW). I have benefited from all the four.

![Types of luck](https://cdn.olai.in/jjude/types-of-luck.png)

### Blind Luck

> _In Chance I, the good luck that occurs is entirely accidental. It is pure blind luck that comes with no effort on our part._

I graduated just as the [economic liberalization](https://en.wikipedia.org/wiki/Economic_liberalisation_in_India) started in India. At around the same time, US companies began to outsource to India. Though I had no role to play in neither of the events, I benefited from both events. I went around the world working with some of the industry's finest minds because lady luck embraced me.

### Active Luck

> _In Chance II, something else has been added -- motion. Chance II involves the kind of luck Charles Kettering... had in mind when he said, "Keep on going and chances are you will stumble on something, perhaps when you are least expecting it. I have never heard of anyone stumbling on something sitting down."_

My life is full of happy accidents as I went along doing whatever that interested me without a clear expectation.

I joined a software development company in 1998. In six months, I was in [bench](https://www.quora.com/What-does-sitting-on-the-bench-in-Indian-IT-companies-refer-to). I had time but no particular insight or vision or goal. Instead of sitting idle, I focussed on "sending emails programmatically," a difficult problem then, which I solved because I had enough time. Unknown to me, the company was pursuing CMM certification. The team in charge of getting the accreditation cracked everything except, you guessed it, "sending emails programmatically." I "happened" to have coffee with a team member with whom I casually mentioned my work. One thing led to the other, and the project's manager, Sastry Tumuluri, became my mentor. Through him, I got an opportunity to work in Belgium, later became a consultant to the Indian government, and recently a chief technology officer. All because I refused to sit idle.

### Discerned Luck
> _Chance III involves a special receptivity, discernment, and intuitive grasp of significance unique to one particular recipient. Chance presents only a faint clue, the potential opportunity exists, but it will be overlooked except by that one person uniquely equipped to observe it, visualize it conceptually, and fully grasp its significance. Louis Pasteur characterized it for all time when he said, "Chance favors the prepared mind."_

I [study trends](/trends-in-industry/). Identifying trends help me to build useful and profitable skills. In one such exercise, I realized that the future of jobs would be done by [network of experts](/future-of-jobs/). Once I was convinced about this trend, I looked to benefit from it. Dorie Clark identified three foundational techniques to stand out in a noisy world. She writes in her [HBR article](https://hbr.org/2017/01/what-you-need-to-stand-out-in-a-noisy-world):

> _These are **social proof,** which gives people a reason to listen to you; **content creation**, which allows them to evaluate the quality of your ideas; and your **network**, which allows your ideas to spread._

With that realization, I started blogging and used blogging as a foundation for building my network. Again, one thing led to the other, and I'm now a part-time CTO of an IT services company. The rest of the time, I spend time with my boys and pursuing other interests like writing for newspapers.

### Personality Luck
> _Chance IV comes to you, unsought, because of who you are and how you behave. Fortuitous events occur when you behave in ways that are highly distinctive of you as a person. Distinctive hobbies, personal life styles, and activities peculiar to you as an individual, especially when they operate in domains seemingly far removed from the area of the discovery._

I am a trained software technologist, a voracious reader, and a persistent writer. And my parents passed their teaching genes to me. Like you, I'm a multitude and a conglomerate. I show all of it in the [pages](/posts/) of this blog.

Tracking my career trajectory, my friend [Ananthakrishnan](https://www.linkedin.com/in/ananthakrishnan-n/) asked me to coach him. What started as weekly calls became a course. Word got out, and more students got attracted. Now I've formalized the course and launching it for the public under "[Gravitas WINS](/gravitaswins/)" title.

### How to prepare for luck?

Learning how to [think in a structured way](/structured-thinking/) is the best intellectual asset you can build for yourself. "Consume, Produce, and Engage" is the [framework](/sdl/) you can follow to prepare yourself for the four types of luck in your life. 

**Consume**: Consume books, documentaries, videos, courses in a wide array of topics. I'm a technologist. But I regularly learn about psychology, theology, frameworks, and investing. Magic happens when you can draw ideas from multiple domains.

**Produce**: Always, always favor action. Write articles, create videos, and synthesize what you read. Do not chase perfection. As a maker [focus on quantity](/quantity-vs-quality/), not on quality.

**Engage**: As Derek Sivers [says](https://sive.rs/syk), all your breakthroughs come from people. So share your creations with others. Ask for their feedback or ask them to share with others. Use your articles and videos as foundations to connect and engage with people.

### In Summary

Luck plays a significant role in your success. Good news is you can be lucky.

Be active     
Learn to discern     
Display your personality

Now go and be lucky.

## Continue Reading

* [Luck Loves The Paranoid](/luck-loves-paranoid/)
* [Life Is Series Of Lost, Found, And Lucky Breaks](/life-is-lost-and-found/)
* [Approximately Correct](/approximate/)
