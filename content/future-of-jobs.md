---
title="I have seen the future of jobs and it is Hollywood model"
slug="future-of-jobs"
excerpt="We will collaborate to create and capture value. Are you ready for the change?"
tags=["coach"]
type="post"
publish_at="25 Jan 18 16:58 IST"
featured_image="https://cdn.olai.in/jjude/future-of-jobs.png"
---
A.R. Rahman and Danny Boyle came together first for the popular movie Slumdog Millionaire. The movie won eight Academy Awards and seven BAFTA awards. Since then they have worked together many times.

As players in Hollywood, Rahman and Danny Boyle don't work for each other. In fact, they don't work for anyone. "They are **jobless**," says Antonio Paraiso in his _[Porto Business School lecture](https://www.youtube.com/watch?v=1HIVBmjtt5g)_ discussing the Hollywood model of working. As he explains in his lecture, they **have a career, not jobs**. They don't work for a company, they work on interesting projects.

In the traditional IT business, companies hire employees for **long-term**. They may undergo training or work on client projects or stay on "bench" or retrained or asked to leave. It is common to see employees working in the same company for decades. As long as they work in a company, employees work only on **projects within the company** that employs them.

![Future of Jobs](https://cdn.olai.in/jjude/future-of-jobs.png "Future of Jobs")

Hollywood flips this model. No studio employs director or musician or editor on a long-term basis. As Antonio says, those in Hollywood **don't have jobs; they work on projects**. Studios identify a project and bring together a suitable team for that project. The team works together as long as needed to complete the project—whether it is six months or three years. Members may come and go as needed. Some members may work exclusively on a project; some may work on many projects simultaneously. They work together to **create value for themselves and others**. Some get paid; some take a share in profit. When that project is completed, they move on. They may or may not work together again.

**Don't confuse Hollywood model with Upwork model**. [Upwork](http://upwork.com/) is a marketplace where faceless freelancers and clients congregate to get work done as cheaply as possible. Time and again they prove the saying, if you pay peanuts, you will get a monkey. Upwork works only for tasks that can be described to the minutest detail which a single person can handle. Upwork is not a suitable for composing a team for creative and intelligent work.

Hollywood model is nowadays embraced outside the movie industry.

### Governments embrace Hollywood Model for their premier projects

The government of India (GoI) adopted this model for [**UIDAI**](https://uidai.gov.in/) project, the largest biometric ID system. Once GoI identified the project, it appointed [Nandan Nilekani](https://en.wikipedia.org/wiki/Nandan_Nilekani) to head project implementation. Nobody had a doubt if Nandan was a government employee. He wasn't. He completed his assignment and moved on.

GoI also brought-in [Amit Ranjan](https://www.linkedin.com/in/amitranjanprofile/), **co-founder of Slideshare** on the same model. He is currently with the e-Governance division of GoI architecting National DigiLocker Project.

**National Institute for Smart Government**, [NISG](https://www.nisg.org/) brings experts to work on GoI projects. Through NISG, I became part of e-governance division of Ministry of Corporate Affairs, GoI. I designed and managed the e-Governance implementation for Limited Liability Partnership (LLP) Act, India. I was not a Government employee. I was **an independent IT consultant working with independent consultants** from other domains.

There are **examples from other governments too**. [Taavi Kotka](https://en.wikipedia.org/wiki/Taavi_Kotka) was the managing director of the largest software development company in the Baltic region. The government of Estonia appointed him as CIO to architect the popular e-residency program of Estonia.

### Startup ecosystem embraces Hollywood model

Startup ecosystems are also adopting this model. By design, startups have to achieve more with less. Smart ones are experimenting with project-based on-demand expertise.

[Mohit Bansal](https://www.linkedin.com/in/mbansal14/) is a master story-teller. He works with startups to polish their pitch into gorgeous looking presentations.

VC firms have small core team managing strategy. For everything else, they form a project team which disbands after project completion. I have been part of due-diligence teams to validate architecture of startups.

### Time for companies to embrace this model is now

Technology is penetrating into every domain. Newer technologies like blockchain and newer regulations like GDPR are emerging at a pace never seen before. Such changes **disrupt but open enormous opportunities**. It is not possible for any company to develop necessary talents in-house to exploit these opportunities. Only way companies can thrive is to embrace this Hollywood style of project-based value creation.

### What it means for companies

From the days of Henry Ford, entrepreneurs are good at spotting **opportunities to create value**. So far, the only way to create value has been to recruit employees and place them under command-and-control hierarchy. Companies are realizing that the traditional corporate hierarchy limits progress.

Corporate Rebels is a group of four, documenting how progressive organizations organize work in radically different ways. In an article titled, _[Destroy The Hierarchal Pyramid And Build a Powerful Network of Teams](https://corporate-rebels.com/rebel-trends-2-network-of-teams/)_, they write (emphasis mine):

> They have evolved themselves from structures that look like static slow-moving pyramids to something that looks more like a **flexible and fast-moving swarm of start-ups**. We have witnessed them in all kinds of shapes and sizes, all called slightly different. [Spotify](https://corporate-rebels.com/spotify-1/) talks about squads and tribes. Buurtzorg about self-governing teams. Stanley McCrystal about a team of teams. [Finext](https://corporate-rebels.com/finext/) and [Incentro](https://corporate-rebels.com/rebellious-practices-salary/) about cells. And [FAVI](https://corporate-rebels.com/zobrist/) calls them mini-factories.

Progressive organizations already embrace the idea of "network of teams".

If the future is project-based value creation, companies will have to **compose a network of experts to create value**.

Tom Goodwin, senior vice president of strategy and innovation at [Havas Media](https://www.havasmedia.com), wrote in [TechCrunch](https://techcrunch.com/2015/03/03/in-the-age-of-disintermediation-the-battle-is-all-for-the-customer-interface/)

> Uber, the world’s largest taxi company, owns no vehicles. Facebook, the world’s most popular media owner, creates no content. Alibaba, the most valuable retailer, has no inventory. And Airbnb, the world’s largest accommodation provider, owns no real estate. Something interesting is happening.

In the lines of Uber, Airbnb, Facebook, and Alibaba, next billion dollar company won't have any employees. There will be owners, **skill composers**, and experts, who collaborate on-need basis. This is going to lead to another set of service marketplace, a B2P (business to professionals) service.

### What it means for employees

Until now, if you graduated from an engineering college you could get into a software company. Once you got into a company, if you worked hard you had a good chance of growing up in that company.

Not any more.

Dorie Clark is an author of two books on this subject — "Reinventing You" and "Stand Out". She identified three foundational techniques to stand out in a noisy world. She writes in her [HBR article](https://hbr.org/2017/01/what-you-need-to-stand-out-in-a-noisy-world):

> These are **social proof**, which gives people a reason to listen to you; **content creation**, which allows them to evaluate the quality of your ideas; and your **network**, which allows your ideas to spread.

Your network plays an important role if you want to work in Hollywood model. Who-knows-whom is an important aspect in this model. Still you need to create a portfolio and put it out in public. This in-turn will enlarge your network, as new people will come in contact with your work.

Under the guise of scientific management, holistic expertise degraded into narrow specialization. As the world becomes collaborative, we all should develop inter-disciplinary skills. Specifically, we should become "T" shaped experts — with deep expertise in few areas and basic understanding of other associated areas.

As Robert Heinlein said:

> "A human being should be able to change a diaper, plan an invasion, butcher a hog, conn a ship, design a building, write a sonnet, balance accounts, build a wall, set a bone, comfort the dying, take orders, give orders, cooperate, act alone, solve equations, analyze a new problem, pitch manure, program a computer, cook a tasty meal, fight efficiently, die gallantly. **Specialization is for insects**."

Peter Merel reframed it for developers like this:

> A programmer should be able to fix a bug, market an application, maintain a legacy, lead a team, design an architecture, hack a kernel, schedule a project, craft a class, route a network, give a reference, take orders, give orders, use configuration management, prototype, apply patterns, innovate, write documentation, support users, create a cool web-site, email efficiently, resign smoothly. Specialization is for recruiters.

### What it means for governments

Ann-Marie Slaughter, CEO of New America, argues in a World Economic Forum [article](https://www.weforum.org/agenda/2017/02/government-responsibility-to-citizens-anne-marie-slaughter/) that the **essential responsibilities of governments are to protect and provide**.

In the past, government as a provider meant **viewing government as a vending machine**—we pay taxes to get our services. In the future, "… **government should invest in citizen capabilities** to enable them to provide for themselves in rapidly and continually changing circumstances".

Ann-Marie argues that one such investment is a creation of **government platforms** "where citizens can shop intelligently and efficiently for everything from health insurance to educational opportunities to business licenses and **potential business partners**".

Tim O'Reilly has been talking about government as a platform for a decade now. He writes in [Open Government](http://chimera.labs.oreilly.com/books/1234000000774/ch02.html) book, which is available for free:

> In the vending machine model, the full menu of available services is determined beforehand. A small number of vendors have the ability to get their products into the machine, and as a result, the choices are limited, and the prices are high. A bazaar, by contrast, is a place **where the community itself exchanges goods and services**.

By embracing such a platform model, **Estonia has become the land of the future**. Its government platform is called [x-road](https://e-estonia.com/solutions/interoperability-services/x-road/). The intricacies of the platform are interesting only to software engineers. For everyone else, what Estonia has done with the platform is far more interesting. Since 2000, Estonia has built citizen services one by one on top of this platform—taxes, health records, land registration, company registration, and voting.

These early building blocks set the stage for e-residency. Estonia is the first and the only country to offer e-residency—a digital identity available for anyone across the world to conduct **location-independent digital business**.

As Estonia's deputy secretary for economic development, Viljar Lubi, [says](https://www.newyorker.com/magazine/2017/12/18/estonia-the-digital-republic), "**Innovation happens anyway. If we close ourselves off, the innovation happens somewhere else.**"

Governments can simplify governance and invest in government platforms or shoo away the future.

### Gospel for some, tragedy for others

Hollywood model won't be a good news for all. History will repeat. Those who can adapt will succeed. Others might pop up in a song.

The winner takes it all   
The loser has to fall   
It's simple and it's plain    
Why should I complain

## Continue Reading

* [Life is series of lost, found, and lucky breaks](/life-is-lost-and-found/)
* [11 Lives to Build a Rich Career](/11-lives/)
* [Mastering sales as a new CTO](/cto-sales/)