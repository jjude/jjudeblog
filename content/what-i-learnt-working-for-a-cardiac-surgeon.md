---
title="Four Lessons I Learned While Working For A Cardiac Surgeon"
slug="what-i-learnt-working-for-a-cardiac-surgeon"
excerpt="Four lessons I learned while developing an IT system for a cardiac surgeon."
tags=["coach","insights"]
type="post"
publish_at="24 Jun 10 16:30 IST"
featured_image="https://cdn.olai.in/jjude/what-i-learnt-working-for-a-cardiac-surgeon-gen.jpg"
---

If I would've followed the trend, I would've been a programmer in a multi-national software company. But I took 'the other road' and became an IT consultant to Dr M R Girinath, a well renowned cardiac surgeon.

He was known to bring the best of his 'boys' and I'm glad to have been part of his team.

Six Sigma & Lean thinking were unheard of those days. But he initiated a 'lean process' to optimize pre-op, on-the-table & post-op processes. I was in-charge of anything to do with computer - from hardware purchase to software development to training. In short I was his CIO!

In those two years, I learned more than what I would've learned in any management course.

An important lesson that I have carried with me until today is to learn **to speak in their language**. I realized that to be successful, it is mandatory to understand their domain and learn their jargons instead of expecting them to learn my - software - language. So I did. Internet wasn't available as prevalent as today and so I would spend hours in the British council library & their cardiac library learning all that I can about cardiac field. I even went to watch a cardiac surgery (I didn't sleep for two days after that. But the surgeon said that I did better as he is used to interns fainting in the theatre). Speaking customer’s language helped me then and it still helps me.

I also learned an equally important lesson about **importance of nurturing people** by observing his people skills. He had a knack of getting the best and get the best of them. His team is composed of those who were with him for more than a decade. He is also known to have mentored many surgeons who have left his team to head cardiac units elsewhere.

Not only Dr MRG, but his whole team was **hard working**. They seemed to have engraved 'there is no substitute for hard work' in them. Many days they would start at 7 or 8 in the morning and go well into late evening. But I didn’t hear them complain about their work as much as I would hear from my software colleagues.

Another admirable character I observed was their **responsiveness**. They knew they were dealing with life & death. I wouldn't say all of them were saints but at least those whom I witnessed had empathy. I once heard a senior surgeon say to a duty nurse who spoke dismissively to the family of a decessed, 'That might be a 100th case for you; but it is a 100% loss for them. Keep that in mind while talking to the family.'

I am lucky to have worked in a non-IT environment in my formative years and that too as a solo-practitioner. These practical lessons were so valuable and they continue to differentiate me.

