---
title="Future Doesn't Happen To Us, Future Happens Because Of Us"
slug="shape-the-future"
excerpt="If you don't know what is coming, how can you prepare? If you are not prepared, how can you spot the opportunities?"
tags=["coach","wins"]
type="post"
publish_at="26 Jul 21 15:38 IST"
featured_image="https://cdn.olai.in/jjude/futurecast.png"
---
### Lady Luck Smiles On The Prepared

"Luck happens when preparation meets opportunity," said Seneca, the ancient Roman philosopher. There are three factors in his claim: luck, preparation, and opportunity.

Opportunity knocks on everyone's door. Yet many of us let go of opportunities because we are not prepared to recognize them. Only a prepared person can spot, seize, and capitalize on opportunities that pass by. When we don't know what is coming, we don't prepare. As a result of not knowing what is coming, we react badly losing great opportunities.

In order to discover the opportunities on the edges, I use a framework. Each year, I prepare for the coming year using this framework.  Every time there is a large-scale event, like Covid, that presents newer opportunities that weren't available earlier, I sit down and think through using this framework.

### Start Where You Are Today

Write down your current strengths, achievements, aspirations, family situation, and career goals.

Even though you can project for a longer duration, three years is a good time frame.

In my case, I am almost 50 years old with a 12-year old son and serve as a chief technology officer. 

![Scenario thinking to prepare for the future](https://cdn.olai.in/jjude/futurecast.png)

### Projected

The question you are asking yourself is what will my life be like if nothing changes for next three years.

I don't suffer from hypertension, diabetes, or any other condition. I should be able to live healthy for another three years unless I get hit by a bus. However, I anticipate that as I age, I will have to deal with other health issues. 

My son will complete 10th standard in another three years.

Given these factors, what is the projected future like?

- Do the same thing every day for three more years as a CTO
- Start taking health more seriously by walking and reducing sweets.
- Increase the health insurance coverage
- Start to look for colleges for my son
- Set up a "college fund" and begin to fill it

### Plausible

Then there are plausible future scenarios that are within my control. In addition to the projected future scenarios, what actions can I take?

My son loves playing Roblox games and creating his own. Possibly I can teach him Lua programming so he can build better Roblox games, then switch to software programming.

I can take courses on microservices, Kubernetes, and Svelte. As the software industry continues to evolve, I can stay current. Maybe I can even learn a few no-code platforms.

### Possible

There are always trends at the periphery that will become the norm in a few years. You study these trends in this phase. Then, choose the trends that will give you the most benefit. Alternatively, identify trends that might negatively affect you and figure out how you can minimize their effect. 

It is probably better for me to embrace no-code platforms now before they become a threat to my job in the future.

Regarding my son, Roblox recently got listed. It is highly likely that the platform will become more popular. I encourage him to think beyond just creating games to the possibility of creating merchandise with the platform. 

### Preposterous

Imagine a crazy future scenario that you can shape if you put in 10x the effort? Can you predict a future in which you have many upsides even if you fail?

Can I build an app for elders (since elders have a lot of money as per social trends) using a no-code platform? Could I encourage my son to start a game company rather than go to college?

If I have any chance of riding a successful wave, I should be able to put in the effort.


### What If Everything Fails?

It is a very high probability. Guess what? By preparing for a preposterous future scenario and failing, I will end up in a far better future than I predicted.

### Quote To Ponder

When I have a good quarterly conference call with Wall Street, people will stop me and say, “Congratulations on your quarter,” and I say, “Thank you,” but what I’m really thinking is **that quarter was baked three years ago**.  - Jeff Bezos

### Actions You Can Take

#### 1. Identify The Trends

Read through [Trends As A Guide To The Future](/trends21/) and then think of the trends that could have impact on you.


<table class="f6 w-100 mw8 center" cellspacing="0">
<thead>
<tr>
<th class="fw6 bb b--black-20 tl pb3 pr3 bg-white">Sector</th>
<th class="fw6 bb b--black-20 tl pb3 pr3 bg-white">Trend</th>
<th class="fw6 bb b--black-20 tl pb3 pr3 bg-white">Impact</th></tr>
</thead>
<tbody class="lh-copy">
<tr>
<td class="pv3 pr3 bb b--black-20">Politics</td>
<td class="pv3 pr3 bb b--black-20"></td>
<td class="pv3 pr3 bb b--black-20"></td>
</tr>
<tr>
<td class="pv3 pr3 bb b--black-20">Economy</td><td class="pv3 pr3 bb b--black-20"></td><td class="pv3 pr3 bb b--black-20"></td></tr>
<tr><td class="pv3 pr3 bb b--black-20">Social</td><td class="pv3 pr3 bb b--black-20"></td><td class="pv3 pr3 bb b--black-20"></td></tr>
<tr><td class="pv3 pr3 bb b--black-20">Technology</td><td class="pv3 pr3 bb b--black-20"></td><td class="pv3 pr3 bb b--black-20"></td></tr>
<tr><td class="pv3 pr3 bb b--black-20">Environment</td><td class="pv3 pr3 bb b--black-20"></td><td class="pv3 pr3 bb b--black-20"></td></tr>
<tr><td class="pv3 pr3 bb b--black-20">Legal</td><td class="pv3 pr3 bb b--black-20"></td><td class="pv3 pr3 bb b--black-20"></td></tr>
</tbody>
</table>

#### 2. Plan Future Scenarios

<table class="f6 w-100 mw8 center" cellspacing="0">
<thead>
<tr><th class="fw6 bb b--black-20 tl pb3 pr3 bg-white">Type</th>
<th class="fw6 bb b--black-20 tl pb3 pr3 bg-white">Scenarios</th></tr>
</thead>
<tbody class="lh-copy">
<tr><td  class="pv3 pr3 bb b--black-20">Projected</td><td class="pv3 pr3 bb b--black-20"></td></tr>
<tr><td  class="pv3 pr3 bb b--black-20">Plausible</td><td class="pv3 pr3 bb b--black-20"></td></tr>
<tr><td  class="pv3 pr3 bb b--black-20">Possible</td><td class="pv3 pr3 bb b--black-20"></td></tr>
<tr><td  class="pv3 pr3 bb b--black-20">Preposterous</td class="pv3 pr3 bb b--black-20"><td></td></tr>
</tbody>
</table>


Need help? I teach these as part of [Gravitas WINS course](https://gravitaswins.com/). Shape your future with the next cohort.

### Continue Your Quest

* [Watch a live session where I teach this concept](https://www.youtube.com/watch?v=2koBIwFM4Mk)
* [Trends As A Guide To The Future](/trends21/)
* [Four Types Of Luck And How To Prepare For Each Of Them](/luck/)