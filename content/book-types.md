---
title="Three types of books as per Elon Musk"
slug="book-types"
excerpt="Categorize books as sort of a semantic tree"
tags=["sdl","coach"]
type="post"
publish_at="11 Nov 20 09:00 IST"
featured_image="https://cdn.olai.in/jjude/types-of-books.jpg"
---

Elon Musk said in a recent "Ask me anything" session:

> it is important to view knowledge as sort of a semantic tree -- make sure you understand the fundamental principles, ie the trunk and big branches, before you get into the leaves/details or there is nothing for them to hang on to.
[Elon Musk on learning](https://www.reddit.com/r/IAmA/comments/2rgsan/i_am_elon_musk_ceocto_of_a_rocket_company_ama/cnfre0a/)

<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless src="https://share.transistor.fm/e/006dd1c0"></iframe>

We can use Elon's idea to categorize books into three types:
- Trunk
- Branch
- Leaf
### Trunk Books

A trunk book explains the foundational principles of a field or a domain. 

Ram Charan explains the essence of business thinking in [What the CEO wants you to know](https://amzn.to/2IhkkN1), Steven Covey laid the foundation for practical living with [The 7 Habits of Highly Effective People](https://amzn.to/3p1nfut), and William Zinsser offers fundamental principles to become a writer in [On Writing Well](https://amzn.to/32mFq3I). Holy books of all religions are trunk books as they lay out the guiding principles for their followers. 

You should read trunk books in physical format, with a pencil and papers to take copious notes. As the content of these books remains relevant through the ages, you should re-read them often.

![Three types of books as per Elon Musk](https://cdn.olai.in/jjude/types-of-books.jpg)
### Branch Books
 
Branch books deal with one area of the field. The McGraw-Hill's [Finance for Non-Financial Managers](https://amzn.to/2Ia2ziW) teaches corporate finance to business managers. It doesn't teach the whole finance domain nor running a business.

Similarly, Christopher Booker wrote [The Seven Basic Plots](https://amzn.to/3ezXmwH) to teach fiction writing.

Branch books are equally important, like trunk books. So read them much like the trunk books.

### Leaf Books

Leaf books teach you a specific skill, like [Excel 2019 All-In-One For Dummies](https://amzn.to/3k69hDP) teaches you Excel 2019. It is useful and probably helps you make some money too. However, it becomes completely useless when the next version of Excel comes out. Leaf books teach a skill that is useful in a specific context. You don't find principles that you can use across tools, domains, and life areas.

[Membership Economy](https://amzn.to/36eflFq) by Robbie Baxter is another leaf book. Such leaf books teach you all you need to know about an emerging trend. But to build a successful venture out of the trend, you still need trunk and branch knowledge.

It is possible to start with a leaf book and go down the journey into building trunk knowledge. You might begin with Excel 2019. Because of early success with it, you might become interested in data visualization in Excel and then onto data modeling using it.

Since leaf books are transient, it doesn't make sense to buy them. Better to read them through a subscription service like [Scribd](https://www.scribd.com/) or [Oreilly Online learning](https://www.oreilly.com/online-learning/). 

### You need them all

You gain from all three types of books. 

Even leaf books give you short-term gains. You need to be cautious because you could chase one shiny object after another, never building a strong foundation to build lasting success.

If you aspire to stay successful, you should lay a strong foundation on solid principles. Only trunk and branch books teach you those rock-solid principles.

## Continue Reading

* [How to get lucky?](/luck/)    
* [Principles trump processes](/principles-trumps/)    
* [Build on the rock for the storms are surely coming](/build-on-rock/)