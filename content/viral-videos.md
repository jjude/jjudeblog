---
title="Let Your Videos Go Viral"
slug="viral-videos"
excerpt="What I gained from the book - Fast, Cheap, and Viral"
tags=["books","coach"]
type="post"
publish_at="24 Aug 21 20:56 IST"
featured_image="https://cdn.olai.in/jjude/fast-cheap-viral.jpg"
---

A lot of us want to build our personal brand on Linkedin, Twitter, blogs, and videos. But we're hesitant because we're not great with cameras or linguistics. In his book, [Fast, Cheap, and Viral](https://www.amazon.in/dp/9351952754/), Aashish debunks the mental limitations

### About the author

[Aashish](https://www.linkedin.com/in/acway/) is the Vice President of Content Marketing at Ixigo, an online travel agency. His team created several viral videos.

One example is, [Stuff you can take from hotels](https://www.youtube.com/watch?v=zc504j0cpkI). It was made at a budget of ₹7000 and it was so popular that it was played on Zee TV as well as on [Whoopi Goldberg's talk show](https://twitter.com/ixigo/status/1156868503607136256). How's this for going viral on a budget! He is a practitioner, not an observer.

This book is based on his experiments in Ixigo. 

### About the book

Aashish illustrated each concept with beautiful illustrations that are easy to understand. Each topic has plenty of examples. Because they are all videos by Ixigo, you can google them and watch them to understand what Aashish is talking about.

In the chapter titled, "Method to the Madness," Aashish discusses the process of making viral videos. I loved it as a believer in systems thinking.

Branding or marketing enthusiasts will find this book useful and interesting.

Below are my notes from the book, in no particular order.

![Fast Cheap Viral Book](https://cdn.olai.in/jjude/fast-cheap-viral.jpg "Fast Cheap Viral Book")

### Virality is like a joke

You can **work on the structure of the joke**, but you can't force people to laugh. Viral videos are similar. A virality is a result. **Process is within your control, but outcome is not**.

### Opportunity vs Challenge

There is a great deal of video content created and uploaded to social media. Social media platforms display videos relevant to their users based on algorithms. It is a massive opportunity for you as well, as rising tides lift all boats. It is possible to ride the rising wave if you create relatable content.

### Only thing that matters: BS or no-BS

We categorize our content into short form, long form, slides, videos, etc. Consumers have only two categories - bullshit content and non-bullshit content.

They apply the same filter to a Netflix movie with a million-dollar budget and a video shot on a cheap smartphone. The only question you need to ask yourself is: **will my content pass that bullshit filter?**

### Method To The Madness

#### 1) Viral = Share Worthy

If you share your video on a social media platform, they don't have an incentive to show your video. If your first-level contacts start sharing it, the platforms have no choice but to display it as a trending video. 

Because these platforms want to keep their users on their platform, their algorithms select videos uploaded to them. Upload videos to each platform rather than uploading on one (such as YouTube) and linking from another.

#### 2) Mobile Is The New Theater

Your audience is not watching the video in a theatre with full attention. Every piece of content is watched on a mobile device. Therefore, your content should be suitable for tiny screens. Attention spans on mobile devices are very short.

Your videos should be square and centered, like a selfie. Come to the point within three to six seconds and focus on the storytelling.

#### 3) Distribution Strategy

If you can get 100 organic shares within the first three hours, there is a good chance social media platforms will share the video. If your immediate community does not love you, how can anyone else love you?

#### 4) Conversations before Campaigns

Conversations are at the heart of social media. Hence, ask what conversations will your content start. Spending money on a campaign makes sense if your video generates a lot of conversations.

#### 5) Be Authentic = Not An Ad

Nobody cares about you, your product, or your service. Ads shout me, me, me. Your content should be about your audience. If your content solves their problem and celebrates their life, they will become your evangelists.

Remember this thumb rule: it is not about a hotel in Goa, but people who go to Goa.

#### 6) Topics That Are Share-Worthy
- **Inspirational**
- **Useful**: List out pain points of your users and solve them creatively. After watching / reading, users should be able to do something concrete to solve their problem. Then it will be shared with the peer groups.
- **Celebrate** their life: If you can't solve the pain point, celebrate it.
- **Topical**: Small shelf life. If you make a video about April 1st, nobody wants to see it on the 2nd. For a video to ride on the topical, the topic has to be breaking news. Not a small trend.
- **Change the world**: You have the power to change the world with every tweet, video, and post. (quotes)

### How It Helped Me

After reading the book, I watched a lot of popular videos on YouTube and Instagram and thought, "I can do that."

As long as the content is entertaining and educational, I don't need to have the perfect video effects or language. After overcoming my inhibitions and fears, I started creating video content.

As an example, I created an [interview with my sons](https://www.youtube.com/watch?v=iKE7gwMAleE) about the games they play on Roblox. I used the mobile and collar mics we had. I was worried because there was a lot of noise. However, within one month of release, it has more than 280 views and is one of the most popular videos on the channel.

To support my course, Gravitas WINS, I created podcast interviews, book club discussions, and webinars. I am managing them all with whatever infrastructure I have at home. 

I have created 30 weekly shows, 20 podcasts, and 15 book discussions. LinkedIn, YouTube, and Facebook now stream the weekly shows. Although they aren't popular, the content I'm producing has increased my reach, and I'm able to create a following of my own.

I now upload videos natively to each platform or stream them. The engagement has gone up as a result. I recently uploaded [a video](https://www.linkedin.com/posts/jjude_start-your-weekend-with-the-best-developer-activity-6834658439042519040-30YL) to LinkedIn (even though I did not create it). It has been re-shared 16 times and has gained 1250 views and 32 likes.

Following, "it is not about the hotel in Goa, but travelers going to Goa" the topics for the Thursday shows are the pain points of the prospective students, it is not about the course. That's why I think people attend the show every week.

The movie, "shall we dance" includes a scene where Jennifer Lopaz tells Richard Gere, "she is the picture and you are the frame; everything you do should show her off". After reading the book, I understood that.

Overall, the book influenced me in continuing to create and be comfortable on video. I have been able to get more DOPE - dollars, opportunities, positions, and esteem.

### Quote To Ponder
_If you’re consistently producing, creating and experimenting with content, your chances of winning big increase - Aashish Chopra._

### Keep Exploring

* [Gravitas WINS Course](/gwcourse/)
* [Gravitas WINS Radio](/podcast/)
* [Gravitas WINS Shows](/gwshow/)