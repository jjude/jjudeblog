---
title="Teaching storytelling to kids"
slug="storytelling-to-kids"
excerpt="Can I teach that one skill that will be in demand even after two decades?"
tags=["parenting","sdl","homeschool"]
type= "post"
publish_at= "06 Oct 19 06:00 IST"
featured_image="https://cdn.olai.in/jjude/storytelling.jpg"

---

!["Teaching storytelling to kids"](https://cdn.olai.in/jjude/storytelling.jpg)

Sir Ken Robinson, in his famous [TED talk][1], made this comment:

> Children starting school this year will be retiring in 2065. Nobody has a clue, what the world will look like in five years' time. And yet, we're meant to be educating them for it.

As a father of two sons aged seven and ten, I think about this comment often. The Indian school curriculum is [inadequate][2] to meet the demands of the industry. So the burden of preparing them falls on the parents.

How can I deduce skills that will be useful in fifteen, twenty years from now?

As I struggled to find an answer, I stumbled on a question that Jeff Bezos, the founder of Amazon asked. A reporter [asked][3] Jeff Bezos, "Jeff, what do you think is going to change most in the next ten years?" Jeff inverted the question and replied, "That's a good question. But a better question is: **What's not going to change in the next 10-20 years?**" He went on to say that from Amazon's perspective, what won't change is people's desire for lower prices and faster delivery.

So what will not change in fifteen years? The basic human needs. They are not going to change. **Even in two decades, every one of us will still long to connect with others to be part of a community**.

History tells us that the communities are formed around stories. The sacred scriptures and epics of major religions are nothing but stories recited over and over again in communities. When these communities moved, they took the stories with them. In a new place, these stories formed new communities.

**Storytelling is one skill that is going to be in demand**, even in twenty, thirty years. Mediums might morph, but the craft will still be in need.

With that epiphany, I started at the dinner table. We always have dinner together as a family. So dinner time seemed to be the right time to play and teach a skill.

At first, I told them stories. Mostly the funny ones, so as not to bore them. Often, they would want me to retell a funny story they loved. I have told a few stories ten, fifteen times in the last two years.

Once they got hold of the structure of storytelling, I asked them to narrate their days in school. **They mimicked me** to narrate funnily. I listened and laughed even when they were boring. Soon they started surprising me with their storytelling skills.

I upped the game when they got better. I created a storytelling game modeled along the lines of [Whose line is it anyway][5]. I will start a story with a sentence. They have to construct the next sentence to keep the story going. And it goes on until we hit, "then they lived happily ever after." Some sessions will go like this:

> "There was a little boy in a village."<br/>
> "He always wanted to play on the beach."<br/>
> "One day his father took him to the beach to play."<br/>
> "The boy was so happy because he built many sand houses on the beach."<br/>
> "When they were coming back, the boy lost his father in a crowd."<br/>
> "The boy cried so loudly, 'appa, appa'."<br/>
> "The father found the boy because the boy cried loudly."<br/>
> "They went home and lived happily ever after."<br/>

**To tell good stories, they need to know good stories.** I enrolled in the British Library so that they could read amazing stories. The elder one, Josh, is readily drawn to reading. He devoured every [Geronimo Stilton][6] book he could find in the library. The younger one, Jerry, is not so much into reading. I tried many different ways to instill a habit of reading in him, yet, he slipped away at the first possible occasion. Since he was interested in cars, I tested him by giving a 'Top Gear' magazine. I wasn't hopeful as I thought he would find it difficult to understand. How often, kids prove you are stupid. He got hooked to car magazines. He couldn't stop reading and talking about his favorite cars. His current favorite car is Bugatti Chiron. Once he told me that a single oil change costs twenty-one thousand dollars. I drive a humble Hyundai i20, which costs half of that. He has promised to take me for a ride in his Chiron when he buys one. I will let you know if he keeps his words.

When they got good at telling stories, **I nudged them to write**. Whenever we came back from a trip, even if it is to a local mall, I asked them to write their experience. I created a [blog][7] to post their writings. Josh wrote about his [favorite teacher](https://blog.garretts.in/favorite-teacher-josh):

> She even convinced my father to let me go on a school trip without my parents. This was my first trip to go without my parents. We went to Hail Himalayas, Himachal Pradesh. There we did lots of fun activities: commando net, zip line, and trekking. There was a reward for the cleanest tent. Our tent got first place. These things made me improve my confidence.

In July of this year, I took them to my college reunion. Jerry carried a note with him and noted every small detail. When he came back, he wrote a detailed [post about the trip](https://blog.garretts.in/cbe-jerry). I learned he has **a keen observing eye** when I read this:

> After sometime we landed in Delhi. It was a windy and rainy day. We went to the taxi point. We took pre-paid taxi but there was a problem. The driver told to my father, for two suitcases it is more than 400 rupees. But my father asked in the counter and confirmed it is only 400 rupees.

At around this time, they got interested in creating **stop motion movies with legos**. They mostly made fight scenes, revealing the sibling rivalry. In the college reunion, one of my college-mate composed a reunion theme song. When kids returned home, Josh created a [stop-motion video for the theme song](https://www.youtube.com/watch?v=ictmMW0_HQA).

**I invested more in the video medium.** I bought a tripod, light-box, and loaned my iPhone. They created mini-movies using mobile iMovie. Jerry astonished me with a [mini-movie](https://www.youtube.com/watch?v=PFDzWwT_cYo) which he scripted, shot, and edited. I want to take credit for it, but truthfully it is all his own making. When you watch it, you will know why I'm brimming with pride.

This is just a beginning–a seed sown in their formative hearts. I’m committed to nurse this seed so it becomes a giant fruitful tree from which they benefit for years to come.

P.S: Visit their [Youtube channel](https://www.youtube.com/channel/UCkMTYnmOdNndliks8c2ueZg/videos). Comment or like a video. Better yet, subscribe to their channel.

[1]: https://www.ted.com/talks/ken_robinson_says_schools_kill_creativity
[2]: /whats-wrong-with-our-educational-system/
[3]: https://www.diamandis.com/blog/what-wont-change-in-20-years
[4]: /vision/
[5]: https://en.wikipedia.org/wiki/Whose_Line_Is_It_Anyway%3F_(American_TV_series)
[6]: https://geronimostilton.com/WW-en/home/?fw=1
[7]: https://blog.garretts.in

## Continue Reading

* ['Homeschooling In India' With Mahendran Kathiresan](/mahendrank/)
* [Why And How I'm Homeschooling My Kids](/why-how-homeschooling/)
* [Sweet Home Is A Safe Home, Even Digitally](/sweet-home-safe-home/)