---
title="Shresth Shrivastav on 'Thrive In Gig Economy'"
slug="shresth-shrivastav-gig-economy"
excerpt="When you become a freelancer, you don't have to lose out on the benefits..."
tags=["coach","gwradio"]
type="post"
publish_at="08 Feb 22 06:00 IST"
featured_image="https://cdn.olai.in/jjude/shresth-shrivastav-gig-economy-gen.jpg"
---

Join me in conversation with the Founder & CEO of BeGig, Shresth Shrivastav. BeGig is an exclusive tech ecosystem, helping freelancers find and bid on exciting tech gigs. You can visit their website at www.begig.io. Shresth and I discuss his journey so far as a solopreneur, building a safety net for gig workers, and what experts should do to thrive in the new economy.

<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless src="https://share.transistor.fm/e/9116e41d"></iframe>

In 2014, I wrote an article saying future of jobs is partnership of experts. In it I argued, that experts will come together much like actors, musicians, and directors come together to make a movie. So when I came across Sresth, who has founded a company in this domain, I couldn't resist inviting him to the conversation. 

Hope today's conversation gives you some perspective about this trend and also help you play along in this domain. 

What you'll learn:
- What is BeGig?
- What benefits will BeGig offer?
- What shifts in society is needed to accept this trend in a positive way
- Will we see fractional CIOs and CTOs?
- Concerns on hiring soloprenuers
- What skills will help me in this new environment?

### Watch on YouTube

<iframe width="560" height="315" src="https://www.youtube.com/embed/DxioSI6yjpE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Connect with me
- Twitter: https://twitter.com/jjude
- LinkedIn: https://www.linkedin.com/in/jjude/
- Website: https://jjude.com/
- Newsletter: https://jjude.com/subscribe/
- Youtube: https://youtube.com/gravitaswins
- Email: podcast@jjude.com
- Executive Coaching Program: https://gravitaswins.com

## Your feedback counts

Thank you for listening. If you enjoy the podcast, would you please leave a short review on Apple podcast or on YouTube? It takes less than 60 seconds, and it really makes a difference in finding this podcast. And it boosts my spirits. 😀

I would like to hear what resonated the most with you in this episode. Either email me or share it on social media and tag me. Thank you for your support.

Have a life of WINS.

_Gravitas WINS Radio is produced by [Joseph Jude](https://jjude.com/). Be sure to subscribe on [Apple Podcast](https://podcasts.apple.com/in/podcast/gravitas-wins-radio/id1562802449), [Spotify](https://open.spotify.com/show/7LOBjAyjBkechMC0xMIj9E),[Google Podcast](https://podcasts.google.com/feed/aHR0cHM6Ly9mZWVkcy50cmFuc2lzdG9yLmZtL2dyYXZpdGFzLXdpbnMtcmFkaW8=),[YouTube](https://www.youtube.com/gravitaswins), or the podcasting app of your choice._

Please check out other episodes of [Gravitas WINS Radio](/gwr-themes/). For easy consumption, they are categorized by themes. If you are a fellow podcaster, you might be interested in [How I Podcast](/how-i-podcast/) or [Tools I Use](/i-use/).

## Continue Reading

* [Future Of Jobs Is Partnership Of Experts](/future-of-jobs/)
* [Future Doesn't Happen To Us, Future Happens Because Of Us](/shape-the-future/)
* [Trends As A Guide To The Future](/trends21/)
