---
title="Are you spending enough on recovery?"
slug="recovery"
excerpt="Knowledge workers need recovery routines too"
tags=["self","wins"]
type="post"
publish_at="10 Feb 21 08:00 IST"
featured_image="https://cdn.olai.in/jjude/recovery-gen.jpg"
---


LeBron spends a [million dollars](https://thesportsrush.com/nba-news-lebron-james-spends-1-million-year-on-just-body-and-mind-recovery-how-lakers-star-remains-injury-free-at-35/) a year on recovery so he can continue to dominate the NBA game. Russel Wilson is another athlete who [spends an insane amount](https://people.com/sports/russell-wilson-reveals-he-spends-1-million-annually-on-his-body/) on recovery, so he can play as long as he can. 

It is easy to get injured as an athlete, so it is essential to focus on recovery. But it is not only about continuing in sports. In an interview with Tim Ferriss, Michael Phelps [said](https://tim.blog/2021/01/23/michael-phelps-grant-hackett-transcript/): 

> for me to be able to do everything I need to do, from playing golf, having enough energy with the kids, and doing everything I need to do personally, I have to recover.

We are knowledge workers, and the mind is our playground. If we need to play until we die, we also need recovery routines.

I don't have the best of the routines yet. I realized the importance of recovery as I was listening to Michael Phelps. I am going to learn in the coming days. These are the routines I have today to recover.

### Spiritual

I read Bible, pray, and meditate daily.

### Mental

Once in a while, I read short stories. As an example, recently, I read "Thick as Thieves" by Ruskin Bond.

I take refresh retreats once a year with family and close friends. It involves jovial debates, a lot of walking, and spending time with nature.

### Physical

- **Sleep**: I sleep for eight hours most days. As I age, Sleep is disturbed once or twice, though.
- **Active Life**: I try to have an active daily life. I walk in the park or work out at home, or cycle.
  
How do you recover? How much do you spend?

Tell me via [twitter](https://twitter.com/jjude) or via [email](https://jjude.in/email)

## Continue Reading

* [Writing my obituary](/my-obituary/)
* [Build on the rock for the storms are surely coming](/build-on-rock/)
* [The dichotomy of contentment and ambition](/contentment-and-ambition/)
