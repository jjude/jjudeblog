---
title="Are you ready for the coming show?"
slug="experience-economy"
excerpt="The world will become one big stage"
tags=["coach","experience"]
type="post"
publish_at="06 Jan 21 08:04 IST"
featured_image="https://cdn.olai.in/jjude/vr-experience.jpg"
---

> People will forget what you said, people will forget what you did, but people will never forget how you made them feel. - Maya Angelou

The last two decades have been about convenience. Developers converted every activity into algorithms and then into apps, improving our lives in tangible ways. 

- Zomato took care of the cooking
- Netflix eliminated going to theatres
- Zoom enabled us to attend meetings in blazers and underwears

These apps are still not perfect, but we continue to get adjusted to their ways.

Now that convenience is covered, most would upgrade to the next level in the hierarchy of wants. I suspect that level is "experience."

### Unevenly distributed future

Joseph Pine and James Gilmore wrote of the emerging experience economy in a [1998 HBR article](https://hbr.org/1998/07/welcome-to-the-experience-economy). They published a  [book](https://amzn.to/2KYDb1d) later on the same topic.

Joseph & James predicted that **"the next competitive battleground lies in staging experiences"** We witness the competition playing out in some areas already. Take drinking coffee, for example.
  
The documentary [Caffinated](https://caffeinated.vhx.tv/) traces the arc of coffee from the undifferentiated coffee seed to a personalized extravagant culinary experience. The coffee-farmers dealing with commoditized seeds earn the same meager amount every year. At the same time, those in the upper stream of the arc, who stage an experience with coffee, reap multifold revenue.

I'm going to give you three examples from different fields to show how the future is already taking shape.

### Education

Teaching kindergarten children is challenging since they can't bring any pre-existing objects as a reference. Shyam Vengalloor, a school teacher in Kerala, is bringing elephants, planets, and astronauts into the classrooms using [augumented reality](https://www.thehindu.com/news/national/kerala/augmented-reality-takes-online-classes-to-exciting-highs/article32014276.ece_). The school children, and teachers, are in awe of what they see as they learn.

Thanks to technology, classes are no more tedious, one-way recital arenas.

### Entertainment

The entertainment industry has been a pioneer in engaging audiences with immersive experiences. 

![VR Experience in Kanyakumari](https://cdn.olai.in/jjude/vr-experience.jpg)

In 2019, I took my kids to Kanyakumari, the southern tip of India. I was expecting to enjoy the sea, sunset, and seafood, the usual pleasures when we were kids. I was surprised to find a VR studio. My kids took the VR show, which turned out to be the memorable aspect of the trip. With a VR headset attached to their eyes, they bounced on the chair and screamed at the top of their voice. When they finished, they couldn't stop smiling and talking about it. Even after a year, they remember that experience more than other events on the trip.

### Politics 

Politicians fear apathy more than bad news. They want to be in the news all the time. The best way to capture and dominate their audiences is through a "staging experience." 

Modi, the Indian Prime minister, has mastered the tact of captivating his electorates. He so dominated the 2014 election with holograms that no party could win enough votes to claim the status of the opposition party. Since then, every appearance is a performance with elaborate props, including dress, hat, and even birds. He has decimated every opposition through careful designing of memorable experiences for his audiences. It is no wonder his popularity is so high even after so many disastrous experiments.

### Up or Out

The coming years will choke the "old-school" undifferentiated performers and award the actors embracing the emerging experience-economy. Employees and executives, poor and elites, citizens and bureaucrats will all crave and stage experiences. The world will become one big stage.

Are you ready for the show?

## Continue Reading

* [Customer Experience Is A Leaf, Not A Trunk](/cx-is-leaf)
* [Delightful Customer Experience in a Highway Restaurant](/cx-in-highway)
* [Can geeks delight?](/geeks-delight)
