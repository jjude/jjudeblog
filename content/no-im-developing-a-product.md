---
title="No, I'm Developing A Product"
slug="no-im-developing-a-product"
excerpt="Say the solo-ISV prayer to focus on the essentials."
tags=["coach","startup"]
type="post"
publish_at="03 Nov 12 16:30 IST"
featured_image="https://cdn.olai.in/jjude/no-im-developing-a-product-gen.jpg"
---

Hey, did you hear about that guy who is falling from the sky? Nothing like this been attempted before. And they are streaming it live man. How cool is that? Are you not watching it?

No, I’m developing a [product](https://olai.in/)

Do you remember the movie ‘[Ramana](https://en.wikipedia.org/wiki/Ramana_(film))?’ How often we, Indians, wished it would be played out in real. Now an [outsider](https://en.wikipedia.org/wiki/Arvind_Kejriwal) to the political circle is exposing scams after scams. Shame on these parties-they have been serving only themselves and their families. Some believe, it may be the greatest revolution since independence struggle. Have you watched his latest press conference? He is all over-press, TV, social media. Have you read them?

No, I’m developing a [product](https://olai.in/)

Aren’t you new to the world of business? You need all the knowledge needed to survive in this rat-race. I read, [Steve Blank](http://steveblank.com/)’s [course on business development](http://steveblank.com/2012/09/06/the-lean-launchpad-online/) is made available online. Shouldn’t you subscribe to it and listen to it first? May be it will help you not to make stupid mistakes. Should I send you the link?

No, I’m developing a [product](https://olai.in/)

<a id="solo-isv-prayer"></a>**_Solo-ISV’s prayer: Lord grant me serenity to focus on the essentials, courage to say no to distractions and wisdom to know the difference._**

