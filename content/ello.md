---
title="Will Ello Survive?"
slug="ello"
excerpt="I believe in the ideals of Ello. We need a better web, where we can communicate, discuss and collaborate without tens of sites prying on to us. Ello has given a hope. I wish it survives."
tags=["networking"]
type="post"
publish_at="03 Dec 14 18:12 IST"
featured_image="https://cdn.olai.in/jjude/ello-gen.jpg"
---

[Ello][1] is a new kid in the social media circle. Ello defines itself as an **ad-free social network** that will never sell its users data to advertisers. While social-media giants readily sell their users data, Ello comes up as noble. But is it possible to build a business around a noble cause?

![Ello](https://cdn.olai.in/jjude/ello_ad.jpg)

[App.net][3] developers tried that. Much like Ello, they started with a 'no-selling-of-user-data' policy. They had a freemium model, released API and built an ecosystem of apps. It gathered stream of paid users into its network. While it was hot, the app.net tribe was sure that it will soon replace twitter. Pity that it dies a **slow death**.

Am I dismissive of Ello? No. Not at all.

I have been using Ello and I quite like it. But there are questions:

- Is it worth spending time on Ello?
- Is it worth inviting my friends over to Ello?
- Will it be there tomorrow?

All these questions can be reduced to, "Is the pro-user, pro-privacy cause a **viable business**?"

[Manton Reece][5], who runs a popular software podcast 'Core Intuition', stopped using Twitter due to its hostile developer policies. He is an avid app.net user, though.

My friend [Sastry][4], deactivated his Facebook account over its privacy policies. Yet, he is on twitter all his waking time.

These may be just two examples. But, if you couple them with steady stream of paid users for App.net, you got a proof for **definitive market for a developer-friendly, user-centred, ad-free social network**.

If there is a market need, how can Ello thrive to be a successful social network? To become one, it needs to become part of daily lives of its users. If it integrates well into daily lives of its users, it will grow its user base. More the users, more the value. More the value, we will gladly invite our friends.

Value comes from three factors:

- Business Model
- Positioning
- Product Features

#### Business Model

Ello has [investment][8] money. Still, if it wants to thrive, it has to **generate revenue on its own**.

T-Shirts and donations will not bring-in enough cash. It needs to come up with a reasonable revenue model. When it has a business model, users will be confident that it will stay in business, which in turn will improve the adaption.

Take for example, [Pinboard][9]. It's a one man venture; Maciej Cegłowski, founder of Pinboard describes it as,

> Pinboard is a fast, no-nonsense bookmarking site for people who value privacy and speed.

He charges a one-time $10 fee and he is in business from 2009. Who would’ve thought a mundane bookmarking service could be a sustaining business?

#### Positioning

So far, Ello has defined itself by what it is not. Imagine, for a moment, that I come to you and introduce myself as "I'm not James Bond." That doesn’t tell a lot about myself. Does it? You would want to know if I’m Jason Bourne or just a chaff in the wind.

Is it Twitter minus ads or Facebook minus ads or Google Plus minus ads? Can I use it as [Medium][7], a publishing tool to post long form articles or is it more useful as [Disqus][6], a commenting system for articles elsewhere.

Or is it creating an entirely different category of social media?

It is not clear.

#### Product Features

In this post-pc era, every product should have a **native mobile app**. [Mobile is indeed eating the world][10]. If Ello has to become part of daily lives of its users; there is simply no other way to achieve it, than to have a mobile app.

At the least, their website should be responsive enough on the mobile. Look at their site in an iPhone. There is no clear indication of what I can do. Can I post something new? Can I reply to a thread? The site is not intuitive.

![Ello Mobile App](https://cdn.olai.in/jjude/ello_mobile.jpg)

Look at Twitter's site on the same mobile. Everything is well laid out. I can go through tweets, I can post new, I can discover friends and so on. Twitter’s site keeps me hooked in their site, which is a must for a social network.

![Twitter Mobile App](https://cdn.olai.in/jjude/twitter-mobile.jpg)

Another must have product feature is **API**. Do you want exponential growth? Build API. Encourage apps to integrate. That is another way to get into the daily lives of users.

I believe in the ideals of Ello. We need a better web, where we can communicate, discuss and collaborate without tens of sites prying on to us. Ello has given a hope. I wish it survives.

[1]: https://ello.co
[2]: https://ello.co/jjude
[3]: http://app.net
[4]: https://twitter.com/sastrytumuluri
[5]: http://www.manton.org
[6]: https://disqus.com
[7]: https://medium.com
[8]: http://foundrygroup.com/blog/2014/10/our-investment-in-ello/
[9]: https://pinboard.in
[10]: http://ben-evans.com/benedictevans/2014/10/28/presentation-mobile-is-eating-the-world

