---
title="Big Ideas From Peter Theil"
slug="big-ideas-from-peter-theil"
excerpt="Seven big challenging ideas from Peter Theil, Paypal founder"
tags=["coach","startup","insights"]
type="post"
publish_at="14 Sep 14 16:30 IST"
featured_image="https://cdn.olai.in/jjude/theil.png"
---
I am a huge fan of [Peter Theil][1]. I first heard him with his crazy, contrarian idea pitched to college students, to drop college and join him in building companies. I have a love and hate relation with the current educational system. I believe my college education formed a strong foundation on which I built my life; but, I also believe the system has become largely a hype and thus corrupt. Since, I have thought about this issue a lot, Theil's idea resonated with me and I started reading everything about him.

Then came his class on entrepreneurship and Blake Master's notes of the classes. Blake has done an excellent job of capturing detailed notes of the classes. These notes are now turned into a book (scheduled to be released on Sept 16th), which is available for pre-order. Both Theil and Master have been on multiple promotional events, discussing the big ideas from the book. I have not read the book (but will surely order the book once it is available in India), but here are the big ideas from Theil. I have based these on four sources:

1. [Theil's interview on Tim Ferris Show][2]
2. [Theil's reddit AMA][3]
3. [Master's interview on z16z podcast][4]
4. [Nabeel Qureshi's answer on Quora][5]

![Big Ideas From Peter Theil](https://cdn.olai.in/jjude/theil.png)

### 1. Mimesis drives people
Philosopher Rene Girard shaped Peter Theil's worldview. As per Rene Girard, _People take their cues from people around them, using them as a 'model', and unconsciously copy their desires for the same objects._ As Theil notes in [an interview][6], people can be _disturbingly herd-like_. Though founders, CEOs and investors would like to believe they are not herd-like, the truth is they are the most herd-like.

**People can be disturbingly herd-like**, is the core-idea behind every other idea of Theil.

### 2. Technology > Globalization
Peter defines technology as new ideas and globalization as copying ideas that worked. Peter argues, rightly so, that new ideas lead to growth while copying ideas produces over-crowded markets (Copying successful ideas is another mimetic behavior).

Developing world may copy ideas that worked in the developed world, but **only new ideas will take the civilized world into next level**.

### 3. Capitalism # Competition
Most people believe that capitalism and competition are synonyms, but they are in-fact opposites. A capitalist accumulates capital, whereas all the capital gets competed away in a world of perfect competition: The restaurant industry in SF is very competitive and very non-capitalistic (e.g., very hard way to make money), whereas Google is very capitalistic and has had no serious competition since 2002.

**Fortunes are always made in monopoly-like businesses**.

### 4. Conventions aren't necessarily true
Conventions are generally a shortcut to truth. As time flows, truth and conventions diverge. While searching for the future, we don't know what future will be. So we ask people whom we know. They, too, don't know and they ask around. This goes on and soon, we end up with everyone listening to everyone else.

As an example, everyone aim to get into elite universities because that guarantees well-paid jobs. That is a convention, not the truth. Not everyone graduating out of elite universities get well-paid jobs; and not every one who gets a well-paid job studied in elite universities.

Think for yourself and **separate conventions from the truth**.

### 5. In life, probability beats calculus
With calculus, you can calculate things precisely. You can even calculate planetary locations years or decades from now. But there are no certainties in future, only distributions. Bell curves and random walks define what the future is going to look like.

If you are planing for the future, get used to **statistical ways of thinking**.

### 6. Investing isn't lottery
Spray and pray indicates sloppy thinking, and thus lack of conviction. CEOs and companies aren't lottery tickets. Rather than investing in 100 companies to cover your bases via volume, invest in 7 or 8 promising companies which can fetch you 10x returns.

**Avoid small probability of payoffs, even if they are big payoffs**.

### 7. Develop your thinking daily
Have a conversation with some of the smartest people you know. Its not like MTV approach, where you talk to a new smart person everyday but have a sustained conversation with group of friends whom you know for a long time.

Since we all take cues from people around us, **develop a daily habit of discussing with smartest people**.

As a [first-time entrepreneur][7], I consciously take steps to shed away employee & consulting mindset. Inculcating Theil's ideas are one step in that direction.

[1]: https://en.wikipedia.org/wiki/Peter_Thiel
[2]: http://overca.st/BmGVvqRF8
[3]: http://www.reddit.com/r/IAmA/comments/2g4g95/peter_thiel_technology_entrepreneur_and_investor/
[4]: http://a16z.com/2014/09/12/a16z-podcast-secrets-power-laws-and-technology-the-ideas-of-zero-to-one/
[5]: http://www.quora.com/How-has-Rene-Girard-shaped-Peter-Thiels-worldview-investing-strategy-and-religious-faith
[6]: https://www.youtube.com/watch?v=esk7W9Jowtc
[7]: /biggest-bet/
