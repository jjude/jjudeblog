---
title="Ten Commandments of a Problem Solver"
slug="ten-commandments-of-a-problem-solver"
excerpt="The main principles for solving any problems"
tags=["problem-solving"]
type="post"
publish_at="09 Jan 11 16:30 IST"
featured_image="https://cdn.olai.in/jjude/ten-commandments-of-a-problem-solver-gen.jpg"
---

Main principles for problem-solving

1.  [Don't frown upon problems](/dont-frown-upon-problems/)
2.  [Don't fail to ask what is the problem](/dont-fail-to-ask-what-is-the-problem/)
3.  Don't shoot the messenger
4.  [Don't polarize](/dont-polarize/)
5.  [Don't torture context to fit your solution](/dont-torture-context-to-fit-your-solution/)
6.  [Don't replace backbone with a wishbone](/dont-replace-backbone-with-a-wishbone/)
7.  [Aim for success, not perfection](/aim-for-success/)
8.  [Talk their language](/talk-their-language/)
9.  Tap the power of simplicity
10.  Move on

_This post is part of '[Be a Problem Solver](/be-a-problem-solver/)' series_

