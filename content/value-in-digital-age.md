---
title="How to deliver value in the digital age?"
slug="value-in-digital-age"
excerpt="In the digital era, the software is the business. What skills are needed to thrive in this era?"
tags=["coach","tech","coach","talks"]
type="post"
publish_at="28 May 18 10:30 IST"
featured_image="https://cdn.olai.in/jjude/digital-value.png"
---
Man landed on the moon in 1969. A tiny computer, with just 64KB, guided the mission. This tiny computer was only a small step in the giant leap of technology that followed. 

The Apollo mission sparked major shifts in electronics, computing, and management. Soon after the mission, every US company started to introduce software into their business. 

Marc Andreessen, the founder of Netscape, [coined](https://a16z.com/2016/08/20/why-software-is-eating-the-world/) the slogan, "**Software is eating the world**" to denote this era.

Two types of professionals emerged in this era. 'I' shaped experts who specialized only in one thing and generalists, who were capable of many things but experts of nothing. There were domain specific specialists who worked in silos. There were marketing specialists, Helpdesk specialists, and so on. Generalists were usually project managers. They saw these specialists as resources to manage.

Fast forward half a century. We now have more powerful computer in our pocket than the Apollo guidance computer. This again has caused innovation in varied industries which led to what we call now as **digital age**.

There are few **characteristics that define the digital age**. Scott Brinker, of [chiefmartec](https://chiefmartec.com/) blog defines these in his book, [Hacking Marketing](https://www.amazon.com/dp/1119183170?tag=jjude-20). These are scale, speed, adjacency, adaptability, and precision.

These characteristics alone do not define digital. Digital is the blend of devices, places, and persons **leading to new business models**.

![What is Digital](https://cdn.olai.in/jjude/what-is-digital.png)

Let’s talk example. Take the current version of hotel booking workflow. You book rooms on their online portal. On the day of check-in, you walk to the reception and show your booking to those in the counter. They check you in and issue a card to open the room door.

In the digital version, your **smartphone becomes the checking agent**. You search and book a room via a mobile app. The mobile app stores the confirmation. On the day of the check-in, you walk into the hotel. The mobile app automatically connects to the hotel reservation system and gets your room number. Then when you walk to the room, the **smartphone becomes the swipe-card**. It interacts with the door-locker and opens the door for you.

In the "software is eating the world" era, software facilitated business. An automotive company like [Ashok Leyland](https://www.ashokleyland.com) used software systems to manage employees, supply chain, payment, and so on. But the physical product itself wasn't impacted much by software.

In the digital era, that changes.

Uber is not a transportation company. It is a software company.
Twitter is not a news & media company. It is a software company.
Airbnb is not a hospitality company. It is a software company.

Each of these companies, **carved a new business model by combining digital characteristics**.

Does this change expectations from software developers? Of course, it does.

In the "software is eating the world" era, software departments (and IT services companies) were considered **cost-centers**. Because they were cost-centers, the expectation was efficiency. **Do it faster, do it cheaper**.

Faster and cheaper meant process, systems, and specialization. 'I' shaped specialists were managed by generalist managers to achieve faster and cheaper products. 

Not enough money? Outsource. Can't outsource? Then eliminate the department.

**In the digital era, software is the business**. Software creates and captures value. You can't just eliminate the department that creates and captures value for the company.

In the digital age, software programmers can’t sit in their isolated cubicles and develop systems designed by other departments. If we have to create value, we have to develop cross-functional skills. We have to become **inverted 'T' shaped professionals** — experts in at least one domain, which rests on "good enough" skills in two or more complementary domains.

I'm dividing the required skillsets for digital into three - **core tech skills, complementary skills, and communication skills**.

![Digital Skills](https://cdn.olai.in/jjude/digital-value.png)

### Core Skills

As builders of software systems, we learn **software languages** first. We know to develop web and mobile applications. Most of us also learn about security and testing.

Most of us only look at behavioral aspects of the application while building the system. We usually ignore architectural aspects of the application until much later. In the digital era, it is important to learn both aspects of application development.

As you recall, one of the characteristics of digital is scale. The software system should be able to scale to millions of users. One easy way out is throwing hardware at the problem. Sooner or later, you will have to learn to inject probes into the application, collect error data, and **tune** the application.

### Complimentary skills

One of the requirements of digital systems is speed. Not the speed of the system itself, but the **speed with which new ideas can be tested**.

It is not advisable to develop every idea from scratch. If you need speed of delivery, you need to ride on packages or platforms. You have to pick and choose these packages and platforms depending on the area of the software development. There is [magento](https://magento.com/) for e-commerce, [drupal](https://www.drupal.org/) for content marketing, and so on. You should know at least one of these platforms or packages so that you can release an idea quickly into the market and validate it.

A mechanic is interested in the internal working of a car; a driver is only interested in going from one place to another. Likewise, a business owner is only interested in **how these techs can solve a problem in an industry**.

If you are developing a product for a particular industry, then you should learn about that industry as much as possible. If you are in IT services company, then you should learn about few industries so that you can talk in your audience language.

Unless you know the problems an industry is facing, you won't be able to solve that problem with digital solutions.

### Communication skills

_Think like a wise man but communicate in the language of the people - William Butler Yeats_

Developers, don't like to communicate. We don't mind sitting in a cubicle for hours thinking and rethinking an elegant solution. When we hit upon a solution, we want to close ourselves to the world to code that elegant solution. This method doesn't scale in the digital era.

We always have to work with teams. This means we have to ever express our views with others. You might get opportunities to discuss with seniors going up to CEOs and other executives. This is the reason, you should **learn to express your ideas and solutions with others**.

To communicate well, you should learn to [think structurally](/structured-thinking/). **Learning how to think is probably the best intellectual asset** you can build for yourself. 

Given to itself, your thinking mind generalizes or complicates issues at hand. Your emotions too influence the decision making. A structured approach will help you organize the information, reveal gaps in understanding, and select the best solution within the constraints of the situation at hand. One structured thinking approach you can learn is [SODAS](/sodas/).

### When businesses blend, skills blend too

I said earlier that digital is a blend of spaces, places, and persons to create newer business models. If companies blend multiple disciplines to create newer business models, then developers will also have to blend cross-disciplines to implement those business models.

_This is blog version of a talk I gave at Azure global Bootcamp, Chandigarh. You can watch the [video](https://youtu.be/59TjHUgBnvg). [Slides](https://www.slideshare.net/JosephJude4/delivering-value-in-digitial-age-global-azure-bootcamp-joseph-jude) are here._
