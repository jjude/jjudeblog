---
title="Krishna Kumar N on 'Power of storytelling in business'"
slug="kk-story"
excerpt="Storytelling enabled KK to shape sales and marketing efforts in a South African company and land him on LinkedIn's first Creator program in India."
tags=["gwradio","coach","wins"]
type="post"
publish_at="27 Jun 23 07:18 IST"
featured_image="https://cdn.olai.in/jjude/kk-yt.jpg"
---

<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless src="https://share.transistor.fm/e/f59b6f52"></iframe>

## What you'll hear

- How KK accidentally started with business storytelling
- Is storytelling useful for fields other than sales & marketing
- Examples of storytelling for project managers
- How writing on LinkedIn helped KK
- Basics of storytelling
- Examples of good and bad storytelling
- Presenting numbers with narratives
- How to get started with storytelling
- Getting started with writing on LinkedIn
- What should be in the first fold of the webpage
- Kindest thing anyone has done for KK
- Who exhibited leadership in KK's life
- Definition of living a good life


## Your feedback counts

Thank you for listening. If you enjoy the podcast, would you please leave a short review on Apple podcast or on YouTube? It takes less than 60 seconds, and it really makes a difference in finding this podcast. And it boosts my spirits. 😀

I would like to hear what resonated the most with you in this episode. Either you can email me or share it on social media and tag me. Thank you for your support.

Have a life of WINS.

_Gravitas WINS Radio is produced by [Joseph Jude](https://jjude.com/). Be sure to subscribe on [Apple Podcast](https://podcasts.apple.com/in/podcast/gravitas-wins-radio/id1562802449), [Spotify](https://open.spotify.com/show/7LOBjAyjBkechMC0xMIj9E),[Google Podcast](https://podcasts.google.com/feed/aHR0cHM6Ly9mZWVkcy50cmFuc2lzdG9yLmZtL2dyYXZpdGFzLXdpbnMtcmFkaW8=),[YouTube](https://www.youtube.com/gravitaswins), or the podcasting app of your choice._

Please check out other episodes of [Gravitas WINS Radio](/gwr-themes/). For easy consumption, they are categorized by themes. If you are a fellow podcaster, you might be interested in [How I Podcast](/how-i-podcast/) or [tools I use](/i-use/).

## Connect with KK
- LinkedIn: https://www.linkedin.com/in/storytellerkrishna/

## Connect with me
- Twitter: https://twitter.com/jjude
- LinkedIn: https://www.linkedin.com/in/jjude/
- Website: https://jjude.com/
- Newsletter: https://jjude.com/subscribe/
- Youtube: https://youtube.com/gravitaswins
- Email: podcast@jjude.com
- Executive Coaching Program: https://gravitaswins.com

## Continue Reading

* [Sampark A. Sachdeva on 'LinkedIn Success Mantras'](/sampark-linkedin/)
* [Jayaram Easwaran on 'Secrets of A Master Storyteller'](/jayrame/)
* [Power Of Storytelling In Business](/biz-storytelling/)