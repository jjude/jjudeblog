---
title="Taking The Biggest Bet Of My Life"
slug="biggest-bet"
excerpt="Rule of thumb for career change-You take a bet, you make it a win. While still winning, you take your next big bet."
tags=["startup","luck"]
type="post"
publish_at="29 Aug 14 16:30 IST"
featured_image="https://cdn.olai.in/jjude/bet-when-winning.png"
---
Today I quit a high paying consulting job. Was it because it was a no-meaning, cut-and-paste job with a suit?

Not at all. Rather, I was part of India's finest e-governance project, [MCA21][2], rolling out new initiatives and [winning awards][1]. It was lot more satisfying than what I imagined. But then, I also have a simple rule-of-thumb for career change: You quit at peak.

![Bet While Winning](https://cdn.olai.in/jjude/bet-when-winning.png)

You take a bet, you make it a win. While still winning, you take your next big bet.

Will you always win? Of course not, you silly boy. But you learn, adjust your wings and then take your next bet.

I am taking my next big bet while still wining in consulting. Here's the bigger bet: I am starting a company with a friend.

What are the odds of winning? Statistically, odds are against me. Most startups fail; you almost never hear forty-year olds starting a company for the first time. Statistics, however, is never a good friend of founders.

**Founders bet on skill, will and lucky breaks.**

I am betting that business climate in India will improve; I am betting that most  new businesses will be small and medium businesses; I am betting that they will be primarily online; I am betting that they would need help in securing their online assets; I am betting that I got the will to turn this new market into a profitable business.

Time will tell if it's a winning bet.

[1]: /an-award-to-end-the-year/
[2]: http://www.mca.gov.in/
