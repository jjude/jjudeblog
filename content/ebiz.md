---
title="Want to make doing business in India easy? Start with incorporation"
slug="ebiz"
excerpt="If starting a business in India has to improve, the entire process should be studied and simplified."
tags=["coach","startup"]
type="post"
publish_at="20 Feb 15 07:10 IST"
featured_image="https://cdn.olai.in/jjude/ebiz.jpg"

---

Mr. Arun Jaitley, Minister of finance, corporate affairs and Information and Broadcasting launched EBiz program today (19th Feb, 2015). EBiz has been in development for almost a decade.

![Ebiz Ad](https://cdn.olai.in/jjude/ebiz.jpg)

The mandate of eBiz is to orchestrate all government services related to doing business. It is not an attempt to simplify underlying processes though. That means it is a beautiful Kashmiri carpet spread on the ugly ducklings, keeping the ducklings ugly for ever.

I am not calling these processes ugly ducklings because of second-hand experiences. Ironically, today my CA tried to reserve a name for our company and she got an error. When I explain the error, you will agree, that it is nothing but idiotic.

To reserve a name, you need to first apply for a director's identification number, called DIN. To apply for DIN, you need PAN, issued by another government department — Income Tax department. Every Indian director needs a PAN, and IT department issues PAN after its own due diligence. If so, what is the need for DIN? Don't ask me. But for a moment, let us not question the collective wisdom of our bureaucrats. I applied for DIN and got it approved.

So with an approved DIN, my CA filled the form for reserving the name. There she got an error that the DIN is erroneous. How could the DIN approved by the ministry be erroneous? After interaction with the support team, I got to know the reason. Now hold on to your chair before reading. Don't fall of laughing.

Ready?

Here is the reason given by the support team:

> There is an extra space between first name and last name in PAN database. In MCA21 there is only one space. [^1]

You can't make this up.

Reality is lot more comical than best of the jokes.

Done ROFL?

I am not done yet.

What is the recommended recourse?

> Change the details in the PAN database or file Form 4 in MCA21 with extra spaces between first and second name.

I am not joking. Seriously.

This is why eBiz will remain lipstick on pigs.

You might wonder if mine is a lone case. It is not. Everybody has their own stories that will beat the best comedy scripts. But you hear them. Why?

First reason is that, entrepreneurs like to focus primarily on their business. We encounter so many problems in building our business. We focus on getting those problems solved, not blogged.

The second reason is, sadly, specific to Indian system. If you poke the elephant called government, it will come after you with a unique vengeance to derail and destroy you. It is not worth it.

But who will bell the cat?

There is hope, though. Both Finance and Corporate Ministry are under the same minister; and there is a renewed focus to improve business environment in India. Now lies the great opportunity to look at the process of starting a business holistically, not in silos.

[^1]: If you have done any bit of programming, you will immediately know that this is integration gone woefully wrong.
