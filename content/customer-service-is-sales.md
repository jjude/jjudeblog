---
title="Customer Service Is Sales"
slug="customer-service-is-sales"
excerpt="If you deliver superb customer service consistently, you will get repeat customers."
tags=["coach","startup","sales"]
type="post"
publish_at="01 Feb 12 16:30 IST"
featured_image="https://cdn.olai.in/jjude/customer-service-is-sales-gen.jpg"
---


[Flipkart][10], the pioneering e-Commerce store in India, just delivered last of the four books that I had ordered. This is the seventy-eighth book they are delivering to me, in the past three years. I could boast that I am a loyal customer; but I couldn't have been one without their exemplary service. If you thought, I am getting a special service because I'm a long-term customer, you are in for a surprise - they don't even have a loyalty program. I have been their customer all these years, only because their service is consistently superior.

Flipkart is not the first of the e-commerce stores in India. I used to be a customer of [Infibeam][2] for about a year. They had a loyalty program and given my book purchasing habits, I joined that. All was well until they had to cancel an order. At first, the delivery was delayed by ten days. That ten days was stretched few more times. Eventually the order got cancelled. But the amount for that cancelled order did not reflect in my account. At first I thought it was an oversight. It took me relentless follow-up for two months to get the refund. I was furious that, a) they didn't have the inventory details b) they took time to inform about their inability c) they took even more time to refund the amount. I decided to look for alternatives.

I asked on [twitter][3] for options and [narain][4] informed about Flipkart. When I started with them they were small. Not this "India's startup success story". I proceeded with caution, but at every interaction my trust increased a little bit more. Not that there weren't any problems. Of about thirty orders I have placed, they had to cancel two books. On both occasions, they informed about the cancellation and the amount was credited to my account on time. Another time, there were two books of the same title - one with a CD and another without a CD and I had ordered the one without a CD, a representative called me to confirm the order before delivery.

Recently they introduced electronic items, thirty day refund policy and cash-on-delivery payment model. I ordered 'Toy Story' movie for my son. They shipped DVD of a wrong region. When I informed them, they picked it up free of cost. And the amount was credited into my account immediately.

When the trust is high, I could overlook a mistake or two. Isn't it?

It is not just Flipkart which is getting the e-commerce right. I have been using [ZoomIn][6] for quite sometime and they too are fantastic. The range of products that they have is ever increasing. I've been able to impress my family members with their photographic products - photo albums, mugs, calendars and like. Some of them are so impressed that they have become their repeat customers.

[FirstCry][7] is another e-commerce store on the right track, though they are relatively new.

E-commerce was once the 'new-thing'. Now it is a competitive market. You can't win just with low price, discounts and loyalty cards. May be these gimmicks will get customers to try your product or service. But its the service delivery that will convert the try-outs into loyal customers. Conventional management teaches that customer service starts after sales. Its no more true in the electronic world.

Customer service starts as soon as a web page is served. And it continues through product search, cart management, order processing, delivery and feedback handling. Customers expect appropriate communication at every stage. And the experience has to be consistent as the orders are repeated.

You may change any of the components of the process, but that shouldn't affect my experience. For example, over the years, Flipkart redesigned their website, introduced [wish-lists][8], added electronic items, started own delivery network and so on. But none of that affected my experience. I still get the products delivered at home at the date mentioned[^1].

Will I switch to a competitor in the future? May be. But for that to happen, Flipkart has to get their act wrong as well as the competitor has to beat them in service.

_Image by: [adrianmelrose][9]_

[2]: http://www.infibeam.com/home
[3]: http://twitter.com/jjude
[4]: http://twitter.com/narain
[5]: /beware-of-cloning-best-practices
[6]: http://www.zoomin.com/
[7]: http://www.firstcry.com/
[8]: http://www.flipkart.com/wishlist/jjude
[10]: http://www.flipkart.com/

[^1]: One thing I would like Flipkart to improve is to inform the time window for a delivery; otherwise someone has to be waiting at home the whole day.

