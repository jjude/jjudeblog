---
title="Top 20 Free iPad Apps"
slug="top-20-free-ipad-apps"
excerpt="My favorite free iPad Apps"
tags=["sdl","apps"]
type="post"
publish_at="20 May 11 16:30 IST"
featured_image="https://cdn.olai.in/jjude/ipad-apps.png"

---

The three major factor by which iPad stands out from its competition (is there any?) are, sleek device, low price and most importantly, vast number of applications. Pick any category and you are sure to find a free, cheap and an expensive application, and mostly a good fit for your needs.

Ever since [I got an iPad](/how-i-came-to-own-an-ipad/), I have been browsing the [AppStore](http://www.apple.com/ipad/from-the-app-store/) for free apps.

Browsing the AppStore can overwhelm you. While large number of applications is a good thing, searching through the AppStore can overwhelm you. There are just too many apps. Of course, if you care to spend time sifting through all of searches and app descriptions, you will find what you want.

But if you are ready to spend money, then you don't have to spend that much time going through the appstore - just pick up the app you want to buy, pay and own it.

Since I had time, I decided to search for free apps.

In fact, I'm surprised that there are any free apps for iPad. Apple charges enormous amount (at least according to my standards) to be part of the developer program and without participating in it, you can't get your app into AppStore which is the only distribution mode for iPad apps. Yes, there are free apps with ads, but not every one.

Here I present the free apps I found useful.

![Top 20 Free iPad Apps](https://cdn.olai.in/jjude/ipad-apps.png)

While there are hundreds of free apps, I didn't go and try every free apps. Why? Because of how I want to use this iPad.

I framed &#8216;[consume, produce, engage](/how-to-be-a-champion-and-remain-one/)' model for learning. I am curious about various subjects and I want to learn about all of them. So I looked for apps that fit into these three broad categories.

1.  [AppStart](http://itunes.apple.com/us/app/appstart-for-ipad/id408984648?mt=8): A beginners guide for iPad. It gets you started with using iPad. It also provides list of essential applications (both free and commercial) under different categories.

2.  [MobileRSS](http://itunes.apple.com/us/app/mobilerss-free-google-rss/id333925239?mt=8): I am a follower of &#8216;[Eat like a bird](http://www.evancarmichael.com/Entrepreneur-Advice/352/The-120-Day-Wonder-How-to-Evangelize-a-Blog.html)' principle advocated by [Guy Kawasaki](http://blog.guykawasaki.com/). So I read lot of blogs and Google Reader is my choice of RSS aggregator. Hence I prefer desktop & mobile clients that syncs with Google Reader. MobileRSS is an easy to use iPad app syncing with Google Reader.
3.  [Read It Later](http://itunes.apple.com/in/app/read-it-later-free/id309597402?mt=8): Another tool for consumption of interesting ideas. As I read blogs and tweets, I collect links that would be of interest which I can read leisurely. &#8216;Read it Later' client enables me to collect these links and as the name indicates, to read them later. An annoying aspect though is that you need to be connected to the Internet to save these links. It would be a welcome feature if links can be added offline too.
4.  [Echofon](http://itunes.apple.com/us/app/echofon-for-twitter/id286756410?mt=8): Twitter enables me to consume, create and engage all within the same system. I learned a lot through Twitter regarding all the topics that I'm interested in. Echofon provides a simple and easy interface to all of Twitter functionalities (lists, retweets, reply etc). I can also save links to &#8216;Read It Later', if connected to Internet.

5.  [Evernote](http://itunes.apple.com/us/app/evernote/id281796108?mt=8): Evernote is a fantastic tool for knowledge management. It has tools for collection from RSS readers, browsers and menu bar and all of it's clients (on MAC, iPad, android) for free. These collected notes can be tagged and organised under notebooks. Since it is a free plan, there is no offline reading of these notes, which I am ok.

6.  [Nebulous Lite](http://itunes.apple.com/us/app/nebulous-notes-lite/id392778235?mt=8): A text editor for iPad. But it's more than a text editor. It has support for [markdown](http://daringfireball.net/projects/markdown/) format with [dropbox](http://itunes.apple.com/us/app/dropbox/id327630330?mt=8) sync. If that isn't enough, it also has support for macros which brings an additional row of keys to default iPad keyboard. I have found that this extra row of keys offer very useful features like undo, redo, navigating shortcuts etc. Try this app if you plan to type a lot on your iPad. In fact, I am typing this blog post in it, which will be converted into HTML on my MAC.
7.  [Bible](http://itunes.apple.com/us/app/bible-hd/id364899669?mt=8): There is no other book that has inspired me as much as Bible. Now I can carry it along with me to read it anytime I want. Not just one version but many versions (I have downloaded Good News Version).

8.  [iBooks](http://itunes.apple.com/in/app/ibooks/id364709193?mt=8): Probably the only free app from Apple for iPad. This is a book reader. You can download books on variety of topics from the bookstore. I have downloaded some of the classics. Currently I am reading, &#8220;[India : What can it teaches us?](http://www.amazon.com/India-What-Can-Teach-Us/dp/1585090646)&#8221;

9.  [Toshl](http://itunes.apple.com/us/app/toshl/id384083725?mt=8): A tool to track expenses. It is a iPhone only app which syncs with their web app. Since I have been already using it, I went ahead to use it in iPad too.

10. [Idea Sketch](http://itunes.apple.com/in/app/idea-sketch/id367246522?mt=8): A simple tool to draw flow chart or mind map. The drawing can be saved locally or emailed.

11. [Documents Free Mobile Office Suite](http://itunes.apple.com/us/app/documents-free-mobile-office/id306273816?mt=8): Mobile office suite which can handle both documents and spreadsheets along with integration to Google Docs. Google Documents can be downloaded, edited and uploaded back. Documents can also be created in the iPad and uploaded into Google Docs.

12. [Dropbox](http://itunes.apple.com/us/app/dropbox/id327630330?mt=8): The glue. Now-a-days, I depend on dropbox for sharing files back and forth between iPad and Mac and writing notes on the iPad and syncing back to Mac.

13. [WordWeb](http://itunes.apple.com/in/app/wordweb-dictionary/id309627313?mt=8): Dictionary for iPad. It's fairly comprehensive and has an extensive search facility too. Every now and then, I would like to refer a word and with word web, I don't have to wait until I get a Internet connection.

14. [WordPress](http://itunes.apple.com/in/app/wordpress/id335703880?mt=8): I host two blogs - one is this and [another] where I blog about consulting. I can post from within the iPad and also track comments. But the most useful feature is the access statistics of the blogs.
15. [Times Of India](http://itunes.apple.com/us/app/the-times-of-india-for-ipad/id427164653?mt=8): [Times of India](http://timesofindia.indiatimes.com/) is the only Indian newspaper that has an iPad version, that too for free. I have been a subscriber of its paper version. Now I sync the iPad app in the morning and I read while traveling or whenever I get free time during the day.

16. [Flipboard](http://itunes.apple.com/us/app/flipboard/id358801284?mt=8): A new way of consuming digital information. An interesting concept that turns blogs and twitter feeds into a digital magazine.

17. [CloudReaders](http://itunes.apple.com/in/app/cloudreaders-pdf-cbz-cbr/id363484920?mt=8): A PDF reader for iPad. You can download from its bookshelf or transfer files from your mac. It has an easy mechanism to transfer files into iPad from Mac.

18. [Chess](http://itunes.apple.com/us/app/chess-free/id311395856?mt=8): If I completed whatever I wanted to do, then I take a break and play for sometime before I get to the next task. This app is well designed.

19. [Epicurious Recipes & Shopping List](http://itunes.apple.com/us/app/epicurious-recipes-shopping/id312101965?mt=8): Cooking can be fun and a stress reliever. Epicurious lists lots of recipes. I'm still looking for a good Indian menu in the list. Right now, I am just trying out the drinks mentioned here.

20. [IM+](http://itunes.apple.com/us/app/im/id285688934?mt=8): I use Google (gmail) as my contact manager app, so I was looking for a chat client with a support for Google protocol. IM+ supports almost all existing chat protocols and it's easy to use too.

As days go by, I will test out other applications in each of these categories. As I use these applications, I will also post full review of these applications. So stay tuned.
