---
title="Books I’ve written"
slug="books"
excerpt="Books I’ve authored or co-authored"
tags=["books"]
type="page"
publish_at="18 Feb 14 16:30 IST"
featured_image="https://cdn.olai.in/jjude/books-gen.jpg"
---

I co-authored a book titled, “**Ionic 2: Definite Guide**” with my sister. It will be published by Apress in 2017. You can read about it [here](/ionicbook/).


![Ionic Book by Joseph Jude and Joyce Justin](https://cdn.olai.in/jjude/2016-ionic-book.png "Ionic Book by Joseph Jude and Joyce Justin")

