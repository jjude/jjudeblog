---
title="Money can buy happiness"
slug="money-buys-happiness"
excerpt="Money gives us pride and satisfaction, but it can also bring us happiness when we spend it wisely."
tags=["wealth","wins"]
type="post"
publish_at="05 Sep 22 19:10 IST"
featured_image="https://cdn.olai.in/jjude/money-buys-happiness-gen.jpg"
---


Money can’t buy happiness. This catchphrase probably resonates with you. It is true in a way, since we spend money on goods to impress others and fail miserably.
  
Money, if spent wisely, can bring happiness. Psychologists have identified [eight ways to spend money to buy happiness](https://scholar.harvard.edu/files/danielgilbert/files/if-money-doesnt-make-you-happy.nov-12-20101.pdf) based on empirical research. I am listing only five of them here. [Read the original paper](https://scholar.harvard.edu/files/danielgilbert/files/if-money-doesnt-make-you-happy.nov-12-20101.pdf) if you are interested.

1. Buy Experiences Instead of Things
2. Help Others Instead of Yourself
3. Buy Many Small Pleasures Instead of Few Big Ones
4. Pay Now and Consume Later
5. Beware of Comparison Shopping

## 1. Buy Experiences Instead of Things

When we earn more money, we buy a material good, such as a home, watch, smartphone or car. A fancy good may give us a temporary high, but it doesn't give us a lasting happiness.

Why?

We get adjusted to the new possession quickly. It doesn't thrill us any more after few days of usage. So what should we do?

Buy experiences.

Take a chopper ride over Niagara Falls. Take a hike through Samaria Gorge. Explore the rock-cut cave temples of Badami. Experience the sunset on the Nile with your spouse. Get your boys a tasty Green Apple Slushie and watch them enjoy drinking it.

Why we get more happiness from experience than buying goods?

Material goods make us happy only when we use them, but we feel happy when we go through an experience as well as whenever we recall our experiences.

## 2. Help Others Instead of Yourself

We're social by nature. Thus, when we help someone else, we feel good.

Sponsor the education of a poor child. Donate to an orphanage. Build homes for the poor with a charity. 

In addition to making you happy, helping others reinforces your life's purpose.

It isn't just about the poor. Spending money on your family and friends also boosts your mood. The next time you want to feel happy, invite me to a delicious dinner by the beach. It will certainly make you happy.

## 3. Buy Many Small Pleasures Instead of Few Big Ones

Our finances are finite. No matter how much money we have, it's still finite. Buying lovely things frequently is better than buying them infrequently.  
  
Why?  
  
Happiness comes from frequency than intensity.

Having two smaller "Qubbani Ka Meetha" on two different days is more enjoyable than having one large sweet on a single day.

Traveling to different places on multiple vacations is a better idea than going on a single vacation in business class and staying in a five star hotel.

It is easy for us to adapt to new environments. But, each time we enjoy a small pleasure, it is different from the last one, so it keeps us looking forward to the next one.

## 4. Pay Now and Consume Later

With so much free credit available, credit card companies tempt customers to consume now and pay later. If we consume when we feel like it, we do not anticipate consuming in the future. We have eliminated happiness by consuming immediately, as anticipation is a source of happiness.

Our desire for immediate consumption often leads to vices like fattening food that provide pleasure right away, but come with long-term consequences. Contrary to delayed consumption, we are more likely to choose virtues that produce longer-lasting well-being when we delay consumption.

If you want to be happy, delay consumption.

## 5. Beware of Comparison Shopping

Others shape our desires more than we care to accept. Social media and equivalent sites have amplified such mimetic desire.

Instead of focusing on factors that will bring us satisfaction, we compare product attributes that are easy to compare. Also the factors we use to while comparing and shopping may not be the factors that matter to us while using the product.

For example, when we're looking for a home, we compare the size, number, and amenities of the building. These are important considerations. What determines our happiness while living in the house? Distance to work, hospitals, and schools, as well as neighbors, pollution, and the condition of the roads. I know a friend who purchased a nice big house after a lot of research and comparisons. The house is near the airport, which he didn't realize. As a result, he has to deal with the sounds of flights all day long. He can't hold a decent conversation over the phone for more than 30 minutes.

## Conclusion

More money gives us pride and satisfaction, but it can also bring us happiness when we spend it wisely. It is even better if you combine these factors.


## Continue Reading

* [How To Succeed In The Stock Market With A Simple And Structured Approach?](/4m-stocks/)
* [Value Investing Is Better Than Real Estate](/value-investing-real-estate/)
* [Proven Biblical Principles To Build Wealth](/biblical-wealth/)

