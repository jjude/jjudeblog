---
title="Winning With Ideas"
slug="winning-ideas"
excerpt="Your ideas have to satisfy two criteria to be winnable ideas"
tags=["wins","insights"]
type="post"
publish_at="17 Jan 22 18:03 IST"
featured_image="https://cdn.olai.in/jjude/winning-ideas-gen.jpg"
---


Imagine you are witnessing the greatest street fight you have ever seen. A large crowd has gathered to witness the fight. As the match progresses, everyone is shouting, and the situation is charged. As the final round comes into play, the crowd is starting to bet on the eventual winner. You also want to bet.  
  
You don't have the luxury of betting someone else's money. You have to wager your hard-earned money. What is going on in your mind right now? You might take into account the body build, punch, and past wins. Whatever factors you consider, **your bet should satisfy two factors**.  
  
**It has to be correct**. Otherwise, you lose money. You don't want to lose money. Remember, it is money you earned with blood and sweat.  
  
What is somewhat non-obvious is that your bet also **has to be contrarian**. Why? If the whole crowd agrees with your opinion, then your returns will be diluted. Contrarian doesn't mean nobody agrees with you; it means that very few people agree with you.  
  
**Only when your bet is correct and contrarian, you enjoy rich rewards**. This maxim is true beyond street fights.

## Continue Reading

* [How to avoid failures with thirty percent feedback?](/30-percent/)
* [One decision instead of hundred decisions](/lead-decisions/)
* [Principles trump processes](/principles-trumps/)
