---
title="Inspiring Managers"
slug="inspiring-managers"
excerpt="I've been lucky, at least officially. Right from the start of my career, I've worked with some of the best minded managers. Each of them have inspired me to be a 'little' better person."
tags=["coach"]
type="post"
publish_at="17 Aug 08 16:30 IST"
featured_image="https://cdn.olai.in/jjude/inspiring-managers-gen.jpg"
---

I've been lucky, at least officially. Right from the start of my career, I've worked with some of the best minded managers. Each of them have inspired me to be a 'little' better person.

##### Dr MR Girinath

Not many software professionals get an opportunity to work for a national award winner. I had the privilege of shaping my career under [Dr Girinath](http://www.ctsnet.org/home/mgirinath)'s wings. Even though he had achieved more than most other cardiac surgeons, he still worked hard. He couldn't rest on past laurels. Every day brought a new challenge for him. And he loved it. Not only I learned importance of hard work, but also of time - seconds determined life and death.

##### Dr PV Rao

[Dr Rao](http://www.ctsnet.org/home/pvrao) belongs to an elite team of surgeons who have done successful heart transplants. If Dr Girinath instilled the importance of hard work, Dr Rao instilled a sense of confidence. He believed in me long before I believed in myself. I would attribute my success to him, though he wouldn't accept it.

##### Sas3

With a bit of luck, I came into the radar of [Sas3](http://www.linkedin.com/in/sastry). Though I worked with him for a very short duration he made the highest impact in terms of management. His impact is so high that whenever I find myself in a tight-corner, in terms of project or people management, I question myself, "what would sas3 do?" and the answer has never failed me.

##### Steven Tilley

Oh! those were magical times. I'm referring to those days in Belgium. My stay in Belgium would have been shorter than it was, if not for Steven. He ensured that I never felt as an outsider. Whenever I was around he will request everyone to converse in English. Now that I work in a multi-lingual group, I understand how difficult it is - jokes and funny quips loose their essence when translated from mother-tongue. Yet he did it most of the time.

But his contribution to my career was not 'making-me-feel-comfortable-around-the-group'. It is in those countless hours of technical, and sometimes not so technical, discussions that we had, often sitting in bars. When mobile computing was still in its baby steps, we discussed on technicalities of brining CRM and ERP systems into mobiles. He encouraged to think beyond 'out-of-the-box'.

##### Kurt Van de Moortele

[Kurt](http://www.linkedin.com/pub/2/821/61A) taught me what trust can bring about in a person. Unlike most others that I worked with, he treated me, a contractor, equal to other team members. He followed a policy of 'trust someone unless it is broken'. He would tell me, "Keep the users away from me; I'll stay away from you". I still try my best to keep the users (from complaining) away from my bosses. Whenever I succeed in that, my bosses keep away from me!

##### Mahendra

The strongest point of [Mahen](http://www.linkedin.com/pub/0/544/86b) is his decision making. I've not seen him indecisive. He might be gathering facts; but he is never indecisive. During his tenure in our department, there were few decisions that went awry. That didn't bother him. He didn't take decisions that made him popular; rather that served the purpose at hand. It is hard to emulate that unless one is confident.

##### Guru Murty

Success and gentleness don't go well together. Not for [Guru](http://www.linkedin.com/pub/2/544/a91). I'd the opportunity to observe him closely during tough times, and I should say he stayed 'a gentleman'. It is easier said; but extremely difficult to follow.

One could quickly jump into conclusion that it has all been only nice managers that I worked with. Not quite true. I've worked with some terrible ones too. They've taught me 'how-not-to-be'.

I wish to be privileged with such best managers.

