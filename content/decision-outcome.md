---
title="Factors affecting the outcome of our decisions"
slug="decision-outcome"
excerpt="Most of us have a simplified image of the connection between our decisions and results. We think we make decisions and we get results. It is far from reality."
tags=["coach","wins","decisionmaking"]
type="post"
publish_at="18 Jun 21 07:35 IST"
featured_image="https://cdn.olai.in/jjude/decisions-outcomes.png"
---

### Chinese Farmer

Once upon a time, there was an old farmer with a small farm.

One day his horse ran away. His neighbor paid a visit and said, "Bad luck my friend." 

The farmer replied, "Maybe so, maybe not."

The next day, the horse returned. It also brought two other wild horses. The neighbor exclaimed, "Wow, what a good fortune. Not only you got your horseback, but you also got two more horses." 

The farmer replied, "Maybe so, maybe not."

The following day, his son tried to ride one of the wild horses. He fell and broke his leg. His neighbor came to offer their sympathy. "Oh, it shouldn't have happened. What bad luck you have."

The farmer replied, "Maybe so, maybe not."

Sometime later, military officials came to the village to recruit all boys for the army. They didn't take the farmer's son because he had a broken leg. His neighbor said, "You are lucky. Your son doesn't have to go to the battlefield."

The farmer replied, "Maybe so, maybe not."

### When I took a pay-cut

In 2015, after a failed startup, I took a pay cut and started working only for 3 days a week. My boys were only three and five years old. I decided to spend time with them as they were growing up.

Everyone I met at that time, said it was a stupid decision. For them, I was wasting the prime of my working years in not earning the most money I could earn.

In 2021, everyone who knows my boys now, says it was a fine and courageous decision to spend time with growing boys.

Maybe so, maybe not. We will see.

### Factors Affecting The Outcomes

Most of us have a simplified image of the connection between our decisions and results. We think we make decisions and we get results.

![Simplified view of decision making](https://cdn.olai.in/jjude/decisions-simplified.png)

It is far from reality.

Many variables influence the outcome. 

![Factors affecting decision making](https://cdn.olai.in/jjude/decisions-outcomes.png)

- **Quality of thinking**: First thing that is under our control is the quality of our thinking. Your belief system, knowledge, and self-control play a huge role in shaping the outcome.
- **Decision Making Process**: As we saw last week, if you follow a decision-making process, in the long term you'll get better results. 
- **Execution & Mid-Stream Adjustment**: Do you have the skills and team to translate what is in your mind into reality? Do you have leading indicators to give you early feedback? Do you have the skills to understand the indicators and correct your course of action? If not, you're going to be frustrated with the results.
- **Chance**: I have talked about [types of luck](/luck/) and how you can influence some of these chance events. You can do three things to take advantage of chance events: Identify the upcoming trends, prepare yourself, and position yourself on the path of the chance events. 
- **Time**:  For most decisions, there is a long gap between the time we decide and the time we see the decision in reality. When we decide, we will not have all the required data to decide. We won't see all the consequences of the decisions too. Yet we have to decide.

When you improve each of these factors, you improve the outcome of the decision.

### Quote To Ponder

_"It is important for you to know who you are, what is important to you, and how you want to exist and proceed in the world. This way, when things happen—and things will happen in life, good and bad, you will easily be able to decide what to do at the moment and later have no regret." ― T.D. Jakes_


## Continue Reading

* [What is your process for making decisions?](/decision-process/)
* [What Are The Top Leadership Challenges?](/leader-challenges/)
* [When To Listen To Your Critics?](/critics/)
