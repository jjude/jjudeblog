---
title="Starting a Startup is Like an Itch"
slug="starting-a-startup-is-like-an-itch"
excerpt="Interview with Vivek, co-founder of InterviewStreet."
tags=["coach","interview","startup"]
type="post"
publish_at="10 Aug 10 16:30 IST"
featured_image="https://cdn.olai.in/jjude/vivek.jpeg"
---
When we hear the word ‘startup’, we think of Facebooks and LinkedIns of the west. But in our own cities youngsters are establishing startups. [InterviewStreet](http://interviewstreet.com) is one such startup based in Chennai. Vivek founded InterviewStreet along with his friend Hari (both were classmates at [NIT, Trichy](http://www.nitt.edu/)). In this interview, Vivek talks of the reason behind the startup and his experience in running the startup.

### Q) Let us begin with your venture. What was the eureka moment that gave the concept of interview-street?

Actually, Interviewstreet was started as a platform for mock interviews where students could attend mock interviews from industry professionals. This idea occurred to me during my final year when a lot of them didn't exactly know what each company expected out of them. We developed a bare minimum prototype and presented it in a few B-plan competitions - won a couple of them too!

### Q) Before you started this venture you worked at Amazon. You also mention that Jeff Bozz is one of your role-model. But you decide to leave to start a venture on your own. What was the trigger point? How difficult it was to leave Amazon?

![vivek](https://cdn.olai.in/jjude/vivek.jpeg)

I think Amazon follows a very startup-ish culture - in terms of ownership, innovation & sometimes the amount of work ;) More than the work, I loved the people and the atmosphere which makes it almost irresistible to move away. But, starting a startup is like an itch which can't be controlled for long and hence had to take a call.

Jeff Bezos is really an inspiring personality and I consider myself lucky to have talked to him for a couple of minutes. Everyone knows about the Apple, Google story, it's surprising that not many actually know the fantastic journey of Amazon which survived the dot-com burst and the business-analysts predictions on it's failure.

When you start working on something, you have someone whom you derive inspiration from and start marching to achieve that - if not become like JeffB someday, I would like to achieve atleast 10% of what he has done!

### Q) Did you discuss your start-up idea with anyone? What was their response?

I think a lot of people liked the idea! It was something novel. Everyone encouraged me to try it out - atleast part-time. The positive vibes gave me a lot of confidence to move ahead - thanks to a wonderful set of peers and my parents.

### Q) Converting an idea into a revenue generating model is a tough one. How did you go about doing it? Who are the people who supported you in realizing your dream?

Yes, it's a tough thing. Having an idea is no big- deal, executing it correctly is the tougher part. There were a lot of people in the process of helping me come from the ideation stage to executing it - parents, [Hari](http://sp2hari.com/) (who was working at IBM at that time), [mentors](http://themorpheus.com/), and genuine friends.

### Q) Who was your first paying customer? What was the feeling to have someone validate your idea by paying for your product?

Great feeling! Its only a sign to work more on the product and satisfy many more customers! But it doesn't stop there - now you have got to ensure they come back to buy more products/renew subscriptions from you - buck up the customer service.

### Q) How is life as an startup entrepreneur? You mention that you travel by bus. Was it difficult to adjust to this life after a comfortable life as an employee of an MNC. What keeps you going?

It's a roller-coaster ride. There are alternating highs and lows, it's a hard journey, emotionally tough - for every rupee that goes out of your pocket, the immediate thought is - _How do I earn it back?_ No hi-fi lunches, no cab travel - yes, it's very hard, but the fact that you are doing something you like and sleep satisfied at the end of the day masks the pain behind it.

### Q) Do you think our Indian Eco-system (academic, society, government) is conducive and encouraging enough for startup ventures? What could be done to encourage startups as well as their success?

No, I don't think so. Not many people actually understand the term _"startup_". I think money and good mentors are the essential things while starting out. If we could get a good sync between the two, we should see good amount of rise in startups.

### Q) Would you agree that support networks are essential to keep the entrepreneurial spirit alive? What kind of support you continue to tap and what networks are available for Indian entrepreneurs?

Absolutely! Having a few people around you who are experiencing the same thing as you are going through helps a lot - communities like that should come up. There are ofcourse open-coffee-clubs in various cities - but those are very informal. At chennai, a few people are building something called as [Startup Centre](http://www.thestartupcentre.com/) which looks promising!

### Q) Why startups are essential for a growing economy like ours? Do startups contribute only financially to the country? What other contributions do startups make to a country's fabric?

I might not be the right person to answer this, since I haven't thought about these things. I started interviewstreet because I badly wanted to! However, more number of startups give rise to a lot of innovation simplifying millions of lives!

### Q) We constantly hearing new startup ventures now-a-days. Is India moving into product innovation space from just services?

A: A lot of startups are coming up - the awareness is definitely increasing, but I am not sure about the numbers and hence might not be able to comment.

### Q) What product innovation have you brought into the market with your startup?

I think hiring/recruitment has been a perennial problem in companies of all sizes (startups/SME's/MNC's). Our product helps us to match the right candidates for the company - increasing the productivity of employees.

### Q) Have you ever regretted your decision? Was there a moment you wanted to throw up everything and join a MNC again?

Hmmm...regretted wouldn't be the right word - but yes, when you are cash strapped and you see people around you "moving forward" (according to the world) - you do feel you have been left behind in the race. But I am sure, this would pay off later!

### Q) Finally, what advice or suggestions you would give to those who start in this path?

I am too young to comment on this. But as I said earlier, starting a startup is more like an itch - you know there would be problems, you know you are going to undergo a lot of pain, you know every problem associated with it, but still you give it a shot. It's natural and the learning curve is exponential, take a calculated decision - it's only a matter of time before you jump in - no entrepreneur can sit with ideas on his head not going live.

If you are a student or a company check out [InterviewStreet](http://interviewstreet.com/). You will surely benefit out of their innovative product. But much more than that let us wish that Vivek and Hari will be next Steve Jobs & Steve Wozniak or Bill Hewlett and Dave Packard. India needs its own startup-success stories in product engineering. Thank you Vivek and Best Wishes to you and Hari.
