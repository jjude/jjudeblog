---
title="Book Notes - Networking like a Pro"
slug="network-like-pro"
excerpt="There are three phases of networking--visibility, credibility, and profitability."
tags=["wins","networking","books"]
type= "post"
publish_at= "08 Sep 19 06:00 IST"
featured_image="https://cdn.olai.in/jjude/network-like-pro.jpg"

---

![Networking like a pro](https://cdn.olai.in/jjude/network-like-pro.jpg "Key lessons from Networking Like a Pro book")

_This is not a summary or review of the book. Rather ideas as presented in the book, may be regrouped as per my likes._

- Networking is about **nurturing mutually beneficial relationships**, which brings you new connections with large numbers of people, some of whom will become good customers.
- Networking is the mainstream business development technique of the future
- If you are expecting to find a direct, immediate correlation between your networking activities and the dollars you harvest as a result, you are going to be sorely disappointed.
- One of the most **important metrics is the number of coffee connections** (follow-up meetings) you have with your new contacts—at least, the ones you want to network with. A contact that you do not follow up with is a contact that will never become part of your network.
- If you are an introvert, become a "**situational extrovert**" - be a loner, reserved ardound strangers, but very outgoing in the right context.
- Have a network of contacts that provide support, information, and business referrals.
- Sometimes you go to a networking event to increase your **visibility**, sometimes you go to establish further **credibility** with people you know, and sometimes you may even go to meet a long-time referral partner and do some business and move to **profitability**.
- Networking can be positive if the networking is about the relationship and not about the transaction.
- **Success is social**: all the ingredients of success that we customarily think of as individual — talent, intelligence, education, effort, and luck—are intertwined with networks.
- Don’t be misled: it is not the number of contacts you make that’s important; it's the ones you turn into lasting relationships.
- **Put together a hit list of 50 people you’d like to stay in touch with this year**.
- Law of reciprocity
  - The law of reciprocity differs from the standard notion of reciprocity in that the giver cannot, should not, and does not expect an immediate return on her investment in another person’s gain. The only thing that she can be sure of is that, given enough effort and time, her generosity will be returned by and through her network of contacts, associates, friends, family, colleagues, and others—many times over and in many different ways.
  - The **law of reciprocity requires giving** to the group; it will pay you back many times over
  - The law of reciprocity validates the abundance mindset
  - The way of reciprocity is not simply a quid pro quo; it’s providing benefits (including referrals) to others in order to create strong networking relationships that will eventually bring benefits (especially referrals) to you, often in a very round-about way rather than directly from the person you benefit.
  - The person who helps you will not necessarily be the person you helped
  - **What is your plan to contribute to others?** How much time and energy you spare for others to achieve success? Do you actively seek out opportunities to help others?
  - Success takes getting involved. It is not just showing up. Get involved in community events, other’s events to make them success
- Scarcity is an illusion
- Farming vs hunting
  - Networking is about farming for new contacts, not hunting them. Most business professionals go about networking the way our cave-dwelling ancestors did when hunting for food—aggressively and carrying a big stick
  - "Farmers" don’t look for right persons; rather they plant seeds and patiently further their crops, they seek to form and build relationships whoever they can find them.
  - Truly profitable relationship can't be rushed
- Relationship marketing in general and networking in particular is about consistency and reliability: consistently meeting new people and reliably following up with the folks you have just met
- Linchpins are people who in some way cross over between two or more clusters or groups of individuals; this allows them to link groups of people together easily. The best way to increase the number of possible connections in your network is to intentionally develop a diverse, heterogenous network instead of a homogenous one
- May be tech will improve that we will hold business networking meetings in orbit (if Virgin Galactic gets its goals done)
- **Phases of growing relationship: visibility, credibility, and profitability**
  - Visibility creates recognition and awareness
  - Credibility is the quality of being reliable, worthy of confidence. Credibility grows when appointments are kept, promises are acted upon, facts are verified, and services are rendered.
  - Results speak louder than words
  - If it doesn't profit both partners, it probably will not endure
- The best compass for networking is the simple, old-fashioned notion "Know thyself." Go back to basics: **what do you want from networking? What are you willing to put into your network relationships**.
- Face-to-face contact is irreplaceable
- One of the biggest mistakes we see business professionals make is trying to be everything to everyone. In other words, they have a great product and terrific service and in their minds everyone should be a client

Go to the [Amazon Page](https://geni.us/network-like-pro) for details, review and purchase.

### You may also like:

- [Topics of interest - 2019-20](/topics1920/)
- [Thriving with WINS](/wins/)
- [Books I’ve Read](/books-read/)
