---
title="Subscribe to Gravitas WINS Newsletter"
slug="subscribe"
excerpt="Build Gravitas . Attact Luck"
tags=[]
type= "page"
publish_at= "21 Jul 20 07:33 IST"
featured_image="https://cdn.olai.in/jjude/subscribe.jpg"
---

<iframe src="https://gravitaswins.substack.com/embed" width="480" height="320" style="border:1px solid #EEE; background:white;" frameborder="0" scrolling="no"></iframe>

_The outside world can push you into Day 2 if you won’t or can’t embrace powerful trends quickly. If you fight them, you’re probably fighting the future. Embrace them and you have a tailwind. - Jeff Bezos_

Every week I talk to experts shaping business and technology and bring you the best of the insights from our conversations. If you want to have an edge in your career and life, you want to know these trends.

I send this email every Wednesday. Think of it as, Wednesday Wisdom.

When you sign up, you'll also receive a **free** copy of the Gravitas WINS weekly-wins worksheet.

I hope you will find value in every newsletter. If you're disappointed continuously, you can always unsubscribe. Every newsletter contains a link to unsubscribe.

Thank you for your time!

Warmly,    
Joseph Jude

### Checkout some of the previous issues

* [Surgery Is Successful 💃, Patient Is Dead 😳](https://mailchi.mp/75ab956584ac/surgery-is-successful-patient-is-dead)
* [Trends As A Guide To The Future](https://mailchi.mp/6429da029f94/trends-as-a-guide-to-the-future)
* [Superstars don't bring success](https://mailchi.mp/eac90afc1ea5/superstars-dont-bring-success)
* [Build A Personal Flywheel](https://mailchi.mp/e49ac8a0ebfd/how-to-build-a-personal-flywheel)
* [What's Your Process For Making Decisions?](https://mailchi.mp/ed7a2b8b132b/whats-your-process-for-making-decisions)


### What Others Are Saying

"Joseph has got the knack of fetching maximum insights from the other person in the conversation.  He doesn’t only cover the work-related aspect of life but also touches upon other things which matter a lot in life." - **Rishabh Garg**
     
"I have known JJ closely for some years now, and the simplicity of his thought process is mind-blowing !!!  A voracious reader, he knows the art of converting information to knowledge… and he makes it all look so simple." - **Ritika Bhatia**

<iframe src="https://gravitaswins.substack.com/embed" width="480" height="320" style="border:1px solid #EEE; background:white;" frameborder="0" scrolling="no"></iframe>