---
title="When To Listen To Your Critics?"
slug="critics"
excerpt="You better listen to critics. But not when you ideate."
tags=["coach","startup","insights","gwradio"]
type="post"
publish_at="27 Jan 14 16:30 IST"
featured_image="https://cdn.olai.in/jjude/listen-to-crticis.png"
---

<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless src="https://share.transistor.fm/e/12ffc991"></iframe>

Critics come in all form. Some are vampires, others genuinely wants to help you.

You should always ignore the first kind. Always. No exception. Not now. Not later. Never listen to them. They cherish when others fail. They suck the enthusiasm out of you. If you are an artist, writer, entrepreneur or any kind of creator, enthusiasm is your blood. Don't let anyone suck even a drop of it.

Listening to other type, the well-wishing critics, depends on where you are at in your creation cycle. There are three stages in any creation cycle - ideate, execute, succeed.

When you contemplate an idea, you don't want to listen to 'Ten reasons why that idea sucks.' **A seed needs time to flourish; an idea needs time to take shape**.

![Listening to critics](https://cdn.olai.in/jjude/listen-to-crticis.png)

As you execute your idea, you want feedback on how well your plan is moving along. As much as you hate to admit, actionable feedback usually comes from your critics. **Listen, adjust, and execute to win**.

But when you succeed, you need to pause to listen to your critics. Many a successful men have fallen from the top because they were blinded by their journey. They get **tied to the pattern that has taken them to the top**. They race through the trodden path only to meet their failure.

When you are succeeding, pause to analyse if your critics are pointing to your blind-spot. If you aren't arrogant, you might still have a chance to continue the path of success.
