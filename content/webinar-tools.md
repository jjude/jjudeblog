---
title="Tools For Successful Webinars"
slug="webinar-tools"
excerpt="Tools I use for weekly Gravitas WINS show"
tags=["tools","systems","wins"]
type="post"
publish_at="01 Mar 21 15:12 IST"
featured_image="https://cdn.olai.in/jjude/gw-show-page.jpg"
---

I started a weekly [webinars](https://lu.ma/jjude) for my coaching program: [Gravitas WINS](https://gravitaswins.com). Here are the tools that help me to keep things in motion.

![Gravitas WINS Show Page](https://cdn.olai.in/jjude/gw-show-page.jpg "Gravitas WINS Show Page")

### Obsidian

All my notes are stored in [Obsidian][1]. I store topics I want to cover, random notes on the topics, and lessons learned from each session. I think as I write, hence Obsidian has become my "digital brain."

### Zoom

I take each session via [Zoom][6]. By now, everyone knows Zoom. Right?

I tried (and try) many different webinar tools. Zoom strikes an optimal balance between the features I want, ease of use for both me and participants, and importantly the annual fee. So I have settled on Zoom for all my session.

### Luma

![Luma Analytics](https://cdn.olai.in/jjude/luma-analytics.jpg "Luma Analytics")

I use [Luma][2] to create awesome looking event pages as quickly as possible. I use Luma to send invitations, reminders, and feedback emails. Luma also captures insights for each event which helps me fine-tune successive events.

With all its features, Luma has become the hub of running the Gravitas WINS show.

### LinkedIn

I promote every event on [LinkedIn][3], because that is where most of my audiences are. 

![Linkedin Event Page](https://cdn.olai.in/jjude/linkedin-event-page.jpg "Linkedin Event Page")

LinkedIn algorithms are not optimised for me; they are optimised for them. It will not surface my event on top of the feed. So I create an event and then invite people individually. This gives me huge boost in registration and attendance. 

### Whatsapp

Surprisingly, most of the registrations come via [WhatsApp][4]. I post on Whatsapp Groups (just 3 groups) and on Whatsapp status. I find conversion ratio of whatsapp is higher than other channels. May be because they have my number (so the trust levels are higher)

### Sketch

I design all l slides using [Sketch][5]. I prefer sketch, because I know it well by now and I'm productive in it. Also it works offline unlike Figma. In India, even in cities with high bandwidth plans, going offline is a normal routine.

### YouTube

I live stream to [YouTube][7] channel, so anyone who was not able to join the show can view them. An YouTube channel also forms as a great lead generator for the Gravitas WINS course. 

I wish there was an easy way to schedule the live streaming from Zoom.

### MacBook

My workhorse is MacBook Pro 15". It just works. 

### Airtel Fiber / Vodafone Mobile Network

Airtel fiber is my primary network. It works well until it is not, which gives me anxiety whenever I schedule the weekly live shows. When the network goes down Airtel will take at least three hours to rectify. Since so much of this venture depends on the high bandwidth network, it is frustrating every time it happens.

As a backup, I have Vodafone mobile network. It gives me good speed as a replacement network. 

### Still expanding the list

I've only started. I'm still looking to expand the list. Bookmark this page and visit again to view the updated list. Better yet, [subscribe](/subscribe/) to my newsletter if you want to know as soon as I update it. 

Do you have any comments? Tell me via [Twitter](https://twitter.com/jjude) or [email](https://jjude.in/email)

[1]: https://obsidian.md/
[2]: https://lu.ma/
[3]: https://www.linkedin.com/
[4]: https://www.whatsapp.com/
[5]: https://www.sketch.com/
[6]: https://zoom.us/
[7]: https://www.youtube.com/channel/UCPDIeR7Lzvt9uATvMZSZX-Q


## Continue Reading

* [System for success](/system-for-success)
* [Essential tools to build traction for your startup](/traction-tools-for-startups)
* [Build An Ecosystem For Learning](/build-an-ecosystem-for-learning)