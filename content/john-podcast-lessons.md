---
title="John Mohana Prakash on 'Podcasting Lessons From an Internet Radio Host'"
slug="john-podcast-lessons"
excerpt="Are there useful insights podcasting hosts can learn from a successful Internet radio host?"
tags=["gwradio"]
type="post"
publish_at="15 Mar 22 05:00 IST"
featured_image="https://cdn.olai.in/jjude/john-fm.jpg"
---
Hello and welcome to Gravitas WINS conversations. Before podcasts became a fashion, we had radios. We all grew up listening to songs, dramas, and news. Are there lessons podcast hosts can learn from Internet radio hosts? That is the topic of today's conversation. Hope you will enjoy our conversation.

<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless src="https://share.transistor.fm/e/663da77f"></iframe>

## What you'll learn
- How to start an Internet radio?
- How to engage with listeners on an Internet radio?
- What are the business models around Internet radios?

![John Mohana Prakash](https://cdn.olai.in/jjude/john-fm.jpg)

## Connect with John

- Website: http://www.inbamfm.com/
- Facebook: https://www.facebook.com/johnmohanaprakash

## Watch on YouTube
<iframe width="560" height="315" src="https://www.youtube.com/embed/YqsYLTo8fBw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Connect with me
- Twitter: https://twitter.com/jjude
- LinkedIn: https://www.linkedin.com/in/jjude/
- Website: https://jjude.com/
- Newsletter: https://jjude.com/subscribe/
- Youtube: https://youtube.com/gravitaswins
- Email: podcast@jjude.com
- Executive Coaching Program: https://gravitaswins.com

## Your feedback counts

Thank you for listening. If you enjoy the podcast, would you please leave a short review on Apple podcast or on YouTube? It takes less than 60 seconds, and it really makes a difference in finding this podcast. And it boosts my spirits. 😀

I would like to hear what resonated the most with you in this episode. Either you can email me or share it on social media and tag me. Thank you for your support.

Have a life of WINS.

## Amazing tools I use:
- I record all the remote interview with [Riverside.fm](https://riverside.fm/?via=joseph-jude)
- I use [Transistor.fm](https://transistor.fm/?via=jjude) for hosting these podcast episodes

_Gravitas WINS Radio is produced by [Joseph Jude](https://jjude.com/). Be sure to subscribe on [Apple Podcast](https://podcasts.apple.com/in/podcast/gravitas-wins-radio/id1562802449), [Spotify](https://open.spotify.com/show/7LOBjAyjBkechMC0xMIj9E),[Google Podcast](https://podcasts.google.com/feed/aHR0cHM6Ly9mZWVkcy50cmFuc2lzdG9yLmZtL2dyYXZpdGFzLXdpbnMtcmFkaW8=),[YouTube](https://www.youtube.com/gravitaswins), or the podcasting app of your choice._

Please check out other episodes of [Gravitas WINS Radio](/gwr-themes/). For easy consumption, they are categorized by themes. If you are a fellow podcaster, you might be interested in [How I Podcast](/how-i-podcast/) or [tools I use](/i-use/).

## Continue Reading

* ['Secrets of A Master Storyteller' with Jayaram Easwaran](/jayarame/)
* [Winning With Ideas](/winning-ideas/)
* ['Mindset for Creator Economy' with Tanmay Vora](/tanmay-creator-economy/)