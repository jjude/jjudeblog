---
title="Rich Repository of Business Cases"
slug="rich-repository-of-business-cases"
excerpt="Practicing with busienss cases facilitate developing a structured approach to solving problems."
tags=["problem-solving"]
type="post"
publish_at="19 Feb 11 16:30 IST"
featured_image="https://cdn.olai.in/jjude/consulting101.png"
---
You can master any amount of theory about problem solving, but all of it is just academic unless you convert that learning into practice. One of the practical applications of problem solving for business organizations is to understand and resolve challenges faced by their executives.

But it is impossible to shadow executives in various industries to know every challenges they face. That's where business cases come in handy. They describe the situation and provide as much information needed about macro & micro conditions surrounding the business. Then you got to come up with a solution as if you are in-charge of the situation.

![Repository of business cases](https://cdn.olai.in/jjude/consulting101.png)

Practicing with such business cases facilitate you to develop a [structured approach](/mckinsey-way/) to the issue, hypothesize logically and then derive the best solution.

[Consulting 101](http://www.consultingcase101.com/) is a rich repository of such business cases. The cases are categorized by industries and case types (ex: market sizing, market entry).

These cases are specific type of practical problem solving and they provide you an opportunity to understand the real-life scenarios. Textbooks may offer you time-tested concepts but it is these cases that help you apply those concepts. Read through them; think through them and improve your problem-solving skills.

_This post is part of ‘[Be a Problem Solver](/be-a-problem-solver/)‘ series._
