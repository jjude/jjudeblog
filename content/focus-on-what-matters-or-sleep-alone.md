---
title="Focus On What Matters Or Sleep Alone"
slug="focus-on-what-matters-or-sleep-alone"
excerpt="If you are an independent software developer, there is only one metric to measure & improve."
tags=["coach","startup"]
type="post"
publish_at="22 May 13 16:30 IST"
featured_image="https://cdn.olai.in/jjude/focus-on-what-matters-or-sleep-alone-gen.jpg"
---

>*A friend of mine was getting married. As is norm in Indian culture, it was an arranged marriage. He was asked, by his parents, what type of girl would he want to be his wife. After long discussions with his friends he came up with just two:*
>* _she should be pretty (no brainer, all guys want pretty girl as wife)_
>* _she should be working (costs are escalating, so it's better to have two income)_
>*It looked reasonable for his parents and like any responsible Indian parents, they were proud of their kid - for being thoughtful and reasonable. They went searching and one fine day he got married. His wife radiates with beauty and she works in an MNC. All were relieved at the turn of events.*
>*On the first-night, the beautiful lovely lady said, "I don't want kids - it will spoil my career & beauty. And I don't like sex life too. So you sleep there, I will sleep here."*

When I released BlogEasy, a blogging tool for Mac & iOS, I read countless articles on increasing sales. Almost all of them emphasized the importance of analytics and elaborated how analytics is fundamental to sales and marketing.

They educated me to analyze who is searching for similar apps, how many land on the [website][13], where they are coming from, who is downloading, how many are buying, how they are using, which age group are they from and so on and so on.

So I signed up with tracking services — [Statcounter][2], [Mixpanel][3], [App Annie][4], [flurry][7], [Google Analytics][9] and started measuring numbers. I became obsessed with analytics like a kid who got a new shiny toy to play with. It was exciting. I even started measuring [twitter][5] followers and Facebook [page][6] likes!  Because…well, just because I could measure them.

I became fixated in improving each of these numbers.

But as days went by, the obsession waned off. The toy was not so fascinating anymore. I was coming to senses. And as I was coming to senses, a realization kicked in.

What matters to business is cash — cash in hand and cash-flow. Everything else should be a leading indicator of the king. Each tracking number in itself means nothing if it is not in the path of increasing cash.

Increasing twitter followers by tweeting jokes and inspirational quotes is useless because these followers won't buy; posting links in [Stumble Upon][8] doesn't help since that traffic is people with remote-control and they won't open their wallet; commenting on irrelevant blogs don't help for the same reason.

Instead of looking at irrelevant numbers, I had to focus on what matters.

If you are selling through AppStore, there are only two aspects you need to focus to improve cash: **making an awesome product** and **marketing**. Everything else[^1] is taken care of by Apple (or you don't have absolutely no control).

Even after ten months since released, BlogEasy doesn't have important features like adding images. Now I'm focusing on improving the product which, definitely, is in the path of increasing cash. UserVoice, a customer support tool, and [Flurry][7] give me the needed insights for new features.

It is in marketing that a solo-developer loose sight of the king and get drifted. By now, I [know][12] drifting has heavy impact. I am concentrating on focused marketing.

Hope I won't sleep alone.


[2]: http://statcounter.com/
[3]: https://mixpanel.com/
[4]: http://www.appannie.com/
[5]: http://twitter.com/blogeasyapp
[6]: https://www.facebook.com/pages/BlogEasy/505489692808389
[7]: http://www.flurry.com/
[8]: http://www.stumbleupon.com/
[9]: http://www.google.co.in/analytics/
[11]: https://www.google.com/webmasters/
[12]: /achieve-your-goals/
[13]: http://blogeasyapp.com

[^1]: Like hosting your app, processing payments, remitting to your bank account etc. If your app gets listed in AppStore, it increases the chance of being found, paid and downloaded rather than if you try to sell it outside AppStore (Mac apps can still be sold outside AppStore)

