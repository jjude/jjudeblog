---
title="Ruby Grace on 'Impact of technology on mental health'"
slug="ruby-mental-health"
excerpt="How to be productive and ambitious while also being mentally resilient"
tags=["gwradio","biz"]
type="post"
publish_at="20 Sep 23 14:48 IST"
featured_image="https://cdn.olai.in/jjude/ruby-yt.jpg"
---
Technology is the beating heart of innovation and progress. As technology executives, we are at the forefront of such advancement. The same technology that helps us grow, also affects our mental health.

To discuss the connection between technology and mental health, I have invited Ruby Grace, a licensed rehabilitation psychologist. She has extensive experience in understanding the intricate relation between technology and mental health. She has helped many students and professionals navigate this complex landscape. Hope today's conversation helps you at least a little bit towards improving your mental health.

<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless src="https://share.transistor.fm/e/2376b19c"></iframe>

## What you'll hear
- Definition of mental health
- What are the short-term mental health issues?
- Recognising short-term issues
- Being productive and mentally healthy
- Impact of working in different timezones
- Being ambitious and being mentally healthy
- How companies can take care of their employees' mental health?
- How to build mental resilience?
- When to seek medical help?
- How to keep track of one's mental health?
- Dealing with mental health as an escape route
- Kindest thing anyone has done for you
- Best leadership quality
- Definition of living a good life


## Edited Transcript
### Definition of mental health
We often discuss mental health extensively these days, encompassing our behaviors, thoughts, and emotions. It's crucial to approach mental health in a wholesome manner, ensuring that we think and act in ways that promote well-being. However, the World Health Organization (WHO) provides a specific definition for mental health.

According to WHO, mental health denotes a state of well-being in which an individual **recognizes their own abilities, can effectively manage the usual challenges of life, and contributes positively to their community or society**. This definition serves as WHO's perspective on mental health. In more colloquial terms, mental health encompasses our emotional, psychological, and social well-being.

The state of our mental health significantly influences how we think, act, and feel. When we don't properly maintain our mental health, it inevitably has an impact on these aspects of our lives.

### What are the short-term mental health issues?
Some individuals experience enduring mental health issues, commonly referred to as psychosis. These conditions persist over the long term, necessitating ongoing medication, therapeutic interventions, and family support. However, it's important to note that similar to other illnesses like diabetes, there are biological changes occurring. Hormonal shifts and alterations in neurotransmitters take place, affecting our mental well-being.

These changes in neurotransmitters can result in short-term mental health challenges alongside the long-term ones, both of which individuals may encounter.

### Recognising short-term issues
Short-term mental health concerns encompass various symptoms such as heightened anxiety, insomnia, palpitations, and excessive stress without clear reasons. While exams naturally induce stress, it's crucial not to exacerbate it unnecessarily. These minor, short-term mental health issues predominantly revolve around the prevalent anxieties and stressors in our lives today.

Due to this anxiety, some individuals might exhibit signs they don't fully comprehend, such as overthinking, restlessness, pacing, irrational thoughts, profuse sweating, trembling hands, and shaky legs. Notably, mood swings and persistent irritability can also manifest as small-scale indicators of an underlying issue.

Recognizing these changes in your daily routine is vital. If you notice such signs in yourself, it's advisable to seek support from a close friend, therapist, or professional. In particular, severe sleep disturbances and significant mood fluctuations should be handled with great care and prompt attention.

### Being productive and mentally healthy
Technology is undoubtedly a boon in our lives, an essential tool in this digital world without which survival seems impossible. From mobile phones to laptops, we rely on technology for practically everything. However, there's a flip side to this coin. Overuse and excessive dependency on technology have far-reaching implications, especially on our mental health.

Even the tiniest notification can have a profound impact on us. Whether it's an email from the office or a message in the school WhatsApp group, we tend to react impulsively to these alerts. This constant connectivity can lead to significant mental health issues.

Information overload is another consequence of our tech-savvy lives. Beyond the devices themselves, it's the endless stream of content we consume that affects us. Websites, YouTube, Google, and the like inundate us with information, but the critical question arises: Is the information we're receiving accurate and reliable? This uncertainty can contribute to excessive worrying and anxiety.

For instance, a simple Google search about a headache can escalate into anxiety-inducing thoughts about severe medical conditions like migraines, tumors, or even cancer. This spiral of anxiety is often fueled by the information we find online.

The fear of missing out, or FOMO, is another significant aspect of our tech-driven lives. People, especially youngsters, regularly share updates on social media. The need for validation from others leads to constant monitoring of likes, views, and responses. The anxiety of not receiving enough validation or attention from the online community can be emotionally draining.

In essence, technology affects us in three primary ways. Firstly, there's the direct impact of spending excessive time on mobile phones and laptops, often leading to time wastage. Secondly, there's the information overload from the internet, which can be overwhelming. Lastly, there's the emotional toll of seeking validation and social affirmation through online interactions. It's crucial to recognize these effects and find a healthy balance in our relationship with technology.

### Impact of working in different timezones
Given the diverse time zones that many of our fellow Indians work in, it's essential to carve out time during the day for adequate rest and sleep. **Sleep is a cornerstone of overall well-being**, and getting proper rest is crucial. However, the ubiquity of technology can often divert our attention towards devices like the TV, laptop, or mobile phone.

It's worthwhile to take a deliberate step towards prioritizing our sleep and rest, much like how we diligently follow a doctor's prescription for medication. We shouldn't wait for a doctor to instruct us to take medicines; similarly, we shouldn't delay self-care when it comes to our sleep patterns. Recognizing that our time zone differs from others, we can proactively take small steps to care for our mental health.

In today's digital age, self-care has gained considerable importance. By being more conscientious about our mental health, we can incorporate simple practices like short naps or brief periods of rest into our daily routine. These actions not only help us feel rejuvenated but also contribute to maintaining our circadian rhythm, ultimately enhancing our work experiences and fostering a healthy work-life balance.

### Being ambitious and being mentally healthy

CEOs face unique expectations within their organizations, which often involve delegating tasks while maintaining a vigilant eye on their own capabilities and workload. Striking a balance becomes crucial. Trusting others with responsibilities is vital because, ultimately, our families depend on us, and our mental and physical well-being are equally significant. It's worth noting that excessive stress and anxiety can lead to psychosomatic issues, manifesting as headaches, digestive problems, skin allergies, or rashes.

CEOs often experience heightened expectations from their organizations, but they can make their lives more manageable by practicing effective time management, establishing a structured routine, and delegating tasks while maintaining trust in their team members. Regular feedback checks can help gauge their progress and effectiveness. This systematic approach helps CEOs stay on track and ensures they are making the right decisions.

Maintaining a daily routine is essential, as CEOs sometimes forget to take care of simple things, like meals, due to their workload. Managing such basics can significantly improve their well-being. It's also crucial for CEOs to assert themselves when needed. If they're unable to participate in a meeting or need time for rest, communicating these needs is vital to ensure productivity.

In the business world, results matter. Regardless of the volume of work, the ultimate goal is profitability. CEOs must always focus on achieving their end goals for the organization's success.

### How companies can take care of their employees' mental health?
In today's work environment, HR departments frequently implement employee engagement programs. However, in addition to these efforts, it's crucial to send out regular weekly emails addressing mental health topics. This is because employees may not openly express their mental health challenges or their need for a break from work. They often won't come forward and say, "I'm struggling with mental health issues, and I need time off to see a mental health professional."

To address this, there are two key approaches organizations should adopt. First and foremost, they should focus on creating awareness about mental health within the workplace. This is the initial and fundamental step. Additionally, managers play a pivotal role as they work closely with their teams. They should pay close attention to any changes in their employees' behavior or attitudes. 

For instance, if employees start displaying increased irritability or frequent outbursts of anger at the office, managers should engage in subtle, non-intrusive conversations. This approach is particularly important for supervisors and managers. They should schedule informal gatherings or meetings every 10 days or so, unrelated to work matters. These gatherings provide an opportunity for employees to engage in conversations.

In today's fast-paced world, people often find it challenging to make time for such discussions. However, even a brief coffee break and casual conversations about non-sensitive topics like politics or recent Bollywood movies can be quite effective. These indirect conversations can pave the way for more direct discussions about personal experiences. For example, a manager might casually mention experiencing sleepless nights, indirectly signaling to the team that it's okay to discuss personal challenges.

This approach allows employees to open up about their stress, both at work and at home. It's a proactive way for organizations to take a stand on mental health and show genuine care for their employees' well-being. By creating awareness about mental health and fostering a culture of open dialogue, organizations can significantly improve the overall mental health and productivity of their workforce.

### How to build mental resilience?
There are several strategies that can help in reducing overthinking, which can be challenging to control once it starts. However, it's crucial to remember that our thoughts are within our control, and we can manage them through immediate diversion. Therapists often emphasize the importance of creating a diverting environment for individuals facing mental health challenges.

To tackle overthinking, we should first understand its stages. Overthinking often involves making assumptions that lead to conclusions, whether they are based on reality or not. When we find ourselves overthinking, we must implement immediate diversions. For example, if you're in the office and notice you're overthinking, step away from your desk, go to a nearby restroom, wash your face, and return. This break helps reduce the intensity of your thoughts and the chain of overthinking.

Adequate sleep is also crucial for managing overthinking. Connecting with trusted individuals, like your spouse, children, partner, or friends, when you're feeling low can be immensely helpful. Engaging in activities like listening to music or expressive art, such as coloring, can provide immediate relief. Artwork, coloring, and journaling help express and process thoughts and emotions.

Engaging in activities like gardening or pottery can also be therapeutic for mental health. For those experiencing panic attacks or anxiety, it's advisable to keep a water bottle and candies on hand for soothing. In moments of panic, open a window to breathe deeply, as controlled breathing techniques are beneficial for short-term mental health concerns. Breathing exercises, like counting breaths and exhaling stress, can help in managing stress and anxiety.

### When to seek medical help?
In the current climate, there's a growing awareness of mental health issues, and this awareness extends to various institutions, including organizations, schools, and colleges. It's important to take a similar approach to mental health as we do with physical health. For instance, when we have a fever, we take our temperature at home and administer basic first aid. In the same way, we should be observant of our mental well-being.

Pay attention to signs like continuous sleeplessness for two or three days, unexplained crying spells lasting for a day or two, frequent emotional breakdowns, and increased irritability. If these symptoms persist for more than four to five days, it's advisable to seek professional help. At this stage, consulting with a psychologist who specializes in counseling can be beneficial. These professionals can offer guidance and various therapeutic approaches that can help.

Sometimes, we hesitate to share our thoughts and emotions with family members, fearing that we might burden them. This is where a therapist plays a crucial role. Therapists are nonjudgmental and maintain strict confidentiality, creating a safe space for individuals to express their concerns. The trust gradually builds as individuals realize that they can confide in a therapist without judgment or fear of their privacy being compromised.

It's essential to recognize that suppressing emotions can lead to mental health issues. Instead, we should aim to release our emotions as they arise to prevent them from affecting our daily lives negatively. Therefore, seeking professional help after observing such signs and symptoms for a few days can be a proactive step towards maintaining good mental health.

### How to keep track of one's mental health?
As we delve into the impact of technology on our lives, it's crucial to consider the concept of **digital detox**. In today's world, we're heavily reliant on digital devices for everything from online payments to work emails, making it challenging to disconnect entirely. However, there are steps we can take to reduce our digital dependency and promote better mental health.

One effective strategy is **managing notifications**. Not all notifications are urgent or necessary, especially on platforms like LinkedIn or Instagram. Clearing unnecessary notifications and putting them in the background can help declutter our digital environment.

Implementing a **digital detox by setting aside four to five hours** without constant device access is another valuable approach. During this period, only respond to calls that are genuinely important, such as those from your boss or parents. Limiting your digital interaction to crucial matters can be difficult, but it's a meaningful step toward improving your mental well-being.

Additionally, consider **physically distancing yourself from your gadgets**. Small changes, like placing your devices slightly out of reach, can encourage you to take baby steps toward reducing your technology usage.

Moreover, **cultivating self-confidence is vital**. Instead of constantly seeking external validation through social media or comparing yourself to others, focus on your own abilities and what you can accomplish. Understand that your qualifications, skills, and background are enough, and you don't need to fear missing out on what others are achieving. Building this mindset takes time and effort but is essential for nurturing your mental health.

In essence, beginning with foundational changes and gradually adopting healthier digital habits can significantly contribute to better mental well-being. While it may be challenging, it's entirely possible to navigate the digital age while maintaining a positive and resilient mindset.

### Dealing with mental health as an escape route
There are one or two ways, for example, in HR, the organization should conduct small training sessions with specific managers. This is important because it helps address issues before employees start making excuses for their performance. Before they come up with reasons like, "I couldn't do this properly because of XYZ," it's crucial to provide these managers with some guidance.

So, having one or two training sessions with managers can be quite effective. Another approach we believe in is **having one-on-one conversations**. When an issue persists, whether it's the first time or the third, it's important to schedule a one-on-one meeting with the individual. The key here is not to confront them but to engage in a polite and genuine conversation.

In this conversation, it's important to express concerns, offer support, and encourage open communication. If an employee needs a break for a day or two due to workload or any other reason, they should feel comfortable sharing this. It's crucial because such issues can affect our work environment.

When dealing with mental health concerns, it's vital to exercise caution and avoid being judgmental. Mental health matters are sensitive and challenging to identify from the outside. Therefore, a small, private one-on-one conversation can be very effective.

**Observation** is also essential. Keeping an eye on employees' behavior, like frequent breaks or extended coffee breaks, can help detect potential issues. It's recommended to maintain vigilance and even review their work logs over the past week.

This approach can be particularly beneficial for managers who often need to report on employee performance and address any concerns that arise.

### Rapid-fire questions
#### Kindest thing anyone has done for you
I have a strong connection with my family, and that's something I truly cherish in my life. It's a source of constant kindness that I always look forward to.

#### Best leadership quality
Leadership plays a crucial role in encouraging and motivating their employees. Recognizing their efforts, showing appreciation, and providing encouragement are essential actions that can significantly improve the employee experience.

When leaders take the time to **acknowledge and appreciate the work their employees do**, it fosters a positive atmosphere and makes employees more motivated to excel. In my own workplace, I have a great relationship with my immediate boss. She consistently shows appreciation and provides encouragement. When she encourages us to come up with new ideas, it energizes us and motivates us to innovate.

This type of relationship between leaders and their employees, between bosses and managers, is something I've personally experienced, and it has been extremely beneficial.

#### Definition of living a good life

Gratitude. I firmly believe that regardless of the circumstances we face, we can choose to live our lives with contentment and a grateful mindset. It's essential to accept things as they are and find contentment in them. 

Being grateful for what we have can greatly enhance our lives and our overall well-being. So, in summary, cultivating gratitude and living with contentment are two fundamental elements that contribute to a fulfilling life.


## Connect with Ruby Grace
- LinkedIn: https://www.linkedin.com/in/ruby-grace-selvaraj-1b540a100/

## Connect with me
- Twitter: https://twitter.com/jjude
- LinkedIn: https://www.linkedin.com/in/jjude/
- Website: https://jjude.com/
- Newsletter: https://jjude.com/subscribe/
- Youtube: https://youtube.com/gravitaswins
- Email: podcast@jjude.com
- Executive Coaching Program: https://gravitaswins.com

## Your feedback counts

Thank you for listening. If you enjoy the podcast, would you please leave a short review on Apple podcast or on YouTube? It takes less than 60 seconds, and it really makes a difference in finding this podcast. And it boosts my spirits. 😀

I would like to hear what resonated the most with you in this episode. Either email me or share it on social media and tag me. Thank you for your support.

Have a life of WINS.

_Gravitas WINS Radio is produced by [Joseph Jude](https://jjude.com/). Be sure to subscribe on [Apple Podcast](https://podcasts.apple.com/in/podcast/gravitas-wins-radio/id1562802449), [Spotify](https://open.spotify.com/show/7LOBjAyjBkechMC0xMIj9E),[Google Podcast](https://podcasts.google.com/feed/aHR0cHM6Ly9mZWVkcy50cmFuc2lzdG9yLmZtL2dyYXZpdGFzLXdpbnMtcmFkaW8=),[YouTube](https://www.youtube.com/gravitaswins), or the podcasting app of your choice._

Please check out other episodes of [Gravitas WINS Radio](/gwr-themes/). For easy consumption, they are categorized by themes. If you are a fellow podcaster, you might be interested in [How I Podcast](/how-i-podcast/) or [Tools I Use](/i-use/).

## Continue Reading

* [Principles trump processes](/principles-trumps/)
* [The curse of EVERYTHING and NOW on building your career](/all-and-now/)
* [Three part solution to leaders burnout](/leaders-burnout/)