---
title="Manish Kumar on 'Edge Computing 101'"
slug="manishk-on-edge-computing"
excerpt="Edge computing will become a utility like electricity and internet. That is what my guest of this episode says. Listen, enjoy, and share your feedback."
tags=["wins","gwradio"]
type="post"
publish_at="25 Jan 22 06:00 IST"
featured_image="https://cdn.olai.in/jjude/manishk-edge-computing.jpg"
---

<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless src="https://share.transistor.fm/e/3dc1f3f4"></iframe>


### What is edge computing?

Think of a self-driving truck, which is driving on a highway at a hundred kilometers per hour speed. All the decision that the truck has to make are determined by software.

Now, when this truck is driving on its own determined by a computer system, think that a small child comes in front of the truck. The system on the truck has to make a real-time decision, whether to accelerate or break because it could be a child or it could be just a plastic bag floating in the air.

Because of the speed at which the truck is moving the decision, needs to be very, very fast in the normal situation. The sensors that the truck has will capture lot of data and then this data will go to a server. And this decision will be made on the server and the response will come back to the truck and based on it, the truck will put on the brakes or continue driving the way it is. 

Because of the critical nature of the application and the speed, the truck needs to make this decision in the minimum time. Since the server is situated far off in a data center, it might take a couple of seconds for the response to come back to the truck. What we want in this situation is to reduce the response time as minimum as possible.

What edge computing does is, it **reduces the distance between the point of data generation and the point where the decision takes place**.

What we will do is we will put a small server on each cell phone tower so that the truck can send the data from its sensor to the nearest cell phone tower. And then the server can compute and sending the response back that way. The response time is reduced to a couple of milliseconds from couple of hundred milliseconds.

![Edge computing podcast](https://cdn.olai.in/jjude/manishk-edge-computing.jpg)

### Benefits of edge computing for business and technologists

Over the last 20 years, internet has gone mainstream. Every business is an internet driven business. Whether you are a bank, whether you are a grocery store, whether you're running a hospital, whether you are a teacher, especially these days with COVID. 

Similarly edge computing is the gradual evolution of the internet paradigm. Every business will be touched by this technology because we are able to make decisions in real time. 

For example, if you are an architect and you are designing a large office building with people coming and going. With edge computing you're able to make near real-time decisions with all the sensors that are there to track the temperature, to decide whether there is a viral load or whether there is too much moisture or the sunlight is too high, then let's dim the internal lights.

All these decisions can be made real time. That is one application of any, even if you're working in finance, for example, somebody tries to scam somebody's money via an ATM machine in the current situation. The fraud will be detected a couple of minutes later because all the data has to hit the central server. And then there will be an alert and then they might block your card, or then maybe you have to call the bank and then run around to get your money back.

With edge computing the decisions can be made in near real time. There could be a real time alert, right at the moment when somebody tries to commit a fraud at the ATM. 

I really liked the story when electricity was getting mainstream. A hundred years ago, initially electricity was only used for lightning. The only electrical connection that you will have at your home is going to be for a light pole. And then what people did is they came up with a lot of adapters. They'll try to run a toaster or a washing machine. 

But now electricity is a utility and it's called a utility for a reason, because the only thing you need is the electric supply and you can connect anything to electricity.

That happened with the internet and with the new paradigm, with all these smart devices, all these sensors, so much of data being generated. This data needs to be processed in real time to give, a more human touch to technology. So **edge computing will also become a utility in the coming days**.

### Will cloud computing replace edge computing?

The cloud will still remain the way it is. Edge is only the next generation of cloud.

Within the edge computing paradigm, there are concepts like far edge, near edge or the central cloud. You will always need the cloud the way it is right now, because you also need to store data somewhere. And you need to backup the data. You also need a lot of reporting.

The cloud will always be there. Edge computing is a gradual extension of it. 

### Associated technologies shaping edge computing

The biggest driver for edge is **smart devices or the sensor technology**. Now everything is going to be smart. 

For example, the thermostat in your home. It is connected to the internet and it can automatically talk to your junction box from where the public utility sends in the electricity. You don't have to turn up the thermostat automatically on or off it can auto adjust and reduce the load. 

Another example, you have a municipal water treatment plant. So somebody does not have to manually go and take the reading, if the water is drinkable, is the water clean enough, is there enough chlorine or is there too much chlorine? And currently, because it's a human involved, they can do it a finite number of times. But if I put a good quality sensor there, then you can monitor it in real time. It will be more accurate. The reporting will be much more faster, and you can take corrective actions. 

Everything around you will generate a **lot of data and that data will be processed in real time** by edge computing and then coordinated with the central cloud by a packet.

When you get quality data and this data is measured accurately, it allows you to make much more informed decisions. So with smart devices, we have high-speed mobile internet. We already have 4g. With **5g** coming in, there's no limit to how much data can be captured. And the more data you capture, the more things you're able to measure, the more informed decision you can make. And that helps in improving the quality of human life.

### Government of India's play in edge computing

India is the second largest internet market in the world, and has one of the cheapest internet connectivity rates. So we were sitting at the top of a very big internet opportunity and there's a lot of value that can be created for our fellow citizens. 

Take for example, UPI, it's an amazing technology. It's really state of the art, the most advanced technology, I guess, anywhere that exists in the world, it's mind blowing. I can send money to you at the click of a button and it's real time. 

Imagine if the same thing happens to everything that is around you.

With sensors, you can monitor your traffic efficiently. You can have green corridors for ambulance. With smart sensors, smart drivers, smart cities. You don't need a traffic policemen to coordinate or somebody to call the police. The ambulance has already tagged; the ambulance is broadcasting. The ambulance driver can turn the siren on. So the system will detect the sound. This is this ambulance at this particular junction box. It will be a green corridor until it reaches the destination hospital.

The ambulance doesn't have stop anywhere. They don't have to have a green corridor for 40 minutes. They just need to turn that particular green light, which is nearest to the ambulance. So the rest of the traffic can continue functioning the way it is. So the people are also able to commute wherever they want, but the ambulance is not actually stopping. And all of this decision is being made in real time. 

In India, the government investing heavily in the smart cities program and a lot of money is being invested where you have such technologies, centralized monitoring systems, where sanitation quality can be monitored. Traffics can be monitored smartly.

Even there is a project for smart electricity, which will be efficient for the both the consumer and the company. So the consumer can always have a prepaid system and not have a surprise at the end of the month. Then the government can plan things better because the actual load is known. 

So all of this is in bites and it is being executed right now in India. So we're really at the forefront of this technology. The government is aware of the potential and there are a lot of programs that are being involved. 

Software technology parks of India has a dedicated center of excellence for IOT. In Mohali they have a national center of excellence.

Something that I'm really passionate about is quality access to healthcare. We have PGI, Chandigarh. Let's say, a person from a village has to travel to PGI. And it's really tough for a small consultation. The actual interaction with these specialized doctor might be for couple of minutes, but there's a lot of ad hoc time. It is not just the patient, but there are people with them. So lot of productivity is being lost. 

With edge computing, can we have dedicated video conferencing corridors with all primary health centers connected via private grid? So normally at times you see that the zoom is not working fine or Google meet is very slow. Or the WhatsApp video call is not very efficient because it's all the same network. But once we have this distributed edge networks, then you can have an HD video connection. It's all local, it's all between, let's say some village in Amritsar to PGI, Chandigarh. We can really reduce the consultation time. It's a direct interaction with these specialists and the specialists can cater to more people. We can have a smart ECG machine or a smart x-ray machine. You go to the nearest lab and the data is fed to the specialist via the dedicated system. And the system can automatically detect that your bone is broken or you have cataract in your eye and things like that. So all of this is being worked on now in this country.

Technology is an equal opportunity enabler. It raises the tide equally for everyone. And especially in the tier two tier three cities, I see a lot of innovation coming in the decades to come.

We are a young country. We are an aspirational country.

### Experiments in edge computing

We run an international coalition called, the open computing Alliance, where we are partnering with some of the companies who are building the platforms that power the edge computing solutions.

In India, we have a project called "The Bharat compute grid," where we are setting up these computing units at various locations within India. Of course not as diverse as cell phone towers, because it's more of a research perspective and we had a small startup. I have got two physical servers running here in the Mohali data center as part of the nearest node.

So we are running experiments on a low latency telemetry system. In addition to it, we're running experiments in agriculture. How can the soil nutrient quality can be monitored in real time? What happens is, when the fertilizer is required it spread liberally across all the track of land.  What we did is we got some small, micro 35 Watts server. We connected it to a solar panel. It works 24 x 7 automatically, no dependency on electricity grid. Especially for rural India. So we are not creating any load on the main power grid. So electricity can come and go, which I believe with time it will improve. But the system is self-sustaining. It has a small 4g connectivity module it's connected, to the high-speed internet. The data does not need to travel to the central server all the time. You can process it locally, make all the local decisions. And later on, you can sync all the data to the central server for reporting and for analysis.

We trying to create a customized version of subutai.io, specific to India for Indian use cases.

India's salvation lies in large scale wealth creation. Technology is the fastest way of doing it. I think it should be our life's mission. It is our responsibility towards our country. 

### Where to start with edge computing

You could be working in finance or you could be working in healthcare. Internet is a necessity. In addition to access to internet, I'm able to give you access to computing power. You can do all the processing right next to where you are without having to wait. For example, we all have those annoying moments where the network is down and it creates a lot of problems. 

I think it is imperative for us to prepare our minds for this new paradigm. 

How can you utilize computing power while designing a building? That's a paradigm change. You need to be aware of it. Or if you are a fashion designer, how can you design fabrics, wall curtains that detect dust. Or self healing bandages, or a smart bandage that reports the bacteria level or a bedsheet. If you're working in the finance sector, how can you benefit from real-time computing capabilities?

I compare it with is the iPhone moment. When the iPhone came out, there was no app store. There were no apps. But today the app store itself generates about $50 billion of revenue every year. The same thing is going to happen with the edge first world. When you have so many connected devices, these devices need smart applications, smart solutions.

If you are a programmer, you need to think what will be relevant? How can I have the expertise to build a distributed application that is going to hit us five years from now.

We need to prepare ourselves. So what is going to be mainstream five years from now? And the moment is right now, irrespective of which domain you are on. You don't have to be a programmer necessarily, but you can always think of solutions. Technology is only an enabler. There can always be somebody who can write the application for you or develop the software, but you need to have the industry expertise.

It's a new, new concept. It's difficult to grasp in the beginning. If I give you a really good computing power right next to your room or your wardrobe, what will you do with it? 

Even I do not know of all the possible use cases, but I think even if we are aware, it really empowers us. It will touch everything, just like every business is an electricity dependent business. Every business is an internet dependent business. So I believe every business is going to be an edge first business.

### Watch on YouTube

<iframe width="560" height="315" src="https://www.youtube.com/embed/Sn4eWv9dmU0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Connect with me
- Twitter: https://twitter.com/jjude
- LinkedIn: https://www.linkedin.com/in/jjude/
- Website: https://jjude.com/
- Newsletter: https://jjude.com/subscribe/
- Youtube: https://youtube.com/gravitaswins
- Email: podcast@jjude.com
- Executive Coaching Program: https://gravitaswins.com

## Your feedback counts

Thank you for listening. If you enjoy the podcast, would you please leave a short review on Apple podcast or on YouTube? It takes less than 60 seconds, and it really makes a difference in finding this podcast. And it boosts my spirits. 😀

I would like to hear what resonated the most with you in this episode. Either you can email me or share it on social media and tag me. Thank you for your support.

Have a life of WINS.

## Amazing tools I use:
- I record all the remote interview with [Riverside.fm](https://riverside.fm/?via=joseph-jude)
- I use [Transistor.fm](https://transistor.fm/?via=jjude) for hosting these podcast episodes

_Gravitas WINS Radio is produced by [Joseph Jude](https://jjude.com/). Be sure to subscribe on [Apple Podcast](https://podcasts.apple.com/in/podcast/gravitas-wins-radio/id1562802449), [Spotify](https://open.spotify.com/show/7LOBjAyjBkechMC0xMIj9E),[Google Podcast](https://podcasts.google.com/feed/aHR0cHM6Ly9mZWVkcy50cmFuc2lzdG9yLmZtL2dyYXZpdGFzLXdpbnMtcmFkaW8=),[YouTube](https://www.youtube.com/gravitaswins), or the podcasting app of your choice._

Please check out other episodes of [Gravitas WINS Radio](/gwr-themes/). For easy consumption, they are categorized by themes. If you are a fellow podcaster, you might be interested in [How I Podcast](/how-i-podcast/) or [tools I use](/i-use/).

## Continue Reading

* ['Building An Enterprise Data Strategy' with Ganesan Ramaswamy](/ganesanr/)
* [Impact of digital technologies in breaking barriers](/impact-of-digital/)
* [How to deliver value in the digital age?](/value-in-digital-age/)