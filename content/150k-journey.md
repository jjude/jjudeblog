---
title="Journey to 150k hits a year"
slug="150k-journey"
excerpt="Putting out words consistently earned me DOPE. May be it is time shift gears."
tags=["wins","action","visuals"]
type="post"
publish_at="29 Aug 22 15:15 IST"
featured_image="https://cdn.olai.in/jjude/jjude-2022-12-mnths-hits.jpg"
---

I'm documenting the journey to 150k page views by the end of the third year (2025). In this article, I explain my reasoning and how I plan to proceed. Here are some background details before we get to the main point.  
  
I started blogging to improve my thinking and writing skills. My thinking improved as a result of writing, and it showed everywhere. Putting out words consistently earned me dollars, opportunities, positions, and esteem. It's time to switch goals after a decade.

## Why now?

I didn't care about page views or subscribers when I wrote to improve thinking. In the last 12 months, I received about 20k page views. Even as late as 2018, I had only 75 subscribers. So far, these statistics have not bothered me.  
  
By combining disciplined thinking and practice, I have developed a deeper understanding of building wealth, insights, networks, and self-control. On this blog, I share my thoughts about them.

My friend [Rishabh Garg](https://www.rishabhgarg.me/) challenged me to popularize these ideas. An underlying driving factor that came up during our discussion was having a popular blog. 

## Current Status

In the last 12 months, I got 20.1k pageviews from 13.4k unique visitors. Top sources of traffic are (numbers indicate unique visitors):
- Direct visitors (7.1k)
- Google (2.9k)
- LinkedIn (967)
- Twitter (510)
- Newsletters (200)

![ahref 12 months hits](https://cdn.olai.in/jjude/jjude-2022-12-mnths-hits.jpg)

According to [Ahref](https://ahrefs.com/), the site has a good health score, but there is still much room for improvement.

![ahref 2022 report](https://cdn.olai.in/jjude/jjude-ahref-2022.jpg)


## Why 150k?

As I set goals, I [think of the future in terms of scenarios](/shape-the-future/) rather than as a single point in time.

![Scenario thinking for future planning](https://cdn.olai.in/jjude/future-sceanrios.png)

I get 20k page-views per year. If I continue with the current 10% growth year on year, in 3rd year, I will get about 25k page-views. 

A preposterous view of the future would be to double page views each year. By doubling the page views every year, I could reach 160k page views in the third year (20 →40 → 80  →160), which I am rounding off to 150k.

## Process

After deciding to take up the challenge, I read extensively. Although there are plenty of great resources available, I rely on the advice of a small number of experts.  

Besides reading [Ross Simmonds](https://twitter.com/TheCoolestCool/) and [Ryan Law](https://www.animalz.co/blog/author/ryan-law/) as individual experts, I also read [Convince & Convert](https://twitter.com/convince) as an agency.

These three posts gave me the direction to follow for the next three years:
- [25 Experts Reveal SEO Strategies You Need to Rank in 2022](https://www.convinceandconvert.com/digital-marketing/expert-tips-best-seo-strategies/)
- [How NerdWallet Uses SEO Topic Clusters to Drive Traffic Worth $84M](https://foundationinc.co/lab/nerdwallet-seo)
- [Content Repurposing](https://foundationinc.co/lab/hootsuite-content-repurposing)

I picked **three areas to focus** on based on all the readings.
- Topic Clusters
- On-page SEO
- Distribution

### 1. Topic clusters
I wrote to improve my writing and thinking, so I wrote on topics that interested me from time to time. There was no intentional unifying theme between posts. Over time, however, a theme of "gravitas" emerged.  
  
A leader's gravitas works similarly to gravitational force. Leaders with gravitas attract dollars, opportunities, positions, and esteem. What is the best way to build gravitas if it is such an essential force?

> We tend to focus on snapshots of isolated parts of the system, and wonder why our deepest problems never seem to get solved.
>  —Peter Senge

Usually, leadership development programs focus on improving individual aspects of leadership. As you apply [systems thinking](/tags/systems/) to leadership development, you'll see that gravitas consists of **wealth, insights, network, and self-control**. These WINS, helps you to win at work and succeed in life.

WINS will be the topic clusters towards the journey of 150k hits. 

_If this topic interests you, please [subscribe to my newsletter](/subscribe/)._

### 2. On page SEO

I never really bothered about SEO. To me, SEO meant stuffing an essay with keywords for Google to rank the essay higher and creating a click-bait title.

![SEO strategies for 2022](https://www.convinceandconvert.com/wp-content/uploads/2022/08/CC-Blog-Featured-Image-Templates-18.png)

These [24 tips from digital marketing experts](https://www.convinceandconvert.com/digital-marketing/expert-tips-best-seo-strategies/) convinced me that SEO is lot more than click-bait headline. If I implement all of them, I should be able to get good SEO juice.

Out of the 24, I am planning to start with these seven tips:

1. Use Original Quotes in Articles
2. Perform In-Depth On-Page SEO Audits
3. Implement Content Clusters
4. Don’t neglect internal linking
5. Know Your Audience’s Interests
6. Go Deep with SERP Analysis
7. Optimize the Page Speed

### 3. Distribution
While I used [Twitter for learning](/learn-via-twitter/) and LinkedIn to connect with like-minded individuals, I didn't use them to distribute my ideas. Since Ross regularly talks about distribution, I decided to distribute my posts on Twitter and LinkedIn, since that is where my potential audience hang out.

![Repurposing your content](https://foundationinc.co/wp-content/uploads/2022/03/6-hootsuite-repurposing-menu-for-hashtag-guide.png)

I experimented with Twitter threads and LinkedIn posts. The engagement on LinkedIn has been much higher than on Twitter so far. I need to figure out how to distribute on Twitter in a way to attract engagement.

## Tools

Tools are the employees of indie-makers. I have a whole bunch of "employees." They are essential to achieve such ambitious goal.

- Books, podcasts, and articles for learning the concepts 
- Reddit, Quora, and Google for research 
- [Obsidian](https://obsidian.md/) for R & D, writing temporary notes, as well as writing long form articles for newsletter and blog
- [Wordtune](https://www.wordtune.com/) - for helping me edit articles I write
- [Excalidraw](https://excalidraw.com/) for drawing low-fidelity illustrations
- [Sketch app](https://www.sketch.com/) for creating images for website and social media
- Self developed static blog engine for converting markdown posts into a blog
- Blog hosted on [Gitlab Pages](https://docs.gitlab.com/ee/user/project/pages/)
- [FeedHive](https://feedhive.io/) for scheduling posting to Twitter & LinkedIn 
- [Twitter](https://twitter.com/jjude/) & [LinkedIn](https://www.linkedin.com/in/jjude/) for the final distribution to intended audiences
- [Plausible](https://plausible.io/jjude.com?period=30d) to track analytics
- [Shields App](https://www.shieldapp.ai/) for LinkedIn analytics
- [Zoom](https://zoom.us/) to get on a call with others to share, debate, and discuss ideas
- [WhatsApp](https://www.whatsapp.com/) for water-cooler chats with friends for informal chats and quick feedback

## Inspiration
I draw inspiration from other individuals who have journeyed the same path earlier. These are just three.

- [Justin Jackson](https://justinjackson.ca/)
- [Derek Sivers](https://sive.rs/)
- [Farnam Street](https://fs.blog/)

## Fears
This journey is not without its fears. I am not so much afraid of the hard work, but I'm afraid that I will be swayed by SEO tactics that I will end up writing for machines rather than humans.

I will keep stop, evaluate, and  pursue to identify if I stray away. I think having some accountability partners will also help in staying the course.

## Keep in touch

I will share the progress on Twitter and here on this blog. If you are interested, please follow me on Twitter. If you have any tips or [smartcuts](/smartcuts/), don't hesitate to [tweet](https://twitter.com/jjude/) them to me.

## Continue Reading

* [First Bullets. Then Cannonballs](/bullets-cannonballs/)
* [Embrace The Unknown](/embrace-the-unknown/)
* [Rise With The Rising Tide](/rise-with-rising-tide/)