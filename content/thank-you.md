---
title="Thank You"
slug="thank-you"
excerpt="To those of you who helped me to become an independent software vendor."
tags=["startup"]
type="post"
publish_at="03 May 13 16:30 IST"
featured_image="https://cdn.olai.in/jjude/thank-you-gen.jpg"
---

Four years ago, I became an independent consultant by capitalising my expertise in designing processes and managing projects. As much as I enjoy process and project management, my heart is into programming. I realised developing an app will get me closer to programming and it will also be a nice addition to my technology portfolio. So last year I developed a Mac app, [BlogEasy](http://www.blogeasyapp.com), and this year I released an iOS version.

The whole journey has been thrilling, educative and satisfying — _thrilling_, because I get to wake up everyday and stay late every night, to work on what I love to do; _educative_, since I had to learn not just programming but marketing, selling, writing and every associate business concepts; this whole business cycle from product development to selling to receiving payment was visible to me and that is _satisfying_.

I want to say thank you to those who helped me have such a tremendous journey.

**[Apple](http://www.apple.com/)**: For a comprehensive business eco-system. I wouldn't have been able to build [BlogEasy](http://www.blogeasyapp.com), sell and collect proceeds, if not for the tight and comprehensive eco-system that [Apple](http://www.apple.com/) built. In the whole process, I deal only with [Apple](http://www.apple.com/); that makes life easy for an indie developer.

**[Wordpress](http://wordpress.org/)**: For an easy-to-install, blogging platform. If [Apple](http://www.apple.com/) follows a strict closed eco-system policy, [Wordpress](http://wordpress.org/) is just the opposite. They open-source everything. It takes lots of guts to develop business in an open-source model and Matt has shown that it is possible to do so. If you are an open-source enthusiast and a blogger, [Wordpress](http://wordpress.org/) should be your choice of blogging platform (and if you use iPhone or iPad, then [BlogEasy](http://www.blogeasyapp.com) should be your choice of blogging editor).

**[Prof. Paul Hegarty](https://itunes.apple.com/us/itunes-u/developing-apps-for-ios-sd/id395631522)**: For an amazing and a free iTunes course on iOS development.

**[Hacker News](http://news.ycombinator.com/)**: For the success stories, lessons from failures, and constant inspiration.

**Fellow developers**: For the many tutorials; for answering questions in [stackoverflow](http://stackoverflow.com/); for [cocoapod](http://cocoapods.org/); for Cocoa/iOS books; for their success stories. The quantum of help available is just amazing. You have to just ask.

**Customers and users**: For clicking the 'Buy' button (Mac Version) or 'Download' button. As I write, 76 people have bought the Mac version and about 100 have downloaded the iOS version. Their actions encourage me to code daily.

**Joel &amp; [Shankar](http://about.me/shankarganesh)**: To Joel, for the superb icon design &amp; Shankar for testing the first version. I designed and developed all parts of this venture — proof of concept, design of both Mac &amp; iOS app, website, marketing and so on. In all of these, I had some knowledge and could fill  whatever gaps existed. But logo design was different. I had absolutely no knowledge. Joel took that load off me. And Shankar took time off from his busy days, tested the app for free, and sent me a page full of suggestions. The last line of his email read,

> 'I think the fact that the app brilliantly uses native UI is a killer USP. That alone is very, very impressive! I think you can do a launch. Good luck :)'

I can't express the happiness I felt when I read that line.

**Wife & kids**: It is not a vacuous, passing thank-you. When you are an indie, there are moments when you want to share your excitement about cracking something difficult and then there are moments you just want a laugh because you are depressed and don't see a point in pursuing whatever you are doing. I had tens of such moments. Without the support that my wife and kids gave me, I wouldn't have released these apps at all. I am grateful to have such a support not far away from my working table.

The surprising fact is, except my wife, kids & Joel, I have not met anyone else, including Shankar. All of the connections happened via Internet. Isn't it amazing? Now, whom should I thank for the Internet?

