---
title="Proven Biblical Principles To Build Wealth"
slug="biblical-wealth"
excerpt="God is ready to give. But are we ready to receive it? We look everywhere else, but not at him."
tags=["wealth","faith"]
type="post"
publish_at="02 May 22 17:32 IST"
featured_image="https://cdn.olai.in/jjude/biblical-wealth-gen.jpg"
---

I used to earn ₹1500 a month in 1994. After 28 years, I am giving away more than 30 times that salary to charities, all while working only 3 days a week. My success is entirely due to following the tried and tested Biblical principles for building wealth.

You might say that Joseph, you studied engineering and this was bound to happen. Perhaps. I will give you another example.

I started teaching these same principles three years ago in a church full of daily-wage laborers and slum dwellers. They struggle to make ends meet. I preached there every second Sunday for three years. I shared my experiences with them and taught them the fundamentals. Gradually they started applying these principles. One of them recently purchased a property worth a couple of lakhs. It was a joyous occasion to lay the foundation stones for their home.

These principles work.

Most of the books of the Bible mention prosperity, some more than others. The first book of the Bible says:

> The name of the first is the Pishon; it winds through the entire land of Havilah, where there is gold. Genesis 2:11 

The last book of the Bible says:

> The great street of the city was of gold, as pure as transparent glass. - Rev 21:21

Deuteronomy is dense with blessings, not just financial blessings but blessings for family, relations, trade, and every aspect of life. It establishes who owns the wealth in the world:

> Indeed heaven and the highest heavens belong to the Lord your God, also the earth with all that is in it. -  Deuteronomy 10:14

Besides talking about prosperity, the god of the Bible is also eager to bless us with it. St. Paul says in his letter to Romans,

> He who did not spare his own Son, but gave him up for us all—how will he not also, along with him, graciously give us all things? - Romans 8:32

God is ready to give. But are we ready to receive it? We look everywhere else, but not at him.

The popular American comedian Kevin Hart shared [this story](https://www.youtube.com/watch?v=pSN8k8mRJKU) from his life with Oprah

When Kevin Hart decided to pursue stand-up comedy, his mother said, "I'm not a dream-killer" and gave him a year to prove that it really was his dream. Months later, Kevin asked her for rent money. She responded:

"Are you reading your Bible?"
"Ma, I don't have time to read the Bible. Give me money."
"Read your Bible and we will talk."

A few more months went by and Kevin was about to be evicted. He calls his mom again and she repeats the same thing.

Out of frustration, Kevin opens his Bible and six cheques fall out. 

His mother kept all the money he needed in the Bible. Kevin struggled for so long because he didn't listen to his mother and read the scriptures.

Most of us are like Kevin. We struggle because we don't open the Bible and read it. The book contains everything we need.

That is why John prays,

> Beloved, I pray that you may prosper in all things and be in health, just as your soul prospers. - III John 1:2

First, your soul prospers, then you prosper.

With that foundation laid, here are the five principles of building wealth from the Bible:

- Budget
- Save
- Invest
- Give
- Pay your debt

### Budget
> Suppose one of you wants to build a tower. Won’t you first sit down and estimate the cost to see if you have enough money to complete it? - Luke 14:28 

The first step to becoming wealthy is budgeting.

Keep track of what you earn and what you spend. You should know how much you spend on rent, groceries, kids' education, clothes, entertainment, and so on. Keep track of the money you save as well.

As the prophet Haggai says,

> You earn wages, only to put them in a purse with holes in it. - Haggai 1:6 

That's how most of our lives go. We put our hard-earned money in a leaky bucket without knowing it. You will discover the holes when you track your expenses.

If you know your current expenses, then you can estimate how much you would need for future events, such as college admission for children, kids' marriage, retirement, etc. Only when you estimate will you be able to begin saving.

You will become a laughing stock if you don't plan and estimate.

### Save
King Solomon urges us to learn from ants. 

> it stores its provisions in summer and gathers its food at harvest. - Prov 6:8

Life is filled with unexpected situations. A sudden illness, unexpected travel, or job change can require you to spend more in a month than usual. When we cannot afford it, we borrow and get into deep debt.

We need to save even if there isn't an emergency situation because there will be days when we will not be able to earn a living. In retirement, we will still need money, but won't be able to work. Ants gather when they can so they will have enough when they cannot find enough.

Joseph demonstrated this when he saved Egypt from the worst famine as recorded in the book of Genesis.

> This food should be held in reserve for the country, to be used during the seven years of famine that will come upon Egypt, so that the country may not be ruined by the famine.” - Genesis 41:36

He built the biggest barns he could build and stored as much as he could during the abundance. Egypt had enough food when famine struck. It's the same for our houses.

King Solomon said this, didn't he?

> The wise store up choice food and olive oil - Proverbs 21:20

It all began small, very small. At a young age. As a young boy, I saved whatever my father gave me as a weekly allowance and deposited it into a post office small savings account.

As soon as I began earning, I began saving ₹100. It was all I could afford. From there, I increased to 10% of my income. I now have six months of expenses in an emergency fund. I don't touch it much.

After you have saved enough, you can take advantage of new opportunities that come your way. This comes next.

### Invest
> Invest in seven ventures, yes, in eight; you do not know what disaster may come upon the land.” - Ecclesiastes 11:2

How do you use the money you saved? Invest it so it can grow. King Solomon tells us to diversify, but not too much. You should invest in seven or eight ventures so that you do not lose your entire portfolio in a disaster.

Savings accounts don't earn much interest. If you want to be absolutely sure of the return on your investment, you can invest in fixed deposits, recurring deposits, or government bonds.

You can invest in good-quality mutual funds and stocks if you are a little ambitious.

If you cannot spend time tracking and analyzing stocks but still want a higher return than bonds, invest in an index fund. Even at a conservative estimate, it will grow your money by 12% over time.

You can also invest in small businesses around you or start your own small businesses if you're not interested in stock market investing.

The Bible mentions trading frequently. Women are encouraged to become entrepreneurs by the Bible. The book of proverbs says:

> She sees that her trading is profitable, and her lamp does not go out at night - Prov 31:18

Not only that, one of the earliest believers of Paul's message was a businesswoman.

> One of those listening was a woman from the city of Thyatira named Lydia, a dealer in purple cloth. She was a worshiper of God. The Lord opened her heart to respond to Paul’s message. - Acts 16:14

Investing in businesses is the only way to multiply your money. Otherwise, inflation will erode your savings every year. Do not invest all of your money in one type of business, but rather in seven or eight.

### Give

The dirty side of growing money is that we become attached to it. This can lead to problems. Lots of problems.

We become greedy and spend all our time building the business that we forget to spend time with family and friends, ignore our health, and even engage in unethical business practices.

No wonder, St. Paul warns us of such greed:

> For the love of money is the root of all kinds of evil. Some people, eager for money, have wandered from the faith and pierced themselves with many griefs. - 1 Timothy 6:10

Is there an antidote to the love of money that gets us into many griefs?

Giving.

Give generously. Donate to charities. Donate to worthy causes. Donate to orphanages and old-age homes.

Tithing is the biblical practice of giving 10% of your income. In both the Old and New Testaments, the Bible encourages giving.

Throughout the Bible, it is repeatedly said not to test the Lord. However, in this case, there is a direct challenge to test the Lord. 

> Bring the whole tithe into the storehouse, that there may be food in my house. Test me in this,” says the Lord Almighty, “and see if I will not throw open the floodgates of heaven and pour out so much blessing that there will not be room enough to store it. - Malachi 3:10

Even Jesus said:

> Give, and it will be given to you. A good measure, pressed down, shaken together, and running over, will be poured into your lap. For with the measure you use, it will be measured to you.” - Luke 6:38

Since the first day, I got my salary, I have tithed. Through my church, I support charities. I am happy to say that I received a good measure of blessings in return.

The financial return on investment is what we think of when we think of return on investment. What about health, joy at home, and esteem at work? These are all benefits I have enjoyed. Not spending on illness alone is a great benefit.

My dream is to one-day share five-tenths of my wealth with others as William Colgate, the founder of Colgate, [did](https://en.wikipedia.org/wiki/William_Colgate#Personal_life).

### Pay your debt

I have been paranoid about debt because the Bible warns us: 

> the borrower is a slave to the lender. - Prov 22:7

I took out loans only to purchase a home and a car. Even then, my EMIs were less than 30% of my salary at that time.

Everything else, I bought through planning, budgeting, and saving. When I plan, I am able to have all that I need, but not all that I want.

I have learned that our wants are driven by mimetic desire. It is natural to covet what we see in others. The ten commandments warn us against covetousness again:

> You shall not covet your neighbor’s house. You shall not covet your neighbor’s wife, or his male or female servant, his ox or donkey, or anything that belongs to your neighbor. - Exodus 20:17

As I learned to live within my income and plan for bigger expenses, I learned I didn't have to take out huge debts.

In addition, I realized I am blessed because I did not have an educational loan hanging over my head when I graduated and I did not have any illnesses, and my parents have enough retirement income that I do not have to support them financially. Others are not so fortunate.

### Becoming a blessing

> For the Lord your God will bless you as he has promised, and you will lend to many nations but will borrow from none. You will rule over many nations but none will rule over you. - Deuteronomy 15:6

We are blessed so we can bless others. The more we use our talents and possessions for the betterment of others, the more blessed we become. In a sense, it's like living a dream.

Give it a try. It's fascinating and addictive. The more you follow these principles, the more blessed you will be. So you want to follow them even more.

These factors, saving, investing, and budgeting, are all vast in nature. However, these broad categories should get you started on your journey to building and enjoying wealth.

## Continue Reading

* [What I Learned About Leadership From The Book 'Lead Like Jesus'](/lead-like-jesus/)
* [Build On The Rock For The Storms Are Surely Coming](/build-on-rock/)
* [The Dichotomy Of Contentment And Ambition](/contentment-and-ambition/)
