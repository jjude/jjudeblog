---
title="Book Notes - How I almost blew it"
slug="i-blew-it-book"
excerpt="Journeys of Indian startup founders, who had a near-death experience, almost shut-down but survived to succeed"
tags=["books","startup","luck"]
type= "post"
publish_at= "15 Sep 19 06:30 IST"
featured_image="https://cdn.olai.in/jjude/i-almost-blew-it.jpg"

---

Most books on startups focus on American entrepreneurs. Indian entrepreneurs have to content themselves with derived knowledge from these American books. Sidharth Rao, an investor and a co-founder, is bridging that gap with his new book, "How I almost blew it." He is narrating the journeys of **seventeen Indian startup entrepreneurs**, who had a near-death experience, almost shut-down but survived to succeed.

Having been in the advertising industry, Siddarth knows a thing or two about the psychology of readers. Instead of arranging the book topically, he dedicates one chapter to each entrepreneur. That way, readers can choose the reading path. Since **every chapter is organized in a similar pattern** — how they started, how they blew up, and how they recovered, the book is easier to read, and the lessons easier to grasp.

There are **certain factors repeated** by these entrepreneurs. If new startup founders understand these factors, it will help them to avoid the mistakes that these entrepreneurs made. These repeated factors are:

- Don't diversify without a core product that can sustain you
- Luck + skill matters
- Patience and perseverance pay. Pivot if needed
- It takes 20 years to be an overnight success
- India looks much bigger than it is for businesses

The impression created in the newspapers often is that the policies of government hinder and create havoc for businesses. I was surprised that none of the entrepreneurs mentioned government policies as a cause of their fiascos. Is it because these are all Internet companies? I don't know, but I would like to think so.

I've listed some of the interesting points from the book. **Take this as a starting point, but you should read the book**.

![How I almost blew it](https://cdn.olai.in/jjude/i-almost-blew-it.jpg)

_Buy it from [Amazon](https://geni.us/how-i-blew-it)._

**SANJEEV BIKHCHANDANI (Info Edge India - Naukri.com)**

- An entrepreneur's capacity for risk-taking can paradoxically lead to success.
- Most of us have had chance encounters. Those who become successful in their chosen business are the ones who manage to keep an eye out for opportunities, believe in them when they show up and seize the moment. They intuitively understand where their control ends, and the power of the universe takes over.
- When JobsAhead.com was launched, it created quite a buzz. It had better user experience and was hippier than Naukri
- Housing.com was a better-looking product. It was art for art's sake.
- Differing insight: housing.com started with a map. His product started with the text-based search page. When you want to buy a house, you are not arriving from Mars. You are driving by that place a hundred times a year, thinking to yourself, "This colony looks reasonable. Maybe I should look for a house here." You are familiar with that place. [*differentiating insights matter*]
- Better product is what the customer wants rather than what appeals to your aesthetic sensibilities
- Customer's money is a hundred times better than investor funding.
- The core of any business is the value of its proposition and its ability to sell itself to its targeted market. Does it have what it takes for a customer to loosen his purse strings?
- Nothing is real apart from profit. valuation based on any other metric is unreal.

**KUNAL SHAH (FreeCharge)**

- pg 24 has many insights on founding CEOs vs. professional CEOs with examples from both
- Professional CEOs do within a managerial framework (unlike founding CEOs go with intuition)
- Startups never have a linear growth curve, their progress is mostly bumpy.
- Horrible decisions happen when you are hungry, horny, or out of the runway.
- If I could do it all over again, I would do this with a lot more confidence and aggression instead of doubting my every move because I saw the same move being made by others with much more confidence.

**MURUGAVEL JANAKIRAMAN (Bharat Matrimony)**

- We got into too many verticals instead of focussing on the core verticals, which led to expenditures that could have been avoided. On the other hand, if it weren't for all these verticals, I wouldn't have raised any money because matrimony had always been profitable
- You have to have one solid profitable business. It's a bad idea to diversify without having one.

**AJIT BALAKRISHNAN (Rediff.com)**

- code + media = wealth concept in practice
- Indian consumer markets are minuscule, particularly for newer technology. He adds, "I think there are only about 3 to 5 % of Indians who adopt technology early, who are aware and like to follow the rest of the world. It almost takes India 15 - 20 years to fully embrace new technology. I have seen it multiple times."
- Rediff.com participated in every new internet opportunity but failed to make a mark in any of them.
- Startups are a great thing for our country. Even at a policy level, the government needs to find more and more ways to encourage people to launch startups. Corporate employment is nowhere as luxurious as it used to be. The era of large companies with 10,000 employees is going to come to an end. In India, historically, the corporate and organized sectors never accounted for more than 12-13 % of all employment. Nearly 87% of Indians have had to fend for themselves with a vast majority engaged in small business and farming. With the idea of re-skill, startups may be the only way that India can continue to grow.
- Look beyond our glamorous world at youngsters who are in their twenties, studying at a polytechnic, for example. Many of them may want to start a computer repair shop and will need capital. But the banks shoo them away.

**ANUPAM MITTAL (People Group)**

- Indian students weren't studying to become qualified but to turn into desirable matrimonial matches.
- We bought Ferraris and Porsches but soon lost the shirt off our backs.
- Our experience often shape our realities
- In case of Ola, Anupam recites his experience of conducting due diligence: he would call ola call center (when they didn't have an app). The phone was answered by Bhavish himself. Anupam would show frustration or anger. The way Bhavesh handled every situation impressed him.
- Demand creation is the need of the hour

**ASHISH HEMRAJANI (BookMyShow)**

- Bookmyshow initial days - they used founder (Ashish) phone to receive calls; since those facilities didn't exist at that time
- Startup founders need to get their hands dirty. That is the only way to build a sound and successful organization. They cant be problem solvers or fine-tune the system unless they become aware of problems hitting the employees on the ground.
- Inexperienced founders can't even learn to delegate work until they have worked on all the stages.
- Founders have to be willing to step into the thick of the situations, pick up the phones and delivery cartons whenever needed which demonstrates the strength of the leadership and the desire to succeed. It sends the right message to the employees
- Certain founders confuse delegation with abdicating their responsibility
- Being an entrepreneur is like being a mongrel that wakes up every morning and says to itself, "where is the scrappy fight today?" When you are out in the street, you fight, you bite, growl, and snarl and quickly consume the food before someone else shows up to try and take a bite off your plate.
- There is no "one" India as a market. There are at least three India - Indian one composed of the top 15 % of Indias (150 - 180 million) earning an average of 30k per month. They are the ones who have money left over after buying necessities. India two is the middle 30% (400 million approx) earning an average of 7k per month. This would be a country about three times the size of Bangladesh with a similar level of purchasing power. India three are the forgotten 650 million who subsist and don't have the money to buy even two square meals. Their income rival that of sub-Saharan Africa. Failure to distinguish between internet users (India two) and internet consumers (India one) has messed up every estimate of the market.
- Many are interested in buying growth and not focussing on profitability
- Investors have to recognize that India is not the US, and certainly not China. The USA has 300 million people, all with some equitable income. They can all afford a smartphone. No one is struggling for food, clothes, or a roof over their head. Most of our population, on the other hand, is struggling to get by on a daily basis. What are they going to do with 40% discount on a pair of jeans?
- Content first kicked-in and now, it should naturally progress to commerce. But for that, India two and India three audiences need to be making more money. It is not happening at the pace at which access to the internet is growing. Money is still not flowing down fast enough. That kind of growth has happened in China, but it hasn't yet happened in India.
- China vs. India is a big insight (pg 104) - China pulled out 730 million out of poverty; India was at 170 million. China is a booming 11.30 trillion dollar economy; the other is not more than a 2.1\$ trillion upstart. The opportunities can't be compared.
- Indians love to give advice. Everyone in this country has pearls of wisdom to impart on everything from how to raise your baby to how to run your company.

**BRIJESH AGARWAL (IndiaMART)**

- First mover advantage is mostly a myth engraved in a startup playbook. In reality, nearly, half of the companies that go first fail. The ones who succeed are the companies that entered the market early but not first. They learn from the proverbial arrows in the pioneer's back and ride the wave of market excitement to success. Companies defined as first followers have a mere 8% failure rate.
- Patience and perseverance pay. Pivot if required.
- By instituting a board of directors, you can select participants who have unique experiences in hiring strategies, capitalization, resource allocation, etc. They can be singularly dedicated to guiding you in developing and executing your long-term strategic goals.
- It takes 20 years to be an overnight success

**JITENDRA GUPTA (Citrus Pay)**

- The company hired Talent Pickup, an IT services company based in Pune. That company hired staff dedicated for our tasks so we could directly allot work and manage them ourselves. The best part was that I didn't have to handle the replacement or recruitment. Investors were concerned. I asked them how it matters whether they were on-rolls or not. It was still my team and worked closely with me.
- ICICI gateway was down for 6 hours. My sales team was flooded with calls from Jet, PVR, and Inox. We had pitched these businesses in the past, but it took adversity for them to recognize what we're bringing to the table.
- Citrus's big catch was Airtel. It took Jitendra and his team over a year to convince them to sign on. Airtel was their first mega client, with 50k - 60k transactions every day. Everything in the company changed the day we signed on Airtel. Signing a deal with such a large customer can generate meaningful revenue.
- A successful B2B company is built around technical merit, reasoned product features, large enterprise clients and at the core, a massive sales culture. A successful B2C firm is built around user segments, understanding emotional and rational needs, brand building, user acquisition, and the management of multiple distribution networks.
- Founder conflicts arise when on one-two things happen -- either the founders want different things for the company, or they want the same thing for themselves. Either way, it is impossible for both parties to get what they want. - Sam Altman of YC

**DEEPINDER GOYAL (Zomato)**

- Started with scanning menus from restaurants, scratching their own itches
- Businesses don't die because of profit and losses; they mostly die because of cashflow issues
- You see billion and a half people in India and think that the country's market is large. What needs to be taken into consideration is the number of people who connect to the internet and use such online platforms. For Zomato, Dubai and India were equal in size.
- Most VCs have this illusion that money can solve everything. There is much more to it than the money that goes behind building companies. Hot money hurts healthy companies.
- There is no problem in the food-tech space in general. There is a problem with startups that are visualizing wrong business models.
- They knew when to pick their battles and engaged in them only after careful evaluation of markets from time to time. They would have none of the foolish chivalry or swashbuckling. They were not afraid to turn their backs on a fight that they knew they could not win.

**SATYAN GAJWANI (Times Internet)**

- Two major changes in the company brought by Satyan:
  - we changed from a media company that used technology into a "product and technology" company that uses media
  - when we see things thriving or moving well, we double down or triple down on them instead of pushing them to break-even
- Businesses that focus on a single product have a better chance at succeeding.
- Sub-brands work in immature markets. As a market matures, it's easier to build a brand association with a standalone product
- Brand building lessons (pg 202) - Times brand stands for trust, integrity, confidence, maturity, and security; gaana.com is a music platform; it is for the young. It should feel light, energetic, and uplifting. It promotes and emotes a different message. Were we to build a bank, a Times Bank would probably work because it conveys a trust of our brand, but when we are building a music platform, Times isn't the ideal brand for it.
- To see a crisis as an opportunity by taking a long-term view of the market is what separates the grain from the chaff
- Allowing the gaps to widen to the extent that taking the leap to the other end becomes an impossible task
- When entrepreneurs get too much credibility too soon, they often end up burning their bridges like Rahul Yadav
- As a big and experienced player, it turned into a facilitator for budding startups instead of creating more verticals to compete with them.

**SAHIL BARUA (Delhivery)**

- Paul Graham talks about a phenomenon called "Schlep blindness." According to him, there is no dearth of great startup ideas. They lie unnoticed right under noses. One reason that we refuse to see them is that they are tedious and involve unpleasant tasks.
- A lot of startup ideas generated in India are usually knockoffs of something original in the US. Sahil's inspiration was as desi as it gets
- No new venture is complete without its own set of idiosyncrasies.
- Things can get messy without warning; At times, it is difficult for founders to see such trouble snowballing towards them.
- There is a challenge of choosing the right advice that fits the context of a business and weeding out the ones that don't
- India is a deceptive market if you do your math. It looks much larger than it actually is. You run out of the addressable market reasonably fast.
- Language barrier is the reason why so many people are willing to be on Facebook but don’t want to make purchases online.
- Good businesses, given the right capital and valuations at the right time, will be successful in five to six years

**GIRISH MATHRUBOOTHAM (Freshworks)**

- SaaS provider market could be divided into two: Horizontal SaaS providers and Vertical SaaS providers. Horizontal SaaS focuses on departments within the organization (such as HR, admin, and payroll) and spread across industries. Vertical SaaS focuses on a particular industry rather than individual departments within organizations.
- It is important to identify a booming market where you can ride the wave and then build something that everybody wants rather than attempting to create a new need.
- Girish's storytelling, combined with the market opportunity to disrupt an established incumbent like Zendesk, impressed the early investors.
- This is what companies today are looking for: to figure out where they stand in their relationship with the customer. It is what helps them make strategic decisions.
- Freshworks is now a customer engagement platform
- You need to innovate on a product for which there is existing and pressing demand.
- Favorite analogy: when a customer walks into a store wanting to buy a washing machine. They aren't at a state where they can define the solution by saying, "I need to figure out a way to remove dirt from my shirt." They are likely to say, "I want an IFB Senorita, 6.5 kg, front-loading washing machine" After they have started this, the salesperson has a chance to either sell the client whichever model they are looking for or convince them to buy a different model, by saying, "Why don't you consider the new LG model, which is much quieter" The same analogy applies to business software.

_I got this book from Tribune Chandigarh to write a review. You can read my review at the [Sunday Special](https://www.tribuneindia.com/news/archive/book-reviews/stuttering-at-the-starting-block-832713) page._ 

_Buy the book from [Amazon](https://geni.us/how-i-blew-it)._

## Also Read

- [Entrepreneurship is a metamorphosis, not a baptism](/morph-as-entrepreneur/)
- [Five Insights From Successful Indian Entrepreneurs](/five-insights-from-successful-indian-entrepreneurs/)
- [Startups And Security](/startups-and-security/)
- [Guest lecture on entrepreneurship at Christ College, Bangalore](/entrepreneurship-talk-at-christ-college-bangalore/)
- [Notes From Session With Mahavir Sharma, Serial Entrepreneur & Trustee, TiE, Global](/mahavir-at-tie-chd/)
