---
title="'Building enterprise applications with Low code tools' with Jinen Dedhia, co-founder of DronaHQ"
slug="jinen-dronahq"
excerpt="Spectrum of LCNC tools for enterprises, how to implement LCNC in enterprises, and more..."
tags=["gwradio","tech"]
type="post"
publish_at="30 May 23 17:55 IST"
featured_image="https://cdn.olai.in/jjude/jinen-yt.jpg"
---
Low code and no code tools are finding their place in enterprises. In this episode, I talk to Jinen Dedhia, co-founder of DronaHQ about how CIOs and CTOs should go about using LCNC tools in their enterprises, success stories of such usage, and much more.

<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless src="https://share.transistor.fm/e/3133db93"></iframe>

## What you'll listen

- Starting DronaHQ
- Where DronaHQ fits in the spectrum of LC tools?
- Success stories
- Why enterprises choose LCNC tools?
- Factors to consider to choose LCNC tool
- Compliances for enterprise applications
- Self-hosted vs Cloud-hosted
- Pricing model of DronaHQ
- Mistakes to avoid
- LCNC tools align to growth of company
- Impact of LCNC on Indian IT industry
- ChatGPT and DronaHQ
- Future of DronaHQ
- Kindest thing anyone has done for Jinen
- Best leadership quality
- Definition of living a good life

## Your feedback counts

Thank you for listening. If you enjoy the podcast, would you please leave a short review on Apple podcast or on YouTube? It takes less than 60 seconds, and it really makes a difference in finding this podcast. And it boosts my spirits. 😀

I would like to hear what resonated the most with you in this episode. Either you can email me or share it on social media and tag me. Thank you for your support.

Have a life of WINS.

_Gravitas WINS Radio is produced by [Joseph Jude](https://jjude.com/). Be sure to subscribe on [Apple Podcast](https://podcasts.apple.com/in/podcast/gravitas-wins-radio/id1562802449), [Spotify](https://open.spotify.com/show/7LOBjAyjBkechMC0xMIj9E),[Google Podcast](https://podcasts.google.com/feed/aHR0cHM6Ly9mZWVkcy50cmFuc2lzdG9yLmZtL2dyYXZpdGFzLXdpbnMtcmFkaW8=),[YouTube](https://www.youtube.com/gravitaswins), or the podcasting app of your choice._

Please check out other episodes of [Gravitas WINS Radio](/gwr-themes/). For easy consumption, they are categorized by themes. If you are a fellow podcaster, you might be interested in [How I Podcast](/how-i-podcast/) or [tools I use](/i-use/).

## Connect with Jinen
- LinkedIn: https://www.linkedin.com/in/jinendedhia/

## Connect with me
- Twitter: https://twitter.com/jjude
- LinkedIn: https://www.linkedin.com/in/jjude/
- Website: https://jjude.com/
- Newsletter: https://jjude.com/subscribe/
- Youtube: https://youtube.com/gravitaswins
- Email: podcast@jjude.com
- Executive Coaching Program: https://gravitaswins.com

## Continue Reading

* [Lee Launches on 'Application Development With No Code And Low Code Tools'](/lee-lcnc/)
* [Liji Thomas on 'Conversational Bots'](/lijit/)
* [Jyothi & Aravinda on 'Why Robotic Process Automation Matters To Enterprise'](/jyothi-rpa/)