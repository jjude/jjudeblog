---
title="Will Social Media Knock Out Newspapers?"
slug="will-social-media-knock-out-newspapers"
excerpt="Reach your economic buyers wherever they are."
tags=["networking"]
type="post"
publish_at="04 Jul 10 16:30 IST"
featured_image="https://cdn.olai.in/jjude/will-social-media-knock-out-newspapers-gen.jpg"
---

Solo-practitioners have to use their resources efficiently. One of the essential activity for business growth is to build market gravity. Social media evangelists like [Seth Godin](http://www.sethgodin.com/sg/) declare that [Newspapers are dead](https://seths.blog/2009/01/when-newspapers/). And there is a tribe building upon that argument. However the paramount question is ‘where are the economic buyers?’

Paul Williams founder of Idea Sandbox wrote [Why Newspaper Ads Beat Social Media](https://idea-sandbox.com/blog/why-newspaper-ads-beat-social-media/). His arguments are:
1) Its better to rely on established newspaper with established reach
2) Social media is fleeting; print has stability and credibility
3) Newspapers provide greater exposure via their established reach
4) One can pitch in Newspaper; in social media it is spam
5) Prospects are reached NOW in Newspapers; in social media one have to wait until the buzz is built

(Paul confuses between social media and digital - when WSJ in Kindle, it is digital but it is not social media)

Yes there are testimonials of social media success. But they are not sufficient enough and convincing enough considering the opportunity costs. If economic buyers are not in Twitter or Facebook or one of their cousins, it is unwise to spend resources there.

**Where are your economic buyers and are you reaching them there?**

