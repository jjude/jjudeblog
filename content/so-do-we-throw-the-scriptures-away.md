---
title="So do we throw the scriptures away?"
slug="so-do-we-throw-the-scriptures-away"
excerpt="Don't interpret scriptures literally. But do read them to enhance the quality of your life."
tags=[""]
type="post"
publish_at="05 Oct 08 16:30 IST"
featured_image="https://cdn.olai.in/jjude/so-do-we-throw-the-scriptures-away-gen.jpg"
---

It's easy to think of life in black & white. But in reality, it is never the case. Lots of factors throw in various shades of gray in between.

So is the case with scriptures. You can't classify them as either bad or good. Its true that (relatively) few insane bigots (and institutions) have taken the scriptures into their hands and created a whole lot of bloodshed and madness. But that doesn't mean you need to throw the scriptures completely.

Ancient wise-men realized that unless life-governing principles were coated with &#8216;God', the message won't get through. So they used the then prevailing religious medium.

Somehow over the years, we've trained ourselves to take the coating seriously leaving what is inside. That is just too sad.

Though the scriptures were written for an earlier generation, there are lots of gems still left in there. We should learn to distinguish the chaff from the useful. Having learned to identify the gem, put that gem in to good use.

I've personally benefited from such a process - distinguishing a gem, and then applying it in real life. Lots of stories from both the Christian and Hindu scriptures have helped me enrich my life.

Though I'm vehemently against interpreting the scriptures literally and creating chaos in the world, I do encourage reading them to enhance the quality of your life.

In the coming days, I'll share some of these stories and how they helped me. I'm not trying to convince you or to convert you into any principle. I'm sharing what worked for me. If it works for you, then well and good.

Or else, well, don't go and burn my village. Just ignore it.

