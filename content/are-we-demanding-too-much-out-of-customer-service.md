---
title="Are we demanding too much out of customer service?"
slug="are-we-demanding-too-much-out-of-customer-service"
excerpt="Before you complain next time about a bad customer service, think again. Were you an annoying customer?"
tags=[""]
type="post"
publish_at="29 Mar 09 16:30 IST"
featured_image="https://cdn.olai.in/jjude/are-we-demanding-too-much-out-of-customer-service-gen.jpg"
---

In a recent article in a leading online magazine, the author recounts his 'bad' experience in a five-star hotel and then compares that with a supposedly 'good' customer service at another equally starred hotel. He says that the 'bad' hotel didn't extend his check-out time (they said that if he pays extra it would be possible); but the other hotel gladly did so.

I have my own experiences of bad customer service. But we need to differentiate between bad customer service and annoying customer demands.

[](http://travel.webshots.com/photo/1200433034060339360XntoCp)

I remember a scene from my favorite romantic comedy - [Notting Hill](https://en.wikipedia.org/wiki/Notting_Hill_(film)). In that movie, William Thacker - the protagonist - runs a travel book shop. One day a well dressed customer walks into the shop and the conversation goes something like this:

Customer :  Do you have any books by Dickens?
William :     No, we're a travel bookshop.  We only sell travel books.
Customer : Oh right.  How about that new John Grisham thriller?
William : No, that's a novel too.
Customer : Oh right.  Have you got a copy of 'Winnie the Pooh'?

What is expected of William Thacker? Should he do something to attract this customer again? Should he send his assistant to get the latest John Grisham thriller and hand it over to the customer as a show of exemplary customer service? Isn't that what we – customers - demand so often?

Before you complain next time about a bad customer service, think again. Were you an annoying customer?

_Image courtesy: [Webshots](http://www.webshots.com/ "webshot")_

