---
title="Technology is Powerful; But Its Not The Panacea"
slug="technology-is-powerful-but-its-not-the-panacea"
excerpt="Interview with Ranjan Varma, who is tapping the internet to create financial products for common man to use."
tags=["coach","interview"]
type="post"
publish_at="30 Jul 10 16:30 IST"
featured_image="https://cdn.olai.in/jjude/rv150-197.jpg"
---
The Internet has provided all of us a common platform for communication without any discrimination for age, gender and skills. Each netizen can utilize its potential according their own needs. [Ranjan Varma](http://ranjanvarma.com/), working in an Indian PSU, is tapping the Internet to spread Financial Literacy among Indians. These days every one is blogging. But Ranjan is creating online financial products by taking advantage of the applications on the cloud - [Zoho](http://www.zoho.com/) and [Google Docs](http://docs.google.com/) to name two.

I'm delighted to interview him for this blog post.

### Q: What is Personal Finance Literacy and why should anyone care?

![Ranjan Varma](https://cdn.olai.in/jjude/rv150-197.jpg)

Personal Finance Literacy is highly overrated. It’s actually just common sense. But there are reasons why people avoid personal finance. One, financial services providers use a lot of jargon to obfuscate and confuse. Two, the sellers know more than the buyers (especially the commission involved). Three, because of our own emotions like fear and greed that leads to irrational behaviors and bad financial decisions.

But why should we care? I have been working with a PSU for 20 long years and had I been saving regularly right from 1990 when I joined work and investing them properly, I would be having a networth of approximately Rs 23-24 lacs The assumptions are very simple. I invest 25% of my income every year and get a return of 10% annually. The investments are made every year and not every month. Every month investment will lead to a much higher figure!

But, the actual networth (excluding my House/Ancestral Property) is not even half of that. That means that my less than ideal money management has resulted in a loss of over Rs 10 lacs. Yes, that’s a million rupee mistake!

### Q: Lot of us, the IT folks, don't understand numbers. Are we at a disadvantage in understanding the basic concepts of personal financial literacy?

Infact IT folks would be more comfortable with numbers. But they are put off by the jargons, the information asymmetry and maybe their own fear/greed. So I guess, the problem is not about understanding the concepts, but it’s about applying those concepts to your own unique situation.

We often mistake personal finance literacy as doling out knowledge. But as long as you don’t include the skills and the attitude towards managing your money, the education will not be complete. At this point, it may be worthwhile to point that knowledge, skills and attitude are three important components of any competency.

So, IT folks aren’t at any disadvantage among others. Infact, my target audience would be the IT folks once I get started full time!

### Q: Please tell us about the financial products you've created.

I built RupeeManager for my own use. I was uncomfortable using an online tracker who can mine my data as they are free to use. Another tracker I tried had too many features and I was phased out by the features!

Once we start measuring our money, we automatically start improving our money management. And that’s what RupeeManager software helps me to get started with.

My RupeeManager primarily helps organize one’s finances and keeps track of where, when and how the money comes and goes.

Other than tracking your earnings and your expenses, it is important to see if your money is working for your future. We have a feature where you can allocate your income among fixed expenses, discretionary expenses, short term savings and long term investments. It’s like assigning goals for your money.

Also, we have built in a financial health check in the software which gives you an idea about your risk profile and how your asset allocation should look like. To me it’s a simple to use money manager. We will do market testing shortly.

### Q: You are a non-IT person. However you have a very active blog, you have built a desktop financial tool, you built another tool on top of Zoho. Do you outsource the technology part of your business or do you do it yourself?

I am a non techie but absolutely love techies who have helped me with my questions (often very dumb questions). So though I have not outsourced the Zoho tool, the Joomla site and my own blog, I am indebted to the techies who have patiently understood my questions and pointed me to the right direction. I have got the desktop financial tool outsourced because it was beyond me to learn (and use) Java.

### Q: What products of Zoho do you use? What is your opinion of their product suite?

I use Zoho Mail, Zoho Creator and Zoho Sheets very often. I use most of their products once in a while. I love Zoho. Zoho has a much broader product suite compared to Google. Other than their mail, spreadsheet, presentation and document products, they have Zoho Creator (Online database management system), Zoho CRM, Zoho Reports, Zoho Invoice, Zoho People, Zoho Recruit, etc. I think they are a complete Web-IT solutions provider to a business.

### Q: How are you marketing your products and services? What percentage of your marketing is online?

Right now it’s through my blog. You asked me for this interview because of my blog, no! But as I start full time, I will start an online marketing campaign. I will also market through free seminars and workshops.

### Q: Until recently, you had a presence in social media. Why did you delete your social media profiles? Do you think time on social media is a waste of time?

I have met awesome people on the social media. I have made many twitter/Facebook friends and they are on my phone book now. Moreover, I am still on the social media through my blog.

Deleting my Twitter account was part of a de-cluttering exercise. I did try to delete Facebook but it got reactivated as Facebook does not delete your data it seems.

I would not say that my time on social media was a total waste. I made friends, had interesting conversations. But they are a distraction too as I was not able to focus on the priorities. Like even when I was writing a blog post, I would check twitter/Facebook updates in between. I thought it prudent to put a stop to that.

### Q: How are you utilizing technology in other parts of your business?

I believe that my future business will be built on a synergy of technology and learning. Technology is a powerful tool but not the panacea.

I am using technological advances made by Google and Zoho to create a better learning environment. Take a look at these examples: [India’s first personal finance search engine](http://search.personalfinance201.com) and [India’s first personal finance ask engine](http://ask.personalfinance201.com)

### Q: From a consumer point of view, is technology explosion enabling us to have a better financial life or complacent one?

It depends on how we understand technology. Technology is a tool and not the answer to everything, IMO. We can search for information using the technology, but how we apply it to our situation is more important.

### Q: Are you planning to branch to You Tube & e-learning to expand your financial services?

Hey, thanks for this idea. I was thinking of getting some videos done. Now I know what to do! Thanks.

### Q: What are you currently working on? What are the other services & products we can expect in the future?

I have started working on a book. As I go full time, I will be able to announce the services and products that I will offer.

### Q: With the world being flat, are you reaching global audience? Or personal financial services are region centric?

I have been writing to an Indian audience. Generally, the products available in India are not available worldwide and vice versa. But there are many issues other than knowledge of products that have a universal appeal. I follow blogs like “[IWillTeachYouToBeRich](http://www.iwillteachyoutoberich.com/)”, <a href="http://www.getrichslowly.org" rel="nofollow">GetRichSlowly</a> and [TheSimpleDollar](http://www.thesimpledollar.com/) which are US blogs.

I do have visitors across the globe. But I am not sure whether I have been able to provide any value. Maybe I’ll consider this as a good idea.

### Q: Mobile technology has a wider reach within India. How can this be tapped for personal financial services?

I just got in touch with [EnableM](http://enablem.com/), a pioneer in mobile learning. EnableM creates frameworks for skill development and mobile training courses. It’s too early to say, but a personal finance service on mobile is totally possible now.

In addition to blogging and creating products, Ranjan conducts Financial Awareness Workshops as well. You can contact him through his [website](http://ranjanvarma.com/). I wish him all the best in tapping the technology to spread Financial Literacy.
