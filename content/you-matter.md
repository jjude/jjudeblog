---
title="You Matter; You Don't Matter"
slug="you-matter"
excerpt="You don't matter to 99.99999% of the world's population. At the same time, you are extremely important to a small group of people."
tags=["wins","self","networking"]
type="post"
publish_at="02 Nov 21 20:01 IST"
featured_image="https://cdn.olai.in/jjude/you-matter.jpg"
---

Come on, let's play a game. Let me throw some names around. When you know a name, you get a point, otherwise, you don't. 

Ready? 

* Kenneth O Stanley
* Alan Weiss
* Sameer Jain
* Periyar
* Derek Sivers
* Satchin Panda
* Srinivas BV
* Astridah Banda
* M R Girinath

You probably got close to zero or a score of less than five.

![You Matter](https://cdn.olai.in/jjude/you-matter.jpg "You Matter")

Let me tell you who they are.

**Periyar**: A renowned social activist from Tamil Nadu. Tamil Nadu shines in every development indicator due to his self-respect principles. Tamil Nadu regards him as one of the most influential figures in the state. In spite of his prominence, he is probably unknown in other parts of India today, which means he matters to about 80 million people now, but he doesn't matter to 99.99% of the current population.

**Alan Weiss**: A millionaire consultant. I have more books by Alan than any other non-fiction author on my bookshelf. Alan's ideas have shaped my thinking and my career. At best, he matters to 1 million people. The rest don't know him.

**Sameer**: In Chandigarh, you will find that every entrepreneur has respect for what Sameer has built in the city. He has been an entrepreneur in a second-tier city in India for the past 22 years. Even though he is extremely successful, he is unknown to most of the people in the city. 

**Astridah Banda**: Sister Banda is a passionate advocate for public health. Through her weekly radio program, she reaches 1.5 million Zambians with important COVID-19 updates. Were you familiar with her before today?

**M R Girinath**: Dr. M R Girinath has performed more than 25000 open-heart surgeries, and his unit has performed more than 40000. Although he matters to his patients and associates, other people are probably unaware of him.

**Srinivas BV**: During Covid-19, when government assistance was almost nonexistent, Srinivas and his 1,000 strong team responded to SOS messages across India, arranging for beds, oxygen cylinders, and covid medicines. As much as his actions are commendable, he is probably unknown to most Indians.

You get my point. Even though everyone on the list matters to a large population, they don't matter to an even larger population on this planet. 

Seth Godin said it [best](https://seths.blog/2021/10/zero-percent-market-share/):

> If you have a million Twitter followers, that means that 99.9% of the people on Twitter are ignoring you, which, with a little rounding, means you have 0%.

Whatever you think, whatever you say, whatever you do doesn't matter to 99.99999% of the world's population. At the same time, you are extremely important to a small group of people. You mean a world to them. 

Your words will warm their hearts. What you do will affect their generations to come. Your example will inspire hundreds of people to follow in your footsteps. You can really put a dent in the universe that knows you.


### Quote To Ponder

_A single, ordinary person still can make a difference – and single, ordinary people are doing precisely that every day. Chris Bohjalian_

## Continue Reading

* [Ignorant Of Many Things](https://jjude.com/ignorant-of-many-things/)
* [If you are not part of the solution, you are a problem](https://jjude.com/be-a-solution)
* [Fame or Fortune](https://jjude.com/fame-fortune)