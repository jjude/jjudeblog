---
title="Notes from NASSCOM Annual Technology Conference - 2016"
slug="atc16"
excerpt="My notes from the annual NASSCOM Technology conference , 2016."
tags=["coach","tech"]
type="post"
publish_at="20 Dec 16 03:28 IST"
featured_image="https://cdn.olai.in/jjude/2016-nasscom-atc.png"
---
I attended the annual NASSCOM Technology conference on 14th December in Delhi. This is the 3rd NASSCOM conference I'm attending. This was by far the best NASSCOM conference.

Except one or two, all speakers were "**doers**". They shared their **experiential knowledge**. There were no professional keynote speakers or consultants who, at best, have 2nd-hand knowledge.

When the day ended, I was glad I was there.

![NASSCOM ATC](https://cdn.olai.in/jjude/2016-nasscom-atc.png "NASSCOM ATC")

The best session was "**Taming the Rush: e-commerce Technology strategies**". Panel members were:

* [Sanjay Mohan](https://www.linkedin.com/in/snjymhn), Chief Technology Officer, Make My Trip
* [Gaurav Gupta](https://www.linkedin.com/in/gagupta79), VP Engineering, Snapdeal
* [Dhruv Singhal](https://www.linkedin.com/in/dhruv-singhal-07129a1), Solutions Architect, Amazon Internet Services Pvt ltd
* [Puneet Goyal](https://www.linkedin.com/in/goyalpuneet), VP engineering services, OYORooms
* [Pushpinder Singh](https://www.linkedin.com/in/pushpinder), CEO Travelkhana

[A Nandini](https://www.linkedin.com/in/ashwani-nandini-494849a), VP Delivery of [GlobalLogic](https://www.globallogic.com/), moderated the session extremely well. She sought out what happens at the background in handling heavy sales periods like Diwali (equivalent to Black-Friday sales in the West).

My notes from the session:

* Most of the **traffic estimation is a black magic**. They rely on numbers from their respective marketing department and their own estimates;
* What kills is not the traffic but the **spike**. (_it aligns with my own experience in planning for annual filing in MCA21_)
* Planning has to start early, even 3 or 4 months prior to these seasons;
* There are hardware optimizations (addition of servers, memory etc), and there are software optimizations. Not all pages will receive equal traffic. **Identify the heavy traffic pages and optimize them, instead of entire system**;
* **Serve as much as possible from cache**. Beware that this leads to a related issue—if you are serving content from a partner site (like prices of a product from a partner), then at the time of checkout the prices will differ from the listed price. This might lead to customer dissatisfaction;
* **Not all bots are bad**. Some bots will be from your partner sites. Identify those bots and route them to a separate server. Throttle them if needed;
* **Build circuit-breakers**. Instead of slowing down the entire system, it is better to serve certain high value customers (or services) well;
* **Build real-time monitoring systems**;
* Inform and **take along your partners**. You can scale as much as you want, but if partners don't scale then it might affect your sale (applicable if you drive the sale);
* In case your partners are the primary drivers (like Airlines for Make My Trip), you need to have monitoring systems that can alert quickly.
* Use **3rd party tools** like WAF which can identify and handle attacks. Bring in machine learning to build models that can identify attack bots;
* Use **micro-services** architecture. Split your system into independent line of work without affecting other lines of business;
* If you run the system in your own data-center, factors differ. You don't have endless elasticity;
* Every year business changes. Just because you were able to handle spike one year doesn't mean you will handle the traffic next year too. You need to constantly watch your system and optimize continuously.

Another excellent session was presented by [Cynthia Srinivas](https://www.linkedin.com/in/cynthia-srinivas-852a782) & [Soumendra Daas](https://www.linkedin.com/in/soumendradaas) of [Intuit](http://intuit.in/). They talked about their experience in **migrating their monolithic application to cloud**. I got so immersed, I didn't take much notes.

My notes:

* Today's startups are born cloud-native. What about enterprises who have large business applications in-house?
* Define success metrics to measure after migration to cloud;
* Look at the post-migration status from customers, employees, and management (ex: no more capital expenses);
* Security considerations changes when you move to cloud from in-house hosting. In in-housing hosting, you may depend more on peripheral security. You may trust the traffic that passed through the firewall. You can't do so in cloud hosting;
* Security is not only about prevention of attack. It includes identifying attacks, alerting for attacks, and response mechanisms;
* You should have sweep mechanism to scan the infrastructure to identify unused infra and redeploy them.


There was a session by **Viral Shah**, co-creator of Julia language. Obviously, he tried to convince, Julia is the best language for data science.

He mentioned an interesting point. We, in software industry, have **two language problem**. You could write program fast or you could execute program fast. He mentioned that Julia solved this problem.

_Note to self: learn Julia in the coming months_.

Last of the interesting session was **Challenges and Strategies for Mega Nation Building projects**. Of course everyone was all praises for demonetization and all were in awe of the progress India is making and all (without a faintest sympathy to people dying while standing in queue and others losing their daily wages standing in queue).

But there was an insightful section of the session.

When we think of scale, we immediately think of Facebook, Google, and Twitter. But we forget IRCTC, Aadhar, and election. They are bigger than most of the systems that come to mind when we think of scale. **We always discount the giant among us**.

Even the best can improve. So there is one suggestion: they should arrange for a store room for attendees from outstation to keep their luggages safe.

Otherwise, this was a well arranged conference. Will definitely attend the next one.
