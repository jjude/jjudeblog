---
title="A new way to read books"
slug="a-new-way-to-read-books"
excerpt="Read your favorite book via email"
tags=["insights"]
type="post"
publish_at="05 Oct 08 16:30 IST"
featured_image="https://cdn.olai.in/jjude/a-new-way-to-read-books-gen.jpg"
---

Next to programming, reading books (along with Photography) is my hobby. If I have not read a book in a month, I feel I've wasted that month; and I spend a weekend exclusively on reading.

Topics I read vary, but I'm particularly interested in practical applications of theories than plain academics. Software Engineering, Management and Self-development top my reading list; followed by theology and psychology. I do read novels and other topics but they have to be really interesting for me as I dedicate less time to straying away from my primary interests.

With increasing work load at office and at home, there is a serious crunch for time to read books. So when I came across a service that enables one to read books via email, I was thrilled. [DailyLit](http://dailylit.com/) emails a snippet of a book daily at a time of your choice.

I like it because it is via email - any ways email client, in my case Microsoft Outlook, is opened throughout the day; it is a snippet, it takes only 10 minutes a day. And since I got Blackberry, I can read the book snippet anywhere.

I've already read Tom Peters' [100 ways to succeed/Make Money](http://dailylit.com/books/100-ways-to-succeed-make-money) in 100 installments. Currently I'm subscribed to [Good Experience Columns](http://dailylit.com/books/good-experience-columns), series of articles on improving user/customer experience. I've gotten only 3 installments and I'm already loving it.

[DailyLit](http://dailylit.com/) has both free and paid services. I'll try out couple of more free books and then wouldn't mind subscribing to their paid services.

If you are a book-lover, like me, and crunched for time, try [DailyLit](http://dailylit.com/). You might like it.

