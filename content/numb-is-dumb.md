---
title="Numb is dumb"
slug="numb-is-dumb"
excerpt="Laugh, think, and cry. Make your life special"
tags=["wins","parenting","self"]
type="post"
publish_at="21 Oct 20 09:30 IST"
featured_image="https://cdn.olai.in/jjude/numb-is-dumb-gen.jpg"
---

Can grownups cry? What a dumb question. With a silly answer.

We have stigmatized an essential emotion causing mental illness of an entire generation.

Within the confines of office life, we demand others to "behave professionally." Outside of professional settings, we expect everyone to "behave like a grownup." Both mean **numbing emotions**. We've heard these phrases so often, and now we feel guilty if we weep or laugh even in our private moments. "I am so sorry," we are so quick to apologize if we laugh long and loud or a sudden tear appear on the sides of our eyes.

Suppressed by society's expectations, some drink to loosen themselves; Others drink to drown their sorrows; Few others smoke to think. I have done all and abandoned them not because of a saintly compulsion, but I refuse to depend on **emotional crutches**. I experience life in raw as much as I can take.

My heart was heavy during a period in my life. I would go to a nearby church as soon as it was opened, before anyone would come, to be alone so I can cry. Tears were my prayers. For three months. 

After that time, I wasn't in as much sadness as this year.

This year has added much pain. I have lost friends, role models, and parents of close friends. Nothing caused more anguish than losing the beautiful and cheerful seventeen-year-old daughter of a best friend to cancer. The spread of COVID has added to the agony because now we can't meet, hug, and sob. I wake up many nights engulfed in the gloom.

When you wash your soul with tears in the night, you can cheer others in the day, knowing well that **every heart has a hidden tear**.

The other day a friend commented, "you are more humorous than the other friends I know." I suppose one's tears of the heart can tickle other's funny bones. It has been the case for the greatest comedians like [Nagesh][1] and [Robin Williams][2]. I don't laugh to hide my heartache. I laugh because laughter is excellent medicine.

At home, the boys and I huddle together almost daily as I narrate funny incidents from my life, especially from college days. They want to hear specific stories again and again. I've lost count of times I have repeated those favorite stories. They can't stop laughing, hearing the story. I can't stop laughing, seeing them laugh. **Laughter is, after all, contagious**.

Some days, I dance uncontrollably and without coordination. My wife comments, "here comes the actions of a mad man." Sometimes boys join me. My elder boy is a good dancer. So when he enters, it becomes a dance routine, rather than just a mad man's actions. We dance until we lose our breaths. **We pant, but our hearts are light**—what an experience!

Most Indians have arranged marriages. Many husbands and wives develop intimacy but not love. After the birth of kids, the relation plateaus to platonic. I wasn't an exception to this peculiar Indian phenomenon. In the last few years, however, we have redefined our relation and rekindled our passion. If I may steal the words of Bernard Shaw, we don't stop loving because we grow old; We grow old because we stop loving. **I hope to stay young until the day I die**.

I'll leave you with the words of James Valvano, which he delivered just two months before his death:

> "If you laugh, you think, and you cry, that's a full day. That's a heck of a day. You do that seven days a week, you're going to have something special."

Experience life. Make your life special.

## Continue Reading 

* [A eulogy for my mother](/eulogy-for-mother/)    
* [Sweet home is a safe home, even digitally](/sweet-home-safe-home/)    
* [Build on the rock for the storms are surely coming](/build-on-rock/)


[1]: https://en.wikipedia.org/wiki/Nagesh
[2]: https://en.wikipedia.org/wiki/Robin_Williams


