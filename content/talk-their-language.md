---
title="Talk Their Language"
slug="talk-their-language"
excerpt="Communication is effective and complete only if audience understand what is being communicated. Don't throw jargons for critical ideas hindering the comprehension."
tags=["problem-solving"]
type="post"
publish_at="28 Feb 11 16:30 IST"
featured_image="https://cdn.olai.in/jjude/talk-their-language.jpg"
---
I once worked as a manager in an internal IT department. During that tenure, I built and managed IT systems for their internal support functions like - HR, Finance, and Quality. If there was one constant complaint from managers of these functions, it was that the development team hid behind software jargons. In fact in some cases, these departments had to train their staffs in IT development process so as to communicate to us!

This is a common phenomena that I witness across private and public sector projects.

![Talk to your audience in their language](https://cdn.olai.in/jjude/talk-their-language.jpg)

IT professionals, in general, are smart enough to understand the customer issue and can work out an effective solution. But when they present that solution, they hide behind the shiny pillars of Activity Diagrams, APIs and so many mumbo-jumbo which sometimes even those in IT can't understand. The smartness is matched with an equal arrogance to expect others to comprehend the words we utter.

Though this arrogance is widely spread in IT, it is not just limited to it. Financial planners, for example, suffer from the same curse. (Let us leave economists out of this discussion since their job description is to talk so none understand. If not for them, how will we get phrases like quantitative easing?).

Service providers, like IT and financial consultants, fail to understand that communication is effective and complete only if the other party comprehend what is being communicated. Throwing up jargons for critical ideas hinder that comprehension.

It doesn't mean we should dumb down discussions. We can and should educate clients about essential concepts. After all, when they seek to improve their situations, they better understand the technology that is helping them. But, like everything else in business, success lies in knowing the difference and balancing the two.

_This post is part of [Be a Problem Solver](/be-a-problem-solver/) series._
