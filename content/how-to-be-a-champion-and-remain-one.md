---
title="How To Be A Champion And Remain One?"
slug="how-to-be-a-champion-and-remain-one"
excerpt="Becoming a champion is a three stage process. It is not a step-by-step process; these three stages may be present in varying degrees in the journey."
tags=["sdl","wins","insights"]
type="post"
publish_at="30 Jun 10 16:30 IST"
featured_image="https://cdn.olai.in/jjude/stages-of-champion.png"
---
When Roger Federer glide through tennis courts winning matches it is clear beyond any doubt that he is born to be a tennis player. Winning comes so easy to him.

The world woke up to A R Rahman after he won Oscars. But Indians have been enjoying the ‘master of melodies’ for more than a decade. One can listen to his melodies for ever. He is a gifted musician.

Champions exhibit their mastery and when they do, years of their deliberate practice is hidden from the crowd. We are carried into an illusion that ‘gods have been good to them’.

‘Gods are good to the champions’, is an excuse we tell ourselves and others to hide our laziness. We too can be a champion so long as we are focused and practice deliberately.

Becoming a champion is a three stage process - consume, produce & engage. It is not a step-by-step process; these three stages may be present in varying degrees in the journey to be a champion.

![stages of champion](https://cdn.olai.in/jjude/stages-of-champion.png)

In the consume stage, one learns the concepts, ideas, best practices and emerging techniques. They may be learned in a formal classroom, workshop, from a mentor or by reading books and listening to podcasts. This is a learning stage.

Consumption makes you a storehouse of ideas; but the producing value makes you an authority. There are just too many walking storehouse of ideas these days. They go from one coaching program to another or go on consuming a book library but never step out of their cocoon to act. But there is no better coach than the act of closing a sale, implementing a project or writing a book.

By consuming you learn and by producing value based results you gain confidence. But by engaging - in conversation, discussion and debate - one broadens the understanding. By engaging with peers, mentors and next generation thinkers you validate your ideas. You investigate if your success is an accident or coincidental or can it be repeated in other contexts and be a pattern.

Championship isn’t a success in a single game. The stages of consume, produce and engage have to be repeated again and again. Then the world may talk about you and say ‘gods have been good to him (or her)’.
