---
title="Innovation models of John F Kennedy and Nehru"
slug="nehru-jfk-innovation"
excerpt="Lay foundations for the future. Take advantage of those foundations too."
tags=["wins","systems","coach"]
type="post"
publish_at="16 Jul 22 12:38 IST"
featured_image="https://cdn.olai.in/jjude/nehru-jfk-innovation-gen.jpg"
---

A nation can innovate in two ways:  
- JFK's way  
- Nehru's way  
  
**JFK's was more goal-oriented. Nehru's approach is more system-oriented.** Each may be appropriate for a different period of time and maturity of the nation. They apply to not just nations, but to companies and self-improvement as well.

JFK outlined a specific, measurable, big audacious goal for the nation - "this nation should commit itself to achieving the goal, before this decade is out, of landing a man on the moon and returning him safely to the earth"

His lofty goal galvanized the nation.

By working backwards from this goal, the US achieved everything related to it. By investing in research, science and technology progressed, ultimately benefiting the general public.

In contrast, **Nehru did not specify a specific goal** for the nation. His vision focused more on "facilitating" systems. His aim was to **create stepping stones** and to leave the goals to the experts.

In his popular speech, "Tryst with Destiny", he said: "The achievement we celebrate today is but a step, an opening of opportunity, to the greater triumphs and achievements that await us.." The freedom was a step, not a goal. Not a destination. Not a place to rest.

His speech outlined a solution: "to create social, economic and political institutions which will ensure justice and fullness of life to every man and woman" He laid the foundations for those institutions - IITs, IIMs, statistical institutions, dams...

When he laid those foundations, he was unsure of his specific goals. He envisioned "fullness of life to every citizen". His "systems thinking" worked for decades afterward. India's trip to Mars was the least expensive ever. Indians who studied in Nehru's institutions lead most large companies across the world.

Imagine Nehru saying, "Within this century, we'll go to Mars for the lowest cost", when poverty, illiteracy, and partition plagued the country. It would have been stupid. Clearly, he was smart. He trusted the process. **Good processes lead to good results**. It did.

Which one is better?

It is not either or, but both. Nehru created strong institutions as a foundation for India. We now need to use those foundations to achieve ambitious and audacious goals. But now is also the time to lay the foundations for the future. One has to feed into the other to continue to grow.

Laying foundation and taking advantage of that foundation is applicable to countries, companies, and individuals. 

If you are an executive in a company, analyze its past achievements and understand its strengths. Set audacious goals based on those strengths. Meanwhile, plan for the future and create a vision for your business. Identify gaps between the company's current state and its vision. Develop facilitating systems that can help the company grow in the direction of that vision.

If you want a practical guide for this process, read my "[future doesn't happen to us, future happens because of us](/shape-the-future/)" post.

## Continue Reading

* [Want to succeed in life? Have a system](/system-for-success/)
* [Future doesn't happen to us, future happens because of us](/shape-the-future/)
* [Build on the rock for the storms are surely coming](/build-on-rock/)
