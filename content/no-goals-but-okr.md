---
title="Don't chase goals. Do this instead"
slug="no-goals-but-okr"
excerpt="Not only achieve your goal but retain the outcome"
tags=["frameworks","coach"]
type="post"
publish_at="09 Jan 20 18:26 IST"
featured_image="https://cdn.olai.in/jjude/no-goals-but-okr-gen.jpg"
---

Happy New Year. 

Did you have a great party welcoming the year or curl up under a thick blanket to escape the cold hell? Whatever way you embraced the year, I'm sure you set up some big audacious goals for the year.

* Lose 5 kgs weight
* Read 20 books this year
* Learn Spanish
* Spend more time with family

My good wishes for your goals. Here is the harsh truth, though: you will forget all about your goals before the end of this month.

No, I am not pessimistic or cynical or condescending; I am just grounded.

Am I against getting better? Oh, no. I'm saying there is a better way.

Instead of chasing a goal, become a person that embodies that goal. If that sounds like a new-age guruji speaking, let me explain.

Why do you want to lose five kilos? Because you want to be fit. So instead of chasing, 'lose five kilos,' become a fit person. Ask what a healthy person would do?

* Going for a jog
* Taking stairs instead of the lift
* Not drinking empty calories

If you "become" that person, you will not only achieve your goal but retain the outcome as well.

When you extend this concept, instead of chasing the above goals, you'll become:

* A curious person
* A learning person
* A loving person

Wanting to become a better person is good. How do you pursue that desire? There is a framework to help - OKR, which stands for **objectives and key results**. Here is how OKR might look like if you had set the goal of losing five kilos:

Objective: Become fit
Key Results: 
1. Jog three days a week for 5 km
2. Intermittent fasting on Thursdays and Saturdays
3. No food after 8 pm

If you had set the goal of "spending more time with the family," then your OKR might look like this:

Objective: Becoming a loving person
Key Results:
1. Pizza night (Wednesday) with family without any devices
2. Date night (Saturday) with the spouse
3. Once a month lunch with close friends

OKR framework brings clarity to your thinking and helps you write down actionable items. I have been following OKRs for the last four years, and it is transformative. I've followed it in personal and professional life and gained significant benefits.

John Doerr popularized OKR. He learned this robust framework when he worked at Intel. Later, John became a venture capitalist and introduced the framework in every company he invested, including Google. He has written a book, [Measure what matters](https://www.amazon.com/dp/0525536221/?tag=jjude-20), where he explains the framework. The book also includes experience on different CEOs in implementing the framework. 

If you want a quick intro to the framework, [Felipe Castro](https://felipecastro.com/en/okr/what-is-okr/) has written an excellent guide to the framework.

Read about it, or feel free to chat with me to learn more about it.
