---
title="How to develop your gravitas?"
slug="how-gravitas"
excerpt="Four steps to build your executive presence, aka gravitas"
tags=["wins","biz"]
type="post"
publish_at="15 Sep 21 10:37 IST"
featured_image="https://cdn.olai.in/jjude/how-gravitas-gen.jpg"
---


When you have competence and confidence you are able to exhibit executive presence, aka [Gravitas](https://jjude.com/gravitas).

But how do you continue to build competence and confidence as you move through different stages of career? There are four steps.

### Insights

It always starts with an insight, a unique insight.

An insight could be something novel:

- sending man to the moon
- traveling the world in 80 days
- electric cars

It could be figuring out how something works:

- building a successful company
- gravitational force
- anthropology

It could be new way of doing old things:

- electric cars
- email
- digital photos

Every unique insight helps you to rise above the mediocre crowd. An insight does not have to be original to be unique. You should have thought deeply about it and formed your opinion on your own.

### Network

All success is social. As you share your unique insights with others, they respect you and gather around you to realize that insight. 

Dharmesh Shah said it [best](https://twitter.com/dharmesh/status/504480566084128769):

Your probability of success is proportional to the number of people that want you to succeed.

Your network should have:

- mentors
- peers
- juniors

It is best to learn from those who have been on the journey before and are willing to coach you to avoid mistakes.

However, you should also surround yourself with people who are on the same journey, so you can share your notes and learn from their experiences.

Remember to give back as you achieve success and gain insights. If a mentor offers his hand, offer your own to those who need it.

### Wealth

When you have large and diverse network, you'll extract wealth from that network. Mind you, I'm not talking about just riches, but wealth.

Being rich means,

- fancy cars
- beach houses
- expensive dresses

Wealth means freedom. You should be free to pursue what you want to do, when you want to do, and how much you want to do. Your days are not constrained by other's priorities and emergencies.

Wealth enables you to:

- experience life in its fullness
- lead a meaningful life
- have fun-filled days with family and friends

### Self-Control

MBA programs and leadership courses teach building insights, networks, and wealth. They don't teach about self-control, however. This is why we routinely see business leaders facing jail time. 

It takes self-control to use wealth, insights, and networks in order to make a name for yourself. It's the ultimate gravitas, after all.

## Continue Reading

* [What Is Gravitas, Anyway?](https://jjude.com/gravitas)
* [My Course on building Gravitas](https://jjude.com/gwcourse)
* [If you don't blow your trumpet, how can you expect music?](https://jjude.com/blow-your-trumpet)
