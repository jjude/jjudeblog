---
title="What game are you playing?"
slug="game-you-play"
excerpt="Your choice of game determines the rules"
tags=["wins","coach"]
type="post"
publish_at="26 Jul 22 13:57 IST"
featured_image="https://cdn.olai.in/jjude/game-you-play-gen.jpg"
---


"In life, the challenge is not so much to figure out how best to play the game; the challenge is to figure out what game you’re playing" - Kwame Appiah

- Employee
- Entrepreneur
- Indie-Maker

These are just a few of the games we can play. The "employee"game differ from the "entrepreneur" game and they differ from the "indie-maker" game.  
  
There are, of course, **certain rules that apply to all games**. We all need to be productive and healthy physically, mentally, and financially. However, most other rules differ.  
  
Nearly everything an employee does is geared toward **improving their salary**. His goal is to work on a high-profile project or in the latest technology so he can ask for a raise or move to a better company at a later time.
  
An entrepreneur is **always selling**. She is selling to the prospect, to existing clients, and to a prospective employee. All other work she does supports the core one.
  
An indie-maker's game could be best of both worlds; but it's usually worst of both worlds.

Each player approaches activities differently depending on the game they play.

Let's take the case of learning. All three players are interested in learning and improving their skills. Their approaches are different, however.
  
Employees flash certificates to **signal others**. Check out the LinkedIn profiles of employees. You'll find a list of certifications. Why? They are signaling to their managers that they are ready for the promotion, or to recruiters that they have the knowledge to take on the role.  
  
Entrepreneurs aren't interested in flashing their certificates. Their certificate is worthless to them. They focus on **converting knowledge into revenue and then into profit**. Therefore, they prefer mentoring over classroom sessions.

Indie-makers attract prospects by what they have done rather than what they know. Thus, they are more interested in flashing portfolios than individual certificates. So, they are interested in **learning skills that are applicable now** rather than acquiring certificates.

I could go on and on about how they handle expenses, growth, and social status. But I think you get the idea.

Your choice of game determines the rules.

## Continue Reading

* [A Game Plan For Life](/game-plan-for-life/)
* [Map Is Not The Territory](/map-is-not-territory/)
* [Do You Have A Best Friend At Work?](/best-friend-at-work/)
