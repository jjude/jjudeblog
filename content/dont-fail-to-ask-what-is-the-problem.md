---
title="Don't Fail to Ask 'What is The Problem?'"
slug="dont-fail-to-ask-what-is-the-problem"
excerpt="A prudent question is one-half of wisdom. So what is a prudent question in resolving a problem?"
tags=["problem-solving"]
type="post"
publish_at="23 Jan 11 16:30 IST"
featured_image="https://cdn.olai.in/jjude/dont-fail-to-ask-what-is-the-problem-gen.jpg"
---

"A prudent question is one-half of wisdom,"  said the father of empiricism, Francis Bacon. A prudent question in problem solving is not the usually asked questions : how to solve or how fast it can be solved. Rather it is the basic question of what the problem is.

Problem, by definition, is a deviation from a stated objective, goal or purpose. Whenever you are called to solve a problem, your first question should be, 'So what is the problem?' and the answers should lead you to the objective that the situation - project or software program - was suppose to achieve. It goes without saying that the question and answer session should be iterative to nail the root cause of the problem. Causes commonly fall into a pattern:

Most problems are **problems of purpose**. Lack of purpose, ill-defined purpose, ill-communicated purpose or such a variant are the major causes of organizational disorders.  You will expect that million-dollar projects are managed with a clear purpose. Far from it. May be they were started with a grand vision. With the passage of time the objectives of the project are no more clear to stakeholders.

Then there is **problem of perception**. What sales team perceives as a great opportunity is usually perceived by operations team as a risky adventure which may ruin the good work they have done so far. Seldom these two groups reconcile; and when they stand firmly on the opposite sides of the opinion-spectrum you got a problem.

What the IT industry suffers is the third kind of problem and that is,**problem of competence**. No, it is not the competence of technology skills. Proven technologists are either promoted by organizations or they themselves race to managerial levels where they prove highly incompetent, which in turn creates all sorts of management problems.
Even when skilled members work along side in a clearly defined project, their efforts can be sabotaged by politics between the executives. "When the elephants fight, it is the grass that suffers," goes one African proverb. The wisdom of the proverb is visible in organizations across the globe.

Why is it important to identify the precise problem? It might appear a foolish question. But often organizations and consultants start out on the path to resolution without a precise understanding of the problem. Without identification of the correct problem, the proposed solution will be a misfit which will open another Pandora's box.

_This post is part of '[Be a Problem Solver](/be-a-problem-solver/)' series._

