---
title="3 Core Requirements For A Happy-path Project Life"
slug="3-core-requirements-for-a-happy-path-project-life"
excerpt="For a happy-path project life-cycle, its essential to have a well defined support structures in place. Without these structures, there won't be any life in the cycle."
tags=["project-mgmt","coach","visuals"]
type="post"
publish_at="03 Jul 10 16:30 IST"
featured_image="https://cdn.olai.in/jjude/project-life-cycle.png"

---

For a happy-path project life-cycle, its essential to have a well defined **support structures** - outer & inner circles in the figure - in place. Without these structures, there won't be any life in the cycle.

![Project Life Cycle](https://cdn.olai.in/jjude/project-life-cycle.png "project_life_cycle")

The most important of these support structure is, undoubtedly, **people** - its not the latest technology; its not the international best practices; but the right people. If right people are on board, they bring in everything necessary to ensure a success.

A **tailored process** brings a common understanding between the people so carefully selected. Its essential to keep processes, lean and effective. Generally processes are defined by a distant group named in one of the flavors of 'quality management group'. While its a good idea to have a group centrally own company's process, its effective if team members are involved in customizing the process. It will bring the buy-in, in the least, awareness.

James Bond needs latest gadgets; Jason Bourne does not. A well trained professional can make use of the available **tools** to accomplish the goals. However, that shouldn't be quoted to deprive the team of needed tools.

**People, process & tools are at the core of successful life-cycles**. Projects should not be started without these structures in place.

_How often projects are started without support structures?_

## Continue Reading

[How to improve project delivery in an IT services company](/it-delivery-system)  
[System for success](/system-for-success)
