---
title="How to have a healthy relation with your siblings and why it matters?"
slug="sibling-relation"
excerpt="Steps to strengthen sibling bond well past childhood"
tags=["self""wins"]
type="post"
publish_at="30 Jun 22 19:16 IST"
featured_image=""
---
Our parents don't stay with us throughout our lives, our romantic partners and friends come and go, but our siblings are always there. Because of that long-term connection, siblings play an important role in our personal growth and well-being. People with close sibling relationships have better mental health, better psychological health, and better social relationships.

I didn't really get along with my younger sister until I was a teenager. I was mean to her. At home, I wanted to be the boss at home. My love for her grew as I saw the world and grew up.

Now the roles are reversed. She doesn't have to show who the boss is at home. We know she is the only man in the house. She has been involved in every major decision in my life. I have never made a decision without consulting her. Sorry. Let me rephrase that. There have been no major turning points in my life without her involvement.

She picked up the flat I had to purchase, negotiated the final price, and arranged the loan. I signed all the papers and bragged about my new  apartment. In the same way, she looked for the perfect match for me,  arranged everything, and I went there only to exchange rings. She hosted my first son and my wife for six months before they moved to live with me. She has become such a friend with my wife that she joyfully joins my wife to tease me.

Here are some points that have strengthened our bonds well past our childhood.

## Early family routines
As children, we had family prayer and dinner almost every day. Daily routines didn't develop love between us, but they bonded us as a family. As a family, we traveled at least twice a year. Those were the times when we often helped each other out, understanding each other's likes and dislikes.

## Continual family rituals
Even after leaving home and starting our own families, we still met at least once a year to spend time together. Our kids play Roblox together. We tease each other and share silly insider jokes regularly on WhatsApp.

## Love covers many flaws
When there was a small family altercation early in their marriage, my brother-in-law made a comment in Tamil - குற்றம் பார்க்கில் சுற்றம் இல்லை, which can be loosely translated as "love covers many flaws". I never forgot that. This has become the unwritten rule binding our families together. Look over flaws for family.

## Let adults be adults
At family reunions and on family Whatsapp groups, we act silly and childish. Other times, however, no one tries to control others. Let adults be adults. In a family, giving each other space is crucial. Unnecessary pressure fractures the family-bond.

Remember life is not one big thing. It is the sum of many small things.


## Continue Reading

* [Are You Spending Enough On Recovery?](/recovery/)
* [Writing My Obituary](/my-obituary/)
* [You Matter; You Don'T Matter](/you-matter/)