---
title="Go faster with a structure to success"
slug="structure-to-success"
excerpt="Create a life of meaning and contribution by uncovering structure behind success."
tags=["wins","coach","systems"]
type="post"
publish_at="16 Feb 22 08:58 IST"
featured_image="https://cdn.olai.in/jjude/jumbled-words.png"
---
I have read a lot of books on self-improvement. "The Seven Habits" always tops the stack.

I first read that book 20 years ago. To say it changed me is an understatement. The book transformed my thinking and thus my life.

_This year, I began rereading older books. In fact, we are reading and reviewing it publicly as a Gravitas WINS community. If you are interested to join, reply back. Will share the details._

Steven Covey offers a number of exercises in the book, one of them being the following:

As shown below, a page has numbers from 1 to 54 jumbled together. You need to identify as many of them as possible in 30 seconds.

Go.

![Jumbled Words](https://cdn.olai.in/jjude/jumbled-words.png "Jumbled Words")


You will probably find 20 or so if you have never played this game. I found only ten.

There is, however, a trick to finding all the numbers. It's about knowing how the numbers are arranged. Even though the numbers appear random and arbitrary, they follow a structure. The madness has a pattern.

It is arranged in a 3 x 3 matrix starting with 1 at the top left corner, 2 in the middle, and so on until 9. After that, 10 is on the top left and this repeats until 54.

[Click here if you want to see the solved puzzle](https://cdn.olai.in/jjude/jumbled-words-solved.jpg).

Each success factor in life-wealth building, networking, growing a company-is similar to these numbers. It seems chaotic, complex, and confusing. Despite your best efforts, it seems impossible to make any progress. You get lost often and feel that your efforts are ineffective.

You can advance much faster and all the way to the top if you discover the structure of success. That's how you create a life of meaning and contribution.

## Continue Reading

* [Structured Communication](/structured-communication/)
* [How to think structurally?](/structured-thinking/)
* [Book Summary-The McKinsey Way](/mckinsey-way/)