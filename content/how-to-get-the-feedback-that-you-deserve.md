---
title="How To Get The Feedback That You Deserve"
slug="how-to-get-the-feedback-that-you-deserve"
excerpt="Obtaining feedback is critical to designing products that stand the test of time. But getting meaningful feedback isn't easy."
tags=["coach","startup","insights"]
type="post"
publish_at="10 Feb 12 16:30 IST"
featured_image="https://cdn.olai.in/jjude/how-to-get-the-feedback-that-you-deserve-gen.jpg"
---

Obtaining feedback is critical to designing products[^1] that stand the test of time. But getting meaningful feedback isn't easy. If it is tough in a corporate environment, it is even more difficult within Government setup[^2].

So how can you get the best feedback?

Interestingly, most of the necessary activities happen outside the meeting room (I'm thankful to my earlier [boss](http://www.linkedin.com/pub/mahendra/0/544/86b) for demonstrating this to me time and again).

To start with, you need to know when to obtain feedback. Not too early, despite the pressure to send the 'first draft'; not too late, despite your love for perfection. Appropriate time is when it is _**small enough to be manageable and big enough to be relevant**_. As your product evolves, you need to repeat this process until the final delivery. This iterative process also gives you an early sign of future problems. If you sense any problems to come, you could still do course correction.

You should _**engage the client team**_ as often as possible - mostly informally - to get their ideas about the project as well as your creation. This enables you to get insider's perspective of the project. Let us say you find out that the project is a low priority one, you can be sure that getting feedback will be of even lower priority.

If you could _**influence the composition of the group**_ to provide feedback, you should do it by all means. There should be an ideological resonance within the team towards the project and hence to the product, without which feedback sessions will be disastrous.

_**Circulate the document**_ well in advance giving the members ample time to read and come prepared. Some people would advice to keep the group in suspense but I don't think it is a good idea. In my experience, circulating the document and engaging the members before the feedback meeting ensures better feedback during the actual meeting.

If you have executed the above steps diligently, feedback meeting should be effective. Still surprises can come from any corner. To wade through these surprises, you should designate someone as a _**facilitator**_ in the meeting. The sole responsibility of the facilitator is to ensure feedback from every one is obtained and rants are controlled effectively.

At the beginning of the meeting, _**set specific goals for the meeting**_ - feedback only on Table of Contents; feedback of only the process flows but not the mock-screen elements and so on.  Give the participants a structure to think and provide feedback. Our ancestors have already proved that horses with blinders run faster and on track. It will be the duty of the facilitator to ensure that the blinders are on throughout the meeting.

Given to itself, meetings have a way of turning into chatter rooms. I have also witnessed feedback meetings morph into review meetings of other projects of the clients, because they want to _take advantage of the presence of the members_. Since it is **your meeting**, it is your responsibility to steer the meeting back into track. As situation may permit, go and stand in the front. If available utilize the board. That is a proven way to take control of the meeting (another point which was emphatically proven by Mahendra again and again).

An important activity that gets left out is the _**follow-up**_. Follow-up is not limited to sending the minutes; but also introspecting what went right and what went wrong, if possible with few of the client members. This is a great enabler for successive meetings turning into success.

Now it is your turn. Tell me - via [twitter](http://twitter.com/jjude), or [gplus](https://plus.google.com/105440944797450602039/) - what else do you do to get the best feedback that you deserve?

_Image by: [HikingArtist](http://www.flickr.com/photos/hikingartist/)_

[^1]: This is applicable to design of every type of products - mock screens, process re-engineering, RFP creations and so on.
[^2]: Though most of this discussion will be true in corporate world, I'm limiting this to a Government setup.

