---
title="Best of what I consumed in 2018 Q1"
slug="2018-q1-consume"
excerpt="Key points from the videos, articles, books, and audio books I consumed in 2018 Q1."
tags=["wins","insights","retro"]
type="post"
publish_at="29 Mar 18 14:47 IST"
featured_image="https://cdn.olai.in/jjude/2018-q1-consume-gen.jpg"
---

I read, watched, and listened a lot in these three months. I took copious notes. Here are the key points from the videos, articles, books, and audio books I consumed in this quarter.

### Videos
* [Shapes of Stories](https://www.youtube.com/watch?v=oP3c1h8v2ZQ) by Kurt Vonnegut
    * All stories can be drawn in two-axis — vertical axis is G-I (good fortune / ill fortune) and horizontal axis is B-E (beginning / end)
* [Freedom is difficult, but be careful of wishing for fences](https://www.youtube.com/watch?v=3xBVtwJSCQQ)
    * **Be aware of the thief in your mind who can steal your promises**
    * Animals are safe in their cages, the fence keeps out predators and competition.
    * Freedom is a choice to do or not do. It is the heaviest burden on the humans.
    * Freedom is being responsible for the consequences.
    * **Discipline comes from inside to do something. Obedience is because of fear**.
    * Discipline and indiscipline pays in the long-term.
    * Judas got the money, but at what price. When he realized the price, he hanged himself.
    * Don't make a masterpiece, make your life a masterpiece

* [Seven habits of highly 'creative' people by Dr. Pavan Soni](https://www.youtube.com/watch?v=LmXGvsuNDv8); [Slideshare](https://www.slideshare.net/pavan7soni/seven-habits-of-creative-individuals)
    1. Multiple affiliations
    2. Curiosity
    3. An opposable mind
    4. Experimental mindset
    5. Taking hobbies seriously
    6. Taking half-chances
    7. Cultivating humor ( have low inhibition)

### Articles
* [Tools for Systems Thinkers: The 6 Fundamental Concepts of Systems Thinking](https://medium.com/disruptive-design/tools-for-systems-thinkers-the-6-fundamental-concepts-of-systems-thinking-379cdac3dc6a)
    * Interconnectedness - Everything needs something else, often a complex array of other things, to survive
    * Synthesis - combining of two or more things to create something new
    * Emergence - emergence is the natural outcome of things coming together (**There is nothing in a caterpillar that tells you it will be a butterfly — R. Buckminster Fuller**)
    * Feedback loops - Reinforcing loops causes instability but exponential growth; balancing loops are self-correcting and brings stability
    * Causality - one results in another in a dynamic and evolving system
    * Systems mapping - identify elements within a system to understand how they act and interact

* [VAPOUR its where great ideas come from](https://www.linkedin.com/pulse/vapour-its-where-great-ideas-come-from-gary-brown/)
    * Versatility 
    * Application
    * Positivity
    * Open
    * Understanding
    * Reduce

* [The four elements of entrepreneurship](https://sethgodin.typepad.com/seths_blog/2018/01/the-three-elements-of-entrepreneurship.html)
    * Entrepreneurial behavior is about a desire for a **certain kind of journey**.
    * What do people do when they’re acting like entrepreneurs?
        1. They make decisions.
        2. They invest in activities and assets that aren’t a sure thing.
        3. They persuade others to support a mission with a non-guaranteed outcome.
        4. They embrace the work of doing things that might not work.

* [Why You Should Have (at Least) Two Careers](https://hbr.org/2017/04/why-you-should-have-at-least-two-careers)
    * By committing to two careers, you will **produce benefits for both**: Subsidizing your skill development, make friends in different circles, discover real innovations;
    * By doing more than one job, you may end up doing all of them better.

* [Making Sense vs. Being Right](https://www.collaborativefund.com/blog/making-sense-vs-being-right/)
    * People care about something making sense than something that is right.
    * **Stories beat statistics**.
    * Storytelling is an exponential fuel.
    * An OK idea told persuasively is more powerful than a good idea told poorly.
    * The CEO who understands everything about their field except the most important thing – rallying employees behind a mission and vision.
    * Virtually **every job is a sales job**, especially as you move up the ladder.
* [Ironies of Luck](https://www.collaborativefund.com/blog/ironies-of-luck/)
    * Luck & risk are cousins. They both happen because the world is too complex to allow 100% of your actions dictate 100% of your outcomes.
    * Realizing that **both luck and risk are ever-present** makes you accept that not everything is in your control, which is the only way to identify and focus on whatever is in your control.
    * There are all kinds of quotes that belittle luck – "It wasn’t luck, it was hard work and persistence," and whatnot. I get the temptation. But can you imagine how crazy someone would look saying the same thing about risk? "The earthquake wasn't a risk, we just didn’t work hard enough to predict it."
* [Career Advice](https://dilbertblog.typepad.com/the_dilbert_blog/2007/07/career-advice.html)
    * Capitalism rewards things that are both rare and valuable. You **make yourself rare by combining two or more "pretty goods" until no one else has your mix of skills**.

* [Use This Equation to Determine, Diagnose, and Repair Trust](http://firstround.com/review/use-this-equation-to-determine-diagnose-and-repair-trust/)
    * Trust = (Credibility + Reliability + Authenticity) / Perception of self interest
    * Credibility = to have the knowledge, experience, and familiarity to perform a particular role well
    * Reliability = do what you say you're going to do
    * Authenticity = do and say what you mean
    * Perception of Self Interest = taking credit, fighting for executive attention, pushing for more money and status; this variable is more about optics

* [The 5 Key People in a SaaS Sales Process](http://tomtunguz.com/five-key-people-in-sales-processes/)
    * There are Proponent, Opponent, Decisionmaker, Other stakeholders, and of course Salesperson.
    * Understanding the relationships in the sales process is key to creating a playbook

### Books
* [The Cluetrain Manifesto](https://www.amazon.com/dp/B06XCF9RLC?tag=jjude-20) by Rick Levin and others
    * I have read many summaries of this book and they had huge impact on the way I looked at work, marketplaces, and future of work. So I decided to read the whole book.
    * Two of the thesis in this book stand-out for me: **Markets are conversation**; **Hyperlinks subvert authority**. I don't think I have grasped the full impact of even these two theses out of 95 theses in the book.

* [Thou shall prosper](https://www.amazon.com/dp/0470485884?tag=jjude-20) by Rabbi Daniel Lapin
    * This is an insightful book but lacks good editing. It is unnecessarily loose, making the book a drag in few places.
    * Rabbi distills centuries of wisdom about money making into ten points:
        1. Believe in the Dignity and the Morality of Business
        2. **Extend the Network** of Your Connectedness to Many People
        3. Get To Know Yourself
        4. Do Not Pursue Perfection
        5. Lead Consistently and Constantly
        6. Constantly **Change the Changeable** While Steadfastly Clinging to the Unchangeable
        7. Learn to **Foretell the Future**
        8. Know Your Money
        9. Act Rich: Give Away 10 Percent of Your After Tax Income
        10. Never Retire

* [The Triumph of Christianity](https://www.amazon.com/dp/0062007696?tag=jjude-20) by Rodney Stark
    * Paras Chopra asked in [Twitter](https://twitter.com/paraschopra/status/969918445071618048), "Is there any book chronicling **the growth hacks that made Christianity #1 religion** in the world?" I got interested in the question and searched for books, which led me to this book. 
    * Stark **overturns lot of common misperceptions** about Christianity's growth. Ex: Constantine embracing christianity diluted the religion rather than strengthening it. Medieval Christians weren't pious as we imagine they were.
    * Luther's "heresy" is called the Reformation because it survived (because winners write history)
    * **Religions fare when there is religious pluralism**. (Every right-wing religious leader, who is trying to annihilate other religions should understand. It is better for their religion that there is religious pluralism) 
    * I listed out few others in my [reply](https://twitter.com/jjude/status/975612222725648384) to Paras.

### Audio Books
* Audacity of Hope by Barack Obama
    * Whenever we exaggerate or demonize, oversimplify or overstate our case, we lose. Whenever we dumb down the debate, we lose.

* [To Sell Is Human](https://www.amazon.com/dp/B00BCCW9BI?tag=jjude-20) by Daniel Pink
    * If your role demands "moving people", you are in sales. That means, all of  us are in sales.
    * New ABCs-- Attunement, Buoyancy, and Clarity
    * Attunement = understanding the context and perspective of the buyer
    * Buoyancy = grit of spirit plus sunny outlook
    * Clarity = distill your offering and simplify what you are offering; give the buyer clear next action to take.

### Tweets

* You get paid linearly for analyzing and solving problems. You get paid non-linearly for spotting and seizing opportunities. [@farnamstreet](https://twitter.com/APompliano/status/960142244815802369)
* Your company will move at the speed of trust. [@kevinbehr](https://twitter.com/kevinbehr/status/956587868427816960)
* What's on the outside sells tickets. What's on the inside wins games. [@EdLatimore](https://twitter.com/EdLatimore/status/964321312725487616)
* We are very good lawyers for our own mistakes, and very good judges for the mistakes of others. - [@paulocoelho](https://twitter.com/paulocoelho/status/973304242910920704)
* Ask not "how are we going to test this?" Ask rather "how are we going to ensure this goes out with the necessary levels of quality?"  - [@flowchainsensei](https://twitter.com/flowchainsensei/status/974003566041030663)
* The quest of a lifetime is to find the context to which you are best suited to thrive and feel alive. Every person has a unique set of genes that are perfectly designed to excel in the right context—if you can only find it.  — [@james_clear](https://twitter.com/james_clear/status/974681672997330945)
* Have you ever considered that your bottleneck is a lack of knowledge not a lack of 'resources'? [@KirstenMinshall](https://twitter.com/KirstenMinshall/status/978946658607255554)

