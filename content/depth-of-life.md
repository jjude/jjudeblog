---
title="Life is more about depth than length"
slug="depth-of-life"
excerpt="Don’t wait for a future time to do all the good. Start today to have a deeper and richer life."
tags=["wins","self"]
type="post"
publish_at="24 Aug 22 18:03 IST"
featured_image="https://cdn.olai.in/jjude/depth-of-life-gen.jpg"
---


I just returned from a memorial service for my uncle. He lived to the age of eighty-six. He joined the merchant navy, at a young age, to support his family. In spite of many hardships, he never developed a grudge against life. Through diligent work, he accumulated wealth, and he used that wealth to help his family members succeed. He was deeply religious. He never missed singing praises about mother Mary.
  
He finished well too. As he breathed his last, he had all his sons, daughters, and grandchildren around him. **In every way, he was a role model**.

During his memorial service, I thought of a classmate who passed away at the age of 45. Most of us felt that he had left us too soon. I have a different view.

Throughout his life, he remained helpful. He helped me speak English fluently. In addition to being a great friend to many of us, he fulfilled his duties as a son, husband, and father. He was a pious Muslim. He took his parents on a trip to Mecca.

Losing him so young was heartbreaking. However, he lived a much richer life than any of us.

**Mourning teaches you a lot about life**. During my walk home from the memorial service, I thought about these two lives and the lessons I could learn from them.

There are more similarities than differences between them.
  
Even though they were deeply religious, their beliefs shaped their hearts to help others rather than to drive social division.
  
They worked diligently and accumulated wealth. After retiring, my uncle continued to teach. Laziness had no place in his life.
  
As they became richer, they built longer tables so their families and friends could feast rather than building higher walls to isolate themselves.
  
They both exemplified John Wesley’s saying:

> Do all the good you can,  
> By all the means you can,  
> In all the ways you can,  
> In all the places you can,  
> At all the times you can,  
> To all the people you can,  
> As long as ever you can.
> - John Wesley

I pray that you have a long life. But **don’t wait for a future time to do all the good** you can. Get started today. Start with your family and friends. Begin with simple help. You will enrich your life and the lives of many others.

## Continue Reading

* [Writing my obituary](/my-obituary/)
* [The dichotomy of contentment and ambition](/contentment-and-ambition/)
* [Build on the rock for the storms are surely coming](/build-on-rock/)
