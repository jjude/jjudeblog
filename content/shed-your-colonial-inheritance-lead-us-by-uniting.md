---
title="Shed your colonial inheritance - lead us by uniting"
slug="shed-your-colonial-inheritance-lead-us-by-uniting"
excerpt="Indians got the British out of India; but the Indian politicians still use their tactics."
tags=["opinion"]
type="post"
publish_at="28 Nov 08 16:30 IST"
featured_image="https://cdn.olai.in/jjude/shed-your-colonial-inheritance-lead-us-by-uniting-gen.jpg"
---

There is politics by unification and then politics by division.

The British ruled India with a 'divide and rule' policy. It kept them in power for decades, until Indians woke up beyond their division - caste, color, religion, language and whatever else it was dividing them.

We got out of their rule; but not out of their tactics.

Our shrewed politicans picked up the tactic.
  > First, the country was divided by religion.
> Then we were divided by languages.
> Then on caste.
> And the tactics of division just goes on.

Every time a crisis unites Indians, politicians come in and stir the divisions.
  > Babri Masjid hurts Hindu sentiments;
> Tamilians should not learn Hindi;
> North Indians can't work in Maharastra;

What is the result? We stand deeply divided.

When the house is divided, terrorists go on a rampage.

Terrorists are not divided. Guns know no division; only our politicians.

Mumbai incident has open up a distinct window of opportunity for our politicians to raise beyond division and to unite Indians. This is an earnest plea to the Modis, Advanis and Rahuls of our political system, to lead us in unity. We still believe in democracy and please don't disappoint us.

