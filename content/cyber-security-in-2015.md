---
title="Will cyber security catch up with rest of the online world in 2015?"
slug="cyber-security-in-2015"
excerpt="Securing online assets is unbelievably cumbersome and inefficient even in 2014. Will it change in 2015?"
tags=["security","dsd"]
type="post"
publish_at="31 Dec 14 21:02 IST"
featured_image="https://cdn.olai.in/jjude/cyber-security-in-2015-gen.jpg"
---

How do you tell your Mom of the fantastic weekend trip to Shimla? You pick up your mobile and call her; if you want a face-to-face talk, you call her on FaceTime. Do you think of picking up a pen and paper to write her a letter? Nah, it doesn't even cross your mind.

What about sending flowers to your lovely sister on her birthday? You browse to 'Fern and Petals' the previous day, and order it from your home in Delhi. A beautiful flower bouquet and a dark chocolate cake is delivered to her in Bangalore. You call her on video chat and see her bright smiling face. Awesome is the word to describe how you feel. Without Internet, lazy brothers would be just awful brothers.

In many aspects **online world is an efficient version of the world before**.

But not security. Securing online assets is unbelievably cumbersome and inefficient even in 2014.

Take the simple example of changing password for wi-fi router. How often do you change this password? Once a month? Once a year? I bet you didn't change it after it was configured first. Why?

When you are bombarded with alerts of every sort, isn't it strange that you never get an alert to change password for your wi-fi?

Even when you attempt to change the password, the option is buried under other options. Look at the actual screen to change password for TP-Link router.

![TP Link](https://cdn.olai.in/jjude/2014-12-tp-link.png)

No wonder many wi-fi routers are left with default passwords, and are vulnerable to hacking by any kid with just a basic knowledge of hacking.

How about **knowing what is happening around** your online assets? While driving your car, you look at rear-view mirror and side-view mirrors to gauge what is happening around you. Now, can you tell me how many devices are connected to your wi-fi? You can't because there is no way to visualise your network. When you can't visualise your wi-fi connectivity, how will you know if a hacker entered your wi-fi? You can't.

As World Economic Forum (WEF) [indicated][1] in its predictions for business technology for 2015, the "**current approaches to security aren't cutting**." We need a paradigm shift in designing security products. In fact, security should be so ingrained within products that we shouldn't even feel we are doing something extra to secure our assets. After all, when you press your car-remote to lock your car, you are not thinking of security. That behaviour has become part of driving experience.

![WEF Prediction](https://cdn.olai.in/jjude/2014-12-wef-prediction.png)

Referring back to WEF prediction, such paradigm shift can come only from startups. At [Digital Self Defense][2], a company I co-founded, we are redefining the core principles behind security products.

![DSD Core Principles](https://cdn.olai.in/jjude/2014-12-dsd-coreprinciples.png)

Our product will give you the capability to visualize, monitor and operate your network from your smartphone, anytime, anywhere. We bring the efficiency found in other online operations into security. We bring in your pocket, a remote for your network.

[1]: https://agenda.weforum.org/2014/12/7-predictions-for-business-tech-in-2015/
[2]: http://www.dsdinfosec.com

