---
title="Getting ready for AI economy"
slug="ai-economy-getting-ready"
excerpt="Will you join me in this journey?"
tags=["tech","aieconomy","gwradio"]
type="post"
publish_at="23 May 23 14:52 IST"
featured_image=""
---

<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless src="https://share.transistor.fm/e/bef2c646"></iframe>

We heard about AI for years. We used to hear news like, "AI can to play chess", "Deep blue computer defeated Garry Kasparov", and so on. All these while, AI seemed like a technology only for researchers and the companies with big pockets.

Generative AI in general, and chatGPT in particular changed that scenario overnight. They brought AI into our every day lives and **handed us tools that we can all use**. It is time to pay attention, whether you are a CEO, developer, or a marketer.

I have used a framework to learn new topics and I'm using to learn about AI. I call the framework: consume, produce, and engage.

In the consume phase, I learn about the topic from books, webinars, courses, videos, and other sources. We retain only 30 - 40 percent of what we consume.

To retain more, we have to produce something with the information we consumed. It could be a blog post, webinar, podcast, or even a product.

Even with consume and produce, we might get only one side of the domain. When we share what we created and seek feedback from others who are also in the same journey, we form a well-rounded perspective.

I'm following the same for learning about AI.

I am reading books like, "2062, The World that AI made" and "Power and prediction". I'm listening to podcasts like "Pondering AI"

I'm also **experimenting with chatGPT** for creating landing pages, writing blog posts, and also generating table of contents for documents I've to create. Some results are better than others. But with every iteration, I'm learning.

I'll **share every insights in public** - via podcast, newsletter, and LinkedIn articles. If you are interested, please subscribe to these. But don't stop with consuming. Reply back with your comments and insights too.

Exciting times are ahead. Happy to be a student again.

_This is part of [Short notes on AI Economy](/ai-economy-notes/)_