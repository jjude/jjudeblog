---
title="AI Factory of the future"
slug="ai-future-factory"
excerpt=""
tags=["tech","aieconomy","gwradio"]
type="post"
publish_at="19 Jul 23 12:44 IST"
featured_image=""
---
<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless src="https://share.transistor.fm/e/f00c47d8"></iframe>

In the '70s, leadership expert Warren Bennis predicted a future factory staffed by just a man and a dog. The man's job is to feed the dog. The dog's role is to stop the man from touching machines. This vision never materialized.

Now, with generative AI on the rise, I hear similar forecasts. These AI models can create movies, music, images – pushing humans to the sidelines of creative work. They suggest our only task will be supervising these LLM models.

I disagree for five reasons:
- Humans adapt
- Humans are irreplaceable
- Troubleshooting real-world problems need humans
- Building an all-AI factory would be costly
- These models are inhaling their own exhaust

Firstly - we adapt. When Steve Jobs introduced a phone without physical keyboard many were skeptical but he said "they'll learn" and we did! We've adapted to touch screens as if they've always been there; likewise we'll integrate AI into our daily lives seamlessly. We will find new jobs or creative ways of doing current jobs.

Secondly - humans are irreplaceable. Despite AI's capabilities (including self-correction), exceptions occur needing problem-solving skills and emotional intelligence unique to us humans.

Third point: troubleshooting real-world problems is human territory! Like previous technologies (outsourcing, internet automation etc.), AI elevates us to new thinking levels and might even debug digital issues but when something fails in physical reality or at its intersection with digital world – that’s where human understanding comes in!

Fourth - building an all-AI factory would be costly! Training LLM models is expensive requiring huge unbiased data sets; some businesses may afford it but many will still need human workforce.

Lastly - These generative AI models are trained on public data. As more and AI models follow this path, most valuable data will be put behind paywalls thus reducing the quality of available data for training these AI models. We are already hearing news that these models are starting to inhale their own exhaust, meaning the AI models are getting trained on other AI generated data and thus their quality of answers are reducing.

When you combine all these factors, we can be sure the AI factory of the future will never materialize. Our future will be more like a dance between humans and AIs instead of AI replacing humans.

I hope you enjoyed this episode. If so, can you please share the episode with others? And also send me an email with your feedback? It helps me to improve and evolve.

Thank you for reading. Have a life of WINS. 

_This is part of [Short notes on AI Economy](/ai-economy-notes/)_

## Continue Reading

- [The skills you need in the age of AI](/skills-ai-era/)
- [Why And How I'm Homeschooling My Kids](/why-homeschool/)
- [Liji Thomas on 'Conversational Bots'](/lijit/)