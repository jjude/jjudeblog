---
title="Generative AI is starting on a wrong note"
slug="gpt-illegal"
excerpt="Three ways how chatgpt is starting on a wrong note"
tags=["tech","aieconomy","gwradio"]
type="post"
publish_at="25 May 23 16:17 IST"
featured_image=""
---

<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless src="https://share.transistor.fm/e/d546804b"></iframe>

As a CTO, I'm more than excited about AI and generative AI. But there are things that are unfair too. And we need to talk about it.

There are three things that I wish was different.
- Generative AI models have sucked up all the free content available online, without giving back equally to the ecosystem
- Generative AI models generate harmful instructions and wrong answers. These could turn dangerous for the untrained, unsuspecting masses
- Generative AI models can put an end to the open ecosystem that it is built on

Let us go one by one.

## 1. Generative models are parasites

Large language models, like chatGPT, need to be trained on billions of text input. So they are trained on available free content like wikipedia, github, and so many other similar sites. The creators of these repositories of input texts are not paid. They probably have no idea how much of their work is used by these models.

We have gotten used capitalistic systems that exploit others. Yet still it is unfair.

## 2. ChatGPT can produce harmful instructions

If you read the FAQs on the chatgpt site, it clearly says that it can give out wrong and harmful instructions. 

We have normalised fake news. Chatgpt and it’s cousins are going to produce such fake text, images, and videos in bulk. Lot of us won’t be able to identify these fake content and will fall victim scams using these content. As a father I am also worried about the environment in which my sons will have to grow up and deal with. 

## 3. ChatGPT can dry up the well it is drinking

As I said before chatgpt is trained on the available corpus of code and text. Today when programmers have a question they ask openly on stack overflow and answers are also visible openly. Going forward all those questions will be asked to chatgpt. Both questions and answers will remain hidden from the public eye. Only the chatgpt and the person asking the question will benefit. This eventually might dry up the well from which chatgpt is drinking.

Technology is moving faster than regulations can catch up. So while we reap the benefits of generative models, we should also fight against the unfair practices. 

Do you agree with these points? Do you have any comments? Feel free to send them to me. 

Thank you for reading. Have a life of WINS. 

What else would you like to cover under the topic - "AI economy"?


_This is part of [Short notes on AI Economy](/ai-economy-notes/)_