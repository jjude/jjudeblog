---
title="Are You Unlearning?"
slug="unlearn"
excerpt="Most learning is true within its context. When that context changes, are you prepared to unlearn?"
tags=["insights","sdl"]
type="post"
publish_at="18 Aug 10 16:30 IST"
featured_image="https://cdn.olai.in/jjude/unlearn-gen.jpg"
---

When I started learning '[Event Based Programming](https://en.wikipedia.org/wiki/Event-driven_programming)', I had to unlearn the techniques of traditional sequential programming. I couldn't learn event based programming with the mindset of sequential programming. That was my first explicit unlearning experience.

Learning is expensive and time consuming. But many learning is within a context of business, society, and technology. When these context changes, that learning should be unlearned too (and new ones should be learned). Even when the context changes, we continue to hold on to what we learned.

Take for instance, women's place in Indian societies. Indian women have gone to space, ruled (and ruling) the country, head companies but still Indian families don't unlearn their traditions of 'girls should stay home and be subjected to men.'

Or take the obsession of the developed nations with protectionism. They are still entrenched with 'west supremacy' that they learned ages ago. When will they unlearn it?

Or the teaching of the gurus of process and frameworks. They continue to teach that by establishing processes, people can be replaced easily. When will they unlearn 'people can be replaced' and learn 'processes can be replaced; but not people.'

*When we learn something and seen it working, we can be attached to it long after it is useful. We need to constantly evaluate and validate what we learned about us, business, society, nation and universe as a whole. If any of our past learning is no more relevant, we should be willing to unlearn.*

Visa founder Dee Hock said it best: "**The problem is never how to get new, innovative thoughts into your mind, but how to get the old ones out.**"

**Are you struggling to get the old ideas out?**

