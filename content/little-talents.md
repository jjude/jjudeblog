---
title="Little Talents"
slug="little-talents"
excerpt="If you are faithful in small things, you will become master of great many things"
tags=["coach","faith"]
type="post"
publish_at="26 Dec 16 12:46 IST"
featured_image="https://cdn.olai.in/jjude/2016-12-25-me-preaching.png"
---
Yesterday I spoke in a Christmas convention of a local church. I spoke about the parable of talents, one of my favorite passages in the Bible.

![me preaching](https://cdn.olai.in/jjude/2016-12-25-me-preaching.png "me preaching")

The parable goes like this:

> A business master is about to go on a long journey. He calls three of his servants and gives them talents (worth a year's wages).
> To the first one, he gives five talents; to the second, he gives three; and to the last one talent.
> The first one trades and earns five more; the second also doubles his share; but the third buries his talent and does nothing with it.
> When the master returns, he commends the first two. He condemns the third and takes the talent from him and gives to the first one.

They key point I focused is [this](http://biblehub.com/nlt/matthew/25-21.htm):

> If you are faithful in small things, you will be made master of greater things.

Though talent in these Bible verses mean money, it has been an accepted tradition to consider it as any gifts or resources given to a person.

Most of us compare ourselves with multi-talented classmates and colleagues and conclude that we don't have good enough talents. I have realized, over many years of observing others and myself, that it is a psychological hole most of us fall into. If we introspect, we will find out we do have talents, if not multiple, at least one.

**When we are faithful in using those insignificant, little talents, God multiplies**. He is a God of increase.

I have seen this work in professional life too. Not just in mine, but in those that I admire too.

When I entered college, I couldn't frame a single sentence in English. I would look at my classmates and feel dejected that they all can speak so eloquently, but I couldn't make a single sentence.

But I had a talent. I could think logically and express that well in Tamil. So encouraged by friends (Thank you [Arun](/words-that-changed-my-life/) & Bro. Jebaraj), I started speaking in English in small prayer gatherings. I embarrassed myself repeatedly. I wanted to quit at every one of those embarrassing moments. Yet, I kept at it.

Twenty years later, I have this blog, I speak in front of large professional gatherings, and I have written a [book](/ionicbook/). All in English. I wouldn't have achieved any of these if I had not faithfully exercised that one little talent.

It is not just me.

[Sivers](https://sivers.org) started as a singer singing _the pig song_ in circus before founding the CD Baby and eventually becoming a millionaire.

[Justin Jackson](https://justinjackson.ca/) recently published [This is what starting small looks like](https://justinjackson.ca/rockstar/). He wrote:

> Want to write a book?
> Start small: write an essay.
>
> Want to be a rock star?
> Start small: sing in the shower.
>
> Want to speak at a conference?
> Start small: give a talk at a meetup.
>
> Want to make $1 million in sales?
> Start small: sell a $10 item to one customer.
>
> Want to create a podcast?
> Start small: record a voice memo on your phone.
>
> Want 1,000 true fans?
> Start small: do something that gets you one true fan.
>
> Start small.
> Show up every day.
> Keep refining your craft.
> Take risks and get uncomfortable.

Having followed him for the past few years, I can say with confidence that this blog post reflects his experience.

I can go on quoting ton of other examples. But the point is this:

Figure out your talent.
Start small.
Don't despise that small beginning.
Keep at it, even through embarrassments and failures.
You will achieve success.

## Continue Reading
- [Can Companies Learn From Religions?](https://jjude.com/can-companies-learn-from-religions/)
- [If Church Can Reinvent Itself, Why Can't Companies](https://jjude.com/church-reinvents/)
- [What are your mantras for life?](https://jjude.com/life-mantras/)
