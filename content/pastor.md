---
title="Think clearly about your venture with P.A.S.T.O.R. Framework"
slug="pastor"
excerpt="Get clarity before you launch your new venture"
tags=["startup","coach","wins","frameworks"]
type="post"
publish_at="27 Mar 21 07:06 IST"
featured_image="https://cdn.olai.in/jjude/pastor.jpg"
---

I use the PASTOR framework to structure my thinking whenever I want to launch a new coaching program, consulting service, or product. Nowadays, I even use it to evaluate the need for a new newsletter.

![PASTOR Framework for startups](https://cdn.olai.in/jjude/pastor.jpg)

### P is for problem

What is the problem you aim to solve with the venture? Is the problem an acute one so people will take action on their own, like taking medicine for their toothache? Or should you have to motivate them every day, much like taking a vitamin pill?

If you articulate the problem well, people will nod along as you explain the problem. If they agree with how you state the problem, they will also willingly try the solution you are proposing for that problem.

### A is for audience

Who experiences the problem? Is it a large enough cohort today, and will it continue to grow as you grow? If it is not large enough today, the venture will not be profitable. If it does not grow continually, the venture will soon hit a plateau.

While the total size of the audience matter, in the long run, the immediately addressable size matter when you start. The immediately addressable audience determines if your venture takes off or not. If they love it, they will talk about it to others spreading the word about your offer. They will form the wind that carries the fire far and wide.

### S is for scenarios

You have to understand the scenarios under which your audience experiences the pain. Is it multiple times a day, is it when they get ready to go to the office, is it when they want to purchase a product, and so on. Such an exercise should tell you the frequency and the severity of the pain. These two factors – frequency and severity, will guide you to decide how much you can charge them for the solution.

### T is for transformation

Your solution should not just improve their life but transform them. Your customers should shout aloud, saying, “I wasn’t able to follow my passion for jogging because of wheezing, but now after using this inhaler, I can jog daily. I am so happy.”

### O is for offer

Offer is your answer to the pain your potential customers suffer. Traditionally it was either a product or a service. But when you launch your startups, you will have to add “experience” to the type of offerings. You can offer an instant coffee powder, or filter coffee, or an exquisite dining ambiance created especially for coffee serving. If you add experience to your offering, you can charge a higher premium. When I think of experience, I admire [Auto Anna of Chennai][1]. He has transformed banal auto rides in Chennai into a memorable experience.

### R is for reward

All the above points are about creating value for the set of customers you identified. This step is about capturing part of that value. What is your reward for doing all that for your customers? What kind of revenue are you going to have?

Once you have clarity in your mind, you can use [30 percent feedback](/30-percent/) to validate your thinking and to get feedback on the early prototype of the solution.

_I borrowed the PASTOR acronym from the copy-writing legend [Ray Edwards](https://rayedwards.com/031/)_


## Continue Reading

- [Documenting Your Decisions](/documenting-your-decisions/)
- [Building In Public, Jeff Bezos Style](/build-in-public-bezos/)
- [Approximately correct](/approximate/)
- [One decision instead of hundred decisions](/lead-decisions/)
- [Talk Their Language](/talk-their-language/)
- [Workshop on Lean Canvas for TiE Young Entrepreneurs, Chandigarh](/lean-canvas-tie/)

[1]: http://www.amazingauto.in/