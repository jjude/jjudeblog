---
title="Authors who shaped my thinking before I turned 18"
slug="authors-impact"
excerpt="What you read when you are young has more impact than you imagine"
tags=["wins","insights","sdl"]
type="post"
publish_at="02 Sep 20 08:00 IST"
featured_image="https://cdn.olai.in/jjude/tamil-authors.jpg"
---

My mom instilled in me a love of reading. Before I turned 18, I had read Mahabaratha and Ramayana - the two great Indian epics, many short stories, and other historical novels.

We didn't have a TV or an iPad or a movie theater in our town, so I was mostly in the library. Our neighbors knew I would either be at home or in the library during the weekends. Such was my passion for reading.

I read what was available in the library, which was mostly around Tamil philosophy and history.

Ellen Fishbein, a writing coach, and a poet reminded me of these when she asked me a question [on Twitter](https://twitter.com/EllenRhymes/status/1282068421421740037): 
> I'm curious - If you had to choose only one, which book had the most profound impact on you BEFORE you turned 18?

Though Ellen asked about a book, I recollected the authors who influenced me the most. Three Tamil authors had the most impact when I grew up - [Valluvar](https://en.wikipedia.org/wiki/Thiruvalluvar), [Bharathiyar](https://en.wikipedia.org/wiki/Subramania_Bharati), and [Kannadhasan](https://en.wikipedia.org/wiki/Kannadasan). 

![Authors who impacted me](https://cdn.olai.in/jjude/tamil-authors.jpg)

### Valluvar

Of the three authors, Valluvar had the most impact. He wrote [Thirukural](https://en.wikipedia.org/wiki/Tirukku%E1%B9%9Ba%E1%B8%B7), ancient literature dated any time between 300 BCE to 5th century CE. 

Let me give you a few couplets from this marvelous book:

The most I use(d) is the [475th couplet](https://thirukkural.gokulnath.com/#/thirukkuralchapters/48/thirukkurals):

> When a cart is loaded full, adding even one more peacock feather will break its axle. 

**When I pack my day to the full, my son's tiny interruption will trigger anxiety, and I'll lose temper**. Instead, I focus on essential tasks for the day, and even then, I stuff enough buffer between tasks so I can go about my day relaxed.

_Read [If you see everything, you'll get nothing 🎯](/focus-to-win) to find out how I identify important tasks_

[Couplet 69](https://thirukkural.gokulnath.com/#/thirukkuralchapters/7/thirukkurals):

> The mother who hears her son called "a wise man" will rejoice more than she did at his birth

I want my mom to rejoice often. So I **built my life with wisdom and reputation as foundation stones**.

[Couplet 396](https://thirukkural.gokulnath.com/#/thirukkuralchapters/40/thirukkurals):

> Water will flow from a well in proportion to the depth to which it is dug, and knowledge will flow from a man in proportion to his learning 

**The deeper I learn a domain, the more capital I've to bank on**. I formulated a [learning framework](/sdl/), to pursue multi-disciplinary learning in life.

(The answer to Ellen's question then is, Thirukural)

### Kannadasan

[Kannadasan](https://en.wikipedia.org/wiki/Kannadasan) is another author who influenced my thinking. He is well-known for his film songs and rightfully so. Here is one couplet from his countless movie songs.

> பரமசிவன் கழுத்தில் இருந்து பாம்பு கேட்டது    
கருடா சௌக்கியமா    
யாரும் இருக்கும் இடத்தில் இருந்து கொண்டால்   
எல்லாம் சௌக்கியமே.. கருடன் சொன்னது.. 

In the Hindu mythology, a snake adorns Lord Siva's neck. From its safety, the snake prods the well-being of its predator, the eagle. The eagle replies, **if one stays in a safe place, it is all well and good**. I appreciate this song even more as the world is battling the COVID pandemic. I'm thankful that I'm safe in every way - psychological safety with family, financial safety with a stable income, and physical safety with a large house. **If you live long enough, work long enough, or run a business long enough, you'll encounter predators**. Knowing this, I've [built my life on a rock](/build-on-rock/).

His masterpiece was not his movie songs, but a treatise on Hinduism called [Arthamulla Indhu Madham](https://amzn.to/3mN6VMg)( Meaningful Hinduism). He explores the meaning of traditions of this ancient religion, at least as practiced in Tamil Nadu. I remember many of the traditions he unraveled, but here I recollect just two:

> On top of every Hindu temple, there is an inverted pot, called [Kalasam](https://en.wikipedia.org/wiki/Kalasam), which are filled with food grains. In times of floods or disaster, the grains are planted to restart agriculture. 

His point is that religious traditions are not out of illiteracy, as portrayed usually in the media. Instead, they are **pragmatic to the continuation of the society**. 

You don't have to embrace every tradition as is as illustrated in this tradition:

> Early Tamilians served food on banana leaves. Before eating, they will take a cup of water, sprinkle drops of water around the leaf and drink the remaining water. The practice was, as per the traditional belief, to seek the blessings of deities. 

Kannadasan analyses this habit in detail. In the olden days, the floors were of mud which attracted insects. The sprinkled water kept these insects from wandering into food. This practice was incredibly helpful in the night as a source of lights wasn't available for everyone. What about drinking the remaining water? Drinking water before eating prevents hiccups.

As we have moved to the dining tables with enough lights around us, we don't face the dangers of insects marching into our food plates. But, we still face risks of hiccups. So we should let go of one and keep the other.

Instead of cynically dismissing traditions, analyze and adopt the suitable ones. Since reading Kannadasan's book, I have studied the fittest ideas that survived for centuries, embraced a few of them, and greatly [benefited](/approximate/) from them.

### Bharathiyar

[Bharathiyar](https://en.wikipedia.org/wiki/Subramania_Bharati) was a **revolutionary** not only because he fought against the British but also **against the evils of the society**.

He wrote against **caste discrimination**:

> சாதிகள் இல்லையடி பாப்பா!-குலத்   
தாழ்ச்சி உயர்ச்சி சொல்லல் பாவம்;

> There is no caste
It is a sin to divide people on caste basis

He was far ahead of his time to believe women as equal to men. He called **women to modernize and break the shackles of the society**.

> ஆணும் பெண்ணும் நிகரெனக் கொள்வதால்   
அறிவி லோங்கி,இவ் வையம் தழைக்குமாம்;

> If the world considers men and women as equal
it will be filled with wisdom and thrive

He also wrote against poverty and religious divisions.

I reserved the best for the last.

> யாமறிந்த மொழிகளிலே தமிழ்மொழி போல் இனிதாவது எங்கும் காணோம்

> Among all the languages we know, we do not see anywhere, any as sweet as Tamil

I am not considering this as his best because I'm a Tamilian. I regard this as his best because he wrote this after becoming well-versed in Sanskrit, Hindi, Telugu, English, and French. And he didn't demonize the other side to sing praises of his side.

In today's polarised bigoted world, everyone seems to take a "position" without ever finding an iota of information about the other side.

"Vim is the best editor."    
"Have you used any other editor?"    
"No"   
🤦


"In 5000 years of history, India never invaded any other country."    
"Guptas invaded up to China; Cholas invaded up to Indonesia."    
"You are an anti-national"    
🤦

"Non-veg is the reason for climate change."    
"Data shows, coffee and chocolate impact climate more than fish and poultry."    
"Nobody is interested in your data."   
🤦


Like Bharathiyar, if we **open our eyes and learn widely** maybe [we will be solutions](/be-a-solution/) rather than problems.
### Thank you, Ma

[Reading](/books-read/) is the leverage I have in my life. When my mother paid five rupees as a library fee every month, I didn't grasp the enormous impact that a small amount would have on my life. That was probably the best thing that happened in my life. Oh, wait. My mom was the best thing in my life. I don't intend to repay her. I can't.

And thank you [Ellen](https://ellenrhymes.com/) for triggering a nostalgic journey into the magical past.
### Read Next

* [If you see everything, you'll get nothing 🎯](/focus-to-win/)     
* [My Learning Framework - Consume, Produce, Engage](/sdl/)    
* [If you are not part of the solution, you are a problem](/be-a-solution/)
