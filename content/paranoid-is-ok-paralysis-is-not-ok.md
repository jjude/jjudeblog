---
title="Paranoid is ok; Paralysis is not ok"
slug="paranoid-is-ok-paralysis-is-not-ok"
excerpt="As I build my new company, I will find myself in novel situations. I may become uncertain, nervous and sweaty. Yet, never should I become paralysed. It will destroy everything I build."
tags=["coach","startup","insights"]
type="post"
publish_at="17 Sep 14 16:30 IST"
featured_image="https://cdn.olai.in/jjude/paranoid-is-ok-paralysis-is-not-ok-gen.jpg"
---

I am staring at a blank screen. I have been typing and deleting for the past half hour.

I'm typing an email to the CEO of a big IT company seeking an appointment. A mutual friend introduced us via email. The CEO agreed to meet me, a first-time entrepreneur, and if everything goes well, will mentor me in my venture.

My fear is not without reasons. I'm nobody — not a MBA, nor a graduate of an elite college, nor have string of success stories on my sleeves; and he is a CEO of a multi-million dollar company. A terrified kid is looking at a giant.

If that is not enough, I'm a South-Indian in a North-Indian territory. It wouldn't matter if I were to meet him on equal terms for business. To meet to seek advice is to invite condescending talk.

To top it all, my friend had told me that my English needs to improve. I know it is an advice with good intentions. But now, those words are playing havoc in my mind blocking everything.

Every time, I typed a sentence, I would imagine how the CEO would judge me.

Here is how it went so far.

Dear Sir,

Dear Sir? That is how a student addresses a principal; Grow up man.

Dear Mr. so-and-so

hmm...it is better but it sounds too formal. How would, "Hi" sound? No, it is too informal. How about, "Dear so-and-so"? Better. I know "ji" is a respect word in North India. May be I should add it?

Dear so-and-so ji,

Now that I got the addressing written, let me go on.

Should I thank him for meeting me? Would it come off as seeing myself too low? Should I sound equal to him? But I'm NOT equal to him.

The entire thought process was exhausting. May be I should abandon the whole idea of starting a venture and go back to consulting. [I was good at it][2]. Isn't it?

Once I looked back, my mind froze. I couldn't get any word out. I stared at the screen helplessly.

After some unfruitful minuets, I got up, went to the backyard, stretched myself and strolled. The fresh air and the physical act relaxed me.

How do I know he will be condescending? If he is a stereotypical north-indian, he wouldn't have agreed to meet me, in the first place. Why should I be prejudicial that he will be prejudicial?

[May be my fears are imaginary][1].

Suddenly, I felt light.

Words flowed fast. I rehearsed what I'm going to type.

>Dear so-and-so ji,
>Thank you for agreeing to meet an younger entrepreneur. Appreciate your generosity.

>Would 4 pm on Monday or Tuesday work well for you? If these two times don't suit your schedule, can you kindly inform an alternate time?

Pff....That wasn't so bad.

I was paranoid. That's ok. This was a new situation for me. But I can't turn hopeless and helpless.

As I build my new company, I am sure, I will find myself in novel situations from product building to sales to deal making. I may become uncertain, nervous and sweaty. Yet, never should I become paralysed. It will destroy everything I build.

[1]: /fears-are-imaginary/
[2]: /an-award-to-end-the-year/

