---
title="Notes from day 2 of TiECon Chandigarh 2017"
slug="tiecon17-02"
excerpt="TiECon Chandigarh 2017 is a two-day conference focused on the theme of 'Entrepreneurship in times of change'. These are my notes from Day 2."
tags=["coach","startup","sdl","tiechd"]
type="post"
publish_at="19 Feb 17 03:29 IST"
featured_image="https://cdn.olai.in/jjude/2017-tiecon2.jpg"
---
TiECon Chandigarh 2017 is a two-day conference focused on the theme of "**Entrepreneurship in times of change**". The conference brought together a range of speakers from local and foreign governments (Canada, Israel, UK, and India), academia, angel investing, venture funding, mega-corporates and of course from startups. It is a confluence of ideas.

These are my notes from the second day of the conference. As you read remember that, I didn't attend all sessions and this is not a review of the sessions. These are just _”notes to self"_ that I'm sharing.

_I also posted my notes from the first day. [Read them](/tiecon17-01/) for continuity._

Sandeep Goyal, Chairman of Mogae Media shared his insights on entrepreneurship under the title, "Winsights". He shared about 12 points. I'm listing those that resonated with me:

- **Never under-estimate your chances of winning**. He recounted how Sunil Mittal, an unknown name in the early days, went against the well established companies like Tata and Essar with focus and built a successful telecom company.
- **Bring together disparate elements for your success**. Like how Steve Jobs brought together design, marketing, and technology to build and market products after products.
- **Let no one tell you or talk you out of your dream**. [Sanjeev Kapoor](https://en.wikipedia.org/wiki/Sanjeev_Kapoor) won Padma Shri, the fourth highest civilian award in India. But think about 30 years back. When everyone was choosing IT and other high paying careers, he chose cooking as his profession. He pursued his dream to become a highly acclaimed chef in India.
- You miss 100% of the chances you don't take.
- **You don't win silver. You lose gold.**
- Know thyself. Are you the personality who can go out and bring business? Or are you the personality who silently builds a company? **Are you a conquerer or a farmer?**
- Some of us are content to look at telescope and happy to look at the moon (**astronomers**), but there are some who want to go to the moon (**astronauts**). How far do you want to go?
- Have a "Konjo", a fighting spirit.
- **When was the last time you did something for the first time?** Something esoteric for the first time? Keep attempting something for the first time. It keeps you curious.

![TiECon 2017](https://cdn.olai.in/jjude/2017-tiecon2.jpg)

The next session was about 'Building for the world'. It was a panel discussion. I could listen only to [Rajendran Dandapani](https://www.linkedin.com/in/rajendran), director of Zoho. He sounded more like a philosopher than an engineer (yet, the ideas resonated with me).

- Treat life, company, and product as **fractal puzzles**. Iterate over previous state and come up with something better.
- **Covet interestingness** in the journey than end goals.
- Keep developing what you like. In one of the iteration, it will click as a product (_May be I should re-start developing [Olai](http://olai.in), the static blog engine, that I dropped_).
- Treat milestones as a **stepping stone for something more interesting**.

Raman Roy, the pioneer of BPO industry in India, shared his insights coming out of building successive successful companies. Primary ideas are:

- There are different models for business. **Some are disruptive, many are improvements**. Not every day is an iPhone launch day. Take an existing product and improve it. Improve its pricing model, take it to another geography, change its quality and take it to a different customer segment, and so on.
- Are you a cost + model or a value - model? They both have its place in doing business.
- If you think of new ideas, you are imaginative. If you implement, only then you become creative. If you monetize, only then you are a businessman.

[Sumit Sood](https://www.linkedin.com/in/sumitsood1) of Global Logic and [Sai Narayan](https://www.linkedin.com/in/sai-narayan-9485742) of Policy Bazaar talked about **New age marketing**. It was more of talking about the new age and the need for rethinking the marketing rather than marketing for new age.

- Since world war II, the world has been in transition. The path has been: **rebuild -> secure -> explore -> express**. In the begining, it was about rebuilding the world. Then it was about securing our lives. Once we secured our lives, we explored. Now it is about expressing oneself and experiencing life. Marketing has to change accordingly.
- AI and other technologies will change consumer's idea of value addition. Consumers will take certain level of product quality and service as granted.
- We used to buy for relevance. Then purchases were for comfort. Now it is all lifestyle purchases.
- In the digital world, **path to purchase is not linear**.

TiECon 2016 was the foundation. TiECon'17 built on top of it. It was well organized. It didn't look like a conference in a II-Tier city. I'm sure there was lot of background work that made these two days smooth and fantastic.

I've only one suggestion for the next conference. There should be a hackathon.
