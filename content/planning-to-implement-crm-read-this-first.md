---
title="Planning To Implement CRM? Read This e-Book First"
slug="planning-to-implement-crm-read-this-first"
excerpt="A free guide to successful CRM implementation."
tags=["others"]
type="post"
publish_at="06 Jul 10 16:30 IST"
featured_image="https://cdn.olai.in/jjude/planning-to-implement-crm-read-this-first-gen.jpg"
---

There is too much marketing hype surrounding the Packaged Software Space including CRM. However, the success rate of CRM implementation doesn’t match that hype. Now a [veteran](http://mareeba.blogspot.com/ "CRM ebook") in CRM implementation has written an insider guide for successful CRM implementation.

Notable aspects of the e-book are:
+  Its a simple download. No need to register, no need for email; just point, click and download
+ No marketing pitch inside the e-book
+ Concise content on CRM implementation from conceptualization to rollout

Top five lessons that stand out are:
+ If management team does not use CRM system, its going to die sooner or later
+ Pilot project (Phase I) should be small enough to be manageable and large enough to be meaningful
+ If reports are not designed in the Phase I, then CRM system will be used mere as data capture system and continue to be so
+ T & M billing is unethical to clients
+ CRM packages can be objectively evaluated, by developing and restricting product demos to demo scenarios

Since author is proposing to revise the content, here are few suggestions:
1) A visual (like in his other e-books) will aid to summarize the concepts.
2) Functional test plans should be prepared immediately after the requirement (or during the requirement) phase
3) If there is a need to conduct gap analysis (which is almost always), it is better to engage independent consultants rather than from the vendor (or their partners) so as to have an unbiased opinion

Go grab the [e-book](http://www.mareeba.co.uk//buying-CRM-software-ebook/).

