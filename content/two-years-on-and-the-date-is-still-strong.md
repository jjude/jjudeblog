---
title="Two Years On; And The Date Is Still Strong"
slug="two-years-on-and-the-date-is-still-strong"
excerpt="None"
tags=[""]
type="post"
publish_at="05 Jun 11 16:30 IST"
featured_image="https://cdn.olai.in/jjude/mac-apps.png"
---
Owning a Mac was a long cherished dream of mine. Like many other dreams, it was beyond my reach. But dreams do come true.

As soon as I became an independent consultant, I bought myself a [MacBook Laptop](/a-byte-of-apple/). Yes it was expensive, but that is one of few purchases that I am not sorry about.

Reasons?

Due to nature of my assignments, I had to use my laptop only on an open network. But so far, I didn’t have to worry about virus creeping to my laptop. I freely use thumb-drives for sharing files with all others Ms-Windows users without any care or concern. I do have an anti-virus installed; yet the **assurance of safety** in itself is a great relief.

Secondly, in these many days, I haven’t wasted an hour of downtime due to system crash, failed updates or hanging system for thousand and one reasons. **Efficiency** is implicitly expected out of consultants and being an IT consultant, I am glad that I never had to say, “please give me one more day since I have to re-install my computer” (though I had to hear that from many of my colleagues).

![MacOS Applications Screenshot](https://cdn.olai.in/jjude/mac-apps.png "Screenshot of My MacBook Desktop")

Another advantage of Mac is the **pleasure of using better designed products**. I can’t comment if this differentiation is brought forth by designers of applications or MacOS itself somehow mandates a pleasing UI and a well functioning design; in any case, Mac users benefit from such application ecosystem. One such feature that is ‘full screen editing’ by which you are presented a full screen canvas with minimalist tools, thus helping you to concentrate on creating the content rather than being diverted by umpteen number of tiny icons scattered all over the screen. I am not saying all Mac users are highly productive - I can’t even say that about myself, but these applications facilitate productivity and then gets out of your way.

Is it all only admiration? Of course not. I am not a Mac zealot to be blinded. There are some **black-spots** too in Mac.

Foremost is the **lack of wide spread customer service centres**, at least in India. There are only handful of service centres, (though they were extremely helpful), and then there are absolutely none in tier-2 cities.

And then there is this, common complaint that anything to do with Mac is very **expensive**. There is definitely a premium to be paid for all the features in Mac, be it hardware, MacOS applications or third-party applications. They are more expensive than their counterparts in Windows machines.

But these gripes are small compared to what I get out of owning a Mac.

I will continue to enjoy the pleasure of being with a Mac.
