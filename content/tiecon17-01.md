---
title="Notes from day 1 of TiECon Chandigarh 2017"
slug="tiecon17-01"
excerpt="TiECon Chandigarh 2017 is a two-day conference focused on the theme of 'Entrepreneurship in times of change'. These are my notes from Day 1."
tags=["coach","startup","sdl","tiechd"]
type="post"
publish_at="18 Feb 17 05:09 IST"
featured_image="https://cdn.olai.in/jjude/2017-tiecon.jpg"
---
TiECon Chandigarh 2017 is a two-day conference focused on the theme of "**Entrepreneurship in times of change**". The conference brings together a range of speakers from local and foreign governments (Canada, Israel, UK, and India), academia, angel investing, venture funding, mega-corporates and of course from startups. It is a confluence of ideas.

These are my notes from the first day of the conference. As you read remember that, I didn't attend all sessions and this is not a review of the sessions. These are _"notes to self"_ that I'm sharing.

I.S. Paul, MD of Drish Shoes inaugurated the sessions. He rightly pointed out that if Chandigarh has to attract investments, it needs to **build an ecosystem, and focus on a specific sector**. He mentioned, "**big data**" could be that sector.

_[As Alec Ross, advisor of Hillary Clinton mentions in his book "Industries of the future", big data and analytics will drive innovation in diverse fields like agriculture, health-care and so on. So I believe that would be a right choice to focus. If you are interested, read my [review](/future-industries/) of the book]._

![Mahesh and Mahavir at TiECon'17](https://cdn.olai.in/jjude/2017-tiecon.jpg "Mahesh and Mahavir at TiECon'17")

The next session was a discussion between [Mahesh Murthy](https://twitter.com/maheshmurthy) and Mahavir Sharma. Mahesh is known for his controversial but insightful comments on the big-boys of Indian startup ecosystem. In the euphoria of chasing ideas, entrepreneurs ignore essential factors in building a company. The essential factors are worth repeating. Some of the ideas he repeated are:

- Don't start a x for India. India is not China. In China, there is government protection to local businesses. So there is google of China. But google for India is Google itself, amazon for India is Amazon itself.
- Look for local problems to solve.
- There is still gold, but there is no gold-rush.
- Create value; don't chase valuation.
- AI may be a tool to create a moat; don't think AI can solve problems at a fundamental level.

[Arvind Gupta](https://twitter.com/buzzindelhi), head of digital India foundation & IT head of BJP, spoke on "FinTech". Understandably, he talked in favor of demonetization and Aadhar. But he made sensible points as an entrepreneur.

A startup entrepreneur faces challenges in **A**cquiring customers, **R**etaining customers, **M**anaging customers, and **M**onetizing customers. Financial institutions are focusing only on high value customers, because acquiring customers are expensive. **What if they can acquire customers for ₹1**, using Aadhar, like Reliance for its Jio launch? What if banks can acquire rural customers using Aadhar? Wouldn’t that lead to disruptive ideas in financial domain?

Money in India migrated to digital long back. For long, we’ve been relying on bank statements, purchasing using cards, and transferring using online banking.

Last interesting session was "**India, a land of opportunities**". [Ravi Mantha](https://twitter.com/rmantha2) was one of the panelist. He is a prolific investor, having invested in more than 20 startups. His experience showed up in the ideas he discussed.

**There are three main learning styles: visual, auditory, and analytical**. Schools teach only through analytical manner. Kids with other learning styles fall behind. Startups like playspring are addressing the other learning styles.

_[note to self: Marketing has to address all these three styles of learning. Marketing has to be on visual (youtube), auditory (podcasts), and analytical (analytical text) mediums]_.

Building pens isn't the solution to improve education. Similarly, building toilets isn't the solution to sanitization. Societal changes are possible only through behavioral changes.

We need Zomato for government offices. If we can rate offices like we rate restaurants, it will have impact on their services. [_I don't think so. Customers have a choice of restaurants. Not so with government offices_].

There was a pitching session too. [Yolo Health](https://yolohealth.in/) was the first one to pitch. Their pitch was crystal clear and had a value. Investors closed the "ask" in the session (in fact, they poured more). I was surprised to watch investors pour money so quickly.

The speakers and sessions themselves aren't the most interesting aspects of such events. Often times, mini-discussions over a cup of tea are more interesting and illuminating. When the crowd is filled with founders and funders, there is no shortage to these mini-discussions. I had many such exchanges about cyber-security, marketing, execution styles of Jugnoo, Flipkart, and other startups.

If you are running a business in Chandigarh, come and be part of [TiE Chandigarh](http://chandigarh.tie.org/). You'll gain and give back. Nothing satisfies an entrepreneur than these two factors.

_Don't forget to read my [notes](/tiecon17-02) from second day._
