---
title="Fame or Fortune"
slug="fame-fortune"
excerpt="Know what wealth and popularity mean to you."
tags=["wins","wealth","networking"]
type="post"
publish_at="13 Jan 21 08:00 IST"
featured_image="https://cdn.olai.in/jjude/fame-fortune.jpg"
---

The wealthiest in Hollywood remain hidden from public eyes. They have chosen to be richer than popular.

You can think of fame vs. fortune as a two by two matrix.


![Fame or Fortune](https://cdn.olai.in/jjude/fame-fortune.jpg)

### Not Rich & Not Known

We, as a family, have been in this state, and it is no fun. You have to escape this quadrant. Though hard-work might get you out of the misery, most probably you get out of this state because you get outside help and turned lucky.

### Rich & Not Known

My favorite place. Given a choice, I would like to operate out of a beach house in Malaga and retain the possibility to walk around any town without anyone noticing me. 

### Not Rich & Known

Trump is a good example. He used his daddy's money and borrowed money to put his name on high-rise buildings across New York while filing for bankruptcy more than once. He used his popularity to become the most powerful man in the world.

### Rich & Known

A lot of people dream of this state. If you desire this place, you should know this: fame is a liability; fortune is a liability. You [attract](https://tim.blog/2020/02/02/reasons-to-not-become-famous/) death threats, stalkers, harassment of family members, and extortion attempts. Not cool.

### Ideal Place

You should know what wealth and popularity mean to you.

I define wealth as discretionary time. If money doesn't aid me to be with my boys until they are bored, walk hand-in-hand with my wife through the woods, or talk to a friend-in-need when she needs it, that money is no good for me. I look at riches as a means to do what I want, when I want, with whom I want, for as long as I want.

[Amir Khan](https://en.wikipedia.org/wiki/Aamir_Khan) defined fame for me: "I want to be recognized when I'm with my peers; I don't care for the rest of the time."

The ideal position is to be at the near-maximum of fortune, and enough fame to earn but not crave more.

What is your ideal place in the fame and fortune matrix?

### Quote To Ponder

_It is better to have a permanent income than to be fascinating.- Oscar Wilde_


## Continue Reading

* [One decision instead of hundred decisions](/lead-decisions/)
* [Writing my obituary](/my-obituary/)
* [Principles trump processes](/principles-trumps/)