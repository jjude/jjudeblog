---
title="Gravitas WINS show"
slug="gwshow"
excerpt="A weekly live event to learn to build Gravitas"
tags=["events"]
type="page"
publish_at="08 Feb 21 18:03 IST"
featured_image="https://cdn.olai.in/jjude/gwshow.jpg"
---

## Upcoming Show

**Topic**: Book Review - Hidden Potential
**On**: Sept 7th, Saturday 7 - 8 pm IST
**Register here**: https://lu.ma/t70qunel

<!-- <a class="f6 fw6 dib ba mb3  b--black-20 bg-white black ph3 ph4-ns pv2 pv3-ns br2 grow no-underline" href="https://us06web.zoom.us/j/85700332437?pwd=0gq8qROR3So3yfqhO2AWrHPazbdaJk.1">Click To Join</span></a> -->


### Earlier Events

<iframe width="560" height="315" src="https://www.youtube.com/embed/videoseries?list=PL538B_xQcbX5dpLywuDy0nwMIRwcX5aVV" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### What Participants Said

> I'd borrow from Google's search button - "I'm feeling lucky" to be part of your community and learn something new each week. A big thank you :-) - Sumit Dhamija

> It was a short and crisp session. Joseph engaged the participants in a dialog to express their views - Aswini Selvaraj

> It is very hard to find in books what you teach us in these sessions - Daljeet Singh
