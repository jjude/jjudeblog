---
title="Building a career amidst a pandemic"
slug="career-in-pandemic"
excerpt="New ways of building a career are emerging. Take advantage of them."
tags=["coach","coach","talks"]
type= "post"
publish_at= "22 Jul 20 10:30 IST"
featured_image="https://cdn.olai.in/jjude/career-in-pandemic.png"
---

_I was invited to speak with recent graduates. This post is an edited version of the talk._

You are graduating amidst a pandemic, so it is natural to have many doubts about your future. As someone who has seen cycles of economic disasters, let me assure you that you will come out of this season just fine if you keep your fears in control and senses sharp.

How can you keep your senses sharp? Study market trends. If you understand trends, you can take advantage of them. I will aid you by laying out three trends I have observed. COVID pandemic accentuated these trends. I will also tell you how you can benefit from these trends.

![Building career in a pandemic](https://cdn.olai.in/jjude/career-in-pandemic.png)

### Trends

**Credentials don't matter**

When I graduated twenty-five years ago, and for long after that, companies demanded degrees as the first criterion to get a job offer. But that changed recently.

As Laszlo Bock, ex-SVP of Google [mentioned][1], "Your college degree is not a proxy anymore for having the skills or traits to do any job." The proportion of people without any college education at Google has increased over time. Google is not the only company. Almost [fifteen companies][2] don't give first preference to college degrees.

It is not that college degrees don't matter. At least not yet. But companies don't equate college degrees to smartness to do the job.

**Work from anywhere**

COVID pandemic accelerated the second trend. Earlier, if you wanted to work in a company in Bangalore or Boston, you have to move there. You have to be geographically closer to the company you wanted to work in.

Not any more.

Automatic Inc, runs Wordpress.com, which is the 15th most trafficked site in the world. Every employee of [Automatic Inc][3] works from home, more precisely, from wherever in the world they wish. Many more companies like Github, Mozilla, and Basecamp also operate with [remote teams][4]. 

After the pandemic, every IT company is operating in the distributed model. Large companies like Facebook and Twitter have already announced that their employees will work from home for at least another year.

What does this mean to you? 

Like credentials, your geographic location doesn't matter anymore to get a job. You can live in Chandigarh and work for a company in California.

**Partnership of experts**

Danny Boyle and AR Rahman collaborated for the much-acclaimed movie "Slumdog Millionaire." They come together for other films. Interestingly they don't work for each other, nor the same company. They are experts coming together for projects and divide the spoils.

Such a partnership of experts isn't restricted to Hollywood. You would be surprised that the Indian Government operates in this model.

I'm sure you have an Aadhaar number. Do you know who headed the UID project? [Nandan Nilekani][5]. Is he an employee of Government? No. 

Some of you might have used [SlideShare][7], an app where you can upload and share your presentations. [Amit Ranjan][6] co-founded the company. After selling SlideShare, he works as an architect for [DigiLocker][7] project with the Government of India.  Like Nandan, he is not an employee of the Government.

Instead of tied to companies for life, the newest trend is to form partnership to work on exciting projects and divide the profits.

### How to benefit from trends

If these are the trends emerging in the job market, what can you do to ride on these trends? I recommend three steps.

**Content Creation**

The first thing you need to do to benefit from these trends is to create a portfolio of your work. 

Imagine with me, please. You visit the Taj Mahal with your family. Two photographers approach you. The first one says, "I can take photographs of you. Each photo is 50 rupees. Please hire me." The second one comes with an album and says, "I have taken these photos. Please look at it and hire me as a photographer."

Who would you hire?

I'm sure you would hire the one with the album because you can look at his previous work. The same is true for you.

Start a blog or write on LinkedIn. Write about what you are learning, the role models you have, the movies that inspired you, the books that shaped your thinking, the videos that taught you the most, and so on. You can write just one article per week, and in a year you will have a good enough portfolio to show.

I have been blogging for more than a decade. Blogging has helped me in two ways: Writing regularly forced me to think in a structured manner; it has given me enormous visibility. 

You don't have to build your body of work only on text. If you are good at creating slides, create a portfolio in [Slideshare](https://www.slideshare.net/) and populate it; if you create fantastic videos, take advantage of [YouTube](https://www.youtube.com/); if you are into design, start filling [Behance](https://www.behance.net/) or [Dribble](https://dribbble.com/).

**Networking**

Secondly, you need to network, not only with your classmates and colleagues. You need to build a diverse network. You should network with CEOs, chartered accountants, doctors, and freelancers. More the mix of professions, the better.

Why? Listen to Dharmesh Shah, founder of Hubspot:

> Your probability of success is proportional to the number of people that want you to succeed. Work to keep increasing that number.

> — Dharmesh Shah (@dharmesh) [August 27, 2014](https://twitter.com/dharmesh/status/504480566084128769)

You increase this number by networking. Networking is not about exchanging business cards. You help others in the network succeed. That way, when the time comes, they will help you succeed.

**Social Proof**

If you desire to carve a chance of partnering with an expert, you need to build proofs in public—of your capability, dependability, and integrity.

How can you start with it?

Join a professional organization. There are Young Indians, The Indus Entrepreneurs, and other professional bodies. Volunteer to help with their meetings and events. There is no other better way to come into contact with some of the best experts in their respective fields. Let the organizers of these professional bodies know you are dependable and skillful. They will refer to others, and slowly, you will start to build a flywheel of social proof.

As in the physical flywheel, building initial momentum is tough. But once the wheel starts moving, it builds momentum on its own. 

### Now is better than tomorrow

Goto LinkedIn and write an article. If you don't have anything to write about, summarise this article. Share the article with a few of your friends and ask their opinion of the article. Write at least one article per week.

If you are a coder, start a project today and push the first commit to Github. If you are a designer, create a profile in Dribble and upload your first design. 

Perfect can wait.

### Also Read

-  [Future of jobs is Hollywood model](/future-of-jobs)
-  [How to find trends in your industry](/trends-in-industry)
-  [11 Lives to Build a Rich Career](/11-lives/)


[1]: https://www.nytimes.com/2014/04/20/opinion/sunday/friedman-how-to-get-a-job-at-google-part-2.html
[2]: https://www.glassdoor.com/blog/no-degree-required/
[3]: https://hbr.org/2013/03/how-wordpress-thrives-with-a-1
[4]: https://scottberkun.com/2013/how-many-companies-are-100-distributed/
[5]: https://en.wikipedia.org/wiki/Nandan_Nilekani
[6]: https://www.linkedin.com/in/amitranjanprofile/
[7]: https://www.slideshare.net/
[8]: https://inc42.com/buzz/slideshares-amit-ranjan-head-govts-project-open-apps/