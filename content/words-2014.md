---
title="Powerful Words That Transformed My Worldview In 2014"
slug="words-2014"
excerpt="Some words have magic in them. They pop out of screens and alter our perspectives permanently. An article by Allison Vesterfelt did exactly that."
tags=["insights"]
type="post"
publish_at="21 Dec 14 02:38 IST"
featured_image="https://cdn.olai.in/jjude/words-2014-gen.jpg"
---

Some words have magic in them. They pop out of screens and alter our perspectives permanently.

This year, an article by Allison Vesterfelt transformed my perspective about wealth in particular and ownership in general. The article itself is about an epiphany that Allison had while walking through Florida beaches, that subsequently led to a shift in her perspectives about money.

### There is plenty to go around

She begins her walk through the beach, with a skewed view of wealth. In that world view, there are only limited resources and everybody is fighting to grab a piece of that limited resource, akin to 'life is a zero-sum game' mindset. As she wanders the beachfront, she comes to grip with wild display of luxury. There are million-dollar beach-houses, expensive vehicles, fancy shoes and end-result of plastic surgeries. Ironically, that wild display of luxury, opens her eyes instead of depressing her. She realises,

> "...we weren't all competing to get a small part of a fixed pie. In fact, we didn't need to compete at all. Instead of feeling like a starving child at a dinner table, mad at everyone else who had a bigger portion than me, or guilty about having the portion I had, I just kept thinking, over and over: there is plenty to go around."

Those who were born in abject poverty and worked their way to sufficient wealth, often swing between the two feelings described by Allison — one time you are mad at everyone and another time, you are guilty. I know it is true because I myself have often swung between these feelings.

The truth is, we don't have to be mad or guilty.  Life is not a zero-sum game. There is, indeed, plenty to go around.

### You don't have to own a thing to enjoy it

She doesn't stop. She continues her stroll. A beautiful flower catches her eyes. As a natural reaction, she wants to pick the flower and take it home. That is when the next realisation hits her.

> “Admiring this beautiful flower on the beach is not the same as taking it back to my house.” I already had the vase in mind I was going to use when I displayed this flower prominently on my kitchen table. That’s when the rational side of me spoke up again. “Yes, but if you pluck this flower from the bush, it will die in a few hours.”

She realises that, she **doesn't have to own a flower to enjoy it**. That is a powerful shift in thinking.

We don't have to own a piece of this earth to enjoy our stay here; we don't have to photograph every moment with our kids to preserve our memory; we don't have to hoard money to feel rich.

Ownership is not an essential criteria for enjoyment, for now or for future.

![Powerful Words](https://cdn.olai.in/jjude/allison.png "You don't have to own to enjoy")

### How these mind-shifts influenced me in my entrepreneurial pursuit

Allison wrote this article in January. As I read it, I knew this is not an article that I can forget about. I stored it in Evernote. I read it again and again. I thought of ways to internalise these two concepts into my life.

I dropped the idea of buying a flat that my sister has been compelling me to buy. I stopped carrying bulky SLR cameras with me to photograph every moment with my kids. Instead, I started investing in experiences — like travelling.

Everything felt good. Very good.

Later came the test.

In June this year (2014, for those who read after the year is over), my friend and mentor, [Sastry][3] and I started discussing about an entrepreneurial venture. If I have to pursue it, I have to leave a cushy consulting job and move to Panchkula from Delhi. I also have to go without income for at least six months.

I loved the idea of the venture. I have been a nomad for a long time, so moving cities didn't bother me. But going without income bothered me. That too with kids. Can I do it?

It was decision time.

I opened Evernote, searched for the article by Allison and read it. Once. Twice. Paused. And read it again.

Is it true that there is plenty to go around?

I'm writing this from Panchkula. Four months have gone by since I started my company DSD Infosec. I can testify that there is indeed plenty to go around. But it isn't necessarily about money. There is plenty of 'love of the strangers'; there is plenty of generosity; there is plenty of ideas. If you work hard, there is plenty of luck too.

We will be launching our product in January. I will be counting on all of these plentifulness then.

Thank you Allison, for those powerful words.

[1]: https://twitter.com/allyvest
[2]: http://storylineblog.com/2014/01/29/a-new-way-to-think-about-money-that-can-make-us-all-rich/
[3]: https://twitter.com/sastrytumuluri
[4]: http://www.dsdinfosec.com

