---
title="Lee Launches on 'Application Development with no code and low code tools'"
slug="lee-lcnc"
excerpt="Low code & No code tools are becoming a category on their own. How should developers approach these tools?"
tags=["gwradio","tech"]
type="post"
publish_at="07 Feb 23 06:19 IST"
featured_image="https://cdn.olai.in/jjude/lee-nclc.jpg"
---

Low code no code tools are not new. Excel is a classic example. Every marketing, finance, and admin department has tons of Excel sheets which should be hundreds of lines of code. There are many other tools like this. But recently low code no code tools have become a category of their own. In today's conversation, Lee and I are discussing this trend and how developers should approach these tools.

<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless src="https://share.transistor.fm/e/e9273ecb"></iframe>

## What you'll learn
- Definition of Low Code & No Code tools
- Categories of Low Code & No Code tools
- Enterprise use cases for Low Code & No Code tools
- Apps suited for Low Code & No Code tools
- Low Code & No Code tools portability
- Cost of Low Code & No Code tools development
- How developers can get into Low Code & No Code tools development
- What apps are suited for Low Code & No Code Tools
- How IT Services can use Low Code & No Code Tools
- ChatGPT & Low Code No Code Tools

## Connect with Lee
- Twitter: https://twitter.com/LeeLaunches
- LinkedIn: https://www.linkedin.com/in/leelaunches/
- Bubble jobs news letter: https://theworkflows.dev/

## Connect with me
- Twitter: https://twitter.com/jjude
- LinkedIn: https://www.linkedin.com/in/jjude/
- Website: https://jjude.com/
- Newsletter: https://jjude.com/subscribe/
- Youtube: https://youtube.com/gravitaswins
- Email: podcast@jjude.com
- Executive Coaching Program: https://gravitaswins.com

## Your feedback counts

Thank you for listening. If you enjoy the podcast, would you please leave a short review on Apple podcast or on YouTube? It takes less than 60 seconds, and it really makes a difference in finding this podcast. And it boosts my spirits. 😀

I would like to hear what resonated the most with you in this episode. Either you can email me or share it on social media and tag me. Thank you for your support.

Have a life of WINS.

_Gravitas WINS Radio is produced by [Joseph Jude](https://jjude.com/). Be sure to subscribe on [Apple Podcast](https://podcasts.apple.com/in/podcast/gravitas-wins-radio/id1562802449), [Spotify](https://open.spotify.com/show/7LOBjAyjBkechMC0xMIj9E),[Google Podcast](https://podcasts.google.com/feed/aHR0cHM6Ly9mZWVkcy50cmFuc2lzdG9yLmZtL2dyYXZpdGFzLXdpbnMtcmFkaW8=),[YouTube](https://www.youtube.com/gravitaswins), or the podcasting app of your choice._

Please check out other episodes of [Gravitas WINS Radio](/gwr-themes/). For easy consumption, they are categorized by themes. If you are a fellow podcaster, you might be interested in [How I Podcast](/how-i-podcast/) or [tools I use](/i-use/).

## Continue Reading

* [Jinen Dedhia on 'Low code tools in enterprises'](/jinen-dronahq/)
* [Liji Thomas on 'Conversational Bots'](/lijit/)
* [Jyothi & Aravinda on 'Why Robotic Process Automation Matters To Enterprise'](/jyothi-rpa/)