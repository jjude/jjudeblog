---
title="Snowball not slot-machines"
slug="sbsm"
excerpt="build things that compound, that give you advantages"
tags=["biz","wins","coach","flywheel","frameworks"]
type="post"
publish_at="04 Jan 24 17:00 IST"
featured_image="https://cdn.olai.in/jjude/snowballs-and-slot-machines.jpg"
---

![focus on snow balls and not slot machines](https://cdn.olai.in/jjude/snowballs-and-slot-machines.jpg "focus on snow balls and not slot machines")

Over the past years, I've been crafting my [personal flywheel](/flywheel/), a wheel of interconnected components. With each improvement, the wheel spins faster, gaining momentum. My flywheel consists of wealth, insights, network, and self-control - [WINS](/wins/).

We all have different starting points; some with more money, some with more knowledge, and others with a vast network. When you have wealth, you can access better courses, buy books others can't afford, and attend exclusive workshops. These experiences provide insights that connect you to more people and opportunities, increasing your wealth. But this flywheel needs an axle: self-control. Without it, the wheel spins out of control and crashes.

As I pondered my focus for 2024, I stumbled upon an [article by Justin Jackson](https://justinjackson.ca/best-saas-marketing-strategies). Though it's about marketing, I found a personal catchphrase: "Snowball not slot machines."

> A snowball starts small but grows larger as it rolls, gaining mass and momentum. Slot machines are one-off events that give fleeting satisfaction but no lasting success.

In 2024, I'm focusing on building snowballs rather than chasing the momentary thrill of slot machines.

What are the examples of slot-machines?
- getting lost in watching random YouTube videos
- drifting aimlessly through Twitter
- playing video games mindlessly

These activities give you a quick hit of dopamine, a fleeting sense of pleasure and satisfaction. But they don't help you in the long run. They don't build momentum or give you any lasting advantage.

Now picture snowballing assets, starting small and growing larger over time. 

Take writing on LinkedIn as an example. You craft a great post and share it with the world. As the days pass, it catches more eyes and gains momentum, providing leverage and benefits. The same goes for meeting people and building a network. It begins with little significance but grows into something valuable with time.

Consider investing in a mutual fund. You can start small, but over time, dividends accumulate and can be reinvested, creating a snowball effect. 

In each area of your life, you can build these snowballs.

As I look toward 2024, I'm going to focus more on snowballs than slot machines. I'll try to curb my phone usage and aimless scrolling through YouTube videos – I know I can't stop completely, but I'll strive to control these fleeting desires. Instead, I'll work on growing my snowballs to see where they lead me.

What are you focusing in 2024?

## Continue Reading

* [Building a personal flywheel](/flywheel/)
* [If you see everything, you will get nothing](/focus-to-win/)
* [Three Types Of Goals You Should Set](https://jjude.com/three-goals/)