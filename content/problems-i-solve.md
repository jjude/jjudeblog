---
title="Problems I Solve"
slug="problems-i-solve"
excerpt="I help you get unstuck and avoid getting stuck again"
tags=["biz","coach"]
type="post"
publish_at="28 Jul 23 08:33 IST"
featured_image=""
---

[Justin Welsh](https://twitter.com/thejustinwelsh) posted on twitter [this](https://twitter.com/thejustinwelsh/status/1682094328569536527) challenge: 

> Ask 100 people who follow you:
>"Describe me - what problem do you think I solve?"
>If you get 100 different answers, it's time to narrow your offer.

Since I started coaching via [Gravitas WINS](/gravitaswins/) course, I've been posting regularly on [LinkedIn](https://www.linkedin.com/in/jjude/). So it seemed like a good time to pause and ask how my tribe sees me and would describe me. So I asked.

I was worried that the responses would be scattered. Thankfully, most of the answers fell into a few broad categories.

- People reach out to me to solve "I'm stuck" problems
- I am able to connect with them at their level and help them find answers in a structured way (instead of giving them a solution)
- I help them pick an impactful solution
- Having solved, I guide them to avoid further problems

The responses warmed my heart because I started blogging with [problem solving](/be-a-problem-solver/). Then focused on [structured thinking](/structured-thinking/) and [structured communication](/structured-communication/). Looks like it shows up in my interactions.

## Actual Responses

### SS

You push me to THINK logically, explore and ask questions. 

And that’s a blessing for me - plus meaningful schooling. 😊

### PS

Teaching us in the most organized way. 

While google throws gazillion theories at us, your way to mentoring help us to gather knowledge in a more summarized way.

### XR

To me, you solve a problem of: *How do I express myself and my ideas to the world uniquely?*

### AS

For me you are my mentor. When in doubt, crossroads, I reach out to you.

You provide an unbiased view, mentioning Pros and cons

### SB

1. Setting the correct course for career and life
2. Be a role model to see what can happen when you are consistent.
3. Motivate to be more productive with intentional imbalance.

### MS

I believe you provide clarity to the shady thoughts of an individual who ever asks you any questions. You know how to narrate story well and connect with your audience. If someone is struggling in his/her career guidance then connecting with you give them clarity in their thoughts. 🙏 you are great problem solver. 🙏

### RG

I think the main value prop that you bring to the table is a different perspective and way to look at things

That's your biggest asset I think

I think more than a course/guide/roadmap/strategy guy

You're more of "hey I've this problem or I'm stuck here" guy

You jump in, offer a different pov to look at things and then the other guy on table is ready to go and kill

This is where I've personally found out most value from our conversation

And this is based on our entire conversation from the day we had first chit chat to enrolling in gravitas wins and then to having regular conversation with you

### PS

Knowing you through the book review club, I feel like you strive to solve the problem or the overarching question in today’s society - How does one improve one self on a daily basis and develop themselves to be a contributor to the society.

### AG

I would say you help giving structure to solve different problems.

Back when we worked together, I learned how to think backwards while pitching by thinking about what three things could fail us.

Lately during the GravitasWINS course, i learned many new structured approaches on dealing with a problem.

### DS

You don’t sugercoat. That is rare.

### CA

I think facilitating exchange of knowledge is a great thing, and you are too good at it 😎

### SP

Helping folks get a better insight into a new domain with 'easy to digest and implement' content

### RS

Without a doubt, I hold a deep appreciation for the various aspects in which you add value. if I were to highlight one that stands out prominently, it would undoubtedly be reliability. Your consistent availability for discussions on any subject, offering valuable insights, is truly commendable. Your dedication and dependability make our interactions ( both personal and professional ) incredibly meaningful and productive.

### PJ

You help to revolve communication issues. I used to have a fear / concern of talking in public . You motivate people to come out of that and talk out their opinions in a forum

### AM

Getting us into a habit of thinking what possibly could go wrong and plan for such things accordingly. 
I know issues still come but a lot of pitfalls get covered with this approach.

Also whenever I discuss something technical you ask those questions which I had possibly not considered. Now whenever I try to review or discuss something with my team, it try to wear the same hat. 😀

### PSG

As you always share valuable resources, knowledge and information relevant to the interests or goal of our community. You help us to connect, learn and grow.

### HG
You encourage people to think SIMPLE solutions for a particular problem, yet in a very IMPACTFUL way 🤝🥰🥰

### SSS
1. Authentic
2. Structured
3. Open to help

You are a guide & mentor to me. 
I think you help me make my decisions more (in)validated by offering your deep insights and keen perspectives!
For that I'm forever grateful 🙏🏻

### KK

You are some one who understands both tech and business well... A critical combination for success as an individual and organisations can benefit from that knowledge.

Personally for me, you are able to guide people by helping them to break down a big challenge into small, solvable problems which otherwise could have been overwhelming.

## Do you have a problem?

I've helped IT executives think about their problems in a structured way, come up with effective solutions, and communicate them well. I've also helped solopreneurs launch and scale their products and services, especially in India. I've also helped a few CEOs along the way.

If you are stuck in your career or business, I can help you like how I helped all the above friends. You can discuss with me via [email](https://jjude.in/email) (preferred means) or via [Twitter](https://twitter.com/jjude) or [LinkedIn](https://www.linkedin.com/in/jjude/).