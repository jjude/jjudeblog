---
title="Did you compliment your friend recently?"
slug="compliment"
excerpt="If you want to make friends for life, be one"
tags=["wins","networking"]
type="post"
publish_at="07 Oct 20 09:00 IST"
featured_image="https://cdn.olai.in/jjude/compliment-gen.jpg"
---

My friend sent me this recently:

> You are an authority in what you do: technology. Your intellect and clarity of thought are unparalleled. And you have an amazing ability to express that too. And you manage to be so modest. You are a very good human. I think that's a success.

I routinely do things beyond my capabilities, so I get disappointed a lot. In one such foolish attempt, I pitched a local newspaper for a weekly column, and it was [rejected](/power-in-hands/). While whining to my friend about the futile pitch, she sent me the above encouraging message. The message brought cheer to my heart and put a smile on my face. When I read it the first time, I didn't expect to reread it. I have to admit, I have read the message tens of times since I received it.

I don't know if my friend ever read the most authoritative book on winning friends written almost a hundred years ago by Dale Carnegie. Still, she manifested the principle inscribed page after page in the book. Here is a paragraph from the [unedited version](https://socialskillswisdom.com/) of the timeless classic:

> You want the approval of those with whom you come in contact. You want recognition of your true worth. You want a feeling that you are important in your little world. You don't want to listen to cheap, insincere flattery, but you do crave sincere appreciation. You want your friends and associates to be, as Charles Schwab puts it, "hearty in their approbation and lavish in their praise." All of us want that.

**We all crave recognition of our true worth, a feeling of importance in our little world, and sincere appreciation from our friends.** In the era of social media, we throw around only flattery. We have lost the art of genuine and honest appreciation. But offering a heartfelt appreciation is like offering a glass of cold water on a hot day.

**When we compliment, we make the world go around a little merrier**. Peter Kaufman, an investment banker [taught](http://latticeworkinvesting.com/2018/04/06/peter-kaufman-on-the-multidisciplinary-approach-to-thinking/) that lesson when he spoke about "Multidisciplinary Approach to Thinking." 

> And this cat's sitting here, and we come over, and we gently pick it up by its tummy, and we put it in the crook of our elbow, and we gently stroke it. Does the cat try and scratch us? What does it do? It licks our hands. And as long as I sit here and stroke it, it's going to continue to try and lick my hand. It wants to show me what? 'I like this. This is agreeable. You're a good guy. Keep it up, man!'

Peter Kaufman summarised his lesson:

> Every interaction you have with another human being is merely mirrored reciprocation.

Didn't the good Lord Jesus also say, "**Do unto others as you would have others do unto you**"?

If you want to make friends for life, be one. Lavish sincere praise on your friends. **Start today**. You don't have to wait until you become an executive to do so. I will leave you with Dale again:

> You don't have to wait until you are an ambassador to France or chairman of the Clambake Committee of the Elk's Club before you use this philosophy of appreciation. You can work magic with it almost every day.

**Let the magic begin in your life**:
* Think of three friends who helped you recently but you haven't conveyed your appreciation
* Send them a relevant and specific appreciation
* Mark your calendar to repeat this every quarter

## Continue Reading

* [One decision instead of hundred decisions](/lead-decisions/)     
* [Sweet home is a safe home, even digitally](/sweet-home-safe-home/)
* [Thanks & Sorry-Say them before it is too late](/thanks-and-sorry/)

