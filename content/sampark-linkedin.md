---
title="Sampark A. Sachdeva on 'LinkedIn Success Mantras'"
slug="sampark-linkedin"
excerpt="Focus on building relations and having conversations. Then LinkedIn can do wonders for you."
tags=["gwradio","networking"]
type="post"
publish_at="12 Apr 22 06:00 IST"
featured_image="https://cdn.olai.in/jjude/sampark-linkedin.jpg"
---
Hello and welcome to Gravitas WINS conversations. Two years back, I attended a workshop by Sampark and it changed my perspective about LinkedIn. I have followed what he taught in that session and benefited immensely. In this episode, we discuss how to use LinkedIn to grow your career, no matter whether you are a junior or an senior level executive.

<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless src="https://share.transistor.fm/e/a3b9a005"></iframe>

## What you'll learn
- How to use LinkedIn to build your personal brand?
- How to use LinkedIn as an entrant into the job market?
- What can senior executives get out of LinkedIn?
- Should we use premium version for visibility?
- Are there any preferred time slots for posting?

## Connect with Sampark
- LinkedIn: https://www.linkedin.com/in/samparksachdeva
- Website: https://www.samparksesampark.com/
- Instagram: https://www.instagram.com/samparksesampark/

![](https://cdn.olai.in/jjude/sampark-linkedin.jpg)

## Connect with me
- Twitter: https://twitter.com/jjude
- LinkedIn: https://www.linkedin.com/in/jjude/
- Website: https://jjude.com/
- Newsletter: https://jjude.com/subscribe/
- Youtube: https://youtube.com/gravitaswins
- Email: podcast@jjude.com
- Executive Coaching Program: https://gravitaswins.com

## Your feedback counts

Thank you for listening. If you enjoy the podcast, would you please leave a short review on Apple podcast or on YouTube? It takes less than 60 seconds, and it really makes a difference in finding this podcast. And it boosts my spirits. 😀

I would like to hear what resonated the most with you in this episode. Either you can email me or share it on social media and tag me. Thank you for your support.

Have a life of WINS.

_Gravitas WINS Radio is produced by [Joseph Jude](https://jjude.com/). Be sure to subscribe on [Apple Podcast](https://podcasts.apple.com/in/podcast/gravitas-wins-radio/id1562802449), [Spotify](https://open.spotify.com/show/7LOBjAyjBkechMC0xMIj9E),[Google Podcast](https://podcasts.google.com/feed/aHR0cHM6Ly9mZWVkcy50cmFuc2lzdG9yLmZtL2dyYXZpdGFzLXdpbnMtcmFkaW8=),[YouTube](https://www.youtube.com/gravitaswins), or the podcasting app of your choice._

Please check out other episodes of [Gravitas WINS Radio](/gwr-themes/). For easy consumption, they are categorized by themes. If you are a fellow podcaster, you might be interested in [How I Podcast](/how-i-podcast/) or [Tools I Use](/i-use/).

## Continue Reading

* [Krishna Kumar on 'Power of storytelling in business'](/kk-sotry/)
* [Jayaram Easwaran on 'Secrets of A Master Storyteller'](/jayrame/)
* [Power Of Storytelling In Business](/biz-storytelling/)