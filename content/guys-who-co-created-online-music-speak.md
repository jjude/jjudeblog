---
title="Guys Who Co-created Online Music Speak"
slug="guys-who-co-created-online-music-speak"
excerpt="Interview with Heflin & Jasinthan about how they co-created music using online tools, with teams in India, Italy & Canada."
tags=["interview"]
type="post"
publish_at="15 Jul 10 16:30 IST"
featured_image="https://cdn.olai.in/jjude/guys-who-co-created-online-music-speak-gen.jpg"
---

Soon after I posted a [blog entry](/thank-god-for-internet/) about the first co-creation of online music, I sent an email to both [Josiah](http://www.josiahheflin.com/) and [Jasinthan](http://www.facebook.com/people/Jasinthan-Sasithasan/573935561) to know if they would be interested in an email interview. They both have a day job and they are busy artists. Yet both sent an affirmative reply. Since this is the first time I’m doing an email interview (any type of interview for that matter) it took me sometime to research about them to frame questions. After few email exchanges, the interview took its final shape.

Here I’m delighted to bring the guys who co-created online music:

### Q) Did you know each other before this project? How did you come together for this project?

**Josiah**: No I had not known Jasinthan till he contacted me via email / internet.

**Jasinthan**: In 2002 Josiah did music for Fr. Berchmans' English album "Praise forever Vol.2", and I was awfully impressed with the music and quickly became a fan of him. The first song I ever sang in stage was from that album, but I never thought that we will work together. In the beginning of 2009 I was planning to make a Tamil Christian album and I found him on Facebook. On May 5, 2009 I sent e-mails to several music directors about my thirst to make an album, but when I saw something different about Josiah's response..it felt like he really wanted to help me.

### Q) How did you respond to request from a stranger? What made you to respond positively?

**Josiah**: I had been missing the music scene in Chennai since I had moved to Italy so it was a pleasant surprise that Jasinthan had approached me for music. Jasinthan is a nice guy you could see it from his emails so I knew it would be a pleasant experience.

**Jasinthan**: People around me kept telling me to be careful when I work with a stranger especially with money and effot, but whenever I got an e-mail from Josaih, I felt I got the right person to work with. So it was Josiah who gets the credit, I never felt any pressure while working with him.

### Q) How was the planning process leading to the final recording? Who were involved and how the technology and internet helped in this process?

**Josiah**: I had actually tried out some other online songwriting collaborations and also some recordings online so had some(although limited) idea as to how I could go about it, but this was the first time I had done a whole album online with so many instruments and involving so many people. Jasinthan composed the songs and recorded the vocals on a basic track (and sent the vocals using [yousendit](https://www.yousendit.com/)) he also gave me an indication as to what feel he wanted on each song.

I had my friend Sam in India in 'Seventh Sound Studio'. We had webcam communication so I can see and hear recording while it goes on; so I could ensure I got what I wanted from the musicians. My sister Helen and nephew Ben helped me in contacting musicians and co- ordinating the recordings because of the different time zones. I hired session musicians Nathan, Bharathi and Kutty in India. I sent the music notes and tracks by FTP. I have an online space which I shared with Sam. This FTP uploading was later replaced by a file syncing and sharing software. I maintained a constant email contact with Jasinthan, Helen and Sam. It was the most important component which glued the whole project together.

In Italy I worked in [Pota Rock Studios](http://www.potarock.it/). I got Alessio to play guitars on 1 or 2 songs. Davide my friend helped me by lending his keyboards.

### Q) Who composed and conducted the music? How was it conducted online?

**Josiah**: Jasinthan composed the songs while I arranged them. I connected to the studio using a webcam during the recording and conducted the music recording.

### Q) What were the instruments used?

**Josiah**: Flute, Dilruba, Tablas and other percussion, Guitars, Keyboards and recording equipment for recordings. For communication headphone mixers, headphones & webcams.

### Q) What were the challenges faced in organizing co-creation of online music? What were the lessons learned?

**Josiah**: It is hard because of time zones when you need to phone musicians so sometimes you depend on local friends/relatives to help you out. And also the recordings were held in odd hours either in India or here in Italy because of the time zones.

Cross cultural aspect didn't apply much this time though I did record a little with Italians (who thought all Indians spoke one language). Most people involved were Indians or of Indian origin. Technically there were some obstacles like exchanging the big high quality wave files, but we tried various methods of sending using yousendit, etc and then settled on ftp. There are lessons learned with every first venture and there were a lot of them this time. To name one "Patience" ....online recording is slower because of the delays involved in communication, timezones etc.

**Jasinthan**: I recorded my vocal with a help of a friend here. He was really curious about this project because I was working with someone I never met and he had no confidence that this would work out. He thought that this might be a scam and advised me that I should be careful. When he listened to the final product he was really amazed and said that, ‘_**unity that we had is the beauty of the album**_’. He mentioned that he thought there is no way this would work out but now he feels that the faith I have with my God helped me get a good person to work with.

### Q) How are you marketing and distributing the Album? How are you tapping the power of internet for these activities (marketing & distribution)?

**Josiah**: I do believe internet is the future for all forms of marketing and distribution. My own album "[New Land](http://www.cdbaby.com/cd/heflin)" sells more mp3s than physical copies.

**Jasinthan**: I uploaded sample of all songs and some full songs at my [YouTube channels](http://il.youtube.com/user/Jasinthan0657) and also web radio such as "[AMEN FM](http://www.amenfm.com/)" and "[Waves of Power](http://www.wavesofpower.com/)" have interviewed both of us and gave us many listeners from all over the world.

### Q) What are the other projects in the pipeline in this model?

**Josiah**: Well I have already done a Tamil Christian worship album for Bro.Solomon who is also a singer songwriter from Sivakasi. I am doing Psalms 1 -10 in English (which was actually brought to me by Jasinthan). It is almost finished. This is a cross cultural album it was exciting since I get to work with musicians from many countries. Jasinthan might soon have a 2nd coming :) there are also some others who seem interested a music for video project in UK is almost finalized.

### Q) What are your views on the impact of such co-creation of online music in the music business?

**Josiah**: From a musician/producer point of view I think it is wonderful as it cuts boundaries; you can sit in your home and work with musicians all around the world. I'm working with musicians from India, UK, US, Lithuania and Brazil sitting in my room in Italy. I hope to be able to get myself a studio so I can work even better.

From an artist point of view I think it is wonderful too as it can enable people living in different lands to have access different instruments/musicians in this case Indian instruments. Jasinthan had pointed this out to me when I had asked for his comments/experiences after the CD was over.

**Jasinthan**: My Views are that it created a new ways to make music, especially in countries that lack Indian instruments. With internet they could easily connect with India. Many Canadian singers I know travel to India and stay there for months. Now the internet production would help them avoid these expenses.

Technology is making the world flat and a true global village. As Jasinthan says, internet technologies opens up new avenues for online collaboration. I wish both Jasinthan and Josiah best wishes.

_How are you tapping technology for your business?_

