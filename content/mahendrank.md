---
title="Mahendran Kathiresan on 'Homeschooling In India'"
slug="mahendrank"
excerpt="Is it possible to home-school your children in India? Hear from Mahendran who home-schooled his two children for the past nine years."
tags=["homeschool","gwradio","parenting"]
type="post"
publish_at="12 Oct 21 07:36 IST"
featured_image="https://cdn.olai.in/jjude/mahendran.jpg"
bsky_id="3lbuexr57et2x"
---

During the COVID-19 pandemic, lot of us were interested in homeschooling. As I explored about homeschooling in India, I came across Mahendran who home-schooled his two children for the past nine years. I interviewed him for [Gravitas WINS conversations](/podcast/). 

Here is the edited version of our conversation. I was liberal in editing the content. You should listen to the podcast.

<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless src="https://share.transistor.fm/e/0df86f06"></iframe>

- Homeschooling is experiential learning.
- Experiential learning is all about asking questions, being curious, and understanding the basics.
- Academics is only one part of homeschooling.
- Learning is driven by the family and the passion of the child.
- Homeschooling is about sharing, debating, and discussing.
- Your lab starts from kitchen.
- Homeschooling is a holistic lifestyle, not just running after books and marks.

![Homeschooling In India](https://cdn.olai.in/jjude/mahendran.jpg)

### What is homeschooling?

It is not keeping the kids at home and running a school.

**Homeschooling is experiential learning. Experiential learning is all about asking questions, being curious, and understanding the basics**. It is not just reading a book, but trying to experience the book by correlating the ideas to experience it. It is bringing all the senses into the play.

### What were your driving factors to get started with homeschooling?

I was a topper in college but struggled to get a job because my basics were not clear. Even after 26 interviews, I didn't get a job. Finally, I got my first job through recommendation. 

We don't understand what we read in school. We mug up, pass, and be happy. 

My younger kids were in the UKG and my son was in the second standard. I felt the same thing was happening to my kids. 

At one point my wife found homeschooling. We thought, why not try it. So far so good. I think its the best decision I ever did in my life.

### Is it a one-way door decision?

I felt that my kids should not feel that they were pushed into homeschooling. So every year we go back to normal schooling in terms of the admission process. I'll ask my kids whether they want to be part of regular schools because they know the best of both worlds. Every year, they choose to be in homeschooling.

I have seen a couple of my friends going back to normal school. There are few schools in Bangalore that take admissions for homeschooling. So it is not that you can't go back to normal schooling if you start with homeschooling.

### How does homeschooling work?

First of all, **homeschooling is not for kids. It's for the family**.  Academics is one part of homeschooling.

My kids start with meditation. And they play the instrumentals like Mridangam and Flute. They do every chore at home like cooking. There is learning everywhere.

It's not like you learn only from a book. Online content helps them a lot. My daughter is a YouTube baby. She goes there and she finds whatever she wants to learn. She is good at cooking by learning from YouTube.

We learn along with them. 

There are sites like Khan academy, funbrain.com, code.org, and a lot of open-source sites where they go in and learn. There is enough content in the online media for kids to learn.

### Will kids get attached to devices?

Online media is only one of the sources of information. 

Travel is a key part of homeschooling. We went to Ratnagiri, Maharashtra, to learn about sustainable living. We went to the Himalayas and stayed there. There are a lot of activities they get involved in. 

So gadget becomes probably 10% to 15% of the whole engagement. The gadget is always for a reference, but that's not the only source to get information.

When we watch a movie, we discuss what happened. What you felts was good and not correct. If you are at that place, how will you behave? It's more of a discussion. There is a discussion about things rather than just the one-way traffic.

There is no set curriculum for homeschooling. It is not that if a kid is in 5th standard, you take the books of 5th standard and teach him at home. It's more about what he likes. Learning is driven by the family and the passion of the child. 

Homeschooling is a method. It is not that kids who are in homeschooling are superior or inferior. If it works for you, you take it.

Every kid is different. So there is no comparison. We should come out of comparison mode.

### How much time do you spend on homeschooling

It is 24 x 7. 

For example, I run a startup. So when my kids are there, I talk about how it went, the kind of customers I met, the challenges I had. We talk about all of that immaterial of the age. 

There's nothing called a framework, like nine o'clock you start and 10 o'clock you finish. Homeschooling is about sharing, debating, and discussing. 

### Any approved education boards for homeschooling

In India, there is [NIOS](https://www.nios.ac.in/), which is NCERT approved board. My son appeared for his 10th exam and he cleared. So now he's 10 certified like you and me. My daughter is appearing now for the board exam. 

There is [SAT](https://collegereadiness.collegeboard.org/sat) as an international board. 

### Career Prospects

As a homeschooling parent, it is about giving them exposure. I run a company which is into refurbishing of computers. They come there.

My son started his own company when he was 11 years. He runs a company called Smooth Rides, which is into on-site bicycle repairing. 

Until the age of 17, it is an exploration mode. At the age of 17 or 18, they will figure out what they want. By that time they would have enough data points in front of them, rather than just one or two. 

I tell my son, even if you're running a shop, you should be mastering it. Convert your passion into a profession.

Don't go into software because everybody is running behind it. If you feel the software is good for you do it. But master it. Be passionate about it. 

**Homeschooling is thinking differently about everything - not only about studies but also about career prospects** etc.

### Learning things that need a lab like Chemistry or Biology

**Your lab starts from the kitchen**. You start learning about measurement and everything there. 

When my son wrote the board exam he had access to a lab. Whatever they teach in a year, they taught in 30 days.

### Support groups in India

There is a Facebook group. 

And there are groups within Bangalore. We used to meet in a park called Krishna Rao park before the pandemic. 

Every city has a group. Every homeschooler will like to meet another homeschooler. 

### Fitness

My kids start the day with meditation. By 3.30 pm, they will leave home. 

My son is an avid basketball player and he plays for the team also. So they are out almost for four hours. They will be tired when they come back. 

We don't have any home assistance. We need to do all day-to-day work at home. 

If you are always listening to online gadgets you will have the problem of kids gaining weight. Here you need to clean the house, bathroom, and so on. Then they go out and play for many hours. 

### Developing social skills

My daughter is an extrovert. The whole community knows me through her. My son, on the contrary, is bookish. Even when they were going to school it was the same. 

It's more of a persona of the person rather than of the homeschooling. If it's an introvert, even in school, it becomes tough. But in homeschooling, they move with people of all ages.

The underlining concept is if you want most, you need to find out. If they want something, they need to figure it out. They need to ask their uncle or grandma or grandpa.

### Is homeschooling a luxurious lifestyle?

After homeschooling, we saved a lot. When we went to the Himalayas, we traveled by train, in sleeper class. That is where you meet people and all. Also, we traveled by the local bus. We walk a lot. We go a lot of trekking. All of these bring a lot of experience.

We follow a minimalistic lifestyle. I believe in a circular economy and sustainably giving back to society. 

**Homeschooling is a holistic lifestyle, not just running after books and marks.**

There are 32 million Indian children who never went to any school. But if they have some access to online information, they should be able to cope up with their peers.

### Dad & Son ventures

I started something called "Dad & Dude." The concept is how to reduce e-waste by this and next generation. I wanted to become an entrepreneur. But at the same time, I wanted to give back to society. 

A lot of corporate use computers and then throw them. Then there are a lot of people who can't afford a computer. They can buy a second-hand computer, but the seller does not take any accountability. 

We took these old computers, refurbished and sell them with a warranty, bringing accountability part. I also started promoting e-waste.

Kids are exposed to a lot of software, but they don't get any opportunity to get their hands dirty in electronics. So I go and do a lot of workshops, give a laptop to kids and ask them to open it and do everything. 

And my son started something around repairing cycles. He felt a lot of people are not cycling because there are no nearby second-hand shops. He started going to apartments and villas and he repairs cycles. 

He was trained in cycle repair in a nearby town called Hosur, where he went for one month and learned to repair the cycle.

Another venture I started is InnoMonk with a vision to bring a computer in every child's hand. As I mentioned earlier, there are 32 million children who never went to school. People have mobile phones, but mobile phones are consumption devices. You can't create things using mobile phones. If you want to create something, you need a laptop or a computer. 

We found something called a single board computer which totally runs on open source. It starts at ₹7500. They can connect the device to the TV and make that as a computer. 

By this, we are trying to democratize access to online information irrespective of their financial status. 

My son is taking care of finance, vendor management in my venture. My daughter takes care of customer service. My son was with me when I met the auditor. He integrated all my accounts with Zoho books. 

Given them the platform. They'll figure it out. We underestimate our kids.

### Interning in a cycle shop

When one of my friends from Chennai came, he saw our cycle being punctured. He taught my son to repair it. Then my son said he want to learn about repairing cycling. We went to a cycle shop called Royal Cycle Mart near Hosur asked permission to learn repairing cycles.

Every day he would take a tiffin-box, go on a bus, and repair 5 - 10 cycles. 

Then he made his own online site using WIX. People call, we go with a bag with all tools. He even refurbishes cycles. He has refurbished 45 cycles.

### Future of homeschooling in India

After the new education policy, I see there's a lot of things which are going to change. It will pave the path for homeschooling in India. I see a bright future for homeschooling.

My son is a better leader than me. Consider taking decisions. He is exploring a lot and taking a lot of decisions.

### Watch on YouTube

<iframe width="560" height="315" src="https://www.youtube.com/embed/FFIIen9dk0w" title="Homeschooling In India" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Connect with Mahendran
- [LinkedIn](https://www.linkedin.com/in/mahendran-kathiresan-0324a74/)

### References

- [Fun Brain](https://www.funbrain.com/)
- [Khan Academy](https://www.khanacademy.org/)
- [Code.org](https://code.org/)
- [Swashikshan - Indian Association of Homeschoolers](http://swashikshan.in/)
- [NIOS](https://www.nios.ac.in/)
- [Dad & Dude](http://www.DadnDude.com)
- [Smooth Rides](https://kavinmahen.wixsite.com/smoothrides/)
- [InnoMonk](https://innomonk.com/)
- [Aurinko Academy](https://www.aurinkoacademy.com/)
- [Aarohi Life Education](https://aarohilife.org/)

## Connect with me
- Twitter: https://twitter.com/jjude
- LinkedIn: https://www.linkedin.com/in/jjude/
- Website: https://jjude.com/
- Newsletter: https://jjude.com/subscribe/
- Youtube: https://youtube.com/gravitaswins
- Email: podcast@jjude.com
- Executive Coaching Program: https://gravitaswins.com

## Your feedback counts

Thank you for listening. If you enjoy the podcast, would you please leave a short review on Apple podcast or on YouTube? It takes less than 60 seconds, and it really makes a difference in finding this podcast. And it boosts my spirits. 😀

I would like to hear what resonated the most with you in this episode. Either you can email me or share it on social media and tag me. Thank you for your support.

Have a life of WINS.

## Amazing tools I use:
- I record all the remote interview with [Riverside.fm](https://riverside.fm/?via=joseph-jude)
- I use [Transistor.fm](https://transistor.fm/?via=jjude) for hosting these podcast episodes

_Gravitas WINS Radio is produced by [Joseph Jude](https://jjude.com/). Be sure to subscribe on [Apple Podcast](https://podcasts.apple.com/in/podcast/gravitas-wins-radio/id1562802449), [Spotify](https://open.spotify.com/show/7LOBjAyjBkechMC0xMIj9E),[Google Podcast](https://podcasts.google.com/feed/aHR0cHM6Ly9mZWVkcy50cmFuc2lzdG9yLmZtL2dyYXZpdGFzLXdpbnMtcmFkaW8=),[YouTube](https://www.youtube.com/gravitaswins), or the podcasting app of your choice._

Please check out other episodes of [Gravitas WINS Radio](/gwr-themes/). For easy consumption, they are categorized by themes. If you are a fellow podcaster, you might be interested in [How I Podcast](/how-i-podcast/) or [tools I use](/i-use/).

## Continue Reading

- [How we started with homechooling...](/how-we-started-with-homeschooling/)
- [Why And How I'm Homeschooling My Kids](/why-how-homeschooling/)
- [Teaching storytelling to kids](/storytelling-to-kids/)