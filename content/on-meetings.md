---
title="On Meetings"
slug="on-meetings"
excerpt="Few things I learned about meetings"
tags=["coach","coach"]
type="post"
publish_at="23 Dec 20 08:00 IST"
featured_image="https://cdn.olai.in/jjude/meetings-dilbert.jpg"
---

When I was a junior in the corporate world, I was a passive participant in meetings. As a [CXO][5], I now organize meetings or shape meetings as an active participant. In this post, I'm sharing what I've learned about meetings.

![Dilbert on meetings](https://cdn.olai.in/jjude/meetings-dilbert.jpg)

### Meetings are unavoidable

If you work in a team, meetings are unavoidable. You may color it by any other name, like focused discussion, brainstorming, or retreat, yet it is still a meeting. Once you acknowledge that meetings are unavoidable, you can plan to get the best out of them. 

### Executives should coach about meetings

Since executives can't get rid of meetings, the next best thing they can do is coaching their teams about having effective meetings. When we have five people in an hour-long session, I routinely remind them that it is not an hour-long meeting; instead, it is a five-hour meeting. I ensure the meeting agenda is clear; I hold the participants accountable for preparation; I'll interrupt if someone tries to divert the meeting from its agenda.

Meetings are leaky-buckets in companies. Executives should coach their teams to minimize these leaks.

### Types of meetings

Not all meetings are the same, and they shouldn't be treated the same. Teams may come together to share information, debate a pressing issue, or decide the next course of action. Not every type of meeting should be an hour-long with everyone arguing with tension around the air. That would make everyone quit the meeting and then sometime later the company. You should evolve different protocols for different types of meetings.

### Writing brings clarity

We all tend to ramble on. To limit loose-talk, I ask the participants to share the main points before the meeting. Not as PowerPoint slides, but in text format. When a member writes her points and shares them with others, there is a high chance she remains focused on those points during the meeting.

When the major points are already shared, it helps me and others come prepared for the meeting. I can ask pointed questions and avoid the "I'll get back to you" type of discussion.

### Learn from practitioners

If you want to get better at any topic, the Internet enables you to [learn][1] from the practitioners. Jeff Bezos has been sharing his ideas via his [shareholder's letters][2]. They are a treasure-trove on every business topic.

[The Observer Effect][3] is a recent favorite. [Daniel Ek][4], founder and CEO of Spotify, recently shared his ideas about making decisions, conducting meetings, and leading teams. 

Templates are another way to learn from practitioners. You can borrow variety of [meeting templates](https://slab.com/library/categories/weekly-meetings/) from industry leaders like Trello and Salesforce and customize to your needs.

What else can you teach me about meetings? Tell me on [Twitter][6] or on [LinkedIn][5]

## Continue Reading

* [Three types of books as per Elon Musk](/book-types)
* [Agile in the C-Suite](/csuite-agile)
* [Mastering sales as a new CTO](/cto-sales)


[1]: /sdl/
[2]: https://ir.aboutamazon.com/annual-reports-proxies-and-shareholder-letters/default.aspx
[3]: https://www.theobservereffect.org/index.html
[4]: https://www.theobservereffect.org/daniel.html
[5]: https://www.linkedin.com/in/jjude/
[6]: https://twitter.com/jjude