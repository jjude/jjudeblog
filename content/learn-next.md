---
title="A Roadmap For Developers"
slug="learn-next"
excerpt="Answer to a frequently asked question by developers"
tags=["coach","sdl"]
type="post"
publish_at="03 Feb 21 08:40 IST"
featured_image="https://cdn.olai.in/jjude/dev-roadmap.jpg"
---

Every developer asks, "What should I learn next," at least once in their lifetime. With my experience as a CTO, I want to provide a roadmap for learning. Mind you, this advice is not about getting the next job. Let us dive in.

![Developer's question to learn next](https://cdn.olai.in/jjude/onkar-qstn.jpg)

We all start with **building applications**. We might start with building an API server or a mobile application, or a browser-based front-end application. Even though you start with a particular layer in the application stack, learn at least one component from each stack of software application development. Maybe Vue.js, Python, and Postgresql. You may learn any other language or framework or DB but know at least one from each layer.

Why learn full-stack? Only then will you **understand the constraints of each stack**, which will help you develop a good perspective of software development.

Once you start building, you will realize you face a few problems again and again and will start wondering if anyone else faced these problems and solved them. That's when it is an excellent time to learn algorithms and design patterns. They are **solutions to common problems in software development**. 

When you have come to this level, you have learned to create applications as a one-time activity. In practice, you have to continue to update the application for a long time while working in a team. Hopefully, you turn lucky, so one of the applications you develop becomes a hit and used by hundreds of users. To prepare for this stage, you need to **learn to scale** -- DevOps, debugging, monitoring, tuning, and Gitflow. 

These three bundles form the foundation layer that all software developers have to understand. But this is not sufficient enough. 

You'll not build applications from scratch every time. All companies need certain applications, like HRMS, Document management systems, marketing automation, and the like. Unless there are requirements that mandate a unique workflow, business owners would like to use existing solutions to go to market quickly. **Learning these platforms and packages will teach abstracting technical complexities** for business users. You will still apply everything you learned from design, build, and scale.

The world of technology changes fast. Hence, you need a mechanism to keep pace with the changing market conditions. You need to **learn to build radars to scan the market** for trends and a means to see whether you will embrace a particular trend or pass it.

_Consume, produce, engage is a good framework as a radar. Read how I use [self directed learning](/sdl/)_

So far, we have talked about a deterministic world where if you write a block of code and it has logic, it will work.

If you want to move to senior positions, you have to cross the chasm and move into the business world. In the business world, **those who interpret get paid well**. Suppose you, a software developer, need a business analyst to translate what the business owners are saying. In that case, you won't get paid well; the business analyst will. But if you raise your understanding to ask pertinent questions, you become valuable and command higher pay.

Try to understand at least one business vertical, like retail, e-commerce, or healthcare. You should know the intricacies of the vertical and required compliances, including the jargons. If an owner says, "I want to build an application to track the bill of materials," you should be able to understand it. Or if they say, "I want a GDPR compliant application," then you shouldn't say, I don't know what you are talking.

Besides the core skills I talked about so far, you need two skills to stand apart from the technology crowd. These two skills cut through all the layers.

The first one is [structured thinking](/structured-thinking/). Take, for example, you have deployed an application to production, and it is not working. Most developers follow a "trial and error" method to identify and solve the issue. Can you instead think in a structured manner to debug the problem and apply a similar "structured thinking" to solve the problem? You can use "structured thinking" for almost all the decisions you face in your life.

_I teach structured thinking in the Gravitas WINS course. If you are interested, check out the [course](https://gravitaswins.com/)._

Thinking in a structured manner is an inward-looking super-power. You should compliment it with the ability to articulate your thinking to others. When you can explain your position to others, that ability sets you apart from others who can talk only to computers. 

![Roadmap to CTO office](https://cdn.olai.in/jjude/dev-roadmap.jpg)

This is the roadmap to a CTO office. If you can learn these things, your career will blossom and flourish even if you do not master every skill mentioned in this post. 

Hopefully, this is helpful to you. If you have any comments, ask me via [twitter](https://twitter.com/jjude) or [email](https://jjude.in/email) and I will be happy to answer.

While you are here, please subscribe to my [Wednesday Wisdom](/subscribe/) newsletter.

## Continue Reading

* [The curse of EVERYTHING and NOW on building your career](/all-and-now/)
* [Are You Learning A New Domain? Visit Its Zoo](/are-you-learning-a-new-domain-visit-its-zoo/)
* [Three types of books as per Elon Musk](/book-types/)