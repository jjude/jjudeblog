---
title="Build on the rock for the storms are surely coming"
slug="build-on-rock"
excerpt="You can't predict when the storms will come. Better build your life on a rock than sand."
tags=["wins","luck","wealth","coach"]
type= "post"
publish_at= "05 Aug 20 10:15 IST"
featured_image="https://cdn.olai.in/jjude/storms-unsplash.jpg"
---

Japan is prone to earthquakes, Tsunami, and typhoons. On March 11, 2011, a 9.0-Richter scale earthquake hit Japan. The quake was so powerful that it shifted earth's axis by 25 cms. The resultant tsunami waves reached as far as Alaska, Hawaii, and Chile. The earthquake and the ensuing Tsunami killed 20,000 people. 

As tragic it is, the total lives lost were ten times lesser than that of the 2004 Tsunami, which claimed the lives of 230,000 people. How did Japan save so many lives?

Japan doesn't treat earthquakes as "if" but "when." The country's building's act mandates adherence to anti-seismic standards. Tokyo has the world's largest fire brigade trained to prevent aftershocks of earthquakes. Japan, and specifically Tokyo, invest heavily in technology to raise early warning alarms and prevent any aftermath of earthquakes.

Contrast this with Mumbai, the financial capital of India. Not a year goes by when it is not flooded and paralyzed by slightest of rain. Even the country's auditor faulted the preparedness of the city's disaster management. Mumbai is not alone. Delhi and Chennai face a similar fate, every year. Unlike in Japan, India continues to limp and lurch.

This post is not about politics, but about **understanding, discovering, and handling risks**, both personally and professionally.

![Artist's rendition of a tornado destroying a structure](https://cdn.olai.in/jjude/storms-unsplash.jpg)

Let us see some of the risks in personal lives:

* Inflation will reduce the value of money
* Your kids will fly out of home into a world they don't understand
* Your health will deteriorate

If you are an employee, then the risks could be:

* You are a disposable resource
* You have a single source of income
* An incompetent but noisy colleague gets promoted

If you run a small business:

* 80% of your revenue comes for a single customer
* Your income is from a single vertical or a geography
* All your employees are allocated to projects

None of the points I mentioned require a prophetic insight. If you **live long enough, work long enough, or run a business long enough, you'll experience these risks** in one form or the other.

If you want to treat these risks like the city of Mumbai, then you hope the rain will kill the next-door neighbor and leave you unscathed year after year. Or you can be wise like the city of Tokyo.

Let me tell you a secret. You don't need to be a genius to find ways to handle these risks. Those who dealt with the risks have told us their ideas via books, blogs, and courses.

Take the financial risks. Scott Adams, the creator of Dilbert, provided [his blueprint](https://www.mattcutts.com/blog/scott-adams-financial-advice/):

1. Make a will
2. Pay off your credit card balance.
3. Get term life insurance if you have a family to support.
4. Fund your company 401K to the maximum.
5. Fund your IRA to the maximum.
6. Buy a house if you want to live in a house and can afford it.
7. Put six months' expenses in a money market account.
8. Take whatever is leftover and invest it 70 percent in a stock index fund and 30 percent in a bond fund through any discount brokerage company and never touch it until retirement
9. If any of this confuses you, or you have something special going on (retirement, college planning, tax issue), hire a fee-based financial planner, not one who charges you a percentage of your portfolio.

[Value Research](https://web.archive.org/web/20210511064643/https://www.valueresearchonline.com/stories/22504/financial-advice-for-a-lifetime) has customized this sage advice from a cartoonist, **for the Indian readers**. 

1. Make a will
2. Pay off your credit card balance.
3. Get term life insurance.
4. Fund provident fund to the maximum.
6. Buy a house if you can afford it.
7. Put six months' expenses in a money market account.
8. Invest in balanced funds

You don't need to read about quants and alphas for financial well-being. Scott's advice is simple, and it works. The simplicity of the advice is also why it is ignored.

How about **making yourself valuable**? Scott to the rescue again. He talks about [Talent Stack](https://web.archive.org/web/20210804134107/ttps://www.scottadamssays.com/2016/12/27/the-kristina-talent-stack/):

> The idea of a talent stack is that you can combine ordinary skills until you have enough of the right kind to be extraordinary. You don't have to be the best in the world at any one thing. All you need to succeed is to be good at a number of skills that fit well together.

He quotes his girlfriend, himself, and President Trump as an example. I have taken his advice seriously, as like his financial advice. 

I am a technologist by training. I realized I'm never going to be the best technologist. I added business skills and storytelling, and now I've a formidable advantage. My talent stack looks like this:

![Talent Stack For Digital Age](https://cdn.olai.in/jjude/digital-value.png)

_Read more about my talent [stack](/value-in-digital-age/)_

How about building a family? Take Ronald Regan as an example. His life is an excellent example of this post's theme. He [wrote letters](https://geni.us/love-you-ronnie) to his wife every day, even during his presidency. Later in life, he had Alzheimer's. His wife returned his love when he forgot who he was.

Your situation will be different, and you can't take everything mentioned in this post as is. That is ok. [Don't torture your context into a solution](/dont-torture-context-to-fit-your-solution/). Understand the fundamentals, and customize the advice to fit your needs.

Talking of the fundamentals, let me end with this parable: 

> Two men started building a house—one built on a rock, another on the sand. Storms and floods came. One stood, another vanished.

You won't know the exact moment storms and floods will come, but you can be sure they will come. Better to build your life on a rock rather than sand.

_Photo: [Artist's rendition of a tornado destroying a structure](https://unsplash.com/photos/Zus94oboIsM) by [NOAA](https://unsplash.com/@noaa) on [Unsplash](https://unsplash.com/)_

## Continue Reading

- [Future Doesn't Happen To Us, Future Happens Because Of Us](/shape-the-future/)
- [How To Get Lucky?](/luck/)
- [If You See Everything, You Will Get Nothing](/focus-to-win/)