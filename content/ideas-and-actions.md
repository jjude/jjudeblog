---
title="Ideas Don't Matter, If You Don't Act"
slug="ideas-and-actions"
excerpt="The ideas that changed my life are those that I took action. Even a small action."
tags=["coach","wins","gwradio"]
type="post"
publish_at="06 Jul 21 20:36 IST"
featured_image="https://cdn.olai.in/jjude/ideas-and-actions-gen.jpg"
---

<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless src="https://share.transistor.fm/e/ca007b12"></iframe>

### Consumption Of Ideas Doesn't Matter
You may read, watch, and listen to some of the most innovative ideas floating around the internet every day. Perhaps you will comment on it, share it, or even save it to your digital notebook. Guess what? None of that matters. 

Do you know what matters? The only thing that matters is, what are you going to do with that idea. When are you going to show up? How are you going to make that idea yours?

The ideas that changed my life are those that I took action. Even a small action.

### Parable Of Talents

One such idea that changed my life is the parable of talents.

I read the parable of the talents in the Bible. A rich man gives one gold coin to one servant, five to another, and ten to a third. After a year, the rich man asks for the account. The one who got the 10 doubled it; the one who got 5 too doubled it; the one who got 1 gold coin says, I buried it in the sand because I was afraid I might lose it. 

The rich man gets angry, takes the coin, and gives it to the person with 20 gold coins. The people standing nearby object to it, saying he's already had enough. The master says those who have will get even more; those who don't have will lose everything they have.

What does this mean?

### Skill Stacking
Scott Adams, the Dilbert guy, says the same thing in a much more modern way. He talks about skill stacking. Stacking skills is about developing complementary skills that work well together. Scott Adams combined his business skills with cartooning. 

I looked for skills to stack after reading the parable and Scott Adams' article, so I could gain as much leverage as possible.

### Action Makes You Stand Out
Here is how I translated that idea into action.

I am a software developer. Software developers are extremely good at communicating with machines, but not with people. That's how I was. The higher up the career ladder I went, the more I talked to people. So I decided to add structured communication to my skill stack. 

First, I blogged, and then I started speaking in in-house events at companies where I worked. Slowly, I began speaking at small conferences. I now have a weekly webinar and a podcast. I am a lot more confident speaker than I was before.

Adding technology to my public speaking allows me to stand out from the crowd of developers and general public speakers.

### Quote To Ponder

Without knowledge action is useless and knowledge without action is futile. - Abu Bakr

## Continue Reading

* [Building a personal flywheel](/flywheel)
* [Get Over Your Fears, For They Are Just Imaginary](/fears-are-imaginary)
* [How Your Attitude Impacts Your Career Growth?](/how-your-attitude-impacts-your-career-growth)
