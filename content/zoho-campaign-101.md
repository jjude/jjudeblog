---
title="A step by step guide to Email marketing with Zoho campaign"
slug="zoho-campaign-101"
excerpt="Email converts 40 times more than Twitter & Facebook combined. Startups and small business may well focus solely on email marketing."
tags=["zoho","martech"]
type="post"
publish_at="07 Jul 15 17:52 IST"
featured_image="https://cdn.olain.in/jjude/zohocamp07.png"
---
Email remains the most effective marketing channel, despite the hype about social media platforms like Twitter, Facebook, and LinkedIn. According to a [McKinsey study ](http://www.mckinsey.com/insights/marketing_sales/why_marketers_should_keep_sending_you_emails), Email converts 40 times more than Twitter and Facebook combined. With such high conversion rates, startups and small business may well focus solely on email marketing. 

Though you could use Gmail and Hotmail for marketing, choosing an email-marketing service provider (ESP) gives you many benefits. ESPs have inbuilt tools to collect and manage subscriber emails, classify subscribers into different segments, design emails with drag-and-drop method, and finally analyze effectiveness of each email campaign.

One such ESP is Zoho Campaign. In this step-by-step guide, I am going to walk you through creating your first email campaign using Zoho Campaign.

### I. Create a list of subscribers

The first step to the campaign is to create a database of subscribers who will receive the campaign.

  1. Go to [https://www.zoho.com/campaigns/](https://www.zoho.com/campaigns/) and create a login. If you already have a Zoho login, you can login using the same URL. 
  2. As soon as you login, you will see a _Getting Started_ screen. Select _Add Contacts_ from the screen.
  3. Enter emails and names of the subscribers. Add them also to a mailing list (you can create a mailing list from _Create Mailing List_ link at the bottom).

![Add contacts in Zoho Campaign](https://cdn.olain.in/jjude/zohocamp02.png)

### II. Design an email campaign

  1. Click on ‘_Email Campaigns_’ link from the navigation bar.
  2. Click on ‘_Create Campaign_’
  3. Enter details about your campaign like subject, sender name and sender email. Give a friendly name, like your name, in the ‘_Sender Name_’ field and not ‘Customer Support’ or something generic. Finish by clicking ‘_Create Content_’.
  4. You can design nice looking HTML email templates, that display well on mobile. If you can also import templates designed by your designers. For this article, let us keep things simple and select ‘_Basic Templates_’.
  5. Drag and drop a ‘_heading_’ and a ‘_paragraph_’ into the editable area and start typing your content. Add more ‘_paragraph_’ or other elements as needed.

![Design your Zoho Campaign](https://cdn.olain.in/jjude/zohocamp07.png)

  6. Once satisfied, click on ‘_Save & Close_’. You can still edit the content anytime you want, until you send the campaign. 
  7. If you like to verify the design, you can send a test email to yourself.
  8. Finish the design by clicking on ‘_Save & Exit_’

### III. Send an email campaign

Now you are ready to send this email campaign to your subscribers.

  1. Click on ‘_Add Recipients_’ and select the mailing list you created in the first section.
  2. For the first few times, Zoho will review your campaign against Spam rules. Click on ‘_Send For Review_’. It takes about 10 - 15 minutes for the review and Zoho will inform you via email when the review is complete. After that you can send the campaign.

![Send your campaign for review](https://cdn.olain.in/jjude/zohocamp10.png)

### IV. Analyze your campaign

Campaign doesn’t end with sending one. A savvy marketer analyses each campaign to increase effectiveness of the next campaign. Zoho provides detailed information to analyze each campaign.

  1. Click on any campaign that you sent.
  2. You will see a summary report of the campaign displaying the total emails sent, total emails that bounced, how many subscribers opened, how many clicked and even how many unsubscribed. You can navigate to detailed reports from here or from the right-side menu.

![Summary report of your campaign](https://cdn.olain.in/jjude/zohocamp11.png)

  3. Click on the ‘_Recipient Activity_’ link from the right. This will show you the subscribers who got the email, opened, clicked, unsubscribed and god forbid, marked it as spam.

![Link activity report of your campaign](https://cdn.olain.in/jjude/zohocamp13.png)

  4. Click on ‘_Link Click Activity_’ link to know the popular links in your campaign.
  5. The other useful report is ‘_User Agent_’ report which displays the break-up of devices and operating systems, desktop email clients, and web browsers.

![User devices report of your campaign](https://cdn.olain.in/jjude/zohocamp14.png)

Congrats. You have started your marketing campaign. Secret to successful marketing is to send out email campaigns at regular intervals to keep your product in the memory of your customers and users. Good luck.
