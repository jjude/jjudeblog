---
title="Importance of quality in global competition"
slug="quality-cma"
excerpt="My Talk at Chandigarh Management Association about import of quality in global competition"
tags=["talks","coach"]
type= "post"
publish_at= "23 Nov 19 20:38 IST"
featured_image="https://cdn.olai.in/jjude/cma.jpg"
---

When we grew up, we had access to only Indian products. Whether it was coffee we drank, or car we drove, or the cap we wore. It was all Indian. These products defined our notion of quality. Today the market is flooded with foreign products. Most of these are cheap Chinese products, but many of them are high-quality products too. Our potential customers are exposed to the highest quality standards because of foreign brands. So even though we might operate only in India, and have absolutely no plan to compete in other markets, quality becomes paramount.

That is why I am thankful to Mr. Tej and CMA for giving me this opportunity to talk about the importance of quality in global competition. 

!["Addressing Chandigarh Management Association"](https://cdn.olai.in/jjude/cma.jpg)

Let me reiterate the point that I made in the introductory remark. **Quality of the highest standards is critical even if we play only within India**, even if we operate only in the Tricity area.

Next, I want to put forward a point that **quality is not just about manufacturing**. We are all aware of six sigma and the defects per million parts. I guess some of you might even be black belts in six sigma. With all those standards, the quality in manufacturing has become the norm.

The Japanese are the pioneers of this quality in manufacturing. I am sure many of you might have heard this story. I don't know if this is true, though.

In those days, when IBM was still manufacturing computers, they wanted to test Japanese manufacturing capabilities. IBM placed a trial order for some computer components and mentioned in the specifications that they will accept only three defective parts per 10,000 parts. On the day of delivery, they delivered the parts in two boxes. One box had 10,000 parts, and another had 3 with a note. "We had difficulty understanding your requirement. With great difficulty, we have produced three defective parts, as per requirements."

The world has adapted lean, kanban, and other quality practices from Japan.

Now when I buy a 40" shirt, I expect it to be of 40" dimension and threads not coming out of buttons. There are no additional points for that. There are expectations of quality for commodities, products, and services.

I want to move along that spectrum and talk about **quality of experience**. It is not something new. In 1977, Mike Markkula wrote a cheque for $250,000 for Apple. That was not his key contribution. He wrote the "marketing philosophy" for Apple, and that defined the trajectory of Apple. It was so simple. It just had only three points - empathy, focus, and impute. I want to talk about the last point —impute. He said people would judge the book by the cover. We might have a creative product, but if we present it in a slipshod manner, people will perceive the product is also slipshod. If we present it creatively, people will impute creative quality to the product. Along with Apple, hundreds of companies have followed this quality of experience to improve their revenue and market share.

Let me explain this quality of experience with **two examples**. 

I have been a customer for Hyundai for the past twelve years. I bought my first car, Accent, from them. Three years back, I exchanged the old Accent for i20. I have been servicing the car for the past three years in the same company where I bought it. Whenever the car is due for service, they will call me. Two weeks back, I serviced the car. This Monday, the battery drained, and the car wouldn't start. When I called them, they said that the battery is three years old, and it is bound to happen. The remedy is to change the battery. They could have reminded me of this when I serviced the car. I could've avoided the embarrassment and inconvenience. The quality of the product was good, the quality of service was good, but they fall short in the quality of experience. They could've given me a better experience.

I want to contrast this with another example. The other experience, this time positive, was with Chayyos. Last month, a friend of mine invited me to Chayyos in Sector 8. We ordered the Kashmir saffron chai. They served the chai, and after some time, one of the servers came with a small packet of sweets. He said, "Sir, you ordered this in Delhi Chayyos. Please accept this as a token of thanks for coming again." The point is not that they gave it for free but that they overwhelmed the customer with a positive experience.

Global competition has moved from commodities to products to services and now to experiences. Tea leaves are the commodities; tea powders are the products, serving saffron tea in five-star hotels are the services. Now specialized stores like Chayyos are competing on the quality of customer experience. If we need to compete in the global arena, we need to start thinking about the quality of customer experience, in addition to the quality of manufacturing and service. These companies **use their service as a stage and products as their stage to engage customers and create memorable events**. 

Next, I want to talk about **quality as a function of the environment**. Many of my peers in the software industry complain of one thing always. That is that the developers don't know how to develop quality code. As an executive from the software development field, I have also faced this problem. So far, I thought this is because of a lack of education or lack of commitment or lack of competency. 

Recently I stumbled on a behavioral psychology equation. A psychologist named Kurt Lewin wrote this equation. It is B = f(P, E), which is **behavior is a function of a person in an environment**. What Lewin says is that our behaviors are driven by our environment. This is coded in the proverb, "tell me your friends, I will tell about you," and "birds of the same feather flock together" If your friends drink alcohol, you will end up drinking alcohol. If your friends are conscious of fitness, you will become one too. Retail stores and e-commerce stores have also mastered this principle. They rearrange the products in such a way we buy expensive items. 

I want to stress my point by narrating another incident involving Japanese. This time, I know it is an actual incident.

The Japanese football team participated in the 2014 football match in Brazil. They played well but lost. The world was not ready for what followed. The Japanese fans went around the stadium and let it as clean as it can be. That is the beauty of growing up in an environment of the highest quality.

How is this equation relevant to quality in global competition? 

Take a young coder in a Western country. I stayed in Belgium for five years, so I believe I can paint a realistic picture.

When he walks to the bus station to take a bus, and the announcement board says the next bus will come in 7 minutes, he knows for sure that the next bus will arrive in seven minutes. Not in five minutes, not in ten minutes, but seven minutes. When he drives, the roads are without potholes throughout the year. He is soaked in an environment where quality is experienced as a norm.

Now consider a coder in India. I stayed in Delhi for five years. So again, I believe I can paint a realistic picture. I used to take the Delhi metro when I lived in Delhi. I should emphasize that, of all the transport systems we have, the Delhi metro is far superior, but it is not reliable throughout the year. There were many days when the announcement board would say the next metro will come in 10 minutes, and we will wait for 20 minutes.

I don't have to talk about the conditions of the road. One monsoon rain and crores of rupees spent on roads are washed out, and potholes appear everywhere.

I'm generalizing here, but the essential point remains. If we grow up in an environment where mediocre quality is the norm, we will not turn up in the office and start building the highest quality products and offer fantastic quality in our services. If we need to compete in international markets, then we need to elevate the quality of our surroundings.

I know what you are thinking. Come on, Joseph, this is a tall order. Elevating our surroundings is not in our hands. Maybe you are right.

But I want to remind you of the speech that our late president, Dr. Kalam, gave in the European Parliament. He recited a Tamil poem that went like this, when there is harmony in the home, there will be order in the nation. The essence of the poem is that the change starts with us. My mentor used to say that if you are not part of the solution, you are the problem. One of my favorite songs is 'man in the mirror,' by Michael Jackson. The lyrics go something like this:

```
I'm starting with the man in the mirror
I'm asking him to change his ways
....
If you want to make the world a better place
Take a look at yourself, and then make a change
```

There is no point in flogging our employees for not producing quality products if we fail to provide quality in the environment. If we need to compete in the international markets, we need to turn selfish and improve our surroundings to the highest standards that we can get to.

We can start with us. We can hold ourselves to the highest standards. We can hold our children to the highest standards. We can build our colleges and offices to the highest standards. Then we can expand that circle to as big as possible.

Let me conclude by summarising. **The first point is that we need to improve our quality standards even if we play only locally. The second point is that we have to go beyond the quality of products and services into the quality of experiences. And the final point is that quality is a function of the environment, and for our selfish reasons, we need to work to elevate our environment.**

Hopefully, I have convinced at least some of you to think differently about quality. If I have done that, I would consider this talk as successful. Thank you again for giving me this opportunity.

_Do you want me to talk in your event? Invite me. Details and previous talks are at [talks](/talks/) page._

## Continue Reading

* [You can beat the giants](/beating-giants)
* [Agile in the C-Suite](/csuite-agile/)
* [How to deliver value in digital age?](/value-in-digital-age/)

