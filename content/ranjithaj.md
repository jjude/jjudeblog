---
title="Ranjitha Jeurkar on 'Nonviolent Communication'"
slug="ranjithaj"
excerpt="Are we becoming polarized because we are not talking to people who are different from us? Or is it because we are polarized, we don't talk to people who are different from us? In either of the cases, talking to people so as to understand their point of view is important."
tags=["gwradio","coach","wins"]
type="post"
publish_at="30 Nov 21 06:09 IST"
featured_image="https://cdn.olai.in/jjude/ranjitha.jpg"
---
We live in a polarised world, which is getting more and more polarised every day. I often wonder are we becoming polarised because we are not talking to people who are different from us. Or is it because we are polarised, we don't talk to people who are different from us. In either of the cases, talking to people so as to understand their point of view is important. To explore this further, I am talking to Ranjitha, a trainer in nonviolence communication. She coaches people in applying the principles of nonviolent communication. I hope to learn more on this topic today.

<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless src="https://share.transistor.fm/e/55ef340b"></iframe>

## Topics Discussed
- What is nonviolent communication? What are its origin?
- Why is it called nonviolent?
- How can we use nonviolent communication at home, office, and in society?
- How can employees use NVC with their bosses?
- As work is moving more and more online, we miss all the cues that happens in physical world. In that scenario, how can we use NVC to communicate better?
- What kind of coaching you offer in NVC?

## Resources mentioned in the episode:

- For kids: http://www.thenofaultzone.com/feelings-needs-cards-english.pdf
- NVC work in prison: Words that liberate / Ces mots qui libèrent : https://youtu.be/nmPYRgi8VFg
- Living Bridges: https://encompassion.com/living-bridges/

![Nonviolent Communication with Ranjitha Jeurkar](https://cdn.olai.in/jjude/ranjitha.jpg)

## Connect with Ranjitha:

- Instagram: https://www.instagram.com/connextcoaching/
- LinkedIn: https://www.linkedin.com/in/ranjithajeurkar/
- Twitter: https://twitter.com/ranjithajeurkar
- Website: www.connextcoaching.com
- Newsletter: https://connextcoaching.substack.com/

## Connect with me
- Twitter: https://twitter.com/jjude
- LinkedIn: https://www.linkedin.com/in/jjude/
- Website: https://jjude.com/
- Newsletter: https://jjude.com/subscribe/
- Youtube: https://youtube.com/gravitaswins
- Email: podcast@jjude.com
- Executive Coaching Program: https://gravitaswins.com

## Your feedback counts

Thank you for listening. If you enjoy the podcast, would you please leave a short review on Apple podcast or on YouTube? It takes less than 60 seconds, and it really makes a difference in finding this podcast. And it boosts my spirits. 😀

I would like to hear what resonated the most with you in this episode. Either you can email me or share it on social media and tag me. Thank you for your support.

Have a life of WINS.

_Gravitas WINS Radio is produced by [Joseph Jude](https://jjude.com/). Be sure to subscribe on [Apple Podcast](https://podcasts.apple.com/in/podcast/gravitas-wins-radio/id1562802449), [Spotify](https://open.spotify.com/show/7LOBjAyjBkechMC0xMIj9E),[Google Podcast](https://podcasts.google.com/feed/aHR0cHM6Ly9mZWVkcy50cmFuc2lzdG9yLmZtL2dyYXZpdGFzLXdpbnMtcmFkaW8=),[YouTube](https://www.youtube.com/gravitaswins), or the podcasting app of your choice._

Please check out other episodes of [Gravitas WINS Radio](/gwr-themes/). For easy consumption, they are categorized by themes. If you are a fellow podcaster, you might be interested in [How I Podcast](/how-i-podcast/) or [Tools I Use](/i-use/).

## Continue Reading

* ['Words Change Lives' with Ritika Singh](/ritikas/)
* [Listening leaders are effective leaders](/ian-listening/)
* [What Is Gravitas, Anyway?](/gravitas/)