---
title="Thriving with WINS"
slug="wins"
excerpt="To thrive in a changing world, you need to have a framework of your own. Here I propose one."
tags=["coach","wins","frameworks","flywheel"]
type="post"
publish_at="25 Aug 19 06:15 IST"
featured_image="https://cdn.olai.in/jjude/wins-flywheel.jpg"

---

If you google "how to be successful," I am sure it will return millions of results. If you search Amazon books, I am sure it will return at least a hundred books. There is no end to reading books. But to succeed, you need to create your methods and get to work on becoming successful.

After reading many books and posts, I have codified the abstract principles I learned into a framework. A significant part of the idea came from the book "[Thou Shall Prosper](https://geni.us/thou-shall-prosper)," by Rabbi Daniel Lapin. In this book, he talks about four motivators for success — Wisdom, Esteem, Wealth, and Power. I've modified it into my needs and calling it WINS framework for success. WINS stands for:

- Wealth
- Insights
- Network
- Self-control

Because of your inheritance and genes, you could already have a head-start in any of these factors. But let's go one by one.

!["WINS Flywheel"](https://cdn.olai.in/jjude/wins-flywheel.jpg)

**Insights are the principal thing.** Whatever else you may gain, get insights. You could read books, watch TED talks, listen to great speakers to get a spark of an idea. But you have to roll-up your sleeves and create something if you want to internalize these ideas. Then you have to let your creation out in the world to engage others. Let others use it, judge it, and comment on it. Usually, these engagements will make you think in a way you never considered.

**Insights lead to esteem**. That esteem builds your network. As Dharmesh Shah [tweeted](https://twitter.com/dharmesh/status/504480566084128769), "Your probability of success is proportional to the number of people that want you to succeed. Work to keep increasing that number." Every talent you have intertwines with your network to reach success.

As you build your network, you will **find wealth in your network**. Wealth, like success, is a nebulous term. Everybody has their definition. "Wealth is discretionary time," says the millionaire consultant, [Alan Weiss](https://twitter.com/BentleyGTCSpeed) and I go with that definition. You may accumulate riches, but if you can't spend time with your kid as you like, that riches does no good to you.

Success is hard. Retaining success is harder. You would think the change in business-models, change in government policies, and technological disruptions are the villains for your success. **Lack of self-control is a bigger threat to your success** than any other external factor.

As you improve yourself on these factors, you will thrive through life.

How to improve on these individual factors? With ideas borrowed from others, I have put together micro-frameworks for each of these factors. Here it goes:

**Wealth**

- Ways to build wealth
  - Linear (Exchanging time for money. Examples: Salary, overtime, commission, consulting, professional services)
  - Leverage (Leveraging other people to make money. Examples: sub-contracting, salaried staff, strategic alliances, running a company)
  - Passive (Income while not working. Example: renewals, interests, dividend, books)
  - Windfall (Sudden large income. Example: winning lottery, appreciation of stocks and real estate)

**Insights**

- Ways to build insights
  - Consume (Books, TED Talks, Podcasts, White-papers)
  - Produce (Blog posts, Talks, Books, Podcasts)
  - Engage (On blogs, social media, networking events)

**Networking**

- Visibility (become visible to your potential network and actively maintain it)
- Credibility (keep your appointments and promises, render services; let your results speak louder than words)
- Profitability (both benefit from the relation)

**Self-control**

- Wellness
  - Mind
  - Body
  - Spirit
  - Social

As you improve on each of these factors, you build a virtuous cycle of success. Keep going from one level of success to another.

If you are interested in hearing about these factors, please subscribe using the form below.
