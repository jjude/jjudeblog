---
title="Capt. Sahaya Arputharaj on 'Leadership on High Seas'"
slug="sahayaa"
excerpt="Flowing with the sea in its beauty and fury"
tags=["gwradio"]
type="post"
publish_at="02 Nov 21 07:24 IST"
featured_image="https://cdn.olai.in/jjude/appu.jpg"
---

<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless src="https://share.transistor.fm/e/ba104a75"></iframe>

For those of us in the software industry, the toughest situations are man-made. In this episode, I talk to a captain in merchant navy who has to navigate against nature's fury at times. Listen in to know about risks, types of leadership, and drills to prepare yourself to face dangers.

![Leadership on high seas](https://cdn.olai.in/jjude/appu.jpg)

- How does one get into merchant navy?
- What are the rituals for new seamen when they come on board?
- Most often your colleagues in one voyage are not the ones in another voyage. How do you build professional friendship in such environment?
- Most of your days are risky dealing with nature and other risks. How are you trained to handle risks?
- Give me an example of the challenging times you had on the sea?
- How is merchant navy adapting to the modern times?

### Watch on YouTube

<iframe width="560" height="315" src="https://www.youtube.com/embed/aIJSu-keZe4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Connect with Capt. Sahaya Arputharaj:

- LinkedIn: https://www.linkedin.com/in/sahaya-arputharaj-4178515a/

## Connect with me
- Twitter: https://twitter.com/jjude
- LinkedIn: https://www.linkedin.com/in/jjude/
- Website: https://jjude.com/
- Newsletter: https://jjude.com/subscribe/
- Youtube: https://youtube.com/gravitaswins
- Email: podcast@jjude.com
- Executive Coaching Program: https://gravitaswins.com

## Your feedback counts

Thank you for listening. If you enjoy the podcast, would you please leave a short review on Apple podcast or on YouTube? It takes less than 60 seconds, and it really makes a difference in finding this podcast. And it boosts my spirits. 😀

I would like to hear what resonated the most with you in this episode. Either you can email me or share it on social media and tag me. Thank you for your support.

Have a life of WINS.

## Amazing tools I use:
- I record all the remote interview with [Riverside.fm](https://riverside.fm/?via=joseph-jude)
- I use [Transistor.fm](https://transistor.fm/?via=jjude) for hosting these podcast episodes

_Gravitas WINS Radio is produced by [Joseph Jude](https://jjude.com/). Be sure to subscribe on [Apple Podcast](https://podcasts.apple.com/in/podcast/gravitas-wins-radio/id1562802449), [Spotify](https://open.spotify.com/show/7LOBjAyjBkechMC0xMIj9E),[Google Podcast](https://podcasts.google.com/feed/aHR0cHM6Ly9mZWVkcy50cmFuc2lzdG9yLmZtL2dyYXZpdGFzLXdpbnMtcmFkaW8=),[YouTube](https://www.youtube.com/gravitaswins), or the podcasting app of your choice._

Please check out other episodes of [Gravitas WINS Radio](/gwr-themes/). For easy consumption, they are categorized by themes. If you are a fellow podcaster, you might be interested in [How I Podcast](/how-i-podcast/) or [tools I use](/i-use/).