---
title="Are you sure you are not shaving the yak?"
slug="shave-the-yak"
excerpt="Beware of productivity distractions"
tags=["wins","productivity"]
type="post"
publish_at="18 Nov 20 09:00 IST"
featured_image="https://cdn.olai.in/jjude/shaving-yak.png"
---

!["Are you shaving the yak"](https://cdn.olai.in/jjude/shaving-yak.png)

The other day I wanted to clean the house. When I started, I found out that there were no trash bags. I needed the car to get trash bags, but the car was so dirty. So I cleaned the car. When I switched on the car, I found that I need to fill gas immediately. So I went to the petrol station, and then to the shop. When I finally came back home, I had lost two full hours.

Such distractions happen in my professional life way too often. I coded the app that runs my website. Some day, I will find out a bug in a particular file. When I'm about to fix it, I will notice that the file is way too big, and I should separate the file into different files. When I set out to do it, I usually get myself into a deep mess of not fixing the bug and introducing more bugs. Now I'm frustrated.

"Shaving the yak" is a term coined in MIT computer labs, which means **doing something that is only slightly related to what you are doing, just because you think it is important to the original task**.

Tell me, do you shave yaks?

## Continue Reading

* [Productivity Hacks That Work](/productivity-hacks-that-work)
* [If you see everything, you'll get nothing](/focus-to-win)
* [How to get lucky?](/luck)