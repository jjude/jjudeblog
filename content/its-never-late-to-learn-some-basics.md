---
title="Its never late to learn some basics"
slug="its-never-late-to-learn-some-basics"
excerpt="Learning the basics of investing."
tags=["wealth","wins"]
type="post"
publish_at="05 Jul 08 16:30 IST"
featured_image="https://cdn.olai.in/jjude/its-never-late-to-learn-some-basics-gen.jpg"
---

Is it common that IT folks don't understand finance and its load of jargons? I think so.

I've tried to understand financial concepts by reading many, many pages of hard-bound books and whatever I could find on the net.

I even tried 'shares trading' so that it will force me to learn those concepts. Did it help? Na, I just lost money; never gained any knowledge. I lost hope of understanding those concepts.

[![](http://ecx.images-amazon.com/images/I/51%2BiB01%2BcHL._SL75_.jpg)](http://www.amazon.com/Rule-Strategy-Successful-Investing-Minutes/dp/0307336840%3FSubscriptionId%3D0525E2PQ81DD7ZTWTK82%26tag%3Dws%26linkCode%3Dsp1%26camp%3D2025%26creative%3D165953%26creativeASIN%3D0307336840 "Rule #1")&#160; That is when I came across '[Rule #1](http://www.amazon.com/Rule-Strategy-Successful-Investing-Minutes/dp/0307336840%3FSubscriptionId%3D0525E2PQ81DD7ZTWTK82%26tag%3Dws%26linkCode%3Dsp1%26camp%3D2025%26creative%3D165953%26creativeASIN%3D0307336840)' by [Phil Town](https://en.wikipedia.org/wiki/Phil_Town) in the near-by library. Amidst scores of books that make you feel investment concepts is a rocket science, this one is a gift. Phil explains the concepts in an easy to understand, layman's terms. I haven't yet finished the book; but I've understood whatever I've read so far and it all make sense.

Phil says 'don't invest; but own a business'. This comes closer to [Ricardo Semler](https://en.wikipedia.org/wiki/Ricardo_Semler)'s business theory of influencing employees to think 'I am not cutting stones; but building a cathedral'. I am a subscriber of such thought process and probably because of that Phil's philosophy appealed to me. Phil quotes a 10-10 rule - 'Don't own a business for 10 minutes if you are not willing to own it for 10 years'.

He then goes on to explain 'The Big Five' - the parameters to look for to choose a company to invest; oops sorry to own.The big five are:

*   Return on Investment Capital
*   Sales Growth Rate
*   Earnings per Share Growth Rate
*   Equity or Book Value per Share Growth Rate
*   Free Cash Flow Growth Rate

He claims that there should be a 10% growth rate for the past 10 years. Wow! that is a strict evaluation. But if I invest my hard-earned money, I should be certain that I'll reap its benefits.

Once I read the first 6 chapters, I wanted to find out how many Indian companies fall into this bracket. Well, it turns out that none of the sites, not even [NSE](http://www.nse-india.com/), provide 10 years data. So I've to be content with 5 years of financial data.

[NSE](http://www.nse-india.com/) provides the date on which a company is listed with them. I've filtered those companies which are present in the market for 10 years. In that list, I've selected those companies that 'means' something to me. I can't wait to see what companies I can own!

