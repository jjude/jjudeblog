---
title="Are You Learning A New Domain? Visit Its Zoo"
slug="are-you-learning-a-new-domain-visit-its-zoo"
excerpt="If you are learning about a new domain, learn about its individual entities well before jumping into its ecosystem."
tags=["sdl","systems","wins","insights"]
type="post"
publish_at="28 Oct 13 16:30 IST"
featured_image="https://cdn.olai.in/jjude/are-you-learning-a-new-domain-visit-its-zoo-gen.jpg"
---


C was the first programming language I learned on my own. I was lucky to learn from the creator himself; I learned from 'The C Programming language' by Brian W. Kernighan and Dennis M. Ritchie. They introduce the most powerful software language in just five lines.

    #include<stdio.h>
    main()
    {
    printf("hello, world\n");
    }

Then they go on to explain each line.

In simple words they go on to teach you everything about C. When you finished the book, you will be a master of the language.

But ...

Is that sufficient enough to create a real-world application?

What about data-structures, APIs, Version control, Deployment methods?

They don't make the assumption that the reader of the book is going to create a real-world application for the public. They took an elephant and introduce you to every aspect of the elephant; if you are interested in knowing how the elephant survives in a jungle you have to go further.

I didn't abstract this into a learning framework then; but, I used this systematic framework to learn every new domain.

When I learned English (I'm a non-native), I learned few words and little grammar without worrying about real-world situation like speaking in a meeting or influencing through communication. When I started to converse in English, which was in college[^1], whatever little I said brought laughter from others. But I separated out what I had to learn and followed the model of the C book -- I learned vocabulary, essential grammar, writing and finally speaking. Now, I'm fairly confident of any form of communication and can even steer conversation towards objectives that I set.

I followed this approach to learn photography, project management and retail investing.

Although I was using this approach extensively, it was happening unconsciously. I never formulated a theory for this approach. That was until I read Donella Meadows', [Thinking in Systems][3][^2]. In a chapter titled, 'A Brief Visit to the Systems Zoo,' she says,

> It gives you an idea of the large variety of systems that exist in the world, but it is far from a complete representation of that variety. It groups the animals by family - monkeys here, bears there - so you can observe the characteristic behaviors of monkeys, as opposed to bears. But, like a zoo, this collection is too neat. ...Ecosystems come later.

A light bulb went off! I was unknowingly practicing 'zoo theory of learning' - group family of things together, study them carefully and when you are confident of the individual pieces, go explore the ecosystem. Donella is brilliant!

[^1]: Before that, I never conversed in English. I could recite few memorized sentences but never converse. In fact, for a long time in school, I would learn my English lessons in Tamil, my mother-tongue, then will reconstruct the sentences during written exams. I was good at it. Thank god, there were no oral exams then!
[^2]: Disclaimer: This is a Amazon associates link. If you purchase through this link, I get a commission.

[3]: https://amzn.to/2Ik3TjG

