---
title="Can geeks delight?"
slug="geeks-delight"
excerpt="An eye for detail, a heart for your customers, and an unmercenary business model is all you need to build a remarkable business."
tags=["coach","sales"]
type= "post"
publish_at= "17 Mar 20 08:30 IST"
featured_image="https://cdn.olai.in/jjude/rootconf-badge.jpg"

---

If my experience with HasGeek is any yardstick, then the answer is a resounding yes.

[HasGeek](https://hasgeek.com/) organizes technology conferences. Their themes are around big data, data privacy, DevOps, react, and payment infrastructure. Really tech stuff.

I had heard good things about them, primarily via Twitter. I wanted to experience all the great things said about them. I got that chance in Jan 2020 when they announced a DevOps conference called [RootConf](https://hasgeek.com/rootconf/2020-delhi/). One distinguishing fact of HasGeek booking is that the ticket is **fully refundable until the day before the event**. What is there to lose? I registered immediately.

I was waiting to be geekily delighted at the conference.

They started delighting well before that.

Few days after the registration, I got an email titled "Rootconf Delhi - Partial Ticket Refunds." It read like this:

> As you are aware of, ticket prices are determined using various factors - including commitment from sponsors. As it stands today, we have more sponsors committed for sponsoring this conference, than we had originally estimated, while determinig our ticket prices.

> Hence we can afford to drop our ticket prices from today, for new buyers. Since the ticket price that you had paid is higher than today's price, **we are issuing partial refunds** of the difference amount.

Are they crazy? Which company refunds just because they got more sponsors? Come on, pocket the profit, man.

It showed their priorities. They are **not mercenaries**, which compelled me to wish for their continued success.

The event day was better than I expected. As soon as we arrived, the volunteers handed a t-shirt and a badge. The badge had all the program details. We don't have to run around to know the session to attend. **They had an eye for detail**.

![Badge](https://cdn.olai.in/jjude/rootconf-badge.jpg)

There was no fluff in the talks as each speaker spoke from the first-hand experience. Even though each talk was deeply technical, organizers had worked with the speakers to ensure **every session flowed like a story**.

Let me give you the talk I enjoyed the most as an example. Hotstar streams movies, cricket matches, and other TV shows. When you watch a cricket match in a stadium, you will give a high-five whenever the team you support hits a six or gets a wicket. The Hotstar team wanted to **simulate that experience in their streaming platform** by enabling the users to send short real-time messages whenever such events happened. Piyush Gupta, who led this initiative in Hotstar, talked about their trials and challenges in the successful delivery of 700+ Million messages in near-real-time with zero downtime. **While the session was deeply technical, Piyush managed to take us through the journey of what happened inside Hotstar in a storytelling format**. He talked about their audacious goals and hypothesis, what they did to meet those goals, why those tries didn't succeed, how they exhausted AWS'AWS' messaging capacity, and finally, the excitement when they sent those many messages on that eventful day. Sitting in the audience, Piyush made us feel as though we lived through those frustrating and exciting moments.

When I congratulated him later during lunch, he mentioned that **the organizers worked with him to polish the talk into that exciting storytelling format**.

There were informal talks too.

As software eats the world, technology has become intertwined with politics. [Devdutta](https://twitter.com/Devdutta96), a lawyer by training, led an insightful discussion on technology and politics. She made an insightful point: Earlier covering your face with a mask will make you a criminal. Now (pollution), **masks have become essential because of the polluted environment. The same goes for VPNs.** Using a VPN is no more optional. The prevailing climate of snooping, mostly by governments, should force us to use a VPN.

Few days after the event, the HasGeek team published all [videos for free](https://hasgeek.tv/rootconf/2020-delhi). If you are in the software domain, as a developer, architect, or even as a CTO, I encourage you to watch [PubSub: real-time messaging service at Hotstar by Piyush Gupta](https://hasgeek.tv/rootconf/2020-delhi/1856-pubsub-real-time-messaging-service-at-hotstar-by-piyush-gupta). You will learn **how to solve a hard problem with the hypothesis, experiments, and iterations**.

If you like to learn, the latest technology trends attend one of the [HasGeek conferences](https://hasgeek.com/). **They will update you and delight you simultaneously**. You can't ask for more.

## Continue Reading

- [Delightful Customer Experience in a Highway Restaurant](/cx-in-highway/)
- [Microsoft still competes on price, not on customer obsession](/msft-mercenary/)
- [System for success](/system-for-success/)
