---
title="Book Summary - The McKinsey Way"
slug="mckinsey-way"
excerpt="Key takeaway from this book is, there should be structure in your thinking, analysis and communication for effective problem solving."
tags=["books","systems"]
type="post"
publish_at="20 Jan 11 16:30 IST"
featured_image="https://cdn.olai.in/jjude/mckinsey-way.png"

---

Drawing on his own years in McKinsey as well as extensive interviews with current and former McKinsey-ites, Ethan Rasiel takes us through the proven approaches used in McKinsey for solving business problems.

Through out the [book](https://geni.us/McKinsey-Way), the author emphasizes the rigorous training of Mckinsey-ites to be structured in every aspect of their approach to finding solutions to business problems - be it thinking or working or selling of solutions.

Also there is a magic number of three in every approach. As the author notes, everything comes in three in the firm (how he calls McKinsey). This again is a form of bringing structure into every dealing.

I have drawn a map of the (relevant) concepts mentioned in the book on problem solving.

![Concept Map of the book McKinsey Way](https://cdn.olai.in/jjude/mckinsey-way.png)

## Thinking about business problems

When you start with problem solving, gather as many facts as possible. Most consultants are generalists, even the experienced ones. For such generalists, facts compensate for the gut instincts. Further facts give credibility when dealing with the stakeholders.

When gathering issues that cause the problem, be exhaustive but avoid overlaps (what McKinsey calls mutually exclusive and completely exhaustive).

With the facts and MECE issues, generate your initial hypothesis. Never be afraid to frame a hypothesis. And then test your initial hypothesis. How do you test? Brainstorming. That is why problem solving should not be a solo venture. You need others to pick apart your ideas. Later in the book the author also warn never to invest your ego with your hypothesis. After all it is a hypothesis.

## Working to solve business problems

On a day-to-day basis the problem solving model includes research, interviews and brainstorming. You should browse trade magazines and annual reports of the client. They give you the idea of jargons, best practices and current issues the particular industry faces.

Magazines and reports can give generic informations but the specifics of the problems are in the heads of front-line people. Listening to their experience and anecdotes provide essential insights into the problem. So go talk to them.

Update every one of the issues, results of research and interview and let them form their own hypothesis. But the real work of problem solving starts only with brainstorming. Pick apart everyone's hypothesis to arrive at a solution.

## Selling the solution

The best solution, no matter how well researched, analyzed and solved, is worthless if your client don't buy it. If they have to buy it you have to sell it. One of the components of that selling is a well structured presentation. But real selling doesn't happen during the presentation. It happens well before everyone gather for the presentation. You should walk the audience through the major points beforehand.

Communication is the lifeline of a team-based operation. Internal communication as well as with those with client's team should be short, thorough and structured.

Client's team play a crucial role in any engagement and hence it is important to keep the client's team on your side. Know that there will be politics and ensure politics favors you.

The key takeaway from the [book](https://geni.us/McKinsey-Way) is that there should be structure in your thinking, analysis and communication for effective problem solving.

_This post is part of '[Be a Problem Solver](/be-a-problem-solver/)' series._
