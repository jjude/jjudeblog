---
title="Work, Life, and Fun is a flywheel not a balance"
slug="work-family-fun-flywheel"
excerpt="Add to life what generates energy and remove what drains it"
tags=["wins","coach","flywheel","visuals"]
type="post"
publish_at="21 Jun 22 14:18 IST"
featured_image="https://cdn.olai.in/jjude/work-family-fun-flywheel.jpg"
---
![](https://cdn.olai.in/jjude/work-family-fun-flywheel.jpg)

The concept of work-life balance is popular today. The word "balance" indicates that we are discussing a trade-off. In order to enjoy more of one, you must sacrifice another.

Author Ryan Holiday elaborates this idea with  an advice he got from author Austin Kleon about choices in life. Austin has a little rule:

> Work, Life, Scene. Pick two

Work - Whatever you are doing for making money - business, job, and side-hustles

Life - Spouse, kids, close relations. 

Scene - the fun stuff that comes along. Parties, dinners and other fun stuff that looks good on Instagram; the things you can brag about.

Among the three you can only pick two.

This line of thought indicates that, having a warm, loving family and plenty of time for a career and weekend partying is impossible. You cannot be a leader and free of responsibility at the same time. Job-security of a corporate job cannot be paired with the exponential growth of a startup. Life is basically a trade-off. Once you've chosen one you have to let go of another.

I disagree with this idea. You lose a part of your soul and hence your life when you try to strike a balance or attempt a trade-off.

I like the metaphor proposed by Jeff Bezos on this topic. He says,

> It's a flywheel, a circle, not a balance.

He explains it further:

> I am energized at work, happy at work, feeling like I’m adding value, part of a team, whatever energizes you, that makes me better at home. It makes me a better husband, a better father. Likewise, if I’m happy at home, it makes me a better employee, a better boss.

It all depends on whether one part of the flywheel adds or drains energy.

I've been blessed that work and family give me so much energy that I don't need to go to parties to refresh myself. Even so, there are times when I go out with like-minded friends and come back energised.

Work, life, and fun are not mutually exclusive. Balance is not necessary. There is no need to compartmentalize and choose one over the other.

You are one. Your days are flywheels. Add what generates energy and remove what drains it.

### References
- [Work, Family, Scene: You Can Only Pick Two](https://ryanholiday.net/work-family-scene/)
- [Work-Life Harmony](https://www.youtube.com/watch?v=xfGbyW6fs5w)
## Continue Reading

* [Map Is Not Territory](/map-is-not-territory/)
* [Building A Personal Flywheel](/flywheel/)
* [Thriving With Wins](/wins/)