---
title="Tamanna Sharma on 'Your Roadmap To A Career In Data Science'"
slug="tamanna-ds"
excerpt="In order to thrive in the field of Data Science, you need a lot more than just Python and statistics knowledge."
tags=["gwradio","coach","wins","career"]
type="post"
publish_at="14 Nov 23 14:19 IST"
featured_image="https://cdn.olai.in/jjude/tamanna-yt.jpg"
---
With the explosive growth of generative AI, data is not just oil, it is the new art. Data scientists are the new artists. We have one such awesome artist with us today. We are going to explore how to get into data science, how to grow in it, and everything in between.

<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless src="https://share.transistor.fm/e/a3ed3297"></iframe>

## What you'll hear
- How did Tamanna get into data science field?
- Sources that helped Tamanna
- How to gain practical knowledge in data science?
- Availability of datasets
- Importance of Maths & Statistics
- Python or R?
- Typical work-week in data science
- Importance of business skills
- How to get started in data science?
- Myths about data science
- Kindest thing anyone has done
- Best leadership quality
- Living a good life

## Edited Transcript
_coming soon_

## Watch on YouTube
<iframe width="560" height="315" src="https://www.youtube.com/embed/whAf1Ii-cN0?si=5z2CqgqHcTF7u-xr" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

## Connect with Tamanna Sharma
- LinkedIn: https://www.linkedin.com/in/tamanna-sharma-4bbb66a1/

## Connect with me
- Twitter: https://twitter.com/jjude
- LinkedIn: https://www.linkedin.com/in/jjude/
- Website: https://jjude.com/
- Newsletter: https://jjude.com/subscribe/
- Youtube: https://youtube.com/gravitaswins
- Email: podcast@jjude.com
- Executive Coaching Program: https://gravitaswins.com

## Your feedback counts

Thank you for listening. If you enjoy the podcast, would you please leave a short review on Apple podcast or on YouTube? It takes less than 60 seconds, and it really makes a difference in finding this podcast. And it boosts my spirits. 😀

I would like to hear what resonated the most with you in this episode. Either email me or share it on social media and tag me. Thank you for your support.

Have a life of WINS.

_Gravitas WINS Radio is produced by [Joseph Jude](https://jjude.com/). Be sure to subscribe on [Apple Podcast](https://podcasts.apple.com/in/podcast/gravitas-wins-radio/id1562802449), [Spotify](https://open.spotify.com/show/7LOBjAyjBkechMC0xMIj9E),[Google Podcast](https://podcasts.google.com/feed/aHR0cHM6Ly9mZWVkcy50cmFuc2lzdG9yLmZtL2dyYXZpdGFzLXdpbnMtcmFkaW8=),[YouTube](https://www.youtube.com/gravitaswins), or the podcasting app of your choice._

Please check out other episodes of [Gravitas WINS Radio](/gwr-themes/). For easy consumption, they are categorized by themes. If you are a fellow podcaster, you might be interested in [How I Podcast](/how-i-podcast/) or [Tools I Use](/i-use/).

## Continue Reading

- [Ganesan Ramaswamy on 'Building An  Enterprise Data Strategy'](/ganesanr/)
- [Liji Thomas on 'Conversational Bots'](/lijit/)
- [Lee Launches on 'Application Development With No Code And Low Code Tools'](/lee-lcnc/)