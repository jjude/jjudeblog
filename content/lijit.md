---
title="Liji Thomas on 'Conversational Bots'"
slug="lijit"
excerpt="What is the origin and future of conversational bots and how to create and deploy them?"
tags=["gwradio","tech"]
type="post"
publish_at="21 Sep 21 09:22 IST"
featured_image="https://cdn.olai.in/jjude/lijit-gen.jpg"
---

<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless src="https://share.transistor.fm/e/46957ca2"></iframe>

- 2nd Copernican Revolution and HCAI
- Successful use cases of chatbots
- Why Conversational AI?
- Guidelines for developing chatbots
- How to take care of local sensitivies which may be specific to certain areas and cultures?
- Should all software engineers should learn anthropology and psychology?
- What makes a successful chatbot?
- Advice for any new manager

### Watch on YouTube

<iframe width="560" height="315" src="https://www.youtube.com/embed/fZGJscLSdnQ" title="Conversational Bots" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Connect with Liji Thomas

- LinkedIn: https://www.linkedin.com/in/liji-thomas/
- Twitter: https://twitter.com/lijithomas2020/

## Connect with me
- Twitter: https://twitter.com/jjude
- LinkedIn: https://www.linkedin.com/in/jjude/
- Website: https://jjude.com/
- Newsletter: https://jjude.com/subscribe/
- Youtube: https://youtube.com/gravitaswins
- Email: podcast@jjude.com
- Executive Coaching Program: https://gravitaswins.com

## Your feedback counts

Thank you for listening. If you enjoy the podcast, would you please leave a short review on Apple podcast or on YouTube? It takes less than 60 seconds, and it really makes a difference in finding this podcast. And it boosts my spirits. 😀

I would like to hear what resonated the most with you in this episode. Either you can email me or share it on social media and tag me. Thank you for your support.

Have a life of WINS.

## Amazing tools I use:
- I record all the remote interview with [Riverside.fm](https://riverside.fm/?via=joseph-jude)
- I use [Transistor.fm](https://transistor.fm/?via=jjude) for hosting these podcast episodes

_Gravitas WINS Radio is produced by [Joseph Jude](https://jjude.com/). Be sure to subscribe on [Apple Podcast](https://podcasts.apple.com/in/podcast/gravitas-wins-radio/id1562802449), [Spotify](https://open.spotify.com/show/7LOBjAyjBkechMC0xMIj9E),[Google Podcast](https://podcasts.google.com/feed/aHR0cHM6Ly9mZWVkcy50cmFuc2lzdG9yLmZtL2dyYXZpdGFzLXdpbnMtcmFkaW8=),[YouTube](https://www.youtube.com/gravitaswins), or the podcasting app of your choice._

Please check out other episodes of [Gravitas WINS Radio](/gwr-themes/). For easy consumption, they are categorized by themes. If you are a fellow podcaster, you might be interested in [How I Podcast](/how-i-podcast/) or [tools I use](/i-use/).

## Continue Reading

- ['Building An Enterprise Data Strategy' with Ganesan Ramaswamy](/ganesanr/)
- ['Why Robotic Process Automation Matters To Enterprise' With Jyothi & Aravinda](/jyothi-rpa/)
- [How to deliver value in the digital age?](/value-in-digital-age/)
