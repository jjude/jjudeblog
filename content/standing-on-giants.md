---
title="Standing on giants"
slug="standing-on-giants"
excerpt="Motivation is a big part in getting through tough challenges. Drawing inspiration from others can help."
tags=["coach"]
type="post"
publish_at="16 Feb 09 16:30 IST"
featured_image="https://cdn.olai.in/jjude/standing-on-giants-gen.jpg"
---


"Consulting is a tough business", says [Alan Weiss](https://alanweiss.com/).

You better listen when a million dollar consultant speak.

From opening a bank account to steering a challenging project can be tough, especially when you are starting out as an independent consultant.

So how do you get through?

Motivation is a big part in getting through tough challenges. I draw motivation from family, friends, colleagues, popular icons. Even events can be source of motivation.

Paraphrasing Sir Issac Newton's words, "If I've succeeded, it is because I stood on the shoulders of the giants.

Standing on giants helped me so far; will it get me further? Only time will tell.

