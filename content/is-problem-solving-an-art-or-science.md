---
title="Is Problem Solving an Art or Science?"
slug="is-problem-solving-an-art-or-science"
excerpt="Problem solving is as much a science as an art."
tags=["problem-solving"]
type="post"
publish_at="24 Mar 11 16:30 IST"
featured_image="https://cdn.olai.in/jjude/is-problem-solving-an-art-or-science-gen.jpg"
---

Problem solving is taught and deemed as numbers game.

The general practice is to fire-up Microsoft Excel (or its equivalent), list parameters to evaluate, give weightage to these parameters and then list out numbers in favor or against each of the options. Every problem solving method is a derivative of [SODAS](/sodas/) methodology.

But does this cover all aspects of problem solving?

Let us take a simple example of buying a tablet. For simplicity, let us limit the choices between Apple iPad and Samsung Tablet.

Factor  | iPad | Samsung Tab

