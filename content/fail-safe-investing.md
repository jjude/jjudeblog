---
title="Book Review - Fail Safe Investing"
slug="fail-safe-investing"
excerpt="World of investment doesn't have to be a parallel universe controlled by prophecy, mysterious symbols, and strange ideas. Harry Browne shows it can be safe and simple."
tags=["books","wealth","wins"]
type="post"
publish_at="29 Aug 10 11:34 IST"
featured_image="https://cdn.olai.in/jjude/fail-safe-investing-gen.jpg"
---

Investment literacy is essential for building and keeping wealth. But for most us the world of investment is "a parallel universe controlled by prophecy, mysterious symbols, obscure cycles, and strange ideas about how human beings behave." It doesn't have to be that way; it can be safe and simple is the message that [Harry Browne](https://en.wikipedia.org/wiki/Harry_Browne) is spreading through this tiny book.

[![Fail Safe Investing by Harry Browne](http://img.fkcdn.com/img/thumb/218/9780312263218.jpg "Fail Safe Investing by Harry Browne")](https://amzn.to/3oreFDZ)

The book encourages a commonsense based, practical approach to building wealth rather than on speculation. So you won't find any tips about getting rich quickly or retiring in Bahamas.

There are 17 rules in Part I of the book. In Part II, the author provides more information on these 17 rules. For a glimpse into the book, I discuss three such rules.

**Rule #1: Build Your Wealth Upon Your Career** - I never heard this rule before and if you have not read this book earlier, you wouldn't have heard it too. The author emphasizes that your career and investment together build a prosperous and secure future. Yes, investing will build wealth but it is your career that gives you the money to invest. So keep your feet on the ground and remember, you got to go to work tomorrow.

**Rule #7: Invest Only On a Cash Basis** - When you see a 'golden opportunity', borrowing money can seem like a smart way to exploit the opportunity. But borrowed money can just easily enlarge your loss as increase your profit.

**Rule #11: Build a Bulletproof Portfolio For Protection** - This rule is the crux of the book. According to the author, economic circumstances can be broadly classified into prosperity, inflation, recession and deflation. To be protected in all circumstances, there must be at least one investment in the portfolio that responds well to it. The author identifies stocks, bonds, gold and cash as the investment instruments for these economic circumstances. He also proposes an equal distribution of these instruments within the portfolio.

Other rules are as conservative as these. It is better to be conservative and preserve the capital rather than end up in the investment graveyard.

This is one of the book that defines my investment principle (others are about 'Value Investing'). Because of these simple and safe investment principles, I sleep better at night.

I will finish with the often repeated statement in the book. Whenever someone pressures you to strike while the iron is hot, remember: When in doubt about an investment decision, it is always better to err on the side of safety.

Good luck with your safe investing.

_Disclosure: Some of the links in the post above are “affiliate links.” This means if you click on the link and purchase the item, I will receive an affiliate commission. Regardless, I only recommend products or services I use personally and believe will add value to my readers._

### You may also like:
- [Topics of interest - 2019-20](/topics1920)
- [Books I’ve Read](/books-read)
