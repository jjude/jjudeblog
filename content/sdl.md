---
title="Self-Directed Learning (SDL)"
slug="sdl"
excerpt="Learning is pivotal to success in today's business. Here is a framework to help you learn quickly and effectively."
tags=["sdl"]
type="page"
publish_at="18 Feb 14 16:30 IST"
featured_image="https://cdn.olai.in/jjude/learning.png"
---
Learning is pivotal to success in today's business. But we don't know how to learn, unlearn and re-learn. I have formed a simple framework consume-produce-engage for self-learning. Here I share the framework and how I use it. Bookmark this page and come back for updates. Or subscribe via [RSS](/feed.xml).

![Learning Phases](https://cdn.olai.in/jjude/learning.png)

[Learning Phases And Its Support Systems](/learning-phases-and-its-support-systems/): Start here. I introduce the framework and talk about all aspects of the framework.

[How To Be A Champion And Remain One?](/how-to-be-a-champion-and-remain-one/): An initial post about the framework.

[Build An Ecosystem For Learning](/build-an-ecosystem-for-learning/): Learning is a living being. Living happens within a ecosystem. So build an ecosystem for your learning.

[Challenges In 'Consume' Phase Of Learning](/challenges-in-consume-phase-of-learning/): To learn successfully, you need to learn from authentic sources with a structure. Challenge is in discovering these sources and framing a structure.

[Documenting Your Decisions](/documenting-your-decisions/): Always document the content of a decision. You'll be surprised how this one activity can improve your decision making capability in the long term.

[Are You Learning A New Domain? Visit Its Zoo](/are-you-learning-a-new-domain-visit-its-zoo/): If you are learning about a new domain, learn about its individual entities well before jumping into its ecosystem.

[Do Credentials Matter?](/do-credentials-matter/): One's credentials do not guarantee success; it's their attitude that makes or breaks a venture.

[How I Use Twitter As A Learning Tool](/learn-via-twitter/): Of all the social media platforms, Twitter stands-out as a great place to discover and discuss ideas. Use it.
