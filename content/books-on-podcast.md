---
title="Books recommended on Gravitas WINS Podcast"
slug="books-on-podcast"
excerpt="List of all the books recommended by guests on my podcast"
tags=["books"]
type="post"
publish_at="12 Aug 23 10:46 IST"
featured_image=""
---

## Gamification of marketing with Arvindh Sunder
- [Storyworthy](https://geni.us/sotryworthy) by Matthew Dicks 
- [Building a story brand](https://geni.us/building-storybrand) by Donald Miller

_Disclaimer: As an Amazon Associate I earn from qualifying purchases. Most links in this description are affiliate links. If you buy from my link, I will get a small commission at no extra cost to you. Thank you for supporting me!_