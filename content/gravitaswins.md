---
title="Building Blocks Of A Successful And A Meaningful Life"
slug="gravitaswins"
excerpt="Gravitas is an attractive force for the leaders. Learn how to build it with wealth, insights, network, and self-control."
tags=["course"]
type="page"
publish_at="15 Oct 20 09:00 IST"
featured_image="https://cdn.olai.in/jjude/gravitaswins-gen.jpg"
---
  

Can you build a successful yet meaningful life?

At first thought, it seems like achieving success in life and living a meaningful life  are diametrically opposite pursuits. 

Success is egoistic, but finding meaning is altruistic. Can you pursue both at the same time? Would such a pursuit cause cognitive dissonance in you and eventually drive you insane?

When you start to study success and meaning, it seems like you need to pursue a million things to make them happen. You don't have to. What you need is to identify the core principles. The million things are just the combination of the principles.

After listening to many deep thinkers in the form of books, videos, and courses and observing the lives of people I admire, I have concluded that the building blocks of a successful and meaningful life are:

- Wealth
- Insights
- Network
- Self-control

You can remember them with an acronym - WINS.

Read on to understand each of these factors.

### Wealth

Mind you, this building block is not called riches but wealth. Riches are just money, but wealth is money with a purpose.

What's your purpose for wealth?

- Owning an expensive 🚘 ?
- Having a beach 🏠 ?
- Travelling with kids around the 🌎 ?

I seek out wealth so I can experience life. Some of my friends earn more than me, but they delegate spending time with their kids to weekends. They are rich but not wealthy. I participate in the little epiphanies of my kids, have breakfast and dinner with my family every day, and talk to friends at length every week. 

When there is clarity of purpose, you can create systems to earn, multiply, and protect wealth.

### Insights

In 1956 IBM had to hire an airplane to transport a hard-disk of 5 MB memory. Today we carry a 128GB memory-chip in our pockets.  I suppose you are reading this on a smartphone. If so, you are holding a more powerful computer than the computer that launched man to the moon. 

Technology molds a new world in every generation. To thrive continually, you need insights that match the new world. 

If you want to win a jackpot from your insights, you should form a contrarian view from the general populace and hold them for long. Any less, you dilute the jackpot. 

### Network

All success is social. "Your probability of success is proportional to the number of people that want you to succeed," [tweeted][1] Dharmesh Shah, co-founder of Hubspot.

Networking is nurturing mutually beneficial relationships. You can't look into the network only when you need something from it. To gain from the network, you need to nurture the network as you would tend to a plant. If you want to benefit from networking, treat everyone as a relation and not as a transaction point.
### Self-control

Attaining even a modest success requires self-control. Continuing on the path of success requires ongoing self-control. 

You might think you need self-control only to decide between good and evil. That is not true. You need self-control to choose between good and better; good now or better later.

When you exercise self-control, you harness the power of wealth, insights, and network to create success and meaning in your life.

### Sum > Parts

Wealth, insights, network, and self-control are potent life forces on their own. When you stitch them together to suit your skills and passions, you build a unique and formidable flywheel. As it happens with any flywheel, building up the momentum takes a long, long time, but once it starts moving, it accelerates with every spin. 

I teach building such a flywheel in my course [Gravitas WINS](https://gravitaswins.com/). If you are interested, please [check out the course](https://gravitaswins.com/) and enroll. 

<div class="razorpay-embed-btn" data-url="https://pages.razorpay.com/pl_FvUTGowmHXGSWE/view" data-text="Pay Now" data-color="#004473" data-size="large">
  <script>
    (function(){
      var d=document; var x=!d.getElementById('razorpay-embed-btn-js')
      if(x){ var s=d.createElement('script'); s.defer=!0;s.id='razorpay-embed-btn-js';
      s.src='https://cdn.razorpay.com/static/embed_btn/bundle.js';d.body.appendChild(s);} else{var rzp=window['__rzp__'];
      rzp && rzp.init && rzp.init()}})();
  </script>
</div>

[1]: https://twitter.com/dharmesh/status/504480566084128769
