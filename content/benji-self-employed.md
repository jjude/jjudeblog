---
title="'Employee-like benefits for the self-employed' with Benjemen Elengovan"
slug="benji-self-employed"
excerpt="Can gig workers marry the flexibility of gig economy and employee-liek benefits?"
tags=["gwradio","startup"]
type="post"
publish_at="19 Mar 24 12:06 IST"
featured_image=""
---
Companies like Zomatos and Ubers have given rise to gig economy. Gig workers have lot of flexibility - they can choose type of work and hours of work. But they lack benefits that full-time employees get. Can we marry flexibility with benefits? My guest today asks, why not?

<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless="" src="https://share.transistor.fm/e/c7759825"></iframe>

## What you'll hear
- Introduction
- What problem Benji is solving
- Identifying the problem
- Journey so far
- Benefits for self-employed
- How self-employed can sign-up
- Response of the community
- Challenges
- Expanding across different geographies
- Future of work
- Kindest thing anyone has done
- Best leadership quality
- Definition of living a good life

## Main Points
Here are the main points discussed in the episode:

### 1: Introduction to the Gig Economy and the Need for Flexibility with Benefits
- Gig workers have flexibility in choosing the type and hours of work, but lack the benefits of full-time employees.
- The concept of work has significantly changed post-COVID, leading to the rise of gig workers.
- The unique problem addressed is helping self-employed people create employee-like benefits.

### 2: Identifying the Problem
- The problem was discovered through personal experience as an international student in Australia, working various odd jobs and in the gig economy.
- The impact of the gig economy on financial security and understanding income and expenses was realized.
- The rise of gig economy during COVID highlighted the need to solve the problem of financial security for gig workers.

### 3: Journey and Launching the FinTech Product
- The journey started with participating in a government challenge on improving the safety of gig workers.
- Through research and conversations with over 10,000 gig workers, the focus shifted to addressing the financial security of gig workers.
- MyGeeksters was initially established as a community and later developed into a fintech startup, catering to various types of gig workers.

### 4: Benefits for Self-Employed Workers
- The platform encourages self-employed workers to create their own desired benefits and structure their lifestyle.
- Examples of benefits include creating leave, rainy day funds, saving for taxes, and contributing to retirement plans.
- The aim is to provide employee-like benefits to self-employed workers and help them feel more secure and stable.

### 5: Challenges and Future Plans
- The challenges include defining and regulating the gig economy globally and balancing the needs of gig workers with different levels of bargaining power.
- The regulatory compliance and licensing requirements for building a fintech product pose additional challenges.
- The future plans involve expanding partnerships with existing platforms and exploring the hybrid approach of traditional finance and decentralized finance.

## Edited Transcript
_coming soon_

## Watch on YouTube
<iframe width="560" height="315" src="https://www.youtube.com/embed/TzIfEkWeDVg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

## Connect with Benjemen Elengovan
- LinkedIn: https://www.linkedin.com/in/benjemenelengovan/

## Connect with me
- Twitter: https://twitter.com/jjude
- LinkedIn: https://www.linkedin.com/in/jjude/
- Website: https://jjude.com/
- Newsletter: https://jjude.com/subscribe/
- Youtube: https://youtube.com/gravitaswins
- Email: podcast@jjude.com
- Executive Coaching Program: https://gravitaswins.com

## Your feedback counts

Thank you for listening. If you enjoy the podcast, would you please leave a short review on Apple podcast or on YouTube? It takes less than 60 seconds, and it really makes a difference in finding this podcast. And it boosts my spirits. 😀

I would like to hear what resonated the most with you in this episode. Either email me or share it on social media and tag me. Thank you for your support.

Have a life of WINS.

_Gravitas WINS Radio is produced by [Joseph Jude](https://jjude.com/). Be sure to subscribe on [Apple Podcast](https://podcasts.apple.com/in/podcast/gravitas-wins-radio/id1562802449), [Spotify](https://open.spotify.com/show/7LOBjAyjBkechMC0xMIj9E),[Google Podcast](https://podcasts.google.com/feed/aHR0cHM6Ly9mZWVkcy50cmFuc2lzdG9yLmZtL2dyYXZpdGFzLXdpbnMtcmFkaW8=),[YouTube](https://www.youtube.com/gravitaswins), or the podcasting app of your choice._

Please check out other episodes of [Gravitas WINS Radio](/gwr-themes/). For easy consumption, they are categorized by themes. If you are a fellow podcaster, you might be interested in [How I Podcast](/how-i-podcast/) or [Tools I Use](/i-use/).

## Continue Reading

* [Thrive In Gig Economy](/shresth-shrivastav-gig-economy/)
* [Future Of Jobs Is Partnership Of Experts](/future-of-jobs/)
* [Future Doesn't Happen To Us, Future Happens Because Of Us](/shape-the-future/)