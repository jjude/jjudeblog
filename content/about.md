title="about"
slug="about"
excerpt="I'm a learner, speaker, podcast host, writer, and a book author. I play at the intersection of technology, business, and story-telling "
tags=["about"]
type="page"
publish_at="03 Dec 14 22:15 IST"
featured_image="https://cdn.olai.in/jjude/learning.png"
---

It's good to have you here!

I'm Joseph Jude. I am interested in the intersection of technology, business, and story-telling. I use this blog as a thinking canvas to experiment.

### I am a learner

![Learning Framework](https://cdn.olai.in/jjude/learning.png "SDL Learning Framework")

I follow [Consume-Produce-Engage model](/sdl) to learn. Books, Blogs, and Journals (very little) are in the consume space. This blog falls under 'Produce' space. Some entries will reflect what I have learned; in others I share what I'm learning. Once I have written a post in this canvas, I use it to 'Engage' others.

What I learn changes often. These days, I'm learning how to live in a digital world.

If you want to get a sense of my writing, read these posts:

- [Abstractions are useful, if you know the context](/abstractions-fail/)
- [Agile in the C-Suite](/csuite-agile/)
- [How to deliver value in digital age?](/value-in-digital-age/)
- [Mastering sales as a new CTO](/cto-sales/)
- [I have seen the future of jobs and it is Hollywood model](/future-of-jobs/)
- [Three stages of startups and how to chose tech-stack for each of them](/stacks-for-startups/)
- [What Is Your Ideal Job?](/ideal-job/)
- [Smart people ask for help. Do you ask for help?](/smart-people-ask-for-help-do-you-ask-for-help/)
- [Documenting Your Decisions](/documenting-your-decisions/)

I invite you to comment or discuss through [Twitter](https://twitter.com/jjude).

I hope these blog entries will be engaging and I encourage you to join the conversation. You can read these entries via [RSS](/feed.xml). You can also subscribe to my newsletter via the form below or from [here](/subscribe/).

Also check out the [Resources](/resources/) page for the tools I use for blogging.

### I speak

![Delivering a talk at Karunya University](https://cdn.olai.in/jjude/karunya.jpg "Joseph Jude delivering a talk at Karunya University")

I love to share what I have learned. I talk about product development, building careers, and living a successful life.

If you would like me to speak at your event, please get in touch by email at [speaking@jjude.com](mailto:speaking@jjude.com) or with a direct message on [Twitter](https://twitter.com/jjude).

### Previous Talks

* [How to choose technology for your business growth](/tech-for-biz-growth/) - at a HP launch event, Chandigarh
* Only innovative teams thrive - delivered at [Signicent](https://signicent.com/) LLP - IP & Market Experts
* [Importance of quality in global competition](/quality-cma/) - Keynote at Chandigarh Management Association learning session
* [Workshop on Lean Canvas](/lean-canvas-tie/) - session for student entrepreneurs as part of 'TiE Young Entrepreneurs'


You can view the full list at my [talks](/talks/) page

### I write

I've been writing in this blog for more than a decade. Some of my writings are [published](/published-articles/) in a local newspaper too.
