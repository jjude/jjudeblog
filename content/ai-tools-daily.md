---
title="AI tools I use daily"
slug="ai-tools-daily"
excerpt="Four AI tools that help me learn and write better"
tags=["tech","aieconomy","gwradio"]
type="post"
publish_at="06 Jun 23 08:14 IST"
featured_image="https://cdn.olai.in/jjude/ai-tools.jpg"
---

<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless src="https://share.transistor.fm/e/44eade85"></iframe>

As a Chief Technology Officer, I constantly seek tools that enhance my productivity and enable me to deliver quality work. The current wave of AI tools has been promising, but many fail to produce consistent results. However, four tools have become an integral part of my daily routine.

The first tool is **AudioPen**. As someone who writes extensively for blogs and social media platforms like LinkedIn, I often find inspiration while walking. AudioPen allows me to dictate my thoughts as I walk; the AI tool then transcribes and rewrites them in a chosen style—be it vivid or concise sentences. This has proven invaluable both for myself and my sons, who now use it regularly for their own writing.

The second tool is **WordTune**—a writing assistant that helps refine initial drafts riddled with errors and gaps in thought by sharpening sentences and providing relevant quotes or statistics where needed. It offers numerous features, but I primarily utilize its quote sourcing, statistical data provision, and rewriting capabilities.

**Video Highlighter** is the third essential tool in my arsenal—it summarizes lengthy videos on topics such as management or stock market investing into digestible sections complete with key points linked to timestamped video segments. This enables me to quickly assess whether a video warrants further viewing before committing time to watch it entirely.

Lastly, **Descript** simplifies podcast editing by functioning as Google Docs for audio/video files: importing raw footage into Descript allows me to edit out unnecessary content just like editing text documents—significantly reducing time spent on post-production from days down to mere hours.

These four tools have greatly improved both the quantity and quality of work I can accomplish daily—I encourage you all to share your favorite productivity-enhancing tools so we may learn from one another's experiences!

I hope you enjoyed this episode. If so, can you please share the episode with others? And also send me an email with your feedback? It helps me to improve and evolve.

Thank you for reading. Have a life of WINS. 

_This is part of [Short notes on AI Economy](/ai-economy-notes/)_