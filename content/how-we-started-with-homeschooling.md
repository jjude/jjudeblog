---
title="How we started with homechooling..."
slug="how-we-started-with-homeschooling"
excerpt="Our story so far in homeschooling..."
tags=["parenting","homeschool"]
type="post"
publish_at="08 Apr 22 20:37 IST"
featured_image="https://cdn.olai.in/jjude/how-we-started-with-homeschooling-gen.jpg"
---


Three TED Talks sowed the seeds for my quest for homeschooling.
- [Do schools kill creativity?](https://www.ted.com/talks/sir_ken_robinson_do_schools_kill_creativity?language=en) by Ken Robinson   
- [Kids can teach themselves](https://www.ted.com/talks/sugata_mitra_kids_can_teach_themselves?language=en) by Sugata Mitra   
- [Stop stealing our dreams](https://www.youtube.com/watch?v=sXpbONjV1Jc) by Seth Godin   

Major points from these talks:

Seth Godin: our school curriculum was set for industrial age
Ken Robinson: we have no clue what will be needed in 20 years when kids come out of school
Sugata Mitra: Kids can learn themselves if we give them the tools

In short, we are training our kids wrong.

When my boys were born, I took a pay-cut, moved to a quiet town (Panchkula), worked 3 days a week and spent rest of the time with the kids.

Initially, it was not about homeschooling. It was just about two things:
- bonding with the kids
- training them in storytelling

We have only 12 summers to spend with kids. After that they will leave the house and find their own way. I wanted to spend the early years as much as possible with the boys.

What do I do when spending time with the kids? I wanted to teach them something as we spent time together. I chose [storytelling](/storytelling-to-kids/) because it is one skill that will be in demand even in 10 - 20 years.

We played a version of "[Whose line is it anyway][1]". Kids loved the improv form of story telling. 

When time came for sending them to school, they were happy to go to school. Surprisingly they didn't cry any day. So I sent them without any hesitation. Teachers were good. Kids enjoyed school. Everything was going good.

As the elder one went to 5th standard and subjects turned tough, he started asking why he has to learn those subjects.

I had no answers.

They showed interest in games, music, and stories. But they are not part of the curriculum. They are extra-curricular activities!

As a result, their ranks went down. They were disappointed on two counts:
- their ranks were going down
- they were not learning what they enjoy

So I started searching seriously about homeschooling in India. But I didn't get any useful resources.

So everything went as usual.

Then covid hit. Kids were glued to the screens but not to their lessons. You and I, as adults can't sit idle in-front of iPads and iPhones. How can the kids?

They will switch to playing online games, watching YouTube videos, or chatting with their classmates. Their interest in school went down drastically.

Then, [Krishna Kumar][2], my friend, introduced me to a family which homeschooled their 2 kids for 9 years. 

I spoke to the dad - Mahendran. We talked at length asking about every detail. Armed with all the details, I spoke to my wife & kids. They had ton of questions. I spoke to Mahendran again. With all the questions, I decided to turn the conversation into a [podcast](/mahendrank/).

My wife and kids were still not convinced.

In the mean time, kids were struggling to pay attention to online classes because of covid. Still they didn't want to stop schooling.

What if..   
What if..   
What if..   

So many questions and hesitations. 

I was convinced. But I didn't push them. 

We talked a lot. My wife and kids started to show interest in homeschooling.  All through this phase, Mahendran was very supportive in giving out all the answers for every questions we had. Then in Oct 2021 we took the decision to stop sending kids to school and start homeschooling.

As they say, covid accelerated digital transformation. It is true at least in our case ;-)

In the initial days, I struggled to make a road-map. We didn't know what the kids should learn. But we decided to take it slow. Slow is smooth and smooth is fast.

We picked up lessons that they enjoy. 

- Music (with a personal tutor)
- Computer games
- Reading Bible
- Learning Tamil
- Reading books

We experimented with a quarter. Slowly we got a hang of homeschooling.

As I interacted with my friends, they all were interested in how I was homeschooling the kids. I got same questions again and again. So I blogged about our experience [here](/why-how-homeschooling/). I was planning to blog our journey as we went along.

Then in one twitter conversation with [Dr Aniruddha Malpani](https://twitter.com/malpani), I [tweeted](https://twitter.com/jjude/status/1511244223852396544) my blog post.He called me later requesting me to "build in public". He said he will also release a grant for us to do so.

That nudged me to make it into a separate venture. I registered a domain named, [FamilyIsSchool](https://familyisschool.com/). Mahendran and I are planning to blog about our experience and answer any questions on homeschooling.

If you have any questions, feel free to ask in the comments. Otherwise, just come along our journey.

_This is an edited version of the [twitter thread](https://twitter.com/FamilyIsSchool/status/1512060448304099328) I posted earlier._

## Continue Reading

* ['Homeschooling In India' With Mahendran Kathiresan](/mahendrank/)
* [Why And How I'm Homeschooling My Kids](/why-how-homeschooling/)
* [Teaching Storytelling To Kids](/storytelling-to-kids/)


[1]: https://en.wikipedia.org/wiki/Whose_Line_Is_It_Anyway%3F_(American_TV_series)
[2]: https://www.linkedin.com/in/krishna-kumar-n-kk/
