---
title="Until everything gets better"
slug="until_everything_gets_better"
excerpt="Don't wait for all things to get better; cherish your life, here and now."
tags=["insights"]
type="post"
publish_at="12 Sep 05 16:30 IST"
featured_image="https://cdn.olai.in/jjude/until_everything_gets_better-gen.jpg"
---

In Chennai's slang, I would be termed a 'tube light'. For those poor souls who are puzzled of its meaning, it refers to someone who cannot grasp a concept quickly. If you still don't understand (welcome to 'tube light' club), read on.

The story goes way back to my school days. When I was at school, I was envying those who were going to VOC and Kamaraj arts colleges. You would've envied too if you were me. While I was carrying loads of books and notes these enviable seniors would carry just a single note. I had to stay in class from morning until evening, while these lucky fellows would force their college for IDC (indefinite closure) for any number of reasons - some good, mostly weird. Whether a student hit a professor or a professor hit a student, all warranted a strike. Envying them, I grew up waiting to go to one of those collges so that I could enjoy my life.

I did go to college but to a disciplined engineering college, where I'd to stay in the class listening to lectures from morning until evening. I'd to study even after class if I were to have any hope of getting out of college with a degree. Life was tougher than anticipated. It's okay. Four years and I'd be earning. Earning means, having money to do what I want to do. That means, being happy.

Not so. I got a really poor paying job. What I got was not even fraction of what I thought I would get. By 20th of every month, I was under financial crunch. So 'being happy' took a back seat. Getting a better job became the goal. After all, if I get a good salary, I could be happy. Few years passed before I realised my goal. I was into a software consulting firm, which paid me well. Now is the time to get happy.

Not so fast! Everyone around me were flying to the US. I wanted to go too. 'If you go abroad, you make both yourself and your parents happy', I told myself. Isn't that better? Having that in mind, I worked harder than anyone else. Within six months, I was in the flight to L.A. Dream of every Indian software engineer was coming true in my life. I was so excited.

In L.A. everything was different! The smell of food, faces of people and way of life. I tried to enjoy. I, with other collegues, visited lots of tourist places - Universal Studio, Long beach, Los Vegas and so on. Everywhere I turned there were couples, couples walking hand in hand and time to time kissing passionately. I wished I had a gal with me to roam around these lovely places.

That is when life took a twist. An unpleasant twist. On one end, I went to scenic Europe. On the other hand .... Whatever followed shook the whole family. But it was also a wake-up call for me personally. I realised that I've been foolish in not having lived, though I was nearing three decades of stay in this big and beautiful planet. Everytime, I was waiting for all things to get better. On the contrary, life became worse. And now it hit rock bottom.

That is when I heard the song, '[Come on over](https://www.youtube.com/watch?v=_BVhjqXKSmo)'. With an enchanting music and an equally motivating lyric, Shania Twain sang:

Get a life-get a grip   
Get away somewhere, take a trip    
Take a break-take control    
Take advice from someone you know    

That was it. I didn't wait any longer. Waiting any longer seemed an idiotic choice. I jumped on my feet, packed clothes for a week and flew to Italy. For the first time, I did what I liked to do, not waiting for things to fall in place. Did I enjoy it? Very much so. Boat rides on Venitian waters, beauty of Florence, remains of Rome spoke to me of what is available, if only I am ready. I made myself ready. 

That was also the time that I read Steven Covey and understood the power and possibility of transforming your mind. As I took baby steps, life made sense. I met Venket, an unique personality amidst scores of transported, judgemental Indians. His wife, Jeysree, was equally accomadative and supportive. Xavier, my first Belgian friend, requested me to be Godfather for his new born son. Through a social service agency, I paid for a child's study. My project manager, Steven, and his friend Sandra, introduced me to various entertainments available in Belgium - theatres, organized walks, bars and so on. Not only my life had a meaning, I was enjoying too. My joyful bliss should've been obvious. Otherwise how would I, a reclusive and fiercely private person, befriend a jovial and cute Flemish girl named Knop. From then on, she accompanied me in many of my trips. I also moved to an unfurnished apartment, bought furnitures to my tastes and fixed them myself. Life was going great. Most things stayed the same, as worse as it was. But I had gone through a change. Depressive, lonely mind was transformed into a 'Beautiful Mind'. Relations with my parents and sister improved greatly too.

When I thought things couldn't get any better, it got worse. My contract came to a sudden end and I had to come back to India. Everything collapsed. I lost my independance in a strong intrusive society; my sister moved out of city; I was posted in a team which did a 24 x 7 production support, which meant no long vacation and being disturbed at all times. I tried to find my rhythm back. I found it in the job. Being a reclusive person, I never knew I would enjoy motivating and managing people. I found something new and that gave me a confidence to go on. May be I'll be able to build upon it.

When I was trying to rebuild myself, I was hit where I was vulnerable. That put me off the course and caused great deal of mental agony.

Probably I needed a jolt to remind me of the lessons that I learned. Deriving strength from past experience, I moved to a house near the beach, bought a car and changed job. I do have trouble adjusting in many aspects: to this highly intrusive society; lower job profile; and most of all the inability to travel as much as I like. It is not all that bad either. There are few positive aspects too: I'm spending much more time with parents and sister - we meet at least once a month; I've made few good friends who know me in entirety yet inclusive. I am exploring the city with a camera and posting regularly in the photo blog and getting appreciation too.

What does the future hold? I don't know. If I'd take a scientific view and extrapolate the data that I've, it is going to be anything but bad. May be I'll stay single all my life, not having the pleasure of holding my child in my hand or experience the pain and pleasure of raising them; may be I'll die alone in bed; may be I'll be washed out in a storm or a flood; may be I'll be broken financially; may be I'll go insane. Or may be life will take a good turn. I'll find a woman who'll be behind my success or better I'll be the one who will be behind her success; may be my days will be filled with life and I'll pass this life surrounded by loving friends and family. Most probably, life will be somewhere inbetween these two extremes.

Whichever way life takes, I'm determined to buckle up and enjoy the ride, despite negative aspects. I shouldn't forget the lesson that I learned in a hard way - Don't wait for all things to get better; cherish your life, here and now. After all, when a tube light picks up, it goes steadily.

### Also read

* [Get Over Your Fears, For They Are Just Imaginary](/fears-are-imaginary/)
* [Life’s little lessons](/lifes-little-lessons/)
* [38 Lessons](/38-lessons/)
