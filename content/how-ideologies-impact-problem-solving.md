---
title="How Ideologies Impact Problem Solving?"
slug="how-ideologies-impact-problem-solving"
excerpt="Ideologies are the ‘invisible hand’, determining our perception and responses. So if the ideologies converge, you will make quick progress."
tags=["problem-solving","visuals"]
type="post"
publish_at="27 Jan 11 16:30 IST"
featured_image="https://cdn.olai.in/jjude/idology-problem.jpg"
---
When teams assemble together to solve a problem, last thing on their mind is differences in ideologies of their members. But
ideologies greatly impact problem solving. In fact, its impact starts well before solving the problem at hand; it affects the
way the problem is perceived.

But before we discuss how ideologies impact problem solving, let us define ideology. The reputed TED speaker [Ken
Robinson](http://www.ted.com/speakers/sir_ken_robinson.html) in his book, [Out of our
minds](http://www.amazon.com/Out-Our-Minds-Learning-Creative/dp/1841121258), defines ideology as “assumptions, values and
beliefs that constitute our taken-for-granted views of reality, our natural conception of the way things are.” He goes on to
say that these ideologies determine how we theorize the world around us.

* "Everything happens for a reason"   
* "God is in control"     
* "I create my world"    
* "Millions can’t be made with honesty"   
* "I’m an optimist; I blame others"

All of the above are valid ideologies; not necessarily helpful ones in all situations. The important point is to be aware of
ideologies of our own and others and the interplay of them in solving the problem at hand.

So how do these ideologies steer us in solving problems?

![How Ideology impact problem solving?](https://cdn.olai.in/jjude/idology-problem.jpg "How Ideology impact problem solving")

**Ideologies influence our perception of the problem**: While Al Gore is educating the world about the [Inconvenient
Truth](https://en.wikipedia.org/wiki/An_Inconvenient_Truth) and winning Oscar for it, there are those who
[question](https://en.wikipedia.org/wiki/The_Great_Global_Warming_Swindle) the accuracy of that truth. When ideologies don’t
converge, what appears as a problem for one group may not be so for another. What solution can emerge if there is no consensus
that there is a problem in the first place?

**Ideologies define boundaries for idea generation**: I am a member of a group recently constituted to solve a technical issue
in integrating nation’s e-Governance programs. One set of members, argued that the individual programs should be developed
around the defined e-Governance standards while another took a position that their program is bigger than anything attempted
so far, so the standards should be modified to suit the evolving needs. While all of us agreed that there was a problem,
conflicting ideologies within the group constrained the options to solve the same.

**Ideologies guide evaluating risks of a selected solution**: Traders and investors (especially value investors) estimate risk
of their investments from diametrically opposing ideologies. Hence a quarterly dip in profit of a sound company may cause
panic to a trader; while a value investor may continue enjoying his holidays without a slight concern about the results.

**Ideologies determine our responses to hurdles**: No organizational problem gets solved without hurdles. Unconsciously our
responses are determined by our belief system. When faced with corrupt government bureaucracy, a lady who believes “Honesty is
the best policy” will respond differently than a man who thinks, “Millions can’t be made with honesty”. Without getting
judgmental about these responses, suffice to say that if these two are in a group, then there will be irresolvable conflicts
in solving problems.

Ideologies don’t get discussed openly in organizations. But they are the ‘invisible hand’, so to speak, determining our
perception and responses. So when you are in a group to solve a problem, check if the ideologies converge. If they do, you
will make quick progress.

_This post is part of ‘[Be a Problem Solver](/be-a-problem-solver/)‘ series._
