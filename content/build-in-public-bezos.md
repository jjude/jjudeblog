---
title="Building In Public, Jeff Bezos Style"
slug="build-in-public-bezos"
excerpt="You can learn a lot if you read Amazon's shareholder letters"
tags=["coach","wealth","wins","frameworks","flywheel"]
type="post"
publish_at="14 Feb 21 16:34 IST"
featured_image="https://cdn.olai.in/jjude/bip-bezos.jpg"
---

[#buildinpublic](https://twitter.com/hashtag/buildinpublic?src=hashtag_click) is the latest trend in the startup world. Building in public is about sharing your vision, the perspective of the problem, solution (either the product or service), and most importantly, failures.

If you read Amazon's shareholders letter, you will agree that Jeff Bezos pioneered the model of "Build In Public."

In this article, I share five ways he shaped this style.

### Vision

![1997 Bezos annual letter](https://cdn.olai.in/jjude/1997-bezos.jpg)

In 1997, he declared, "this is day 1 for the internet" and went on to describe how personalization will impact commerce. Technology is still shaping personalization, but Bezos talked about this in 1997.

### Product

![2007 Bezos annual letter](https://cdn.olai.in/jjude/2007-bezos.jpg)

In 2007, he talked about his "product." He explains how Kindle is different from a physical book. He goes to every detail to sell his product.

### Strategy

![2014 Bezos annual letter](https://cdn.olai.in/jjude/2014-bezos.jpg)

Jim Collins, the management guru, talked about the concept of the flywheel to Amazon's executives. Jeff Bezos applied his insights and created a moat for Amazon. In 2014, he wrote about the Amazon flywheel. 

Amazon attracted customers with low prices. Amazon enticed the third parties with the traffic numbers to offer more products on the store. More products attracted even more traffic. With increasing total sales, the cost of selling one product went down, which went to the customers as discounts, which attracted even more traffic. The virtuous cycle has been spinning and spinning. 

### Financial View

![Why Bezos prefer cashflow](https://cdn.olai.in/jjude/2011-bezos.jpg)

In 2011, he said he prefers cash flows rather than any other parameter. He goes on to explain his thinking behind it.

### Team Working

![2016 Bezos annual letter](https://cdn.olai.in/jjude/2016-bezos.jpg)

In 2016, he shared how teams in Amazon make high-velocity, high-quality decisions at Amazon. Speed matters in business. Bezos shares how they achieve that speed.

### How can you follow this?

If you are a creator, small business owner, or an executive, you can benefit immensely from Amazon's shareholder letters. Download them for [free](https://drive.google.com/file/d/1SpgDsIpC_cAS0O4cBz4Sb_GJcEIBhUtA/view?usp=sharing) and read them. Importantly, follow Jeff Bezos' model. Share your vision. Tell us why you care so much about the problem you are solving and what unique perspective you bring in with your solution. Build-in public. Attract dollars and supporters.


## Continue Reading

* [How to find trends in your industry](/trends-in-industry/)
* [What small IT businesses can learn from Mary Meeker's internet trends report](/2017-trends/)
* [Entrepreneurship is a metamorphosis, not a baptism](/morph-as-entrepreneur/)