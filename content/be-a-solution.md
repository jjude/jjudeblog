---
title="If you are not part of the solution, you are a problem"
slug="be-a-solution"
excerpt="Put a dent in the universe that knows you"
tags=["opinion"]
type="post"
publish_at="03 Dec 19 08:31 IST"
featured_image="https://cdn.olai.in/jjude/be-a-solution-gen.jpg"
---


Another young girl is [raped and burnt](https://www.thehindubusinessline.com/news/national/brutal-rape-and-murder-of-veterinary-doctor-shakes-the-conscience-of-telangana/article30124231.ece). What is the response of society? Outrage, social-media trends, candle-light march, and then... back to life as usual. Until the next one. 

Typical.

> There are people who fix problems and people who complain about problems, and the two sets are remarkably disjoint.
<cite>– [Paul Graham](https://twitter.com/paulg/status/1133285829646979072)</cite>

I'm no activist. I have no desire to change the whole wide world. But I can change a part of it. I have no desire to put a dent in the universe. But I can **put a dent in the universe that knows me**.

I grew up in a society where caste was tied to one's identify and name. Together people talked about evils of caste but individually were proud of their caste. When I was fifteen years old, I watched the disaster of caste. There was a caste clash in my town, and two brothers from the same house were shot. As a young boy, I didn't know what I could do. But that incidence was etched in my memory.

When the time came to make my own identity, I decided to do something about it. I quietly dropped caste from my name. My dad was furious when he came to know about it, but he knew it was too late to do anything about it. I had grown past the **grip of vanity identity**.

Years later, in 2011, a young man called Reuben Fernandez was stabbed by a group of thugs. His crime? He tried to stop those thugs from eve-teasing girls. I couldn't sleep that night. Someone lost his precious life in doing the right thing. I kept asking, "why?" Something clicked. Every Indian movie celebrates eve-teasing. The hero will eve-tease a girl, stalk her, and finally, she will give up and fall in love with him. Boys growing up watching these scenes in every movie think this is normal. Eve-teasing is every boy's birthright. At least that is what these movies teach them. Even educated mothers brim with ecstacy when their sons tease girls, because as per the standards set by Bollywood, that is what marks a man.

Who am I to do anything about this evil phenomenon? I am nobody.

Yet, there was something I could do for sure. I could **vote with my money**. I stopped watching those movies. Most movies were of that type, so I stopped watching movies altogether. It is going to be a decade since I watched any movie. Even my boys are growing up without watching such non-sense.

I have taken this a step further and named my boys after their mother. They don't carry my name. I teach them that it is to respect women in general and their mother in particular. It is the least I could do for all the blood she spilled for them.

I never talked about these earlier, because first of all, I thought to talk about these is egotistic and proud. Secondly, I didn't think too much about it. I didn't consider myself an activist. I had a life to live, and I went about it.

Then why am I sharing now?

Now that I'm old, I don't care if people think of me as proud and egotistic. Importantly, younger boys have repeatedly asked me what they can do. I have also realized that we are all looking up to others to make a decision.

One of my favorite songs is "**man in the mirror**" by Michael Jackson. The lyrics go like this:

>I'm starting with the man in the mirror
>I'm asking him to change his ways
>....
>If you want to make the world a better place
>Take a look at yourself, and then make a change


You always have an option to start with the man in the mirror. So the question really is, what are you doing to change your world?

## Continue Reading

* [What Ails India?](/what-ails-india/)
* [Ignorant Of Many Things](/ignorant-of-many-things/)
* [Where there is vision, people prosper](/vision/)


