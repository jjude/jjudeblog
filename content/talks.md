---
title="Speaking"
slug="talks"
excerpt="Joseph Jude is a speaker, author, and mentor"
tags=["talks"]
type="page"
publish_at="19 Jan 19 21:09 IST"
featured_image="https://cdn.olai.in/jjude/2018-agile-talk.jpg"
---
I love to share what I have learned. I talk about product development, building careers, and living a successful life.

![Talk at Agile Chandigarh](https://cdn.olai.in/jjude/2018-agile-talk.jpg)

You can see what I'm currently interested in from my recent talks listed below, or my writing on this blog.

If you would like me to speak at your event, please get in touch by email at [speaking@jjude.com](mailto:speaking@jjude.com) or with a direct message on [Twitter](https://twitter.com/jjude).

### Previous Talks

_June 25, 2023_
**Branding, channels, and market penetration**
Talk at IDE bootcamp at [NITTTR, Chandigarh](http://www.nitttrchd.ac.in/) organized by Ministry of Education, GoI   
[Post](/brand-channel-mkt-penetration/)

***

_July 30, 2020_
**Building Knowledge Networks Between Educational Institutions  & Industries**
[National Institute Of Technical Teachers Training and Research, Chandigarh](http://www.nitttrchd.ac.in/)    
[Post](/kn/)

***

_July 11, 2020_
**Building a career amidst a pandemic**
[Post](/career-in-pandemic/)

***

_April 23, 2020_
**Gravitas WINS**
Amity University

***

_January 31, 2020_
**Agile in the C-Suite**
Agile Gurugram, 2020.
[Slides](https://www.slideshare.net/AgileNetwork/agile-gurugram-conference-2020-agile-in-the-csuite-joseph-jude?qid=17577c4a-b3d9-4e8b-ac28-78dc4a1a2e36&v=) / [Post](/csuite-agile/)

***

_January 24, 2020_
**How to choose technology for your business growth**
Addressing small and medium business owners at a HP Launch event, Chandigarh.
[Post](/tech-for-biz-growth/)

***

_December 19, 2019_
**Only innovative teams thrive**
Talk at [Signicent](https://signicent.com/) LLP - IP & Market Experts     
[Watch](https://www.youtube.com/watch?v=3_NDMCCexZ4)

***

_November 23, 2019_
**Importance of quality in global competition**
Keynote at Chandigarh Management Association learning session.
[Post](/quality-cma/)

***

_November 04, 2019_
**Workshop on Lean Canvas**
I took a session on 'Lean Canvas' for student entrepreneurs as part of 'TiE Young Entrepreneurs'.
[Post](/lean-canvas-tie/) / [watch](https://www.youtube.com/watch?v=lsHRFxmSKm4)

***

_July 13, 2019_
**You can beat the giants**
Talk at Karunya Institute of Technology, Coimbatore
[Post](/beating-giants/)

***

_March 07, 2019_
**What you can learn from Apple and Amazon about building your career**
Talk at Gulzar Group of Institutes, Punjab
[Post](/apple-amazon-career/)

***

_January 21, 2019_
**Don't ignore little beginnings**
Talk at OakRidge Internation School, Mohali

***

_August 30, 2018_
**4 Factors for Success**
Talk at Gulzar Group of Institutes, Punjab

***

_August 16, 2018_
**Non-functional requirements for scale and growth**
Talk at Product Tank Event, Chandigarh

***

_July 27, 2018_
**Essential Skills for a Digital World**
Talk at Digital Fridays, Net Solutions, Chandigarh

***

_June 01, 2018_
**Agile in the C-Suite**
Talk at Agile Chandigarh
[Slides](https://www.slideshare.net/JosephJude4/agile-in-csuite-99737030) | [Post](/csuite-agile/)

***

_April 21, 2018_
**How to deliver value in digital age?**
Keynote at Azure bootcamp, Chandigarh
[Slides](https://www.slideshare.net/JosephJude4/delivering-value-in-digitial-age-global-azure-bootcamp-joseph-jude) | [Post](/value-in-digital-age/)

***

_August 10, 2017_
**Panelist on Cyber-Security**
"I'm the change" event as part of "Young Indians" (a CII initiative)
[Post](/cyber-security-for-women/)

***

_July 22, 2017_
**Three Stages Of Startups And How To Chose Tech-Stack For Each Of Them**
Keynote at AWS Event, Chandigarh
[Slides](https://www.slideshare.net/JosephJude4/stages-of-startups-their-tech-stacks) | [Post](/stacks-for-startups/)

***

_June 05, 2017_
**Guest Lecture On Entrepreneurship**
Christ College, Bangalore
[Post](/entrepreneurship-talk-at-christ-college-bangalore/)

***
