---
title="Are you a Rowan?"
slug="rowan"
excerpt="If do the thing, you will have less competition."
tags=["coach","coach"]
type= "post"
publish_at= "19 Nov 19 08:00 IST"
featured_image="https://cdn.olai.in/jjude/rowan.jpg"
---
Professors invite me to talk to their students. Business owners call me to seek advice. Juniors reach out to me for mentorship.

I didn't jump out of my mother's womb this way.  I was once, full of doubts, dilemmas, and despair.

How did I become what I'm?

"Don't ask me who's influenced me. A lion is made up of the lambs he's digested," so said, the Greek poet and Nobel laureate, Giorgos Seferis, when asked a similar question. Like him, I'm what I'm because of many.

Yet, if I have to choose one lamb that had a profound influence on me, amidst all I digested, then it would be the [essay][1] titled, "**A message to Garcia**," by Elbert Hubbard, the American philosopher.

!["Rowans has less competition"](https://cdn.olai.in/jjude/rowan.jpg "Rowans has less competition")

The essay is based on a true story of a young lieutenant named Andrew S. Rowan, carrying a letter to Garcia, the leader of the Cuban insurgents, during the Spanish-American war. Garcia was a guerilla fighter, fighting against Spain, deep inside Cuba. Nobody knew where Garica was, including Rowan. Contacting Garcia was vital as the outcome of the Spanish-American war depended on it.

When the president handed the letter, Rowan didn't ask questions or find an excuse. He "took the letter, sealed it up in an oilskin pouch, strapped it over his heart, disappeared into the jungle, and in three weeks came out on the other side of the Island, having traversed a hostile country on foot, and delivered his letter to Garcia."

Hubbard heaps high praise on Rowan. "There is a man whose form should be cast in deathless bronze and the statue placed in every college of the land." Why?

"Slipshod assistance, foolish inattention, dowdy indifference, and half-hearted work seem the rule."  Amidst this doom comes Rowan with a "stiffening vertebrae," which caused him "to be loyal to a trust, to act promptly, concentrate their energies: do the thing—'Carry a message to Garcia.'"

Guess what? Even after hundred years, human attitudes have not changed. Corporates, governments, and society at large are full of people who do half-hearted work. Hubbard concluded:

> Civilization is one long, anxious search for just such individuals. Anything **such a man asks shall be granted**; his kind is so rare that **no employer can afford to let him go**. He is wanted in every city, town, and village—in every office, shop, store, and factory. **The world cries out for such**; he is needed, and needed badly—the man who can carry a message to García.

*[emphasis mine]*

After I read this, I decided to be a Rowan. I practiced, to stiffen the vertebrae, be loyal to a cause, and focus energies to do the task at hand. Every wisdom I gained is because I strive to be a Rowan.

Let me tell you this: I have worked in small companies, I have worked in government, I have worked in large corporates, and even served in Churches. Everywhere such men and women are celebrated and sought after. Such fine folks negotiate deals on their terms, receive pay-hikes when layoffs are norms, and earn bonuses when pay-cuts are rolled out.

Be a Rowan. There is less competition for Rowans.

[1]: http://www.gutenberg.org/files/17195/17195-h/17195-h.htm

## Continue Reading

* [Beware of Cloning Best Practices](/beware-of-cloning-best-practices)   
* [System for success](/system-for-success)   
* [You can beat the giants](/beating-giants)   
