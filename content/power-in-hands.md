---
title="Power in your hands"
slug="power-in-hands"
excerpt="The phone you have is more powerful than the computer that launched a man into the moon. What are you doing with that power?"
tags=["startup","coach"]
type="post"
publish_at="11 Jun 20 18:00 IST"
featured_image="https://cdn.olai.in/jjude/power-in-hands-gen.jpg"
---


In 1969, man landed on the moon. Without computers, there would've been no spaceflight, no moon landing, and the "giant leap for mankind." Even the best-trained pilots can't handle the complexities of space navigation without the aid of computers.

As vital as it was, the computer onboard the spaceflight was not as powerful as the smartphone you have in your hand. **The cheapest smartphone of today is a hundred thousand times faster and has one million times more memory**.

As Uncle Ben said in the movie Spider-Man, "**with great power comes great responsibility**." The best computer scientists, engineers, and designers, have worked hard to place a powerful computer in your hand. Are you going to waste that power on snaps and selfies?

In the last seventy-three years, our country has made tremendous progress. We are the first Asian country to launch a successful mission to Mars; our prowess in cricket is second to none;  Indians are heading multi-national companies. Yet many problems remain unsolved. Poverty, illiteracy, frequent floods, droughts, and raising bigotry are some of the significant issues that await solutions.

Teenagers have a fertile imagination. You can heal the nation and shape a better future for its citizens when you combine the potency of creativity with the power of technology. **But where do you start?**

Do you carry your phone whenever you step out of your home? As you walk through the roads, observe the world around you, and ask yourself is there a better way to do what people are doing. Can the vegetable cart-puller carry out his business in an efficient way? Can he sell his vegetables differently to make more money? How can he attract passerby to his "shop"? Take a photo of the cart-puller and list your ideas in the notes app of your smartphone. When you are stuck in traffic, take a picture of the traffic, and make a note of how traffic can be managed well. When your father forgets to buy an item from the grocery store, make a note of how the shopping can be improved, so fathers don't forget items from their list. After you **collect such ideas for a month, review your ideas**. Pick one problem and the solution that attracts you. Start to develop this further. Talk to your friends, parents, friends of parents, neighbors, and whoever will listen to you. Refine the idea.

If you are so inclined, go to [Figma](https://figma.co) to sketch your solution. Show your solutions to others and ask for their feedback. Join "[Young Indians](https://www.youngindians.net/)" or "[TiE Young Entrepreneurs](https://tie.org/programs/tye-global-program/)" groups and connect with elders to debate your solution. **One of your ideas is bound to make a mark on the planet**.

If you are not so much into ideas but are interested in learning, that is a good thing. With the internet, **the best teachers of the past and present are just a click away**.

Want to learn Spanish? Open up the "[Duolingo](https://www.duolingo.com/)" app and start learning. Once you have a basic conversational skill, find someone in Barcelona to talk. Connect with them via "[Skype](https://www.skype.com/en/)" to understand their culture and history.

Want to learn history, or science, or nature? You can watch lectures and documentaries from "[Curiosity Streams](https://curiositystream.com/)" or "[The Great Courses Plus](https://www.thegreatcoursesplus.com/categories)".

Want to learn to code? Take [Google Coding for students](https://edu.google.com/code-with-google/). Go one step further. Use the free machine learning tools from Google and Microsoft to create an application to forecasting the weather. Or the stock market.

**Don't please yourself with passive learning**. Create a blog at [wordpress](https://wordpress.com) and write about your passion. Write about how technology connected you to a stranger in Barcelona and turn her into a friend by discovering common threads in the culture of India and Spain. If words are not your forte, try videos. Convert your knowledge of history, science, or nature into a vernacular language. Shoot videos from your phone and post them on "[YouTube](https://www.youtube.com/)." You will learn a thing or two about creativity, solving problems, and spreading ideas.

Humanity is longing for radical solutions for long-pending weighty problems. Will you delight them with ideas or disappoint them with a dog filter?

_I wrote this for a youth magazine of a local newspaper. They rejected it, saying it reads more as a motivational piece! Okay, so here it is on my blog._

## Continue Reading

* [Thanks & Sorry. Say them before it is too late](/thanks-and-sorry/)
* [If you are not part of the solution, you are a problem](/be-a-solution/)
* [Teaching storytelling to kids](/storytelling-to-kids/)
