---
title="Happy 75th Birthday Ma"
slug="happy-75th-birthday-mom"
excerpt="Her life stands as a testament to the power of hard work and faith in uplifting not only oneself but also one's family."
tags=["parenting","self"]
type="post"
publish_at="08 Dec 23 18:18 IST"
featured_image=""
---

As the daughter of a village head, my mom grew up in privilege. Even though girls weren't allowed to attend school then, she went to school because of her privilege. Because she excelled at school, she even got a job in Madras, the state capital. It seemed her fortune was on the rise.

However, a series of misfortunes soon changed everything. Her father insisted she marry near their village to care for him in his old age. So she married my dad. On the day of marriage, she lost all her jewels gifted by my grandpa. Suddenly thrust into poverty, she and my father lived in a hut where I was born.

After me, my mother had another son with a medical condition causing his joints to bleed. Unable to provide adequate care due to their financial situation, he tragically died after a year. This was followed by numerous miscarriages and continued hardship.

Sadly, my mother's father passed away just a year after her marriage, leaving her unable to fulfill her promise to care for him. The weight of these events never truly left her.

Despite the challenges, my parents struggled to make ends meet by working as substitute teachers. My dad sold groceries to put food on the table. Then, fortune finally smiled on our family when my sister was born. A relative helped secure a permanent job for my mother at a primary school, allowing us to move from the village.

With my mother's new job came stability for our family. We moved to a different town. My father also found permanent employment - in another town. He would work there during the weekdays and return on weekends.

My mother juggled work and caring for me; we often walked together to catch the bus to school. With only one bus per hour, missing it meant walking 10 kilometers carrying me.

In spite of everything, she never stopped learning. She kept getting degrees. When the primary school became a secondary school, my mother was qualified to teach. Then years later, when it became a higher-secondary school, she was qualified to teach there again. My mom was the only one who started as a primary school teacher and went on to be a high school teacher.

Working hard and earning a degree became a relatively easy thing for her. Every day she faced resistance from patriarchal colleagues who didn't want to see a woman succeed. The headmaster and all the male colleagues didn't make it easy for her. The fight never ended for her.

Even though this post is about my mom, I want to say a few things about my dad. His support helped her study more, even more than him and ended up earning more than him. He stood with her in every fight. He would write petitions, and wait patiently at government offices to fight for promotions and pay rises. He never wavered in his faith either. Even when the family was in the darkest pits, we prayed every day and attended church every Sunday.

Although our financial situation improved over time, my mother's health remained a challenge. Asthma was one of her many ailments. Nevertheless, she continued to study and contribute to the family through various means - offering tutoring sessions, assisting my father in his sari business, and exploring other avenues to enhance our lives. Eventually, we were better off; both my sister and I pursued engineering degrees. 

After my parents retired, they moved to Bangalore to live with my sister. Now 75 years old, my mother remains active and engaged in life. She had her share of challenges but also reaped rewards. She's been to faraway places like Jerusalem and Sri Lanka. While in Sri Lanka, she visited the church her mom attended. She's seen her grandchildren through both of her kids. Even at 75, she's got all her senses. She's celebrating her birthday with both of her kids and our families, which is a treat.

Her life stands as a testament to the power of hard work and faith in uplifting not only oneself but also one's family. Our lives have been enriched by her example.

I inherited her diligent work ethic. My sister inherited this quality as well as nerves of steel and fierce competitiveness. I'll be honest, I avoid competition. My sister inherited these admirable traits from both of our parents.

So, with joy in my heart, I'm celebrating my mom's birthday. Happy birthday, Mom.

***

_I'm glad I wrote this. Few months later, my mother [passed away](/eulogy-for-mother/) on 14th March, 2024._