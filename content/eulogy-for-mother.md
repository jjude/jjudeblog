---
title="Eulogy for my mother"
slug="eulogy-for-mother"
excerpt="My mother passed away on 14th March 2024. I delivered this eulogy at her funeral."
tags=["parenting","self"]
type="post"
publish_at="24 Mar 24 20:25 IST"
featured_image="https://cdn.olai.in/jjude/eulogy-for-mother.jpg"
---

![Eulogy for my mother](https://cdn.olai.in/jjude/eulogy-for-mother.jpg "Eulogy for my mother")

We all live up to names given to us by our parents, whether we realize it or not. My mother, Maria Johnsi, was no exception. Named after Mother Mary and Queen Jansi of India, my mother embodied a spirit of prayer and fighting. She prayed not only for herself but also for her family, friends, and anyone who sought her prayers. Likewise, she fought poverty, discrimination, and jealousy.

Born in 1948 to a privileged village-head's family, my mother received good education. She excelled in her studies, eventually landing a job in Chennai despite hailing from a small village in South Tamil Nadu. However, her father wanted her close during his final days, so she married near her hometown. An endless stream of disasters struck her from then on.

On her wedding day, she lost all her property and dowry; she had to start from scratch. My parents worked as substitute teachers without a steady income. My dad also bought and sold rice bags to make ends meet.

Sadly, though she married near home, my mother couldn't be with her father in his last moments. When I was conceived, doctors claimed I was just a dead baby since they couldn't detect any movements. My mother displayed her prayer and fighting spirit by walking daily to St. Jude Church from the hospital. Her determination and faith brought me into this world. I witnessed these traits throughout her life.

After me came another son, born with a medical condition that needed constant care. Due to poverty, they were unable to afford such treatment. A kind doctor graciously treated him for free. One day he fell so sick that the doctor advised my parents to take him to a hospital in the near-by town. My parents didn't have 50 rupees, which was a lot of money then, especially for them. My brother passed away that night at their hands. Afterward, my mother suffered several miscarriages.

These experiences deeply affected her psyche, even as blessings found their way into our lives.

Then my sister was born. My parents named her Joy, hoping that the name would bring happiness into our lives. And it did. A relative helped my mom secure a permanent teaching job, and from there, our lives slowly improved.

My mother became a first-grade teacher at a new school where most teachers were officers' wives; my mother was the exception. Despite that difference, all her colleagues embraced her and remained friends. In fact, one of them visited us to share her memory of my mother and offer condolences.

My mother persevered, studying hard and working tirelessly to better her life. As the school expanded, she moved up grade by grade with her students. They joked that she graduated alongside them, being their teacher from first through twelfth grade.

But this journey wasn't without hurdles. Her male colleagues resented her promotions and tried to hinder her progress by lodging false complaints against her. My mom and dad fought back with prayers, petitions, and appeals to school management. Despite all those obstacles, my mother received promotions far beyond her peers.

After retiring, my parents moved in with my sister, and my mother enjoyed the rewards of her prayers and resilience. Born in a village, she traveled from India's southern tip, Kanyakumari, to its northern border, Wagah. She visited her mother's home-church in Sri Lanka, holy sites in Jerusalem, and the pyramids.

Over the last 3 years, we celebrated their 50th wedding anniversary, their 75th birthday, and my 50th birthday. Whenever I visited my sister's house for these celebrations, we would sit around the table eating, discussing, debating, and having a fun time. She was a testament to what the Psalmist sang, "your children will be like olive shoots around your table." For all her tears, she also enjoyed tremendous blessings. I used to tell her, I would be happy to get half of your blessings.

Our family remained religious. We never missed church on Sundays or special occasions like Easter and Christmas. We prayed every evening at home. When my mother retired, she spent most of her time reading the Bible and praying. I heard from so many people who visited us to offer their condolences that they were consoled by her prayers. For some she would recite a Bible verse or two as a means of counsel or comfort.

As I stand here missing her, I also accept that it was time for her to go. When I heard the news of her demise, I prayed and opened the Bible. My eyes fell on Psalm 75:2 – "It is an appointed time." This brought me peace beyond understanding. One of her recurring prayers was to die without troubling others. God answered that prayer; she remained active until two days before her death.

On the day she passed, my sister changed her clothes and said goodbye before leaving for work. My mother died while talking to my father. There couldn't be a better way to finish one's race.

To celebrate her life, I must follow her footsteps - embrace life's challenges and persevere through prayer and perseverance. I pray that God grants me that spirit.

## Read also
- [Happy 75th Birthday Ma](/happy-75th-birthday-mom/)
- [Lessons from mourning](/lessons-from-mourning/)
