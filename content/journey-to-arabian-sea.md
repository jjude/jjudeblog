---
title="A day’s journey to the Arabian sea"
slug="journey-to-arabian-sea"
excerpt="Everything has a first. My experience in travelling in a group."
tags=["travel"]
type="post"
publish_at="13 Jan 06 16:30 IST"
featured_image="https://cdn.olai.in/jjude/journey-to-arabian-sea-gen.jpg"
---

Barely two months passed when I got an invitation to join the team for a weekend trip. Malpe, an exotic beach at the western end of Karnataka was a good choice to get away from the daily routine. However, I was caught between two strong opposing views. I absolutely love traveling, and what better opportunity to start the year than being in an Arabian Sea beach. On the other hand, I've not traveled in a group. Being a seclusive and an introvert, I've always traveled alone. After a little thoughtful consideration, I decided to go because traveling with a team that makes me comfortable seemed like a safe bet.

As the day approached all of us were excited. Preparations went on for group games, room allocation and all other activities that accompany such team outings.

We were supposed to leave on Friday night at 20h30. We experienced the first rule of travel - Be ready for change of plans! Due to chaotic traffic, dinner arrived later than our departure time. By the time we were on the bus it was already 22h00. Still, the enthusiasm was so great that everyone was dancing in bus until midnight and little after midnight slowly they fell asleep.

Next day, we woke up to an entirely different environment - A warm sun welcomed us and backwaters on both sides of road cheered us. We reached 'Paradise Isle Beach resort'. Was the hotel a paradise? Well, it wasn't and it didn't matter.

After a quick shower and a short breakfast, we headed to St.Mary's island. History tells us that the great Portuguese explorer 'Vasco da gamma' landed on one of these islands and named them 'El Padron de Santa Maria', from which the present day name is derived. The island is filled with coconut groove and hexagonal shaped basalt rocks; this unique combination provides the island an idyllic nature. Probably nothing attracts a team as good as water. Almost everyone got into water to play, including our boss. As usual, I disappeared from the crowd clicking photos. When everyone got tired, they realised it is time for lunch.

Lunch refreshed the team for another play in water. As Malpe beach was dirty, we headed to Kaup beach. It was another time for water games. Again, almost every one got into water. However, I found another pleasure: walking on unspoiled, soft beach sand is a treat for your feet.

There is an old 100ft tall lighthouse built on a rock. Sure, from top of lighthouse it would've been a fantastic view, but it wasn't opened. So I'd to be satisfied with the view from the rock, which is not that bad. Backwaters, sea and sight of other islands make a spectacular all-round view from the rock. Sitting on the serene rock and letting sea-breeze play on your body will quieten any soul. As if it wasn't enough, sun was setting into the Arabian Sea. While I was becoming one with the nature, the team had beach volleyball, tug-of-war and so on. We kissed good-bye to the lovely beach and proceeded to our resort.

Back in the resort the mood was one of exhilaration. Arrangements were made for bon-fire, disco and a little bar. I was amazed about the energy of the team. For the next three hours, they danced - some consciously and others unknowingly, if you know what I mean. I witnessed the national unity too - there were music from Tamil, Kannada and Hindi films. There was little English music too.

We had a well spent day. But we had to return. We returned with lots of memories - both personal and digital.

P.S: After our return, there were prizes on various categories. One was 'Best Swimmer'. Apparently, I was the only one who didn't get into water and I got the award.

