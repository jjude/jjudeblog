---
title="Is There DOPE In Your Network"
slug="network-dope"
excerpt="Increase value of your network by diversifying it."
tags=["networking","wins"]
type="post"
publish_at="03 Aug 21 18:34 IST"
featured_image="https://cdn.olai.in/jjude/network-dope-gen.jpg"
---



I made two important networking mistakes in the first decade of my career:

- I had a homogeneous network
- I didn't understand the importance of networking

These two mistakes, I believe, are related.

The majority of my friends were also Tamil men in the software industry. In our spare time, we discussed which company sent employees onsite or paid the most. Nothing else.

We didn't discuss: 

- Problems developers faced when developing and delivering software
- How software was becoming a force for wealth creation
- How to become a better software developer

None of these topics interested us.

As the group was homogeneous, I also wasn't enthusiastic about networking. What is the use? Anyway, it was just disappointing and depressing.

In the last fifteen years, I have corrected those mistakes.

I now have friends from all walks of life.

- Entrepreneurs
- Movie directors
- Charted Accountants
- IAS officers
- Writers
- Social workers

and of course

- Software Engineers

I never have a dull conversation because of the diversity. My friends excite me every time we talk. Every interaction within my group teaches me something, and I help in any way I can.

The benefit of a diverse network goes beyond just animated discussions.

I get DOPE. No, not that dope. I'll explain.

### Esteem

As a result of the diverse viewpoints my friends offer about any topic, I gain a matured understanding of that topic. I have also become more structured in my thinking and communication over the years. 

Friends respect me and seek my opinion whenever they are faced with a critical decision. The more I help them make good choices, the more they respect me and seek me out.

### Positions

An engineering college in Punjab appointed me as its adviser for computer engineering. The position wasn't there before. They created one for me.

### Opportunities

Throughout my career so far, all of my job opportunities (other than one) have come through referrals. I was able to convert all opportunities when I approached them with high recommendations.

This brings me to my last point.

### Dollars

Not all esteem and opportunities convert into money in the bank, but all the money in the bank is the result of esteem and opportunities. 

### DOPE is in diversity

You gain multiple insights when you have a diverse network. You can transform your insights into DOPE if you are mindful.

DOPE begins with a diverse network. Seek out a diverse network.

### Quote To Ponder

The richest people in the world look for and build networks, everyone else looks for work - Robert T. Kiyosaki

## Continue Reading

* [Did you compliment your friend recently?](https://jjude.com/compliment)
* [Thriving with WINS](https://jjude.com/wins)
* [Networking like a Pro](https://jjude.com/network-like-pro)
