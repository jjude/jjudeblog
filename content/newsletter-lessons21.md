---
title="Lessons Learned Running A Newsletter"
slug="newsletter-lessons21"
excerpt="A twitter thread of lessons learned about newsletters"
tags=["produce","biz"]
type="post"
publish_at="28 Jul 21 10:59 IST"
featured_image="https://cdn.olai.in/jjude/newsletter-lessons21-gen.jpg"
---



I have been running a newsletter for 4-5 years now, before it became cool. Initially I sent a newletter whenever I published a blog post, which was whenever I wanted to write. In the last 2 years I've been regular. I send every Wednesday. I call it "Wednesday Wisdom"

I had 94 subscribers in 2020 Jan I have 2404 subscribers now in 2021 July It is not a lot. But I learned quite a bit in these two years. Here are the lessons.

### 1. Don't be a tool-hopper. 

Mailchimp, convertkit, mailerlite are all just tools. 

- List what you want to accomplish 
- Research for the best fit (there won't be a single tool that will meet all your needs) 
- Stay with it for at least 2 years 

**Lead with value, not with tool**

### 2. Focus more on the conversation that the newsletter generates 

None of 👇 matter, if it doesn't generate conversation - either via email (best) or on social-media 
- open rate 
- click rate

### 3. Don't confuse newsletter with e-commerce 
- newsletter: provide value to the reader 
- e-commerce: extract value from the reader 

You could have a section in your newsletter for selling, but if it doesn't provide value, you'll lose subscribers.

### 4. Don't confuse "news" with "letter" 

- Letter - personal, provides value, predictable frequency 
- News - Juicy happenings Unless you are a brand (company like Amazon or famous personality like Joe Rogan), people are more interested in letter than news.

### 5. Send at the same frequency 

I experimented with many timeslots and settled at Wednesday at 10.15 AM IST. 
 
When you send at the same time, it becomes a "habit" for your readers. I have been sending at this time for so long, now readers tell me they "expect" my email ;-)

### 6. Choose a topic 
 
Internet is so wide, that you can always find folks who are interested in any topic. 

- Easiest to get started is curated letter (weekly collection of best articles in the topic) 
- You can also write your take on a topic in different angle

### 7. Don't stray away from the main topic 

Don't confuse the readers with unrelated topics. 

If it is a leadership newsletter, don't send me your "news" about cycling trip you took (unless it is leadership lessons from cycling 😜

### 8. Create your own niche 

Mix interesting categories

- marketing + technology 
- business + psychology 
- freelancing + India


### 9. How to get subscribers? 

Your newsletter is a stadium. Like stadiums, your newsletter should have many entrances. 

- subscribe form on main page 
- subscribe form on every post 
- two forms on a longer post 
- link on your email signature 
- link on social media


### 10. Mention your newsletter at every chance 

- I speak at many college events. I mention it as a way to connect with me 
- When someone connects on Linkedin, I mention it 
- Mention on slack groups; facebook pages; forums (all related either to topic or to newsletters)

### 11. Reward new subscribers with a valuable asset 

- ebooks 
- planner 
- best posts as pdf 

Make the new readers feel appreciated as soon as possible.

### 12. Have a unique url to signup 

I used to say, "goto my site and signup" 

Now I can share the signup url: https://jjude.com/subscribe/

(while we are on the topic, subscribe using the  link)


### 13. Make your newsletter recognizable 

- I follow the same design 
- I mention where they signedup 
- I mention my name and the newsletter name 

With same frequency and same design readers can recognize what the email is about.


### 14. Have a separate domain to send newsletter 

There is a chance that people click your email as spam. Or google thinks it is a spam. 

When you have a separate domain for newsletter (from that of email/website), you reduce the possibility of your main emails land in spam folder

### 15. DKIM / domain confirmation increases deliverability rate 

I didn't know about them until very recently (6 months back). Do them from the beginning


### Helpful resources

Two resources that helped me a lot: 
- [@nathanbarry](https://twitter.com/nathanbarry) conversation with [@fortelabs](https://twitter.com/fortelabs) : [https://youtube.com/watch?v=CzYdwd1Eevw](https://t.co/2UYlU853Rv?amp=1) 
- Newsletter cohort run by [@vijayanands](https://twitter.com/vijayanands). He closed it now. But it was extremely useful when it was running. We shared our experiments, learned from others.

### Tools I use

- [@Mailchimp](https://twitter.com/Mailchimp): have been my email newsletter tool for long. They UX can improve but overall it is great 
- [@mjmlio](https://twitter.com/mjmlio): I design using mjml language mjml design is from [@harrydry](https://twitter.com/harrydry): [https://mjml.io/try-it-live/C7](https://t.co/9eHW2SG3ZD?amp=1)

### Questions?

Do you have any questions? Ask in this [twitter thread](https://twitter.com/jjude/status/1419991368781516800).
