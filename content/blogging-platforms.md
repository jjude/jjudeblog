---
title="Roundup of popular and emerging blogging platforms"
slug="blogging-platforms"
excerpt="Once you decide to blog, then comes the question of 'where' to blog. In this article, I list some of the popular and emerging choices."
tags=["martech","blogging","tech"]
type="post"
publish_at="17 Jul 15 22:34 IST"
featured_image="https://cdn.olai.in/jjude/blogging-platforms.png"
---
Blogging is at the heart of online marketing for businesses. Blogging helps businesses to communicate their vision, their take on their industry and how they will shape the industry. Regular blogging also attract leads. If they sell online products, they might even make a sale through their blog.

Once you decide to blog, then comes the question of 'where' to blog. In this article, I list some of the popular and emerging choices.

Blogging platforms can be divided into four major categories:

1. Hosted
2. Self-hosted
3. Desktop blogging engines
4. Hosted blog generators

### 1. Hosted platforms
Hosted platforms take the pain of installing and maintaining a blog. You signup, login, and start publishing your articles. The hosting company takes care of every technical details of running a blog smoothly. If you are starting to blog, then you should start with one of the free hosted platforms. It comes with a disadvantage though. You can't customize the design of these hosted blogs much.

Here are some of the hosted platforms.

* [Wordpress][8]: The most popular blogging platform. It powers quarter of the top million blogs. It comes in all variety. You can start for free with their hosted platform or download their platform and host it yourself or if you can afford, you can use their expensive VIP platform. Though I run a [competing platform][1], I have great respect for the way they build their business.

* [Ghost][9]: Ghost is similar to Wordpress. You can download and host it on your own host or you can start with one of their offering. Their hosting solution starts at $8 / month.

* [Postagon](http://postagon.com): Tagline for Postagon is 'Blogging for Minimalists'. It supports markdown, custom domain, and posting via email. It costs $4.99 per month.

* [Medium](http://medium.com): This is a new-age article directory like [EzineArticles](http://ezinearticles.com). Medium has large following and hence attracts lots of readers. It has beautiful design and has no ads. Those are the two differences between EzineArticles and Medium. Lot of bloggers, cross-post their articles on Medium to gain readers. You can start for free with a twitter login.

* [Postach](http://postach.io/): Many people use Evernote for storing their notes. Postach takes an Evernote notebook and turns that into a beautiful blog. You can start with $9 / month.

* [Weebly](http://www.weebly.com/): Weebly is a website builder. You can build a blog and e-commerce store too. You can start for free and then upgrade to other plans.

* [Silvrback](http://silvrback.com): Yet another hosted blogging platform with [Markdown][5] support. It costs $29.99 / year.

### 2. Self-hosted

Hosted platforms are easy to start, but they also come with constraints. You don't own the end product — your blog. I don't mean the legal rights of your writing, but the blog with its contents and design. If the blogging platform shuts down, like [Posterous](https://en.wikipedia.org/wiki/Posterous) did, then you are forced to migrate elsewhere. Self-hosted platforms eliminate that uncertainty. Additionally, you can customize the look and functionality of your self-hosted blog to your desire.

As I mentioned earlier, you can download both [Wordpress][10] and [Ghost][9] host on your own servers. They both are open source software and offered free of cost.

* [Statamic](http://statamic.com): Calls itself, the flat file CMS with superpowers. Instead of database, it stores everything in flat files. It costs $29 to start for a personal site.

![Popular blogging platforms](https://cdn.olai.in/jjude/blogging-platforms.png)

### 3. Static blog generators

Traditional blogging systems, mentioned previously, make a request to a database (or to a file) every time a visitor wants to read a blog post. One way people have solved this unnecessary problem is by throwing more hardware and more memory at it. Another way to solve this is by making the blog with plain HTML files. You write a blog post in your laptop, generate all the necessary files and then push to your server. In this way, you get performance and you get security too, since there are minimal hackable parts (Javascript or ads from third-parties) in a static site.

* [Jekyll](http://jekyllrb.com): Jekyll popularized the notion of static blogs. You install Jekyll locally, write your posts in [Markdown][5] format, generate the blog using templates, and then upload to your host. You could host the generated blogs on your own webserver, or [Amazon S3][6] or on [Github][7].

* [Statiked](http://statiked.com): Statiked is a native Mac OSX application, which you can download from Mac Appstore. It costs $19.99 for one time download. You write your posts locally, generate the blog and upload to Amazon S3 or Github Pages.

* [Cactus](http://cactusformac.com): It is a free Mac Application to generate static sites. You write locally, generate the blog and upload to Amazon S3.

There are many other static site generators. You should have a look at the entire [list](https://staticsitegenerators.net) to pick one.

### 4. Hosted static blog generators

Static blog generators boost the performance of a blog and make the blog secure. But they also introduce a constraint. You can blog only from the machine you installed the static blog generator. Hosted static blog generators gets the best of both systems -- they generate plain HTMLs as like static blog generators; they are hosted like CMS so you an write from anywhere.

* [Siteleaf](http://www.siteleaf.com): Siteleaf supports all features of a regular CMS like [Wordpress][8]. You can host with them or host your site at any web-host. You can also collaborate with your team in drafting your articles. You can start for $9 / month for one site.

* [Olai][1]: A disclaimer. I developed [Olai][1]. Currently Olai transfers files automatically to Amazon S3 or Github Pages. The plan is to support as many hosts as possible. It has built-in support for XMLRPC APIs of Wordpress, so you can continue to use your favorite blog editors like [MarsEdit][2] and [ScribeFire][3] to blog your posts. Once you setup your blog at Amazon S3 and connect to Olai, you don't have to make any changes to your blogging workflow. (I didn't know about Siteleaf until I started researching every single blogging engine for competitive analysis. I'm surprised two people unconnected to each other thought similarly. What is even more surprising is the name. Olai in my mother tongue, Tamil, means leaf!)

If you are a startup or a small business, you should have a blog and update it regularly. My recommendation, obviously, would be Olai. But it doesn't matter. Start with Wordpress, if you have to. Build your business through content.

[1]: http://olai.in
[2]: https://red-sweater.com/marsedit/
[3]: http://www.scribefire.com
[4]: https://www.cloudflare.com
[5]: http://daringfireball.net/projects/markdown/
[6]: http://aws.amazon.com/s3/
[7]: https://pages.github.com
[8]: https://wordpress.com
[9]: http://ghost.org
[10]: http://wordpress.org
