---
title="Whatever Happened To New Year Resolutions"
slug="whatever-happened-to-new-year-resolutions"
excerpt="Magic happens when you follow-through your new year resolutions"
tags=["retro"]
type="post"
publish_at="31 Mar 13 16:30 IST"
featured_image="https://cdn.olai.in/jjude/whatever-happened-to-new-year-resolutions-gen.jpg"
---

I made a resolution on Jan 1. There is nothing extraordinary about it. I have been making such resolutions every year. Am sure you do too.

There is one thing extraordinary though.

Unlike other years, I didn't forget about it by end of January. I persisted in getting started and archiving it. I am glad I'm midway achieving it.

Are you curios?

I have programmed extensively for web and desktop. But never for mobile. Way back when Palm came with its PDAs, I tried my hand in developing apps for it. But I never got past the 'hello-worlds.'  I did try many times after that, but somehow it never materialised.

The desire to develop a mobile app continued but it remained just that - a desire.

Once I developed BlogEasy for MacOSX, I was closer than ever to fulfil that desire. I decided to pursue it vigorously.

Starting Jan, I listened to the [fantastic lectures][1] by Prof Paul Hegarty of Stanford. I also bought three books (normally I don't spend a dime on technology focused books, since they become outdated quickly). I started out slowly. I wrote small snippets, downloaded lot of code from github, searched through stackoverflow, when I was stuck. I was ready to write the app.

There was just one more issue to be handled.

I already had a packed life with two kids and a full time assignment. Finding time was a major challenge. I deactivated my Facebook account, cut down on tweeting, quit mostly on TV. Yet, I couldn't get large chunk of time, needed for a focused development. So I would get up as early as I could and stayed as late as I could. I avoided going out on weekends; I read and replied only essential emails; and let RSS feeds remain un-read.

Progress was slow. Yet I kept at it. Everyday I would drag myself and sit at the same place and code — even if it was just few lines of code. I made no exception.

By mid-March, the app was in a good shape for testing. I tested. Then I asked around — in twitter and facebook (I activated FB by then). [Shanker][2] came forward to test. I submitted the app for Apple's approval on 26th. May be it will be approved in one shot or may be it will take few iterations. I don't know. I'm waiting anxiously.

It has been an incredible first quarter. I'm happy with the journey so far in this year.

*Contact me if you want to test BlogEasy for iOS devices. I could surely use some helping hand.*

[1]: http://online.stanford.edu/course/coding-together-developing-apps-iphone-and-ipad
[2]: http://about.me/shankarganesh/
[3]: http://blogeasyapp.com

