---
title="Five Insights From Successful Indian Entrepreneurs"
slug="five-insights-from-successful-indian-entrepreneurs"
excerpt="Inspiration from those who ran before me."
tags=["coach","startup","insights"]
type="post"
publish_at="16 Nov 14 20:13 IST"
featured_image="https://cdn.olai.in/jjude/five-insights-from-successful-indian-entrepreneurs-gen.jpg"
---

I have been reflecting on some of the successful Indian entrepreneurs to learn from them. After all, we look up to those who run (or ran) in-front of us for inspiration & instruction.

Here I note five insights I gained from such reflection.

 1. <a name="ambani"></a>[Dhirubhai Ambani][1]: There are many American rags-to-riches stories. Dhirubhai's is the first Indian rags-to-riches story. He was smart in capitalising every opportunity to grow his empire. But there is one strategy that he employed at every venture. He took pains to **educate every stakeholder**—regulators, common public and market— about his venture. For Dhirubhai, educating stakeholders was part of doing business.


 2. <a name="nrn"></a>[NRN][3]: NRN showed that it is possible to build a clean business in India. He was also generous in sharing his wealth with employees. No entrepreneur practiced employee share option in India like NRN did. **He made his employees millionaires**.

    NRN shaped my generation's view of employee compensation. If employees are part of wealth-creation, then they should be part of wealth-enjoyment too.

 3. <a name="bansals"></a>The Bansals[^1]: Bansals were not intimidated by Goliath sized competition. They dreamt big and made [flipkart][4] the biggest e-commerce store in India. The exponential growth may be because of a single thing that every top executive, including the founders do.  Everyone of them [take part][5] in customer service calls.

    It is one thing to talk to prospects but it is another experience to **talk to customers making service calls**. It reveals broken products, broken processes, and broken promises. It's the duty of a CEO to fix these, before they destroy the company.

 4. <a name="vivek"></a>[Vivek][6]: I've been following Vivek since I [interviewed][7] him. He left privileges and prestige of working in an MNC to start his [company][8]. While he was taking public transport, his classmates were buying luxury cars. I'm sure it was a trying time. But Vivek believed in his idea and persevered. Reward for his perseverance came in the form of lucky break from Y-Combinator. Last time I checked, his company's client list is impressive. It includes Facebook. Adobe, Yelp and Evernote. The best in the list? White House.

    **Perseverance is one of the traits that sets apart an entrepreneur**. Having left a cushy job to start DSD, I hope to persevere like Vivek to achieve our mission.

 5. <a name="girish"></a>[Girish Mathrubootham][9]: In Tamil there is a saying, 'வம்பு சண்டைக்கு போகாதே. வந்த சண்டையை விடாதே'. In entrepreneurs' language, it means, "**Never start a crisis;  Never waste a crisis**". Girish lived up to it when his competitor ZenDesk came knocking with nasty comments. He didn't get into a pig fight with them. He punched them with a knack. He put up a [landing page][13] and presented his story with facts. But he didn't stop there. He hit the growth pedal so strong that ZenDesk had no option than to recognise his [company][10] as a competitor, in their SEC filing.

    Crises are inevitable part of building companies. I can't avoid crises, but, I hope, when they come, I will be as street-smart as Girish in turning them into a great opportunity for growth.

To summarise:

 - Educating your stakeholders is part of doing business
 - Let your employees be millionaires too
 - Attend customer service calls as often as possible
 - Persevere in your idea
 - Never waste a crisis

I'm thankful for those who have paved a way before me. Because of their effort, my journey is little better.

Who are the successful entrepreneurs you look up to and what have they taught you? Care to share?

[^1]: [Sanchin Bansal][11] & [Binny Bansal][12].

[1]: https://en.wikipedia.org/wiki/Dhirubhai_Ambani
[2]: http://www.dsdinfosec.com
[3]: https://en.wikipedia.org/wiki/N._R._Narayana_Murthy
[4]: http://www.flipkart.com
[5]: http://articles.economictimes.indiatimes.com/2014-04-26/news/49422720_1_binny-bansal-sachin-bansal-customer-service
[6]: http://rvivek.com
[7]: /starting-a-startup-is-like-an-itch/
[8]: https://www.hackerrank.com
[9]: http://blog.freshdesk.com/author/girish/
[10]: http://freshdesk.com
[11]: https://en.wikipedia.org/wiki/Sachin_Bansal
[12]: https://en.wikipedia.org/wiki/Binny_Bansal
[13]: http://www.ripoffornot.org

