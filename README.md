# jjudeblog

code for generating jjude.com from markdown

## gitlab pages

## custom domain

## 404
it has to be 404.html and not 404/index.html

## redirects
not working across domains :-(

## cron jobs

## still to incorporate
- comemnts with https://remark42.com/


## build
`GOOS=linux GOARCH=amd64 go build -v .`

## comments
- https://remark42.com/

## gzip
- https://stackoverflow.com/questions/43795885/how-to-enable-gzip-compression-for-gitlab-pages
- added `gzip -k -9 $(find public -type f)`

## still to do
- https://evilmartians.com/chronicles/how-to-favicon-in-2021-six-files-that-fit-most-needs

## References
- https://docs.gitlab.com/ee/user/project/pages/getting_started/pages_from_scratch.html

## What I've learned
- `script` section indicates scripts to run for generating the artifacts
